//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  antia3.h -- caccia nemico in grado di ruotare nel piano del video e compiere loop di 
/*  ribaltamento rispetto all'asse x 
///////////////////////////////////////////////////////////////////////*/


#include "gfighter.h"
#include "a32graph.h"
#include "a32sprit.h"
#include "a32util.h"
#include "a32audio.h"
#include "sbengine.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern CObjectStack g_objStack;
extern CObjectStack g_sEShell5;
extern CObjectStack g_sEShell6; //proiettile sferico giallo
extern CObjectStack g_sFlashBomb; //bombe
extern TP_PATH_DATA_PTR g_Path2;
extern LONG g_snBfire;
extern CADXSound g_cs;

BOOL g_bGFighterLoaded=FALSE;
BOOL g_bFlashBombLoaded=FALSE;

IMAGE_FRAME g_imgGFighter[39]; //frame totali
IMAGE_FRAME g_imgFlashBomb[7];

HRESULT LoadGFighterRes(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg=NULL;
	CADXFrameManager *pfm=NULL;
	int i1;

	if (g_bGFighterLoaded) return S_OK; //risorse gi� caricate

	if (!prm->m_lpFm) return E_FAIL; //l'oggetto non � nello stato adatto

	//carica la frame 2 di data3.vpx
    if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

    //puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	i1=0;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[0],0,0,98,50,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[1],i1*98,0,i1*98+98,64,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[2],i1*98,0,i1*98+98,75,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[3],i1*98,0,i1*98+98,86,pimg))) return E_FAIL;
	i1++;
    if (FAILED(pfm->GrabFrame(&g_imgGFighter[4],i1*98,0,i1*98+98,100,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[5],i1*98,0,i1*98+98,112,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[6],i1*98,0,i1*98+98,110,pimg))) return E_FAIL;
	//seconda fila
	i1=0;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[7],i1*98,117,i1*98+98,117+83,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[8],i1*98,117,i1*98+98,117+54,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[9],i1*98,117,i1*98+98,117+64,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[10],i1*98,117,i1*98+98,117+100,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[11],i1*98,117,i1*98+98,117+114,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[12],i1*98,117,i1*98+98,117+112,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[13],i1*98,117,i1*98+98,117+100,pimg))) return E_FAIL;
	i1++;
	if (FAILED(pfm->GrabFrame(&g_imgGFighter[14],i1*98,117,i1*98+98,117+64,pimg))) return E_FAIL;

	if (FAILED(pfm->CreateRotFrame(180,&g_imgGFighter[15],&g_imgGFighter[0]))) return E_FAIL;

	for (int count=1;count<24;count++)
	{
		if (FAILED(pfm->CreateRotFrame(count*15,&g_imgGFighter[count+15],&g_imgGFighter[15]))) return E_FAIL;
	}

	g_bGFighterLoaded=TRUE;

	return S_OK;
}

void FreeGFighterRes(CADXFrameManager *fm)
{	
	if (fm)
	{
		fm->FreeFrameArray(g_imgGFighter,39);	

		g_bGFighterLoaded=FALSE;
	}
}


//carica le immagini della flash bomb
HRESULT LoadFlashBombRes(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg;
	CADXFrameManager *pfm=NULL;

	if (g_bFlashBombLoaded) return S_OK;

	if (!prm->m_lpFm) return E_FAIL;

	//carica la frame 2 di data3.vpx
    if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	if (FAILED(pfm->GrabFrameArray(g_imgFlashBomb,pimg,7,0,240,40,40))) return E_FAIL;

	g_bFlashBombLoaded=TRUE;

	return S_OK;
}

void FreeFlashBombRes(CADXFrameManager *fm)
{
	if (fm)
	{
		fm->FreeFrameArray(g_imgFlashBomb,7);
		g_bFlashBombLoaded=FALSE;
	}

}

HRESULT CreateFlashBombStack(CResourceManager *prm,CObjectStack *pstack,DWORD numitems,int iflags)
{
	CFlashBomb *fb=NULL;
	DWORD count1;

	if (!g_bFlashBombLoaded)
	{
		if (FAILED(LoadFlashBombRes(prm))) return E_FAIL;
	}

	pstack->Create(numitems);

	for (DWORD count=0;count<numitems;count++)
	{
		pstack->m_objStack[count]=new CFlashBomb;

		fb=(CFlashBomb *)pstack->m_objStack[count];
		
		fb->SetFrameManager(prm->GetFrameManager());
		fb->lpfnExplode=PushExpl9;
		fb->pfnCollide=PushCollideMetal;
		fb->SetActiveStack(&g_objStack);
		fb->SetMathModule(&g_fmath);
		fb->SetCartridgeStack(&g_sEShell6);

		for (count1=0;count1<7;count1++) fb->AddFrame(&g_imgFlashBomb[count1]);

	}

	return S_OK;
}


HRESULT CreateGFighter(GFighter *g,CResourceManager *prm)
{
	if (!g) return E_FAIL;
    
	if (FAILED(LoadGFighterRes(prm))) return E_FAIL;

	g->SetUpdateFreq(20);
	g->SetInitialEnergy(10);
	g->dwPower=8;
	g->lpfnExplode=PushExpl9;
	g->pfnCollide=PushCollideMetal;
	g->pfnSmoke=PushSmoke;
	g->SetActiveStack(&g_objStack);
	g->SetCartridgeStack(&g_sEShell6); //sferico
	g->SetWeapon1(&g_sFlashBomb); //bomba

	if (FAILED(g->Create(&g_imgGFighter[15],&g_imgGFighter[0],15))) return E_FAIL;
	
	return S_OK;
}

//crea uno stack di oggetti GFighter
HRESULT CreateGFighterStack(CResourceManager *prm,CObjectStack *pstack,DWORD numitems,int iflags)
{
	GFighter *g=NULL;

	if (!pstack) return E_FAIL;

	pstack->Create(numitems);

	for (DWORD count=0;count<numitems;count++)
	{
		pstack->m_objStack[count]=new GFighter();

		g=(GFighter *)pstack->m_objStack[count];

		g->dwResId=EN_GFIGHTER;

		g->SetGraphicManager(prm->m_lpGm);

		g->SetFrameManager(prm->m_lpFm);
		
		if (FAILED(CreateGFighter(g,prm))) return E_FAIL;

		g->SetFlags(iflags);

		g->bActive=FALSE;

		g->iConfigFreq=5;

		g->SetPath(g_Path2,70);	

		g->SetMathModule(&g_fmath);

	}

	return S_OK;
}

//-------------------------------------- class GFighter ---------------------------------------------

GFighter::GFighter(void)
{
	m_iframeset_rot=m_iframeset_roll=m_inum_roll_frames=0;
	m_pmath=NULL;
	dwClass=CL_ENEMY;
}

GFighter::~GFighter(void)
{
	CEnemy::~CEnemy();
}

void GFighter::Reset(void)
{
	CEnemy::Reset();
	iStepConfig=0;
	iStepUpdate=0;
	m_irollcnt=0;
	m_istate=GF_STATE_NONE;
	m_bResettingTrim=FALSE;
	m_bFollowPath=(pFollowPath != NULL); 
	m_ittl=0;
	m_t=0.0f;
	m_iloopincr=1;
	iWoundedState=0;

	iConfigFreq=0;

	switch(m_dwFlags)
	{
	case 0:
		//segue tutto il path fino alla fine poi esegue l'algoritmo AI
		iFireFreq=90;
		break;

	case 1:

		m_bFollowPath=FALSE;
		iConfigFreq=2+m_pmath->RndFast(3);
		iUpdateFreq = (int)(iUpdateFreq *0.5f);
		m_istate=GF_STATE_STRIGHT;
		iFireFreq=80;
		break;	

	default:

		iFireFreq=40;


	}


	if (m_dwFlags>0) m_bFollowPath=FALSE;

	switch(m_dwFlags)
	{
	case 1:
		SetVelocity(180,3); //velocit� iniziale
		break;
    case 2:
		SetVelocity(180,5);
		break;
    default:
		SetVelocity(180,7);
		break;
	}	 

	SetCurFrame(12); //180�
}

//crea il fighter impostando le frames
//pimgRot � obbligatorio e punta al vettore di frames che visualizzano la rotazione del fighter nel piano del video
//pimgRoll � facoltativo e indica un insieme di frames che indicano la rotazione del fighter rispetto all'asse x del video
//iRollNum=numero di frames contenute in pimgRoll
HRESULT GFighter::Create(IMAGE_FRAME_PTR pimgRot,IMAGE_FRAME_PTR pimgRoll,int iRollNum)
{
	if (!pimgRot) return E_FAIL;
	
	int count;

	//aggiunge il primo set di frames 
	for (count=0;count<24;count++) 
	{
		if (FAILED(AddFrame(&pimgRot[count]))) return E_FAIL; 
	}

	m_iframeset_rot=0; //il primo set inizia alla frame zero
	
	if (pimgRoll)
	{
		//indice della frame iniziale del roll
		m_iframeset_roll=this->GetMaxFramesIndex()+1;
        //aggiunge le frames di rollio
		for (count=0;count<iRollNum;count++)
		{
			if (FAILED(AddFrame(&pimgRoll[count]))) return E_FAIL;			
		}

		m_inum_roll_frames=iRollNum;
	}

	return S_OK;
}

//imposta il modulo matematico
void GFighter::SetMathModule(CADXFastMath *pmath)
{
	m_pmath=pmath;
}

BOOL GFighter::Update(void)
{
	int itmpframe,delta;

	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		iStepUpdate=0;
		iStepConfig++;
	

		if (m_bFollowPath)
		{			

			//sta seguendo un path predefinito
			m_bFollowPath=SetNextPathStep();			
		
			if (!m_bFollowPath) 
			{
 				return FALSE; //fine del path
			}
		}
		else 
		{					

			if (m_istate==GF_STATE_NONE)
			{			
				
				m_ittl++;

				if (m_ittl>=120)
				{
					m_istate=GF_STATE_LEAVING;
					
				}
				else
				{

					//� in stato di attesa deve decidere cosa fare
					if ((x<90 && angle_dir==180) || (x>500 && angle_dir==0))
					{
						//deve fare un loop per ritornare in posizione
						iConfigFreq=11; //imposta il numero esetto di passi per cambiare traiettoria da 180� a 0� o da 0� a 180�
						iStepConfig=0;

						if (y<240)
						{
							m_iloopincr=(angle_dir==180 ? -1 : 1);
						}
						else
						{
							m_iloopincr=(angle_dir==180 ? 1 : -1);
						}

						m_istate=GF_STATE_LOOPING;
					}

					else
					{
						delta=m_pmath->RndFast(20);

						if (angle_dir==180)
						{
							if (delta<=2)
							{
								m_istate=GF_STATE_ROLLING_LOOP;

							}
							else if (delta>2 && delta<=8)
							{
								m_istate=GF_STATE_ROLLING;
								iConfigFreq=m_pmath->RndFast(8)+10;
								SetCurFrame(m_iframeset_roll);

								if (lTargetY<y+10) SetVelocity(angle_dir+15,speed);
								else SetVelocity(angle_dir-15,speed);

							}
						}

					}

				}
			}			
			
		}

		switch(m_istate)
		{
		case GF_STATE_ROLLING_LOOP:

			//rotazione completa su se stesso intorno asse x
			itmpframe=cur_frame;

			if (itmpframe==12) itmpframe=m_iframeset_roll;

			itmpframe++;
			
			if ((itmpframe>=m_iframeset_roll+m_inum_roll_frames-1) || x<80 || x>600) 
			{
				//quando arriva ll'ultima frame il loop � finito
				m_istate=GF_STATE_NONE;
				//fine loop
				itmpframe=12;
				iStepConfig=0;
				SetVelocity(180,speed);
			}

			SetCurFrame(itmpframe);

			break;

		case GF_STATE_ROLLING:
		
			delta=m_pmath->GetAngle(x,y,lTargetX,lTargetY);

			if (delta>angle_dir+30) SetVelocity(angle_dir+15,speed);
			else if (delta<angle_dir-30) SetVelocity(angle_dir-15,speed);
		
			SetCurFrame(angle_dir / 15);

			if (iStepConfig>=iConfigFreq || y<100)
			{
				iStepConfig=0;
				m_istate=GF_STATE_TRANSITION;
			}

			break;

		case GF_STATE_LOOPING:

			//loop completo nel piano del video

			cur_frame+=m_iloopincr;

			if (cur_frame>m_iframeset_rot+23) cur_frame=0;
			else if (cur_frame<m_iframeset_rot) cur_frame=m_iframeset_rot+23;

			SetVelocity(cur_frame*15,speed);
			SetCurFrame(cur_frame);

			if (iStepConfig>=iConfigFreq)
			{
				iStepConfig=0;
				m_istate=GF_STATE_NONE;
			}
		
			break;

        case GF_STATE_STRIGHT:

			
			if (iStepConfig>=iConfigFreq)
			{
				iStepConfig=0;
				m_istate=GF_STATE_NONE;
			}


			break;	
			
        case GF_STATE_TRANSITION:

		
			if (angle_dir>=90 && angle_dir<270)
			{
				delta=angle_dir-180;
				
				//riporta la traiettoria a 180�
				if (delta>0 )
				{
					SetVelocity(angle_dir-15,speed);

				}
				else if (delta<0)
				{
					SetVelocity(angle_dir+15,speed);
				}

			}
			else
			{				
				//riporta la traiettoria a 0�
				if (angle_dir>0)
				{
					SetVelocity(angle_dir-15,speed);

				}
				else if (angle_dir<0)
				{
					SetVelocity(angle_dir+15,speed);
				}

			}		


			if (angle_dir==0) 
			{
				//fine fase transitoria
				SetCurFrame(0);
				m_istate=GF_STATE_NONE;
			}
			else if (angle_dir==180) 
			{
				//fine fase transitoria
				m_istate=GF_STATE_NONE;
				SetCurFrame(12);
			}
			else
			{
				if (cur_frame>=m_iframeset_roll)
				{
					//rollio
					SetCurFrame(--cur_frame);

				}
				else SetCurFrame(angle_dir / 15);
			}


		case GF_STATE_LEAVING:

			//l'oggetto sta muovendosi ruotando sul piano del video
			SetCurFrame(angle_dir / 15);
			
			if (x<-100 || x>SCREEN_WIDTH || y<-80 || y>SCREEN_HEIGHT)
			{
				//il nemico esce di scena
				return FALSE;
			}


			break;

		case GF_STATE_CRASHING:
			
			IncFrame();
			//emette fumo mentre precipita
			if (pfnSmoke) pfnSmoke(x+15,y+10);	

			break;

		default:
			
			//l'oggetto sta muovendosi ruotando sul piano del video
			SetCurFrame(angle_dir / 15);

			break;

		}//fine switch	

	}//fine if (iStepUpdate==iUpdateFreq)


	if (m_istate!=GF_STATE_CRASHING) 
	{
		UpdatePosition();	

		iStepFire++;

		if (iStepFire>=iFireFreq)
		{
			CShell *pShell=NULL;
			int lambda;
			iStepFire=0;

			if (m_pmath->RndFast(5)==1 && m_pWeapon1)
			{
				CEnemy *pen=(CEnemy *)m_pWeapon1->Pop();

				if (pen)
				{
								
					pen->Reset();
					pen->dwClass=CL_ENEMY;				
					pen->SetPosition(x+10,y+20);
					pen->SetEnergy(6);
					m_ActiveStack->Push((CSBObject *)pen);
				}

			}

			else
			{
				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					g_cs.PlaySound(g_snBfire,0,0);	
					//m_Cannon.GetJointPosition(&xf,&yf,1);
					lambda=m_pmath->GetAngle(x,y,lTargetX+10,lTargetY+10);	
					pShell->SetCurFrame(0);						
					//pShell->SetJointPosition(xf,yf,0);
					pShell->SetPosition(x+20,y+20);
					pShell->SetVelocity(lambda,8);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=5;
					m_ActiveStack->Push(pShell);

				}

			}

		}
	}

	else
	{
		//lo stato � GF_STATE_CRASHING
		//il nemico sta precipitando con moto parabolico
		m_t += (float)0.1;
		x+=iVelx;
		y=(int)(m_t*m_t*CSbLevel::g_gravity+iVely*m_t+old_y);				
		
	}

	if (dwEnergy<=0)
	{

		//l'oggetto sta precipitando e si sta schiantando al suolo
		m_istate=GF_STATE_CRASHING;

		if (dwEnergy<-3)
		{
			CreateBonus();
			return FALSE;
		}
		else
		{  	
			if (!iWoundedState)
			{
				//iUpdateFreq=(int)(iUpdateFreq*0.5); //aumenta la frequenza di aggiornamento
				iWoundedState=1;
				m_t=0;
				old_x=x;
				old_y=y;
			
			}		

			if (y>m_lpGm->GetScreenHeight()-CSbLevel::g_bottom)
			{

				if (lpfnExplode) lpfnExplode(x+10,y+10);
				return FALSE;
			}

			return (x>-70 && y<m_lpGm->GetScreenHeight()+100);		   
		}
	}

	return (dwEnergy>0);
}

//------------------------------------- CFlashBomb ----------------------------------------


CFlashBomb::CFlashBomb(void)
{	
	dwClass=CL_ENEMY;
	dwScore=100;
	m_pmath=NULL;
	SetAnimSpeed(0.2f);
}


void CFlashBomb::Reset(void)
{
	int initan=90;

	CEnemy::Reset();
	SetCurFrame(0);
	
	if (m_pmath)
	{
		initan=m_pmath->RndFast(360);

		m_iLife=3+m_pmath->RndFast(5);

	}
	else m_iLife=20;
	
	SetVelocity(initan,1); 
	SetUpdateFreq(40);
	dwPower=8;
	dwEnergy=6;
}

//imposta il modulo matematico
void CFlashBomb::SetMathModule(CADXFastMath *pmath)
{
	m_pmath=pmath;
}


BOOL CFlashBomb::Update(void)
{
	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		iStepUpdate=0;

		int ang,adiff,acur;

		if (m_pmath)
		{
			acur=GetAngle();

			ang=m_pmath->GetAngle(x,y,lTargetX,lTargetY);			

			adiff=m_pmath->GetAngleDiff(acur,ang);

			if (adiff>0)
			{
				SetVelocity(acur+10,1);
			}
			else if (adiff<0)
			{
				SetVelocity(acur-10,1);
			}				

		}

		iStepConfig++;

		if (iStepConfig>=m_iLife)
		{
			CShell *pShell=NULL; 
			//la bomba esplode rilasciando frammenti
			dwEnergy=0;
			if (lpfnExplode) lpfnExplode(x+10,y+10);

			if (m_Cartridges)
			{

				for (DWORD count=0;count<24;count++)
				{
					pShell=(CShell *)m_Cartridges->Pop();

					if (pShell)
					{
					
						//imposta posizione iniziale e velocit� proiettile
						pShell->SetPosition(x,y);
						pShell->dwClass=CL_ENFIRE;
						pShell->SetInitialEnergy(1);
						pShell->dwPower=8;
						pShell->SetVelocity(count*15,5);
						pShell->Reset();
						//immette il projettile nello stack attivo
						m_ActiveStack->Push(pShell);

					}
				}

			}//fine if (m_Cartridges)		

		}

	}

	UpdatePosition();
	UpdateFrame();

	if (y>CSBObject::g_iScreenHeight-70) 
	{		
		//si schianta al suolo
		if (lpfnExplode) lpfnExplode(x,y);
		dwEnergy=0;
	}

	return (dwEnergy>0);

}



