//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  snakeres.h -- funzioni di caricamento risorse per serpenti
///////////////////////////////////////////////////////////////////////*/


#ifndef _SNAKERES_INCLUDE_
#define _SNAKARES_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "tent.h"

//serpente corpo blu
HRESULT LoadSnake1Resources(CResourceManager *prm);
HRESULT FreeSnake1Resources(CADXFrameManager *pfm);

//serpente corpo rosso
HRESULT LoadSnake2Resources(CResourceManager *prm);
HRESULT FreeSnake2Resources(CADXFrameManager *pfm);

HRESULT CreateBlueTentacle(CTentacle *pTent,CResourceManager *prm);
//crea uno stack di oggetti tipo snake rosso
HRESULT CreateSnakeRedStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int ibodyel);
void FreeSnake2Stack(CObjectStack *pstk);


#endif
