//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  spider.h - ragno
///////////////////////////////////////////////////////////////////////*/

#ifndef _SB_SPIDER_
#define _SB_SPIDER_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "mecharm.h"
#include "tent.h"
#include "antia3.h"

#define SPIDER_MAX_LEGS 12

typedef enum LEG_ENUM_TAG
{
	FORWARD=1,
    BACKWARD=2,
	FRONT=4,
	BACK=8

} LEG_TYPE;

//definice una zampa del ragno
typedef struct LEG_STRUCT
{
	LEG_TYPE type;   //tipo: esempio: FORWARD | FRONT = zampa rivolta in avanti frontale
	int xrel,yrel; //positione della zampa nel sistema di coordinate del corpo
	int coupling_id; //id del giunto a cui � collegata la zampa		
	CADXSprite spr; 
	BOOL bStartCycle; //flag che indica che la zampa � all'inizio del ciclo di movimento (serve solo per il rumore)


} SPIDER_LEG_TYPE,*SPIDER_LEG_TYPE_PTR;


class CSpider:public CEnemy
{

protected:
	
	BOOL m_bLegCouplings;
	CADXFastMath *m_pmath;
	int m_iNumLegs; //numero di zampe
	SPIDER_LEG_TYPE m_legs[SPIDER_MAX_LEGS]; 
	CADXSprite m_sprLegFwd,m_sprLegBack;
	CADXSprite *m_pspr; //general purpose
	void SetMovement(int iDirection,int ispeed); //imposta la direzione (0=fermo 1=avanti -1=indietro)
	CObjectStack* m_pSmoke;
			
public:

	CMechComponent m_BodyFrame;
	CTentacle *m_pTentacle;
	CMechArm *m_pArm[2];
	CAntiAircraft3 *m_pAA3;
	CSpider(void);
	virtual ~CSpider(void);
	HRESULT Create(IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimgLegFwd,int *piFwdHotSpot,IMAGE_FRAME_PTR pimgLegBack,int *piBackHotSpot,int iLegFrames,int iBodyFrames);
    HRESULT AddLegCouplings(int xrel,int yrel,LEG_TYPE type,int start_frame);
	BOOL Update(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	void SetSmokeStack(CObjectStack *pStack);
	void (*lpfnExplMulti)(CADXSprite *psrc,int num_explosions);
	HRESULT Draw(void);
	void Reset(void);
	void Explode(void);
	HRESULT SetMathModule(CADXFastMath *pmath);

};

//carica le risorse del ragno
HRESULT LoadSpiderResources(CResourceManager *prm);
HRESULT FreeSpiderResources(CADXFrameManager *pfm);
HRESULT CreateSpiderStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);
void FreeSpiderStack(CObjectStack *pstk);


#endif