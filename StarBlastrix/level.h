//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  level.h - Classe per gestione dei livelli di gioco
///////////////////////////////////////////////////////////////////////*/

//------------------------------- classe CSbLevel ------------------------------------------------------------------

#ifndef __SBLEVEL__
#define __SBLEVEL__

//propriet� del terreno
#define LV_SOLID 0x0
#define LV_LIQUID 0x1
#define LV_INCONSISTENT 0x2
#define LV_ICE 0x3 //ghiaccio che si spacca
#define LV_LUNAR 0x4 //superficie su cui si formano crateri

#define MAX_SCROLL_LAYERS 6
#define MAX_SCROLL_BACKGROUNDS 6

#include "sbl.h"

//-------------------------------------- SCROLL_LAYER --------------------------------------------

//struttura che contiene le informazioni di un layer che scrolla
typedef struct 
{
	LPRECT prcScrollTile; //livelli di scroll del terreno (rettangoli dei tiles)	
	LONG lTop;	          //ordinata di blitting
	DWORD lTileWidth;     //larghezza dei tiles di ogni livello
	float fParallax;	
	DWORD dwNumTiles; //numero di tiles presenti nel vettore di ciascun livello di parallasse
	DWORD dwNumMapItems; //numero di elementi nella mappa di ciascun livello di parallasse
    UINT *piMap; //punta alla mappa dei tile	
	DWORD dwTilesOnScreen; //numero di tiles necessari per coprire una striscia di video per ogni livello	
    RECT rcClipRectLeft; //rettangolo di clipping del primo tile
	RECT rcClipRectRight; //rettangolo di clipping dell'ultimo tile	
	DWORD dwCurrentX;
	DWORD dwCurrentTile; //tile corrente per ogni livello, � il tile che viene disegnato per primo
	DWORD dwMaxX;
	DWORD dwClip; //bordo di clipping a sx
	DWORD dwClip1; //bordo di clipping a dx
	DWORD dwDeltaClip; //differenza fra la larghezza dei tile allineati e la larghezza del display
	RECT rcViewPort;   //bacground view port (rettagolo sorgente per il blitting)
	IMAGE_FRAME_PTR pimgBack;  //immagine background (non usa la mappa se si tratta del background)
	LPDIRECTDRAWSURFACE7 pDDSBack; //sup. immagine background

}	SCROLL_LAYER,*SCROLL_LAYER_PTR;


//---------------------------------------- SB_TRIGGER --------------------------------------------

//trigger
//(� un evento)
typedef struct SB_TRIGGER_TAG
{
	DWORD dwPower;	
	DWORD dwFlags;
	DWORD dwEnergy;
    LONG xabs; //ascissa assoluta in cui viene azionato il trigger
	LONG y; //yrelativa del punto in cui deve apparire l'oggetto
	int zlayer;
	CObjectStack *pSrcStack; //punta lo stack dal quale deve prelevare il nemico 
	CObjectStack *pBonusStack; //stack degli oggetti bonus , se � nullo non viene associato nessun bonus
}

SB_TRIGGER,*SB_TRIGGER_PTR;

typedef struct SB_RECTARRAY_TAG
{
	LPRECT pRect;

} SB_RECTARRAY;

typedef struct SB_TILEMAP_TAG
{
	int *pimap;

} SB_TILEMAP;


//statistica nemici colpiti
typedef struct SB_STAT_TAG
{
	int iTot; //nemici totali di un certo tipo
	int iHit; //nemici distrutti

} SB_STAT,*SB_STAT_PTR;


//La classe CSbLevel implementa il livello corrente (sfondo con i vari livelli di parallasse
//eventi ecc...
//ci sono 4 livelli di parallasse per il terreno e due per il cielo

class CSbLevel:public CADXBase,public CADXDebug
{
private:

	SCROLL_LAYER *m_sLayer; //livelli di scrolling del terreno
	SCROLL_LAYER *m_sBackLayer; //background (cielo)
	DWORD m_dwNumScrollLayers; //numero di layer per il terreno
	DWORD m_dwNumScrollBackground; //numero di layer per il background (cielo)
	IMAGE_FRAME m_imgSource; //immagine che contiene tutti i tiles dei livelli  
	LPDIRECTDRAWSURFACE7 m_pDDSSource; //superficie che contiene tutti i tiles
	LONG m_lMaxX;
	LONG m_lLimit; //punto in cui inizia la sequenza di fine livello
	LONG m_lLevelCurrentX;
	LONG m_lLevelXTrigger; //ascissa per controllo triggers
	DWORD m_dwDispW,m_dwDispH; //dimensioni della superficie di output
	RECT m_rcDisplay; //rettangolo di clipping del display 
	void (*m_lpfnClear)(IMAGE_FRAME_PTR pimgDisplay); //funzione che pulisce il background prima di renderizzare
	SB_TRIGGER_PTR pCurTrigger; //trigger corrente
	SB_TRIGGER_PTR pTriggers; //Array di trigger popolato tramite la funzione CreateTrigger
	DWORD m_dwNumTriggers; //numero di trigger presenti in pTriggers 
	SB_TRIGGER_PTR pTopTrigger; //punta all'ultimo trigger del vettore dei trigger
	CObjectStack *m_lpActiveStack; //punta lo stack attivo
	int *m_piGroundType; //vettore con il tipo di terreno
	int m_iScrollSpeed; //velocit� di scrolling in pixel/ciclo	
	TCHAR *m_szLevelName; //nome del livello
	TCHAR *m_szFinalMessage; //messaggio visualizzato quando il livello viene completato
	SB_RECTARRAY *m_prcLayer; //rettangoli di grabbing dei layer creati allocati dinamicamente
	SB_TILEMAP *m_pmaps; //mappe dei tile dei layer del terreno
	IMAGE_FRAME_PTR m_pbackimg; //immagini per i layers del background		
	int m_password; //password

public:  
	
	static float g_gravity; //acc. di gravit� corrente
	static int g_scroll_speed; //velocit� di scroll corrente
	static int g_bottom; //livello del terreno corrente	

	CSbLevel(void);
	~CSbLevel(void);
	//imposta il nome del livello e il messaggio finale
	void SetLevelName(TCHAR *szLevelName);
	void SetFinalMessage(TCHAR *szFinalMessage);
	TCHAR *GetLevelName(void);
	TCHAR *GetFinalMessage(void);
	SB_STAT m_Stats[EN_LAST+1];
	
	//restituisce il numero di layer per il terreno e per il background
	DWORD GetNumScrollLayers(void);
	BOOL SetNumScrollLayers(DWORD dwNum); //imposta il numero di layer per il terreno
	DWORD GetNumScrollBackground(void); //numero di layer del background
	BOOL SetNumScrollBackground(DWORD dwNum); //imposta il numero di layer per il background
	void ResetStats(void);
	//imposta la larghezza e l'altezza della superficie che viene usata come output
	//(queste due dimensioni possono coincidere con la risuluzione video)
	void SetDisplay(DWORD dwDispW,DWORD dwDispH);	
	//Imposta il vettore di tile a cui si riferisce la mappa (vettore di indici che indicano i vari tile)
	//dwLayer indica il livello di parallasse, 0 � il livello piu' vicino ed � quello che si muove piu' velocemente
	HRESULT SetScrollTileArray(LPRECT prcArray,DWORD dwNumTiles,DWORD dwLayer);
	//imposta la mappa dei tile per un determinato livello di parallasse
	HRESULT SetScrollMap(UINT *piArray,DWORD dwNumItems,DWORD dwLayer,LONG lTop,float fParallax);
	//imposta un livello di scrolling per il cielo (background)
	HRESULT SetScrollBackground(DWORD dwLayer,LONG lTop,float fParallax,LONG lrcLeft,LONG lrcTop,LONG lrcRight,LONG lrcBottom,IMAGE_FRAME_PTR imgSrc);
	//imposta un layer di scrolling del terreno
	HRESULT SetScrollLayer(DWORD dwLayer,LONG lTop,float fParallax,LONG lrcLeft,LONG lrcTop,LONG lCols,LONG lRows,LONG lTileWidth,LONG lTileHeight,UINT *piArray,DWORD dwNumItems);
	//imposta la funzione da chiamare per pulire il background (l'argomento � un puntatore a funzione)
	HRESULT SetBackgroundClearFn(void (*lpFn)(IMAGE_FRAME_PTR));
	//imposta una propriet� del terreno per il tile con indice iTileIndex (es. LV_SOLID,LV_LIQUID ecc..., si riferisce al layer del terreno in primo piano)
	HRESULT SetGroundType(int iTileIndex,int iType);	
	//restituisce il tipo di terreno in corrispondenza della ascissa relativa xrel
	int GetGroundType(int xrel);
	//restituisce la x massima del livello corrente (viene deternimata dopo aver impostato gli array e le mappe)
	LONG GetMaxX(void);
    //renderizza i triggers in modo statico (usato per il level editor)
	HRESULT RenderTriggers(IMAGE_FRAME_PTR pimgDest,SB_TRIGGER_PTR pSelected);
	//restituisce il trigger alle coordinate x,y
	SB_TRIGGER_PTR GetTriggerAt(LONG x,LONG y);
	//restituisce il vettore dei trigger
	SB_TRIGGER_PTR GetTriggers(void);
	//restituisce il top trigger
	SB_TRIGGER_PTR GetTopTrigger(void);
	//esegue il rendering del livello sulla frame indicata
	void Render(IMAGE_FRAME_PTR pimgDest);
	//Scrolla il livello
	void ScrollOn(IMAGE_FRAME_PTR pimgDest);
	//imposta la posizione del livello
	void SetPosition(DWORD dwPosX);
	//restituisce la x corrente
	LONG GetCurrentX(void);	
	//punto finale del livello in cui inizia la sequenza di fine livello
	void SetXLimit(LONG limit);
	//restituisce l'indice del primo tile visibile per il layer iLayer
	int GetCurrentTileIndex(int iLayer);
	//imposta un tile di un layer
	int SetTile(int iTile,int iIndex,int iLayer);
	//restituisce il numero di tiles in un layer
	int GetTilesCount(int iLayer);
	//acquisisce un tile
	int GetTile(int iLayer,int iIndex);
	LONG GetXLimit(void);
	//restituisce la velocit� di scrolling in pixel/ciclo
	int GetScrollSpeed(void);
	//imposta la velocit� di scrolling  (pixel/ciclo)
	void SetScrollSpeed(int ispeed);
	//Crea il livello dimensionando le varie superfici e impostando lo stack degli oggetti attivi
	HRESULT CreateLevel(TCHAR *szVPXSource,DWORD dwFrame,CObjectStack *pActiveStack);
	void Release(void); //rilascia il livello corrente
	//controlla le collisioni fra gli oggetti 
	BOOL DetectCollisions(CSBObject *pobj,DWORD dwStart,DWORD dwNumObjects);
	//crea un trigger (evento)
	HRESULT CreateTrigger(LONG xabs,LONG yrel,DWORD dwPower,DWORD dwEnergy,DWORD dwFlags,CObjectStack *pEnemyStack,CObjectStack *pBonusStack=NULL,int zlayer=0);
	//RemoveTrigger
	HRESULT RemoveTrigger(SB_TRIGGER_PTR pTrigger);
	//ordina i triggers dal piu' vicino al piu' lontano
	void SortTriggers(void);
	//esegue l'evento corrente se la x corrente e >= della x dell'evento puntato da NextEvent
	int RaiseEvent(void); 
	//imposta il puntatore al trigger corrente in base alla posizione dwPos (usata per debug)
	void SetTriggerPosition(DWORD dwPos);
	//imposta la password
	void SetPassword(int pwd);
	//restituisce la password
	int GetPassword(void);
};

#endif