# Microsoft Developer Studio Generated NMAKE File, Based on StarBlastrix.dsp
!IF "$(CFG)" == ""
CFG=StarBlastrix - Win32 Debug
!MESSAGE No configuration specified. Defaulting to StarBlastrix - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "StarBlastrix - Win32 Release" && "$(CFG)" != "StarBlastrix - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "StarBlastrix.mak" CFG="StarBlastrix - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "StarBlastrix - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "StarBlastrix - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "StarBlastrix - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\StarBlastrix.exe" "$(OUTDIR)\StarBlastrix.bsc"


CLEAN :
	-@erase "$(INTDIR)\a32audio.obj"
	-@erase "$(INTDIR)\a32audio.sbr"
	-@erase "$(INTDIR)\a32debug.obj"
	-@erase "$(INTDIR)\a32debug.sbr"
	-@erase "$(INTDIR)\a32fastm.obj"
	-@erase "$(INTDIR)\a32fastm.sbr"
	-@erase "$(INTDIR)\a32graph.obj"
	-@erase "$(INTDIR)\a32graph.sbr"
	-@erase "$(INTDIR)\a32img16.obj"
	-@erase "$(INTDIR)\a32img16.sbr"
	-@erase "$(INTDIR)\a32input.obj"
	-@erase "$(INTDIR)\a32input.sbr"
	-@erase "$(INTDIR)\a32object.obj"
	-@erase "$(INTDIR)\a32object.sbr"
	-@erase "$(INTDIR)\a32sprit.obj"
	-@erase "$(INTDIR)\a32sprit.sbr"
	-@erase "$(INTDIR)\a32util.obj"
	-@erase "$(INTDIR)\a32util.sbr"
	-@erase "$(INTDIR)\antia3.obj"
	-@erase "$(INTDIR)\antia3.sbr"
	-@erase "$(INTDIR)\asteroid.obj"
	-@erase "$(INTDIR)\asteroid.sbr"
	-@erase "$(INTDIR)\blaster2.obj"
	-@erase "$(INTDIR)\blaster2.sbr"
	-@erase "$(INTDIR)\bleezer.obj"
	-@erase "$(INTDIR)\bleezer.sbr"
	-@erase "$(INTDIR)\boss1.obj"
	-@erase "$(INTDIR)\boss1.sbr"
	-@erase "$(INTDIR)\ccharset.obj"
	-@erase "$(INTDIR)\ccharset.sbr"
	-@erase "$(INTDIR)\ceffcts.obj"
	-@erase "$(INTDIR)\ceffcts.sbr"
	-@erase "$(INTDIR)\enemy.obj"
	-@erase "$(INTDIR)\enemy.sbr"
	-@erase "$(INTDIR)\gfighter.obj"
	-@erase "$(INTDIR)\gfighter.sbr"
	-@erase "$(INTDIR)\grndauxen.obj"
	-@erase "$(INTDIR)\grndauxen.sbr"
	-@erase "$(INTDIR)\jltent.obj"
	-@erase "$(INTDIR)\jltent.sbr"
	-@erase "$(INTDIR)\level.obj"
	-@erase "$(INTDIR)\level.sbr"
	-@erase "$(INTDIR)\lifter.obj"
	-@erase "$(INTDIR)\lifter.sbr"
	-@erase "$(INTDIR)\mammoth.obj"
	-@erase "$(INTDIR)\mammoth.sbr"
	-@erase "$(INTDIR)\mcasmbl.obj"
	-@erase "$(INTDIR)\mcasmbl.sbr"
	-@erase "$(INTDIR)\mech.obj"
	-@erase "$(INTDIR)\mech.sbr"
	-@erase "$(INTDIR)\mecharm.obj"
	-@erase "$(INTDIR)\mecharm.sbr"
	-@erase "$(INTDIR)\modenemy.obj"
	-@erase "$(INTDIR)\modenemy.sbr"
	-@erase "$(INTDIR)\modular.obj"
	-@erase "$(INTDIR)\modular.sbr"
	-@erase "$(INTDIR)\resman.obj"
	-@erase "$(INTDIR)\resman.sbr"
	-@erase "$(INTDIR)\ring.obj"
	-@erase "$(INTDIR)\ring.sbr"
	-@erase "$(INTDIR)\sb.res"
	-@erase "$(INTDIR)\sbengine.obj"
	-@erase "$(INTDIR)\sbengine.sbr"
	-@erase "$(INTDIR)\sbl.obj"
	-@erase "$(INTDIR)\sbl.sbr"
	-@erase "$(INTDIR)\sblastrix.obj"
	-@erase "$(INTDIR)\sblastrix.sbr"
	-@erase "$(INTDIR)\sbutil.obj"
	-@erase "$(INTDIR)\sbutil.sbr"
	-@erase "$(INTDIR)\snake.obj"
	-@erase "$(INTDIR)\snake.sbr"
	-@erase "$(INTDIR)\snakeres.obj"
	-@erase "$(INTDIR)\snakeres.sbr"
	-@erase "$(INTDIR)\spider.obj"
	-@erase "$(INTDIR)\spider.sbr"
	-@erase "$(INTDIR)\starfield.obj"
	-@erase "$(INTDIR)\starfield.sbr"
	-@erase "$(INTDIR)\tent.obj"
	-@erase "$(INTDIR)\tent.sbr"
	-@erase "$(INTDIR)\thunder.obj"
	-@erase "$(INTDIR)\thunder.sbr"
	-@erase "$(INTDIR)\train.obj"
	-@erase "$(INTDIR)\train.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\StarBlastrix.bsc"
	-@erase "$(OUTDIR)\StarBlastrix.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /W3 /GX /Ob1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\StarBlastrix.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x410 /fo"$(INTDIR)\sb.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\StarBlastrix.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\a32audio.sbr" \
	"$(INTDIR)\a32debug.sbr" \
	"$(INTDIR)\a32fastm.sbr" \
	"$(INTDIR)\a32graph.sbr" \
	"$(INTDIR)\a32img16.sbr" \
	"$(INTDIR)\a32input.sbr" \
	"$(INTDIR)\a32object.sbr" \
	"$(INTDIR)\a32sprit.sbr" \
	"$(INTDIR)\a32util.sbr" \
	"$(INTDIR)\antia3.sbr" \
	"$(INTDIR)\asteroid.sbr" \
	"$(INTDIR)\blaster2.sbr" \
	"$(INTDIR)\boss1.sbr" \
	"$(INTDIR)\ccharset.sbr" \
	"$(INTDIR)\ceffcts.sbr" \
	"$(INTDIR)\enemy.sbr" \
	"$(INTDIR)\gfighter.sbr" \
	"$(INTDIR)\grndauxen.sbr" \
	"$(INTDIR)\jltent.sbr" \
	"$(INTDIR)\level.sbr" \
	"$(INTDIR)\lifter.sbr" \
	"$(INTDIR)\mammoth.sbr" \
	"$(INTDIR)\mcasmbl.sbr" \
	"$(INTDIR)\mech.sbr" \
	"$(INTDIR)\mecharm.sbr" \
	"$(INTDIR)\modenemy.sbr" \
	"$(INTDIR)\modular.sbr" \
	"$(INTDIR)\resman.sbr" \
	"$(INTDIR)\ring.sbr" \
	"$(INTDIR)\sbengine.sbr" \
	"$(INTDIR)\sbl.sbr" \
	"$(INTDIR)\sblastrix.sbr" \
	"$(INTDIR)\sbutil.sbr" \
	"$(INTDIR)\snake.sbr" \
	"$(INTDIR)\snakeres.sbr" \
	"$(INTDIR)\spider.sbr" \
	"$(INTDIR)\starfield.sbr" \
	"$(INTDIR)\tent.sbr" \
	"$(INTDIR)\thunder.sbr" \
	"$(INTDIR)\train.sbr" \
	"$(INTDIR)\bleezer.sbr"

"$(OUTDIR)\StarBlastrix.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib ddraw.lib dxguid.lib dinput.lib dsound.lib winmm.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\StarBlastrix.pdb" /machine:I386 /out:"$(OUTDIR)\StarBlastrix.exe" 
LINK32_OBJS= \
	"$(INTDIR)\a32audio.obj" \
	"$(INTDIR)\a32debug.obj" \
	"$(INTDIR)\a32fastm.obj" \
	"$(INTDIR)\a32graph.obj" \
	"$(INTDIR)\a32img16.obj" \
	"$(INTDIR)\a32input.obj" \
	"$(INTDIR)\a32object.obj" \
	"$(INTDIR)\a32sprit.obj" \
	"$(INTDIR)\a32util.obj" \
	"$(INTDIR)\antia3.obj" \
	"$(INTDIR)\asteroid.obj" \
	"$(INTDIR)\blaster2.obj" \
	"$(INTDIR)\boss1.obj" \
	"$(INTDIR)\ccharset.obj" \
	"$(INTDIR)\ceffcts.obj" \
	"$(INTDIR)\enemy.obj" \
	"$(INTDIR)\gfighter.obj" \
	"$(INTDIR)\grndauxen.obj" \
	"$(INTDIR)\jltent.obj" \
	"$(INTDIR)\level.obj" \
	"$(INTDIR)\lifter.obj" \
	"$(INTDIR)\mammoth.obj" \
	"$(INTDIR)\mcasmbl.obj" \
	"$(INTDIR)\mech.obj" \
	"$(INTDIR)\mecharm.obj" \
	"$(INTDIR)\modenemy.obj" \
	"$(INTDIR)\modular.obj" \
	"$(INTDIR)\resman.obj" \
	"$(INTDIR)\ring.obj" \
	"$(INTDIR)\sbengine.obj" \
	"$(INTDIR)\sbl.obj" \
	"$(INTDIR)\sblastrix.obj" \
	"$(INTDIR)\sbutil.obj" \
	"$(INTDIR)\snake.obj" \
	"$(INTDIR)\snakeres.obj" \
	"$(INTDIR)\spider.obj" \
	"$(INTDIR)\starfield.obj" \
	"$(INTDIR)\tent.obj" \
	"$(INTDIR)\thunder.obj" \
	"$(INTDIR)\train.obj" \
	"$(INTDIR)\sb.res" \
	"$(INTDIR)\bleezer.obj"

"$(OUTDIR)\StarBlastrix.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "StarBlastrix - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\StarBlastrix.exe" "$(OUTDIR)\StarBlastrix.bsc"


CLEAN :
	-@erase "$(INTDIR)\a32audio.obj"
	-@erase "$(INTDIR)\a32audio.sbr"
	-@erase "$(INTDIR)\a32debug.obj"
	-@erase "$(INTDIR)\a32debug.sbr"
	-@erase "$(INTDIR)\a32fastm.obj"
	-@erase "$(INTDIR)\a32fastm.sbr"
	-@erase "$(INTDIR)\a32graph.obj"
	-@erase "$(INTDIR)\a32graph.sbr"
	-@erase "$(INTDIR)\a32img16.obj"
	-@erase "$(INTDIR)\a32img16.sbr"
	-@erase "$(INTDIR)\a32input.obj"
	-@erase "$(INTDIR)\a32input.sbr"
	-@erase "$(INTDIR)\a32object.obj"
	-@erase "$(INTDIR)\a32object.sbr"
	-@erase "$(INTDIR)\a32sprit.obj"
	-@erase "$(INTDIR)\a32sprit.sbr"
	-@erase "$(INTDIR)\a32util.obj"
	-@erase "$(INTDIR)\a32util.sbr"
	-@erase "$(INTDIR)\antia3.obj"
	-@erase "$(INTDIR)\antia3.sbr"
	-@erase "$(INTDIR)\asteroid.obj"
	-@erase "$(INTDIR)\asteroid.sbr"
	-@erase "$(INTDIR)\blaster2.obj"
	-@erase "$(INTDIR)\blaster2.sbr"
	-@erase "$(INTDIR)\bleezer.obj"
	-@erase "$(INTDIR)\bleezer.sbr"
	-@erase "$(INTDIR)\boss1.obj"
	-@erase "$(INTDIR)\boss1.sbr"
	-@erase "$(INTDIR)\ccharset.obj"
	-@erase "$(INTDIR)\ccharset.sbr"
	-@erase "$(INTDIR)\ceffcts.obj"
	-@erase "$(INTDIR)\ceffcts.sbr"
	-@erase "$(INTDIR)\enemy.obj"
	-@erase "$(INTDIR)\enemy.sbr"
	-@erase "$(INTDIR)\gfighter.obj"
	-@erase "$(INTDIR)\gfighter.sbr"
	-@erase "$(INTDIR)\grndauxen.obj"
	-@erase "$(INTDIR)\grndauxen.sbr"
	-@erase "$(INTDIR)\jltent.obj"
	-@erase "$(INTDIR)\jltent.sbr"
	-@erase "$(INTDIR)\level.obj"
	-@erase "$(INTDIR)\level.sbr"
	-@erase "$(INTDIR)\lifter.obj"
	-@erase "$(INTDIR)\lifter.sbr"
	-@erase "$(INTDIR)\mammoth.obj"
	-@erase "$(INTDIR)\mammoth.sbr"
	-@erase "$(INTDIR)\mcasmbl.obj"
	-@erase "$(INTDIR)\mcasmbl.sbr"
	-@erase "$(INTDIR)\mech.obj"
	-@erase "$(INTDIR)\mech.sbr"
	-@erase "$(INTDIR)\mecharm.obj"
	-@erase "$(INTDIR)\mecharm.sbr"
	-@erase "$(INTDIR)\modenemy.obj"
	-@erase "$(INTDIR)\modenemy.sbr"
	-@erase "$(INTDIR)\modular.obj"
	-@erase "$(INTDIR)\modular.sbr"
	-@erase "$(INTDIR)\resman.obj"
	-@erase "$(INTDIR)\resman.sbr"
	-@erase "$(INTDIR)\ring.obj"
	-@erase "$(INTDIR)\ring.sbr"
	-@erase "$(INTDIR)\sb.res"
	-@erase "$(INTDIR)\sbengine.obj"
	-@erase "$(INTDIR)\sbengine.sbr"
	-@erase "$(INTDIR)\sbl.obj"
	-@erase "$(INTDIR)\sbl.sbr"
	-@erase "$(INTDIR)\sblastrix.obj"
	-@erase "$(INTDIR)\sblastrix.sbr"
	-@erase "$(INTDIR)\sbutil.obj"
	-@erase "$(INTDIR)\sbutil.sbr"
	-@erase "$(INTDIR)\snake.obj"
	-@erase "$(INTDIR)\snake.sbr"
	-@erase "$(INTDIR)\snakeres.obj"
	-@erase "$(INTDIR)\snakeres.sbr"
	-@erase "$(INTDIR)\spider.obj"
	-@erase "$(INTDIR)\spider.sbr"
	-@erase "$(INTDIR)\starfield.obj"
	-@erase "$(INTDIR)\starfield.sbr"
	-@erase "$(INTDIR)\tent.obj"
	-@erase "$(INTDIR)\tent.sbr"
	-@erase "$(INTDIR)\thunder.obj"
	-@erase "$(INTDIR)\thunder.sbr"
	-@erase "$(INTDIR)\train.obj"
	-@erase "$(INTDIR)\train.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\StarBlastrix.bsc"
	-@erase "$(OUTDIR)\StarBlastrix.exe"
	-@erase "$(OUTDIR)\StarBlastrix.ilk"
	-@erase "$(OUTDIR)\StarBlastrix.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\StarBlastrix.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x410 /fo"$(INTDIR)\sb.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\StarBlastrix.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\a32audio.sbr" \
	"$(INTDIR)\a32debug.sbr" \
	"$(INTDIR)\a32fastm.sbr" \
	"$(INTDIR)\a32graph.sbr" \
	"$(INTDIR)\a32img16.sbr" \
	"$(INTDIR)\a32input.sbr" \
	"$(INTDIR)\a32object.sbr" \
	"$(INTDIR)\a32sprit.sbr" \
	"$(INTDIR)\a32util.sbr" \
	"$(INTDIR)\antia3.sbr" \
	"$(INTDIR)\asteroid.sbr" \
	"$(INTDIR)\blaster2.sbr" \
	"$(INTDIR)\boss1.sbr" \
	"$(INTDIR)\ccharset.sbr" \
	"$(INTDIR)\ceffcts.sbr" \
	"$(INTDIR)\enemy.sbr" \
	"$(INTDIR)\gfighter.sbr" \
	"$(INTDIR)\grndauxen.sbr" \
	"$(INTDIR)\jltent.sbr" \
	"$(INTDIR)\level.sbr" \
	"$(INTDIR)\lifter.sbr" \
	"$(INTDIR)\mammoth.sbr" \
	"$(INTDIR)\mcasmbl.sbr" \
	"$(INTDIR)\mech.sbr" \
	"$(INTDIR)\mecharm.sbr" \
	"$(INTDIR)\modenemy.sbr" \
	"$(INTDIR)\modular.sbr" \
	"$(INTDIR)\resman.sbr" \
	"$(INTDIR)\ring.sbr" \
	"$(INTDIR)\sbengine.sbr" \
	"$(INTDIR)\sbl.sbr" \
	"$(INTDIR)\sblastrix.sbr" \
	"$(INTDIR)\sbutil.sbr" \
	"$(INTDIR)\snake.sbr" \
	"$(INTDIR)\snakeres.sbr" \
	"$(INTDIR)\spider.sbr" \
	"$(INTDIR)\starfield.sbr" \
	"$(INTDIR)\tent.sbr" \
	"$(INTDIR)\thunder.sbr" \
	"$(INTDIR)\train.sbr" \
	"$(INTDIR)\bleezer.sbr"

"$(OUTDIR)\StarBlastrix.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib ddraw.lib dxguid.lib dinput.lib dsound.lib winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\StarBlastrix.pdb" /debug /machine:I386 /out:"$(OUTDIR)\StarBlastrix.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\a32audio.obj" \
	"$(INTDIR)\a32debug.obj" \
	"$(INTDIR)\a32fastm.obj" \
	"$(INTDIR)\a32graph.obj" \
	"$(INTDIR)\a32img16.obj" \
	"$(INTDIR)\a32input.obj" \
	"$(INTDIR)\a32object.obj" \
	"$(INTDIR)\a32sprit.obj" \
	"$(INTDIR)\a32util.obj" \
	"$(INTDIR)\antia3.obj" \
	"$(INTDIR)\asteroid.obj" \
	"$(INTDIR)\blaster2.obj" \
	"$(INTDIR)\boss1.obj" \
	"$(INTDIR)\ccharset.obj" \
	"$(INTDIR)\ceffcts.obj" \
	"$(INTDIR)\enemy.obj" \
	"$(INTDIR)\gfighter.obj" \
	"$(INTDIR)\grndauxen.obj" \
	"$(INTDIR)\jltent.obj" \
	"$(INTDIR)\level.obj" \
	"$(INTDIR)\lifter.obj" \
	"$(INTDIR)\mammoth.obj" \
	"$(INTDIR)\mcasmbl.obj" \
	"$(INTDIR)\mech.obj" \
	"$(INTDIR)\mecharm.obj" \
	"$(INTDIR)\modenemy.obj" \
	"$(INTDIR)\modular.obj" \
	"$(INTDIR)\resman.obj" \
	"$(INTDIR)\ring.obj" \
	"$(INTDIR)\sbengine.obj" \
	"$(INTDIR)\sbl.obj" \
	"$(INTDIR)\sblastrix.obj" \
	"$(INTDIR)\sbutil.obj" \
	"$(INTDIR)\snake.obj" \
	"$(INTDIR)\snakeres.obj" \
	"$(INTDIR)\spider.obj" \
	"$(INTDIR)\starfield.obj" \
	"$(INTDIR)\tent.obj" \
	"$(INTDIR)\thunder.obj" \
	"$(INTDIR)\train.obj" \
	"$(INTDIR)\sb.res" \
	"$(INTDIR)\bleezer.obj"

"$(OUTDIR)\StarBlastrix.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("StarBlastrix.dep")
!INCLUDE "StarBlastrix.dep"
!ELSE 
!MESSAGE Warning: cannot find "StarBlastrix.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "StarBlastrix - Win32 Release" || "$(CFG)" == "StarBlastrix - Win32 Debug"
SOURCE=..\Arcade32\a32audio.cpp

"$(INTDIR)\a32audio.obj"	"$(INTDIR)\a32audio.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32debug.cpp

"$(INTDIR)\a32debug.obj"	"$(INTDIR)\a32debug.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32fastm.cpp

"$(INTDIR)\a32fastm.obj"	"$(INTDIR)\a32fastm.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32graph.cpp

"$(INTDIR)\a32graph.obj"	"$(INTDIR)\a32graph.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32img16.cpp

"$(INTDIR)\a32img16.obj"	"$(INTDIR)\a32img16.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32input.cpp

"$(INTDIR)\a32input.obj"	"$(INTDIR)\a32input.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\a32object.cpp

"$(INTDIR)\a32object.obj"	"$(INTDIR)\a32object.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=..\Arcade32\a32sprit.cpp

"$(INTDIR)\a32sprit.obj"	"$(INTDIR)\a32sprit.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\Arcade32\a32util.cpp

"$(INTDIR)\a32util.obj"	"$(INTDIR)\a32util.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=.\antia3.cpp

"$(INTDIR)\antia3.obj"	"$(INTDIR)\antia3.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\asteroid.cpp

"$(INTDIR)\asteroid.obj"	"$(INTDIR)\asteroid.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\blaster2.cpp

"$(INTDIR)\blaster2.obj"	"$(INTDIR)\blaster2.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\bleezer.cpp

"$(INTDIR)\bleezer.obj"	"$(INTDIR)\bleezer.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\boss1.cpp

"$(INTDIR)\boss1.obj"	"$(INTDIR)\boss1.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ccharset.cpp

"$(INTDIR)\ccharset.obj"	"$(INTDIR)\ccharset.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ceffcts.cpp

"$(INTDIR)\ceffcts.obj"	"$(INTDIR)\ceffcts.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\enemy.cpp

"$(INTDIR)\enemy.obj"	"$(INTDIR)\enemy.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\gfighter.cpp

"$(INTDIR)\gfighter.obj"	"$(INTDIR)\gfighter.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\grndauxen.cpp

"$(INTDIR)\grndauxen.obj"	"$(INTDIR)\grndauxen.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\jltent.cpp

"$(INTDIR)\jltent.obj"	"$(INTDIR)\jltent.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\level.cpp

"$(INTDIR)\level.obj"	"$(INTDIR)\level.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\lifter.cpp

"$(INTDIR)\lifter.obj"	"$(INTDIR)\lifter.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mammoth.cpp

"$(INTDIR)\mammoth.obj"	"$(INTDIR)\mammoth.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mcasmbl.cpp

"$(INTDIR)\mcasmbl.obj"	"$(INTDIR)\mcasmbl.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mech.cpp

"$(INTDIR)\mech.obj"	"$(INTDIR)\mech.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\mecharm.cpp

"$(INTDIR)\mecharm.obj"	"$(INTDIR)\mecharm.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\modenemy.cpp

"$(INTDIR)\modenemy.obj"	"$(INTDIR)\modenemy.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\modular.cpp

"$(INTDIR)\modular.obj"	"$(INTDIR)\modular.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\resman.cpp

"$(INTDIR)\resman.obj"	"$(INTDIR)\resman.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ring.cpp

"$(INTDIR)\ring.obj"	"$(INTDIR)\ring.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sb.rc

"$(INTDIR)\sb.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\sbengine.cpp

"$(INTDIR)\sbengine.obj"	"$(INTDIR)\sbengine.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sbl.cpp

"$(INTDIR)\sbl.obj"	"$(INTDIR)\sbl.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sblastrix.cpp

"$(INTDIR)\sblastrix.obj"	"$(INTDIR)\sblastrix.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\sbutil.cpp

"$(INTDIR)\sbutil.obj"	"$(INTDIR)\sbutil.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\snake.cpp

"$(INTDIR)\snake.obj"	"$(INTDIR)\snake.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\snakeres.cpp

"$(INTDIR)\snakeres.obj"	"$(INTDIR)\snakeres.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\spider.cpp

"$(INTDIR)\spider.obj"	"$(INTDIR)\spider.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\starfield.cpp

"$(INTDIR)\starfield.obj"	"$(INTDIR)\starfield.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\tent.cpp

"$(INTDIR)\tent.obj"	"$(INTDIR)\tent.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\thunder.cpp

"$(INTDIR)\thunder.obj"	"$(INTDIR)\thunder.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\train.cpp

"$(INTDIR)\train.obj"	"$(INTDIR)\train.sbr" : $(SOURCE) "$(INTDIR)"



!ENDIF 

