//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  antia3.cpp -- torretta contraerea doppia
///////////////////////////////////////////////////////////////////////*/

#include "a32graph.h"
#include "a32sprit.h"
#include "a32util.h"
#include "a32audio.h"
#include "snakeres.h"
#include "sbengine.h"
#include "antia3.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern CObjectStack g_sThunderBolt;
extern CObjectStack g_sEShell11;
extern CObjectStack g_objStack;
extern CObjectStack g_sEShell3,g_sSmoke;
extern CADXSound g_cs;
extern LONG g_snBfire,g_snMagnetic; //rumore sparo

BOOL g_bAA3Loaded=FALSE;
IMAGE_FRAME g_imgAA3[5];  //frame della torretta contraerea 0=240� 1=255� 2=270� (verticale) 3=285� 4=300�
IMAGE_FRAME g_imgAA3Turret;
IMAGE_FRAME g_imgAA3Turret1; //torretta danneggiata

//carica le risorse del ragno
HRESULT LoadAA3Resources(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg=NULL;
	CADXFrameManager *pfm=NULL;

	if (g_bAA3Loaded) return S_OK;

   	if (!prm->m_lpFm) return E_FAIL; //l'oggetto non � nello stato adatto

	//carica la frame 1 di data3.vpx
    if (!(1==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),1))) return E_FAIL;
	}

    //puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	if (FAILED(pfm->GrabFrame(&g_imgAA3Turret,309,167,431,264,pimg))) return E_FAIL; //torretta
	if (FAILED(pfm->GrabFrame(&g_imgAA3Turret1,493,167,615,264,pimg))) return E_FAIL; //torretta danneggiata
    if (FAILED(pfm->GrabFrame(&g_imgAA3[2],453,161,489,266,pimg))) return E_FAIL; //cannoni 270 �
	//crea le frame ruotate per i cannoni
	if (FAILED(pfm->CreateRotFrame(-30,&g_imgAA3[0],&g_imgAA3[2],TRUE))) return E_FAIL; //cannoni a sinistra 30� //150
	if (FAILED(pfm->CreateRotFrame(-15,&g_imgAA3[1],&g_imgAA3[2],TRUE))) return E_FAIL; //cannoni a sinistra 15� //165
	if (FAILED(pfm->CreateRotFrame(15,&g_imgAA3[3],&g_imgAA3[2],TRUE))) return E_FAIL; //195
	if (FAILED(pfm->CreateRotFrame(30,&g_imgAA3[4],&g_imgAA3[2],TRUE))) return E_FAIL; //cannoni a destra di 30�

	g_bAA3Loaded=TRUE;
	
	return S_OK;

}

//rilascia la memoria allocata per il caricamento delle immagini 
HRESULT FreeAA3Resources(CADXFrameManager *pfm)
{	
	if (!g_bAA3Loaded) return S_OK;
	if (!pfm) return E_FAIL;
	pfm->FreeImgFrame(&g_imgAA3Turret);
	pfm->FreeImgFrame(&g_imgAA3Turret1);
	pfm->FreeFrameArray(g_imgAA3,5);
	g_bAA3Loaded=FALSE;
	return S_OK;
}

HRESULT CreateAA3(CAntiAircraft3 *pAA3,CResourceManager *prm)
{
	if (FAILED(LoadAA3Resources(prm))) return E_FAIL; //carica gli sprites pr questo tipo di tentacolo
	if (!pAA3) return E_FAIL;

	pAA3->SetUpdateFreq(20);
	pAA3->SetInitialEnergy(40);
	pAA3->dwPower=15;
	pAA3->lpfnExplode=PushExpl9;
	pAA3->lpfnMultiExplosion=PushExplMulti;
	pAA3->pfnCollide=PushCollideMetal;
	pAA3->SetActiveStack(&g_objStack);
	pAA3->SetCartridgeStack(&g_sEShell3);
	pAA3->AddWoundedState(20,1);
	//g_imgAA3Turret
	//le coordinate 60 81 sono riferite alla prima frame cio� a quella ruotata di 30 gradi a sinistra
	if (FAILED(pAA3->Create(&g_imgAA3Turret,&g_imgAA3Turret1,g_imgAA3,5,240,60,81,61,47,61,94))) return E_FAIL;
	//aggiunge gli hot spot per le bocche dei cannoni
	pAA3->AddCannonsHotSpots(6,4,23,4);
	pAA3->SetSmokeStack(&g_sSmoke);
	pAA3->AddFrame(&g_imgAA3Turret);
	pAA3->AddFrame(&g_imgAA3Turret1);

	return S_OK;
}


HRESULT CreateAA3Stack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	CAntiAircraft3 *pAA3=NULL;

	if (!pstk) return E_FAIL;

	//carica le risorse
	if (FAILED(LoadAA3Resources(prm))) return E_FAIL;

	if (FAILED(pstk->Create(inumitems))) return E_FAIL;

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CAntiAircraft3;
		pAA3=(CAntiAircraft3 *)pstk->m_objStack[count];
		pAA3->dwResId=EN_ANTIAIRCRAFT3;
		pAA3->bActive=FALSE;
		pAA3->SetGraphicManager(prm->GetGraphicManager());
		pAA3->SetFrameManager(prm->GetFrameManager());	
		if (FAILED(CreateAA3(pAA3,prm))) return E_FAIL;
	
	}

	return S_OK;
}


void FreeAA3Stack(CObjectStack *pstk)
{
	if (pstk)
	{
	//	CAntiAircraft3 *pAA3;

	/*	for (DWORD count=0;count<pstk->ItemCount();count++)
		{
			pAA3=(CAntiAircraft3 *)pstk->m_objStack[count];
			pAA3->~CAntiAircraft3();

		}*/

		pstk->Clear(TRUE);
	}
}

//------------------------------------ CAntiAircraft3 -------------------------------------


CAntiAircraft3::~CAntiAircraft3(void)
{	
	m_Cannon.Release();
	m_Base.Release();
}

CAntiAircraft3::CAntiAircraft3(void)
{
	lCurX=0;
	m_pSmoke=NULL;
	m_iLeftCannon=m_iRightCannon=0;
	dwClass=CL_ENEMY;
	dwType=24;
	m_jBase=0;
	dwScore=5000;
}


/*

Crea la torretta contraerea
pimgTurret=imagine della torretta (fissa)
pimgCannon=punta al vettore di frames del cannone
pimgTurretDamaged=frame torretta danneggiata
iCannonFrames=numero di frames del cannone
ifrom_angle=angolo della prima frame del cannone,gli angoli si suppongono crescenti a partire dalla prima frame
con incremento di 15�
jointx,jointy=coordinate del giunto nel sistema di riferimento del cannone
joint_basex,joint_basey=coordinate del giunto 
joint_basex1,joint_basey1=coordinate del giunto alla base della torretta che puo' essere usato per vincolarla ad un veicolo 
*/

 HRESULT CAntiAircraft3::Create(IMAGE_FRAME_PTR pimgTurret,IMAGE_FRAME_PTR pimgTurretDamaged,IMAGE_FRAME_PTR pimgCannon,int iCannonFrames,int ifrom_angle,int jointx,int jointy,int joint_basex,int joint_basey,int joint_basex1,int joint_basey1)
{
	if (!pimgTurret) return E_FAIL;
	if (!pimgCannon) return E_FAIL;
	if (iCannonFrames<=0) return E_FAIL;

	int angle=ifrom_angle+15*iCannonFrames;

	m_Cannon.SetFrameManager(m_lpFm);
	
	m_Base.SetFrameManager(m_lpFm);

	if (FAILED(m_Cannon.CreateComponent(ifrom_angle,angle,pimgCannon,iCannonFrames))) return E_FAIL;
	if (FAILED(m_Base.CreateComponent(0,0,pimgTurret,1))) return E_FAIL;
	m_Base.AddFrame(pimgTurretDamaged);

	int j1=m_Cannon.AddRotCoupling(jointx,jointy,ifrom_angle,angle,0);
	int j2=m_Base.AddRotCoupling(joint_basex,joint_basey,ifrom_angle,angle,0);
	m_jBase=m_Base.AddFixCoupling(joint_basex1,joint_basey1);

	if (!(j1>=0 && j2>=0)) return E_FAIL;

	m_Cannon.LinkToComponent(j1,j2,&m_Base);
	m_Cannon.SetLambda(270);
	
	return S_OK;
}

//aggiunge gli hot spot per le bocche dei due cannoni
HRESULT CAntiAircraft3::AddCannonsHotSpots(int xleft,int yleft,int xright,int yright)
{	
	//memorizza gli id degli hot spot delle bocche dei cannoni
    m_iLeftCannon=m_Cannon.AddFixCoupling(xleft,yleft);
	m_iRightCannon=m_Cannon.AddFixCoupling(xright,yright);

	if (m_iLeftCannon>0 && m_iRightCannon>0) return S_OK;
	else return E_FAIL;

}

void CAntiAircraft3::SetEnergy(LONG energy)
{
	m_Cannon.m_iTag1=30;
	m_Base.m_iTag1=energy;
	dwEnergy=energy;
}


BOOL CAntiAircraft3::DoCollisionWith(CSBObject *pobj)
{
	if (m_Cannon.m_iTag1>0)
	{
		if (m_Cannon.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Cannon,pobj);
			return TRUE;
		}
	}

	if (m_Base.m_iTag1>0)
	{
		if (m_Base.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Base,pobj);
			dwEnergy=m_Base.m_iTag1;

			if (dwEnergy<0) CreateBonus();

			return TRUE;
		}
	}

	return FALSE;

}

HRESULT CAntiAircraft3::LinkToVehicle(CMechComponent *pParent,int iParentJoint)
{
	if (!pParent) return E_FAIL;

	return (m_Base.LinkToComponent(m_jBase,iParentJoint,pParent)>0 ? S_OK:E_FAIL);

}

void CAntiAircraft3::Reset(void)
{
	lCurX=0;
	CEnemy::Reset();
//	m_Cannon.m_iTag1=30;
//	m_Base.m_iTag1=dwEnergy;
	SetEnergy(dwEnergy);
	m_Base.SetCurFrame(0);
	m_Base.Unlink(); //svincola la base se era vincolata ad un altro veicolo
}


HRESULT CAntiAircraft3::SetSmokeStack(CObjectStack *pStack)
{
	if (pStack)
	{
		m_pSmoke=pStack;
		return S_OK;
	}
	else return E_FAIL;
}


HRESULT CAntiAircraft3::Draw(void)
{
	m_Base.SetDisplay(m_pimgOut);
    m_Base.RenderToDisplay();
	
	if (m_Cannon.m_iTag1>0)
	{
		m_Cannon.SetDisplay(m_pimgOut);
		m_Cannon.RenderToDisplay();
	}

	return S_OK;	
}

BOOL CAntiAircraft3::Update(void)
{
	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		iStepUpdate=0;
	    
		if (lTargetX+10>m_Base.x)
		{
			m_Cannon.SetLambda(m_Cannon.GetLambda()+15);

		}
		else if (lTargetX-10<m_Base.x)
		{
			m_Cannon.SetLambda(m_Cannon.GetLambda()-15);
		}

        //aggiorna lo stato di danneggiamento
		if (UpdateWoundedState())
		{
			m_Base.SetCurFrame(this->GetCurFrame());
		}

	}

	if (m_Base.GetParentComponent())
	{
		x=m_Base.x;
		y=m_Base.y;	
	}

	else
	{
		//la torretta � vincolata a terra aggiorna la posizione come se fose un ground object        
		if (lCurX>0)
		{
			x-=lGroundX-lCurX; //moto di trascinamento dovuto allo scrolling
		}

		lCurX=lGroundX; //lGroundX � la x del terreno corrente impostata da RefreshObjects

		m_Base.SetPosition(x,y);
		m_Base.UpdatePosition();

	}


	if (!GetWoundedState())
	{

		//funzione di fuoco
		iStepFire++;

		if (iStepFire==iFireFreq)
		{
			if (m_Cannon.m_iTag1>0 && m_iLeftCannon>0 && m_iRightCannon>0)
			{
				iStepFire=0; 					

				CShell *pShell;

				LONG xf,yf;
				int lambda,iframe;
				int ang;
				CDebris *pd;

				lambda=m_Cannon.GetTheta();
				//frame del proiettile (solo per proiettili direzionabili)
				iframe=lambda/15;

				m_Cannon.GetJointPosition(&xf,&yf,m_iLeftCannon);
				ang=g_fmath.GetAngle(xf,yf,lTargetX,lTargetY);



				//spara solo se il player � di fronte
				if (abs(ang-lambda)<30)
				{
				
					pShell=(CShell *)m_Cartridges->Pop();
					if (pShell)
					{		
					
						//proiettile cannone sinistro
						g_cs.PlaySound(g_snBfire,0,0);																						

						if (pShell->GetMaxFramesIndex()<iframe) pShell->SetCurFrame(pShell->GetMaxFramesIndex());
						else pShell->SetCurFrame(iframe);
										
						pShell->SetJointPosition(xf,yf,0);
						pShell->SetVelocity(lambda,7),
						pShell->dwClass=CL_ENFIRE;
						pShell->SetEnergy(1);
						pShell->dwPower=8;
						m_ActiveStack->Push(pShell);

						//imette il fumo nello stack
						if (m_pSmoke)
						{
						   //immette una nuvoletta di fumo dopo aver sparato
						   pd=(CDebris *)m_pSmoke->Pop();
						   if (pd)
						   {
							   //coordinate assolute della bocca del cannone					  
							   pd->Reset();
							   pd->SetG(-0.3f);
							   pd->SetPosition(xf,yf-5);
							   pd->SetMaxFrames(4); //termina alla frame 4
							   pd->SetVelocity(lambda,4);					  
							   m_ActiveStack->Push(pd);    					 			   

						   }
						}

							
						
					}

					pShell=(CShell *)m_Cartridges->Pop();
					if (pShell)
					{				
						
						m_Cannon.GetJointPosition(&xf,&yf,m_iRightCannon);
							
						if (pShell->GetMaxFramesIndex()<lambda) pShell->SetCurFrame(pShell->GetMaxFramesIndex());
						else pShell->SetCurFrame(iframe);
										
						pShell->SetJointPosition(xf,yf,0);
						pShell->SetVelocity(lambda,7),
						pShell->dwClass=CL_ENFIRE;
						pShell->SetEnergy(1);
						pShell->dwPower=8;
						m_ActiveStack->Push(pShell);

						//imette il fumo nello stack
						if (m_pSmoke)
						{
						   //immette una nuvoletta di fumo dopo aver sparato
						   pd=(CDebris *)m_pSmoke->Pop();
						   if (pd)
						   {
							   //coordinate assolute della bocca del cannone					  
							   pd->Reset();
							   pd->SetG(-0.3f);
							   pd->SetPosition(xf,yf-5);
							   pd->SetMaxFrames(4); //termina alla frame 4
							   pd->SetVelocity(lambda,4);					  
							   m_ActiveStack->Push(pd);    					 			   

						   }
						}
						

					}//fine pShell		

				}//fine controllo sull'angolo


			} //m_Cannon.m_iTag1;

		}//step fire

	}
	

	return (dwEnergy>0 && x>-200 && x<SCREEN_WIDTH+300);

}