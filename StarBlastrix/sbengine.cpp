//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////

    Starblastrix - side scrolling shoot'em up
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

sbengine.cpp

Engine principale del gioco

///////////////////////////////////////////////////////////////////////*/


#include <tchar.h>
#include "a32object.h"
#include "a32debug.h"
#include "level.h"    //gestione livelli di gioco
#include "enemy.h"
#include "sbl.h" //header che contiene le strutture necessarie a caricare il livello da file binario sbl
#include "sbengine.h"
#include "boss1.h"
#include "snakeres.h"
#include "spider.h"
#include "resman.h" //resource manager
#include "thunder.h"
#include "antia3.h"
#include "gfighter.h"
#include "blaster2.h"
#include "a32util.h"
#include "modenemy.h"
#include "jltent.h"
#include "asteroid.h"
#include "starfield.h"
#include "grndauxen.h"
#include "train.h"
#include "ring.h"
#include "mammoth.h"
#include "lifter.h"
#include "bleezer.h"

#ifdef _DEBUG_
#include <crtdbg.h>
#endif

//variabili definite in sblastrix.cpp
//external variables that are declared in sblastrix.cpp
extern HWND g_hwnd;
extern CADXGraphicManager g_gm;  //gestore grafica direct draw
extern CADXFrameManager g_fm;    //gestore immagini e sprite 
extern CADXSound g_cs;           //gestore effetti sonori
extern CADXMusic g_cm;           //gestore file MIDI
extern CADXInput g_ci;           //gestione device di input
extern CADXCharSet g_chLittleBlue,g_chLittleRed; //caratteri piccoli blu e rossi
extern CADXCharSet g_chRed,g_chBlue; //caratteri rossi grandi
extern CADXFastMath g_fmath; //engine matematico inizializzato da InitGame
extern void DrawBackMenu(IMAGE_FRAME_PTR img);
extern CADXDebug g_cdb;

#ifdef _TESTGAME_

extern CADXDebug g_cdb;

#endif

extern TP_ENVIRON g_Environ; 
extern void CloseForm (void *pform);    
extern int MsgOut(UINT,TCHAR *,TCHAR *,...); //funzione di output messaggi 
extern IMAGE_FRAME imgBackMenu; //immagine back ground
extern CSbForm *m_pActiveForm; //form attiva
extern void ConsoleLoop(void); //funzione x console
extern int m_GameStatus; //stato del gioco
extern void *m_label_hash;
extern TCHAR* g_szLoadCustomLevel; //punta al nome del livello "custom" da caricare

/////////////////////////////// VAR. GLOBALI ////////////////////////////////////////////////

#ifdef _TRACE_

BOOL g_bTraceOn=FALSE;

#endif

DWORD g_dwCurTime; //tempo corrente in millisecondi (usato per la sincronizzazione delle animazioni)

IMAGE_FRAME_PTR imgScreen; //front buffer e back buffers

DWORD g_dwFrameCnt; //contatore frame (serve a determinare fps)

BOOL g_bDebug1=FALSE;



//sprites :

//proiettili e bombe
//bullets and bombs
IMAGE_FRAME p1,p2,p3,p4,p5,p6,p7,p8,e1[5],p9[24],p17[12],pl[13],p10,p11,b1,b2,b3,
            b4[24],b5,p14[9],p12[12],p13[12],p15,p16,al,b6[24],p18[5],p19,p20[4],p21,p22,p23[13],p24,p25,p26;

IMAGE_FRAME es_big[15],es_medium[17],es_small[7],ple[9],bonus1[9],bonus2[9],bonus3[9],bonus4[9],bonus5[9],b7[24],b8[24],b9[24];	         

IMAGE_FRAME m_imgFriend,m_imgFriendCannon; //friendly fire   

IMAGE_FRAME m_electric_barrier[4]; //schermo elettrico del player

LONG g_lLimit=0; //limite del livello corrente
 
//tool per barriera protettiva
//shield sprite
IMAGE_FRAME m_imgShield[18];
//fiamma posteriore ugello del player
//the flame of the nozzle
IMAGE_FRAME m_imgBackFlame[4]; 
//fiamme ugello grande
IMAGE_FRAME m_imgNozzleFlame[3];
//fiamme ugello verticale
IMAGE_FRAME m_imgNozzleFlameV[3];

//fuoco ausiliario 
//aux fire
IMAGE_FRAME m_imgGyroFrame[12]; //frame rotante

IMAGE_FRAME m_imgGyroCannon[9]; //cannone

//IMAGE_FRAME g_imgBackGrounds[MAX_SCROLL_BACKGROUNDS]; 
//simbolo delle vite nella barra dei punteggi
//the lives symbol in the status bar
IMAGE_FRAME g_imgLabelShip; 

IMAGE_FRAME m_imgBonusCapt[11]; //bonus (descrizioni)
//detriti 
//debris
IMAGE_FRAME m_imgDebris1[6],m_imgDebris2[6],m_imgDebris3[5],m_imgDebris4,m_imgDebris5,m_imgDebris6[8],m_imgDebris7[6];
//fumo
//smoke images
IMAGE_FRAME m_imgSmoke[9];
//fumo bianco spray piccolo
IMAGE_FRAME m_imgWhiteSmoke[5];
//buco nel ghiaccio
IMAGE_FRAME m_imgIceHole; 

//barra di stato (energia vite ecc...)
//status bar
IMAGE_FRAME m_imgStatusBar;
//nemici:
//enemies:

//immagine background dello schema
IMAGE_FRAME m_imgBackGround;

LONG m_lBackImageTop=0; //ordinata di output immagine di background

RECT m_rcBack;

//disco volante
//ufo
IMAGE_FRAME m_imgUfo[9];

IMAGE_FRAME m_gf[10];//golden fighter 

IMAGE_FRAME m_gaf[4];//contraerea 1 - antiaircraft #1

IMAGE_FRAME m_gaf2[3]; //torretta - turret

IMAGE_FRAME m_imgRadar[12]; //radar nemico - enemy radar

IMAGE_FRAME m_imgSnakeHead[36];//testa del serpente - head of the snake
IMAGE_FRAME m_imgSnakeBody[36];//corpo del serpente - an element of the snake body
IMAGE_FRAME m_imgXWing[9];//X-Wing
IMAGE_FRAME m_gf1[9];//Golden Fighter1
IMAGE_FRAME m_imgWake[5]; //scia - wake
IMAGE_FRAME m_imgBlaster[11]; //Blaster
IMAGE_FRAME m_imgMFighter[9]; //M fighter
IMAGE_FRAME m_imgHFighter[9]; //H Fighter
IMAGE_FRAME m_imgLambda[9]; //Lambda Wing
IMAGE_FRAME m_imgUfo2[6]; //ufo2
IMAGE_FRAME m_imgRotCannon[13]; //canna del cannone per torrette a terra - ground cannon
IMAGE_FRAME m_imgAircraftCarrier; //porta aerei - aircraft carrier
IMAGE_FRAME m_imgRotCannon2[24]; //cannoncino rotante 2 - rotating cannon #2
IMAGE_FRAME m_imgShip[2]; //incrociatore 
IMAGE_FRAME m_imgTank1[6]; //cingolo carro armato - tank's crawler track  
IMAGE_FRAME m_imgTank2[3]; //carro armato - tank
IMAGE_FRAME m_imgFire[8]; //fuoco - flames
IMAGE_FRAME m_imgThunderbolt[14]; //fulminizzo! - thunderbolt
IMAGE_FRAME m_imgBlackSmoke[6];
IMAGE_FRAME m_imgSplash[9]; //splash
IMAGE_FRAME m_imgGrndImpact[7]; //impatto su terreno solido - ground explosion
IMAGE_FRAME m_imgWpIcons[11]; //icone armi speciali - icons of the special weapons
IMAGE_FRAME m_imgWpFrame; //indicatore arma speciale (frame esterna) - 
IMAGE_FRAME m_imgWpLevel; //indicatore livello arma speciale
IMAGE_FRAME m_imgRail; //ferrovia
IMAGE_FRAME m_imgShanty1[3]; //capanna tetto verde mimetico
IMAGE_FRAME m_imgShanty2[3]; //capanna gialla
IMAGE_FRAME m_imgShanty3[3]; //capanna piccola verticale
IMAGE_FRAME m_imgShanty4[3]; //capannone
IMAGE_FRAME m_imgIceberg[3]; //iceberg
IMAGE_FRAME m_imgWall[2]; //muro ad angolo
IMAGE_FRAME m_imgFactory[2]; 
IMAGE_FRAME m_imgStreet[2];
IMAGE_FRAME m_imgPalmThicket[2]; 
IMAGE_FRAME m_imgPylon[3];
IMAGE_FRAME m_imgBuilding[4][3]; //palazzi
IMAGE_FRAME m_imgScrewFighter[12]; //caccia rotante verde
IMAGE_FRAME m_imgUfo3[6]; //ufo piccolo rotante
IMAGE_FRAME m_imgLFighter; //l fighter
IMAGE_FRAME m_imgNightBuilding[4][3];
IMAGE_FRAME m_imgNightBridge[2];
IMAGE_FRAME m_imgBlaster2; //ugelli verdi
BOUND_POLY  m_bpBlaster2;
IMAGE_FRAME m_imgBlaster3; //"ponte"

//livello corrente - current level
CSbLevel CCurLevel; 

//sequenza di esecuzione dei livelli
//il giocatore puo' scegliere se iniziare dal 1 2 o 3 , la sequenza viene adattata di conseguenza
int g_iLevelSequence[]={1,2,3,4,5,6,7,8};

//UINT *g_iLayerMap[MAX_SCROLL_LAYERS];

DWORD g_tm=0;

//Giocatore - player
CPlayer Player1; 

RECT g_rcFill,g_rcBar,g_rcShip,g_rcEnergy[10];

DDBLTFX g_ddbltfx,g_blxEn,g_blxEn1; 
//input del giocatore - player's input
PL_INPUT g_plInput; 
//Path di movimento per i nemici - the paths of some enemies
TP_PATH_DATA *g_Path1=NULL,*g_Path2=NULL,*g_Path3=NULL,*g_Path4=NULL,*g_Path5=NULL,*g_Path6=NULL,*g_Path7=NULL,*g_Path8=NULL,*g_Path9=NULL,*g_Path10=NULL;
TP_PATH_DATA *g_SnakePath1=NULL,*g_SnakePath2=NULL,*g_SnakePath3;

LPDIRECTDRAWSURFACE7 g_lpStatusBar; //sup. della status bar - status bar surface

DDBLTFX g_dbxSb; //ddblt fx status bar 

TCHAR g_szLevelName[30]; //stringa nome livello

CDynString g_LevelMessage; //titolo del livello 
CDynString g_LevelName; //nome livello (livello 1,livello 2 ecc...)

CObjectStack g_objStack; //stack che contiene gli oggetti attivi del gioco (STACK PRINCIPALE)

int g_iContinue=0; //indica quante volte si puo' continuare all'interno di ogni schema

//armi-------------------------------------------------------------------------------------

CObjectStack g_sShell;   //stack che contiene i proiettili (tipo 1)
CObjectStack g_sShell1;  //tipo 2
CObjectStack g_sShell2;  //tipo 3
CObjectStack g_sShell3;  //proiettili gialli orizzontali
CObjectStack g_sShell4;  //proiettili rossi orizzontali
CObjectStack g_sEShell1; //pr. nemici tipo 1
CObjectStack g_sEShell2; //pr. gialli ellittici direzionabili
CObjectStack g_sEShell3; //proiettile sferico
CObjectStack g_sEShell4; //proiettile rosso
CObjectStack g_sEShell5; //proiettile piccolo turchese
CObjectStack g_sEShell6; //prociettile rotondo piccolo giallo
CObjectStack g_sEShell7; //proiettile sferico
CObjectStack g_sEShell8; //falce a sx
CObjectStack g_sEShell9; //falce a dx
CObjectStack g_sEShell10; //fiamma
CObjectStack g_sEShell11; //proiettile laser rosso piccolo (direzionabile)
CObjectStack g_sEShell12; //fuoco fatuo
CObjectStack g_sShellBlue; //proiettile laser blu
CObjectStack g_sFwBullet; //proiettili sferici inseguitori
CObjectStack g_sDBomb1;  //collezione di drop bomb 1
CObjectStack g_sDBomb2;  //bomba 2  
CObjectStack g_sDBomb3;  //bomba 3
CObjectStack g_sSMissile; //missile stright (va dritto sempre nella stessa direzione)
CObjectStack g_sRMissile; //missile da sinistra a dx
CObjectStack g_sFwMissile1; //missile inseguitore 1
CObjectStack g_sThunderBolt; //fulmine
CObjectStack g_sLaser; //laser

//bonus ---------------------------------------------------------------------------------

CObjectStack g_sBonusGreen; //power up arma primaria
CObjectStack g_sBonusRed; //special
CObjectStack g_sBonusBlue; //+barriera
CObjectStack g_sBonusYellow; //+energia
CObjectStack g_sBonusOrange; //power up arma secondaria
CObjectStack g_sBonusBombs10; //+10 bombe
CObjectStack g_sBonusBombs50; //+50 bombe
CObjectStack g_sBonusScore; //punti++
CObjectStack g_sBonusThrust; //spinta
CObjectStack g_sBonusBooster; //booster (accelera lo scrolling)
CObjectStack g_sBonusThunder; //arma speciale thunder
CObjectStack g_sBonusMissile; //missili
CObjectStack g_sBonusLaser; //laser
CObjectStack g_sBonusWpA; //special weapon A
CObjectStack g_sBonusWpB; //special weapon B
CObjectStack g_sBonusWpC; //special weapon C
CObjectStack g_sBonusWpD; //special weapon C
CObjectStack g_sBonus1Up; //bonus +1 vita
CObjectStack g_sBooster; //booster: rampa di accelerazione
CObjectStack g_sBonusGrndFire; //fuoco verso terra
CObjectStack g_sBonusTripleFire; //triplo fuoco
CObjectStack g_sBonusStrightMissile; //missile a dritto
CObjectStack g_sBonusElectricBarrier; //barriera elettrica
CObjectStack g_sBonusBlueShell; //proiettile laser blue
CObjectStack g_sBonusFwShells; //proiettili insegiutori
CObjectStack g_sBonusLaser3;    //tre p laser

//numero di tipi diversi di bonus (nota si somma trenta perh� i tag EB_BONUS insieme ai tag EN_.. devono essere univoci
#define MAX_BONUS 30+EN_LAST   


CObjectStack *g_pBonus[MAX_BONUS];

//nemici-------------------------------------------------------------------------------------

CObjectStack g_sUfo; //ufo
CObjectStack g_sGoldenFighter; //golden fighter
CObjectStack g_sAntiAircraft; //contraerea
CObjectStack g_sAntiAircraft2; //seconda contraerea
CObjectStack g_sAntiAircraft3; //terzo tipo di contraerea
CObjectStack g_sRadar; //radar nemico
CObjectStack g_sSnake;//serpente
CObjectStack g_sSnakeRed; //serpe rossa
CObjectStack g_sSnakeRed1; //serpe rossa lungh. media
CObjectStack g_sSnakeRed2; //serpe rossa molto lunga
CObjectStack g_sSnakeBody;//corpo serpente
CObjectStack g_sXWing;//X-Wing
CObjectStack g_sLambda;//lambda fighter
CObjectStack g_sMFighter; //M Fighter
CObjectStack g_sAircraftCarrier; //porta aerei
CObjectStack g_sTank; //carro armato
CObjectStack g_sShip; //incrociatore
CObjectStack g_sUfo2; //ufo 2 algo 1
CObjectStack g_sUfo3;
CObjectStack g_sLFighter; //L fighter
CObjectStack g_sScrewFighter;
CObjectStack g_sBlaster;    //blaster
CObjectStack g_sShanty1;  //baracca tetto mimetico
CObjectStack g_sShanty2;  //baracca gialla orizzontale
CObjectStack g_sShanty3;
CObjectStack g_sShanty4;  //capannone
CObjectStack g_sBuilding1; //palazzi
CObjectStack g_sBuilding2;
CObjectStack g_sBuilding3;
CObjectStack g_sBuilding4;
CObjectStack g_sNightBuilding1;
CObjectStack g_sNightBuilding2;
CObjectStack g_sNightBuilding3;
CObjectStack g_sNightBuilding4;
CObjectStack g_sNightBridge;
CObjectStack g_sIceberg; //iceberg
CObjectStack g_sFactory;    //fabbrica
CObjectStack g_sPylon;    //traliccio
CObjectStack g_sStreet;   //strada dritta orizzontale
CObjectStack g_sPalmThicket; //palme
CObjectStack g_sWall;        //muro ad angolo
CObjectStack g_sBoss1;
CObjectStack g_sBoss2;
CObjectStack g_sBoss3;
CObjectStack g_sBoss4;
CObjectStack g_sBoss5;
CObjectStack g_sBlaster2;
CObjectStack g_sBlaster3;
CObjectStack g_sSpider1;
CObjectStack g_sSpider2;
CObjectStack g_sSpider3;
CObjectStack g_sGFighter;
CObjectStack g_sFlashBomb; 
CObjectStack g_sJellyShip;
CObjectStack g_sAsteroidSmall;
CObjectStack g_sAsteroidMedium;
CObjectStack g_sAsteroidBig;
CObjectStack g_sShieldBase; //base con cupola che si apre e chuide
CObjectStack g_sShellBase;
CObjectStack g_sTurret; //torretta oro
CObjectStack g_sTrain; //treno
CObjectStack g_sRail; //ferrovia
CObjectStack g_sSpaceBuild1; //arco
CObjectStack g_sSpaceBuild2; //silos giallo orizz.
CObjectStack g_sSpaceBuild3; //triplo silos
CObjectStack g_sSpaceBuild4; //silos orizz. con tubi
CObjectStack g_sHTruss;
CObjectStack g_sVTruss;
CObjectStack g_sRing;
CObjectStack g_sMammoth;
CObjectStack g_sLifter;
CObjectStack g_sBleezer;
CObjectStack g_sHFighter;

#define MAX_MOD_ENEMIES 8
//navi nemiche composte da piu' parti collegate insieme
CObjectStack g_sModularEnemy[MAX_MOD_ENEMIES];

#define MAX_ENEMIES 100   //numero di tipi di nemici

CObjectStack *g_pEnemy[MAX_ENEMIES];

//esplosioni -------------------------------------------------------------------------------

CObjectStack g_sExplBig,g_sExplMediumHit,g_sExplMedium,g_sExplSmall;

//oggetti vari-----------------------------------------------------------------------------

CObjectStack g_sLabelPowerUp; //label power up
CObjectStack g_sLabelShield; //label shield
CObjectStack g_sLabelSpecial; //label special
CObjectStack g_sLabel1Up; //label 1UP
CObjectStack g_sLabelEnergy;
CObjectStack g_sLabelThrust;
CObjectStack g_sLabelScore;
CObjectStack g_sLabelBombs10;
CObjectStack g_sLabelBombs50;
CObjectStack g_sGameOver; //stack di oggetti "game over"
CObjectStack g_sExpBig; //esplosione piccola
CObjectStack g_sExpMediumHit; //esplosione media (solo le prime frames)
CObjectStack g_sExpMedium; //esplosione grande (tutte le frames)
CObjectStack g_sExpSmall; //esplosione piccola (quando un proiettile esplode su un oggetto)
CObjectStack g_sDebris1; //detriti dovuti ll'esplosione
CObjectStack g_sDebris2;
CObjectStack g_sDebris3;
CObjectStack g_sDebris4;
CObjectStack g_sDebris5;
CObjectStack g_sDebris6;
CObjectStack g_sDebris7;
CObjectStack g_sSplash; //effetto causato da un proiettile che entra in acqua
CObjectStack g_sIceHole; //buco nel ghiaccio
CObjectStack g_sGrndImpact;
CObjectStack g_sSmoke; //fumo
CObjectStack g_sBlackSmoke; //fumo nero
CObjectStack g_sWhiteSmoke; //fumo bianco
CObjectStack g_sSmokeSrc; //sorgente di fumo
CObjectStack g_sFire; //fuoco
CObjectStack g_sShield; //barriera protettiva
CObjectStack g_sElectricBarrier; //barriera elettrica protettiva
CObjectStack g_sGyros; //fuoco amico 
CObjectStack g_sMechArm; //braccio meccanico N� 1
CObjectStack g_sNozzleFlames; //fiamme ugello grande (orizzontali)
CObjectStack g_sNozzleFlamesV;
CADXSprite g_sprNozzleFlames; //fiamme ugello del player
//------------------------------------------------------------------------------------

CStarField *g_pStarField=NULL;

struct 
{
	int ivalue;
	char *svalue1; //nome IT
	char *svalue2; //nome EN
}

m_enemy_names[]=
{

	EN_UFO,"UFO","UFO",
    EN_GOLDENFIGHTER,"CACCIA ORO","GOLDEN FIGHTER",
	EN_ANTIAIRCRAFT,"CONTRAEREA","ANTI AIRCRAFT",
    EN_ANTIAIRCRAFT2,"CONTRAEREA II","ANTI AIRCRAFT II",
	EN_RADAR,"RADAR","RADAR",
	EN_SNAKE,"SERPENTE","SNAKE",
	EN_XWING,"XWING","XWING",
	EN_LAMBDAFIGHTER,"CACCIA LAMBDA","LAMBDA FIGHTER",
	EN_MFIGHTER,"CACCIA M2","M2 FIGHTER",
	EN_AIRCRAFTCARRIER,"PORTAEREI","AIRCRAFT CARRIER",
	EN_TANK,"CARRO ARMATO","EN_TANK",
	EN_SHIP,"INCROCIATORE","EN_SHIP",
	EN_UFO2,"UFO II","UFO II",
	EN_BLASTER,"NAVETTA BLASTER","BLASTER SHIP",
	EB_POWERUP1,"POWERUP1","POWERUP1",
	EB_SPECIAL,"SPECIAL","SPECIAL",
	EB_SHIELD,"BARRIERA","SHIELD",
	EB_ENERGY,"ENERGIA","ENERGY",
	EB_POWERUP2,"POWERUP2","POWERUP2",
	EB_BOMBS10,"10 BOMBE","10 BOMBS",
	EB_BOMBS50,"50 BOMBE","50 BOMBS",
	EB_SCORE,"EXTRA PUNTI","EXTRA SCORE",
	EB_THRUST,"THRUST","THRUST",
	EB_BOOSTER,"BOOSTER","BOOSTER",
	EB_THUNDER,"FULMINIZZO","THUNDER",
	EB_MISSILE,"MISSILE","MISSILE",
	EB_LASER,"LASER","LASER",
	EB_WEAPONA,"ARMA A","WEAPON A",
	EB_WEAPONB,"ARMA B","WEAPON B",
	EB_WEAPONC,"ARMA C","WEAPON C",
	EB_WEAPOND,"ARMA D","WEAPON D",
	EB_GYROS,"GIROSCOPIO","GYROS",
	EB_1UP,"VITA","1UP",
	EN_SHANTY1,"BARACCA 1","SHANTY 1",
	EN_SHANTY2,"BARACCA 2","SHANTY 2",
	EN_SHANTY3,"BARACCA 3","SHANTY 3",
	EN_SHANTY4,"BARACCA 4","SHANTY 4",
	EN_FACTORY,"CENTRALE ELETTRICA","POWER PLANT",
	EN_PYLON,"PILONE","PYLON",
	EN_STREET,"STRADA","STREET",
	EN_PALMS,"PALMETO","PALMS",
	EN_WALL,"MURO","WALL",
	EN_BOSS1,"MOSTRO 1","BOSS 1",
	EN_BOSS2,"MOSTRO 2","BOSS 2",
	EN_BOSS3,"MOSTRO 3","BOSS 3",
	EN_BOSS4,"MOSTRO 4","BOSS 4",
	EN_BOSS5,"MOSTRO 5","BOSS 5",
	EN_SNAKERED,"SERPENTE ROSSO","RED SNAKE",
	EN_SPIDER1,"RAGNO 1","SPIDER 1",
	EN_SPIDER2,"RAGNO 2","SPIDER 2",
	EN_SPIDER3,"RAGNO 3","SPIDER 3",
	EN_ANTIAIRCRAFT3,"CONTRAEREA III","ANTIAIRCRAFT III",
	EN_GFIGHTER,"CACCIA GURO","GURO FIGHTER",
	EB_GRND_FIRE,"EB_GRND_FIRE","EB_GRND_FIRE", //fuoco verso terra
	EB_TRIPLE_FIRE,"EB_TRPLE_FIRE","EB_TRPLE_FIRE", //fuoco triplo
	EB_STRIGHT_MISSILE,"EB_STRIGHT_MISSILE","EB_STRIGHT_MISSILE", //missile
	EN_SCREW_FIGHTER,"SCREW FIGHTER","SCREW FIGHTER",
	EN_NIGHT_BUILDING1,"EDIFICIO 1","BUILDING 1",
	EN_NIGHT_BUILDING2,"EDIFICIO 2","BUILDING 2",
	EN_NIGHT_BUILDING3,"EDIFICIO 3","BUILDING 3",
	EN_NIGHT_BUILDING4,"EDIFICIO 4","BUILDING 4",
	EN_ICEBERG,"ICEBERG","ICEBERG",
	EN_UFO3,"UFO 3","UFO 3",
	EN_LFIGHTER,"L-FIGHTER","L-FIGHTER",
	EN_BLASTER2,"BLASTER 2","BLASTER 2",
	EN_BLASTER3,"BLASTER 3","BLASTER 3",
	EN_MODULAR1,"ASTRONAVE NEMICA 1","ENEMY SPACESHIP 1",
	EN_MODULAR2,"ASTRONAVE NEMICA 2","ENEMY SPACESHIP 2",
	EN_MODULAR3,"ASTRONAVE NEMICA 3","ENEMY SPACESHIP 3",
	EN_MODULAR4,"ASTRONAVE NEMICA 4","ENEMY SPACESHIP 4",
	EN_MODULAR5,"ASTRONAVE NEMICA 5","ENEMY SPACESHIP 5",
	EN_MODULAR6,"ASTRONAVE NEMICA 6","ENEMY SPACESHIP 6",
	EN_MODULAR7,"ASTRONAVE NEMICA 7","ENEMY SPACESHIP 7",
	EN_MODULAR8,"ASTRONAVE NEMICA 8","ENEMY SPACESHIP 8",
	EN_SNAKERED1,"VIPERA ROSSA","RED VIPER",
	EN_SNAKERED2,"VIPERA ROSSA 2","RED VIPER 2",
	EN_JELLYSHIP,"MEDUSA","JELLY SHIP",
	EN_ASTEROID_SMALL,"ASTEROIDE PICCOLO","SMALL SIZE ASTEROID",
	EN_ASTEROID_MEDIUM,"ASTERIODE MEDIO","MEDIUM SIZE ASTEROID",
	EN_ASTEROID_BIG,"ASTEROIDE GRANDE","BIG SIZE ASTEROID",
	EN_SHIELD_BASE,"BASE ALFA","ALPHA BASE",
	EN_SHELL_BASE,"BASE BETA","BETA BASE",
	EN_TURRET,"TORRETTA LASER","LASER TURRET",
	EN_TRAIN,"TRENO","TRAIN",
	EN_RAIL,"EN_RAIL","FERROVIA",
	EN_SPACE_BUILD1,"IMPIANTO 1","PLANT 1",
	EN_SPACE_BUILD2,"IMPIANTO 2","PLANT 2",
	EN_SPACE_BUILD3,"IMPIANTO 3","PLANT 3",
	EN_SPACE_BUILD4,"IMPIANTO 4","PLANT 4",
	EN_HTRUSS,"STRUTTURA","STRUCTURE",
	EN_VTRUSS,"STRUTTURA","STRUCTURE",
	EN_RING,"ANELLO","RING",
	EN_MAMMOTH,"MAMMOTH","MAMMOTH",
	EN_LIFTER,"LIFTER","LIFTER",
	EN_BLEEZER,"BLEEZER","BLEEZER",
	EN_HFIGHTER,"CACCIA TIPO H","H FIGHTER"
};

//----------------------------------------------------------------------

//vettore che contiene le corrispondenze fra i tipi di classi degli oggetti (propriet� dwClass)
DWORD g_dwClMatch[33]; 
//frame che viene visualizzata quando si vuol interrompere il gioco
CSbForm m_frmExit;
//mesaggio che chiede di proseguire il gioco 
CSbForm m_frmContinue;

//controlli per la form AskForExit che chiede di uscire dal gioco
CSbStaticText mnuExitYes;
CSbStaticText mnuExitNo;
CSbStaticText mnuExitTitle;
//controlli form continue
CSbStaticText mnuContinueYes;
CSbStaticText mnuContinueNo;
CSbStaticText mnuContinueTitle;

//cursori della form m_frmExit
CADXSprite g_Cursor;

IMAGE_FRAME m_imgCursor; //cursore

IMAGE_FRAME m_imgCurScreen; //usata per catturare lo schermo
//buffer general purpose
WORD *g_wBuffer=NULL; 

//rettangolo dello schermo
RECT g_rcScreen;

LONG g_pbx,g_pby,g_pbw=200; //progress bar

//suoni
LONG g_snBonus=0;LONG g_snDrop=0;LONG g_snExplo1=0;LONG g_snExplo2=0;LONG g_snExplo3=0;LONG g_snExplo4=0;LONG g_snFire1=0;LONG g_snFire2=0;LONG g_snFire3=0;
LONG g_snFire4=0;LONG g_snFire5=0;LONG g_snFire6=0;LONG g_snFire7=0;LONG g_snFire8=0;LONG g_snFire9=0;LONG g_snFire10=0;LONG g_snFire11=0;LONG g_snFire12=0;LONG g_snFire13=0;
LONG g_snHit1=0,g_snBfire=0,g_snMagnetic,g_snFire14,g_snMissile=0,g_snFwMissile=0,g_snFFlames=0,g_snSunk=0,g_snUfoFire=0,g_snFinal=0,g_snBlip=0,g_snElectro3=0;
LONG g_snRadar=0,g_snBooster=0,g_snNozzle=0,g_snMech1=0,g_snMech2=0,g_snCrash=0;
LONG g_snPlLaser=0;
LONG g_snBleep1,g_snBleep2;
LONG g_snFire1_1=0,g_snFire1_2;

//level editor

LONG g_dwLayerEditor=0; //layer che si sta editando (modalit� level editor)

LONG g_lCurStackIndex=0; //indice dello stack corrente del piazzamento degli oggetti nel level editor

CSBObject *g_pobjEditor=NULL; //oggetto selezionato corrente nel level editor

BOOL g_bEditorSelMode=TRUE; //indica se siamo in modalit� di selezione nell'editor

SB_TRIGGER_PTR g_SelectedTrigger=NULL; //trigger oggetto corrente selezionato

CResourceManager g_rm;


//--------------------------------- funzioni modulo sbengine --------------------



//---------------------------- ScreenTransition ----------------------------------

void ScreenTransition(IMAGE_FRAME_PTR imgOut,IMAGE_FRAME_PTR imgIn)
{
	RECT rc;

	g_gm.UseScreen(1); //back buffer
	g_gm.Cls(0);
	if (imgIn) g_fm.PutImgFrame(0,0,imgIn);
	if (!imgOut) return;
	if (!imgOut->status) return;

	LONG w=imgOut->width;
	LONG h=imgOut->height;
	LONG xi,yi;
	const LONG dx=(LONG)(w/22);
	const LONG dy=(LONG)(h/22);
	LONG w2=(LONG)(w/2);
	LONG h2=(LONG)(h/2);
	LONG sx,sy;
	IMAGE_FRAME_PTR pimgDispl;
	float f=1.0;
	float fd=1.0;

	for (int i=0;i<35;i++)
	{		
		g_gm.Cls(0);
		if (imgIn) 
		{
			g_fm.PutImgFrame(0,0,imgIn);	
		}
		else g_gm.Cls(0);

		pimgDispl=g_gm.GetScreenImage(1);
		
		f+=0.06f;

		fd-=0.02f;

		if (fd>0)
		{
			for (xi=0;xi<w;xi+=dx)
			{
				for (yi=0;yi<h;yi+=dy)
				{
					rc.left=xi;
					rc.top=yi;
					rc.right=xi+(LONG)(dx*fd);
					rc.bottom=yi+(LONG)(dy*fd);		
					
					sx=(LONG)(w2+(xi-w2)*f);
					sy=(LONG)(h2+(yi-h2)*f);
					
					g_fm.PutImgFrameRect(sx,sy,pimgDispl,imgOut,&rc);

				}
			}
		}

		else break;

		g_gm.FlipScreen(1);		
	}	

}

//acquisisce gli sprites principali del gioco
HRESULT GrabSprites(void)
{

IMAGE_FRAME imgSrc;
IMAGE_FRAME imgTemp;
HRESULT hr;
DWORD dwCount;
RECT rc;
int iAngle;

//sprites ausiliari
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\spraux.vpx",0);

if (FAILED(hr)) return hr;
//cursore
hr=g_fm.GrabFrame(&m_imgCursor,52,66,103,185,&imgSrc);

if (FAILED(hr)) return hr;

hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data.vpx",0);

if (FAILED(hr)) return hr; //errore acquisendo gli sprites

//file data.vpx immagine 0

g_fm.SetDefaultCaps (DDSCAPS_OFFSCREENPLAIN | DDSCAPS_VIDEOMEMORY);

//Il seguente codice � generato da un programma apposito...
g_fm.GrabFrame(&p1,105,21,146,34,&imgSrc); // right proiettile tipo 3 rosa 
g_fm.GrabFrame(&p2,150,21,191,34,&imgSrc); // left
g_fm.GrabFrame(&p5,4,91,21,98,&imgSrc); // right proiettile tipo 1 rosa
g_fm.GrabFrame(&p6,24,91,41,98,&imgSrc); // left
g_fm.GrabFrame(&p7,45,91,71,101,&imgSrc); // right proiettile tipo 2 rosa
g_fm.GrabFrame(&p8,74,91,100,101,&imgSrc); // left
g_fm.GrabFrame(&e1[0],4,291,26,319,&imgSrc); // esplosione piccola
g_fm.GrabFrame(&e1[1],30,291,60,329,&imgSrc); // esplosione piccola
g_fm.GrabFrame(&e1[2],63,291,106,336,&imgSrc); // esplosione piccola
g_fm.GrabFrame(&e1[3],110,291,163,343,&imgSrc); // esplosione piccola
g_fm.GrabFrame(&e1[4],167,291,228,352,&imgSrc); // esplosione piccola
g_fm.GrabFrame(&p9[0],5,105,49,111,&imgSrc); // 'proiettile rosso 
g_fm.GrabVFrameArray(m_imgNozzleFlame,&imgSrc,3,1040,364,80,33); //fiamme ugello

for (dwCount=1;dwCount<3;dwCount++)
{
	g_fm.CreateRotFrame(90,&m_imgNozzleFlameV[dwCount],&m_imgNozzleFlame[dwCount],TRUE);
}
	
ProgressBar(g_pbx,g_pby,0.05f,g_pbw);

iAngle=0;

for (dwCount=1;dwCount<24;dwCount++)
{
	iAngle+=15;
	hr=g_fm.CreateRotFrame(iAngle,&p9[dwCount],&p9[0],TRUE);
	if (FAILED(hr)) return hr;

}


g_fm.GrabFrame(&imgTemp,2,360,138,444,&imgSrc); // player 
g_fm.CreateScaledFrame(80,&pl[0],&imgTemp);
g_fm.GrabFrame(&imgTemp,143,360,279,440,&imgSrc); // player 
g_fm.CreateScaledFrame(80,&pl[1],&imgTemp);
g_fm.GrabFrame(&imgTemp,283,360,418,436,&imgSrc); // player 
g_fm.CreateScaledFrame(80,&pl[2],&imgTemp);
g_fm.GrabFrame(&imgTemp,422,360,560,432,&imgSrc); // player 
g_fm.CreateScaledFrame(80,&pl[3],&imgTemp);
g_fm.GrabFrame(&imgTemp,565,360,701,426,&imgSrc); // player 
g_fm.CreateScaledFrame(80,&pl[4],&imgTemp);
g_fm.GrabFrame(&imgTemp,706,360,842,420,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[5],&imgTemp);
g_fm.GrabFrame(&imgTemp,846,360,981,416,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[6],&imgTemp);
g_fm.GrabFrame(&imgTemp,2,447,138,503,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[7],&imgTemp);
g_fm.GrabFrame(&imgTemp,142,447,279,509,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[8],&imgTemp);
g_fm.GrabFrame(&imgTemp,283,447,418,513,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[9],&imgTemp);
g_fm.GrabFrame(&imgTemp,421,447,557,517,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[10],&imgTemp);
g_fm.GrabFrame(&imgTemp,560,447,696,523,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[11],&imgTemp);
g_fm.GrabFrame(&imgTemp,699,447,832,526,&imgSrc); // player
g_fm.CreateScaledFrame(80,&pl[12],&imgTemp);
g_fm.GrabFrame(&p10,210,102,236,125,&imgSrc); // palla
g_fm.GrabFrame(&p11,240,110,305,129,&imgSrc); // falce sx
g_fm.GrabFrame(&p24,240,132,305,151,&imgSrc); // falce dx
//g_fm.GrabFrame(&p12,240,132,305,151,&imgSrc); // falce

ProgressBar(g_pbx,g_pby,0.08f,g_pbw);

g_fm.GrabFrame(&b1,214,159,306,191,&imgSrc); // bomba verde right
g_fm.GrabFrame(&b2,214,194,302,227,&imgSrc); // 
g_fm.GrabFrame(&b3,214,230,312,260,&imgSrc); // 
g_fm.GrabFrame(&b4[0],214,264,269,285,&imgSrc); // 0�
g_fm.GrabFrame(&b4[1],272,264,324,284,&imgSrc); // 15�
g_fm.GrabFrame(&b4[2],328,260,372,292,&imgSrc); // 30�
g_fm.GrabFrame(&b4[3],377,264,417,306,&imgSrc); // 45
g_fm.GrabFrame(&b4[4],421,260,448,308,&imgSrc); // 60�
g_fm.GrabFrame(&b4[5],451,264,471,314,&imgSrc); // 75
g_fm.GrabFrame(&b4[6],474,264,497,317,&imgSrc); // 90�
g_fm.GrabFrame(&b4[7],709,195,729,245,&imgSrc); // 105�
g_fm.GrabFrame(&b4[8],732,195,760,242,&imgSrc); // 120
g_fm.GrabFrame(&b4[9],763,195,803,237,&imgSrc); // 135
g_fm.GrabFrame(&b4[10],806,195,853,225,&imgSrc); // 150
g_fm.GrabFrame(&b4[11],856,195,908,215,&imgSrc); // 165
g_fm.GrabFrame(&b4[12],911,195,966,216,&imgSrc); // 180� (left)
g_fm.GrabFrame(&b4[13],422,321,474,341,&imgSrc); // 195�
g_fm.GrabFrame(&b4[14],372,311,419,341,&imgSrc); // 210�
g_fm.GrabFrame(&b4[15],329,299,369,341,&imgSrc); // 225�
g_fm.GrabFrame(&b4[16],298,297,326,344,&imgSrc); // 240�
g_fm.GrabFrame(&b4[17],275,291,295,341,&imgSrc); // 255�
g_fm.GrabFrame(&b4[18],249,288,272,341,&imgSrc); // 270�
g_fm.GrabFrame(&b4[19],918,238,938,288,&imgSrc); // 285�
g_fm.GrabFrame(&b4[40],887,241,915,288,&imgSrc); // 300�
g_fm.GrabFrame(&b4[21],844,246,884,288,&imgSrc); // 315�
g_fm.GrabFrame(&b4[22],794,258,841,288,&imgSrc); // 330�
g_fm.GrabFrame(&b4[23],739,268,791,288,&imgSrc); // 345�
g_fm.GrabFrame(&b5,528,159,560,242,&imgSrc); // bomba verticale
g_fm.GrabFrame(&p14[0],381,40,430,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[1],435,40,484,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[2],489,40,538,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[3],543,40,592,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[4],596,40,645,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[5],651,40,700,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[6],705,40,754,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[7],759,40,808,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p14[8],813,40,862,85,&imgSrc); // fuoco fatuo
g_fm.GrabFrame(&p12[0],331,126,360,133,&imgSrc); // 0�
g_fm.GrabFrame(&p12[1],416,150,444,162,&imgSrc); // 15�
g_fm.GrabFrame(&p12[2],387,150,413,169,&imgSrc); // 30�
g_fm.GrabFrame(&p12[3],363,150,384,171,&imgSrc); // 45�
g_fm.GrabFrame(&p12[4],343,150,360,175,&imgSrc); // 60�
g_fm.GrabFrame(&p12[5],326,150,339,179,&imgSrc); // 75�
g_fm.GrabFrame(&p12[6],484,126,491,156,&imgSrc); // 90�
g_fm.GrabFrame(&p12[7],468,126,481,155,&imgSrc); // 105�

ProgressBar(g_pbx,g_pby,0.1f,g_pbw);

g_fm.GrabFrame(&p12[8],447,126,464,151,&imgSrc); // 120�
g_fm.GrabFrame(&p12[9],423,126,444,147,&imgSrc); // 135�
g_fm.GrabFrame(&p12[10],394,126,420,145,&imgSrc); // 150�
g_fm.GrabFrame(&p12[11],363,126,391,138,&imgSrc); // 165�
g_fm.GrabFrame(&p13[0],326,183,346,188,&imgSrc); // 0� proiettile piccolo turchese
g_fm.GrabFrame(&p13[1],406,206,425,216,&imgSrc); // 360-15�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[2],384,206,403,217,&imgSrc); // 360-30�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[3],365,206,381,221,&imgSrc); // 360-45�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[4],348,206,361,224,&imgSrc); // 360-60�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[5],334,206,344,225,&imgSrc); // 360-75�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[6],326,206,331,226,&imgSrc); // 360-90�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[7],431,183,441,202,&imgSrc); // 75�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[8],414,183,427,201,&imgSrc); // 60�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[9],394,183,410,198,&imgSrc); // 45�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[10],372,183,391,194,&imgSrc); // 30�   proiettile piccolo turchese
g_fm.GrabFrame(&p13[11],350,183,369,193,&imgSrc); // 15�   proiettile piccolo turchese
g_fm.GrabFrame(&p15,480,101,494,113,&imgSrc); // palla piccola
g_fm.GrabFrame(&p16,456,183,474,190,&imgSrc); // left

ProgressBar(g_pbx,g_pby,0.12f,g_pbw);

g_fm.GrabFrame(&p17[0],210,91,237,99,&imgSrc); // pr giallo ellittico 0�
g_fm.GrabFrame(&p17[1],269,91,295,103,&imgSrc); // pr giallo ellittico 15�
g_fm.GrabFrame(&p17[2],325,91,349,107,&imgSrc); // pr giallo ellittico 30�
g_fm.GrabFrame(&p17[3],378,91,399,112,&imgSrc); // pr giallo ellittico 45�
g_fm.GrabFrame(&p17[4],422,91,439,116,&imgSrc); // pr giallo ellittico 60�
g_fm.GrabFrame(&p17[5],456,91,467,118,&imgSrc); // pr giallo ellittico 75�
g_fm.GrabFrame(&p17[6],470,91,477,119,&imgSrc); // pr giallo ellittico 90�
g_fm.GrabFrame(&p17[7],240,91,266,103,&imgSrc); // pr giallo ellittico -15�
g_fm.GrabFrame(&p17[8],298,91,322,107,&imgSrc); // pr giallo ellittico -30�
g_fm.GrabFrame(&p17[9],353,91,374,112,&imgSrc); // pr giallo ellittico -45�
g_fm.GrabFrame(&p17[10],402,91,419,116,&imgSrc); // pr giallo ellittico -60�
g_fm.GrabFrame(&p17[11],442,91,453,118,&imgSrc); // pr giallo ellittico -75�
g_fm.GrabFrame(&al,683,7,703,28,&imgSrc); // label a dell'indicatore arma corrente
g_fm.GrabFrame(&b6[0],683,143,714,156,&imgSrc); // 0�
g_fm.GrabFrame(&b6[1],717,141,748,156,&imgSrc); // 15�

ProgressBar(g_pbx,g_pby,0.15f,g_pbw);

g_fm.GrabFrame(&b6[2],751,138,778,156,&imgSrc); // 30�
g_fm.GrabFrame(&b6[3],781,132,806,156,&imgSrc); // 45�
g_fm.GrabFrame(&b6[4],809,128,828,156,&imgSrc); // 60�
g_fm.GrabFrame(&b6[5],831,125,845,156,&imgSrc); // 75�
g_fm.GrabFrame(&b6[6],848,125,861,156,&imgSrc); // 90�
g_fm.GrabFrame(&b6[7],864,125,878,156,&imgSrc); // 105�
g_fm.GrabFrame(&b6[8],881,128,900,156,&imgSrc); // 120�
g_fm.GrabFrame(&b6[9],903,132,928,156,&imgSrc); // 135�
g_fm.GrabFrame(&b6[10],931,138,958,156,&imgSrc); // 150�
g_fm.GrabFrame(&b6[11],961,141,992,156,&imgSrc); // 165�
g_fm.GrabFrame(&b6[12],995,143,1026,156,&imgSrc); // 180�
g_fm.GrabFrame(&b6[13],961,91,992,106,&imgSrc); // 195�
g_fm.GrabFrame(&b6[14],931,91,958,109,&imgSrc); // 210�
g_fm.GrabFrame(&b6[15],903,91,928,115,&imgSrc); // 225�
g_fm.GrabFrame(&b6[16],881,91,900,119,&imgSrc); // 240�
g_fm.GrabFrame(&b6[17],864,91,878,122,&imgSrc); // 255�
g_fm.GrabFrame(&b6[18],848,91,861,122,&imgSrc); // 270�

ProgressBar(g_pbx,g_pby,0.2f,g_pbw);

g_fm.GrabFrame(&b6[19],831,91,845,122,&imgSrc); // 285�
g_fm.GrabFrame(&b6[20],809,91,828,119,&imgSrc); // 300�
g_fm.GrabFrame(&b6[21],781,91,806,115,&imgSrc); // 315
g_fm.GrabFrame(&b6[22],751,91,778,110,&imgSrc); // 330
g_fm.GrabFrame(&b6[23],717,91,748,106,&imgSrc); // 345
g_fm.GrabFrame(&p18[0],198,7,209,17,&imgSrc); // fiammella blu anim
g_fm.GrabFrame(&p18[1],212,7,223,17,&imgSrc); // fiammella blu anim
g_fm.GrabFrame(&p18[2],226,7,237,17,&imgSrc); // fiammella blu anim
g_fm.GrabFrame(&p18[3],240,7,251,17,&imgSrc); // fiammella blu anim
g_fm.GrabFrame(&p18[4],254,7,265,17,&imgSrc); // fiammella blu anim

ProgressBar(g_pbx,g_pby,0.25f,g_pbw);

g_fm.GrabFrame(&p19,449,227,479,234,&imgSrc); //proiettile blu lungo
g_fm.GrabFrame(&p25,763,297,763+15,297+15,&imgSrc); //p. sferico bianco
g_fm.GrabFrame(&p26,132,244,155,250,&imgSrc); //p. per raggio laser rosso
if (FAILED(g_fm.GrabFrameArray(p20,&imgSrc,4,883,494,52,26))) return E_FAIL; //proiettile grande a fiamma
g_fm.GrabFrameArray(m_imgWhiteSmoke,&imgSrc,5,793,299,13,13);
g_fm.GrabFrame(&p21,513,276,533,281,&imgSrc); //proittile giallo lungo orizzontale
g_fm.GrabFrame(&p22,550,310,560,320,&imgSrc); //sfera gialla

ProgressBar(g_pbx,g_pby,0.30f,g_pbw);

g_fm.GrabFrame(&p23[0],128,245,160,250,&imgSrc); //proiettile rosso lungo

for (dwCount=1;dwCount<13;dwCount++) g_fm.CreateRotFrame(dwCount*15,&p23[dwCount],&p23[0],TRUE); 

if (FAILED(g_fm.GrabFrameArray(m_imgThunderbolt,&imgSrc,14,0,544,64,64))) return E_FAIL; //fulmine
//icone armi speciali
if (FAILED(g_fm.GrabFrameArray(m_imgWpIcons,&imgSrc,11,560,324,28,25))) return E_FAIL;
g_fm.GrabFrame(&m_imgWpFrame,870,330,950,346,&imgSrc);
g_fm.GrabFrame(&m_imgWpLevel,870,313,902,323,&imgSrc);

ProgressBar(g_pbx,g_pby,0.35f,g_pbw);

//data.vpx immagine 1
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data.vpx",1);

if (FAILED(hr)) return hr; //errore acquisendo gli sprites


g_fm.GrabFrame(&ple[0],2,237,144,309,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[1],148,237,292,319,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[2],296,237,446,326,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[3],450,237,606,332,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[4],609,237,779,341,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[5],782,237,970,355,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[6],974,237,1183,387,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[7],2,911,221,1064,&imgSrc); // esplosione player
g_fm.GrabFrame(&ple[8],228,911,462,1092,&imgSrc); // esplosione player
g_fm.GrabFrame(&bonus1[0],2,336,53,390,&imgSrc); // esplosione

ProgressBar(g_pbx,g_pby,0.40f,g_pbw);

g_fm.GrabFrame(&bonus1[1],56,336,108,390,&imgSrc); // esplosione
g_fm.GrabFrame(&bonus1[2],111,336,163,390,&imgSrc); // esplosione
g_fm.GrabFrame(&bonus1[3],166,336,218,390,&imgSrc); // esplosione
g_fm.GrabFrame(&bonus1[4],221,336,273,390,&imgSrc); // bonus arancione
g_fm.GrabFrame(&bonus1[5],276,336,328,390,&imgSrc); // bonus arancione
g_fm.GrabFrame(&bonus1[6],331,336,383,390,&imgSrc); // bonus arancione
g_fm.GrabFrame(&bonus1[7],386,336,438,390,&imgSrc); // bonus arancione
g_fm.GrabFrame(&bonus1[8],441,336,493,390,&imgSrc); // bonus arancione
g_fm.GrabFrame(&bonus2[0],2,393,53,447,&imgSrc);
g_fm.GrabFrame(&bonus2[1],56,393,108,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[2],111,393,163,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[3],166,393,218,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[4],221,393,273,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[5],276,393,328,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[6],331,393,383,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[7],386,393,438,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus2[8],441,393,493,447,&imgSrc); // bonus giallo
g_fm.GrabFrame(&bonus3[0],2,450,53,504,&imgSrc);
g_fm.GrabFrame(&bonus3[1],56,450,108,504,&imgSrc); // bonus verde

ProgressBar(g_pbx,g_pby,0.45f,g_pbw);

g_fm.GrabFrame(&bonus3[2],111,450,163,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[3],166,450,218,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[4],221,450,273,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[5],276,450,328,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[6],331,450,383,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[7],386,450,438,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus3[8],441,450,493,504,&imgSrc); // bonus verde
g_fm.GrabFrame(&bonus4[0],2,507,53,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[1],56,507,108,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[2],111,507,163,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[3],166,507,218,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[4],221,507,273,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[5],276,507,328,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[6],331,507,383,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[7],386,507,438,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus4[8],441,507,493,561,&imgSrc); //bonus blu
g_fm.GrabFrame(&bonus5[0],498,392,550,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[1],553,392,605,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[2],608,392,660,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[3],663,392,715,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[4],718,392,770,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[5],773,392,825,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[6],828,392,880,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[7],883,392,935,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&bonus5[8],938,392,990,446,&imgSrc); // bonus rosso
g_fm.GrabFrame(&b7[0],497,452,546,473,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[1],549,452,597,477,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[2],600,452,645,489,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[3],648,452,690,494,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[4],693,452,726,497,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[5],730,452,758,499,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[6],762,452,790,496,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[7],794,452,822,499,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[8],826,452,859,497,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[9],862,452,904,494,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[10],907,452,952,489,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[11],955,452,1003,477,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[12],1006,452,1055,473,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[13],955,524,1003,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[14],907,512,952,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[15],862,507,904,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[16],826,504,859,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[17],794,502,822,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[18],762,505,790,549,&imgSrc); // bomba piccola

ProgressBar(g_pbx,g_pby,0.50f,g_pbw);

g_fm.GrabFrame(&b7[19],730,502,758,549,&imgSrc); // bomba piccola 
g_fm.GrabFrame(&b7[20],693,504,726,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[21],648,507,690,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[22],600,512,645,549,&imgSrc); // bomba piccola
g_fm.GrabFrame(&b7[23],549,524,597,549,&imgSrc); // bomba piccolag_fm.GrabFrame(
g_fm.GrabFrame(&b8[0],39,589,128,620,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[1],96,725,180,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[2],183,710,263,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[3],266,698,329,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[4],332,690,380,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[5],384,681,418,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[6],421,674,454,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[7],494,681,528,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[8],532,690,580,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[9],583,698,646,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[10],649,710,729,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[11],732,725,816,763,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[12],820,725,909,756,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[13],768,582,852,620,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[14],685,582,765,635,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[15],619,582,682,647,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[16],568,582,616,655,&imgSrc); // missile verde

ProgressBar(g_pbx,g_pby,0.55f,g_pbw);

g_fm.GrabFrame(&b8[17],530,582,564,664,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[18],494,582,527,671,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[19],420,582,454,664,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[20],368,582,416,655,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[21],302,582,365,647,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[22],219,582,299,635,&imgSrc); // missile verde
g_fm.GrabFrame(&b8[23],132,582,216,620,&imgSrc); // missile verde
g_fm.GrabFrame(&b9[0],559,789,658,819,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[1],661,789,755,825,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[2],758,789,842,845,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[3],845,789,922,864,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[4],925,789,979,873,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[5],982,789,1021,884,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[6],3,777,33,873,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[7],36,777,75,872,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[8],78,777,132,861,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[9],135,777,212,852,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[10],215,777,299,833,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[11],302,777,396,813,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[12],399,777,498,807,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[13],783,871,877,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[14],696,851,780,907,&imgSrc); // missile grande grigio

ProgressBar(g_pbx,g_pby,0.60f,g_pbw);

g_fm.GrabFrame(&b9[15],616,832,693,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[16],559,823,613,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[17],517,812,556,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[18],484,811,514,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[19],442,812,481,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[20],385,823,439,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[21],305,832,382,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[22],218,851,302,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&b9[23],121,871,215,907,&imgSrc); // missile grande grigio
g_fm.GrabFrame(&m_imgFriend,0,0,41,26,&imgSrc); //dispositivo di aiuto
g_fm.GrabFrame(&m_imgFriendCannon,44,0,88,11,&imgSrc); //cannoncino del dispositivo di fuoco amico
if (FAILED(g_fm.GrabFrameArray(m_imgShield,&imgSrc,18,0,56,32,28))) return E_FAIL;

ProgressBar(g_pbx,g_pby,0.62f,g_pbw);

//frame posteriore del fuoco ausiliario
if (FAILED(g_fm.GrabVFrameArray(m_imgGyroFrame,&imgSrc,10,1080,432,54,72))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_imgGyroFrame[10],1134,432,1134+54,432+72,&imgSrc))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_imgGyroFrame[11],1134,504,1134+54,504+72,&imgSrc))) return E_FAIL;
//cannone del fuoco ausiliario
if (FAILED(g_fm.GrabVFrameArray(m_imgGyroCannon,&imgSrc,8,1134,576,54,72))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_imgGyroCannon[8],1026,576,1026+54,576+72,&imgSrc))) return E_FAIL;

if (FAILED(g_fm.GrabFrameArray(m_imgBackFlame,&imgSrc,4,36,644,36,14))) return E_FAIL; //fiamma ugello posteriore
if (FAILED(g_fm.GrabVFrameArray(m_imgBonusCapt,&imgSrc,4,480,924,80,21))) return E_FAIL;
g_fm.GrabFrame(&m_imgBonusCapt[4],480,1008,480+144,1008+21,&imgSrc); //"shield"
g_fm.GrabFrame(&m_imgBonusCapt[5],480,1029,480+144,1029+21,&imgSrc); //"power up"
g_fm.GrabFrame(&m_imgBonusCapt[6],480,1050,480+62,1050+21,&imgSrc); //"1UP" g_fm.GrabFrame(&m_imgBonusCapt[7],480,1071,480+124,1071+21,&imgSrc); //"SPECIAL"
g_fm.GrabFrame(&m_imgBonusCapt[7],480,1071,480+124,1071+21,&imgSrc); //special
g_fm.GrabFrame(&m_imgBonusCapt[8],480,1092,550,1092+21,&imgSrc); //1000 punti
g_fm.GrabFrame(&m_imgBonusCapt[9],480,1113,480+120,1113+21,&imgSrc); //energy
g_fm.GrabFrame(&m_imgBonusCapt[10],480,1134,480+120,1134+21,&imgSrc); //thrust
//simbolo delle vite nella barra dei punteggi
if (FAILED(g_fm.GrabFrame(&g_imgLabelShip,0,93,56,124,&imgSrc))) return E_FAIL;

//detriti
if (FAILED(g_fm.GrabFrameArray(m_imgDebris1,&imgSrc,6,598,945,46,45))) return E_FAIL;
if (FAILED(g_fm.GrabVFrameArray(m_imgDebris2,&imgSrc,6,1012,900,46,45))) return E_FAIL;
if (FAILED(g_fm.GrabVFrameArray(m_imgDebris3,&imgSrc,5,966,945,46,45))) return E_FAIL;
g_fm.GrabFrame(&m_imgDebris4,630,990,640,999,&imgSrc);
g_fm.GrabFrame(&m_imgDebris5,630,1008,640,1017,&imgSrc);
if (FAILED(g_fm.GrabFrameArray(m_imgDebris6,&imgSrc,8,630,1008,10,9))) return E_FAIL; //detrito rotante piccolo
if (FAILED(g_fm.GrabFrameArray(m_imgDebris7,&imgSrc,6,644,1040,28,26))) return E_FAIL;
if (FAILED(g_fm.GrabFrameArray(m_imgSmoke,&imgSrc,9,736,672,32,32))) return E_FAIL;
if (FAILED(g_fm.GrabFrameArray(m_imgFire,&imgSrc,8,192,0,32,42))) return E_FAIL; //fiamme
if (FAILED(g_fm.GrabFrameArray(m_imgBlackSmoke,&imgSrc,6,630,0,42,42))) return E_FAIL; //fumo nero
//splash

ProgressBar(g_pbx,g_pby,0.65f,g_pbw);

g_fm.GrabFrame(&m_imgSplash[0],1,1070,25,1091,&imgSrc); //splash
g_fm.GrabFrame(&m_imgSplash[1],30,1070,60,1096,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[2],63,1070,92,1111,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[3],95,1070,137,1125,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[4],140,1070,195,1141,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[5],200,1097,279,1167,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[6],283,1097,364,1154,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[7],368,1096,420,1133,&imgSrc); // splash
g_fm.GrabFrame(&m_imgSplash[8],45,1129,137,1170,&imgSrc); // splash

//impatto su terreno solido
if (FAILED(g_fm.GrabFrameArray(m_imgGrndImpact,&imgSrc,7,648,1104,24,24))) return E_FAIL;

//Estrae gli sprites delle astronavi nemiche
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data1.vpx",0);

if (FAILED(hr)) return hr;

//disco volante UFO
if (FAILED(g_fm.GrabFrameArray(m_imgUfo,&imgSrc,9,0,0,96,38))) return E_FAIL;
//GoldenFighter
if (FAILED(g_fm.GrabFrameArray(m_gf,&imgSrc,10,0,62,128,62))) return E_FAIL;
//contraerea 1
if (FAILED(g_fm.GrabFrameArray(m_gaf,&imgSrc,4,1232,616,88,77))) return E_FAIL;
//radar
if (FAILED(g_fm.GrabFrameArray(m_imgRadar,&imgSrc,11,720,864,80,108))) return E_FAIL;
//X-Wing
if (FAILED(g_fm.GrabFrameArray(m_imgXWing,&imgSrc,9,0,152,112,76))) return E_FAIL;
//Golden Fighter 1
if (FAILED(g_fm.GrabFrameArray(m_gf1,&imgSrc,9,0,280,112,56))) return E_FAIL;
//Blaster
if (FAILED(g_fm.GrabFrameArray(m_imgBlaster,&imgSrc,5,0,336,192,112))) return E_FAIL;
//M Fighter
if (FAILED(g_fm.GrabFrameArray(m_imgMFighter,&imgSrc,9,0,280,112,56))) return E_FAIL;

ProgressBar(g_pbx,g_pby,0.67f,g_pbw);

for (dwCount=5;dwCount<11;dwCount++)
{
	rc.left=(dwCount-5)*192;
	rc.top=448;
	rc.bottom=rc.top+112;
	rc.right=rc.left+192;

	if (FAILED(g_fm.GrabFrame(&m_imgBlaster[dwCount],&rc,&imgSrc))) return E_FAIL;
}

///Lambda Fighter
if (FAILED(g_fm.GrabFrameArray(m_imgLambda,&imgSrc,9,0,576,112,64))) return E_FAIL;
//ufo 2
if (FAILED(g_fm.GrabFrameArray(m_imgUfo2,&imgSrc,6,826,722,118,38))) return E_FAIL;
//torretta
if (FAILED(g_fm.GrabFrameArray(m_gaf2,&imgSrc,3,672,720,42,48))) return E_FAIL;
//porta aerei
if (FAILED(g_fm.GrabFrame(&m_imgAircraftCarrier,17,730,539,826,&imgSrc))) return E_FAIL;
//scia
if (FAILED(g_fm.GrabFrameArray(m_imgWake,&imgSrc,5,880,36,22,12))) return E_FAIL;

ProgressBar(g_pbx,g_pby,0.70f,g_pbw);

//cannone
if (FAILED(g_fm.GrabFrame(&m_imgRotCannon[0],1151,0,1151+48,7,&imgSrc))) return E_FAIL;

for (dwCount=1;dwCount<13;dwCount++)
{
	//crea la frame ruotata
	g_fm.CreateRotFrame(360-dwCount*15,&m_imgRotCannon[dwCount],&m_imgRotCannon[0]);
}

//cannone 2
if (FAILED(g_fm.GrabFrame(&m_imgRotCannon2[0],200,840,280,920,&imgSrc))) return E_FAIL;
//frame ruotate del cannone a 360�
for (dwCount=1;dwCount<24;dwCount++)
{
	g_fm.CreateRotFrame(dwCount*15,&m_imgRotCannon2[dwCount],&m_imgRotCannon2[0],FALSE);
}

//incrociatore
if (FAILED(g_fm.GrabFrame(&m_imgShip[0],1209,168,1209+386,168+94,&imgSrc))) return E_FAIL;
//incrociatore danneggiato
if (FAILED(g_fm.GrabFrame(&m_imgShip[1],1209,268,1209+386,268+96,&imgSrc))) return E_FAIL; 
//cingolo del carro armato
if (FAILED(g_fm.GrabVFrameArray(m_imgTank1,&imgSrc,6,1200,420,60,30))) return E_FAIL;
//torretta del carro armato
if (FAILED(g_fm.GrabVFrameArray(m_imgTank2,&imgSrc,3,1302,510,62,34))) return E_FAIL;

g_fm.FreeImgFrame(&imgSrc);

//Estrae gli sprites delle esplosioni
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data1.vpx",1);

ProgressBar(g_pbx,g_pby,0.73f,g_pbw);

if (FAILED(hr)) return hr;

//esplosione grande (60x 80)
if (FAILED(g_fm.GrabFrameArray(es_big,&imgSrc,10,0,0,60,80))) return E_FAIL;
//seconda riga esplosione grande
if (FAILED(g_fm.GrabFrameArray(&es_big[10],&imgSrc,5,0,80,60,80))) return E_FAIL;
//esplosione media (contiene una biforcazione della sequenza a seconda che il nemico venga distrutto o meno)
if (FAILED(g_fm.GrabFrameArray(es_medium,&imgSrc,10,0,192,64,64))) return E_FAIL;
if (FAILED(g_fm.GrabFrameArray(&es_medium[10],&imgSrc,7,0,256,64,64))) return E_FAIL;
//esplosione piccola
if (FAILED(g_fm.GrabFrameArray(es_small,&imgSrc,7,320,154,20,22))) return E_FAIL;

g_fm.FreeImgFrame(&imgSrc);

ProgressBar(g_pbx,g_pby,0.75f,g_pbw);

g_fm.FreeImgFrame(&imgSrc);

hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data2.vpx",1);

if (FAILED(hr)) return hr;

ProgressBar(g_pbx,g_pby,0.77f,g_pbw);

if (FAILED(g_fm.GrabFrame(&m_electric_barrier[0],0,0,264,236,&imgSrc))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_electric_barrier[1],264,0,264+264,236,&imgSrc))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_electric_barrier[2],0,236,264,236+236,&imgSrc))) return E_FAIL;
if (FAILED(g_fm.GrabFrame(&m_electric_barrier[3],264,236,264+264,236+236,&imgSrc))) return E_FAIL;

g_fm.FreeImgFrame(&imgSrc);

//altri sprites
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data4.vpx",0);

if (FAILED(hr)) return hr;

//buco nel ghiaccio
if (FAILED(g_fm.GrabFrame(&m_imgIceHole,230,67,230+112,67+36,&imgSrc))) return E_FAIL;

g_fm.FreeImgFrame(&imgSrc);

//data5.vpx
hr=g_fm.CreateImgFrameFromVPX(&imgSrc,"data\\data5.vpx",1);

if (FAILED(hr)) return hr;

//rot fighter verde
hr=g_fm.GrabFrame(&m_imgScrewFighter[0],0,31,99,69,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[1],100,34,199,70,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[2],200,25,299,77,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[3],300,18,399,86,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[4],400,11,499,91,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[5],500,7,599,93,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[6],600,7,699,93,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[7],0,98+9,99,98+93,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[8],100,98+12,199,98+90,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[9],200,98+16,299,98+85,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[10],300,98+24,399,98+75,&imgSrc);
g_fm.GrabFrame(&m_imgScrewFighter[11],400,98+33,499,98+68,&imgSrc);

if (FAILED(hr)) return hr;

//ufo 3
hr=g_fm.GrabFrameArray(m_imgUfo3,&imgSrc,6,0,270,96,68);
if (FAILED(hr)) return hr;

g_fm.FreeImgFrame(&imgSrc);

g_fm.SetDefaultCaps (DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY);

return S_OK;

}

//rilascia gli sprites
void FreeSprites(void)
{
	DWORD dwCount;
	g_fm.FreeImgFrame(&p1);
	g_fm.FreeImgFrame(&p2);g_fm.FreeImgFrame(&p3);g_fm.FreeImgFrame(&p4);
	g_fm.FreeImgFrame(&p5);g_fm.FreeImgFrame(&p6);g_fm.FreeImgFrame(&p7);
	g_fm.FreeImgFrame(&p8);g_fm.FreeImgFrame(&p10);g_fm.FreeImgFrame(&p11);
	g_fm.FreeImgFrame(&b5);g_fm.FreeImgFrame(&p24);g_fm.FreeImgFrame(&p25);
	g_fm.FreeFrameArray(m_imgNozzleFlame,3);g_fm.FreeImgFrame(&p26);
	g_fm.FreeFrameArray(m_imgNozzleFlameV,3);

	//data1.vpx
	g_fm.FreeFrameArray(es_big,15);
	g_fm.FreeFrameArray(es_medium,17);
	g_fm.FreeFrameArray(es_small,7);
	g_fm.FreeFrameArray(e1,5);
	g_fm.FreeFrameArray(p18,5);
	g_fm.FreeFrameArray(p9,24);
	g_fm.FreeFrameArray(b4,24);
	g_fm.FreeFrameArray(b6,24);
	g_fm.FreeFrameArray(b7,24);
	g_fm.FreeFrameArray(b8,24);
	g_fm.FreeFrameArray(b9,24);
	g_fm.FreeFrameArray(pl,13);
	g_fm.FreeFrameArray(p12,12);
	g_fm.FreeFrameArray(p13,12);
	g_fm.FreeFrameArray(p14,9);
	g_fm.FreeFrameArray(bonus1,10);
	g_fm.FreeFrameArray(bonus2,10);
	g_fm.FreeFrameArray(bonus3,10);
	g_fm.FreeFrameArray(bonus4,10);
	g_fm.FreeFrameArray(bonus5,10);
	g_fm.FreeFrameArray(ple,10);
	g_fm.FreeFrameArray(p17,12);
	g_fm.FreeFrameArray(m_imgUfo,9);
	g_fm.FreeFrameArray(m_gf,10);
	g_fm.FreeFrameArray(m_gaf,4);
	g_fm.FreeFrameArray(m_imgRadar,11);
	g_fm.FreeFrameArray(m_imgXWing,9);
	g_fm.FreeFrameArray(m_gf1,9);
	g_fm.FreeFrameArray(m_imgBlaster,11);
    g_fm.FreeFrameArray(m_imgLambda,9);	
	g_fm.FreeFrameArray(m_imgMFighter,9);
	g_fm.FreeFrameArray(m_imgUfo2,6);
	g_fm.FreeFrameArray(m_imgRotCannon,13);
	g_fm.FreeFrameArray(m_gaf2,3);
	g_fm.FreeFrameArray(m_imgWake,5);
	g_fm.FreeFrameArray(m_imgRotCannon2,24);
	g_fm.FreeFrameArray(m_imgShip,2);
	g_fm.FreeFrameArray(m_imgTank1,6);
	g_fm.FreeFrameArray(m_imgTank2,3);
	g_fm.FreeFrameArray(m_imgFire,11);
	g_fm.FreeFrameArray(m_imgBlackSmoke,6);
	g_fm.FreeFrameArray(m_imgWhiteSmoke,5);
	g_fm.FreeFrameArray(m_imgGrndImpact,7);
	g_fm.FreeFrameArray(m_imgThunderbolt,14);
	g_fm.FreeFrameArray(m_imgWpIcons,11);
	g_fm.FreeImgFrame(&m_imgWpLevel);
	g_fm.FreeImgFrame(&m_imgWpFrame);
	g_fm.FreeImgFrame(&m_imgAircraftCarrier);
	g_fm.FreeFrameArray(m_electric_barrier,4);
	g_fm.FreeFrameArray(m_imgScrewFighter,12);
	g_fm.FreeFrameArray(m_imgUfo3,6);

	for (dwCount=0;dwCount<9;dwCount++) g_fm.FreeImgFrame(&m_imgUfo[dwCount]);
	for (dwCount=0;dwCount<10;dwCount++) g_fm.FreeImgFrame(&m_gf[dwCount]);
	for (dwCount=0;dwCount<4;dwCount++) g_fm.FreeImgFrame(&m_gaf[dwCount]);
	for (dwCount=0;dwCount<9;dwCount++) g_fm.FreeImgFrame(&m_imgSplash[dwCount]);

	g_fm.FreeImgFrame(&p19);	
	g_fm.FreeFrameArray(p20,4);
	g_fm.FreeImgFrame(&p21);
	g_fm.FreeImgFrame(&p22);
	g_fm.FreeFrameArray(p23,13);
	g_fm.FreeImgFrame(&m_imgFriend);
	g_fm.FreeImgFrame(&m_imgFriendCannon);
	g_fm.FreeImgFrame(&g_imgLabelShip);
	g_fm.FreeFrameArray(m_imgShield,18);
	g_fm.FreeFrameArray(m_imgBackFlame,4);
    g_fm.FreeFrameArray(m_imgBonusCapt,11);
	g_fm.FreeFrameArray(m_imgDebris1,6);
	g_fm.FreeFrameArray(m_imgDebris2,6);
	g_fm.FreeFrameArray(m_imgDebris3,5);
	g_fm.FreeImgFrame(&m_imgDebris4);
	g_fm.FreeImgFrame(&m_imgDebris5);
	g_fm.FreeFrameArray(m_imgDebris6,8);
	g_fm.FreeFrameArray(m_imgDebris7,6);
	g_fm.FreeFrameArray(m_imgGyroFrame,12);
	g_fm.FreeFrameArray(m_imgGyroCannon,9);
	g_fm.FreeImgFrame(&m_imgIceHole);

}

//----------------------------------- InitSounds -------------------------------------------

//crea gli effetti sonori
HRESULT CreateSounds()
{
	HRESULT hr=S_OK;

	if (!g_Environ.bSound) return S_OK;

	g_snBonus=g_cs.CreateSound("WAVS\\bonus.wav",2);	
	if (g_snBonus==-1) hr=E_FAIL;

	g_snDrop=g_cs.CreateSound("WAVS\\drop.wav",4);
	if (g_snDrop==-1) hr=E_FAIL;

	g_snExplo1=g_cs.CreateSound("WAVS\\explo1.wav",8);
	if (g_snExplo1==-1) hr=E_FAIL;

	g_snExplo2=g_cs.CreateSound("WAVS\\explo2.wav",5);
	if (g_snExplo2==-1) hr=E_FAIL;

	g_snExplo3=g_cs.CreateSound("WAVS\\explo3.wav",5);
	if (g_snExplo1==-1) hr=E_FAIL;

	g_snFire1=g_cs.CreateSound("WAVS\\fire1.wav",8);
	if (g_snFire1==-1) hr=E_FAIL;

	g_snFire1_1=g_cs.CreateSound("WAVS\\fire1_1.wav",8);
	if (g_snFire1_1==-1) hr=E_FAIL;

	g_snFire1_2=g_cs.CreateSound("WAVS\\fire1_2.wav",8);
	if (g_snFire1_2==-1) hr=E_FAIL;

	g_snFire2=g_cs.CreateSound("WAVS\\fire2.wav",8);
	if (g_snFire2==-1) hr=E_FAIL;

	g_snFire3=g_cs.CreateSound("WAVS\\fire3.wav",8);
	if (g_snFire3==-1) hr=E_FAIL;

	g_snFire4=g_cs.CreateSound("WAVS\\fire4.wav",8);
	if (g_snFire4==-1) hr=E_FAIL;

	g_snFire5=g_cs.CreateSound("WAVS\\fire5.wav",8);
	if (g_snFire5==-1) hr=E_FAIL;

	g_snFire6=g_cs.CreateSound("WAVS\\fire6.wav",10);
    if (g_snFire6==-1) hr=E_FAIL;	
	
	g_snFire7=g_cs.CreateSound("WAVS\\fire7.wav",10);
	if (g_snFire7==-1) hr=E_FAIL;

	g_snFire8=g_cs.CreateSound("WAVS\\fire8.wav",5);
    if (g_snFire8==-1) hr=E_FAIL;

	g_snFire9=g_cs.CreateSound("WAVS\\fire9.wav",5);
    if (g_snFire9==-1) hr=E_FAIL;
	
	g_snFire10=g_cs.CreateSound("WAVS\\electro2.wav",2);
	if (g_snFire10==-1) hr=E_FAIL;

	g_snFire11=g_cs.CreateSound("WAVS\\fire10.wav",3);
	if (g_snFire11==-1) hr=E_FAIL;
	
    g_snFire12=g_cs.CreateSound("WAVS\\liquidblast.wav",6); //rumore di un oggetto pesante che cade in acqua
	if (g_snFire12==-1) hr=E_FAIL;

	//missile
	g_snMissile=g_cs.CreateSound("WAVS\\missile.wav",6);
	if (g_snMissile==-1) hr=E_FAIL;

	g_snFwMissile=g_cs.CreateSound("WAVS\\missile2.wav",8);
	if (g_snFwMissile==-1) hr=E_FAIL;

	//fuoco piccolo nemico
	g_snFire13=g_cs.CreateSound("WAVS\\fire13.wav",10);
	if (g_snFire13==-1) hr=E_FAIL;

	g_snUfoFire=g_cs.CreateSound("WAVS\\ufofire.wav",4);
	if (g_snUfoFire==-1) hr=E_FAIL;

	g_snBlip=g_cs.CreateSound("WAVS\\blip.wav",10);
	if (g_snBlip==-1) hr=E_FAIL;

	g_snHit1=g_cs.CreateSound("WAVS\\hithard.wav",5);
	if (g_snHit1==-1) hr=E_FAIL;

	g_snBfire=g_cs.CreateSound("WAVS\\bfire.wav",5);
	if (g_snBfire==-1) hr=E_FAIL;

	g_snMagnetic=g_cs.CreateSound("WAVS\\magnetic.wav",8);
	if (g_snMagnetic==-1) hr=E_FAIL;

	g_snFire14=g_cs.CreateSound("WAVS\\shot007.wav",5);
	if (g_snFire14==-1) hr=E_FAIL;

	g_snElectro3=g_cs.CreateSound("WAVS\\electro3.wav",5);
    if (g_snElectro3==-1) hr=E_FAIL;

	g_snFinal=g_cs.CreateSound("WAVS\\final.wav",1);
	if (g_snFinal==-1) hr=E_FAIL;

	g_snRadar=g_cs.CreateSound("WAVS\\radar.wav",5);
    if (g_snRadar==-1) hr=E_FAIL;

	g_snBooster=g_cs.CreateSound("WAVS\\booster.wav",1);
	if (g_snBooster==-1) hr=E_FAIL;

	g_snNozzle=g_cs.CreateSound("WAVS\\nozzle.wav",3);
	if (g_snNozzle==-1) hr=E_FAIL;

	g_snMech1=g_cs.CreateSound("WAVS\\mech1.wav",3);
	if (g_snMech1==-1) hr=E_FAIL;

	g_snMech2=g_cs.CreateSound("WAVS\\mech2.wav",3);
	if (g_snMech2==-1) hr=E_FAIL;

	g_snCrash=g_cs.CreateSound("WAVS\\crash.wav",3);
    if (g_snCrash==-1) hr=E_FAIL;

	g_snPlLaser=g_cs.CreateSound("WAVS\\plaser.wav",8);
	if (g_snPlLaser==-1) hr=E_FAIL;

	g_snBleep1=g_cs.CreateSound("WAVS\\bleep1.wav",4);
	if (g_snBleep1==-1) hr=E_FAIL;

	g_snSunk=g_cs.CreateSound("WAVS\\sunk.wav",2);

	g_snFFlames=g_cs.CreateSound("WAVS\\fflames.wav",5);
	
	return hr;	
}

//------------------------------------ FreeSounds ------------------------------------------

//rilascia gli effetti sonori caricati
void FreeSounds()
{
	g_cs.FreeAllSounds();
}

//------------------------------------- CreateObjects --------------------------------------

//crea gli stack di oggetti e altri oggetti necessari al ciclo del gioco
//Nota Bene: la creazione degli oggetti basi del gioco avviene solo all'inizio del gioco
//e non ogni volta che si carica un livello

HRESULT CreateObjects()
{

	CSBObject *pobj;
	CExplosion *pExpl=NULL;
	CEnemy *pEn=NULL;	
    CLambda *pL=NULL;
	CSbBonus *pBn=NULL;
	CGyro *pGyr=NULL;
	CDynString *pStr=NULL;
	CGroundObject *pGr=NULL;
	CShip *pSh=NULL;
	CTank *pTk=NULL;
	CLaser *pL1=NULL;
	CSmoke *pSmk=NULL;
	CGameOver *pGM=NULL;
	CUfo2 *pU2=NULL;
	CAntiAircraft2 *pAA2=NULL;
	CDropBomb *pDb1=NULL;
	CDebris *pDb=NULL;
	CBooster *pBoo=NULL;
	CAnimatedObject *pAo=NULL;
	CMissile *pmis=NULL;
	CThunderBolt *pTh=NULL;
	DWORD dwCount;
	DWORD dwCount1;
	LONG lCount;
	HRESULT hr;
	CADXFastMath fh;

//	float f1,f2,f3; //general purpose


#ifdef _TESTGAME_

	g_cdb.WriteErrorFile("CREATE OBJECT");

#endif
  
	ProgressBar(g_pbx,g_pby,0.78f,g_pbw);
  
	
	 //bonus verde : power up
	g_sBonusGreen.Create(2);

	for (dwCount=0;dwCount<g_sBonusGreen.ItemCount();dwCount++)
	{
		g_sBonusGreen.m_objStack[dwCount]=new CSbBonus;
		g_sBonusGreen.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusGreen.m_objStack[dwCount];
		pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
        pBn=(CSbBonus *)pobj;	
		pBn->SetCurFrame(0);
		pBn->lpfnExplode=PushBonusFire1; //funzione associata al bonus (viene attivata quando il giocatore collide oon il bonus)

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			if (FAILED(pobj->AddFrame(&bonus3[dwCount1]))) return E_FAIL;
		}											
		
	}

	ProgressBar(g_pbx,g_pby,0.79f,g_pbw);

	//bonus arancione
	g_sBonusOrange.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusOrange.m_objStack[dwCount]=new CSbBonus;
		g_sBonusOrange.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusOrange.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusFire2; //funzione associata al bonus (viene attivata quando il giocatore collide oon il bonus)

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus1[dwCount1]);
		}		
		
	}

	//bonus giallo
	g_sBonusYellow.Create(2);
	
	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusYellow.m_objStack[dwCount]=new CSbBonus;
		g_sBonusYellow.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusYellow.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusEnergy;

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus2[dwCount1]);
		}		
		
	}

	ProgressBar(g_pbx,g_pby,0.80f,g_pbw);

	//bonus blu
	g_sBonusBlue.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusBlue.m_objStack[dwCount]=new CSbBonus;
		g_sBonusBlue.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusBlue.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusShield; //funzione che immette la barriera protettiva

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus4[dwCount1]);
		}		
		
	}

	ProgressBar(g_pbx,g_pby,0.81f,g_pbw);
	//bonus rosso
	g_sBonusRed.Create(4);

	for (dwCount=0;dwCount<4;dwCount++)
	{
		g_sBonusRed.m_objStack[dwCount]=new CSbBonus;
		g_sBonusRed.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusRed.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusGyro;

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus5[dwCount1]);
		}		
		
	}

	//booster: fa aumentare la velocit� di scrolling
	g_sBonusBooster.Create(1);

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sBonusBooster.m_objStack[dwCount]=new CSbBonus;
		g_sBonusBooster.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusBooster.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusBooster;

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus5[dwCount1]);
		}		
	}

	ProgressBar(g_pbx,g_pby,0.85f,g_pbw);

	//bonus +10 bombe
	g_sBonusBombs10.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusBombs10.m_objStack[dwCount]=new CSbBonus;
		g_sBonusBombs10.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusBombs10.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusBombs10;

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus1[dwCount1]);
		}		


	}

	//bonus +50 bombe
	g_sBonusBombs50.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusBombs50.m_objStack[dwCount]=new CSbBonus;
		g_sBonusBombs50.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sBonusBombs50.m_objStack[dwCount];
        pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusBombs50;

		for(dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus1[dwCount1]);
		}	

	}

    //bonus punti
	g_sBonusScore.Create(8);

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sBonusScore.m_objStack[dwCount]=new CSbBonus;
		g_sBonusScore.m_objStack[dwCount]->bActive=FALSE;
		pobj=(CSBObject *)g_sBonusScore.m_objStack[dwCount];
		pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);			
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusScore;

		//immette le singole frame all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus5[dwCount1]);
		}

	}

	//bonus thrust
	g_sBonusThrust.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonusThrust.m_objStack[dwCount]=new CSbBonus;
		g_sBonusThrust.m_objStack[dwCount]->bActive=FALSE;
		pobj=(CSBObject *)g_sBonusThrust.m_objStack[dwCount];
		pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonusThrust;

		//immette le singole frame all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus5[dwCount1]);
		}

	}

	g_sBonusElectricBarrier.Create(1);

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sBonusElectricBarrier.m_objStack[dwCount]=new CSbBonus;
		pBn=(CSbBonus *)g_sBonusElectricBarrier.m_objStack[dwCount];
		pBn->bActive=FALSE;
		pBn->SetGraphicManager(&g_gm);
		pBn->SetFrameManager(&g_fm);	
		pBn->lpfnExplode=PushBonusEB;

		//immette le singole frame del bonus all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pBn->AddFrame(&bonus5[dwCount1]);
		}
	}

	g_sBonusBlueShell.Create(1);

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sBonusBlueShell.m_objStack[dwCount]=new CSbBonus;
		pBn=(CSbBonus *)g_sBonusBlueShell.m_objStack[dwCount];
		pBn->bActive=FALSE;
		pBn->SetGraphicManager(&g_gm);
		pBn->SetFrameManager(&g_fm);	
		pBn->lpfnExplode=PushBonusBlueShell;

		//immette le singole frame del bonus all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pBn->AddFrame(&bonus5[dwCount1]);
		}
	}

	g_sBonusFwShells.Create(1);

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sBonusFwShells.m_objStack[dwCount]=new CSbBonus;
		pBn=(CSbBonus *)g_sBonusFwShells.m_objStack[dwCount];
		pBn->bActive=FALSE;
		pBn->SetGraphicManager(&g_gm);
		pBn->SetFrameManager(&g_fm);	
		pBn->lpfnExplode=PushBonusFwShells;

		//immette le singole frame del bonus all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pBn->AddFrame(&bonus5[dwCount1]);
		}
	}

	//fiamme ugello
	g_sNozzleFlames.Create(5);

	CAnimatedObject *pa;

	for (dwCount=0;dwCount<5;dwCount++)
	{
		g_sNozzleFlames.m_objStack[dwCount]=new CAnimatedObject();
		g_sNozzleFlames.m_objStack[dwCount]->bActive=FALSE;
		pa=(CAnimatedObject*)g_sNozzleFlames.m_objStack[dwCount];
		pa->SetFrameManager(&g_fm);
		pa->AddFrame(&m_imgNozzleFlame[0]);
		pa->AddFrame(&m_imgNozzleFlame[1]);
		pa->AddFrame(&m_imgNozzleFlame[2]);
		pa->SetAnimSpeed(0.2f);
		pa->SetCycles(0); //ciclo infinito di animazione			

	}

	g_sNozzleFlamesV.Create(5);
    for (dwCount=0;dwCount<5;dwCount++)
	{
		g_sNozzleFlamesV.m_objStack[dwCount]=new CAnimatedObject();
		g_sNozzleFlamesV.m_objStack[dwCount]->bActive=FALSE;
		pa=(CAnimatedObject*)g_sNozzleFlamesV.m_objStack[dwCount];
		pa->SetFrameManager(&g_fm);
		pa->AddFrame(&m_imgNozzleFlameV[0]);
		pa->AddFrame(&m_imgNozzleFlameV[1]);
		pa->AddFrame(&m_imgNozzleFlameV[2]);
		pa->SetAnimSpeed(0.2f);
		pa->SetCycles(0); //ciclo infinito di animazione

	}

	g_sBonus1Up.Create(2);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sBonus1Up.m_objStack[dwCount]=new CSbBonus;
		pobj=g_sBonus1Up.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pBn=(CSbBonus *)pobj;	
		pBn->lpfnExplode=PushBonus1Up;

		//immette le singole frame all'interno dello sprite
		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&bonus5[dwCount1]);
		}

	}

	ProgressBar(g_pbx,g_pby,0.87f,g_pbw);

	g_sBonusThunder.Create(1);
	g_sBonusThunder.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusThunder.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusThunder;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}
	

	g_sBonusMissile.Create(1);
	g_sBonusMissile.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusMissile.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusMissile;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	g_sBonusLaser.Create(1);
	g_sBonusLaser.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusLaser.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusLaser;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	//laser 3 spara 4 p laser in alto basso avanti e indietro
	g_sBonusLaser3.Create(1);
	g_sBonusLaser3.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusLaser3.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusLaser3;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}
	
	//special weapon A
	g_sBonusWpA.Create(1);
	g_sBonusWpA.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusWpA.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusWeaponA;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	//special weapon B
	g_sBonusWpB.Create(1);
	g_sBonusWpB.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusWpB.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusWeaponB;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	ProgressBar(g_pbx,g_pby,0.88f,g_pbw);

	//special weapon C
	g_sBonusWpC.Create(1);
	g_sBonusWpC.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusWpC.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusWeaponC;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	//bonus grnd fire
	g_sBonusGrndFire.Create(1);
	g_sBonusGrndFire.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusGrndFire.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusGrndFire;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}
	
	//fuoco triplo
	g_sBonusTripleFire.Create(1);
	g_sBonusTripleFire.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusTripleFire.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusTripleFire;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}

	
	//missile in avanti
	g_sBonusStrightMissile.Create(1);
	g_sBonusStrightMissile.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusStrightMissile.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusStrightMissile;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}
	


	//special weapon D
	g_sBonusWpD.Create(1);
	g_sBonusWpD.m_objStack[0]=new CSbBonus;
	pobj=g_sBonusWpD.m_objStack[0];
	pobj->bActive=FALSE;
	pobj->SetFrameManager(&g_fm);
	pBn=(CSbBonus *)pobj;
	pBn->lpfnExplode=PushBonusWeaponD;

	for(dwCount1=0;dwCount1<8;dwCount1++)
	{
		pobj->AddFrame(&bonus5[dwCount1]);
	}


	ProgressBar(g_pbx,g_pby,0.89f,g_pbw);

	//descrizioni dei bonus
    if (FAILED(g_sLabelPowerUp.Create(4))) return E_FAIL;

	for (dwCount=0;dwCount<4;dwCount++)
	{
		g_sLabelPowerUp.m_objStack[dwCount]=new CDynString;
		g_sLabelPowerUp.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelPowerUp.m_objStack[dwCount];
		pStr->SetGraphicManager(&g_gm);
		pStr->SetFrameManager(&g_fm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[5]);

	}


	if (FAILED(g_sLabelShield.Create(4))) return E_FAIL;

	for (dwCount=0;dwCount<4;dwCount++)
	{
		g_sLabelShield.m_objStack[dwCount]=new CDynString;
		g_sLabelShield.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelShield.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[4]);
	}
	if (FAILED(g_sLabel1Up.Create(2))) return E_FAIL;


	ProgressBar(g_pbx,g_pby,0.90f,g_pbw);

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sLabel1Up.m_objStack[dwCount]=new CDynString;
		g_sLabel1Up.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabel1Up.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[6]);

	}
	if (FAILED(g_sLabelSpecial.Create(6))) return E_FAIL;

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sLabelSpecial.m_objStack[dwCount]=new CDynString;
		g_sLabelSpecial.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelSpecial.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[7]);
	}

	if (FAILED(g_sLabelThrust.Create(3))) return E_FAIL;

	ProgressBar(g_pbx,g_pby,0.92f,g_pbw);

	for (dwCount=0;dwCount<3;dwCount++)
	{
		g_sLabelThrust.m_objStack[dwCount]=new CDynString;
		g_sLabelThrust.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelThrust.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[10]);
	}

	if (FAILED(g_sLabelEnergy.Create(6))) return E_FAIL;

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sLabelEnergy.m_objStack[dwCount]=new CDynString;
		g_sLabelEnergy.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelEnergy.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[9]);
	}

	if (FAILED(g_sLabelScore.Create(5))) return E_FAIL;

	//etichetta 1000 punti
	for (dwCount=0;dwCount<5;dwCount++)
	{
		g_sLabelScore.m_objStack[dwCount]=new CDynString;
		g_sLabelScore.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelScore.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[8]);
	}


    //etichetta bonus +10 bombe
	if (FAILED(g_sLabelBombs10.Create(2))) return E_FAIL;

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sLabelBombs10.m_objStack[dwCount]=new CDynString;
		g_sLabelBombs10.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelBombs10.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[2]);

	}	

	//etichetta bonus +50 bombe
	if (FAILED(g_sLabelBombs50.Create(2))) return E_FAIL;

	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sLabelBombs50.m_objStack[dwCount]=new CDynString;
		g_sLabelBombs50.m_objStack[dwCount]->bActive=FALSE;
		pStr=(CDynString *)g_sLabelBombs50.m_objStack[dwCount];
		pStr->SetFrameManager(&g_fm);
		pStr->SetGraphicManager(&g_gm);
		pStr->iMode=0;
		pStr->AddFrame(&m_imgBonusCapt[3]);

	}	


	//barriera protettiva
	if (FAILED(g_sShield.Create(24))) return E_FAIL;
	
	for (dwCount=0;dwCount<24;dwCount++)
	{
		g_sShield.m_objStack[dwCount]=new CShield;
		g_sShield.m_objStack[dwCount]->bActive=FALSE;

		pobj=g_sShield.m_objStack[dwCount];

		pobj->SetFrameManager(&g_fm);


		for (dwCount1=0;dwCount1<18;dwCount1++)
		{
			pobj->AddFrame(&m_imgShield[dwCount1]);
		}

		pobj->SetAnimSpeed(0.2f);
		pobj->lpfnCollide=PushExpl1;
	
	}

	//barriera elettrica
	if (FAILED(g_sElectricBarrier.Create(5))) return E_FAIL;

	for (dwCount=0;dwCount<g_sElectricBarrier.ItemCount();dwCount++)
	{
		g_sElectricBarrier.m_objStack[dwCount]=new CAnimatedObject;
		g_sElectricBarrier.m_objStack[dwCount]->bActive=FALSE;

		pobj=g_sElectricBarrier.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);

		pAo=(CAnimatedObject *)g_sElectricBarrier.m_objStack[dwCount];	
		pAo->AddFrame(&m_electric_barrier[0]);
		pAo->AddFrame(&m_electric_barrier[1]);
		pAo->AddFrame(&m_electric_barrier[2]);
		pAo->AddFrame(&m_electric_barrier[3]);
		pAo->lpfnCollide=PushCollideElectro;
		pAo->SetAnimSpeed(0.30f);	
		

	}
	
    //crea uno stack di oggetti game over
	if (FAILED(g_sGameOver.Create(1))) return E_FAIL;

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sGameOver.m_objStack[dwCount]=new CGameOver;
		pobj=g_sGameOver.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pGM=(CGameOver *)pobj;
		pGM->pCharSet=&g_chRed;
		pGM->pQuitLoopFlag=&Player1.bExitGame; //flag da impostare alla fine per far terminare il loop del gioco
		pGM=NULL;
		
	}
    
	ProgressBar(g_pbx,g_pby,0.94f,g_pbw);
    
    //crea le collezioni dei proiettili
	if (FAILED(g_sShell.Create(30))) return E_FAIL;
	if (FAILED(g_sShell1.Create(30))) return E_FAIL;
	if (FAILED(g_sShell2.Create(30))) return E_FAIL;
	if (FAILED(g_sShell3.Create(30))) return E_FAIL;
	if (FAILED(g_sShell4.Create(30))) return E_FAIL;
    
	//crea i contenitori dei proiettili
	for (dwCount=0;dwCount<30;dwCount++)
	{
		g_sShell.m_objStack[dwCount]=new CShell;
		g_sShell.m_objStack[dwCount]->bActive=FALSE;
		pobj= (CShell *)g_sShell.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,9);
		pobj->dwPower=4;
		pobj->SetEnergy(1);
		pobj->AddFrame(&p5); //proiettile piccolo rosa
		pobj->lpfnExplode=PushExpl1; //esplosione del proiettile quando colpisce un altro oggetto


		g_sShell1.m_objStack[dwCount]=new CShell;
		g_sShell1.m_objStack[dwCount]->bActive=FALSE;
		pobj= g_sShell1.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,9);
		pobj->dwPower=8;
		pobj->SetEnergy(1);
		pobj->AddFrame(&p7); //proiettile medio rosa
		pobj->lpfnExplode=PushExpl1;	

		g_sShell2.m_objStack[dwCount]=new CShell;
		g_sShell2.m_objStack[dwCount]->bActive=FALSE;
		pobj= g_sShell2.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,10);
		pobj->dwPower=9;
		pobj->SetInitialEnergy(1);
		pobj->AddFrame(&p1); //proiettile piccolo rosa	
		pobj->lpfnExplode=PushExpl1;
	
		g_sShell3.m_objStack[dwCount]=new CShell;
		g_sShell3.m_objStack[dwCount]->bActive=FALSE;
		pobj= g_sShell3.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,14);
		pobj->dwPower=12;
		pobj->SetInitialEnergy(4);
		pobj->AddFrame(&p21); //proiettile giallo lungo
		pobj->lpfnExplode=PushExpl1;
	
		g_sShell4.m_objStack[dwCount]=new CShell;
		g_sShell4.m_objStack[dwCount]->bActive=FALSE;
		pobj= g_sShell4.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,18);
		pobj->dwPower=18;
		pobj->SetInitialEnergy(8);
		pobj->AddFrame(&p23[0]);
	
		pobj->lpfnExplode=PushExpl1;
		
	}

	if (FAILED(g_sShellBlue.Create(30))) return E_FAIL;
	
	for (dwCount=0;dwCount<30;dwCount++)
	{
	
		g_sShellBlue.m_objStack[dwCount]=new CShell;
		g_sShellBlue.m_objStack[dwCount]->bActive=FALSE;
		pobj= (CShell *)g_sShellBlue.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,16);
		pobj->dwPower=18;
		pobj->SetEnergy(1);
		pobj->AddFrame(&p19); //proiettile piccolo rosa
		pobj->lpfnExplode=PushExpl1; //esplosione del proiettile quando colpisce un altro oggetto
	
	}
	
	ProgressBar(g_pbx,g_pby,0.95f,g_pbw);

	g_sEShell1.Create(8);

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sEShell1.m_objStack[dwCount]=new CShell;
		g_sEShell1.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell1.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,-7);
		pobj->dwPower=1;
		pobj->SetInitialEnergy(1);
		pobj->AddFrame(&p6);
		pobj->lpfnExplode=PushExpl1;

	}

	//proiettili direzionabili gialli
	g_sEShell2.Create(20);

	for (dwCount=0;dwCount<20;dwCount++)
	{
		g_sEShell2.m_objStack[dwCount]=new CSBObject;
		g_sEShell2.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell2.m_objStack[dwCount];		
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,-7); //vel. fittizia
		pobj->dwPower=1;
		pobj->SetInitialEnergy(1);
		pobj->lpfnExplode=PushExpl1;
		pobj->AddFrame(&p17[0]); //0�
		pobj->AddFrame(&p17[7]); //15�
		pobj->AddFrame(&p17[8]); //30�
		pobj->AddFrame(&p17[9]); //45�
		pobj->AddFrame(&p17[10]); //60�
		pobj->AddFrame(&p17[11]); //75;
		pobj->AddFrame(&p17[6]); //90�
		pobj->AddFrame(&p17[5]); //105�
		pobj->AddFrame(&p17[4]); //120�
		pobj->AddFrame(&p17[3]);
		pobj->AddFrame(&p17[2]);
		pobj->AddFrame(&p17[1]);
	}

	//proiettile rotondo piccolo
	g_sEShell3.Create(80);
	
	for (dwCount=0;dwCount<80;dwCount++)
	{
		g_sEShell3.m_objStack[dwCount]=new CSBObject;
		g_sEShell3.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell3.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(180,3);
		pobj->dwPower=2;
		pobj->SetInitialEnergy(1);	
		pobj->lpfnExplode=PushExpl1;
		pobj->SetCurFrame(0);

		hr=pobj->AddFrame(&p15);
		if (FAILED(hr)) return hr;

	}

	//proiettile rosso direzionabile
	g_sEShell4.Create(20);

	for (dwCount=0;dwCount<20;dwCount++)
	{
		g_sEShell4.m_objStack[dwCount]=new CSBObject;
		g_sEShell4.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell4.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,-7); //default
		pobj->dwPower=2;
		pobj->SetInitialEnergy(1);
		pobj->lpfnExplode=PushExpl1; //esplosione
		pobj->SetCurFrame(0);
		pobj->SetType(33);
				
		for (dwCount1=0;dwCount1<24;dwCount1++)
		{
			hr=pobj->AddFrame(&p9[dwCount1]);		
			if (FAILED(hr)) return hr;
		}		
	}
	
	//proiettile piccolo tuchese direzionabile
	if (FAILED(g_sEShell5.Create(25))) return E_FAIL;

	for (dwCount=0;dwCount<25;dwCount++)
	{
		g_sEShell5.m_objStack[dwCount]=new CSBObject;
		g_sEShell5.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell5.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,-8); //default
		pobj->dwPower=3;
		pobj->SetInitialEnergy(2);
		pobj->lpfnExplode=PushExpl1; //funzione esplosione
		pobj->SetCurFrame(0);

		//imposta le frames
		for (dwCount1=0;dwCount1<12;dwCount1++)
		{
			hr=pobj->AddFrame(&p13[dwCount1]);
			if (FAILED(hr)) return hr;
		}

		hr=pobj->AddFrame(&p13[0]); //180�
	}


	if (FAILED(g_sEShell6.Create(48))) return E_FAIL;

	for (dwCount=0;dwCount<48;dwCount++)
	{
		g_sEShell6.m_objStack[dwCount]=new CSBObject;
		g_sEShell6.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell6.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(180,3);
		pobj->dwPower=2;
		pobj->SetInitialEnergy(1);	
		pobj->lpfnExplode=PushExpl1;
		pobj->SetCurFrame(0);

		hr=pobj->AddFrame(&p22);
		if (FAILED(hr)) return hr;
	}

		//proiettile sferico
	if (FAILED(g_sEShell7.Create(48)))
	{
		g_fm.WriteErrorFile("Errore durante la creazione dello stack g_sEShell3");
		return E_FAIL;
	}

	for (dwCount=0;dwCount<48;dwCount++)
	{
		g_sEShell7.m_objStack[dwCount]=new CSBObject;
		g_sEShell7.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell7.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);	
		pobj->SetType(28);
		pobj->SetGraphicManager(&g_gm);
		pobj->SetVelocity(0,4); //reimpotata dall'oggetto che spara
		pobj->dwPower=3;
		pobj->SetInitialEnergy(10);	
		pobj->AddFrame(&p10);
		pobj->lpfnExplode=PushExpl1;
	}

	//falce a sinistra
	if (FAILED(g_sEShell8.Create(10)))
	{
		g_fm.WriteErrorFile("Errore durante la creazione dello stack g_sEShell3");
		return E_FAIL;
	}

	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_sEShell8.m_objStack[dwCount]=new CSBObject;
		g_sEShell8.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell8.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetGraphicManager(&g_gm);
		pobj->SetVelocity(0,4); //reimpotata dall'oggetto che spara
		pobj->dwPower=3;
		pobj->SetInitialEnergy(10);	
		pobj->AddFrame(&p11);
		pobj->lpfnExplode=PushExpl1;
	}

	//falce a destra
	if (FAILED(g_sEShell9.Create(10)))
	{
		g_fm.WriteErrorFile("Errore durante la creazione dello stack g_sEShell3");
		return E_FAIL;
	}

	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_sEShell9.m_objStack[dwCount]=new CSBObject;
		g_sEShell9.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell9.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetGraphicManager(&g_gm);
		pobj->SetVelocity(0,4); //reimpotata dall'oggetto che spara
		pobj->dwPower=3;
		pobj->SetInitialEnergy(10);	
		pobj->AddFrame(&p24);
		pobj->lpfnExplode=PushExpl1;
	}


	ProgressBar(g_pbx,g_pby,0.96f,g_pbw);

	//fiamma
	if (FAILED(g_sEShell10.Create(6)))
	{
		g_fm.WriteErrorFile("Errore durante la creazione dello stack g_sEShell3");
		return E_FAIL;
	}

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sEShell10.m_objStack[dwCount]=new CAnimatedObject;
		g_sEShell10.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell10.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetGraphicManager(&g_gm);
		pobj->SetVelocity(0,4); //reimpotata dall'oggetto che spara
		pobj->dwPower=3;
		pobj->SetInitialEnergy(10);	
		pobj->AddFrame(&p20[0]);
		pobj->AddFrame(&p20[1]);
		pobj->AddFrame(&p20[2]);
		pobj->AddFrame(&p20[3]);
		pobj->lpfnExplode=PushExpl1;
		pobj->SetAnimSpeed(2);
	}

	//proiettile laser rosso
	if (FAILED(g_sEShell11.Create(35)))
	{
		return E_FAIL;
	}

	for (dwCount=0;dwCount<35;dwCount++)
	{
		g_sEShell11.m_objStack[dwCount]=new CSBObject;
		g_sEShell11.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sEShell11.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetGraphicManager(&g_gm);
		pobj->SetVelocity(0,4); //reimpotata dall'oggetto che spara
		pobj->dwPower=3;
		pobj->SetInitialEnergy(10);	

		for (dwCount1=0;dwCount1<12;dwCount1++)
		{
			pobj->AddFrame(dwCount1,&p23[dwCount1]); //proiettile rosso laser lungo			
			pobj->AddFrame(dwCount1+12,&p23[dwCount1]);		
			if (dwCount1<6)  
			{
				pobj->SetFrameHotSpot(dwCount1,0,0,0); //0-90
				pobj->SetFrameHotSpot(dwCount1+12,0,p23[dwCount1].width,p23[dwCount1].height); //90-180

			}
			else
			{
				pobj->SetFrameHotSpot(dwCount1,0,p23[dwCount1].width,0);
				pobj->SetFrameHotSpot(dwCount1+12,0,0,p23[dwCount1].height);
			}
		}

		pobj->lpfnExplode=PushExpl1;
		pobj->SetAnimSpeed(0);
	}

	//fuoco fatuo
	if (FAILED(g_sEShell12.Create(8))) return E_FAIL;

	ProgressBar(g_pbx,g_pby,0.97f,g_pbw);

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sEShell12.m_objStack[dwCount]=new CAnimatedObject;
		pAo=(CAnimatedObject *)g_sEShell12.m_objStack[dwCount];

		pAo->SetGraphicManager(&g_gm);
		pAo->SetFrameManager(&g_fm);
		
		for (dwCount1=0;dwCount1<9;dwCount1++)
		{
			pAo->AddFrame(&p14[dwCount1]);
			pAo->SetFrameHotSpot(dwCount1,0,(int)(p14[dwCount1].width*0.5f),(int)(p14[dwCount1].height*0.5f));
		}

		pAo->SetAnimSpeed(0.11f);
		pAo->SetCurFrame(0);
		pAo->SetCycles(1); //dopo un ciclo di animazione viene eliminato
		pAo->dwPower=20;
		pAo->SetInitialEnergy(8);
		pAo->lpfnExplode=PushExpl1;
	}
	
	//drop bombs
	//Attenzione! per le bombe � obbligatorio settare il puntatore lpfnExplode	
	g_sDBomb1.Create(10);
	g_sDBomb2.Create(10);
	g_sDBomb3.Create(10);

	for (dwCount=0;dwCount<10;dwCount++)
	{
		//crea un set di bombe tipo 1
		g_sDBomb1.m_objStack[dwCount]=new CDropBomb;
		g_sDBomb1.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sDBomb1.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(270,2); 
		pobj->dwPower=8;	
		pobj->SetInitialEnergy(1);
		pobj->AddFrame(&b6[0]); //0�
        pobj->AddFrame(&b6[23]); //345�
		pobj->AddFrame(&b6[22]);
		pobj->AddFrame(&b6[21]);
		pobj->AddFrame(&b6[20]);
		pobj->AddFrame(&b6[19]);
		pobj->AddFrame(&b6[18]); //270� bomba verticale verso il basso
		pobj->lpfnExplode=PushExpl4; //setta il puntatore alla funzione che fa esplodere la bomba
		pDb1=(CDropBomb *)pobj;
	 

		//crea un set di bombe tipo 2
		g_sDBomb2.m_objStack[dwCount]=new CDropBomb;
		g_sDBomb2.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sDBomb2.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(270,2); 
		pobj->dwPower=3; //potere distruttivo	
		pobj->SetInitialEnergy(1);
		pobj->AddFrame(&b7[0]); //0�
        pobj->AddFrame(&b7[23]); //345�
		pobj->AddFrame(&b7[22]);
		pobj->AddFrame(&b7[21]);
		pobj->AddFrame(&b7[20]);
		pobj->AddFrame(&b7[19]);
		pobj->AddFrame(&b7[18]); //270� bomba verticale verso il basso	
		pobj->lpfnExplode=PushExpl4;
		pDb1=(CDropBomb *)pobj;

		//crea un set di bombe tipo 3
		g_sDBomb3.m_objStack[dwCount]=new CDropBomb;
		g_sDBomb3.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sDBomb3.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(270,2); 
		pobj->dwPower=3; //potere distruttivo	
		pobj->SetInitialEnergy(1);
		pobj->AddFrame(&b8[0]); //0�
		pDb1=(CDropBomb *)pobj;
	        
		for (dwCount1=23;dwCount1>0;dwCount1--)
		{
			pobj->AddFrame(&b8[dwCount1]);

		}	
	
		pobj->lpfnExplode=PushExpl4;

	}

	 
	//missile inseguitore
	if (FAILED(g_sFwMissile1.Create(18)))
	{
		g_fm.WriteErrorFile("Errore durante la creazione dello stack g_sFwMissile");
		return E_FAIL;
		
	}

	for (dwCount=0;dwCount<18;dwCount++)
	{
	
		g_sFwMissile1.m_objStack[dwCount]=new CFwMissile; //classe missile inseguitore
		
		g_sFwMissile1.m_objStack[dwCount]->bActive=FALSE; //oggetto non attivo	
	
		pobj=g_sFwMissile1.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(180,4);
		pobj->dwPower=4;
		pobj->dwClass=CL_ENEMY;
	
	
		//nel sistema di riferimento delle coordinate video, una rotazione positiva � in senso orario
		//le frames sono invece messe in sequenza secondo un rotazione antioraria
		pobj->AddFrame(&b4[0]); //0�
		
		for (dwCount1=23;dwCount1>0;dwCount1--)
		{				
			pobj->AddFrame(&b4[dwCount1]);			
		}        		

		pobj->lpfnExplode=PushExpl4;
	
		pobj->SetCurFrame(12);
	
	}

    //esplosione grande 6 esplosioni al massimo in contemporanea
	g_sExplBig.Create(24);

	for (dwCount=0;dwCount<24;dwCount++)
	{
		g_sExplBig.m_objStack[dwCount]=new CExplosion;
		g_sExplBig.m_objStack[dwCount]->bActive=FALSE;
		pobj=g_sExplBig.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->dwPower=1;
		pobj->SetVelocity(0,0);
		pExpl=(CExplosion *)pobj;
		pExpl->SetTrigger(10);
		
		//ogni esplosione ha 15 frames
		for (dwCount1=0;dwCount1<15;dwCount1++)
		{
			pobj->AddFrame(&es_big[dwCount1]);
		}

	}

	//esplsione piccola (quando un proiettile colpisce un oggetto)
	g_sExplSmall.Create(25);

	for (dwCount=0;dwCount<25;dwCount++)
	{
		g_sExplSmall.m_objStack[dwCount]=new CExplosion;
		pobj=g_sExplSmall.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->dwPower=0;
		pobj->SetVelocity(0,0);

		for (dwCount1=0;dwCount1<7;dwCount1++)
		{
			pobj->AddFrame(&es_small[dwCount1]);
		}
	}

	//eplosione media (il nemico viene colpito e distrutto)
	g_sExplMediumHit.Create(25);

	for (dwCount=0;dwCount<25;dwCount++)
	{
		g_sExplMediumHit.m_objStack[dwCount]=new CExplosion;
		pobj=g_sExplMediumHit.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->dwPower=1;
		pobj->SetVelocity(0,0);

		for (dwCount1=0;dwCount1<11;dwCount1++)
		{
			pobj->AddFrame(&es_medium[dwCount1]);

		}
	}

	//esplosione media (il nemico viene distrutto parzialmente oppure una bomba esplode a terra)

	g_sExplMedium.Create(8);

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sExplMedium.m_objStack[dwCount]=new CExplosion;
		pobj=g_sExplMedium.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->dwPower=1;
		pobj->SetVelocity(0,0);

		for (dwCount1=0;dwCount1<3;dwCount1++)
		{
			pobj->AddFrame(&es_medium[dwCount1]);
		}

		//biforcazione
		for (dwCount1=11;dwCount1<17;dwCount1++)
		{
			pobj->AddFrame(&es_medium[dwCount1]);
		}
	}


	//laser
	if (FAILED(g_sLaser.Create(8))) return E_FAIL;
	{
		for (dwCount=0;dwCount<8;dwCount++)
		{
			g_sLaser.m_objStack[dwCount]=new CLaser();
			pL1=(CLaser *)g_sLaser.m_objStack[dwCount];
			pL1->bActive=FALSE;
			pL1->SetFrameManager(&g_fm);
			pL1->SetVelocity(0,0);
			pL1->dwPower=25;
			pL1->SetInitialEnergy(0);
			pL1->AddFrame(&p26);
			pL1->lpfnExplode=NULL;
			//imposta la lunghezza del laser e la dimensione originale
			pL1->SetProps(p26.width-2,CSBObject::g_iScreenWidth+80);
		}
	}

	//detriti
	if (FAILED(g_sDebris1.Create(15))) return E_FAIL;

	for (dwCount=0;dwCount<15;dwCount++)
	{
		g_sDebris1.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris1.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pobj->AddFrame(&m_imgDebris1[dwCount1]);
		}
	}

	if (FAILED(g_sDebris2.Create(10))) return E_FAIL;

	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_sDebris2.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris2.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pobj->AddFrame(&m_imgDebris2[dwCount1]);
		}
	}	
	
	if (FAILED(g_sDebris3.Create(10))) return E_FAIL;

	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_sDebris3.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris3.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<5;dwCount1++)
		{
			pobj->AddFrame(&m_imgDebris3[dwCount1]);
		}
	}

	if (FAILED(g_sDebris4.Create(6))) return E_FAIL;

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sDebris4.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris4.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;
				
		pobj->AddFrame(&m_imgDebris4);
		
	}

	if (FAILED(g_sDebris5.Create(6))) return E_FAIL;

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sDebris5.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris5.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;
		pobj->AddFrame(&m_imgDebris5);		
	}
	
	if (FAILED(g_sDebris6.Create(16))) return E_FAIL;

	for (dwCount=0;dwCount<16;dwCount++)
	{
		g_sDebris6.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris6.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&m_imgDebris6[dwCount1]);
		}
	}
	
	if (FAILED(g_sDebris7.Create(25))) return E_FAIL;

	for (dwCount=0;dwCount<25;dwCount++)
	{
		g_sDebris7.m_objStack[dwCount]=new CDebris;
		pobj=g_sDebris7.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(320,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pobj->AddFrame(&m_imgDebris7[dwCount1]);
		}
	}

	//spalsh
	if (FAILED(g_sSplash.Create(20))) return E_FAIL;
	for (dwCount=0;dwCount<20;dwCount++)
	{
		
		g_sSplash.m_objStack[dwCount]=new CDebris;
		pDb=(CDebris *)g_sSplash.m_objStack[dwCount];
		pDb->bActive=FALSE;
		pDb->SetFrameManager(&g_fm);
		pDb->SetVelocity(1,180);
		pDb->SetG(0);
		pDb->dwClass=CL_NONE;
		pDb->dwPower=0;
		pDb->SetConfigFreq(5);

		for (dwCount1=0;dwCount1<9;dwCount1++)
		{
			pDb->AddFrame(&m_imgSplash[dwCount1]);
			//aggiunge un hot pot in basso nel centro
			pDb->SetFrameHotSpot(dwCount1,0,(LONG)(m_imgSplash[dwCount1].width*0.5),m_imgSplash[dwCount1].height);
		}

	}

	//buco nel ghiaccio 
	if (FAILED(g_sIceHole.Create(20))) return E_FAIL;
	for (dwCount=0;dwCount<20;dwCount++)
	{
		g_sIceHole.m_objStack[dwCount]=new CGroundObject;
		pGr=(CGroundObject *)g_sIceHole.m_objStack[dwCount];
		pGr->dwClass=CL_NONE; 
		pGr->bActive=FALSE;
		pGr->SetFrameManager(&g_fm);
		pGr->dwPower=0;
		pGr->SetInitialEnergy(1); //per mantenerlo "vivo"
		pGr->AddFrame(&m_imgIceHole);
		pGr->SetType(33);

	}

	//impatto su terreno solido
	if (FAILED(g_sGrndImpact.Create(20))) return E_FAIL;
		for (dwCount=0;dwCount<20;dwCount++)
	{
		g_sGrndImpact.m_objStack[dwCount]=new CDebris;
		pDb=(CDebris *)g_sGrndImpact.m_objStack[dwCount];
		pDb->bActive=FALSE;
		pDb->SetFrameManager(&g_fm);
		pDb->SetVelocity(1,180);
		pDb->SetG(0);
		pDb->dwClass=CL_NONE;
		pDb->dwPower=0;
		pDb->SetConfigFreq(5);

		for (dwCount1=0;dwCount1<7;dwCount1++)
		{
			pDb->AddFrame(&m_imgGrndImpact[dwCount1]);
			//aggiunge un hot pot in basso nel centro
			pDb->SetFrameHotSpot(dwCount1,0,(LONG)(m_imgGrndImpact[dwCount1].width*0.5),m_imgGrndImpact[dwCount1].height);
		}

	}

	//fumo
	if (FAILED(g_sSmoke.Create(50))) return E_FAIL;

	for (dwCount=0;dwCount<50;dwCount++)
	{
		g_sSmoke.m_objStack[dwCount]=new CDebris;
		pobj=g_sSmoke.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(270,5);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<9;dwCount1++)
		{
			pobj->AddFrame(&m_imgSmoke[dwCount1]);
		}
	}

	//fumo nero
	if (FAILED(g_sBlackSmoke.Create(40))) return E_FAIL;

	for (dwCount=0;dwCount<40;dwCount++)
	{
		g_sBlackSmoke.m_objStack[dwCount]=new CDebris;
		pobj=g_sBlackSmoke.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(250,1);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pobj->AddFrame(&m_imgBlackSmoke[dwCount1]);
		}
	}

	//fumo bianco spray piccolo
	if (FAILED(g_sWhiteSmoke.Create(100))) return E_FAIL;

	for (dwCount=0;dwCount<100;dwCount++)
	{
		g_sWhiteSmoke.m_objStack[dwCount]=new CDebris();
		pobj=g_sWhiteSmoke.m_objStack[dwCount];
		pobj->bActive=FALSE;
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(0,0);
		pobj->dwPower=0;

		for (dwCount1=0;dwCount1<5;dwCount1++)
		{
			pobj->AddFrame(&m_imgWhiteSmoke[dwCount1]);
		}

	}

	{		
		CFwBullet *p;
	
		//proiettili inseguitori sferici
		if (FAILED(g_sFwBullet.Create(20))) return E_FAIL;

		for (dwCount=0;dwCount<20;dwCount++)
		{
			g_sFwBullet.m_objStack[dwCount]=new CFwBullet();
			p=(CFwBullet*)g_sFwBullet.m_objStack[dwCount];
			p->bActive=FALSE;
			p->SetFrameManager(&g_fm);
			p->lpfnExplode=PushExpl1;
			p->SetSmoke(&g_sWhiteSmoke);
			p->dwPower=7;		
			p->AddFrame(&p25);
		}
	}

	//sorgente di fumo
	if (FAILED(g_sSmokeSrc.Create(8))) return E_FAIL;

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sSmokeSrc.m_objStack[dwCount]=new CSmoke;
		pSmk=(CSmoke *)g_sSmokeSrc.m_objStack[dwCount];
		pSmk->SetSmokeVelocity(250,6); //fumo in alto
		pSmk->SetUpdateFreq(14);
		pSmk->SetSmokeStack(&g_sBlackSmoke);
		pSmk->SetActiveStack(&g_objStack);

	}

	//fiamme
	if (FAILED(g_sFire.Create(15))) return E_FAIL;

	for (dwCount=0;dwCount<g_sFire.ItemCount();dwCount++)
	{
		g_sFire.m_objStack[dwCount]=new CGroundObject;
		g_sFire.m_objStack[dwCount]->bActive=FALSE;
		
		pobj=g_sFire.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->dwClass=CL_NONE;
		pobj->SetVelocity(0,0);
		pobj->SetInitialEnergy(1);
		pobj->SetType(34);

		for (dwCount1=0;dwCount1<8;dwCount1++)
		{
			pobj->AddFrame(&m_imgFire[dwCount1]);
		}

		pobj->SetCurFrame(0);
	}	

	//fuoco amico
	if (FAILED(g_sGyros.Create(6))) return E_FAIL;

	for (dwCount=0;dwCount<g_sGyros.ItemCount();dwCount++)
	{
		g_sGyros.m_objStack[dwCount]=new CGyro;
		g_sGyros.m_objStack[dwCount]->bActive=FALSE;

		pobj=g_sGyros.m_objStack[dwCount];

		pobj->SetFrameManager(&g_fm);

		for (dwCount1=0;dwCount1<12;dwCount1++)
		{
			pobj->AddFrame(&m_imgGyroFrame[dwCount1]);
		}

		pobj->SetCurFrame(4);
		pobj->lpfnExplode=PushExpl8; //esplosione del proiettile quando colpisce un altro oggetto			
		pobj->lpfnCollide=PushExpl1;
		pGyr=(CGyro *)pobj;
		pGyr->Cannon.SetFrameManager(&g_fm);	
		pGyr->SetCartridgeStack(&g_sEShell5);
		
		//imposta le frames per il cannone
		for (dwCount1=0;dwCount1<9;dwCount1++)
		{
			pGyr->Cannon.AddFrame(&m_imgGyroCannon[dwCount1]);

		}
	
		pGyr->Cannon.SetCurFrame(4);
		
	}

	pGyr=NULL;

	//booster
	if (FAILED(g_sBooster.Create(1))) return E_FAIL;

	for (dwCount=0;dwCount<1;dwCount++)
	{
		g_sBooster.m_objStack[dwCount]=new CBooster;
		g_sBooster.m_objStack[dwCount]->bActive=FALSE;
		pBoo=(CBooster *)g_sBooster.m_objStack[dwCount];
		pBoo->m_pLv=&CCurLevel;

		pBoo->SetRamp(35,6,180);
	}

	//thunderbolt (fulmine)
	if (FAILED(g_sThunderBolt.Create(4))) return E_FAIL;

	for (dwCount=0;dwCount<4;dwCount++)
	{
		g_sThunderBolt.m_objStack[dwCount]=new CThunderBolt;
		g_sThunderBolt.m_objStack[dwCount]->bActive=FALSE;
	
		pTh=(CThunderBolt*)g_sThunderBolt.m_objStack[dwCount];

		pTh->SetGraphicManager(&g_gm);
		pTh->SetFrameManager(&g_fm);

		for (dwCount1=0;dwCount1<14;dwCount1++) pTh->AddFrame(&m_imgThunderbolt[dwCount1]);		

		pTh->SetFrameHotSpot(0,0,32,0);//ingresso
		pTh->SetFrameHotSpot(0,1,44,62); //uscita
		pTh->SetFrameHotSpot(1,0,43,0);
		pTh->SetFrameHotSpot(1,1,15,62);
		pTh->SetFrameHotSpot(2,0,32,0);
		pTh->SetFrameHotSpot(2,1,44,62);
		pTh->SetFrameHotSpot(3,0,0,17);
		pTh->SetFrameHotSpot(3,1,64,45);
		pTh->SetFrameHotSpot(4,0,0,36);
		pTh->SetFrameHotSpot(4,1,64,29);
		pTh->SetFrameHotSpot(5,0,0,29);
		pTh->SetFrameHotSpot(5,1,32,62);
		pTh->SetFrameHotSpot(6,0,64,29);
		pTh->SetFrameHotSpot(6,1,32,62);
		pTh->SetFrameHotSpot(7,0,32,0);
		pTh->SetFrameHotSpot(7,1,0,36);
		pTh->SetFrameHotSpot(8,0,33,0);
		pTh->SetFrameHotSpot(8,1,6,62);
		pTh->SetFrameHotSpot(9,0,31,0);
		pTh->SetFrameHotSpot(9,1,56,62);
		pTh->SetFrameHotSpot(10,0,0,32);
		pTh->SetFrameHotSpot(10,1,51,62);
		pTh->SetFrameHotSpot(11,0,0,32);
		pTh->SetFrameHotSpot(12,0,32,0);
		pTh->SetFrameHotSpot(13,0,32,62);		
		pTh->SetFinalFrames(3,11,13,12);

	}

	//Definisce i path di movimento per i nemici
	g_Path1=new TP_PATH_DATA[12];
	//inizializza
	AddPath(180,3,40,g_Path1,TRUE); 
	AddPath(170,3,20);
	AddPath(160,3,25);
	AddPath(150,3,25);
	AddPath(160,3,20);
	AddPath(170,3,10);
	AddPath(180,3,45);
	AddPath(190,3,20);
	AddPath(220,3,40);
	AddPath(230,3,80);
	AddPath(240,3,30);
	AddPath(220,3,180);
		
	g_Path3=new TP_PATH_DATA[17];
	//inizializza
	AddPath(180,4,40,g_Path3,TRUE); 
	AddPath(160,4,10);
	AddPath(150,4,10);
	AddPath(140,4,5);
	AddPath(150,4,10);
	AddPath(160,4,10);
	AddPath(180,4,5);
	AddPath(190,4,5);
	AddPath(200,4,15);
	AddPath(190,4,5);
	AddPath(180,4,10);
	AddPath(180,5,10);
	AddPath(180,6,10);
	AddPath(180,7,10);
	AddPath(180,6,10);
	AddPath(170,3,60);
	AddPath(160,3,120);

	g_Path4=new TP_PATH_DATA[27];
	AddPath(180,4,40,g_Path4,TRUE);
	AddPath(190,4,42);
	AddPath(185,4,5);
	AddPath(180,4,50);
	AddPath(175,4,25);
	AddPath(170,4,10);
	AddPath(160,4,50);
	AddPath(175,3,5);
	AddPath(170,3,5);
	AddPath(180,3,5);
	AddPath(190,3,15);
	AddPath(200,3,5);
	AddPath(210,3,5);
	AddPath(220,3,5);
	AddPath(230,3,5);
	AddPath(240,3,5);
	AddPath(250,3,5);
	AddPath(260,3,5);
	AddPath(270,3,5);
	AddPath(270,2,5);
	AddPath(270,1,4);
	AddPath(270,0,10);
	AddPath(260,1,4);
	AddPath(180,5,10);
	AddPath(180,6,10);
	AddPath(180,7,10);
	AddPath(180,8,120);
    
	g_Path2=new TP_PATH_DATA[70];
	AddPath(180,3,90,g_Path2,TRUE);
	AddPath(170,3,10);
	AddPath(160,3,10);
	AddPath(150,3,5);
	AddPath(140,3,5);
	AddPath(130,3,3);
	AddPath(120,3,3);
	AddPath(110,3,3);
	AddPath(100,3,15);
	AddPath(90,3,5);
	AddPath(80,3,8);
	AddPath(70,3,8);
	AddPath(60,3,8);
	AddPath(50,3,8);
	AddPath(40,3,8);
	AddPath(30,3,8);
	AddPath(20,3,8);
	AddPath(10,3,8);
    AddPath(0,3,8);
	AddPath(350,3,8);
	AddPath(340,3,8);
	AddPath(330,3,8);
	AddPath(320,3,8);
    AddPath(310,3,8);
	AddPath(300,3,8);
	AddPath(290,3,8);
	AddPath(280,3,8);
	AddPath(270,3,45); //verticale in alto
	AddPath(260,3,6);
	AddPath(250,3,6);
	AddPath(240,3,6);
	AddPath(230,3,6);
	AddPath(220,3,6);
	AddPath(210,3,6);
	AddPath(200,3,6);
	AddPath(180,3,80);
	AddPath(170,3,5);
	AddPath(160,3,5);
	AddPath(150,3,5);
	AddPath(140,3,12);
	AddPath(130,3,8);
	AddPath(120,3,6);
	AddPath(110,3,6);
	AddPath(100,3,6);
	AddPath(90,3,6);
	AddPath(80,3,6);
	AddPath(70,3,6);
	AddPath(60,3,6);
	AddPath(50,3,6);
	AddPath(40,3,6);
	AddPath(30,3,6);
	AddPath(20,3,6);
	AddPath(10,3,6);
	AddPath(0,3,6);
	AddPath(350,3,6);
	AddPath(340,3,6);
	AddPath(320,3,6);
	AddPath(310,3,6);
	AddPath(300,3,6);
	AddPath(290,3,6);
	AddPath(280,3,6);
	AddPath(270,3,5);
	AddPath(260,3,10);
	AddPath(250,3,10);
	AddPath(240,3,10);
	AddPath(230,3,56);
	AddPath(220,3,66);
	AddPath(210,3,66);
	AddPath(200,3,144);

	//path per decollo verticale
	g_Path5=new TP_PATH_DATA[19];
	AddPath(270,1,14,g_Path5,TRUE);
	AddPath(270,2,14);
	AddPath(270,3,14);
	AddPath(270,4,14);
	AddPath(270,3,14);
	AddPath(270,2,14);
	AddPath(260,2,8);
	AddPath(250,2,8);
	AddPath(240,2,8);
	AddPath(230,2,8);
	AddPath(220,2,5);
	AddPath(200,2,5);
	AddPath(190,2,5);
	AddPath(180,2,8);
	AddPath(180,3,15);
	AddPath(180,4,15);
	AddPath(180,5,15);
	AddPath(180,6,225);
	AddPath(180,6,120);

	//path per missile nemico (da destra a sinistra)
	g_Path6=new TP_PATH_DATA[15];
	AddPath(100,1,5,g_Path6,TRUE);
	AddPath(110,2,4);
	AddPath(120,2,4);
	AddPath(130,2,6);
	AddPath(140,2,6);
	AddPath(150,2,6);
	AddPath(160,2,8);
	AddPath(170,2,10);
	AddPath(180,3,8);
	AddPath(180,4,8);
	AddPath(180,5,10);
	AddPath(180,6,12);
	AddPath(180,7,14);
	AddPath(180,8,250);
	AddPath(180,8,250);

	//path pr blaster
	g_Path7=new TP_PATH_DATA[11];
	AddPath(180,1,25,g_Path7,TRUE);
	AddPath(180,2,15);
	AddPath(180,3,15);
	AddPath(180,4,15);
	AddPath(180,3,15);
	AddPath(180,2,15);
	AddPath(180,1,15);
	AddPath(180,0,30);
	AddPath(190,2,15);
	AddPath(200,2,20);
	AddPath(210,2,10);

	
	//path per oggetto che torna indietro
	g_Path8=new TP_PATH_DATA[25];
	AddPath(180,3,25,g_Path8,TRUE);
	AddPath(180,3,15);
	AddPath(190,3,20);
	AddPath(200,3,20);
	AddPath(180,3,35);
	AddPath(170,3,12);
	AddPath(160,3,30);
	AddPath(180,2,78);
	AddPath(180,0,150); //fermo
	AddPath(0,1,20);
	AddPath(180,2,20);
	AddPath(0,3,20);
	AddPath(0,4,20);
	AddPath(0,4,20);
	AddPath(0,5,30);
	AddPath(0,4,10);
	AddPath(0,3,10);
	AddPath(0,2,10);
	AddPath(0,0,100);
	AddPath(170,1,30);
	AddPath(160,3,40);
	AddPath(150,4,10);
	AddPath(160,5,10);
	AddPath(180,4,110);
	AddPath(180,3,300);

	//path per nemico veloce
	g_Path9=new TP_PATH_DATA[17];
	AddPath(180,4,15,g_Path9,TRUE);
	AddPath(180,5,25,g_Path9);
	AddPath(190,5,10,g_Path9);
	AddPath(200,5,20,g_Path9);
	AddPath(190,5,5,g_Path9);
	AddPath(180,5,4,g_Path9);
	AddPath(170,5,4,g_Path9);
	AddPath(160,5,14,g_Path9);
	AddPath(150,5,14,g_Path9);
	AddPath(140,5,10,g_Path9);
	AddPath(150,5,4,g_Path9);
	AddPath(160,5,10,g_Path9);
	AddPath(170,5,15,g_Path9);
	AddPath(180,5,10,g_Path9);
	AddPath(180,6,10,g_Path9);
	AddPath(180,6,300,g_Path9);
	AddPath(180,6,300,g_Path9);

	//veloce zig zag
	g_Path10=new TP_PATH_DATA[33];
	AddPath(180,5,10,g_Path10,TRUE);
	AddPath(180,5,75,g_Path10);
	AddPath(180,4,5,g_Path10);
	AddPath(180,3,4,g_Path10);
	AddPath(180,4,4,g_Path10);
	AddPath(180,1,4,g_Path10);
	AddPath(180,0,64,g_Path10);
	AddPath(235,1,5,g_Path10);
	AddPath(235,2,5,g_Path10);
	AddPath(235,3,5,g_Path10);
	AddPath(235,5,14,g_Path10);
	AddPath(235,3,3,g_Path10);
	AddPath(0,5,65,g_Path10);
	AddPath(180,2,10,g_Path10);
	AddPath(185,3,10,g_Path10);
	AddPath(188,4,10,g_Path10);
	AddPath(190,5,40,g_Path10);
	AddPath(185,5,10,g_Path10);
	AddPath(180,5,10,g_Path10);
	AddPath(30,0,40,g_Path10);
	AddPath(30,1,5,g_Path10);
	AddPath(30,3,10,g_Path10);
	AddPath(30,4,10,g_Path10);
	AddPath(30,5,30,g_Path10);
	AddPath(180,0,60,g_Path10);
	AddPath(180,1,20,g_Path10);
	AddPath(175,2,20,g_Path10);
	AddPath(180,3,20,g_Path10);
	AddPath(180,4,40,g_Path10);
	AddPath(180,5,50,g_Path10);
	AddPath(188,5,30,g_Path10);
	AddPath(180,5,300,g_Path10);
	AddPath(180,5,300,g_Path10);

	//path per il serpente
	g_SnakePath1=new TP_PATH_DATA[70];
	AddPath(180,3,90,g_SnakePath1,TRUE);
	AddPath(160,3,10);
	AddPath(150,3,10);
	AddPath(140,3,5);
	AddPath(150,3,5);
	AddPath(140,3,3);
	AddPath(130,3,3);
	AddPath(120,3,3);
	AddPath(110,3,3);
	AddPath(90,3,15);
	AddPath(95,3,3);
	AddPath(100,3,8);
	AddPath(110,3,8);
	AddPath(115,3,8);
	AddPath(130,3,8);
	AddPath(160,3,8);
	AddPath(170,3,3);
	AddPath(180,3,18);
    AddPath(200,3,8);
	AddPath(230,3,8);
	AddPath(240,3,8);
	AddPath(250,3,8);
	AddPath(290,3,8);
    AddPath(300,3,8);
	AddPath(310,3,8);
	AddPath(330,3,8);
	AddPath(0,3,18);
	AddPath(20,3,15); //verticale in alto
	AddPath(30,3,6);
	AddPath(40,3,6);
	AddPath(50,3,6);
	AddPath(70,3,6);
	AddPath(90,3,6);
	AddPath(100,3,6);
	AddPath(110,3,6);
	AddPath(140,3,80);
	AddPath(170,3,5);
	AddPath(180,3,5);
	AddPath(195,3,5);
	AddPath(180,3,12);
	AddPath(170,3,8);
	AddPath(175,3,6);
	AddPath(180,3,16);
	AddPath(190,3,6);
	AddPath(180,3,6);
	AddPath(170,3,16);
	AddPath(160,3,16);
	AddPath(140,3,6);
	AddPath(130,3,10);
	AddPath(100,3,6);
	AddPath(90,3,6);
	AddPath(80,3,6);
	AddPath(70,3,6);
	AddPath(60,3,6);
	AddPath(50,3,6);
	AddPath(40,3,6);
	AddPath(30,3,10);
	AddPath(20,3,10);
	AddPath(10,3,16);
	AddPath(0,3,6);
	AddPath(330,3,6);
	AddPath(320,3,5);
	AddPath(310,3,5);
	AddPath(300,3,5);
	AddPath(260,3,5);
	AddPath(250,3,5);
	AddPath(220,3,12);
	AddPath(200,3,5);
	AddPath(180,3,144);

	g_SnakePath2=new TP_PATH_DATA[70];
	AddPath(180,3,20,g_SnakePath2,TRUE);
	AddPath(170,3,90);
	AddPath(160,3,10);
	AddPath(150,3,5);
	AddPath(140,3,5);
	AddPath(130,3,3);
	AddPath(120,3,3);
	AddPath(130,3,3);
	AddPath(140,3,3);
	AddPath(150,3,15);
	AddPath(160,3,3);
	AddPath(170,3,8);
	AddPath(160,3,8);
	AddPath(150,3,13);
	AddPath(140,3,13);
	AddPath(130,3,8);
	AddPath(120,3,23);
	AddPath(110,3,18);
    AddPath(100,3,8);
	AddPath(90,3,8);
	AddPath(80,3,8);
	AddPath(70,3,8);
	AddPath(60,3,8);
    AddPath(50,3,8);
	AddPath(40,3,8);
	AddPath(30,3,18);
	AddPath(20,4,5);
	AddPath(10,4,5); //verticale in alto
	AddPath(0,4,5);
	AddPath(350,4,5);
	AddPath(340,4,26);
	AddPath(330,3,26);
	AddPath(320,3,6);
	AddPath(300,3,9);
	AddPath(280,3,9);
	AddPath(270,3,4);
	AddPath(260,3,6);
	AddPath(250,3,5);
	AddPath(240,3,5);
	AddPath(230,3,22);
	AddPath(220,3,8);
	AddPath(210,3,6);
	AddPath(200,3,6);
	AddPath(190,3,12);
	AddPath(180,3,36);
	AddPath(170,3,16);
	AddPath(160,3,6);
	AddPath(150,3,6);
	AddPath(160,3,20);
	AddPath(170,3,6);
	AddPath(180,3,6);
	AddPath(190,3,6);
	AddPath(200,3,6);
	AddPath(210,3,6);
	AddPath(220,3,6);
	AddPath(230,3,6);
	AddPath(220,3,10);
	AddPath(210,3,10);
	AddPath(200,3,16);
	AddPath(190,3,6);
	AddPath(180,3,16);
	AddPath(150,3,5);
	AddPath(160,3,5);
	AddPath(170,3,5);
	AddPath(180,3,5);
	AddPath(190,3,12);
	AddPath(180,3,6);
	AddPath(170,3,12);
	AddPath(180,3,144);

	g_SnakePath3=new TP_PATH_DATA[70];
	AddPath(180,3,90,g_SnakePath3,TRUE);
	AddPath(170,3,10);
	AddPath(160,3,10);
	AddPath(150,3,5);
	AddPath(160,3,5);
	AddPath(170,3,3);
	AddPath(180,3,3);
	AddPath(190,3,23);
	AddPath(200,3,23);
	AddPath(190,3,15);
	AddPath(180,3,3);
	AddPath(170,3,8);
	AddPath(160,3,8);
	AddPath(120,3,13);
	AddPath(110,3,13);
	AddPath(100,3,8);
	AddPath(90,3,5);
	AddPath(80,3,5);
    AddPath(70,3,5);
	AddPath(60,3,5);
	AddPath(50,3,5);
	AddPath(40,3,3);
	AddPath(30,3,3);
    AddPath(20,3,3);
	AddPath(10,3,3);
	AddPath(0,3,8);
	AddPath(30,3,5);
	AddPath(40,3,5); //verticale in alto
	AddPath(50,3,15);
	AddPath(60,3,15);
	AddPath(70,3,26);
	AddPath(80,3,26);
	AddPath(90,3,6);
	AddPath(120,3,9);
	AddPath(140,3,9);
	AddPath(120,3,19);
	AddPath(150,3,6);
	AddPath(160,3,5);
	AddPath(170,3,15);
	AddPath(180,3,22);
	AddPath(190,3,8);
	AddPath(200,3,16);
	AddPath(220,3,6);
	AddPath(250,3,12);
	AddPath(260,3,16);
	AddPath(270,3,6);
	AddPath(280,3,16);
	AddPath(290,3,6);
	AddPath(330,3,20);
	AddPath(0,3,6);
	AddPath(10,3,6);
	AddPath(20,3,6);
	AddPath(30,3,6);
	AddPath(40,3,16);
	AddPath(30,3,6);
	AddPath(40,3,16);
	AddPath(70,3,10);
	AddPath(80,3,10);
	AddPath(90,3,16);
	AddPath(110,3,6);
	AddPath(130,3,26);
	AddPath(150,3,5);
	AddPath(160,3,15);
	AddPath(170,3,5);
	AddPath(180,3,5);
	AddPath(190,3,12);
	AddPath(180,3,6);
	AddPath(170,3,12);
	AddPath(180,3,144);


	
		//missile nemico va da destra a sinistra seguendo un path di accelerazione
	g_sSMissile.Create(10);

	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_sSMissile.m_objStack[dwCount]=new CMissile;
		g_sSMissile.m_objStack[dwCount]->bActive=FALSE;
		
		pobj=g_sSMissile.m_objStack[dwCount];
		pobj->SetFrameManager(&g_fm);
		pobj->SetVelocity(180,4);
		pobj->dwClass=CL_ENEMY;

		pobj->AddFrame(&b8[0]);
		pEn=(CEnemy *)g_sSMissile.m_objStack[dwCount];
		
		pEn->SetPath(g_Path6,15); 
		pEn->SetUpdateFreq(2);

		pmis=(CMissile *)g_sSMissile.m_objStack[dwCount];

		pmis->SetSmokeStack(&g_sSmoke);

		pmis->SetActiveStack(&g_objStack);
		
		for (dwCount1=23;dwCount1>0;dwCount1--)
		{
			pobj->AddFrame(&b8[dwCount1]);				
		}

		pobj->SetCurFrame(12); //frame a 180�
		pobj->lpfnExplode=PushExpl3;
	}

    //Autmentare la dimensione del vettore se si aggiungono elemente
	//Si deve anche impostare il nuovo numero di elementi del path quando si chiama SetPath nella definizione del nemico     
	
    //crea i nemici
	
	//1]UFO.

	g_sUfo.Create(8);

	for (dwCount=0;dwCount<8;dwCount++)
	{
		g_sUfo.m_objStack[dwCount]=new CEnemy;
		g_sUfo.m_objStack[dwCount]->bActive=FALSE;
		pEn=(CEnemy *)g_sUfo.m_objStack[dwCount];
		pEn->dwResId=EN_UFO;
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->SetVelocity(180,1);
		pEn->dwPower=1;
		pEn->dwClass=CL_ENEMY; //il tipo serve a determinare con cosa si puo' scontrare (funzione DetectCollisions)
		pEn->dwScore=100; //100 punti !!
		pEn->SetFireSound(g_snFire13);
		//aggiunge le frame allo sprite
		pEn->AddFrame(&m_imgUfo[0]);
		pEn->AddFrame(&m_imgUfo[1]);
		pEn->AddFrame(&m_imgUfo[2]);
		pEn->AddFrame(&m_imgUfo[3]);
		pEn->AddFrame(&m_imgUfo[4]);
		pEn->AddFrame(&m_imgUfo[5]);
		pEn->AddFrame(&m_imgUfo[6]);
		pEn->AddFrame(&m_imgUfo[7]);
		pEn->AddFrame(&m_imgUfo[8]);

		//path di default
		pEn->SetPath(g_Path1,12);

		//path addizionali
		pEn->SetAuxPath(0,g_Path1,12); //flags=1
		pEn->SetAuxPath(1,g_Path4,27); //flags=2
		pEn->SetAuxPath(2,g_Path8,25); //torna indietro 3
		pEn->SetAuxPath(3,g_Path3,17); //flags=4 
		pEn->SetAuxPath(4,g_Path10,33); //flags=5
	
		pEn->iStartFireFreq=50;
		pEn->iBaseFrame=4;
		pEn->SetInitialEnergy(4);
		pEn->pfnSmoke=PushSmoke0;
		pEn->SetUpdateFreq(2);
		pEn->lpfnExplode=PushExpl4;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(&g_sEShell1);
		pEn->SetCurFrame(4);//frame corrente
	}
	 
	//2]GOLDEN FIGHTER

	ProgressBar(g_pbx,g_pby,0.98f,g_pbw);

	g_sGoldenFighter.Create(6); //crea sei oggetti di questo tipo

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sGoldenFighter.m_objStack[dwCount]=new CGoldenFighter;
		g_sGoldenFighter.m_objStack[dwCount]->bActive=FALSE;
		pEn=(CGoldenFighter *)g_sGoldenFighter.m_objStack[dwCount];
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->dwResId=EN_GOLDENFIGHTER;
		pEn->SetVelocity(180,2);
		pEn->dwPower=2;
		pEn->SetFireSound(g_snFire13);
		pEn->dwClass=CL_ENEMY;
		pEn->dwScore=200;
		pEn->AddFrame(&m_gf[0]);
		pEn->AddFrame(&m_gf[1]);
		pEn->AddFrame(&m_gf[2]);
		pEn->AddFrame(&m_gf[3]);
		pEn->AddFrame(&m_gf[4]);
		pEn->AddFrame(&m_gf[5]);
		pEn->AddFrame(&m_gf[6]);
		pEn->AddFrame(&m_gf[7]);
		pEn->AddFrame(&m_gf[8]);
		pEn->AddFrame(&m_gf[9]);
		pEn->SetPath(g_Path1,12); //imposta il path (viene usato solo in certi casi)
		pEn->iStartFireFreq=45;
		pEn->SetUpdateFreq(2);
		pEn->iBaseFrame=4;
		pEn->SetFireSound(g_snFire11);
		pEn->SetInitialEnergy(8);
		pEn->lpfnExplode=PushExpl9;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(&g_sEShell1); //arma primaria
		pEn->SetWeapon1(&g_sFwMissile1); //arma ausiliaria: missili inseguitori 1
		pEn->SetCurFrame(4);//frame corrente
	}
	 
	//3] CONTRAEREA N� 1
	g_sAntiAircraft.Create(5);
	
	for (dwCount=0;dwCount<5;dwCount++)
	{
		g_sAntiAircraft.m_objStack[dwCount]=new CAntiAircraft;
		g_sAntiAircraft.m_objStack[dwCount]->bActive=FALSE;
		pEn=(CAntiAircraft *)g_sAntiAircraft.m_objStack[dwCount];
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->SetVelocity(180,1);
		pEn->dwPower=2;
		pEn->dwResId=EN_ANTIAIRCRAFT;
		pEn->dwClass=CL_ENEMY;
		pEn->dwScore=500;
		pEn->AddFrame(&m_gaf[0]);
		pEn->AddFrame(&m_gaf[1]);
		pEn->AddFrame(&m_gaf[2]);
		pEn->AddFrame(&m_gaf[3]); //frame oggetto distrutto			
		pEn->iStartFireFreq=85;
		pEn->SetUpdateFreq(8);
		pEn->iBaseFrame=0;
		pEn->SetInitialEnergy(10);
		pEn->lpfnExplode=PushExpl5;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(&g_sEShell2); //arma primaria	
		pEn->SetCurFrame(0);//frame corrente

	}

	//4] RADAR
	g_sRadar.Create(5);

	for (dwCount=0;dwCount<5;dwCount++)
	{
		g_sRadar.m_objStack[dwCount]=new CRadar;
		g_sRadar.m_objStack[dwCount]->bActive=FALSE;
		pEn=(CRadar *)g_sRadar.m_objStack[dwCount];
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->SetVelocity(180,1);
		pEn->dwPower=7;
		pEn->dwResId=EN_RADAR;
		pEn->dwClass=CL_ENEMY;
		pEn->dwScore=700;
		
		for (dwCount1=0;dwCount1<11;dwCount1++)
		{
			pEn->AddFrame(&m_imgRadar[dwCount1]);
		}				
        
		pEn->iStartFireFreq=0;
		pEn->SetUpdateFreq(8);
		pEn->iBaseFrame=0;
		pEn->SetInitialEnergy(20);	
		pEn->lpfnExplode=PushExpl6;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(NULL); //arma primaria	
		pEn->SetCurFrame(0);//frame corrente
		pEn->AddWoundedState(5,9);
		pEn->AddWoundedState(2,10);
		pEn->SetAnimSpeed(0.6f);
		pEn->SetAnimRange(0,8); //le frame della sequenza animata vanno da 0 a 8 le altre visualizzano l'oggetto danneggiato
		pEn->SetAnimLoopType(ANIM_TO_AND_FRO); //animazione avanti e indietro
		
	}
  

	//6]X-Wing
	g_sXWing.Create(12);

	for (dwCount=0;dwCount<12;dwCount++)
	{
		g_sXWing.m_objStack[dwCount]=new CEnemy;
		g_sXWing.m_objStack[dwCount]->bActive=FALSE;
		pEn=(CEnemy *)g_sXWing.m_objStack[dwCount];
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->SetVelocity(180,1);
		pEn->dwPower=1;	
		pEn->SetFireSound(g_snFire11);
		pEn->dwResId=EN_XWING;
		pEn->dwClass=CL_ENEMY; //il tipo serve a determinare con cosa si puo' scontrare (funzione DetectCollisions)
		pEn->dwScore=100; //100 punti !!
		//aggiunge le frame allo sprite
		
		pEn->AddFrame(&m_imgXWing[0]);
		pEn->AddFrame(&m_imgXWing[1]);
		pEn->AddFrame(&m_imgXWing[2]);
		pEn->AddFrame(&m_imgXWing[3]);
		pEn->AddFrame(&m_imgXWing[4]);
		pEn->AddFrame(&m_imgXWing[5]);
		pEn->AddFrame(&m_imgXWing[6]);
		pEn->AddFrame(&m_imgXWing[7]);
		pEn->AddFrame(&m_imgXWing[8]);
		if ((dwCount % 2)>0) pEn->SetPath(g_Path4,27);
		else pEn->SetPath(g_Path3,17);			
		pEn->iStartFireFreq=30;
		pEn->iBaseFrame=4;
		pEn->pfnSmoke=PushSmoke0;
		pEn->SetInitialEnergy(7);
		pEn->lpfnExplode=PushExpl4;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(&g_sEShell1);
		pEn->SetCurFrame(4);//frame corrente		
	}

	//7]Lambda Fighter

	g_sLambda.Create(6);

	for (dwCount=0;dwCount<6;dwCount++)
	{
		g_sLambda.m_objStack[dwCount]=new CLambda;
		g_sLambda.m_objStack[dwCount]->bActive=FALSE;
		pL=(CLambda *)g_sLambda.m_objStack[dwCount];
		pEn=(CEnemy *)g_sLambda.m_objStack[dwCount];
		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);
		pEn->SetVelocity(180,1);
		pEn->dwPower=4;	
		pEn->dwResId=EN_LAMBDAFIGHTER;
		pEn->dwClass=CL_ENEMY; //il tipo serve a determinare con cosa si puo' scontrare (funzione DetectCollisions)
		pEn->dwScore=100; //100 punti !!
		//aggiunge le frame allo sprite		
		
		
		pEn->AddFrame(&m_imgLambda[0]);
		pEn->AddFrame(&m_imgLambda[1]);
		pEn->AddFrame(&m_imgLambda[2]);
		pEn->AddFrame(&m_imgLambda[3]);
		pEn->AddFrame(&m_imgLambda[4]);
		pEn->AddFrame(&m_imgLambda[5]);
		pEn->AddFrame(&m_imgLambda[6]);
		pEn->AddFrame(&m_imgLambda[7]);
		pEn->AddFrame(&m_imgLambda[8]);	
		pEn->SetPath(g_Path1,12);		
		pEn->SetAuxPath(0,g_Path1,12);
		pEn->SetAuxPath(1,g_Path3,17);
		pEn->SetAuxPath(2,g_Path4,27);
		pEn->SetAuxPath(3,g_Path5,19); //path per decollo da terra
		pEn->iStartFireFreq=120;
		pEn->iBaseFrame=4;
		pEn->SetInitialEnergy(7);
		pEn->lpfnExplode=PushExpl4;
		//Imposta lo stack attivo e  lo stack proiettili
		pEn->SetActiveStack(&g_objStack);
		pEn->SetCartridgeStack(&g_sEShell4);
		pEn->SetCurFrame(4);//frame corrente	
		
		//impostazioni specifiche
		pEn->SetFlags(1); //per default decolla da terra
		

	}
	
	//8] MFighter
	g_sMFighter.Create(10);
	{
		for (dwCount=0;dwCount<10;dwCount++)
		{
			g_sMFighter.m_objStack[dwCount]=new CMFighter;
			g_sMFighter.m_objStack[dwCount]->bActive=FALSE;
			pEn=(CEnemy *)g_sMFighter.m_objStack[dwCount];
			pEn->SetGraphicManager(&g_gm);
			pEn->SetFrameManager(&g_fm);
			pEn->dwResId=EN_MFIGHTER;
			for (dwCount1=0;dwCount1<9;dwCount1++)
			{
				pEn->AddFrame(&m_imgMFighter[dwCount1]);

			}				
			
			pEn->SetVelocity(180,1);
			pEn->SetPath(g_Path3,17);		
			pEn->iStartFireFreq=120;
			pEn->iBaseFrame=4;
			pEn->SetInitialEnergy(7);
			pEn->dwPower=7;		
			pEn->SetFireSound(g_snBlip);
			pEn->dwClass=CL_ENEMY;
			pEn->dwScore=200;
			pEn->lpfnExplode=PushExpl4;
			pEn->SetCurFrame(4);
			pEn->SetActiveStack(&g_objStack);
		    pEn->SetCartridgeStack(&g_sEShell3);
			pEn->SetWeapon1(&g_sSMissile);
		}
	}
	
	//9]Blaster
	g_sBlaster.Create(5);
	{
		for (dwCount=0;dwCount<5;dwCount++)
		{
			g_sBlaster.m_objStack[dwCount]=new CBlaster;
			g_sBlaster.m_objStack[dwCount]->bActive=FALSE;
			pEn=(CEnemy *)g_sBlaster.m_objStack[dwCount];
			pEn->SetGraphicManager(&g_gm);
			pEn->SetFrameManager(&g_fm);
			pEn->dwResId=EN_BLASTER;
			for (dwCount1=0;dwCount1<11;dwCount1++)
			{
				pEn->AddFrame(dwCount1,&m_imgBlaster[dwCount1]);
			}

			pEn->SetVelocity(180,1);
			pEn->SetPath(g_Path7,11);
			pEn->iStartFireFreq=100;
			pEn->SetUpdateFreq(4);
			pEn->iBaseFrame=0;
			pEn->iStartFireFreq=30;
			pEn->dwPower=10;
			pEn->SetInitialEnergy(20);		
			pEn->dwClass=CL_ENEMY;
			pEn->SetCartridgeStack(&g_sEShell6);
			pEn->SetWeapon1(&g_sEShell4);
			pEn->dwScore=1000;
			pEn->lpfnExplode=PushExpl2;
			pEn->SetActiveStack(&g_objStack);
			pEn->SetCartridgeStack(&g_sEShell3);

		}
	}

	//10]portaerei
	if (FAILED(g_sAircraftCarrier.Create(2))) return E_FAIL;
	
	for (dwCount=0;dwCount<2;dwCount++)
	{
		g_sAircraftCarrier.m_objStack[dwCount]=new CShip;
		g_sAircraftCarrier.m_objStack[dwCount]->bActive=FALSE;
		pSh=(CShip *)g_sAircraftCarrier.m_objStack[dwCount];
		pSh->dwResId=EN_AIRCRAFTCARRIER;
		pSh->SetGraphicManager(&g_gm);
		pSh->SetFrameManager(&g_fm);
		pSh->AddFrame(&m_imgAircraftCarrier);
		pSh->dwClass=CL_ENEMY;
		pSh->dwScore=5000;
		pSh->lpfnExplode=PushExpl5;
		pSh->SetActiveStack(&g_objStack);
		pSh->SetWakeStack(m_imgWake,5);
		pSh->SetUpdateFreq(8);
		pSh->SetAAStack(&g_sAntiAircraft2);//stack torrette contraeree
		pSh->SetNumAA(2);
		pSh->SetInitialEnergy(35);
		pGr=(CGroundObject *)g_sAircraftCarrier.m_objStack[dwCount];
		pGr->AddWoundedState(12,0); //aggiunge una soglia di danneggiamento in corrispondenza di dwEnergy=6
		pGr->SetVelocity(180,1);

	}

	//11]contraerea 2
	if (FAILED(g_sAntiAircraft2.Create(6))) return E_FAIL;
	{
		for (dwCount=0;dwCount<6;dwCount++)
		{
			g_sAntiAircraft2.m_objStack[dwCount]=new CAntiAircraft2;
			g_sAntiAircraft2.m_objStack[dwCount]->bActive =FALSE;
			pAA2=(CAntiAircraft2 *)g_sAntiAircraft2.m_objStack[dwCount];
			pAA2->SetGraphicManager(&g_gm);
			pAA2->dwResId=EN_ANTIAIRCRAFT2;
			pAA2->SetFrameManager(&g_fm);
			pAA2->AddFrame(&m_gaf2[0]);
			pAA2->AddFrame(&m_gaf2[1]);
			pAA2->AddFrame(&m_gaf2[2]);
			pAA2->lpfnExplode=PushExpl1; 
			pAA2->SetWeapon1(&g_sEShell5);
			pAA2->AddWoundedState(8,1); //primo livello di danneggiamento
			pAA2->AddWoundedState(4,2); //secondo livello di danneggiamento
			pAA2->SetBurst(5,7);
			pAA2->iStartFireFreq=65;
			pAA2->SetVelocity(0,0);
			pAA2->SetToleranceAngle(15);
			pAA2->SetActiveStack(&g_objStack);
			pAA2->SetSmokeStack(&g_sSmoke);
			pAA2->m_sprCannon.SetFrameManager(&g_fm);
			pAA2->m_sprCannon.SetBlitEffects(DDBLT_WAIT | DDBLT_KEYSRC);
			
			for (dwCount1=0;dwCount1<13;dwCount1++)
			{		
				pAA2->m_sprCannon.AddFrame(dwCount1,&m_imgRotCannon[dwCount1]);
				if (dwCount1<=6)
				{
					//imposta l'asse di rotazione sul vertice inferiore sinistro
					pAA2->m_sprCannon.SetFrameHotSpot(dwCount1,0,0,m_imgRotCannon[dwCount1].height);
					//punto di uscita del proiettile
					pAA2->m_sprCannon.SetFrameHotSpot(dwCount1,1,m_imgRotCannon[dwCount1].width-9,0);

				}
				else
				{
					//imposta l'asse di rotazione sul vertice inferiore destro
					pAA2->m_sprCannon.SetFrameHotSpot(dwCount1,0,m_imgRotCannon[dwCount1].width,m_imgRotCannon[dwCount1].height);
					pAA2->m_sprCannon.SetFrameHotSpot(dwCount1,1,0,0);
				}
			}

			pAA2->SetBaseCannonFrame(6); //frame che corrisponde a 270�
			pAA2->m_pfm =&g_fmath; //engine matematico
			pAA2->SetUpdateFreq(8);

		}
	}

	ProgressBar(g_pbx,g_pby,0.99f,g_pbw);

	//12]Incrociatore
	if (FAILED(g_sShip.Create(3))) return E_FAIL;

	for (dwCount=0;dwCount<g_sShip.ItemCount();dwCount++)
	{
		g_sShip.m_objStack[dwCount]=new CShip;
		g_sShip.m_objStack[dwCount]->bActive=FALSE;
		pSh=(CShip *)g_sShip.m_objStack[dwCount];
		pSh->SetGraphicManager(&g_gm);
		pSh->SetFrameManager(&g_fm);
		pSh->dwResId=EN_SHIP;
		pSh->AddFrame(&m_imgShip[0]);
		pSh->AddFrame(&m_imgShip[1]);
		pSh->dwClass=CL_ENEMY;
		pSh->dwScore=3000;
		pSh->iStartFireFreq=75;
		pSh->lpfnExplode=PushExpl5;
		pSh->SetActiveStack(&g_objStack);
		pSh->SetWakeStack(m_imgWake,5);
		pSh->SetUpdateFreq(4);		
		pSh->SetAAStack(&g_sAntiAircraft2);//stack torrette contraeree
		pSh->SetNumAA(1); //numero di torrette presenti
		pSh->SetWeapon1(&g_sFwMissile1); //arma ausiliaria: missili inseguitori 1
		pSh->SetInitialEnergy(28);
		pSh->SetAnimSpeed(0.0f);
		pGr=(CGroundObject *)g_sShip.m_objStack[dwCount];
		pGr->AddWoundedState(15,1);
		pGr->SetVelocity(180,1);
	}

	//13]Ufo 2
	if (FAILED(g_sUfo2.Create(8))) return E_FAIL;

	for (dwCount=0;dwCount<g_sUfo2.ItemCount();dwCount++)
	{
		g_sUfo2.m_objStack[dwCount]=new CUfo2;
		g_sUfo2.m_objStack[dwCount]->bActive=FALSE;
	
		pU2=(CUfo2 *)g_sUfo2.m_objStack[dwCount];
		pU2->SetGraphicManager(&g_gm);
		pU2->dwResId=EN_UFO2;
		pU2->SetFrameManager(&g_fm);
		pU2->SetCartridgeStack(&g_sEShell6);
		pU2->SetWeapon1(&g_sDBomb2);
		
		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pU2->AddFrame(&m_imgUfo2[dwCount1]);
		}

		pU2->dwScore=1000;
		pU2->iStartFireFreq=20;
		pU2->lpfnExplode=PushExpl3;
		pU2->SetActiveStack(&g_objStack);
		pU2->SetUpdateFreq(18);
		pU2->SetInitialEnergy(20);
		pU2->iStartFireFreq=80;
		pU2->SetBurst(3,5);
		pU2->m_pfm=&g_fmath;
		pU2->pfnSmoke=PushSmoke0;
		pU2->SetAI(3);


	}

	//14]Carro armato

	if (FAILED(g_sTank.Create(4))) return E_FAIL;

	for (dwCount=0;dwCount<4;dwCount++)
	{
		g_sTank.m_objStack[dwCount]=new CTank;
		g_sTank.m_objStack[dwCount]->bActive=FALSE;
		pTk=(CTank *)g_sTank.m_objStack[dwCount];
		pTk->SetFrameManager(&g_fm);
		pTk->SetGraphicManager(&g_gm);
		pTk->SetWeapon1(NULL);
		pTk->dwResId=EN_TANK;
		
		//torretta del carro
		for (dwCount1=0;dwCount1<3;dwCount1++)
		{
			pTk->AddFrame(&m_imgTank2[dwCount1]);
		}

		pTk->m_sprTrackCrawler.SetFrameManager(&g_fm);
		pTk->m_sprTrackCrawler.SetGraphicManager(&g_gm);
		pTk->m_sprTrackCrawler.SetCurFrame(0);
		pTk->lpfnExplode=PushExpl3; //esplosione
		pTk->iStartFireFreq=80;
		pTk->SetActiveStack(&g_objStack); //stack degli oggetti attivi
		pTk->SetAAStack(&g_sAntiAircraft2);//stack della torretta con il cannone
		pTk->AddWoundedState(8,2); //primo livello di danneggiamento
		pTk->AddWoundedState(4,2); //secondo livello di danneggiamento
		pTk->SetExtents(200,900); //area di movimento
		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pTk->m_sprTrackCrawler.AddFrame(&m_imgTank1[dwCount1]);
		}

	}

	//15]Screw_fighter

	if (FAILED(g_sScrewFighter.Create(8))) return E_FAIL;

	for (dwCount=0;dwCount<g_sScrewFighter.ItemCount();dwCount++)
	{
		g_sScrewFighter.m_objStack[dwCount]=new CScrewFighter();
		g_sScrewFighter.m_objStack[dwCount]->bActive=FALSE;

		pEn=(CEnemy *)g_sScrewFighter.m_objStack[dwCount];

		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);


		//la frame bse � quella con indice 6
		for (lCount=6;lCount>=0;lCount--)
		{
			pEn->AddFrame(&m_imgScrewFighter[lCount]);
			//usa l'asse di rotzione orizzontale come hot spot
			pEn->SetFrameHotSpot(pEn->GetMaxFramesIndex(),0,0,(LONG)(m_imgScrewFighter[lCount].height *0.5f));

		}

		//aggiunge le frame
		for (lCount=11;lCount>=7;lCount--)
		{
			pEn->AddFrame(&m_imgScrewFighter[lCount]);
			//usa l'asse di rotzione orizzontale come hot spot
			pEn->SetFrameHotSpot(pEn->GetMaxFramesIndex(),0,0,(LONG)(m_imgScrewFighter[lCount].height *0.5f));

		}

		pEn->iBaseFrame=6;
		pEn->dwScore=300;
		pEn->SetActiveStack(&g_objStack);
		pEn->dwResId=EN_SCREW_FIGHTER;
		pEn->SetInitialEnergy(10);
		pEn->lpfnExplode=PushExpl3;
		pEn->SetCartridgeStack(&g_sEShell4); //proiettili laser orizzontali

	}

	//16]UFO3

	if (FAILED(g_sUfo3.Create(10))) return E_FAIL;

	for (dwCount=0;dwCount<g_sUfo3.ItemCount();dwCount++)
	{
		g_sUfo3.m_objStack[dwCount]=new CUfo3();
		g_sUfo3.m_objStack[dwCount]->bActive=FALSE;

		pEn=(CEnemy*)g_sUfo3.m_objStack[dwCount];

		pEn->SetGraphicManager(&g_gm);
		pEn->SetFrameManager(&g_fm);

		for (dwCount1=0;dwCount1<6;dwCount1++)
		{
			pEn->AddFrame(&m_imgUfo3[dwCount1]);

		}

		pEn->dwScore=400;
		pEn->SetActiveStack(&g_objStack);
		pEn->dwResId=EN_UFO3;
		pEn->SetInitialEnergy(10);
		pEn->lpfnExplode=PushExpl3;
		pEn->SetCartridgeStack(&g_sEShell3); //piccolo tondo
		pEn->SetWeapon1(&g_sEShell11); //laser rosso
		pEn->SetWeapon2(&g_sFlashBomb);
		pEn->iStartFireFreq=18;
	}
	
	//inizializza la status bar
	hr=g_fm.CreateFrameSurface(&m_imgStatusBar,630,40);

	
	if (FAILED(hr)) return hr; 

	//crea alcune form ausiliare (ask for exit ecc...)
	CreateAuxForms();

    return S_OK;

}

//----------------------------------- LoadAuxResource ---------------------------------------
//Carica in memoria una risorsa ausiliaria
//Le risosrse ausiliarie sono gli oggetti che non sono comuni a tutti i livelli, esempio alcuni tipi di nemici
//resid � l'id della risorsa ausiliaria
//NOTA: se resid fa riferimento ad una risorsa standard la funzione non fa nulla e rende S_OK
//iflags=parametro opzionale (per ogni risorsa ci sono piu' sottotipi)

HRESULT LoadAuxResource(int resid,int iflags)
{	

	static StackManager<CGroundObject> sm1; //stack manager per oggetti CGroundObject
	static StackManager<CLFighter> smlf;
	static StackManager<CEnemy> smen;
	CSBObject *pobj=NULL;
	CEnemy *pen=NULL;
	CGroundObject *pgrnd=NULL;
	CBlaster2* pb2=NULL;	
	CModularEnemy *pmod=NULL;	
	CModularEnemy *p;
	CModularEnemy *p1;
	CModularEnemy *pc;
	int count;
	int lu;
	DWORD dwCount;
	
	/*
		EN_SHANTY1,
	EN_SHANTY2,
	EN_SHANTY3,
	EN_SHANTY4,
	EN_FACTORY,
	EN_PYLON,
	EN_STREET,
	EN_PALMS,
	EN_WALL
*/

	switch(resid)
	{
	case EN_SHANTY1:
		
		if (g_sShanty1.ItemCount()) return S_OK; //risorsa gi� caricata

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgShanty1,3,0,0,80,38))) return E_FAIL;

		sm1.CreateStack(&g_sShanty1,8,m_imgShanty1,3,15,1,0,PushExpl3); //3
		
		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		break;

	case EN_SHANTY2:

		if (g_sShanty2.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgShanty2,3,0,52,60,26))) return E_FAIL;

		sm1.CreateStack(&g_sShanty2,8,m_imgShanty2,3,15,1,0,PushExpl6); //3
		
		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		break;

	case EN_SHANTY3:

		if (g_sShanty3.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgShanty3,3,0,84,40,28))) return E_FAIL;

		sm1.CreateStack(&g_sShanty3,8,m_imgShanty3,3,15,1,0,PushExpl3); //3
		
		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		break;

	case EN_SHANTY4:

		if (g_sShanty4.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgShanty4,3,0,140,39,35))) return E_FAIL;

		sm1.CreateStack(&g_sShanty4,8,m_imgShanty4,3,15,1,0,PushExpl4); //4
		
		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		break;
    
    case EN_BUILDING1:

		if (g_sBuilding1.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgBuilding[0],3,0,0,56,144))) return E_FAIL;

		sm1.CreateStack(&g_sBuilding1,8,m_imgBuilding[0],3,15,8,0,PushExpl5);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;
		
	case EN_BUILDING2:

		if (g_sBuilding2.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgBuilding[1],3,216,0,108,177))) return E_FAIL;

		sm1.CreateStack(&g_sBuilding2,8,m_imgBuilding[1],3,15,8,0,PushExpl3);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

	case EN_BUILDING3:

		if (g_sBuilding3.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgBuilding[2],3,0,202,78,202))) return E_FAIL;

		sm1.CreateStack(&g_sBuilding3,8,m_imgBuilding[2],3,15,8,0,PushExpl3);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

	case EN_BUILDING4:

		if (g_sBuilding4.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgBuilding[3],3,228,278,76,93))) return E_FAIL;

		sm1.CreateStack(&g_sBuilding4,8,m_imgBuilding[3],3,15,8,0,PushExpl3);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

	case EN_NIGHT_BUILDING1:		
	
        if (g_sNightBuilding1.ItemCount()) return S_OK; 

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgNightBuilding[0],3,288,272,48,68))) return E_FAIL;

		sm1.CreateStack (&g_sNightBuilding1,8,m_imgNightBuilding[0],3,20,8,0,PushExpl3);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

    //zeppa   
	case EN_NIGHT_BUILDING2:		
	
        if (g_sNightBuilding2.ItemCount()) return S_OK; 

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;
		
		if (FAILED(g_rm.GrabFrameArray(m_imgNightBuilding[1],3,300,420,60,60))) return E_FAIL;

		sm1.CreateStack (&g_sNightBuilding2,8,m_imgNightBuilding[1],3,20,8,0,PushExpl3);

		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

		//grattacielo notturno 1
	case EN_NIGHT_BUILDING3:		
	
        if (g_sNightBuilding3.ItemCount()) return S_OK; 

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;
		
		if (FAILED(g_rm.GrabVFrameArray(m_imgNightBuilding[2],3,608,0,79,149))) return E_FAIL;

		sm1.CreateStack (&g_sNightBuilding3,5,m_imgNightBuilding[2],3,40,8,0,PushExpl3);

		sm1.AddWoundedState(24,1);

		sm1.AddWoundedState(12,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;
	
    	//grattacielo notturno 2
	case EN_NIGHT_BUILDING4:		
	
        if (g_sNightBuilding4.ItemCount()) return S_OK; 

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;
		
		if (FAILED(g_rm.GrabVFrameArray(m_imgNightBuilding[3],3,740,0,74,110))) return E_FAIL;

		sm1.CreateStack (&g_sNightBuilding4,5,m_imgNightBuilding[3],3,40,8,0,PushExpl3);

		sm1.AddWoundedState(24,1);

		sm1.AddWoundedState(12,2);

		sm1.SetDontRemoveFlag(TRUE);

		break;

	case EN_NIGHT_BRIDGE:

		if (g_sNightBridge.ItemCount()) return S_OK; 

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;
		
		//2 frame
		if (FAILED(g_rm.GrabVFrameArray(m_imgNightBridge,2,476,320,68,32))) return E_FAIL;

		sm1.CreateStack (&g_sNightBridge,12,m_imgNightBridge,2,20,8,0,PushExpl3);

		sm1.AddWoundedState(10,1);
		
		sm1.SetDontRemoveFlag(TRUE);

		break;			

	
	case EN_ICEBERG:

		if (g_sIceberg.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabVFrameArray(m_imgIceberg,3,348,0,174,86))) return E_FAIL;

		sm1.CreateStack(&g_sIceberg,8,m_imgIceberg,3,15,8,0,PushExpl3);

		sm1.AddWoundedState(11,1);

		sm1.AddWoundedState(7,2);

		break;


	case EN_WALL:

		if (g_sWall.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabVFrameArray(m_imgWall,2,176,82,44,41))) return E_FAIL;

		sm1.CreateStack(&g_sWall,4,m_imgWall,3,15,1,0,PushExpl2);

		sm1.AddWoundedState(14,1);
	
		break;

	case EN_FACTORY:

		if (g_sFactory.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabVFrameArray(m_imgFactory,2,0,204,90,51))) return E_FAIL;

		sm1.CreateStack(&g_sFactory,4,m_imgFactory,3,15,1,0,PushExpl6);

		sm1.AddWoundedState(14,1);
	
		break;

	case EN_PALMS:

		if (g_sPalmThicket.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabVFrameArray(m_imgPalmThicket,2,0,385,128,55))) return E_FAIL;

		sm1.CreateStack(&g_sPalmThicket,8,m_imgPalmThicket,3,15,1,0,PushExpl5);

		sm1.AddWoundedState(14,1);
	
		break;

	case EN_PYLON:

		if (g_sPylon.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabVFrameArray(m_imgPylon,3,183,172,61,86))) return E_FAIL;

		sm1.CreateStack(&g_sPylon,8,m_imgPylon,3,15,1,0,PushExpl3);
		
		sm1.AddWoundedState(14,1);

		sm1.AddWoundedState(7,2);

		break;

	case EN_STREET:

		if (g_sStreet.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data4.vpx",0))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgStreet,2,0,310,100,31))) return E_FAIL;

		sm1.CreateStack(&g_sStreet,8,m_imgStreet,3,15,1,0,PushExpl2);

		sm1.AddWoundedState(14,1);
	
		break;

	case EN_RAIL:

		if (g_sRail.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data6.vpx",4))) return E_FAIL;

		if (FAILED(g_fm.GrabFrame(&m_imgRail,17,224,226,246,g_rm.GetCurrentImage()))) return E_FAIL;

		sm1.CreateStack(&g_sRail,30,&m_imgRail,1,200,8,0,NULL);

		sm1.SetDontRemoveFlag(TRUE);
		
		break;

	case EN_RING:

		if (g_sRing.ItemCount()) return S_OK;

		if (FAILED(CreateRingStack(&g_rm,&g_sRing,8,0))) return E_FAIL;
		
		break;

	case EN_MAMMOTH:

		if (g_sMammoth.ItemCount()) return S_OK;

		if (FAILED(CreateMammothStack(&g_rm,&g_sMammoth,6,0))) return E_FAIL;

		break;

	case EN_BLEEZER:

		if (g_sBleezer.ItemCount()) return S_OK;

		if (FAILED(CreateBleezerStack(&g_rm,&g_sBleezer,3,0))) return E_FAIL;

	case EN_LIFTER:

		if (g_sLifter.ItemCount()) return S_OK;

		if (FAILED(CreateLifterStack(&g_rm,&g_sLifter,3,0))) return E_FAIL;

		break;

	case EN_BLASTER2:

		if (g_sBlaster2.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

		if (FAILED(g_fm.GrabFrame(&m_imgBlaster2,755,6,996,100,g_rm.GetCurrentImage()))) return E_FAIL;
		
	
		m_bpBlaster2.poly=new  VERTEX2D[34];
		m_bpBlaster2.inum=0;	

		AddVertex(&m_bpBlaster2,1,34);
		AddVertex(&m_bpBlaster2,3,23);
		AddVertex(&m_bpBlaster2,9,16);
		AddVertex(&m_bpBlaster2,14,12);
		AddVertex(&m_bpBlaster2,48,1);
		AddVertex(&m_bpBlaster2,64,0);
		AddVertex(&m_bpBlaster2,69,4);
		AddVertex(&m_bpBlaster2,143,14);
		AddVertex(&m_bpBlaster2,147,7);
		AddVertex(&m_bpBlaster2,162,7);
		AddVertex(&m_bpBlaster2,165,12);
		AddVertex(&m_bpBlaster2,185,11);
		AddVertex(&m_bpBlaster2,196,4);
		AddVertex(&m_bpBlaster2,212,0);
		AddVertex(&m_bpBlaster2,229,0);
		AddVertex(&m_bpBlaster2,232,8);
		AddVertex(&m_bpBlaster2,233,11);
		AddVertex(&m_bpBlaster2,239,23);
		AddVertex(&m_bpBlaster2,240,39);
		AddVertex(&m_bpBlaster2,239,60);
		AddVertex(&m_bpBlaster2,234,63);
		AddVertex(&m_bpBlaster2,232,78);
		AddVertex(&m_bpBlaster2,230,89);
		AddVertex(&m_bpBlaster2,226,93);
		AddVertex(&m_bpBlaster2,211,92);
		AddVertex(&m_bpBlaster2,198,89);
		AddVertex(&m_bpBlaster2,189,91);
		AddVertex(&m_bpBlaster2,155,87);
		AddVertex(&m_bpBlaster2,137,74);
		AddVertex(&m_bpBlaster2,63,84);
		AddVertex(&m_bpBlaster2,59,82);
		AddVertex(&m_bpBlaster2,28,76);
		AddVertex(&m_bpBlaster2,9,64);
		AddVertex(&m_bpBlaster2,1,52);	
				
		g_sBlaster2.Create(3);		

		for (dwCount=0;dwCount<3;dwCount++)
		{			
			g_sBlaster2.m_objStack[dwCount]=new CBlaster2;
			pb2=(CBlaster2*)g_sBlaster2.m_objStack[dwCount];
			pb2->bActive=FALSE;
			pb2->SetGraphicManager(&g_gm);
			pb2->SetFrameManager(&g_fm);
			pb2->SetNozzleFlamesStack(&g_sNozzleFlames);
			pb2->SetMathModule(&g_fmath);
			pb2->lpfnExplode=PushExpl9;
			pb2->lpfnMultiExplosion=PushExplMulti; //esplosione multipla con ritardi
			pb2->dwPower=10;
			pb2->SetInitialEnergy(20);
			pb2->SetCartridgeStack(&g_sEShell3);
			pb2->dwScore=3000;
			pb2->SetActiveStack(&g_objStack);
			pb2->Body.SetFrameManager(&g_fm);
			pb2->Body.CreateComponent(0,0,&m_imgBlaster2,1);			
			pb2->Body.SetBoundPoly(0,&m_bpBlaster2);
			//giunto al quale attaccare altri oggetti
			int rj=pb2->Body.AddRotCoupling(59,44,0,360,1);
			pb2->Body.AddFixCoupling(234,28); //ugello: questo punto serve per posizionare la fiamma dell'ugello

			//crea il braccio meccanico e lo attacca al blaster2
			if (!FAILED(LoadMechArmResources(&g_rm))) 
			{
				pb2->m_pArm=new CMechArm;				

				if (!FAILED(CreateMechArm(pb2->m_pArm,&g_fm,0)))
				{
					CMechComponent *pCrank=&pb2->m_pArm->m_Crank1;
					//attacca il braccio al giunto rotidale del blaster2
					pCrank->LinkToComponent(0,rj,&pb2->Body);
					pb2->m_pArm->SetActiveStack(&g_objStack);
					pb2->m_pArm->SetAutoConfig(TRUE);
				}
			}
		}
	
		break;

		///nave spaziale con braccio meccanico
	case EN_BOSS1:
	
		if (g_sBoss1.ItemCount()) return S_OK; //gi� creato			
		//carica le immagini relative al boss1
		if (FAILED(LoadBoss1Resources(&g_rm))) return E_FAIL; 
		
		if (FAILED(CreateBoss1Stack(&g_rm,&g_sBoss1,1,iflags))) return E_FAIL;
	
		break;

    
    //ragno con tentacolo
	case EN_SPIDER1:

		if (g_sSpider1.ItemCount()) return S_OK;
		
		if (FAILED(CreateSpiderStack(&g_rm,&g_sSpider1,2,1))) return E_FAIL;

		break;

    //ragno con bracci meccanici
    case EN_SPIDER2:

		if (g_sSpider2.ItemCount()) return S_OK; 

		if (FAILED(CreateSpiderStack(&g_rm,&g_sSpider2,1,2))) return E_FAIL;

		break;

    //ragno con torretta
	case EN_SPIDER3:

		if (g_sSpider3.ItemCount()) return S_OK; 

		if (FAILED(CreateSpiderStack(&g_rm,&g_sSpider3,1,3))) return E_FAIL;

		break;

		//serpente rosso (8 elementi)
	case EN_SNAKERED:

		if (g_sSnakeRed.ItemCount()) return S_OK; //gi� creato

		if (FAILED(CreateSnakeRedStack(&g_rm,&g_sSnakeRed,3,8))) return E_FAIL;

		break;

		//12 elementi
	case EN_SNAKERED1:

		if (g_sSnakeRed1.ItemCount()) return S_OK; //gi� creato

		if (FAILED(CreateSnakeRedStack(&g_rm,&g_sSnakeRed1,3,12))) return E_FAIL;

		break;


		//20 elementi
	case EN_SNAKERED2:

		if (g_sSnakeRed2.ItemCount()) return S_OK; //gi� creato

		if (FAILED(CreateSnakeRedStack(&g_rm,&g_sSnakeRed2,3,20))) return E_FAIL;

		break;


    case EN_ANTIAIRCRAFT3:

		if (g_sAntiAircraft3.ItemCount()) return S_OK;

		if (FAILED(CreateAA3Stack(&g_rm,&g_sAntiAircraft3,3,0))) return E_FAIL;	
		break;
		
    case EN_GFIGHTER:

		if (g_sGFighter.ItemCount()==0) 
		{
			if (FAILED(CreateGFighterStack(&g_rm,&g_sGFighter,15,iflags))) return E_FAIL;		
		}

		if (g_sFlashBomb.ItemCount()==0)
		{
			if (FAILED(CreateFlashBombStack(&g_rm,&g_sFlashBomb,10,iflags))) return E_FAIL;
		}
		break;

	case EN_UFO3:

		//crea le bombe per ufo 3
		if (g_sFlashBomb.ItemCount()==0)
		{
			if (FAILED(CreateFlashBombStack(&g_rm,&g_sFlashBomb,10,iflags))) return E_FAIL;
		}
		break;

	case EN_LFIGHTER:

		if (g_sLFighter.ItemCount()==0)
		{
			
			if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",0))) return E_FAIL;

			if (FAILED(g_fm.GrabFrame(&m_imgLFighter,478,229,632,277,g_rm.GetCurrentImage()))) return E_FAIL;

			smlf.CreateStack(&g_sLFighter,8,&m_imgLFighter,1,15,8,0,PushExpl3,&g_sEShell11);		

		}

		break;

		//composto da: testa+ 2 moduli semplici + cockpit + modulo cinque giunti + cannone rotante + torretta + ugello

	case EN_SHIELD_BASE:

		if (g_sShieldBase.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,4,&g_sShieldBase,&g_rm,0))) return E_FAIL;			
		}

		break;

	case EN_SHELL_BASE:

		if (g_sShellBase.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,4,&g_sShellBase,&g_rm,0))) return E_FAIL;
		}
		break;

	case EN_TURRET:

		if (g_sTurret.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,4,&g_sTurret,&g_rm,0))) return E_FAIL;	
		}
		break;

		//portale
	case EN_SPACE_BUILD1:

		if (g_sSpaceBuild1.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,6,&g_sSpaceBuild1,&g_rm,0))) return E_FAIL;	
		}
		break;	

		//silos giallo
	case EN_SPACE_BUILD2:

		if (g_sSpaceBuild2.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,6,&g_sSpaceBuild2,&g_rm,0))) return E_FAIL;	
		}
		break;	

		//triplo silos
	case EN_SPACE_BUILD3:

		if (g_sSpaceBuild3.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,6,&g_sSpaceBuild3,&g_rm,0))) return E_FAIL;	
		}
		break;	

		//silos orizzonatale
	case EN_SPACE_BUILD4:

		if (g_sSpaceBuild4.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,6,&g_sSpaceBuild4,&g_rm,0))) return E_FAIL;	
		}
		break;

	case EN_HTRUSS:

		if (g_sHTruss.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,30,&g_sHTruss,&g_rm,0))) return E_FAIL;

		}
		break;

	case EN_VTRUSS:

		if (g_sVTruss.ItemCount()==0)
		{
			if (FAILED(CreateGrndAuxStack(resid,30,&g_sVTruss,&g_rm,0))) return E_FAIL;
		}
		break;

	case EN_TRAIN:

		return CreateTrainStack(2,&g_sTrain,&g_rm,0);

		break;


	case EN_MODULAR1:

		//astronave composta da piu' elementi collegati tra loro
	
		if (g_sModularEnemy[0].ItemCount()==0)
		{
			g_sModularEnemy[0].Create(1);

		
			for (count=0;count<1;count++)
			{			
				if (!(g_sModularEnemy[0].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[0].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[0].m_objStack[count];
				if(!p) break;
				p->dwResId=EN_MODULAR1;
				p->dwScore=12000;
				
				//HRESULT AttachEnemy(CResourceManager *prm,int iCoupling,int iChildCoupling,CModularEnemy *parent,MOD_ENEMY_ID child_type)

				AttachEnemy(&g_rm,0,0,p,MOD_MODULE1);
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE1);
                p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,2,0,p,MOD_COCKPIT);  //0
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE3);  //1
				AttachEnemy(&g_rm,4,0,p,MOD_ROT_CANNON); //2
				//imposta il tipo di proiettili per il cannone
				pc=p->GetAttachedEnemy(2);
				if (pc)
				{
					pc->SetCartridgeStack(&g_sEShell11); 
					pc->SetActiveStack(&g_objStack);
				}
				p=p->GetAttachedEnemy(1); //prende il MODULE3
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE3);
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				//attacca al giunto in alto la torretta a 45�
				AttachEnemy(&g_rm,2,1,p,MOD_TURRET);
				pc=p->GetAttachedEnemy(0);
				if (pc)
				{
					pc->SetActiveStack(&g_objStack); //imposta lo stack attivo per la torretta
				}

				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);						
				
			}
			
			p=NULL;
		}

		break;


		//composto da : testa + modulo a 5 giunti + ugello + cannone circolare + cannone fisso
	case EN_MODULAR2:

		if (g_sModularEnemy[1].ItemCount()==0)
		{
		
			g_sModularEnemy[1].Create(3);

			for (count=0;count<3;count++)
			{			
				if (!(g_sModularEnemy[1].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[1].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[1].m_objStack[count];
				if (!p) break;
				p->dwResId=EN_MODULAR2;
				p->dwScore=7000;			
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				//prende il modulo 3
				p=p->GetAttachedEnemy(0);	
				if (!p) break;
				//attacca il cannone circolare al modulo 3
				AttachEnemy(&g_rm,4,0,p,MOD_CANNON5);
			//	AttachEnemy(&g_rm,3,0,p,MOD_CANNON4); //attacca un cannone fisso sotto il modulo
			//	AttachEnemy(&g_rm,2,0,p,MOD_CANNON1);
				//attacca l'ugello
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);
				//prende il cannone in basso e imposta lo stack attivo
			//	p->GetAttachedEnemy(1)->SetActiveStack(&g_objStack);
			//	p->GetAttachedEnemy(2)->SetActiveStack(&g_objStack);
				//prende il cannone circolare
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				p->SetActiveStack(&g_objStack);
				//imposta arma (p. sferico)
				p->SetCartridgeStack(&g_sFwBullet);              				
				//imposta l'arma secondaria
				p->SetWeapon1(&g_sEShell3);
				
			}
			
			p=NULL;
		}

        break;

	case EN_MODULAR3:

		//astronave con traliccio centrale e tre cannoni fissi+uno rotante

		if (g_sModularEnemy[2].ItemCount()==0)
		{
		
			g_sModularEnemy[2].Create(3);

			for (count=0;count<3;count++)
			{			
				if (!(g_sModularEnemy[2].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[2].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[2].m_objStack[count];
				if (!p) break;
				p->dwResId=EN_MODULAR3;
				p->dwScore=9000;
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				//prende il modulo 3
				p=p->GetAttachedEnemy(0);
				//attacca il cannone centrale
				AttachEnemy(&g_rm,4,0,p,MOD_CANNON1);
				//attacca il cannone inferiore
				AttachEnemy(&g_rm,3,0,p,MOD_CANNON4);
				//attacca il cannone superiore rivolto indietro
				AttachEnemy(&g_rm,2,0,p,MOD_CANNON2);
				//attacca il traliccio
				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);
				//prende il traliccio
				p=p->GetAttachedEnemy(3);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);
				//prende il modulo2
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,4,0,p,MOD_ROT_CANNON);
				pc=p->GetAttachedEnemy(0);
				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 
				}
				//attacca l'ugello finale
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);

			}

			p=NULL;

		}

		break;


	case EN_MODULAR4:

		if (g_sModularEnemy[3].ItemCount()==0)
		{
		
			g_sModularEnemy[3].Create(2);

			for (count=0;count<2;count++)
			{			
				if (!(g_sModularEnemy[3].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[3].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[3].m_objStack[count];
				p->dwResId=EN_MODULAR4;
				p->dwScore=10000;

				//attacca una toretta in testa
				AttachEnemy(&g_rm,1,1,p,MOD_TURRET);
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				//prende il modulo 3
				p=p->GetAttachedEnemy(1);
				if (!p) break;
				AttachEnemy(&g_rm,2,0,p,MOD_CANNON3);
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE1);
				//prende il modulo 1
				p=p->GetAttachedEnemy(1);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE3);
				//prende il modulo 3
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				//cannone circolare
				AttachEnemy(&g_rm,4,0,p,MOD_CANNON5);
				AttachEnemy(&g_rm,2,1,p,MOD_TURRET);
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);
                //prende il cannone circolare
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				p->SetActiveStack(&g_objStack);
				//imposta arma (p. sferico)
				p->SetCartridgeStack(&g_sFwBullet); 
				//imposta l'arma secondaria
				p->SetWeapon1(&g_sEShell3);
			}


		}

		break;

	//0=testa 1=coda 2=alto 3=basso 4=rotante

	case EN_MODULAR5:

		if (g_sModularEnemy[4].ItemCount()==0)
		{		
			g_sModularEnemy[4].Create(1);

			for (count=0;count<1;count++)
			{			
				if (!(g_sModularEnemy[4].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[4].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[4].m_objStack[count];
				if (!p) break;
				p->dwResId=EN_MODULAR5;
				p->dwScore=12000;
				
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				//prende il modulo 3
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				p->SetInitialEnergy(100);
				AttachEnemy(&g_rm,4,0,p,MOD_CANNON5);
				AttachEnemy(&g_rm,3,0,p,MOD_CANNON4);
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);
				//prende il cannone rotante
				p1=p->GetAttachedEnemy(0);
				if (!p1) break;
				p1->SetActiveStack(&g_objStack);
				//imposta arma (p. sferico)
				p1->SetCartridgeStack(&g_sFwBullet); 
				//imposta l'arma secondaria
				p1->SetWeapon1(&g_sEShell3);
				//prende il modulo 2
				p=p->GetAttachedEnemy(2);
				if (!p) break;
				//attacca un cannone rotante
				AttachEnemy(&g_rm,4,0,p,MOD_ROT_CANNON);
				pc=p->GetAttachedEnemy(0);
				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 							
				}

				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);
				AttachEnemy(&g_rm,2,1,p,MOD_VFRAME);			
				//vframe
				p1=p->GetAttachedEnemy(2);
				if (p1)
				{
					AttachEnemy(&g_rm,0,3,p1,MOD_MODULE3);
					//prende il modulo 3
					p1=p1->GetAttachedEnemy(0);
					if (p1)
					{
						AttachEnemy(&g_rm,4,0,p1,MOD_CANNON5);
						AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE);
						p1=p1->GetAttachedEnemy(0);
						if (p1)
						{
							//imposta il cannone circolare
							p1->SetActiveStack(&g_objStack);
							//imposta arma (p. sferico)
							p1->SetCartridgeStack(&g_sFwBullet); 
							//imposta l'arma secondaria
							p1->SetWeapon1(&g_sEShell3);		
						}
					}
				}

				//prende l'hframe
				p=p->GetAttachedEnemy(1);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE3);
				//prende il modulo 3 di coda
				p=p->GetAttachedEnemy(0);
				AttachEnemy(&g_rm,2,0,p,MOD_COCKPIT);
				AttachEnemy(&g_rm,3,0,p,MOD_CANNON4);
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);
				//prende il modulo 2
				p=p->GetAttachedEnemy(2);
				if (!p) break;
				AttachEnemy(&g_rm,2,3,p,MOD_MODULE2); //alto
				AttachEnemy(&g_rm,3,2,p,MOD_MODULE2); //basso
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);
				p1=p->GetAttachedEnemy(0);
				if (!p1) break;
				AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE); //ugello basso
				AttachEnemy(&g_rm,4,0,p1,MOD_ROT_CANNON);
				//imposta il rot cannon
				pc=p->GetAttachedEnemy(1);
				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 							
				}
				p1=p->GetAttachedEnemy(1);
				if (!p1) break;
				AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE); //ugello in basso
			}

		}

		break;

		//treno lungo 2 hframe
	case EN_MODULAR6:		

		if (g_sModularEnemy[5].ItemCount()==0)
		{		
			g_sModularEnemy[5].Create(2);

			for (count=0;count<2;count++)
			{			
				if (!(g_sModularEnemy[5].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[5].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[5].m_objStack[count];
				if (!p) break;

				p->dwResId=EN_MODULAR6;
				p->dwScore=3000;
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,4,0,p,MOD_CANNON1);
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE1);
				p=p->GetAttachedEnemy(1);
				if (!p) break;
				//prende il modulo 1
				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);
				//prende la frame
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);				
				//prend il module 2
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,4,0,p,MOD_ROT_CANNON);
				//imposta il rot cannon
				pc=p->GetAttachedEnemy(0);
				
				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 							
				}

				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);
				//prende l'hframe
				p=p->GetAttachedEnemy(1);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE1);
				p=p->GetAttachedEnemy(0);
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);

			}
			
		}		
				
		break;

		//0=testa 1=coda 2=alto 3=basso 4=rotante

	case EN_MODULAR7:

		if (g_sModularEnemy[6].ItemCount()==0)
		{		
			g_sModularEnemy[6].Create(1);

			for (count=0;count<1;count++)
			{				
				if (!(g_sModularEnemy[6].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[6].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[6].m_objStack[count];
				if (!p) break;

				p->dwResId=EN_MODULAR7;
				p->dwScore=20000;
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				p=p->GetAttachedEnemy(0);
				if (!p) break;

				AttachEnemy(&g_rm,2,3,p,MOD_MODULE2); //alto
				AttachEnemy(&g_rm,3,2,p,MOD_MODULE2); //basso
				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME); //dietro

				p1=p->GetAttachedEnemy(0); //alto

				if (p1)
				{
					AttachEnemy(&g_rm,5,0,p1,MOD_CANNON1);
					AttachEnemy(&g_rm,2,0,p1,MOD_CANNON2); //cannone rivolto indietro
					AttachEnemy(&g_rm,1,0,p1,MOD_HFRAME);
					p1=p1->GetAttachedEnemy(2); //prende la frame
					AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE);

				}

				p1=p->GetAttachedEnemy(1); //modulo in basso
				if (p1)
				{
					AttachEnemy(&g_rm,5,0,p1,MOD_CANNON1);
					AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE);
				}

				p1=p->GetAttachedEnemy(2); //frame posteriore

				if (p1)
				{
					AttachEnemy(&g_rm,1,0,p1,MOD_MODULE3);
				
					p1=p1->GetAttachedEnemy(0); //prende il modulo3

					if (p1) 
					{
						AttachEnemy(&g_rm,4,0,p1,MOD_CANNON5); //cannone circolare
						pc=p1->GetAttachedEnemy(0);

						if (pc)
						{
							//imposta il cannone circolare
							pc->SetActiveStack(&g_objStack);
							//imposta arma (p. sferico)
							pc->SetCartridgeStack(&g_sFwBullet); 
							//imposta l'arma secondaria
							pc->SetWeapon1(&g_sEShell3);
						}

					
					
						AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE);
					}				

				}

			}
		}

		break;


		
	case EN_MODULAR8:

		if (g_sModularEnemy[7].ItemCount()==0)
		{		
			g_sModularEnemy[7].Create(1);

			for (count=0;count<1;count++)
			{			
				if (!(g_sModularEnemy[7].m_objStack[count]=GetModEnemy(&g_rm,MOD_ENEMY_ROOT))) return E_FAIL;					
				g_sModularEnemy[7].m_objStack[count]->bActive=FALSE;
				p=(CModularEnemy *)g_sModularEnemy[7].m_objStack[count];
				if (!p) break;

				p->dwResId=EN_MODULAR8;
				p->dwScore=25000;
				AttachEnemy(&g_rm,0,0,p,MOD_MODULE3);
				p=p->GetAttachedEnemy(0); //prende il modulo3 (0)
				if (!p) break;
				
				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);		//(1)		
				AttachEnemy(&g_rm,2,1,p,MOD_VFRAME); //1� montante  (2)

				p1=p->GetAttachedEnemy(1);

				if (p1)
				{
					AttachEnemy(&g_rm,0,3,p1,MOD_MODULE2);	//(3)			
					p1=p1->GetAttachedEnemy(0); //prende il module2
					AttachEnemy(&g_rm,1,0,p1,MOD_HFRAME); //(4)
					AttachEnemy(&g_rm,0,2,p1,MOD_TURRET); //(5)
				}

				p=p->GetAttachedEnemy(0); //prende la prima hframe in basso
				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2); //(6)

				p=p->GetAttachedEnemy(0); //prende il module 2 (riga in basso)
				if (!p) break;

				AttachEnemy(&g_rm,2,1,p,MOD_VFRAME); //2� montante (7)
				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME); //frame orizzontale (8)

				AttachEnemy(&g_rm,4,0,p,MOD_ROT_CANNON);

				pc=p->GetAttachedEnemy(2);

				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 									
				}

				//prende la vframe
				p1=p->GetAttachedEnemy(0); 

				if (!p1) break;

				//construisce il lato in alto
				AttachEnemy(&g_rm,0,3,p1,MOD_MODULE2);

				//prende il modulo 2 (in alto)
				p1=p1->GetAttachedEnemy(0); 

				if (!p1) break;
			
				AttachEnemy(&g_rm,4,0,p1,MOD_ROT_CANNON);

				pc=p1->GetAttachedEnemy(0);

				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 									
				}

				AttachEnemy(&g_rm,1,0,p1,MOD_HFRAME);

				p1=p1->GetAttachedEnemy(1);
				
				if (!p1) break;

				AttachEnemy(&g_rm,1,0,p1,MOD_HFRAME);

				p1=p1->GetAttachedEnemy(0);
				
				if (!p1) break;

				AttachEnemy(&g_rm,1,0,p1,MOD_MODULE2);

				p1=p1->GetAttachedEnemy(0);
				
				if (!p1) break;

				AttachEnemy(&g_rm,4,0,p1,MOD_ROT_CANNON);

				//cannone sul lato in alto
				pc=p1->GetAttachedEnemy(0);

				if (pc)
				{
					pc->SetActiveStack(&g_objStack);
					pc->SetCartridgeStack(&g_sEShell11); 									
				}

				AttachEnemy(&g_rm,3,0,p1,MOD_VFRAME); //� montante 
				AttachEnemy(&g_rm,1,0,p1,MOD_HFRAME);

				//prende l'hframe
				p1=p1->GetAttachedEnemy(2); 
				
				if (!p1) break;

				AttachEnemy(&g_rm,1,0,p1,MOD_MODULE3);

				//prende il modulo 3
				p1=p1->GetAttachedEnemy(0);

				if (!p1) break;

				AttachEnemy(&g_rm,3,0,p1,MOD_VFRAME);
				AttachEnemy(&g_rm,3,0,p1,MOD_CANNON4);
				AttachEnemy(&g_rm,1,0,p1,MOD_NOZZLE);

				//completa la riga in basso
				p=p->GetAttachedEnemy(1); //prende l'hframe (8) orizzontale

				if (!p) break;

				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME); //prolunga l'hframe in basso

				p=p->GetAttachedEnemy(0);				

				if (!p) break;
				AttachEnemy(&g_rm,1,0,p,MOD_MODULE2);

				//prende il module2
				p=p->GetAttachedEnemy(0);

				if (!p) break;

				AttachEnemy(&g_rm,1,0,p,MOD_HFRAME);

				p=p->GetAttachedEnemy(0);

				if (!p) break;

				AttachEnemy(&g_rm,1,0,p,MOD_MODULE1);

				//prende il modulo1
				p=p->GetAttachedEnemy(0);

				if (!p) break;
		
				AttachEnemy(&g_rm,1,0,p,MOD_NOZZLE);

			}

		}

		break;

	case EN_JELLYSHIP:

		if (g_sJellyShip.ItemCount()==0)
		{
            if (FAILED(CreateJellyShipStack(&g_rm,&g_sJellyShip,2,0))) return E_FAIL;			

		}

		break;

	case EN_ASTEROID_SMALL:

		//nella creazione fra i differenti tipi di asteroidi cambia il parametro iflags
		if (g_sAsteroidSmall.ItemCount()==0)
		{
			if (FAILED(CreateAsteroidStack(&g_rm,&g_sAsteroidSmall,30,0,NULL,NULL))) return E_FAIL;
		}
		break;

	case EN_ASTEROID_MEDIUM:

		if (g_sAsteroidMedium.ItemCount()==0)
		{
			if (FAILED(CreateAsteroidStack(&g_rm,&g_sAsteroidMedium,12,1,NULL,&g_sAsteroidSmall))) return E_FAIL;
		}
		break;

	case EN_ASTEROID_BIG:

		if (g_sAsteroidBig.ItemCount()==0)
		{
			if (FAILED(CreateAsteroidStack(&g_rm,&g_sAsteroidBig,8,2,&g_sAsteroidMedium,&g_sAsteroidSmall))) return E_FAIL;	
		}
		break;

	case EN_HFIGHTER:

		if (g_sHFighter.ItemCount()) return S_OK;

		if (FAILED(g_rm.LoadAuxResFile("data\\data3.vpx",2))) return E_FAIL;

		if (FAILED(g_rm.GrabFrameArray(m_imgHFighter,9,0,480,120,60))) return E_FAIL;
		
		smen.CreateStack(&g_sHFighter,20,m_imgHFighter,9,4,6,0,PushExpl8,&g_sEShell3);
		
		lu=g_sHFighter.ItemCount();

		for (count=0;count<lu;count++)
		{
			pen=(CEnemy*)g_sHFighter.m_objStack[count];

			pen->dwScore=200;
			pen->SetUpdateFreq(2);
			pen->SetAuxPath(0,g_Path9,16);
			pen->SetAuxPath(1,g_Path4,27);
			pen->SetAuxPath(2,g_Path1,70);
			pen->SetAuxPath(3,g_Path10,33);
			pen->iStartFireFreq=90;
			pen->dwClass=CL_ENEMY;
			pen->SetType(45);
			pen->SetFireSound(g_snFire9);
			pen->iBaseFrame=4;
			pen->pfnSmoke=PushSmoke0;
			pen->SetFrameHotSpot(0,0,0,25); //usato come posizione cannone
		}

		break;

		//serpente
	case EN_SNAKE:

		if (g_sSnake.ItemCount()) return S_OK;

		LONG hx,hy,hl,hw;
		RECT rc;
		int iAngle=0;
		LONG px,py,x1,y1;
		DWORD dwCount1;
		CADXFastMath fh;		

		fh.InitTables();
	
		if (FAILED(g_rm.LoadAuxResFile("data\\data2.vpx",0))) return E_FAIL;

		rc.left=0;
		rc.top=0;
		rc.right=146;
		rc.bottom=146;

		if (FAILED(g_fm.GrabFrame(&m_imgSnakeHead[0],&rc,g_rm.GetCurrentImage()))) return E_FAIL;

		rc.left=192;
		rc.top=0;
		rc.right=192+64;
		rc.bottom=64;

		//corpo del serpente
		if (FAILED(g_fm.GrabFrame(&m_imgSnakeBody[0],&rc,g_rm.GetCurrentImage()))) return E_FAIL;

		//crea le frame ruotate
		for (dwCount=1;dwCount<36;dwCount ++)
		{
			iAngle += 10;
			if (FAILED(g_fm.CreateRotFrame(iAngle,&m_imgSnakeHead[dwCount],&m_imgSnakeHead[0],FALSE))) return E_FAIL;		
			if (FAILED(g_fm.CreateRotFrame(iAngle,&m_imgSnakeBody[dwCount],&m_imgSnakeBody[0],FALSE))) return E_FAIL;			
		}

			CSnakeBody *pSnb=NULL;

		//corpo del serpente
		g_sSnakeBody.Create(20);

		for (dwCount=0;dwCount<20;dwCount++)
		{
			g_sSnakeBody.m_objStack[dwCount]=new CSnakeBody;
			g_sSnakeBody.m_objStack[dwCount]->bActive=FALSE;
			pSnb=(CSnakeBody *)g_sSnakeBody.m_objStack[dwCount];	
			pSnb->SetGraphicManager(&g_gm);
			pSnb->SetFrameManager(&g_fm);
			pSnb->SetVelocity(180,3);
			pSnb->dwPower=4;
			pSnb->SetInitialEnergy(10);		
			pSnb->dwClass=CL_ENEMY;
			pSnb->dwScore=100;
			pSnb->SetCurFrame(0);
			pSnb->SetActiveStack(&g_objStack);			
			pSnb->lpfnExplode=PushExpl8;			
			pSnb->SetCartridgeStack(NULL);

			//coordinate dell'hot spot relativo al centro della frame
			hl=(LONG)((float)m_imgSnakeBody[0].height*0.5f);
			hw=(LONG)((float)m_imgSnakeBody[0].width*0.5f);
			hx=10-hw;
			hy=32-hl;

			for (dwCount1=0;dwCount1<36;dwCount1++)
			{
				pSnb->AddFrame(dwCount1,&m_imgSnakeBody[dwCount1]);		
				pSnb->SetFrameHotSpot(dwCount1,0,(LONG)(hw+hx*fh.CosTable[dwCount1*10]+hy*fh.SinTable[dwCount1*10]),(LONG)(hl+hx*fh.SinTable[dwCount1*10]-hy*fh.SinTable[dwCount1*10]));

			}

			pSnb->SetCurFrame(0);
					
		}

		CSnake *pSn=NULL;
		
		g_sSnake.Create(3);

		for (dwCount=0;dwCount<2;dwCount++)
		{
			g_sSnake.m_objStack[dwCount]=new CSnake;
			g_sSnake.m_objStack[dwCount]->bActive=FALSE;
			pSn=(CSnake *)g_sSnake.m_objStack[dwCount];
			pSn->SetGraphicManager(&g_gm);
			pSn->dwResId=EN_SNAKE;
			pSn->SetFrameManager(&g_fm);
			pSn->SetVelocity(180,3);
			pSn->dwPower=7;
			pSn->SetInitialEnergy(35);
			pSn->dwClass=CL_ENEMY;
			pSn->dwScore=5000;	
			pSn->SetUpdateFreq(5);

			//coordinate dll'hot spot relative al centro della frame
			hl=(LONG)((float)m_imgSnakeHead[0].height*0.5f);
			hw=(LONG)((float)m_imgSnakeHead[0].width*0.5f);
			hx=10-hw;
			hy=74-hl;

			for (dwCount1=0;dwCount1<36;dwCount1++)
			{
				pSn->AddFrame(dwCount1,&m_imgSnakeHead[dwCount1]);	
				//giunzione con l'elemento del corpo
				x1=(LONG)(hw+hx*fh.CosTable[dwCount1*10]+hy*fh.SinTable[dwCount1*10]);
				y1=(LONG)(hl+hx*fh.SinTable[dwCount1*10]-hy*fh.CosTable[dwCount1*10]);
				pSn->SetFrameHotSpot(dwCount1,0,x1,y1);				
				//cannoni
				px=137-hw;
				py=53-hl;
				x1=(LONG)(hw+px*fh.CosTable[dwCount1*10]+py*fh.SinTable[dwCount1*10]);
				y1=(LONG)(hl+px*fh.SinTable[dwCount1*10]-py*fh.CosTable[dwCount1*10]);
				pSn->SetFrameHotSpot(dwCount1,1,x1,y1);
				py=85-hl;
				x1=(LONG)(hw+px*fh.CosTable[dwCount1*10]+py*fh.SinTable[dwCount1*10]);
				y1=(LONG)(hl+px*fh.SinTable[dwCount1*10]-py*fh.CosTable[dwCount1*10]);
				pSn->SetFrameHotSpot(dwCount1,2,x1,y1);		
			
			}

			pSn->SetCartridgeStack(&g_sEShell3); //arma primaria
			pSn->iStartFireFreq=40;
			pSn->pBodyStack=&g_sSnakeBody;
			pSn->SetPath(g_Path2,69);
			//path ausiliari
			pSn->SetAuxPath(0,g_Path2,69);
			pSn->SetAuxPath(1,g_SnakePath1,69); //flags=1
			pSn->SetAuxPath(2,g_SnakePath2,69); //flags=2
			pSn->SetAuxPath(3,g_SnakePath3,69); //torna indietro 3
		
			pSn->SetCurFrame(0);
			pSn->SetActiveStack(&g_objStack);
			pSn->lpfnExplode=PushExpl3;
		
	}

	pSn=NULL;
	break;


	}

	return S_OK;
}

//------------------------------------------- StackManager<T> -----------------------------------------

//costruttore
template <class T>
StackManager<T>::StackManager(void)
{
	pLastStack=NULL;
	pobj=NULL;
}

//Crea uno stack di oggetti e aggiunge le frame dal vettore di frames
/* pstack=punta allo stack da creare
   numitems=numero di elementi da creare
   pimgFrames=punta alle frames
   numframes=numero di frames
   energy=energia
   power=parametro power
   bonus=bonus
   fnExpl=funzione esplosione

*/

template <class T>
HRESULT StackManager<T>::CreateStack(CObjectStack *pstack,int numitems,IMAGE_FRAME_PTR pimgFrames,int numframes,int energy,int power,int bonus,void ( *fnExpl)(LONG,LONG),CObjectStack *pCartridges)
{	
	int count,count1;
	if (!pstack) return E_FAIL;

	pstack->Create(numitems);

	for (count=0;count<numitems;count++)
	{
		pstack->m_objStack[count]=new T;
		pobj=(T *)pstack->m_objStack[count];
		pobj->bActive=FALSE;
		pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		for (count1=0;count1<numframes;count1++)
		{
			pobj->AddFrame(&pimgFrames[count1]);
		}
		pobj->SetInitialEnergy(energy);
		pobj->dwPower=power;
		pobj->SetCurFrame(0);
		pobj->SetVelocity(0,0);
		pobj->lpfnExplode=fnExpl;
		pobj->SetCartridgeStack(pCartridges);
		pobj->SetActiveStack(&g_objStack);
	}

	pLastStack=pstack;

	return S_OK;
}

//aggiunge uno stato di danneggiamento a tutti gli oggetti dello stack
template <class T>
BOOL StackManager<T>::AddWoundedState(DWORD dwEnergyThreshold,DWORD dwFrame)
{	
	DWORD lu;
	CEnemy *pen;

	if (!pLastStack) return E_FAIL;
	lu=pLastStack->ItemCount();

	for (DWORD count=0;count<lu;count++)
	{
		pen=(CEnemy *)pLastStack->m_objStack[count];
		if (!pen->AddWoundedState(dwEnergyThreshold,dwFrame)) return FALSE;
	}

	return TRUE;

}


template <class T>
void StackManager<T>::SetDontRemoveFlag(BOOL bVal)
{
	CGroundObject *pobj;

	LONG lu=pLastStack->ItemCount();

	for (LONG count=0;count<lu;count++)
	{
		pobj=(CGroundObject *)pLastStack->m_objStack[count];
		pobj->m_bDontRemove=bVal;
	}
}


/*template <class T>
void StackManager<T>::SetSmokeStack(CObjectStack *psmoke)
{
	CGroundObject *pobj;

	LONG lu=pLastStack->ItemCount();

	for (LONG count=0;count<lu;count++)
	{
		pobj=(CGroundObject *)pLastStack->m_objStack[count];
		pobj->SetSmokeStack(psmoke);
	}
}
*/
//----------------------------------- FreeAuxResources ----------------------------------------
//rilascia le risorse ausiliarie caricate

void FreeAuxResources(void)
{	
	//oggetto istanziato da LoadSbLevel
	SAFE_DELETE(g_pStarField); 

	g_fm.FreeFrameArray(m_imgShanty1,3);
	g_sShanty1.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgShanty2,3);
	g_sShanty2.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgShanty3,3);
	g_sShanty3.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgShanty4,3);
	g_sShanty4.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgWall,2);
	g_sWall.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgPylon,3);
	g_sPylon.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgFactory,2);
	g_sFactory.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgStreet,2);
	g_sStreet.Clear(TRUE);
	g_fm.FreeFrameArray(m_imgPalmThicket,2);
	g_sPalmThicket.Clear(TRUE);
	g_sSnakeBody.Clear(TRUE);
	g_sSnake.Clear(TRUE);
	g_sGFighter.Clear(TRUE);
	g_sFlashBomb.Clear(TRUE);
	g_sRail.Clear(TRUE);
	g_fm.FreeImgFrame(&m_imgRail);
	g_sRing.Clear(TRUE);
	g_sMammoth.Clear(TRUE);
	g_sLifter.Clear(TRUE);
	g_sBleezer.Clear(TRUE);

	g_fm.FreeFrameArray(m_imgIceberg,3);
	g_sIceberg.Clear(TRUE);

	for (DWORD dwCount=0;dwCount<36;dwCount++)
	{
		g_fm.FreeImgFrame(&m_imgSnakeHead[dwCount]);
		g_fm.FreeImgFrame(&m_imgSnakeBody[dwCount]);
	}

	if (g_sBuilding1.ItemCount())
	{
		g_sBuilding1.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgBuilding[0],3);
	}

	if (g_sBuilding2.ItemCount())
	{
		g_sBuilding2.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgBuilding[1],3);
	}

	if (g_sBuilding3.ItemCount())
	{
		g_sBuilding3.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgBuilding[2],3);
	}

	if (g_sBuilding4.ItemCount())
	{
		g_sBuilding4.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgBuilding[3],3);
	}

	if (g_sNightBuilding1.ItemCount())
	{
		g_sNightBuilding1.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgNightBuilding[0],3);
	}

	if (g_sNightBuilding2.ItemCount())
	{
		g_sNightBuilding2.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgNightBuilding[1],3);
	}

	if (g_sNightBuilding3.ItemCount())
	{
		g_sNightBuilding3.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgNightBuilding[2],3);
	}

	if (g_sNightBuilding4.ItemCount())
	{
		g_sNightBuilding4.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgNightBuilding[3],3);
	}

	if (g_sNightBridge.ItemCount())
	{
		g_sNightBridge.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgNightBridge,2);
	}

	if (g_sLFighter.ItemCount())
	{
		g_sLFighter.Clear(TRUE);
		g_fm.FreeImgFrame(&m_imgLFighter);
	}

	if (g_sBlaster2.ItemCount())
	{
		g_sBlaster2.Clear(TRUE);
		g_fm.FreeImgFrame(&m_imgBlaster2);
		SAFE_DELETE_ARRAY(m_bpBlaster2.poly);
	}	

	if (g_sHFighter.ItemCount())
	{
		g_sHFighter.Clear(TRUE);
		g_fm.FreeFrameArray(m_imgHFighter,9);
	}
	

	FreeJlTentacleRes(&g_fm);
	FreeJellyShipRes(&g_fm);
	g_sJellyShip.Clear(TRUE);		
	g_sAsteroidSmall.Clear(TRUE);
	g_sAsteroidMedium.Clear(TRUE);
	g_sAsteroidBig.Clear(TRUE);
	FreeAsteroidRes(&g_fm);
	FreeSpiderStack(&g_sSpider1);
	FreeSpiderStack(&g_sSpider2);
	FreeSpiderStack(&g_sSpider3);
	FreeSnake2Stack(&g_sSnakeRed);
	FreeSnake2Stack(&g_sSnakeRed1);
	FreeSnake2Stack(&g_sSnakeRed2);
	FreeBoss1Stack(&g_sBoss1);
	FreeGFighterRes(&g_fm);
	FreeFlashBombRes(&g_fm);
	FreeBoss1Resources(&g_fm);
	FreeMechArmResources(&g_fm);
	FreeSnake1Resources(&g_fm);
	FreeSnake2Resources(&g_fm);
	FreeSpiderResources(&g_fm);
	FreeTrainRes();
	FreeRingRes();
	FreeMammothRes();
	FreeLifterRes();
	FreeBleezerRes();

	g_sShieldBase.Clear(TRUE);
	g_sShellBase.Clear(TRUE);
	g_sTurret.Clear(TRUE); //torretta oro
	g_sTrain.Clear(TRUE); //treno   
    g_sSpaceBuild1.Clear(TRUE); //arco
    g_sSpaceBuild2.Clear(TRUE); //silos giallo orizz.
    g_sSpaceBuild3.Clear(TRUE); //triplo silos
    g_sSpaceBuild4.Clear(TRUE); //silos orizz. con tubi
    g_sHTruss.Clear(TRUE);
    g_sVTruss.Clear(TRUE);
	g_sShieldBase.Clear(TRUE);
	g_sShellBase.Clear(TRUE);
	g_sTurret.Clear(TRUE); //torretta oro   
    g_sRail.Clear(TRUE); //ferrovia      

	//rilascia gli oggetti modulari
	FreeModEnemies();
	//rilascia le immagini dei nemici a terra ausiliari
	FreeGrndAuxRes();

	for (int count=0;count<MAX_MOD_ENEMIES;count++)
	{
		g_sModularEnemy[count].Clear(FALSE);
	}

#ifdef _DEBUG_		
	_CrtCheckMemory();
#endif

}

//-------------------------------------- InitData ---------------------------------------------
//Inizializza gli sprites
HRESULT InitData()
{

	//crea lo stack principale
	//tutti gli oggetti attivi vengono messi qui
	g_objStack.Create(MAX_OBJECTS);

	DWORD dwCount;
	LONG lefts,tops,widths,heights;

	//questi oggetti devono essere inizializzati
	if (g_gm.GetStatus() <=0) return -1;
	if (g_fm.GetStatus() <=0) return -2;
          

	//inizializza la form con il menu di uscita
	if (FAILED(m_frmExit.SetCharSet(&g_chRed))) return -3;	
    
	if (FAILED(g_Cursor.SetGraphicManager(&g_gm))) return -6;
	if (FAILED(g_Cursor.SetFrameManager(&g_fm))) return -7;
	if (FAILED(g_Cursor.AddFrame(&m_imgCursor))) return -4;
	
	g_Cursor.SetCurFrame(0);	

	m_frmExit.SetBackGroundProc(DrawBackGround);

	//imposta il cursore	
	if (FAILED(m_frmExit.SetMousePointer(&g_Cursor))) return -5;

	m_frmExit.SetSelector(NULL,0);
		
	m_frmExit.Create(0,0,TEXT("Vuoi uscire dal gioco ?"),0);
	m_frmExit.SetInputManager(&g_ci);
	m_frmExit.SetGraphicManager(&g_gm);
	m_frmExit.SetFrameManager(&g_fm);

	m_frmContinue.SetBackGroundProc(DrawBackMenu);

	if (FAILED(m_frmContinue.SetCharSet(&g_chRed))) return -3;	  

	//imposta il cursore	
	if (FAILED(m_frmContinue.SetMousePointer(&g_Cursor))) return -5;

	m_frmContinue.SetSelector(NULL,0);

		
	m_frmContinue.Create(0,0,TEXT("Continuare ?"),0);
	m_frmContinue.SetInputManager(&g_ci);
	m_frmContinue.SetGraphicManager(&g_gm);
	m_frmContinue.SetFrameManager(&g_fm);
	

	//Impostazioni iniziali PLAYRE1
	//imposta lo stato iniziale del player (vite, armi ecc...)
	Player1.Reset();   
    //imposta le funzioni di fuoco primario e secondario
	Player1.Fire1=PlFire1;	   //fuoco primario
	Player1.Fire2=PlDropBomb1; //bombe	
	Player1.Fire3=PlFire6;
	//n� di vite iniziali
	Player1.dwLives=5;
	Player1.dwScore=0;
	Player1.bActive=TRUE;
    Player1.dwClass=CL_PLAYER;
	Player1.SetGraphicManager(&g_gm);
	Player1.SetFrameManager(&g_fm);	   
	Player1.pfnGameOver=GameOver; //punta la funzione game over
	Player1.pfnSmoke=PushSmoke180; //funzione fumo
	Player1.pfnUpdateBar=UpdateStatusBar; //funzione di aggiornamento barra di stato
	Player1.lpfnExplode=PushExpl8;
	Player1.bNozzleFlames=TRUE;

	g_sprNozzleFlames.Release();

	g_sprNozzleFlames.SetFrameManager(&g_fm);
	g_sprNozzleFlames.AddFrame(&m_imgBackFlame[0]);
	g_sprNozzleFlames.AddFrame(&m_imgBackFlame[1]);
	g_sprNozzleFlames.AddFrame(&m_imgBackFlame[2]);
    g_sprNozzleFlames.SetAnimSpeed(0.2f);

    //imposta le fiamme dell'ugello
	Player1.pNozzleFlames=&g_sprNozzleFlames;

	//frame dell'astronave del player
	for (dwCount=0;dwCount<=12;dwCount++) Player1.AddFrame(&pl[dwCount]);

	//frame del player che sta esplodendo
	for (dwCount=0;dwCount<=8;dwCount++) Player1.AddFrame(&ple[dwCount]);	

	g_rcScreen.left=0;
	g_rcScreen.top=0;
	g_rcScreen.bottom=SCREEN_HEIGHT;
	g_rcScreen.right=SCREEN_WIDTH;
   
	DD_INIT_STRUCT(g_ddbltfx);    
   
	//il player viene colpito dagli oggetti di tipo CL_ENFIRE e CL_ENEMY
	//definisce il vettore che specifica quali tipi di oggetti possono collidere fra loro
	g_dwClMatch[CL_PLAYER]=CL_ENEMY+CL_ENFIRE+CL_BONUS; 
	g_dwClMatch[CL_ENEMY]=CL_PLFIRE+CL_PLAYER+CL_FRIEND;
	g_dwClMatch[CL_ENFIRE]=CL_PLAYER+CL_FRIEND;	
	g_dwClMatch[CL_PLFIRE]=CL_ENEMY;
	g_dwClMatch[CL_NONE]=CL_NONE;
	g_dwClMatch[CL_BONUS]=CL_NONE;
	g_dwClMatch[CL_FRIEND]=CL_ENEMY;	

    //imposta il puntatore alla sup. della SB
	g_lpStatusBar=m_imgStatusBar.surface;

	DD_INIT_STRUCT(g_dbxSb);
	//inizializza la struttura per cancellare la status bar prima di aggiornarla
	g_dbxSb.dwFillColor=0;

	//imposta il rettangolo di destinazione per la status bar
	g_rcBar.left=10;
	g_rcBar.top=10;
	g_rcBar.right=m_imgStatusBar.width+10;
	g_rcBar.bottom=m_imgStatusBar.height+10;
	//rettengolo di destinazione per la label delle vite
	g_rcShip.left=1;
	g_rcShip.top=2;
	g_rcShip.bottom=g_rcShip.top+g_imgLabelShip.height;
	g_rcShip.right=g_rcShip.left+g_imgLabelShip.width;

		
	//definisce la funzione di pulizia del background del I schema
	//colore sfondo del cielo
	//variabili usate da ClearBack1
	g_ddbltfx.dwFillColor=g_gm.CreateRGB(99,127,252);
	
	g_rcFill.left=0;
	g_rcFill.right=SCREEN_WIDTH;
	g_rcFill.top=90;
	g_rcFill.bottom=SCREEN_HEIGHT -125;

	lefts=195;
	tops=10;
	widths=5;
	heights=15;
	
	DD_INIT_STRUCT(g_blxEn);
	g_blxEn.dwFillColor=g_gm.CreateRGB(252,220,90); //colorre barra energia

	DD_INIT_STRUCT(g_blxEn1); 
	g_blxEn1.dwFillColor=g_gm.CreateRGB(255,80,10); //colore energia quando diventa poca

    //definisce i rettangoli di blitting per il misuratore di energia della status bar
	for (dwCount=0;dwCount<10;dwCount++)
	{
		g_rcEnergy[dwCount].left=lefts;
		g_rcEnergy[dwCount].top=tops;
		g_rcEnergy[dwCount].right=lefts+widths;
		g_rcEnergy[dwCount].bottom=tops+heights;

		lefts += (widths +2);
	}
	
	CreateResHandler();

	return S_OK;		
}

///////////////////////////////////// CreateResHandler ////////////////////////////

//popola i vettori g_pBonus e g_pEnemy che puntano alle risorse disponibili
/*
nota: quando si aggiungono id modificare enemy_enum in sbl.h,m_label_hash in sbl.cpp e
e la funzione CreateResHanler in sbengine.cpp e e m_enemy_names[] in sbengine.cpp
*/

void CreateResHandler(void)
{
	int count;

	//bonus
	for (count=0;count<MAX_BONUS;count++)
	{
		g_pBonus[count]=NULL;
	}

	g_pBonus[EB_POWERUP1]=&g_sBonusGreen;
	g_pBonus[EB_SPECIAL]=&g_sBonusRed;
	g_pBonus[EB_SHIELD]=&g_sBonusBlue;
	g_pBonus[EB_ELECTRIC_BARRIER]=&g_sBonusElectricBarrier;
	g_pBonus[EB_ENERGY]=&g_sBonusYellow;
	g_pBonus[EB_POWERUP2]=&g_sBonusOrange;
	g_pBonus[EB_BOMBS10]=&g_sBonusBombs10;
	g_pBonus[EB_BOMBS50]=&g_sBonusBombs50;
	g_pBonus[EB_SCORE]=&g_sBonusScore;
	g_pBonus[EB_THRUST]=&g_sBonusThrust;
	g_pBonus[EB_BOOSTER]=&g_sBonusBooster;
	g_pBonus[EB_THUNDER]=&g_sBonusThunder;
	g_pBonus[EB_MISSILE]=&g_sBonusMissile;
	g_pBonus[EB_LASER]=&g_sBonusLaser;
	g_pBonus[EB_WEAPONA]=&g_sBonusWpA;
	g_pBonus[EB_WEAPONB]=&g_sBonusWpB;
	g_pBonus[EB_WEAPONC]=&g_sBonusWpC;
	g_pBonus[EB_WEAPOND]=&g_sBonusWpD;	
    g_pBonus[EB_GYROS]=&g_sBonusRed;
	g_pBonus[EB_GRND_FIRE]=&g_sBonusGrndFire; //fuoco verso terra
	g_pBonus[EB_TRIPLE_FIRE]=&g_sBonusTripleFire; //triplo fuoco
    g_pBonus[EB_STRIGHT_MISSILE]=&g_sBonusStrightMissile; //miss
	g_pBonus[EB_1UP]=&g_sBonus1Up;
	g_pBonus[EB_BLUE_SHELL]=&g_sBonusBlueShell;
	g_pBonus[EB_FWSHELLS]=&g_sBonusFwShells;
	g_pBonus[EB_LASER3]=&g_sBonusLaser3;

	//nemici
	//ogni oggetto colpibile ha un ID di risorsa univoco ed � associato ad uno stack di oggetti
	//Alcune risorse sono comuni a tutti gli schemi, altre invece sono ausiliarie e vengono caricate solo quando necessario
	for (count=0;count<MAX_ENEMIES;count++)
	{
		g_pEnemy[count]=NULL;
	}

	g_pEnemy[EN_UFO]=&g_sUfo;
	g_pEnemy[EN_GOLDENFIGHTER]=&g_sGoldenFighter;
	g_pEnemy[EN_ANTIAIRCRAFT]=&g_sAntiAircraft;
	g_pEnemy[EN_ANTIAIRCRAFT2]=&g_sAntiAircraft2;
	g_pEnemy[EN_RADAR]=&g_sRadar;
	g_pEnemy[EN_SNAKE]=&g_sSnake;
	g_pEnemy[EN_XWING]=&g_sXWing;
	g_pEnemy[EN_LAMBDAFIGHTER]=&g_sLambda;
	g_pEnemy[EN_MFIGHTER]=&g_sMFighter;
	g_pEnemy[EN_AIRCRAFTCARRIER]=&g_sAircraftCarrier;
	g_pEnemy[EN_TANK]=&g_sTank;
	g_pEnemy[EN_SHIP]=&g_sShip;
	g_pEnemy[EN_UFO2]=&g_sUfo2;
	g_pEnemy[EN_BLASTER]=&g_sBlaster;

	//oggetti ausiliari
	g_pEnemy[EN_SHANTY1]=&g_sShanty1;
	g_pEnemy[EN_SHANTY2]=&g_sShanty2;
	g_pEnemy[EN_SHANTY3]=&g_sShanty3;
	g_pEnemy[EN_SHANTY4]=&g_sShanty4;
	g_pEnemy[EN_FACTORY]=&g_sFactory;
	g_pEnemy[EN_PYLON]=&g_sPylon;
	g_pEnemy[EN_STREET]=&g_sStreet;
	g_pEnemy[EN_PALMS]=&g_sPalmThicket;
	g_pEnemy[EN_WALL]=&g_sWall;	
	g_pEnemy[EN_BOSS1]=&g_sBoss1;
	g_pEnemy[EN_BOSS2]=&g_sBoss2;
	g_pEnemy[EN_BOSS3]=&g_sBoss3;
	g_pEnemy[EN_BOSS4]=&g_sBoss4;
	g_pEnemy[EN_BOSS5]=&g_sBoss5;
	g_pEnemy[EN_SNAKERED]=&g_sSnakeRed;
	g_pEnemy[EN_SPIDER1]=&g_sSpider1; //ragno con tentacolo
	g_pEnemy[EN_SPIDER2]=&g_sSpider2; //ragno con bracci meccanici
	g_pEnemy[EN_SPIDER3]=&g_sSpider3; //ragno con torretta
	g_pEnemy[EN_ANTIAIRCRAFT3]=&g_sAntiAircraft3;
	g_pEnemy[EN_GFIGHTER]=&g_sGFighter; 
	g_pEnemy[EN_BUILDING1]=&g_sBuilding1;
	g_pEnemy[EN_BUILDING2]=&g_sBuilding2;
	g_pEnemy[EN_BUILDING3]=&g_sBuilding3;
	g_pEnemy[EN_BUILDING4]=&g_sBuilding4;
	g_pEnemy[EN_ICEBERG]=&g_sIceberg;
	g_pEnemy[EN_UFO3]=&g_sUfo3;
	g_pEnemy[EN_SCREW_FIGHTER]=&g_sScrewFighter;
	g_pEnemy[EN_NIGHT_BUILDING1]=&g_sNightBuilding1;
	g_pEnemy[EN_NIGHT_BUILDING2]=&g_sNightBuilding2;
	g_pEnemy[EN_NIGHT_BUILDING3]=&g_sNightBuilding3;
	g_pEnemy[EN_NIGHT_BUILDING4]=&g_sNightBuilding4;
	g_pEnemy[EN_NIGHT_BRIDGE]=&g_sNightBridge;
	g_pEnemy[EN_LFIGHTER]=&g_sLFighter;
	g_pEnemy[EN_BLASTER2]=&g_sBlaster2;
	g_pEnemy[EN_BLASTER3]=&g_sBlaster3;
	g_pEnemy[EN_MODULAR1]=&g_sModularEnemy[0];
	g_pEnemy[EN_MODULAR2]=&g_sModularEnemy[1];
	g_pEnemy[EN_MODULAR3]=&g_sModularEnemy[2];
	g_pEnemy[EN_MODULAR4]=&g_sModularEnemy[3];
	g_pEnemy[EN_MODULAR5]=&g_sModularEnemy[4];
	g_pEnemy[EN_MODULAR6]=&g_sModularEnemy[5];
	g_pEnemy[EN_MODULAR7]=&g_sModularEnemy[6];
	g_pEnemy[EN_MODULAR8]=&g_sModularEnemy[7];
	g_pEnemy[EN_SNAKERED1]=&g_sSnakeRed1;
	g_pEnemy[EN_SNAKERED2]=&g_sSnakeRed2;
	g_pEnemy[EN_JELLYSHIP]=&g_sJellyShip;
	g_pEnemy[EN_ASTEROID_SMALL]=&g_sAsteroidSmall; 
	g_pEnemy[EN_ASTEROID_MEDIUM]=&g_sAsteroidMedium;
	g_pEnemy[EN_ASTEROID_BIG]=&g_sAsteroidBig;
    g_pEnemy[EN_SHIELD_BASE]=&g_sShieldBase;
	g_pEnemy[EN_SHELL_BASE]=&g_sShellBase;
	g_pEnemy[EN_TURRET]=&g_sTurret;
	g_pEnemy[EN_TRAIN]=&g_sTrain;
	g_pEnemy[EN_RAIL]=&g_sRail;
	g_pEnemy[EN_SPACE_BUILD1]=&g_sSpaceBuild1;
	g_pEnemy[EN_SPACE_BUILD2]=&g_sSpaceBuild2;
	g_pEnemy[EN_SPACE_BUILD3]=&g_sSpaceBuild3;
	g_pEnemy[EN_SPACE_BUILD4]=&g_sSpaceBuild4;
	g_pEnemy[EN_HTRUSS]=&g_sHTruss;
	g_pEnemy[EN_VTRUSS]=&g_sVTruss;	
	g_pEnemy[EN_RING]=&g_sRing;
	g_pEnemy[EN_MAMMOTH]=&g_sMammoth;
	g_pEnemy[EN_LIFTER]=&g_sLifter;
	g_pEnemy[EN_BLEEZER]=&g_sBleezer;
	g_pEnemy[EN_HFIGHTER]=&g_sHFighter;
}

///////////////////////////////////// GetResId /////////////////////////////////////////////////
//Restituisce l'id della risorsa a partire dallo stack di oggetti dei nemici
int GetResId(CObjectStack *pstack)
{
	if (!pstack) return -1;
	
	for (int cnt=EN_FIRST;cnt<=EN_LAST;cnt++)
	{
		if (pstack==g_pEnemy[cnt]) return cnt;
	}

	for (cnt=0;cnt<MAX_BONUS;cnt++)
	{
		if (pstack==g_pBonus[cnt]) return cnt;
	}

	return -1; //oggetto non trovato

}

//////////////////////////////////// FreeObjects ///////////////////////////////////////////////
//Rilascia tutti gli oggetti creati da CreateObjects
void FreeObjects(void)
{
	g_fm.FreeImgFrame(&m_imgStatusBar); //status bar
	
#ifdef _TESTGAME_

	g_cdb.WriteErrorFile("FREE OBJECTS");

#endif
	//immagine background
	g_fm.FreeImgFrame(&m_imgBackGround);

	//queste collezioni contengono degli oggetti istanziati con new e quindi vanno eliminati con delete
	g_sShell.Clear(TRUE);
	g_sShell1.Clear(TRUE);
	g_sShell2.Clear(TRUE);
	g_sShell3.Clear(TRUE);
	g_sShell4.Clear(TRUE);
	g_sShield.Clear(TRUE);
	g_sElectricBarrier.Clear(TRUE);
	g_sGyros.Clear(TRUE);
	g_sEShell1.Clear(TRUE);
	g_sEShell2.Clear(TRUE);
	g_sEShell3.Clear(TRUE);
	g_sEShell4.Clear(TRUE);
	g_sEShell5.Clear(TRUE);
	g_sEShell6.Clear(TRUE);
	g_sEShell7.Clear(TRUE);
	g_sEShell8.Clear(TRUE);
	g_sEShell9.Clear(TRUE);
	g_sEShell10.Clear(TRUE);
	g_sEShell11.Clear(TRUE);
	g_sEShell12.Clear(TRUE);
	g_sShellBlue.Clear(TRUE);
	g_sFwBullet.Clear(TRUE);
	g_sExplBig.Clear(TRUE);
	g_sExplMediumHit.Clear(TRUE);
	g_sExplMedium.Clear(TRUE);
	g_sExplSmall.Clear(TRUE);
	g_sLaser.Clear(TRUE);
	g_sDBomb1.Clear(TRUE);
	g_sDBomb2.Clear(TRUE);
	g_sDBomb3.Clear(TRUE);
	g_sSMissile.Clear(TRUE);
	g_sFwMissile1.Clear(TRUE); 
	g_sDebris1.Clear(TRUE);
	g_sDebris2.Clear(TRUE);
	g_sDebris3.Clear(TRUE);
	g_sDebris4.Clear(TRUE);
	g_sDebris5.Clear(TRUE);
	g_sDebris6.Clear(TRUE);
	g_sDebris7.Clear(TRUE);
	g_sSplash.Clear(TRUE);
	g_sIceHole.Clear(TRUE);
	g_sGrndImpact.Clear(TRUE);
	g_sBooster.Clear(TRUE);
	g_sThunderBolt.Clear(TRUE);
	g_sSmoke.Clear(TRUE);
	g_sSmokeSrc.Clear(TRUE);
	g_sWhiteSmoke.Clear(TRUE);
	g_sBlackSmoke.Clear(TRUE);
    g_sUfo.Clear(TRUE);
	g_sGoldenFighter.Clear(TRUE);
	g_sAntiAircraft.Clear(TRUE);
	g_sXWing.Clear(TRUE);
	g_sUfo3.Clear(TRUE);
	g_sScrewFighter.Clear(TRUE);
	g_sBlaster.Clear(TRUE);
	g_sTank.Clear(TRUE);
	g_sBonusGreen.Clear(TRUE);
	g_sBonusRed.Clear(TRUE);
	g_sBonusBlue.Clear(TRUE);
	g_sBonusOrange.Clear(TRUE);
	g_sBonusYellow.Clear(TRUE);
	g_sBonusBombs10.Clear(TRUE);
	g_sBonusBombs50.Clear(TRUE);
	g_sBonusScore.Clear(TRUE);
	g_sBonusBooster.Clear(TRUE);
	g_sBonusThrust.Clear(TRUE);
	g_sBonusMissile.Clear(TRUE);
	g_sBonusThunder.Clear(TRUE);
	g_sBonusLaser.Clear(TRUE);
	g_sBonusWpA.Clear(TRUE);
	g_sBonusWpB.Clear(TRUE);
	g_sBonusWpC.Clear(TRUE);
	g_sBonusWpD.Clear(TRUE);
	g_sBonus1Up.Clear(TRUE);
	g_sBonusTripleFire.Clear(TRUE);
	g_sBonusElectricBarrier.Clear(TRUE);
	g_sBonusStrightMissile.Clear(TRUE);
	g_sBonusGrndFire.Clear(TRUE);
	g_sBonusBlueShell.Clear(TRUE);
    g_sBonusFwShells.Clear(TRUE);
	g_sBonusLaser3.Clear(TRUE);
	g_sLabelPowerUp.Clear(TRUE);
	g_sLabelShield.Clear(TRUE);
	g_sLabel1Up.Clear(TRUE);
	g_sLabelScore.Clear(TRUE);
	g_sLabelEnergy.Clear(TRUE);
	g_sLabelThrust.Clear(TRUE);
	g_sLabelBombs10.Clear(TRUE);
	g_sLabelBombs50.Clear(TRUE);
	g_sGameOver.Clear(TRUE);
	g_sFire.Clear(TRUE);
	g_sAircraftCarrier.Clear(TRUE);
	g_sShip.Clear(TRUE);
	g_sAntiAircraft2.Clear(TRUE);
	g_sUfo2.Clear(TRUE);
	g_LevelMessage.FreeText();
	g_LevelName.FreeText();
	g_sNozzleFlames.Clear(TRUE);
	g_sNozzleFlamesV.Clear(TRUE);


	//rilascia eventuali risorse ausiliarie
	FreeAuxResources();

	//rilascia la memoria dei path cinematici
	SAFE_DELETE_ARRAY(g_SnakePath1);
	SAFE_DELETE_ARRAY(g_SnakePath2);
	SAFE_DELETE_ARRAY(g_SnakePath3);
	SAFE_DELETE_ARRAY(g_Path1);
	SAFE_DELETE_ARRAY(g_Path2);
	SAFE_DELETE_ARRAY(g_Path3);
	SAFE_DELETE_ARRAY(g_Path4);
	SAFE_DELETE_ARRAY(g_Path5);
	SAFE_DELETE_ARRAY(g_Path6);
	SAFE_DELETE_ARRAY(g_Path7);
	SAFE_DELETE_ARRAY(g_Path8);
	SAFE_DELETE_ARRAY(g_Path9);
	SAFE_DELETE_ARRAY(g_Path10);

	//g_fm.FreeImgFrame(&m_imgCursor);

	//SAFE_DELETE_ARRAY(g_dwClMatch);
#ifdef _DEBUG_
	_CrtCheckMemory();
#endif

}

/////////////////////////////////// FreeData ////////////////////////////////////////////////
//Rilascia la memoria allocata da InitData
void FreeData(void)
{	

	g_objStack.Clear(FALSE); //lo stack degli oggetti principale contiene dei riferimenti agli oggetti che non vanno eliminati con delete

	//rilascia le frames del player
	Player1.Release();
	//rilascia il cursore del mouse
	g_Cursor.Release();
	
}

/////////////////////////////////// CFastChars //////////////////////////////////////////////

void CFastChars::BlitString(LONG xout,LONG yout,TCHAR *szText,IMAGE_FRAME_PTR imgDest)
{
	int iChar;
	TCHAR *szBuffer;
	
	szBuffer=szText;
	
	iChar=(int) szBuffer[0];

	while(iChar)
	{		
		xout += m_lCharWidth;				
		
		szBuffer++;

		iChar=(int) szBuffer[0];
		
		//da finire
		
	}
}



///////////////////////////////////// CPlayer ////////////////////////////////////////////////////
//Classe CPlayer

//Il costruttore resetta i valori
CPlayer::CPlayer(void)
{
	dwType=1000;
	dwScore=0;
    dwLives=3;
	bBlinking=FALSE;
	iBlkStep=0;
	iBlkCount=0;
	bInvisible=FALSE;
	dwClass=CL_PLAYER;
	pfnGameOver=NULL; //stack degli oggetti game over 
	pfnSmoke=NULL;
	dwShells1=dwShells2=dwShells3=0;
	dwShells3Full=0;	
	Fire1=Fire2=Fire3=NULL;
	bExploding=FALSE;
	pNozzleFlames=NULL;
	bNozzleFlames=FALSE;
	pfnStoredFire=NULL;
	dwSpecialShells=0;
	this->Reset();
}

//----------------------------------- CPlayer::Reset ----------------------------------------------

//imposta lo stato iniziale
void CPlayer::Reset(void)
{
	cur_frame=6;
	bActive=TRUE;
	dwEnergy=80; //imposta il valore massimo dell'energia	
	dwPower=2;   //potere distruttivo (� l'enrgia tolta ad un oggetto quando urta con esso)
	dwShells1=-1; //arma primaria (infiniti)
	dwShells2=40; //bombe
	dwShells3=0;
	dwShells4=0;
	dwShells5=0;
	dwShells3Full=dwShells3;
	dwSpecialShells=0;
	dwWeapon1=0; 	
	dwWeapon2=0;
	dwWeapon3=0;
	iFire1Delay=20; //ritardo fuoco 1
	iStoredDelay1=20;
	iFire2Delay=20; //ritardo fuoco 2
	iFire3Delay=20;
	iDisplacement=5; //spostamanto lungo x e y ad ogni input
	m_dwTimer1=0;
	szName=NULL;
	bExitGame=FALSE;
	bInvisible=FALSE;
	bExploding=FALSE;
	bNozzleFlames=FALSE; 
	bElectricBarrier=FALSE;
	dwType=1000;
	iy=0;	
	dwClass=CL_PLAYER;
	bGrndFire=bTripleFire=bStrightMissile=FALSE;
	dwStatus=1; //� vivo
    //posizione iniziale del player; questa � anche la posizione di inizio dopo che viene ucciso
	SetPosition(20,120);
	//frame iniziale (trim=0)
	SetCurFrame(6);	
}

//------------------------------ CPlayer::Explode -------------------------------------------------

void CPlayer::Explode(void)
{
	return; //il player non usa questa funzione per esplodere (va definita perch� altrimenti richiama CSBObject::Explode

}

//------------------------------ CPlayer::SendInput -----------------------------------------------
//invia l'input del giocatore all'astronave del player
HRESULT CPlayer::SendInput(PL_INPUT_PTR pInput) 
{	

//	static int iCd=3; //aumentando il count down diventa piu' lenta l'inclinazione del caccia (roll)
  //  static int iDelay1=1;
    static int iLastFire1=0; //delay del fuoco
	static int iLastFire2=0;   
	static int iLastFire3=0;

		
	if (dwEnergy >0)
	{
		//funzionamento normale
	
		iy=pInput->y_axis;

		x += pInput->x_axis;
		y += iy;

		
		if ( x > SCREEN_WIDTH -100) x=SCREEN_WIDTH -100;
		if ( x < 0) x=0;
		if (y < 0) y=0;
		if (y > SCREEN_HEIGHT - 120) y= SCREEN_HEIGHT -120;
		

		iLastFire1++;
		
		if (pInput->fire1 && iLastFire1>iFire1Delay) 
		{
				iLastFire1=0;
				Fire1(x,y); //fuoco primario
				if (bTripleFire) 
				{	
					dwShells5--;
					PlTripleFire(x,y);
					if (dwShells5<=0) bTripleFire=0;
				}
				else if (bGrndFire) 
				{				
					dwShells5--;
					PlFireGrnd(x,y);				
					if (dwShells5<=0) bGrndFire=0;
					
				}
				
				if (bStrightMissile)
				{
					dwShells4--;
					PlFireMissile(x,y);
					if (dwShells4<=0) bStrightMissile=FALSE;
				}

				if (dwSpecialShells>0)
				{
					//sta usando dei proiettili speciali (es. blue shells)
					dwSpecialShells--;
					if (dwSpecialShells<=0)
					{
						dwSpecialShells=0;
						if (pfnStoredFire)
						{
							//ripristina la vecchia funzione di fuoco
							Fire1=pfnStoredFire;
							iFire1Delay=iStoredDelay1;
						}
						else 
						{
							Fire1=PlFire1;
							iFire1Delay=20;
						}
					}
				}
		}

		iLastFire2++;

		//bombe
		if (pInput->fire2 && iLastFire2>iFire2Delay && dwShells2>0)
		{
			    dwShells2--;
				iLastFire2=0;
				Fire2(x,y);
		}

		iLastFire3++;

		//arma speciale
		if (pInput->fire3 && dwShells3>0 && iLastFire3>iFire3Delay)
		{
			iLastFire3=0;
			if (Fire3) 
			{
				dwShells3--;
				Fire3(x,y);
			}
		}
		

	}
	
	return S_OK;
}

//------------------------------------- CPlayer::Update--------------------------------------
//Aggiorna la posizione prendendo l'input dal giocatore tramite la periferica impostata
//all'inizio

BOOL CPlayer::Update(void)
{
	static int iCd=3;
	static int iDelay1=1;
	static int icnt1=0;
	//int iy;
	
	//bBlinking significa che il playr � stato ucciso ed � appena ripartito con una nuova vita
	//in questa fase � invulnerabile
	if (bBlinking)
	{
		iBlkCount++;		
		
		if (iBlkCount>=200)
		{
			bBlinking=FALSE;
			bInvisible=FALSE;
			dwClass=CL_PLAYER; //rende il player di nuovo colpibile			

		}
		else
		{
	
			iBlkStep++;
            //visualizza il player in modo intermittente
			if (iBlkStep==7)
			{
				bInvisible=!bInvisible; //bInvisible indica se il player � invisibile
				iBlkStep=0; //resetta il contatore di intermittenza
			}
		}

	}
	

	//lpfnGetInput();	
    
	if (dwEnergy>0)
	{
		
		iCd--; //decrementa il count down

		if (!iCd)
		{
			iCd=3; //resetta il count down
			if (iy !=0)
			{	

				if (iy > 0) cur_frame--;
				else cur_frame++;		

				if (cur_frame<0) cur_frame=0;
				else if (cur_frame>12) cur_frame=12;
			}
			else 
			{
				if (iDelay1)
				{
					//tende a far ritornare il caccia in posizione normale se non si preme UP o DOWN
					if (cur_frame > 6)
					{
						cur_frame--;				
					}
					else if (cur_frame < 6)
					{
						cur_frame++;					
					}
				}

				//cambia lo stato di iDelay in modo da rallentare il ritorno in pos normale
				iDelay1=!iDelay1;
			}
		}

		if (dwEnergy<24)
		{
			icnt1++;
			if (icnt1==5)
			{
				icnt1=0;
				if (pfnSmoke) pfnSmoke(x,y+8);
			}
		}



	}
	else
	{
		//quando ha finito le vite, lo stato � zero e non deve continuare ad esplodere
		if (!dwStatus) return FALSE;

		dwClass=CL_NONE; //in questa fase non � piu' colpibile ulteriormente
		//il player sta esplodendo 
		//il player non ha piu' energia e quindi esplode
		iCd--;

		if (!bExploding)
		{
			bExploding=TRUE;
			PushExpl8(x-10,y-30);
			PushExpl8(x+20,y-10);
			PushExpl9(x+10,y+10);
			cur_frame=12;
		}
		
		if (iCd<=0)
		{
			iCd=8;
			cur_frame++;	
			PushExpl8(x+20,y-10);
	
			if (cur_frame>max_frame_index) 
			{
				//decrementa il numero di vite
				dwLives--;			
				
				if (dwLives<=0)
				{
					if (dwStatus)
					{
					//GAME OVER					
					
						//fine partita: il player non � piu' visibile
						bInvisible=TRUE;
						dwClass=CL_NONE;	
						bActive=FALSE;
						dwStatus=0; //il player � morto

						if (pfnGameOver) pfnGameOver(x,y);				
					}
				}
				else
				{	
					DWORD dw2;
					dw2=dwShells2;
					//resetta il flag dell'esplosione				   					
					Reset();
					//mantiene le bombe
					if (dw2>dwShells2) dwShells2=dw2;
					iCd=3;
					SetCurFrame(0);
					if (pfnUpdateBar) pfnUpdateBar();
					//in questo stato il player � temporaneamente invulnerabile
					SetBlinking();
					bActive=TRUE;													
					Fire1=PlFire1; //riparte con la funzione di fuoco iniziale
					Fire2=PlDropBomb1;
				}

			}

		}

	}

	return (dwStatus==1); //rende true se � vivo
}

//--------------------------------------- CPlayer::IsBlinking --------------------------------------------
//rende TRUE se il player � in modalit� "blinking" 
BOOL CPlayer::IsBlinking(void)
{
	return bBlinking;
}

void CPlayer::SetBlinking(void)
{
	bBlinking=TRUE;
	iBlkStep=0;
	iBlkCount=0;
	dwClass=CL_NONE;//questo lo rende non colpibile
}

//--------------------------------------- CPlayer::IsInvisible ----------------------

BOOL CPlayer::IsInvisible()
{
	return bInvisible;
}

//--------------------------------------- CPlayer::Draw -------------------------------------------------

HRESULT CPlayer::Draw(void)
{

	static IMAGE_FRAME_PTR pimg;
	static LPDIRECTDRAWSURFACE7 lpDDS;
	
	pimg=frames[cur_frame];

	if (!bInvisible)
	{	
		rcBoundBox.left=x;
		rcBoundBox.top=y;
		rcBoundBox.right=x+pimg->width;
		rcBoundBox.bottom=y+pimg->height;
		lpDDS=m_pimgOut->surface;			
		
		if (bNozzleFlames && !bExploding)
		{			
			//fiamme ugello
			pNozzleFlames->UpdateFrame();		
			pNozzleFlames->RenderToDisplay(x-11,y+23);

		}

		return lpDDS->Blt(&rcBoundBox,pimg->surface,NULL,DDBLT_KEYSRC,NULL);

	}

	else return E_FAIL;

}


//----------------------------------- CAnimatedObject ----------------------------------------------

CAnimatedObject::CAnimatedObject(void)
{
	m_iTotalCycles=0;
	m_iCycleStep=0;
}

void CAnimatedObject::Reset(void)
{
	CSBObject::Reset();
	SetCurFrame(0);
	m_iCycleStep=0;
}

void CAnimatedObject::SetCycles(int icyc)
{
	if (icyc>0) m_iTotalCycles=icyc;
}

BOOL CAnimatedObject::Update(void)
{
	//aggiorna la frame
	UpdateFrame();

	if (m_iTotalCycles>0 && cur_frame==0)
	{
		//ha compiuto un ciclo completo di animazione
		m_iCycleStep++;
		if (m_iCycleStep>=m_iTotalCycles) return FALSE;
	}

	return CSBObject::Update();
}


//////////////////////////////////// CFwBullet ///////////////////////////////////////////////////

CFwBullet::CFwBullet(void)
{
	m_pTarget=NULL;
	m_pSmoke=NULL;
	m_pNextTg[0]=NULL;
	m_ittl=0;
}

CFwBullet::~CFwBullet(void)
{
	m_pTarget=NULL;
	m_pSmoke=NULL;
}

void CFwBullet::Reset(void)
{
	CEnemy::Reset();
	m_pTarget=NULL;
	m_pNextTg[0]=NULL;
	iConfigFreq=4;
	iUpdateFreq=8;	
	g_cs.PlaySound(g_snMissile,0,0);
	m_ittl=10; //valore di default
}

//imposta il "time to live", quando questo contatore raggiunge zero smette di inseguire
void CFwBullet::SetTTL(int ttl)
{
	if (ttl>0) m_ittl=ttl;
	else m_ittl=0;
}

BOOL CFwBullet::Update(void)
{
	if (++iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;	

		//quando il contatore ttl reggiunge zero l'oggetto non insegue piu'
		if (m_ittl>0)
		{
			m_ittl--;

			//controlla che si sia un bersaglio e che abbia energia
			if (m_pTarget && m_pTarget->GetEnergy()>0)
			{

				int ang=g_fmath.GetAngle(x,y,m_pTarget->x,m_pTarget->y);
				int adiff=g_fmath.GetAngleDiff(GetAngle(),ang,10);
				//cambio traiettoria
				SetVelocity(GetAngle()+adiff*10,speed);

			}

			else 
			{
				//cerca il primo bersaglio disponibile
				GetTargets(m_pNextTg,1);
				m_pTarget=m_pNextTg[0];

			}
		}
	}

	if (++iStepConfig>=iConfigFreq)
	{
		//scia di fumo
		iStepConfig=0;

		if (m_pSmoke)
		{
			//fumo
			CDebris *pobj=NULL;

			pobj=(CDebris *)m_pSmoke->Pop();

			if (pobj)
			{
				pobj->SetPosition(x,y+3);
				pobj->SetVelocity(180,CSbLevel::g_scroll_speed);
				pobj->Reset();
				pobj->SetG(0); //no gravit�
				g_objStack.Push(pobj);
			}

		}
	}

	UpdatePosition();	

	//si schianta al suolo
	if (y>CSBObject::g_iScreenHeight-CSBObject::g_iScreenBottomHeight)
	{
		this->lpfnExplode(x,y);
		return FALSE;	
	}

	return (dwEnergy>0 && x>-80 && y>-80 && x<CSBObject::g_iScreenWidth+80 && y<CSBObject::g_iScreenHeight+80);	
}

void CFwBullet::SetTarget(CSBObject *target)
{
	m_pTarget=target;
}

void CFwBullet::SetSmoke(CObjectStack *smoke)
{
	m_pSmoke=smoke;

}


////////////////////////////////// classe CShell /////////////////////////////////////////////////

CShell::CShell(void)
{
	dwScore=0;
	dwType=6;
	dwClass=CL_ENFIRE;
}

//estende CSBObject
//Proiettile che si muove in senso orizzontale
BOOL CShell::Update(void)
{
	x += iVelx;	

	if (dwEnergy<0) return FALSE;
	return (!(x>SCREEN_WIDTH || x<-10)); 
}

//////////////////////////////////// classe CLaser /////////////////////////////////////////////////

CLaser::CLaser(void)
{
	dwClass=CL_ENFIRE;
	dwType=27;
	iLasts=10;
	m_lOrigW=0;
	m_lLaserLength=0;
//	m_bp.poly=new VERTEX2D[64];
//	m_bp.inum=64;
}

CLaser::~CLaser(void)
{
//	SAFE_DELETE_ARRAY(m_bp.poly);
}

void CLaser::Reset(void)
{
	iUpdateStep=0;
    this->SetCurFrame(0);

//	SetBoundPoly(0,&m_bp);
    

    frames[cur_frame]->width=m_lLaserLength;

}

void CLaser::SetProps(LONG orig_width,LONG length)
{
	m_lOrigW=orig_width;
	m_lLaserLength=length;

}

BOOL CLaser::Update(void)
{	
	/*int cur=x;

	for (int count=0;count<32;count++)
	{
		m_bp.poly[count].x=cur;
		m_bp.poly[count].y=y;
		cur+=20;
	}
	
	for (count=32;count<64;count++)
	{
		m_bp.poly[count].x=cur;
		m_bp.poly[count].y=y+8;
		cur-=20;
	}*/


	iUpdateStep++;
	//this->UpdateFrame();	
	//imposta la lunghezza della frame
	frames[cur_frame]->width=m_lLaserLength;
	return iUpdateStep<iLasts;
}


HRESULT CLaser::Draw(void)
{
//	LONG lw;	
//	lw=SCREEN_WIDTH;
	frames[cur_frame]->width=m_lOrigW;
	g_fm.PutImgFrameResize(x,y,m_pimgOut,frames[cur_frame],m_lLaserLength);	
	frames[cur_frame]->width=m_lLaserLength;
	
	return S_OK;
}

/////////////////////////////////// classe CDropBomb ///////////////////////////////////////////////
//estende CSBobject

CDropBomb::CDropBomb(void)
{
	dwScore=100;
	iChangeDir=6; //inizializza il count down;
	dwType=1;	
	ig=0;
}

void CDropBomb::Reset(void)
{
	CSBObject::Reset();
	if (CSbLevel::g_gravity>0)
	{
		ig=1;
	}
	else ig=0;

}

BOOL CDropBomb::Update(void)
{	
	iChangeDir--;    
	
	if (iChangeDir == 0)
	{
		iChangeDir=6;
		if (cur_frame < max_frame_index)
		{			
			cur_frame++;					
		}	

		speed+=ig;
	}

	UpdatePosition();
    
	if (y > CSBObject::g_iScreenHeight-CSBObject::g_iScreenBottomHeight)
	{
		//acquisisce il tipo di terreno nel punto dell'impatto
		int iGroundType=CCurLevel.GetGroundType(x+18);

		if (iGroundType==LV_INCONSISTENT)
		{			
			//in questo caso � su un tile vuoto (non c'� suolo)
			return (y< CSBObject::g_iScreenHeight); 
		}
		else
		{			
			if (CSBObject::g_iScreenBottomHeight>0)
			{
				this->lpfnExplode(x,y); //la bomba esplode sul terreno
			}
		
			return FALSE;		
		}
	}

	else if (dwEnergy<=0) return FALSE;

	else return TRUE;

}



//////////////////////////////////////// classe CFwMissile /////////////////////////////////////
//missile inseguitore

CFwMissile::CFwMissile(void)
{

	dwScore=10;
	Reset();
	iUpdateFreq=10;	
	dwClass=CL_ENEMY;
	m_pTarget=NULL;
	dwType=4;
}

void CFwMissile::Reset(void)
{
	CSBObject::Reset();
	iStep=0;
	iStartFwStep=5;
	iUpdateStep=0;
	iUpdateFreq=12;
	iFinishFwStep=115;
	m_pTarget=NULL;
	dwEnergy=1;
	SetVelocity(180,speed);
	SetCurFrame(11);
}
//------------------------------------ CFwMissile::Update  ----------------------------------------------

BOOL CFwMissile::Update(void)
{
	iStep++; 
	iUpdateStep++;		
	
	if (iStep>=iStartFwStep && iStep<=iFinishFwStep)
	{
		if (iUpdateStep>=iUpdateFreq)
		{			
			//aggiorna la direzione
			iUpdateStep=0;
            //modifica la traiettoria in modo da inseguire il bersaglio
			RefreshParams();
		}
	}

	//aggiorna la posizione
	UpdatePosition();
    
	//l'oggetto esce dallo schermo
	if (!(x>=-40 && x<=CSBObject::g_iScreenWidth+80 && y>-20)) return FALSE;

	//il missile si schianta al suolo
	if (y>CSBObject::g_iScreenHeight-CSBObject::g_iScreenBottomHeight)
	{
		int iGroundType=CCurLevel.GetGroundType(x+18);

		if (iGroundType==LV_INCONSISTENT)
		{			
			//in questo caso � su un tile vuoto (non c'� suolo)
			return (y< CSBObject::g_iScreenHeight); 
		}

		else
		{
			this->lpfnExplode(x,y);
			return FALSE;
		}
	}
	
	if (dwEnergy<=0) return FALSE;

	return TRUE;

}

//---------------------------------------- CFwMissile::SetTarget --------------------------------------
//imposta l'oggetto bersaglio da inseguire
HRESULT CFwMissile::SetTarget(CSBObject *pTarget)
{
	if (pTarget)
	{
		m_pTarget=pTarget;
		return S_OK;
	}

	else return E_FAIL;
}

//---------------------------------------- CFwMissile::RefreshParams -----------------------------------
//aggiorna la traiettoria in modo da inseguire il bersaglio
void CFwMissile::RefreshParams(void)
{   
	int ang;	
	int g1,d1,g2,d2;
	int a1,a2;
    
	if (m_pTarget) 
	{
		//se viene impostato un target cerca di inseguire quello, altrimenti insegue il target di default
		if (m_pTarget->bActive)
		{
			//acquisisce le coordinate assolute dell'oggetto
			m_pTarget->GetAbsXY((int *)&lTargetX,(int *)&lTargetY);			
		}
	}

   	//acquisisce l'angolo della direzione fra il missile e il bersaglio (player in questo caso) da colpire
	ang=m_fn.GetAngle(x,y,lTargetX,lTargetY);	

	//varia la traiettoria solo se l'angolo differisce piu' di 18 gradi
	if (abs(ang-angle_dir)>18) 
	{

		//colacola quale variazione di direzione rende piu' favorevole la rotta del missile nel colpire il bersaglio
		a1=angle_dir+18;
		a2=angle_dir-18;

		g1=abs(ang-a1);
		g2=abs(ang-a2);
		d1=360-g1;
		d2=360-g2;

		if (g1<=d1) d1=g1;
		if (g2<=d2) d2=g2;

		//imposta la veloci� corrente
		if (d1<=d2) SetVelocity(a1,speed);
		else SetVelocity(a2,speed);
	

		//imposta la frame corrente
		SetCurFrame(angle_dir/15-1);
	}
}


///////////////////////////////////////////// classe CExplosion ///////////////////////////////////////////
//estende CSBobject

CExplosion::CExplosion(void)
{
	dwClass=CL_NONE;
	iStartTrigger=6; //il trigger attiva il cambio frame quando diventa 0
	iTrigger=iStartTrigger;
	dwType=3;
	idelay=0;
}

//imposta il trigger per il cambio della frame
void CExplosion::SetTrigger(int iTrigger)
{
	this->iStartTrigger=iTrigger; //la velocit� di cambio frame diminuisce quando aumenta iStartTrigger
	this->iTrigger=iTrigger;
}


BOOL CExplosion::Update(void)
{
	iTrigger--;
	idelay--;

	if (idelay<=0)
	{
		if (!bstarted)
		{
			//imposta il flag: esplosione avviata
			bstarted=TRUE;
			//emette il rumore
			if (isound_id>0) g_cs.PlaySound(isound_id,0,0);

		}

		if (iTrigger<=0)
		{
			iTrigger=iStartTrigger;//resetta

			cur_frame++; //incrementa la frame			
		}	
		

	}

	UpdatePosition();
	
	return (cur_frame <= max_frame_index);
}


int CExplosion::GetDelay(void)
{
	return idelay;
}

void CExplosion::SetDelay(int delay)
{
	if (idelay>=0)
	{
		idelay=delay;
	}
}


void CExplosion::Reset(int sound,int delay)
{
	//resetta 
	bstarted=FALSE;
	if (delay>=0)
	{
		//ritardo
		idelay=delay;
	}
	else idelay=0;
	if (sound>0)
	{
		//id del rumore
		isound_id=sound;
	}
	else isound_id=0;

}

HRESULT CExplosion::Draw(void)
{
	if (idelay<=0)
	{
		//inizia a disegnare solo dopo che � terminato il ciclo di ritardo
		return CSBObject::Draw();
	}

	else return TRUE;

}

////////////////////////////////////////// CGoldenFighter ///////////////////////////////////////////

CGoldenFighter::CGoldenFighter(void)
{
	dwScore=600;
	IAType=0;
	IAStep=0;
	iStepWeapon1=0;
	dwClass=CL_ENEMY;
	dwType=5;
}

//---------------------------------- CGoldenFighter::Reset -------------------------------------
//Resetta i contatori

void CGoldenFighter::Reset(void)
{	
	CEnemy::Reset();
	iStepFire=0;
	iStepPath=0;
	iStepUpdate=0;
	iStepConfig=0;
	IAStep=0;	
	iStepWeapon1=0;		
}

//---------------------------------- CgoldenFighter::Update -------------------------------------

BOOL CGoldenFighter::Update(void)
{
	LONG lD;
	CShell *pShell=NULL;
	CSBObject *pobj=NULL;
		
	iStepUpdate++;
    if (iStepUpdate == iUpdateFreq)
	{
		IAStep++; //incrementa lo step dell'algoritmo IA
		iStepUpdate=0;
        
		if (IAStep<2)
		{
			//fino a che IAStep<5 segue un path predefinito
			this->SetVelocity(pPath->Angle,pPath->Velocity);
			iUpdateFreq=pPath->Displ;
			pPath++;
			iStepPath++;

			this->UpdatePosition(); //aggiorna la posizione

			return (iStepPath < iStepPathNum);			
		}

		else if (IAStep<23)
		{
			//a questo punto inizia a spostarsi verso il giocatore			
			if (lTargetY < y) 	SetVelocity(angle_dir+10,speed);
			else 	SetVelocity(angle_dir-10,speed);
			speed=3;		
		}
		else
		{
			if (speed<5) speed++; 
			SetVelocity(180,speed);		
		}	
	}

	iStepFire++;

	//fuoco... primario
	if (iStepFire>=iFireFreq)
	{
	
		//spara se � davanti al player
		lD=lTargetY - y;
	
		if (lD<40 || lD>-40)
		{
			pShell=(CShell*) m_Cartridges->Pop();
			if (pShell)
			{
				//imposta posizione iniziale e velocit� proiettile
				pShell->SetPosition(x,y+15);
				pShell->dwClass=CL_ENFIRE;
				//immette il projettile nello stack attivo
				m_ActiveStack->Push(pShell);
			}
		
		}

		iStepFire=0;
	}
	
	iStepWeapon1++;

	if (iStepWeapon1>=45)
	{
		//missile inseguitore
		iStepWeapon1=0;
		pobj=m_pWeapon1->Pop(); //prende un missile dallo stack
	
		if (pobj)
		{
			pobj->Reset();			
			pobj->dwClass=CL_ENEMY;
			pobj->SetPosition(x,y+15);						
			m_ActiveStack->Push(pobj);
			g_cs.PlaySound(g_snFwMissile,0,0);
		
		}
	}

	this->UpdatePosition(); //aggiorna la posizione
	
	this->UpdateTrim(); //aggiorna l'assetto  	
    
    if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return (x>=-100 && x<=SCREEN_WIDTH+250 && y<SCREEN_HEIGHT && y>-50);
}

////////////////////////////////////// classe CUfo2 ///////////////////////////////////////////////

CUfo2::CUfo2(void)
{
	dwType=20;
	m_iAIAlgo=0;
	m_pfm=NULL;
	m_iAlgoStep=0;
	m_il1=0;
	m_il2=0;
	m_il3=0;
	m_iFreq=0;
	m_iFreqCnt=0;
	m_iShots=0;
}


//imposta il tipo di algoritmo AI
void CUfo2::SetAI(int iAlgo)
{
   if (iAlgo>0) m_iAIAlgo=iAlgo;

}

void CUfo2::Reset(void)
{
	dwClass=CL_ENEMY;
	CEnemy::Reset();
	SetVelocity(180,2);
	SetCurFrame(0);
	SetAnimSpeed(0.7f);
	iWoundedState=0;
	m_il1=m_pfm->RndFast(8)+10;
	m_il2=m_il1-4;
	m_iCurAlgo=m_iAIAlgo; //imposta l'algoritmo AI iniziale (puo' variare durante la vita dell'oggetto)
	if (m_dwFlags>0) m_iCurAlgo=m_dwFlags;
	
	m_iFreqCnt=0; //contatore raffiche
	m_iShotsCnt=0;
}

//imposta il tipo di raffica
void CUfo2::SetBurst(int iShots,int iFreq)
{
	if (m_iShots>=0 && iFreq>1)
	{
	   m_iShots=iShots;
       m_iFreq=iFreq;
	}
}

//specifica un algoritmo diverso da quello impostato
void CUfo2::SetFlags(DWORD dwFlags)
{
	if (dwFlags<=3) m_dwFlags=dwFlags;	
}

BOOL CUfo2::Update(void)
{
	int ang;

	iStepUpdate++;
	
    if (dwEnergy>0)
	{
		if (iStepUpdate >= iUpdateFreq)
		{
			iStepUpdate=0;
			m_iAlgoStep++;

			switch (m_iCurAlgo)
			{
			case 1: //algoritmo 1: segue il player , si ferma un po' e poi va via
				
				if (m_iAlgoStep>=4 && m_iAlgoStep<m_il1)
				{
					//si sposta verso il player
					//angolo del target
					ang=m_pfm->GetAngle(x,y,lTargetX,lTargetY);					
					if (angle_dir<ang-10) SetVelocity(angle_dir+12,2);
					else if (angle_dir>ang+10) SetVelocity(angle_dir-12,2);						

				}
			
				else if (m_iAlgoStep==m_il1)
				{
					//resta fermo in aria
					SetVelocity(180,0);
					g_cs.PlaySound(g_snBleep1,0,0);
				

				}

				else if (m_iAlgoStep>35)
				{
					speed++;
					angle_dir+=10;
					//accelera e sparisce in cielo
					SetVelocity(angle_dir,speed);
				}			
				
				break;


			case 2: //algoritmo 2: va in alto e bombarda
				
				if (m_iAlgoStep>m_il2)
				{

					if (m_iAlgoStep<45)
					{

						if (y>55)
						{
							speed++;
							angle_dir+=15;						
							if (speed>4) speed=4;
							SetVelocity(angle_dir,speed);						

						}
						else
						{
							if (y<55) y+=2; //se � troppo in alto lo fa ritornare verso il basso

							if (!(angle_dir==180 || angle_dir==0)) angle_dir=180;

							if (lTargetX>x+35)
							{
								if (angle_dir==180) speed--; //il player � a destra
								else speed++; //il player � a sinistra

							}
							else if (lTargetX<x-35)
							{
								if (angle_dir==180) speed++;
								else speed--;
							}
							else
							{
								//decelera per restare sopra il player
								speed--;						
							}

							if (speed<0) 
							{
								//inverte il movimento
								if (angle_dir==180) angle_dir=0;
								else if (angle_dir==0) angle_dir=180;
								speed=0;

							}
							else if (speed>4) speed=4;  //limita la velocit� per diminuire l'inerzia 

							SetVelocity(angle_dir,speed);

						}

					}

					else
					{
						//attiva l'algoritmo 1
						m_iCurAlgo=1;
						m_iAlgoStep=5;
					
					}

				}
				
				else SetVelocity(180,3);

				break;

			case 3: //algoritmo AI 3 : vola basso sotto il player


				if (m_iAlgoStep>m_il1)
				{
					if (m_iAlgoStep<45)
					{

						if (y<SCREEN_HEIGHT-190)
						{
							angle_dir-=15;
						}

						else
						{
							//vola basso e segue il player spostandosi in senso orizzontale
							if (!(angle_dir==180 || angle_dir==0)) angle_dir=180;

							if (lTargetX>x+35)
							{
								if (angle_dir==180) speed--; //il player � a destra
								else speed++; //il player � a sinistra

							}
							else if (lTargetX<x-35)
							{
								if (angle_dir==180) speed++;
								else speed--;
							}
							else
							{
								//decelera per restare sopra il player
								speed--;						
							}

							if (speed<0) 
							{
								//inverte il movimento
								if (angle_dir==180) angle_dir=0;
								else if (angle_dir==0) angle_dir=180;
								speed=0;

							}
							else if (speed>4) speed=4;  //limita la velocit� per diminuire l'inerzia 

						}

						SetVelocity(angle_dir,speed);
					}				

					else
					
					{
						angle_dir=270;
						speed++;
						SetVelocity(angle_dir,speed);					
					}
				}


			
				

				break;

                default:
				//comportamento di default: si muove di moto rettilineo uniforme

				break;


			}

		}

		//aggiorna la frame
		UpdateFrame();
		UpdatePosition();

		iStepFire++;
        //ufo 2  
		if (iStepFire>=iFireFreq)
		{
			CSBObject *pShell=NULL;

			if (y<lTargetY)
			{
				if (abs(x+65-lTargetX)<120 && m_pWeapon1)
				//l'oggetto � sopra il player e qundi bombarda
				{						

					pShell=m_pWeapon1->Pop();
					if (pShell)
					{
						pShell->dwClass=CL_ENFIRE;
						pShell->SetCurFrame(0);
						pShell->SetPosition(x+45,y+25);
						pShell->dwPower=6;
						pShell->SetInitialEnergy(1);
						//bomba
						if (CSbLevel::g_gravity>0)
						{
							pShell->SetVelocity(90,3); //cade verticalmente a 3 pixel/ciclo 						
						}
						else 
						{
							//in assenza di gravit�							
							pShell->SetVelocity(45,3); //bomba ferma
						}
											
						pShell->Reset();
						m_ActiveStack->Push(pShell);
						g_cs.PlaySound(g_snDrop,0,0);					

					}
				}

				iStepFire=0;
			}

			else
			{
				m_iFreqCnt++;

				if (m_iFreqCnt>=m_iFreq)
				{

					m_iFreqCnt=0;
					m_iShotsCnt++;

					if (m_iShotsCnt>=m_iShots)
					{
					   //fine raffica
					   iStepFire=0;
					   m_iShotsCnt=0;	
				
					}				
				
					if (m_Cartridges)
					{
						//estrae un proiettile
						pShell=m_Cartridges->Pop();
						if (pShell)
						{
							pShell->SetInitialEnergy(1);
							pShell->Reset(); //resetta l'oggetto
							pShell->dwClass=CL_ENFIRE;						
							pShell->SetVelocity(270,6);
							pShell->SetPosition(x+50,y+8);							
							m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi
							g_cs.PlaySound(g_snBlip,0,0);

						}
						//estrae un proiettile
						pShell=m_Cartridges->Pop();
						if (pShell)
						{
							pShell->SetInitialEnergy(1);
							pShell->Reset(); //resetta l'oggetto
							pShell->dwClass=CL_ENFIRE;							
							pShell->SetVelocity(255,6);
							pShell->SetPosition(x+50,y+8);
							m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi

						}
						//estrae un proiettile
						pShell=m_Cartridges->Pop();
						if (pShell)
						{
							pShell->Reset(); //resetta l'oggetto
							pShell->dwClass=CL_ENFIRE;
							pShell->SetVelocity(285,6);
							pShell->SetInitialEnergy(1);
							pShell->SetPosition(x+50,y+8);
							m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi

						}
						//estrae un proiettile
						pShell=m_Cartridges->Pop();
						if (pShell)
						{
							pShell->Reset(); //resetta l'oggetto
							pShell->dwClass=CL_ENFIRE;
							pShell->SetVelocity(240,6);
							pShell->SetInitialEnergy(1);
							pShell->SetPosition(x+50,y+8);
							m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi

						}

						//estrae un proiettile
						pShell=m_Cartridges->Pop();
						if (pShell)
						{
							pShell->SetInitialEnergy(1);
							pShell->Reset(); //resetta l'oggetto
							pShell->dwClass=CL_ENFIRE;						
							pShell->SetVelocity(300,6);
							pShell->SetPosition(x+50,y+8);
							m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi
							g_cs.PlaySound(g_snBlip,0,0);

						}
					}
				} //freq cnt
			}
		}

	}

	else 

	{
		//energia < 0
		if (iWoundedState)
		{			

			//il nemico sta precipitando con moto parabolico
			m_t += (float)0.1;
			x+=iVelx;
			y=(int)(m_t*m_t*CSbLevel::g_gravity+iVely*m_t+old_y);
		
			//l'oggetto sta precipitano
			if (iStepUpdate >= iUpdateFreq)
			{
					iStepUpdate=0; //resetta
					//emette fumo mentre precipita
					if (pfnSmoke) pfnSmoke(x+15,y+10);										
			}
		}
		else
		{
			//inizia a precipitare
			iUpdateFreq=(int)(iUpdateFreq*0.5);
			iWoundedState=1;
			m_t=0;
			old_x=x;
			old_y=y;
			if (iVelx==0 && CSbLevel::g_gravity==0) iVelx=-2; //serve a non far restare fermo l'oggetto dopo morto
		}		

		if (dwEnergy<-3)
		{
			CreateBonus();
			return FALSE;
		}

	}

	if (y>CSBObject::g_iScreenHeight-CSbLevel::g_bottom)
	{
	    
		//acquisisce il tipo di terreno nel punto dell'impatto
		int iGroundType=CCurLevel.GetGroundType(x+18);

		if (iGroundType==LV_LIQUID)
		{
			GrndImpact(x,y);
			GrndImpact(x-10,y);
			GrndImpact(x+10,y);
			GrndImpact(x+20,y);
		}
		else
		{			

			g_cs.PlaySound(g_snCrash,0,0);			
			//si schianta al suolo esplodendo
			//da fare: se cade in acqua attivare lo splash
			PushExpl3(x,y);
		}

		CreateBonus();
		return FALSE;
	}

	return (x>-260 && x<SCREEN_WIDTH+160 && y>-60);		
}

/////////////////////////////////////// classe CUfo3 ////////////////////////////////////////////


CUfo3::CUfo3(void)
{
	m_iAIAlgo=m_iNextAlgo=m_iNextFreq=0;
	m_lTgX=m_lTgY=0; //punto destinazione
	dwClass=CL_ENEMY;
	dwType=29;
}

void CUfo3::Reset(void)
{
	CEnemy::Reset();
	
	dwPower=8;
	m_iNextFreq=0;
	m_iW2=(int)(frames[0]->width*0.5f);

	if (m_dwFlags==1)
	{
		//decolla da terra
		SetVelocity(180,CSbLevel::g_scroll_speed);
		iUpdateFreq=0;
		SetPosition(x,440); //provvisorio: la y per decollo da terra va acquisita dalle caratteristiche del livello corrente
		m_iNextAlgo=11; //decollo da terra (algo speciale)
		SetAnimSpeed(0);

	}
	else
	{
		//parte in volo
		m_iNextAlgo=0;
		SetVelocity(180,CSbLevel::g_scroll_speed);			
		iUpdateFreq=40;
		this->SetAnimSpeed(0.3f);
	}

	m_lTgX=m_lTgY=0; //punto destinazione
	m_fTgSpin=0.2f;
	m_fSpinStep=0.0f;
	iStepPath=0;

	
}

void CUfo3::SetTgSpin(float fSpin)
{
	m_fTgSpin=fSpin;
	m_fSpinStep=(fSpin-(float)GetAnimSpeed())/80;
}

BOOL CUfo3::Update(void)
{
	iStepUpdate++;

	if (iStepUpdate>=iUpdateFreq)
	{
		int iangle;
		
		iStepUpdate=0;

		m_lTgX=m_lTgY=0;

		if (m_iNextAlgo>0) 
		{
			m_iAIAlgo=m_iNextAlgo;
			m_iNextAlgo=0;
		}

		else
		{			
			m_iAIAlgo=g_fmath.RndFast(14);
			if (m_iAIAlgo==11) m_iAIAlgo=4; //lacuni algoritmi sono utilizzati solo alla partenza
		}

		
		iUpdateFreq=40;

		switch (m_iAIAlgo)
		{
			
		case 1:				

			//posizione da raggiungere
			m_lTgX=60;
			m_lTgY=60;

			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			break;
		
		case 2:
		case 3:
			
			m_lTgX=g_fmath.RndFast(400);
			m_lTgY=g_fmath.RndFast(160);
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			SetTgSpin(-0.3f);
			m_iNextAlgo=100;
			break;
			
		case 4:

			//cerca di andare addosso al player
			m_lTgX=lTargetX;
			m_lTgY=lTargetY;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);

			if (g_fmath.RndFast(8)==1)
			{
				m_iNextAlgo=0;
			}
			else 
			{				
				iUpdateFreq=15;
				m_iNextAlgo=4; //continua a seguire il player
			}

			SetTgSpin(0.3f);
			break;

		case 5:

			m_lTgX=CSBObject::g_iScreenWidth-40;
			m_lTgY=CSBObject::g_iScreenHeight-200;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			SetTgSpin(0.2f);
			break;
		
		case 6:

			m_lTgX=-4;
			m_lTgY=160;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			SetTgSpin(-0.1f);
			m_iNextAlgo=100;
			break;

		case 7:

			m_lTgX=200;
			m_lTgY=2;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			m_iNextAlgo=100;
			break;

		case 8:

			//si sposta in alto e si prepara a bombardare
			m_lTgY=2;
			m_lTgX=x;
			SetVelocity(270,1);
			SetTgSpin(0.7f);
			m_iNextAlgo=9;
			break;

		case 9:

			//insegue il player in senso orizzontale
			m_lTgX=lTargetX;
			m_lTgY=y;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);			

			SetTgSpin(-0.3f);
						
			SetVelocity(iangle,2);
			if (g_fmath.RndFast(3)==1)
			{
				m_iNextAlgo=0; //la prossima volta cambia algoritmo
			}
			else m_iNextAlgo=9; //continua a seguire il player dall'alto
			break;
			
		case 10:
			//decollo da terra
			m_lTgX=x;
			m_lTgY=2;
			iangle=g_fmath.GetAngle(x,y,m_lTgX,m_lTgY);
			SetVelocity(iangle,2);
			SetTgSpin(0.4f);			
			break;

		case 11:

			//speciale usato per decollo da terra
			if (CSbLevel::g_scroll_speed>2)			
			{
				//se scrolla veloce lo fa alzare prima
				iUpdateFreq=190;
			}
			else
			{
				iUpdateFreq=360;
			}

			SetVelocity(180,CSbLevel::g_scroll_speed); //qui si deve mettere la velocit� dello schema
			SetAnimSpeed(0.0f);
			SetPosition(x,400); //posizionato a terra
			m_iNextAlgo=10; //poi decolla
			break;

		default:

			iUpdateFreq=100;
			SetVelocity(0,0); //sta fermo
			break;

		}		

	}


	if (m_lTgX>0 && m_iAIAlgo!=2)
	{
		if ((abs(x-m_lTgX)+abs(y-m_lTgY))<40)
		{
			iStepUpdate=iUpdateFreq; //ha raggiunto il punto quindi deve aggiornare l'algoritmo AI

		}
		else
		{
			iStepUpdate=0;
		}

	}

	SetAnimSpeed(anim_speed+m_fSpinStep);

	if (fabs(anim_speed-m_fTgSpin)<0.05) m_fSpinStep=0.0f;

	this->UpdatePosition();
	this->UpdateFrame(); //aggiorna l'animazione

	if (m_iAIAlgo != 11)
	{

		//routine di fuoco
		iStepFire++;

		if (iStepFire>=iFireFreq)
		{	
			BOOL bDownFire=FALSE;

			iStepFire=0;

			if (abs(x+m_iW2-lTargetX-15)<30 && lTargetY>y)
			{
				if (m_pWeapon1)
				{
					//se il nemico � sotto...
					CSBObject *pShell=NULL;
				
					pShell=m_pWeapon1->Pop();
					
					if (pShell)
					{					
						//spara un proiettile verso il basso
						pShell->dwClass=CL_ENFIRE;
						pShell->SetCurFrame(6);
						pShell->SetPosition(x+m_iW2,y+25);
						pShell->dwPower=6;
						pShell->SetInitialEnergy(1);
						pShell->SetVelocity(90,7);
						pShell->Reset();
						m_ActiveStack->Push(pShell);
						g_cs.PlaySound(g_snFire2,0,0);	
						bDownFire=TRUE;
					}

				}
			
			}
			
			if (m_Cartridges && !bDownFire)
			{
				int prob=g_fmath.RndFast(12);

				//se iStepPath>0 significa che sta sparando una raffica(la raffica termina quando iStepPath va a zero)			
				if (iStepPath>0 || prob==1)
				{
					if (iStepPath<=0)
					{
						iStepPath=5; //raffica di x colpi
						iFireFreq=5; //aumenta la frequenza di fuoco
					}

					else 
					{
						iStepPath--;
					}

					if (iStepPath<=0) iFireFreq=iStartFireFreq;

					CSBObject *pShell=NULL;
				
					pShell=m_Cartridges->Pop();
					
					if (pShell)
					{
						//spara in direzione del player
						int angle=g_fmath.GetAngle(x,y,lTargetX,lTargetY);
						pShell->dwClass=CL_ENFIRE;
						pShell->SetCurFrame(0);
						pShell->SetPosition(x+m_iW2,y+25);
						pShell->dwPower=6;
						pShell->SetInitialEnergy(1);
						pShell->SetVelocity(angle,7);
						pShell->Reset();
						m_ActiveStack->Push(pShell);
						g_cs.PlaySound(g_snFire7,0,0);		
					}				
				}
				
				else if (prob==2 && m_pWeapon2)
				{
					prob=g_fmath.RndFast(10);

					if (prob==1)
					{
						//weapon 2 (flash bomb)				

						CEnemy *pen=(CEnemy *)m_pWeapon2->Pop();									

						if (pen)
						{
							pen->Reset();
							pen->dwClass=CL_ENEMY;				
							pen->SetPosition(x+10,y+20);
							pen->SetEnergy(6);
							m_ActiveStack->Push((CSBObject *)pen);
							g_cs.PlaySound(g_snFire7,0,0);	
						}

					}
				}

			}

		} //fine if (iStepFire>=iFireFreq)

	} //fine AILgo !=11
	

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	if (x<-120 || x>CSBObject::g_iScreenWidth+280 || y<-100 || y>CSBObject::g_iScreenHeight) return FALSE;	
	else return TRUE;
}

/////////////////////////////////////// classe CScrewFighter ////////////////////////////////////
/*
//----------------------------------- CScrewFighter --------------------------------------------------
*/



CScrewFighter::CScrewFighter(void)
{
	dwType=25;
	dwClass=CL_ENEMY;
	iBaseFrame=0;
	m_iStepDir=0;
	m_iChgDir=0;
}

void CScrewFighter::Reset()
{
	CEnemy::Reset();
	switch (m_dwFlags)
	{
	case 1: //veloce a dritto

		SetVelocity(180,4);	
		this->SetAnimSpeed(-0.2f);
		iFireFreq=35;
		m_iAnimStartFrame=0;
		m_iAnimEndFrame=this->GetMaxFramesIndex();
		break;

	case 2:
		//insegue il player

		SetVelocity(180,3);
		this->SetAnimSpeed(0);
		iFireFreq=40;
		m_iAnimStartFrame=3;
		m_iAnimEndFrame=9;
		iConfigFreq=12;
		break;
	
    case 3:
		
		//insegue il player (versione piu' aggressiva)
		SetVelocity(180,4);
		this->SetAnimSpeed(0);
		iFireFreq=35;
		m_iAnimStartFrame=2;
		m_iAnimEndFrame=10;
		iConfigFreq=12;
		break;

	
	default:
	
		m_iAnimStartFrame=0;
		m_iAnimEndFrame=this->GetMaxFramesIndex();
		iFireFreq=55;
		SetVelocity(180,2);	
		this->SetAnimSpeed(0.2f);
		break;
	}

	iUpdateFreq=40;
	iStepConfig=0;
	m_iStepDir=0;
	m_iChgDir=(g_fmath.RndFast(10)-5);
	m_iChgDir += 2*g_fmath.Sgn(iConfigFreq);
	SetCurFrame(iBaseFrame);
	
}


BOOL CScrewFighter::Update(void)
{	
	iStepUpdate++;

	if (iStepUpdate>iUpdateFreq)
	{
		LONG lx,ly;
		
		int angle=this->GetAngle();

		if (!(m_dwFlags==2 || m_dwFlags==3))
		{
			//nota: prima di calcolare la nuova velocit� deve spostare la posizione in base all'hotspot della 
			//frame per evitare "scatti" di posizione
			this->GetFrameHotSpot(cur_frame,0,&lx,&ly);

			x+=lx;
			y+=ly;
		}

		iStepUpdate=0;

		switch (m_dwFlags)
		{

		case 0:

			angle+=m_iChgDir;

			if (angle<150 || angle>210) iConfigFreq=-iConfigFreq; //cambia direzione

			if (y<20) 
			{
				m_iChgDir=-5;
				angle=170;
			}
			else if (y>CSBObject::g_iScreenHeight-180)
			{
				m_iChgDir=5;
				angle=190;
			}

			SetVelocity(angle,2);
			break;

		case 2:
        case 3:

			if (x<CSBObject::g_iScreenWidth*0.8)
			{

				//varia l'assetto quando cambia direzione
				if (lTargetY<y-20)
				{
					angle+=10;

				}
				else if (lTargetY>y+20)
				{
					angle-=10;
				}
				else
				{


				}

				//limita la direzione
				if (angle<150) angle=150;
				else if (angle>210) angle=210;

				SetVelocity(angle,GetSpeed());
			
			}

			
		case 1:
			
					
			//va dritto senza cambiare direzione

			break;			

		}

	}

	this->UpdatePosition();
	
	if (m_dwFlags==0 || m_dwFlags==1)
	{
		//ruota (si avvita)
		this->UpdateFrame();
		this->SetJointPosition(x,y,0); //usa l'asse di rotazione come riferimento 

	}

	else 
	{
    //   	this->SetJointPosition(x,y,0); //usa l'asse di rotazione come riferimento 
		this->UpdateTrim(); //aggiorna l'assetto
	}

	//fuoco
	iStepFire++;

	if (iStepFire>=iFireFreq)
	{
		LONG lx,ly;

		this->GetJointPosition(&lx,&ly,0);

		//controlla se il player � davanti 
		if (abs((lTargetY-y))<50)
		{

			iStepFire=0;

			if (m_Cartridges)
			{
				
				CShell *pShell=(CShell*)m_Cartridges->Pop();

				pShell->SetInitialEnergy(1);
				pShell->Reset(); //resetta l'oggetto
				pShell->dwClass=CL_ENFIRE;						
				pShell->dwPower=18;
				pShell->SetVelocity(180,10);			
				pShell->SetCurFrame(12);
				//posiziona il proiettile sull'asse
				pShell->SetPosition(lx,ly);
				m_ActiveStack->Push(pShell); //inserisce il proiettile nello stack degli oggetti attivi
				g_cs.PlaySound(g_snFire11,0,0);

			}

		}

	}

	if (dwEnergy<0) CreateBonus();

	return (dwEnergy>0 && x>-100 && x<CSBObject::g_iScreenWidth+200 && y>-80 && y<CSBObject::g_iScreenHeight);
	
}

/////////////////////////////////////// classe CAntiAircraft /////////////////////////////////////

CAntiAircraft::CAntiAircraft(void)
{
	dwScore=800;
	dwType=8; //id del tipo dell'oggetto 
	this->Reset();
	lCurx=0;
	iFreq1=0;
}

void CAntiAircraft::Reset(void)
{
	dwClass=CL_ENEMY;
	CEnemy::Reset();
	lCurx=0;

}

BOOL CAntiAircraft::Update(void)
{
	CShell *pShell=NULL;

	iStepUpdate++;    

	if (dwEnergy<=0)
	{
		dwClass=CL_NONE;
		cur_frame=3;
		CreateBonus();		
	}

	else
	{

		if (iStepUpdate==iUpdateFreq)
		{
			//aggiorna l'inclinazione del cannone
			if (lTargetX<x-80)
			{
				if (cur_frame>0) cur_frame--;
			}

			else if (lTargetX>x+80)
			{
				if (cur_frame<2) cur_frame++;
			}
			
			iStepUpdate=0;
		}	

		iStepFire++;

		if (iStepFire==iFireFreq)
		{
			iFreq1--;
			
			//il seguente codice serve per sparare una serie di proiettili
			if (iFreq1>0)
			{
				iStepFire=iFireFreq-10;
			}
			
			else 
			{
				iStepFire=0;
				iFreq1=4;
			}

			//spara nella direzione di puntamento corrente
			pShell=(CShell *) m_Cartridges->Pop();  		            		

			//estrae un proiettile dalla collezione
			if (pShell)
			{
				pShell->dwClass=CL_ENFIRE;
				pShell->SetInitialEnergy(1);
				pShell->Reset();
			
				switch (cur_frame)
				{
				case 0:

					pShell->SetCurFrame(1);
					pShell->SetPosition(x,y+23);
					pShell->SetVelocity(195,10);					

					break;

				case 1:

					pShell->SetCurFrame(2);
					pShell->SetPosition(x+16,y+9);					
					pShell->SetVelocity(210,10);

					break;

				case 2:
					
					pShell->SetVelocity(225,10);
					pShell->SetCurFrame(3);
					pShell->SetPosition(x+39,y);

					break;
				}

				m_ActiveStack->Push(pShell);
				g_cs.PlaySound(g_snFire6,0,0);
			}		

		}
    	
	}

	if (lCurx>0)
	{
		x-=lGroundX-lCurx; //moto di trascinamento dovuto allo scrolling
	}

	lCurx=lGroundX;

	return (x>-80);	

}

/////////////////////////////////////////////// Classe CGroundObject //////////////////////////////////

CGroundObject::CGroundObject(void)
{
	CEnemy::Reset();
	memset(m_idestlevel,0,sizeof(int)*MAX_DLV);
	memset(m_iframe,0,sizeof(int)*MAX_DLV);
	m_icurlevel=0;
	dwType=11;
	m_bDontRemove=FALSE;
	
}

void CGroundObject::Reset(void)
{
	CEnemy::Reset();
	dwClass=CL_ENEMY;
	lCurX=0;
	iWoundedState=0;
	cur_frame=0;
	m_lW=100;
}


BOOL CGroundObject::Update(void)
{	
	iStepUpdate++;
	//BOOL b;

	if (lCurX>0 && !pParent)
	{
		x-=lGroundX-lCurX; //moto di trascinamento dovuto allo scrolling
	}

	lCurX=lGroundX; //lGroundX � la x del terreno corrente impostata da RefreshObjects

	if (iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;
	    //aggiorna lo stato di danneggiamento
		UpdateWoundedState();

		//aggiorna la posizione in base alle componenti della velocit�
		x+=iVelx;
		y+=iVely;
	}

    m_lW=frames[cur_frame]->width;
	
	if (pParent)
	{
		CEnemy *pen=(CEnemy *)pParent;
		//se � vincolato ad un oggetto genitore , se l'oggetto genitore non � piu' attivo disattiva anche l'oggetto corrente
		if (!pParent->bActive || pParent->GetEnergy()<=0) 
		{
			dwEnergy=0;
			return FALSE;
		}

		UpdateAbsXY();		
		if (pen->GetWoundedState()>0) 
		{
			if (lpfnExplode && !pParent) lpfnExplode(xabs,yabs);
			return FALSE;
		}
		return (dwEnergy>0 && x>-m_lW && xabs<SCREEN_WIDTH+300);		

	}	

	else 
	{
		if (dwEnergy<=0)
		{
			CreateBonus();

			if (m_bDontRemove)
			{
               dwPower=0; //in queste non danneggia piu' il player se collide
			   return (x>-m_lW && x<SCREEN_WIDTH+300); //in questo caso lo rimuove solo quando esce dall'area visibile
			}
			else return FALSE; //l'oggetto puo' essere rimoso dallo stack
		}
		
		else return (x>-m_lW && x<SCREEN_WIDTH+300);
	}

}
//////////////////////////////////////////////// Classe CAntiAircraft2 ////////////////////////////////
//contraerea formata da una torretta piu' un cannone
//� vincolabile ad un altro oggetto

CAntiAircraft2::CAntiAircraft2(void)
{
	dwType=18;
	
	m_iFrmCannonCnt=0; //contatore frames (numero di frame del cannone)
	m_iBaseCannonFrame=0; //frame base del cannone
	m_iCurCannonAngle=0;
	m_iAngTolerance=0;
	m_pfm=NULL;
	m_iShots=3;
	m_iFreq=6;
	m_iJntX=10;
	m_iJntY=25;
	m_pSmoke=NULL;
	m_bDontDrawBottom=FALSE;
	SetVelocity(0,0);
}

void CAntiAircraft2::Reset(void)
{
	CGroundObject::Reset();
	//inizialmente � rivolto in alto
	m_sprCannon.SetCurFrame(m_iBaseCannonFrame);
	m_iCurCannonAngle=270;
	m_iFrmCannonCnt=m_sprCannon.GetMaxFramesIndex()+1; //numero di frame del cannone
	m_iShotsCnt=0;
	m_iFreqCnt=0;
	SetCurFrame(0);
	m_bDontDrawBottom=FALSE;
}

//indica se deve disegnare solo il cannone.
//Se impostato su true, disegna solo il cannone
void CAntiAircraft2::DontDrawBottom(BOOL bval)
{
	m_bDontDrawBottom=bval;
}

HRESULT CAntiAircraft2::SetSmokeStack(CObjectStack *pStack)
{
	if (pStack)
	{
		m_pSmoke=pStack;
		return S_OK;

	}
	else return E_FAIL;
}


void CAntiAircraft2::SetRotJoint(int jx,int jy)
{
	m_iJntX=jx;
	m_iJntY=jy;
}


void CAntiAircraft2::SetBaseCannonFrame(int iframe)
{
	if (iframe>=0 && iframe<=m_sprCannon.GetMaxFramesIndex())
	{
		m_iBaseCannonFrame=iframe;
	}
}

void CAntiAircraft2::SetToleranceAngle(int iTol)
{
	iTol=abs(iTol);
	if (iTol<45) m_iAngTolerance=iTol;
}


//imposta il tipo di raffica
void CAntiAircraft2::SetBurst(int iShots,int iFreq)
{
	if (m_iShots>=0 && iFreq>1)
	{
	   m_iShots=iShots;
       m_iFreq=iFreq;
	}
}

BOOL CAntiAircraft2::Update(void)
{
	BOOL bres;
	LONG cf;
	
	bres=CGroundObject::Update();

	//aggiorna le coordinate assolute
	GetAbsXY(&absx,&absy);

	if (iStepUpdate==0 && m_pfm)
	{
		if (GetWoundedState()==0)
		{
			//sposta il cannone
			int ang;      

			//determina l'angolo del vettore che congiunge l'oggetto al player
			ang=m_pfm->GetAngle(absx,absy,lTargetX+30,lTargetY+12);

			if (ang<m_iCurCannonAngle-m_iAngTolerance)
			{
				if (!FAILED(m_sprCannon.IncFrame())) m_iCurCannonAngle-=15; 		
			}

			else if (ang>m_iCurCannonAngle+m_iAngTolerance)
			{
				if (!FAILED(m_sprCannon.DecFrame())) m_iCurCannonAngle+=15;
			}

		}
	}

	iStepFire++;

	if (iStepFire>=iFireFreq)
	{
		if (GetWoundedState()==0)
		{

			m_iFreqCnt++;

			if (m_iFreqCnt>=m_iFreq)
			{
				m_iFreqCnt=0;

				m_iShotsCnt++;		

				if (m_iShotsCnt>=m_iShots)
				{
				   //fine raffica
				   iStepFire=0;
				   m_iShotsCnt=0;
				}	

				CShell *ps=NULL;			

				if (m_pWeapon1)
				{

					//frame corrente del cannone
					cf=m_sprCannon.GetCurFrame();
					//acquisisce l'hot spot 1 (bocca della canna)
					m_sprCannon.GetJointPosition(&lx,&ly,1);
					ps=(CShell *)m_pWeapon1->Pop();			
				
					//immette il proiettile nello stack
					if (ps)
					{
						ps->dwClass=CL_ENFIRE;
						ps->SetInitialEnergy(1);
						ps->Reset();
						//imposta la posizione del proiettile in uscita
						ps->SetJointPosition(lx,ly,0);
						ps->SetVelocity(m_iCurCannonAngle,8);
						ps->SetCurFrame(cf);
						m_ActiveStack->Push(ps);
						g_cs.PlaySound(g_snFire7,0,0);
					}

					//imette il fumo nello stack
					if (m_pSmoke)
					{
					   //immette una nuvoletta di fumo dopo aver sparato
					   CDebris *pd=(CDebris *)m_pSmoke->Pop();
					   if (pd)
					   {
						   //coordinate assolute della bocca del cannone					  
						   pd->Reset();
						   pd->SetG(-0.3f);
						   pd->SetPosition(lx-7,ly-7);
						   pd->SetMaxFrames(4); //termina alla frame 4
						   pd->SetVelocity(m_iCurCannonAngle,6);					  
						   m_ActiveStack->Push(pd);    				  			   

					   }
				   }
				}

			}

		}

		//spara						
	}

    return bres; 
}


HRESULT CAntiAircraft2::Draw(void)
{
	HRESULT hr;
	//controlla se deve disegnare anche la parte di sotto
	if (!m_bDontDrawBottom) hr=CGroundObject::Draw();

	else hr=S_OK;

	int xa,ya;

	this->GetAbsXY(&xa,&ya);
	
	if (!FAILED(hr) && m_iFrmCannonCnt>0 && GetWoundedState()==0)
	{		
		//disegna il cannone
		m_sprCannon.SetDisplay(m_pimgOut);
	
		//renderizza il cannone in modo che l'asse di rotazione della canna sia in x+10,y+25
		
		m_sprCannon.SetJointPosition(absx+m_iJntX,absy+m_iJntY,0);
		
		m_sprCannon.RenderToDisplay();
	}

	return hr;		
}

///////////////////////////////////////////// Classe CTank ////////////////////////////////////////////
/*
class CTank:public CGroundObject
{
private:

	int m_iRotAnim;
	
public:
	
	CADXSprite m_sprTrackCrawler; //parte inferiore (cingoli)
	CTank(void);
	HRESULT SetAAStack(CObjectStack *pStack);
	HRESULT Draw(void);
	void Reset(void);
	void SetFlags(void);
};
*/

CTank::CTank(void)
{
	dwType=22;
	dwClass=CL_ENEMY;
	m_pAAStack=NULL;
	m_iExtentLeft=1500;
	m_iExtentRight=1500;
	m_iInit=0;
	m_paa=NULL;
}

void CTank::Reset(void)
{
	CGroundObject::Reset();
	m_iRotAnim=1;
	m_iInit=0;
	iStepConfig=0;

	if (m_dwFlags>=1) m_iAvgSpeed=3; //piu' veloce
	else m_iAvgSpeed=2;

	SetVelocity(180,m_iAvgSpeed); 
	m_imaxc=m_sprTrackCrawler.GetMaxFramesIndex();
	
	//imposta la torretta con il cannone
	if (m_pAAStack)
	{		
		//collega la torrette al carro
		m_paa=(CAntiAircraft2 *)m_pAAStack->Pop();

		if (m_paa)
		{				
			m_paa->SetInitialEnergy(12);	
			//m_paa->AddWoundedState(2,1);
			m_paa->Reset();
			m_paa->pParent=(CSBObject *)this; //oggetto parente (la torretta si muove rispetto a questo)
			m_paa->SetPosition(20,4); //coordinte relative
			m_paa->SetVelocity(0,0);			
			m_paa->lpfnExplode=this->lpfnExplode;
			m_paa->DontDrawBottom(TRUE); //disegna solo il cannone
			m_ActiveStack->Push(m_paa);		
		}	

	}
}

//definisce l'area all'interno della quale si puo' muovere
//left=massima distanza verso sinistra dal punto di immissione
//right=massima distanza verso destra

void CTank::SetExtents(int left,int right)
{
	if (left>=0) m_iExtentLeft=left;
	if (right>=0) m_iExtentRight=right;


}

BOOL CTank::Update(void)
{
	BOOL bres;
	int icmp,icfc;
	bres=CGroundObject::Update();
	

	//lCurX=coordinata assoluta rispetto al terreno
	if (!m_iInit && lCurX)
	{
		//salva le coordinate assolute del punto di immissione in modo da clacolare l'area di movimento
		m_iInit=1;
		m_ixr=lCurX+m_iExtentRight;
		m_ixl=lCurX-m_iExtentLeft;
	}


	//acquisisce le coordinate assolute
	GetAbsXY(&absx,&absy);
	
	icmp=lTargetX-absx;	

	iStepConfig++;

	if (iStepConfig==8)
	{

		iStepConfig=0;

		if (icmp<-50) 
		{
			if (lCurX+x<=m_ixl) 
			{
				SetVelocity(0,0); //non puo' andare oltre : si ferma
				m_iRotAnim=0;
			}
			else 
			{
				SetVelocity(180,m_iAvgSpeed); //insegue il player a sinistra
				m_iRotAnim=-1; //incremento rotazione frame dei cingoli
			}
		}
		else if (icmp>50)
		{
			if (lCurX+x>=m_ixr) 
			{
				m_iRotAnim=0;
				SetVelocity(0,0); //non puo' andare oltre : si ferma
			}
			else 
			{
				m_iRotAnim=1;
				SetVelocity(0,m_iAvgSpeed); //insegue il player verso destra
			}

			
		}
		else 
		{
			m_iRotAnim=0;
			SetVelocity(0,0); //resta fermo
		}	

		icfc=m_sprTrackCrawler.GetCurFrame();

		icfc += m_iRotAnim;

		//nota: l'ultima frame del cingolo � il cingolo danneggiato
		if (icfc>m_imaxc-1) m_sprTrackCrawler.SetCurFrame(0);
		else if (icfc<0) m_sprTrackCrawler.SetCurFrame(m_imaxc-1);
		else m_sprTrackCrawler.SetCurFrame(icfc);	
	} 

	if (dwEnergy<=0) 
	{
		CreateBonus();
		bres=FALSE;
	}

	return bres;
}

HRESULT CTank::Draw(void)
{
	HRESULT hr;
	int xa,ya;

	hr=CGroundObject::Draw();

	this->GetAbsXY(&xa,&ya);
    
	if (!FAILED(hr))
	{
		//disegna il cingolo
		//imposta la superficie di output corrente
		m_sprTrackCrawler.SetDisplay(m_pimgOut);
		m_sprTrackCrawler.SetPosition(xa-5,ya+18);
		m_sprTrackCrawler.RenderToDisplay();

	}

	return hr;
}


HRESULT CTank::SetAAStack(CObjectStack *pStack)
{
	if (pStack)
	{
		m_pAAStack=pStack;
		return S_OK;
	}

	else return E_FAIL;
}

/////////////////////////////////////////////// Classe CShip //////////////////////////////////////////

CShip::CShip(void)
{
	dwType=14;
	CGroundObject::Reset();
	iWreck=0;
	iWakeCnt=0;
	iAANum=0;
	iWakeNum=0;	
	dwClass=CL_ENEMY;	
	dwScore=2500;
	pimgWake=NULL;
	m_paa=NULL;
	m_pAAStack=NULL;
	m_dwFlags=0;
}

void CShip::Reset(void)
{
	CGroundObject::Reset();
	iWreck=0;
	iWakeCnt=0;
	dwClass=CL_ENEMY;
	anim_clock=0.0f;
	int count;
	
    iAANum += m_dwFlags;

	if (m_pAAStack)
	{
		for (count=0;count<iAANum;count++)
		{
			//collega le torrette alla nave	
			m_paa=(CAntiAircraft2 *)m_pAAStack->Pop();

			if (m_paa)
			{		
				m_paa->SetInitialEnergy(15);
				m_paa->Reset();
				m_paa->pParent=(CSBObject *)this;
				m_paa->SetPosition(80+80*count,15);
				m_paa->SetVelocity(0,0);				
				m_ActiveStack->Push(m_paa);		
			}
		
		}

	}

}

//specifica il numero di torrette contraerre extra
void CShip::SetFlags(DWORD dwFlags)
{
	if (dwFlags<3) m_dwFlags=dwFlags;
}

//imposta il numero di torrette contraeree
void CShip::SetNumAA(int inum)
{
	if (inum>=0)
	{
		iAANum=inum;
	}
}

HRESULT CShip::SetAAStack(CObjectStack *pStack)
{
	if (!pStack) return E_FAIL;

	m_pAAStack=pStack;

	return S_OK;
}

HRESULT CShip::SetWakeStack(IMAGE_FRAME_PTR pWake,int inum)
{
	if (!pWake) return E_FAIL;
	
	pimgWake=pWake;
	iWakeNum=inum; //numero di elementi nel vettore della scia
	iWakeStepUpdate=0;
	return S_OK;
}

void CShip::Explode(void)
{
	dwEnergy=-1;
	UpdateWoundedState();
};

HRESULT CShip::Draw(void)
{
	static IMAGE_FRAME_PTR pimg;
	static idel=0;
	int icount,h;
	int ifr;
	HRESULT hr;	

	pimg=frames[cur_frame];

	if (iWoundedState>0)
	{
		if (++idel>=2)
		{
			//rallenta l'affondamento
			idel=0;
			iWreck++;
		}
		//a questo punto l'oggetto non � piu' colpibile
		dwClass=CL_NONE;

		//fa in modo da lasciare la scia anche quando la nave � affondata da poco
		//quando iWreck raggiunge il limite dwEnergy viene settato su zero in modo da
		//far eliminare l'ogetto dallo stack
		if (iWreck>=140) dwEnergy=0;
	
		if (pimg)
		{
			//la nave sta affondando
			RECT rcSrc;

			rcSrc.left=0;
			rcSrc.top=0;
			rcSrc.bottom=pimg->height-iWreck;
			rcSrc.right=pimg->width;						
		
			hr=m_lpFm->PutImgFrameRect(x,y+iWreck,m_pimgOut,pimg,&rcSrc);
		}

		else return E_FAIL;		

	}
	else
	{
		//disegna l'oggetto normalmente
		CGroundObject::Draw();		
	}	

	h=pimg->height;

	iWakeStepUpdate++; //contatore aggiornamento scia
	if (iWakeStepUpdate==5)	
	{
		iWakeCnt++;
		iWakeStepUpdate=0;
	}


	if (iWakeCnt>iWakeNum) iWakeCnt=0;
	//disegno della scia
	for (icount=20;icount>=0;icount--)
	{
		ifr=(icount+iWakeCnt) % iWakeNum;	

		m_lpFm->PutImgFrameRect(x+70+icount*22,y+h-10,m_pimgOut,&pimgWake[ifr],&pimgWake[ifr].rcClip);
	}

	return hr;
}


BOOL CShip::Update(void)
{
	if (CGroundObject::Update())
	{
		iStepWeapon1++;

		if (iStepWeapon1==iFireFreq)
		{
			if (m_pWeapon1 && !iWoundedState)
			{
				CSBObject *pobj;

				pobj=m_pWeapon1->Pop();

				if (pobj)
				{
					pobj->Reset();
					pobj->SetPosition(x+35,y+29);
					m_ActiveStack->Push(pobj);	
					g_cs.PlaySound(g_snFwMissile,0,0);
				}
			}
		
			iStepWeapon1=0;
		}

		if (iWoundedState>0)
		{
			g_cs.PlaySound(g_snSunk,0,0);
		}

		return TRUE;
	}

	else return FALSE;

}

////////////////////////////////////////////// Classe CRadar //////////////////////////////////////////
//radar

CRadar::CRadar(void)
{
	dwScore=150;
	dwType=7; //id del tipo dell'oggetto 
	this->SetCurFrame(0);//frame iniziale	
	lCurx=0;
}



void CRadar::Reset(void)
{
	CEnemy::Reset();
	CGroundObject::Reset();
	dwClass=CL_ENEMY;
	cur_frame=0;
	lCurx=0;
	SetVelocity(0,0);
}

BOOL CRadar::Update(void)
{

	if (cur_frame==4)
	{
		g_cs.PlaySound(g_snRadar,0,0);
	}

	return CGroundObject::Update();
}

//-------------------------------- CSnake ------------------------------------------------

CSnake::CSnake(void)
{
	DWORD dwCount;

	dwScore=2000;
	pBodyStack=NULL;
	bInit=FALSE;
	m_iNumElems=0;
	m_iUpdateFrameFreq=3; //freq. di aggirnamento frame
	m_iFrameStep=0;
	dwType=9;
	dwClass=CL_ENEMY;
	m_dwFlags=0;

	for (dwCount=0;dwCount<10;dwCount++) m_objBody[dwCount]=NULL;
}

CSnake::~CSnake(void)
{
	bInit=FALSE;
	m_iNumElems=0;
}

BOOL CSnake::DoCollisionWith(CSBObject *pobj)
{
	LONG hx,hy,tx,ty;

	//prende l'hot spot della frame corrente
	//gets the current frame hot spot
	this->GetFrameHotSpot(cur_frame,0,&hx,&hy);
		
	tx=x;
	ty=y;

	//esegue una tralsazione prima di fare il controllo collisione
	x-=hx;
	y-=hy;

	BOOL bres=CSBObject::DoCollisionWith(pobj);

	x=tx;
	y=ty;

	return bres;
	
};

void CSnake::Explode(void)
{
	//fa esplodere la testa
	if (this->lpfnExplode) lpfnExplode(this->x,this->y);

	//fa esplodere ogni elemento del corpo
	CEnemy *pen;		
	//il serpente non ha piu' energia quandi si devono far esplodere anche gli elementi del corpo
	for (DWORD dwCount=0;dwCount<m_iNumElems;dwCount++)
	{
			pen=(CEnemy *)m_objBody[dwCount];
			if (pen->lpfnExplode) pen->lpfnExplode(m_objBody[dwCount]->x,m_objBody[dwCount]->y);
	}

}

void CSnake::SetFlags(DWORD dwFlags)
{
     m_dwFlags=dwFlags;
}

BOOL CSnake::Update(void)
{
	DWORD dwCount;	
	const DWORD dwDist=15;
	LONG px,py;
	CSBObject *pShell;
	LONG hx,hy;	
	
	if (bInit==FALSE)
	{
		//inizializza il serpente
		if (pBodyStack)
		{
			//crea gli elementi del corpo del serpente
			CSBObject *pobj=NULL;			
			CSnakeBody *pen;
			//numero di elementi del corpo (testa esclusa)
			m_iNumElems=7+m_dwFlags;

			this->SetVelocity(pPath->Angle,pPath->Velocity);
			
			//popola il corpo del serpente
			for (dwCount=0;dwCount<m_iNumElems;dwCount++)
			{
				//estrae un elemento dallo stack del corpo
				pobj=pBodyStack->Pop();

				if (pobj)
				{
					//ogni elemento del corpo � un oggetto che viene inserito nello stack					
					
					//controllare le seguenti istruzioni su XP
					pen=(CSnakeBody *)pobj;				
					pobj->SetInitialEnergy(15);
					pen->Reset();				
					pobj->dwPower=2;
					pobj->SetAnimSpeed(0.5);				
					pobj->SetCurFrame(0);
					//primo el. dopo la tasta
					if (dwCount==0) 
					{					
						pobj->SetPosition(x+(dwDist+3)*speed*(dwCount+1),y);			
					}
					//altri elementi
					else pobj->SetPosition(x+dwDist*speed*(dwCount+1),y);					

					pobj->SetVelocity(180,speed); //imposta a tutti la stessa velocit�
					//pobj->pParent=this; //imposta la testa come genitore di ogni elemento del corpo
					pobj->pParent=NULL;
					pen->m_pHead=this;
					pobj->dwStatus=1;					
					pen->SetPath(this->pPath,iStepPathNum);
					pen->SetUpdateFreq(iStartUpdateFreq);
					pen->iStartFireFreq=0;
					pen->m_iFrameStep=0;					
					pen->m_ixBound=x;						
					//effetto rotazione (la frame di partenza � diversa per ogni elemento)
					pen->SetCurFrame(0);
					//NOTA BENE: le seguenti due impostazioni
					//fanno in modo che gli elementi della coda seguano la testa
					//in modo corretto, cio� segueno la testa come se fossero su un binario
					if (dwCount==0) pen->SetUpdateFreq(5+(dwDist+3)*(dwCount+1));
					else pen->SetUpdateFreq(5+dwDist*(dwCount+1));
                    pen->SetVelocity(180,3);
					pen->bFollowPath=FALSE;
					m_ActiveStack->Push(pobj);//inserisce l'elemento del corpo nello stack degli oggetti	
					m_objBody[dwCount]=pobj; //salva il riferimento all'elemento appena inserito nello stack attivo
				}
			}

			bInit=TRUE;//indica che il corpo � stato creato
		}
	}

	iStepUpdate++;

	if (iStepUpdate == iUpdateFreq)
	{
		//esegue un cambio di traiettoria
		iStepUpdate=0;	
		//segue un path
		this->SetVelocity(pPath->Angle,pPath->Velocity);
		this->SetCurFrame(GetAngle()/10);
		iUpdateFreq=pPath->Displ;
		pPath++;
		iStepPath++;
		
	}	

	this->UpdatePosition();

	if (iStepPath>=iStepPathNum)
	{
		return FALSE;//fine path elimina il serpente 
	}

	else if(dwEnergy<=0)
	{
		this->Explode();

		CreateBonus();

		return FALSE;
	}

	iStepFire++;
	//Controlla il contatore del fuoco
	if (iStepFire == iFireFreq)
	{
	
		//prende l'hot spot della frame corrente
		this->GetFrameHotSpot(cur_frame,0,&hx,&hy);

		//estrae un proiettile dall collezione di proiettili
		//SPARA!!
		pShell=m_Cartridges->Pop();

		if (pShell)
		{			
				
			//acquisisce la poszione del primo cannone
			GetFrameHotSpot(cur_frame,1,&px,&py);
			//trasla
			px = x-hx+px;
			py = y-hy+py;		
			//imposta posizione iniziale e velocit� proiettile
			pShell->SetInitialEnergy(1);
			pShell->Reset();
			pShell->SetPosition(px,py);
			pShell->SetVelocity(angle_dir,8);
			pShell->dwClass=CL_ENFIRE;
			
			//immette il projettile nello stack attivo
			m_ActiveStack->Push(pShell);

			g_cs.PlaySound(g_snFire2,0,0);

			pShell=NULL;
			//secondo proiettile
			pShell=m_Cartridges->Pop();

			if (pShell)
			{
				//acquisisce la poszione del secondo cannone
				GetFrameHotSpot(cur_frame,2,&px,&py);
				//trasla
				px = x-hx+px;
				py = y-hy+py;
				//imposta le condizioni iniziali del proiettile
				pShell->SetPosition(px,py);				
				pShell->SetVelocity(angle_dir,8);
				pShell->dwClass=CL_ENFIRE;
				pShell->SetInitialEnergy(1);
				pShell->Reset();
				//imette il proiettile nello stack degli oggetti
				m_ActiveStack->Push(pShell);
				pShell=NULL;

			}
		}
		
		iStepFire=0;
	}
	
    
	return TRUE;
}

//------------------------------------ CSnake::Draw --------------------------------------------------

//disegna la testa e tutti gli elementi del corpo
HRESULT CSnake::Draw(void)
{
	LONG hx=0,hy=0; //hot spot	
	static LPDIRECTDRAWSURFACE7 lpDDS;	

	//prende l'hot spot della frame corrente
	//gets the current frame hot spot
	this->GetFrameHotSpot(cur_frame,0,&hx,&hy);
		

	//esegue una traslazione delle coordinate di output, in questo modo l'hot spot verr� posizionato esattamente in x,y,	
	//coords transformation in order to put the hot spot int x,y
	hx = x-hx;
	hy = y-hy;

	pimg=frames[cur_frame];

	if (pimg)
	{	
		rcBoundBox.left=hx;
		rcBoundBox.top=hy;
		rcBoundBox.right=hx+pimg->width;
		rcBoundBox.bottom=hy+pimg->height;
		lpDDS=m_pimgOut->surface;			
		//disegna la testa del serpente
		//draw the head of the snake
		return lpDDS->Blt(&rcBoundBox,pimg->surface,NULL,DDBLT_KEYSRC,NULL);
	}

	else return E_FAIL;	

}

//----------------------------------- CSnake::Reset -------------------------------------

void CSnake::Reset(void)
{
	CEnemy::Reset();
	dwClass=CL_ENEMY;
	bInit=FALSE;	
	rcBoundBox.left=-1; //non iniz.
}

//------------------------------------ CSnakeBody ---------------------------------------------
/*
class CSnakeBody:public CEnemy
{
public:

	m_ixBound; //ascissa in corrispondenza della quale inizia a seguire il path
	CSnakeBody(void);
	~CSnakeBody(void);
	BOOL Update(); 
	HRESULT Draw();
}
*/

CSnakeBody::CSnakeBody(void)
{
	m_pHead=NULL;
	dwScore=100;
	m_ixBound=0;
	bFollowPath=FALSE;
	m_iUpdateFrameFreq=3; //freq. di aggirnamento frame
	m_iFrameStep=0;
	dwType=10;
	dwClass=CL_ENEMY;
}

CSnakeBody::~CSnakeBody(void)
{
	m_ixBound=0;
}

CSnakeBody::Update(void)
{

    iStepUpdate++;

	if (iStepUpdate == iUpdateFreq)
	{

		iStepUpdate=0;
		this->SetVelocity(pPath->Angle,pPath->Velocity);
		iUpdateFreq=pPath->Displ;
		//segue il path
		pPath++;
		iStepPath++;

		this->SetCurFrame(GetAngle() /10);
	}	

	this->UpdatePosition();

	return (m_pHead->bActive);	
}

//------------------------------------------- CSnakeBody::Draw --------------------------------

HRESULT CSnakeBody::Draw(void)
{
	LONG hx=0,hy=0; //hot spot
	static IMAGE_FRAME_PTR pimg;
	static LPDIRECTDRAWSURFACE7 lpDDS;	

	//prende l'hot spot della frame corrente
	this->GetFrameHotSpot(cur_frame,0,&hx,&hy);		
	
	//esegue una trasalzione delle coordinate di output, in questo modo l'hot spot verr� posizionato esattamente in x,y,	
	hx = x-hx;
	hy = y-hy;

	pimg=frames[cur_frame];

	if (pimg)
	{	
		rcBoundBox.left=hx;
		rcBoundBox.top=hy;
		rcBoundBox.right=hx+pimg->width;
		rcBoundBox.bottom=hy+pimg->height;
		lpDDS=m_pimgOut->surface;			
		//disegna la testa del serpente
		return lpDDS->Blt(&rcBoundBox,pimg->surface,NULL,DDBLT_KEYSRC,NULL);
	}

	else return E_FAIL;	
	
	return S_OK;
}

/*
//Lambda Fighter
class CLambda:public CEnemy
{
private:
	
	int iIAStep;

public:

	int iMode; //tipo di comportamento: 0=come CEnemy 1=decolla da terra 2=algoritmi IA		
	CLambda(void);
	BOOL Update(void);
	void Reset(void);
}
*/

//------------------------------------- CLambda -------------------------------------------------

CLambda::CLambda(void)
{
	dwScore=200;	
	iTakeOffDelay=58; 
	dwType=12;
	pShell=NULL;
	pShell1=NULL;
	pShell2=NULL;
	Reset();
}


//-----------------------------------  CLambda::Reset -----------------------------------------

void CLambda::Reset(void)
{
	CEnemy::Reset();
	iStepFire=0;
	iStepPath=0;
	iStepUpdate=0;
	iStepConfig=0;	
	iUpdateFreq=2;	
	pPath=pFollowPath; //riporta nella posizione originale	
	bFollowPath=FALSE;
	if (m_dwFlags==1) 
	{
		//x per decollo
		iTakeOffDelay=g_fmath.RndFast(300)+180;
		SetPath(m_pAuxPath[3],m_iAuxSteps[3]); //path per decollo da terra	
		bGround=TRUE;
		if (this->y>90) SetPosition(x,90); //decollo da terra
	}
	else bGround=FALSE;	

	switch (m_dwFlags)
	{
	case 2:
		SetVelocity(180,2);
		break;
	case 3:
		SetPath(m_pAuxPath[2],m_iAuxSteps[1]); //imposta un path addizionale	
		break;
	case 4:
		SetPath(m_pAuxPath[3],m_iAuxSteps[2]); //imposta un path addizionale	
		break;
	}

	iIAStep=0;
	lCurx=0;		
	pPath=pFollowPath; //riporta nella posizione originale
	SetCurFrame(iBaseFrame);

}

//----------------------------------- CLambda::Update -------------------------------------------

BOOL CLambda::Update(void)
{	

	LONG lY; 

	iStepUpdate++;

	if (iStepUpdate>=iUpdateFreq)
	{
		iIAStep++;
		iStepUpdate=0;

		//si comporta in modo differente in base al tipo di lambda fighter
		//il membro iMode viene impostato dalla funzione di inizializzazione dati e non viene piu' cambiato
		switch(m_dwFlags)
		{
		case 1: //decollo da terra

			if (bGround && x<=iTakeOffDelay) 
			{
				g_cs.PlaySound(g_snBleep1,0,0);
			}
            
            bGround=x>iTakeOffDelay;
			bFollowPath=!bGround;
			break;

        case 2://algo ia

			lY=(lTargetY - y);

            //insegue il player
			if (lY<-40) SetVelocity(200,3);
			else if (lY>40) SetVelocity(160,3);
			else SetVelocity(180,3);
			bFollowPath=FALSE;		
		
			break;

		default://si comporta come un enemy generico (segue un path cinematico)

			bFollowPath=TRUE;
			break;		

		}
		
		if (bFollowPath)
		{	
			this->SetVelocity(pPath->Angle,pPath->Velocity);
			iUpdateFreq=pPath->Displ;
			pPath++;
			iStepPath++;		
		}
	}

    //inserire qui la procedura di fuoco  	
	if (dwEnergy<=0) 
	{
		CreateBonus();
		return FALSE;
	}
 
	if (!bGround)
	{
		//fuoco
		iStepFire++;

	if (iStepFire==iFireFreq)
	{
		iStepFire=0;
	
		//estrae tre proiettili 
		pShell=(CShell *)m_Cartridges->Pop();
		pShell1=(CShell *)m_Cartridges->Pop();
		pShell2=(CShell *)m_Cartridges->Pop();

		if (pShell)
		{
			pShell->SetPosition(x,y+15);
			pShell->SetCurFrame(10);//10
			pShell->SetVelocity(150,7);
			pShell->dwPower=2;
			pShell->SetInitialEnergy(1);
			pShell->Reset();
			pShell->dwClass=CL_ENFIRE;
			m_ActiveStack->Push(pShell);	
			g_cs.PlaySound(g_snFire11,0,0);
		}	

		if (pShell1)
		{
			pShell1->SetPosition(x,y+15);
			pShell1->SetCurFrame(14);
			pShell1->SetVelocity(210,7);
			pShell1->dwPower=2;
			pShell1->SetInitialEnergy(1);
			pShell1->Reset();
			pShell1->dwClass=CL_ENFIRE;
			m_ActiveStack->Push(pShell1);	
		}	

		if (pShell2)
		{
			pShell2->SetPosition(x,y+15);
			pShell2->SetCurFrame(12);
			pShell2->SetVelocity(180,7);
			pShell2->dwPower=2;
			pShell2->SetInitialEnergy(1);
			pShell2->Reset();
			pShell2->dwClass=CL_ENFIRE;
			m_ActiveStack->Push(pShell2);	
		}	

	}
	}

	if (bFollowPath)
	{			
		//aggiorna la posizione in base ai nuovi valori di traiettoria e velocit�
		this->UpdatePosition();	
		//aggiorna l'assetto
		this->UpdateTrim();
		return (iStepPath < iStepPathNum);
	}
	else if (bGround)
	{
		//non � ancora decollato
		if (lCurx>0)
		{
			x-=lGroundX-lCurx; //moto di trascinamento dovuto allo scrolling
		}
		lCurx=lGroundX; //lGroundX � la x del terreno corrente impostata da RefreshObjects

	}
	else
	{
		//aggiorna la posizione in base ai nuovi valori di traiettoria e velocit�
		this->UpdatePosition();	
		//aggiorna l'assetto
		this->UpdateTrim();
		//l'oggetto esce dallo schermo
		if (!(x>=-50 && x<=SCREEN_WIDTH+180 && y>-20)) return FALSE;
		
		//il missile si schianta al suolo
		if (y>SCREEN_HEIGHT-90)
		{
			this->lpfnExplode(x,y);
			return FALSE;	
		}
	}

	return TRUE;
}


///////////////////////////////// classe CMFighter //////////////////////////////////////////////

//costruttore
CMFighter::CMFighter(void)
{	
	dwScore=500;
	Reset();
	dwClass=CL_ENEMY;
	dwType=13;
}


//------------------------------------ CMFighter::Reset --------------------------------------------

void CMFighter::Reset(void)
{
	CEnemy::Reset();
	iIAStep=0;
	iStepSeq=0;
	pShell=NULL;
	lCurx=0;
	bFollowPath=FALSE;
	iUpdateFreq=iStartUpdateFreq;
	iStepFire1=0;
	iStepSeq1=0;
	SetCurFrame(iBaseFrame);
}


void CMFighter::SetFlags(DWORD dwFlags)
{
	m_dwFlags=dwFlags;
}

//------------------------------------ CMFighter::Update --------------------------------------------

BOOL CMFighter::Update(void)
{
	int ang;
	LONG ldelta;

	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		iStepUpdate=0;
	
			if (lTargetX>x)
			{
				SetVelocity(180,3);
			}
			else
			{
				ldelta=(y-lTargetY);

				//inizia ad accelerare se il target � di fronte
				if (ldelta<15 && ldelta>-15) 
				{

					speed++;
					ang=180;
					if (speed>6) speed=6;

				}
				else
				{

					ang=m_fm.GetAngle(x,y,lTargetX,lTargetY);
					if (ang<120) ang=120;
					else if (ang>240) ang=240;

					if (ang>angle_dir) ang=angle_dir + 5;
					else if (ang<angle_dir) ang=angle_dir -5;

					speed--;
					if (speed<3) speed=3;

				}
				
				SetVelocity(ang,speed);	
			}			
	
	}

	//se non ha piu' energia rende false e l'oggetto viene rimosso
	if (dwEnergy<=0) 
	{
		CreateBonus();
		return FALSE;
	}

	iStepFire++;

	if (iStepFire==iFireFreq)
	{
		iStepSeq++;

		//spara una raffica 
		if (iStepSeq<4) iStepFire=iFireFreq-8;
		else 
		{
			iStepSeq=0;
			iStepFire=0;
		}

	
		//estrae un proiettile dallo stack 
		//pick a shell from the shells stack
		pShell=(CShell *)m_Cartridges->Pop();

		if (pShell)
		{
			//angolo della congiungente nemico-giocatore
			ang=m_fm.GetAngle(x,y,lTargetX,lTargetY);
			pShell->SetPosition(x,y+15);
			pShell->SetVelocity(ang,6);
			pShell->dwPower=2;		
			pShell->SetInitialEnergy(1);
			pShell->Reset();
			pShell->dwClass=CL_ENFIRE;
			g_cs.PlaySound(m_lFireSound,0,0);
			m_ActiveStack->Push(pShell);
		}

	}

	//missili
	iStepFire1++;

	if (iStepFire1==110)
	{
		iStepSeq1++;

		//spara una raffica 
		if (iStepSeq1<2) iStepFire1=90;
		else 
		{
			iStepSeq1=0;
			iStepFire1=0;
		}
        //estrae un missile 
		pM=(CMissile *)m_pWeapon1->Pop();

		if (pM)
		{
			//angolo della congiungente nemico-giocatore		
			pM->SetInitialEnergy(1);
			pM->Reset();
			pM->SetPosition(x,y+15);
			pM->SetVelocity(90,6);
			pM->SetCurFrame(12);		
			pM->dwPower=6;
			pM->dwClass=CL_ENEMY;
			pM->mode=1;			
			m_ActiveStack->Push(pM);
			g_cs.PlaySound(g_snMissile,0,0);

		}

	}
	
	//aggiorna la posizione in base ai nuovi valori di traiettoria e velocit�
	this->UpdatePosition();	
	//aggiorna l'assetto
	this->UpdateTrim();
	//l'oggetto esce dallo schermo
	//object is out of the screen 
	if (!(x>=-100 && x<=SCREEN_WIDTH+100 && y>-40)) return FALSE;
	
	//il missile si schianta al suolo
	if (y>SCREEN_HEIGHT-90)
	{
		this->lpfnExplode(x,y);
		return FALSE;	
	}
	

	return TRUE;
}

///////////////////////////////////////////// classe CLFighter ///////////////////////////////////////////

CLFighter::CLFighter(void)
{
	dwClass=CL_ENEMY;
	dwType=30;
 
}

void CLFighter::Reset(void)
{
	CEnemy::Reset();
	SetVelocity(180,CSbLevel::g_scroll_speed+1);
	iUpdateFreq=50;
	iFireFreq=g_fmath.RndFast(50)+20;
	m_iStartY=0;
}

void CLFighter::SetDontRemoveFlag(BOOL bVal)
{
	
};

BOOL CLFighter::Update(void)
{
	iStepUpdate++;	

	if (iStepUpdate>=iUpdateFreq && m_dwFlags==0)
	{
		int prob=g_fmath.RndFast(10);

		iStepUpdate=0;

		switch(prob)
		{
		case 1:

			SetVelocity(165,CSbLevel::g_scroll_speed+1);
			break;

		case 2:

			SetVelocity(195,CSbLevel::g_scroll_speed+1);
			break;

		default:

			SetVelocity(180,CSbLevel::g_scroll_speed+1);

		}		

		if (y<20 || y>350) SetVelocity(180,CSbLevel::g_scroll_speed+1);	

	}

	if (m_dwFlags==0)
	{
		UpdatePosition();
	}
	else
	{
		if (m_iStartY==0) m_iStartY=y;

		float f=(float)sin(2*3.1415f*x/CSBObject::g_iScreenWidth);
		//movimento sinusoidale
		SetPosition(x-2,(LONG)(50*f+m_iStartY));

	}


	iStepFire++;

	if (iStepFire>iFireFreq)
	{

		iStepFire=0;

		if (m_Cartridges)
		{
			if (abs(lTargetY-y-22)<50)
			{

				CSBObject* pobj=m_Cartridges->Pop();

				if (pobj)
				{
					pobj->Reset();
					pobj->dwClass=CL_ENFIRE;
					pobj->SetInitialEnergy(1);
					pobj->dwPower=10;
					pobj->SetPosition(x,y+22);
					pobj->SetVelocity(180,16);
					pobj->SetCurFrame(0);
					g_cs.PlaySound(g_snFire8,0,0);
					m_ActiveStack->Push(pobj);

				}
			}

			else iStepFire=iFireFreq-1; //� pronto a sparare di nuovo

		}


	}

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return (x>-150 && y>-80 && y<CSBObject::g_iScreenHeight);
}



//////////////////////////////////////////// classe CBlaster /////////////////////////////////////////////

CBlaster::CBlaster(void)
{
	dwType=21;
	dwScore=1000L;	
	Reset();
}

//------------------------------------------ CBlaster::SetFlags --------------------------------------
//imposta l'algoritmo ia
void CBlaster::SetFlags(DWORD dwFlags)
{
	if (dwFlags<=4)
	{
		m_dwFlags=dwFlags;
	}		
}

//------------------------------------------ CBlaster::Reset ------------------------------------------

void CBlaster::Reset(void)
{	
	CMFighter::Reset();
	iStepUpdate=0;
	iStepConfig=0;	
	iConfigFreq=2;	
	iStepConfig=0;
	iAnimIncr=1;
	iRotStep=0;
	iChRotFreq=80;
	iIAStep=0;
	dwEnergy=20;
	iFireCnt=0;
	dwClass=CL_ENEMY;
	SetCurFrame(0);
	SetVelocity(0,0);

	if (m_dwFlags>0) iIAStep = (int) m_dwFlags;
}

//----------------------------------------- CBlaster::Update ----------------------------------------

BOOL CBlaster::Update(void)
{

	static int ang;
	static int ir;

	iStepConfig++;

	if (iStepConfig==iConfigFreq)
	{
		//procedura per cambio frame in modo da far apparire l'oggetto in rotazione sul proprio asse
		iStepConfig=0;//resetta il contatore di configurazione
		cur_frame += iAnimIncr;

		if (cur_frame>7) cur_frame=0;
		else if (cur_frame<0) cur_frame=7;

		iRotStep++;

		if (iRotStep==iChRotFreq)
		{
			iRotStep=0;
			//cambia il verso di rotazione
			iAnimIncr = -iAnimIncr; 
		}
	}	

	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{

	
		iStepUpdate=0;

		//iIAStep � zero nella prima fase in cui segue un path cinematico
		if (iIAStep==0)  
		{
			if (!SetNextPathStep()) iIAStep=1; //entra nella seconda fase
		}

		else if (iIAStep==1)
		{		
			ang=m_fm.GetAngle(x,y,lTargetX,lTargetY);
			SetVelocity(ang,2);			
			if (x<190) iIAStep=2; //terza fase : si allontana
		}
		else if (iIAStep==2)
		{
			iAnimIncr=1;
			iIAStep=3; //non aggiorna piu'
			ir=m_fm.RndFast(100);
			SetVelocity(170+ir,2); //imposta la traiettoria di allontanamento
			
		}
		else if (iIAStep==4)
		{
			//sta esplodendo
			cur_frame++;
			speed++;
			if (speed>6) speed=6;
			if (cur_frame>10) cur_frame=10;
			if (lpfnExplode) lpfnExplode(x+20,y+20);
		}
	
	}

	//non c'� piu' energia: inzia la fase di distruzione (da fare)
	if (dwEnergy<=0) 
	{
	//	dwClass=CL_NONE; //in questa fase non puo' essere colpito
		if (iIAStep<4)
		{
			iIAStep=4; //fase di esplosione		
			if (CSbLevel::g_gravity>0)
			{			
				SetVelocity(90,1); //inizia a precipitare
			}			
			iConfigFreq=0;
			cur_frame=8;
			CreateBonus();
		}
	}

	UpdatePosition();


	//l'oggetto esce dallo schermo
	if (iIAStep>=1 && !(x>=-300 && x<=SCREEN_WIDTH+300 && y>-200)) 
	{		
		return FALSE;
	}

	iStepFire++;

	if (iStepFire>=iFireFreq && m_Cartridges && iIAStep<4)
	{
		iFireCnt++;

		if (iFireCnt>3) iFireCnt=1;
	
		switch(iFireCnt)
		{

			case 1://spara una rosa di colpi in avanti

            	iStepFire=0;	

				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					pShell->SetInitialEnergy(1);
					pShell->Reset();
					pShell->SetPosition(x,y+35);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetVelocity(180,8);					
					pShell->dwPower=3;
					m_ActiveStack->Push(pShell);

				}

				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					pShell->SetInitialEnergy(1);
					pShell->Reset();
					pShell->SetPosition(x,y+35);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetVelocity(164,8);				
					pShell->dwPower=3;
					m_ActiveStack->Push(pShell);

				}

				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					pShell->SetInitialEnergy(1);
					pShell->Reset();
					pShell->SetPosition(x,y+35);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetVelocity(196,8);				
					pShell->dwPower=3;
					m_ActiveStack->Push(pShell);

				}

				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					pShell->SetInitialEnergy(1);
					pShell->Reset();
					pShell->SetPosition(x,y+35);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetVelocity(172,8);				
					pShell->dwPower=3;
					m_ActiveStack->Push(pShell);

				}

				pShell=(CShell*)m_Cartridges->Pop();

				if (pShell)
				{
					pShell->SetInitialEnergy(1);
					pShell->Reset();
					pShell->SetPosition(x,y+35);
					pShell->dwClass=CL_ENFIRE;
					pShell->SetVelocity(188,8);				
					pShell->dwPower=3;
					m_ActiveStack->Push(pShell);

				}

				g_cs.PlaySound(g_snFire8,0,0);

			break;

			case 2:

				//raffica
				iStepFire1++;
				if (iStepFire1==5)
				{
					iStepFire1=0;
					iStepFire=0;					
				}

				else 
				{
					iFireCnt--;
					iStepFire=iFireFreq-8;
				}

				ang=m_fm.GetAngle(x,y,lTargetX,lTargetY);

				if (ang>120 && ang <240)
				{
					//se il nemico � davanti spara
					pShell=(CShell*)m_Cartridges->Pop();

					if (pShell)
					{
						pShell->SetInitialEnergy(1);
						pShell->Reset();
						pShell->SetPosition(x,y+35);
						pShell->dwClass=CL_ENFIRE;
						pShell->SetVelocity(ang,8);					
						pShell->dwPower=2;
						m_ActiveStack->Push(pShell);
						g_cs.PlaySound(g_snFire8,0,0);


					}
				}


			break;

			case 3:

				//raffica a dritto
				iStepFire1++;
				if (iStepFire1==4)
				{
					iStepFire1=0;
					iStepFire=0;
					iFireCnt=0;
				}

				else 
				{
					iFireCnt--;
					iStepFire=iFireFreq-8;
				}

				if (m_pWeapon1)
				{
					//se il nemico � davanti spara
					pShell=(CShell*)m_pWeapon1->Pop();

					if (pShell)
					{
						pShell->SetInitialEnergy(1);
						pShell->Reset();
						pShell->SetPosition(x,y+35);
						pShell->SetCurFrame(12);
						pShell->dwClass=CL_ENFIRE;
						pShell->SetVelocity(180,12);					
						pShell->dwPower=6;
						m_ActiveStack->Push(pShell);
						g_cs.PlaySound(g_snFire10,0,0);

					}
				}

				break;
		}
	}
	
	//il missile si schianta al suolo
	if (y>SCREEN_HEIGHT-120)
	{		
		this->lpfnExplode(x,y);	
		return FALSE;	
	}

	return TRUE;	
}

////////////////////////////////// classe CMissile //////////////////////////////////////////////

CMissile::CMissile(void)
{
	m_pSmoke=NULL;
	Reset();
	
}

//------------------------------------ CMissile::Reset() ------------------------------------------

void CMissile::Reset(void)
{
	CEnemy::Reset();
	iIAStep=0;
	dwEnergy=1;
	dwPower=4;
	iIAStart=5; //passo ia opo il quale inizia a comportarsi in modo intelligente
	mode=0;
	iSmokeStep=0;  
}

//-------------------------------------- CMissile::SetSmokeStack ------------------------------------

HRESULT CMissile::SetSmokeStack(CObjectStack *pstack)
{
	if (!pstack) return E_FAIL;

	m_pSmoke=pstack;

	return S_OK;
}

//------------------------------------- CMissile::Update --------------------------------------------

BOOL CMissile::Update(void)
{
	static int ang;

	if (m_pSmoke)
	{
		iSmokeStep++;

		if (iSmokeStep==4)
		{
			CDebris *pobj=NULL;

			pobj=(CDebris*)m_pSmoke->Pop();

			if (pobj)
			{
				//crea una nuvola di fumo           
   				pobj->Reset();
				pobj->SetPosition(x+10,y);
				//imposta la velocit� 
				pobj->SetVelocity(0,0);
				//annulla l'accelerazione di gravit� per questo oggetto
				pobj->SetG(0); 
				//imette la nuvola nello stack degli oggetti
				m_ActiveStack->Push(pobj);
			}

			iSmokeStep=0;
		}

	}

	switch (mode)
	{
	case 0:	//va a dritto	

		UpdatePosition();
		if (dwEnergy<0) return FALSE;

		//object is out of the screen 
		if (!(x>=-100 && x<=SCREEN_WIDTH+100 && y>-40)) return FALSE;
	
		//il missile si schianta al suolo
		if (y>SCREEN_HEIGHT-90)
		{
			this->lpfnExplode(x,y);
			return FALSE;	
		}
		
		return TRUE;		

	case 1: //segue un path
		
		iStepUpdate++;

		if (iStepUpdate==iUpdateFreq)
		{
			iStepUpdate=0; //azzera il contatore

			if (!SetNextPathStep()) return FALSE; //passo successivo nel path cinematico
			
		}

		this->UpdatePosition();

		if (dwEnergy<=0) return FALSE;

		return TRUE;		

	case 2:

		if (iIAStep>iIAStart)
		{
			iStepUpdate++;

			if (iStepUpdate==iUpdateFreq)
			{

				iStepUpdate=0;

				//segue il bersaglio
				ang=m_fm.GetAngle(x,y,lTargetX,lTargetY);
				if (ang<angle_dir) angle_dir+=5;
				else if (ang>angle_dir) angle_dir+=5;

				SetVelocity(angle_dir,speed);

				cur_frame=angle_dir/15;
			}

			UpdatePosition();

					//l'oggetto esce dallo schermo
			if (!(x>=-10 && x<=SCREEN_WIDTH+80 && y>-20)) return FALSE;

			//il missile si schianta al suolo
			if (y>SCREEN_HEIGHT-90)
			{
				this->lpfnExplode(x,y);
				return FALSE;	
			}
			
			if (dwEnergy<=0) return FALSE;

			return TRUE;
		}

		else
		{
			iStepUpdate++;

			if (iStepUpdate==iUpdateFreq)
			{
				iStepUpdate=0; //azzera il contatore
	
				return SetNextPathStep();//passo successivo nel path cinematico
			}

			this->UpdatePosition();

			if (dwEnergy<=0) return FALSE;

			return TRUE;
		}		
	}

	return FALSE;
}

////////////////////////////////// classe CSbBonus ///////////////////////////////////////////////

CSbBonus::CSbBonus(void)
{
	dwScore=10;
	dwType=15;
	dwClass=CL_BONUS;
	dwScore=100;
	anim_clock=0;
	anim_speed=1;
	Reset();

}

BOOL CSbBonus::Update(void)
{

	iStepConfig++;

	if (iStepConfig==2)
	{
		iStepConfig=0;
		//cambia la frame 
		UpdateFrame();		
	}

    iStepUpdate++;
	
	if (iStepUpdate==8)
	{
		iStepClock++;
		iStepUpdate=0;
		if (iStepClock<40)
		{			
			angle_dir=angle_dir+10;		
			SetVelocity(angle_dir,2);		
		}
		else
		{
			//dopo un po' il bonus va in fuga
			angle_dir=angle_dir+(int)(30-iStepClock*0.4);
			SetVelocity(angle_dir,3);
			if (y<-50 || y>SCREEN_HEIGHT || x<-40 || x>SCREEN_WIDTH) return FALSE;
		}

	}

	UpdatePosition();

	return (dwEnergy>0);

}


void CSbBonus::Reset(void)
{

	iStepClock=0;
	cur_frame=0;
	dwPower=0;
	dwEnergy=5;	
	iStepUpdate=0;
	dwClass=CL_BONUS;
	iStepConfig=0;
	
	SetVelocity(g_fmath.RndFast(360),2);
}

//////////////////////////////////////// classe CShield /////////////////////////////////////

CShield::CShield(void)
{
	dwType=16;
	dwClass=CL_FRIEND; 
	pfm=NULL;
}

void CShield::Reset(void)
{
	
	lradius=12;
	anim_clock=0;
	anim_speed=0.8f;
	angle_dir=0;
	speed=2;
	dwEnergy=60;
	dwPower=4;
	iStepConfig=0;
	iUpdateFreq=2;
	iStepUpdate=0;
	iTotalStep=1200;
}


//------------------------------------- CShield::Update --------------------------------------


BOOL CShield::Update(void)
{

	UpdateFrame();

	angle_dir +=2;	
	if (angle_dir>359) angle_dir=2;		
	iTotalStep--;

	if (lradius<120) lradius+=2;

	if (iTotalStep<120) lradius+=3; //allontana la barriera in movimento a spirale quando sta per finire
	
	x=FIXEDTOLONG(lradius*pfm->CosFast[angle_dir]);
	y=FIXEDTOLONG(lradius*pfm->SinFast[angle_dir]);    

	return (iTotalStep>0 && dwEnergy>0);

}

/////////////////////////////////////////////////// CBooster ////////////////////////////////////////

CBooster::CBooster(void)
{

	m_iWidth=500;
	m_iDeltaSpeed=2;
	m_iLength=80;
	m_pLv=NULL;
	m_iUpdateStep=0;
	m_iStatus=0;
	m_iUpdateFreq=15;
	m_fCurSpeed=0;
	m_lx1=m_lx2=0;
	m_fAcc=(float)m_iDeltaSpeed/m_iWidth; //delta vel./pixel
}


//imposta le caratteristiche della rampa di accelerazione
//iWidth=larghezza aree di accelerazione e decelerazione
//iLength=numero di cicli area centrale a velocit� costante
//iDeltaSpeed = incremento velocit�

HRESULT CBooster::SetRamp(int iUpdateFreq,int iDeltaSpeed,int iLength)
{
	if (iLength>0 && iUpdateFreq>0)
	{
		m_iUpdateFreq=iUpdateFreq;
		m_iDeltaSpeed=iDeltaSpeed;
		m_iLength=iLength;

		return S_OK;
	}

	else return E_FAIL;
}

void CBooster::Reset(void)
{
	m_iUpdateStep=0;
	if (m_pLv)
	{

		m_iStatus=0; //in fase di accelerazione (prima rampa)
		m_fAcc=(float)m_iDeltaSpeed/m_iWidth; //delta vel./pixel
		//inizio rampa
		m_lx1=m_pLv->GetCurrentX();		
		m_iStartSpeed=m_pLv->GetScrollSpeed(); //incremento relativo della velocit�
		Player1.bNozzleFlames=TRUE; //fiamme ugello del player
	}

	else m_iStatus=-1;
}


BOOL CBooster::Update(void)
{		
	int v;

	m_iUpdateStep++;
	
	switch (m_iStatus)
	{
	case 0:

		if (m_iUpdateStep>=m_iUpdateFreq)
		{
			v=m_pLv->GetScrollSpeed();
			
			v++;
			
			if (v>=m_iStartSpeed+m_iDeltaSpeed) m_iStatus=1;
			
			m_pLv->SetScrollSpeed(v); //cambia la velocit� di scrolling

			m_iUpdateStep=0;
		
		}	

		break;

	case 1:
	
		//parte centrale: velocit� costante
		if (m_iUpdateStep>=m_iLength) 
		{	
			v=m_pLv->GetScrollSpeed();
			
			m_iStatus=2; //imposta lo stato successivo
			
			m_iUpdateStep=0;			
		}
		
		break;

	case 2:

		if (m_iUpdateStep>=m_iUpdateFreq)
		{
			v=m_pLv->GetScrollSpeed();
		
			m_iUpdateStep=0;
		
			v--;
			
			m_pLv->SetScrollSpeed(v); //cambia la velocit� di scrolling
			
			if (v<=m_iStartSpeed) 
			{
			
				m_iStatus=0;	
				
				Player1.bNozzleFlames=FALSE;				

				return FALSE;
			}
		
		}			
		
		break;

	default:    			

		//distrugge l'oggetto 
		Player1.bNozzleFlames=FALSE;

		return FALSE;
	}


	return TRUE;
}


HRESULT CBooster::Draw(void)
{

	return S_OK;
}

//////////////////////////////////////////////////// CGyro //////////////////////////////////////////
//fuoco amico

CGyro::CGyro(void)
{
	dwType=17;
	dwClass=CL_FRIEND;
	pfm=NULL;
	iFireFreq=90;
}


void CGyro::Reset(void)
{

	CEnemy::Reset();
	Cannon.SetInitialEnergy(20);
	Cannon.SetCurFrame(4);
	Cannon.Reset();
	SetAnimSpeed(0.7f);
	dwEnergy=190;
	iTotalStep=1200;
	lradiusx=lradiusy=20;
	iUpdateFreq=40;
	iLastTarget=-1;
	iCannonTargetFrame=-1;
	iLstTgType=-1;
	iStepFire=0;
	iRepeatFire=0;
	iFireFreq=70;
	
}

//restituisce un numero >= che indica l'importanza di un nemico
//serve a selezionare il nemico piu' forte come target
int CGyro::GetObjectValue(CSBObject *pobj)
{
	switch(pobj->dwResId)
	{
	case EN_RADAR:
	case EN_FACTORY:

		return 1;

	case EN_UFO:
	case EN_GOLDENFIGHTER:
	case EN_LAMBDAFIGHTER:
	case EN_MFIGHTER:
	case EN_XWING:
	case EN_ANTIAIRCRAFT:

		return 2;

	case EN_BLASTER:
	case EN_ANTIAIRCRAFT2:
	case EN_UFO2:

		return 3;

	case EN_SHIP:
	case EN_AIRCRAFTCARRIER:
	case EN_TANK:
	case EN_GFIGHTER:

		return 4;

	case EN_SNAKERED:
	case EN_SNAKE:
	case EN_ANTIAIRCRAFT3:

		return 5;

	case EN_BOSS1:
	case EN_BOSS2:
	case EN_BOSS3:
	case EN_BOSS4:
	case EN_BOSS5:
	case EN_SPIDER1:
	case EN_SPIDER2:
	case EN_SPIDER3:

		return 6;

	default:

		return 0;
	}
}	

BOOL CGyro::Update(void)
{
	
	iStepConfig++;

	if (iStepConfig==2)
	{
		iStepConfig=0;
		//cambia la frame 
		cur_frame++;
		if (cur_frame>max_frame_index) cur_frame=0;			

		//riposiziona il cannone se � stato impostato un bersaglio
		if (iCannonTargetFrame > 0)
		{
			cf=Cannon.GetCurFrame();

			cf+=iFrmIncr;
			
			Cannon.SetCurFrame(cf);

			if (cf == iCannonTargetFrame) iCannonTargetFrame=-1; //in questo caso � arrivato alla posizione voluta
		}
	}

	iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		if (m_ActiveStack)
		{
			
			iStepUpdate=0;

			int iMax;
			//punta il primo oggetto dello stck attivo
			//inizia da qui la scansione alla ricerca di nemici da puntare
			CSBObject **pList=m_ActiveStack->m_objStack;
			CSBObject *pobj=NULL;
			CSBObject *pTarget=NULL; //target scelto
			int cur_value=-1;

			pobj=pList[0];
		
			//acquisisce il numero di elementi attivi
			iMax=m_ActiveStack->ItemCount();

			//acquisisce le coordinate assolute
			GetAbsXY(&iax,&iay);

			//ricerca un nemico a cui sparare all'interno dello stack degli oggetti attivi
			for (int count=0;count<iMax;count++)
			{
				
				if (pobj && pobj->bActive && pobj->dwClass==CL_ENEMY && iLastTarget != count)
				{
					if (cur_value<GetObjectValue(pobj)) 
					{
						iLastTarget=count;
						pTarget=pobj;
					}
				}

				pobj=pList[count]; //passa all'oggetto successivo
			}//fine for

			if (pTarget)
			{						
				
				//acquisisce l'angolo di direzione per colpire il nemico
				ang=pfm->GetAngle(iax,iay,pTarget->x,pTarget->y);					
				
				if (ang<=70 && ang>=0)
				{
					iLstTgType=0;
					//ricava la frame su cui deve spostarsi per puntare approssimativamente quella direzione
					//(ruota in senso orario)
					iCannonTargetFrame=4+(int)((float)ang * 0.066666667);  //divide per 15
					
					cf=Cannon.GetCurFrame();

					if (iCannonTargetFrame < cf) iFrmIncr=-1;
					else if (iCannonTargetFrame > cf) iFrmIncr=+1;
					else iFrmIncr=0;					
					

				}
				else if (ang<=360 && ang >=290)
				{					
				
					iLstTgType=1;
					//(ruota in senso antiorario)
					ang=360-ang;
					iCannonTargetFrame=4-(int)((float)ang * 0.066666667);
					if (iCannonTargetFrame<0) iCannonTargetFrame=0;

					cf=Cannon.GetCurFrame();

					if (iCannonTargetFrame < cf) iFrmIncr=-1;
					else if (iCannonTargetFrame > cf) iFrmIncr=+1;
					else iFrmIncr=0;
				
				}						
			}

		}					
			
	}


	iStepFire++;

	//ciclo per il fuoco
	if (iStepFire==iFireFreq)
	{
		iRepeatFire++;

		//spara una raffica
		if (iRepeatFire<5) iStepFire=iFireFreq-5;
		else 
		{
			iStepFire=0;
			iRepeatFire=0;
		}


		CSBObject *pshell=NULL;
	
		pshell=m_Cartridges->Pop();

		//frame corrente del cannone
		cf=Cannon.GetCurFrame();
		
		if (pshell)
		{
		
			pshell->SetInitialEnergy(3);
			pshell->Reset();
			pshell->dwClass=CL_PLFIRE;		
			pshell->dwPower=3;		

			if (cf<4) 
			{
				//rivolto in alto				
				pshell->SetCurFrame(4-cf);
				pshell->SetVelocity(360-(4-cf)*15,8);
			}
			else 
			{
				//rivolto in basso
				pshell->SetCurFrame(12-(cf-4));		
				pshell->SetVelocity((cf-4)*15,8);
			}

			//acquisisce le coordinate assolute
    		GetAbsXY(&iax,&iay);

			pshell->SetPosition(iax+18,iay+22);
		
			//inserisce il proiettile nello stck attivo
			m_ActiveStack->Push(pshell);

			g_cs.PlaySound(g_snFire1,0,0); //effetto sonoro	
		
		}

	}


	angle_dir -=1;	
	if (angle_dir<0) angle_dir=360;		
	iTotalStep--;

	if (lradiusx<150) 
	{
		lradiusx+=2;
		
	}

	if (iTotalStep<120) 
	{
		lradiusx+=3; //allontana la barriera in movimento a spirale quando sta per finire	
	}
	
	x=FIXEDTOLONG(lradiusx*pfm->CosFast[angle_dir]);
	y=FIXEDTOLONG(lradiusx*pfm->SinFast[angle_dir]);    
   
	return (iTotalStep>0 && dwEnergy>0);	

}


HRESULT CGyro::Draw(void)
{
	HRESULT hr;

     //imposta per l'oggetto figlio l'immagine di destinazione
	Cannon.m_pimgOut=m_pimgOut;
    //le coordinate x e y del cannone si riferiscono al sistema del suo oggetto genitore
	Cannon.x=x+22;
	Cannon.y=y;
	Cannon.Draw();
	hr=CSBObject::Draw();

	return hr;

}

//////////////////////////////////////////////////// CDynString ////////////////////////////////////

CDynString::CDynString(void)
{
	iMode=0;
	iDelay=50; 
	dwClass=CL_NONE; //non interagisce
	anim_clock=1;//per default non � un oggetto animato
	anim_speed=0; 
	m_pch=NULL;
	m_szText=NULL;
	Reset();
}

CDynString::~CDynString(void)
{
	SAFE_DELETE_ARRAY(m_szText);

}

void CDynString::FreeText(void)
{
	SAFE_DELETE_ARRAY(m_szText);
}

void CDynString::Reset(void)
{
	iStatus=0;
	iBlinkCnt=0;
	iStepUpdate=0;
	iStepConfig=0;
}

BOOL CDynString::Update(void)
{
	iStepConfig++;

	if (iStepConfig==2)
	{
		iStepConfig=0;
		//cambia la frame 
		UpdateFrame();		
	}
	
	switch (iMode)
	{

    //movimento verso l'alto (es. descrizioni dei bonus) 
	case 0:
		
		if (iStatus==0) 
		{
			SetVelocity(270,1);
			iStatus=1;
		}

		iStepUpdate++;

		if (iStepUpdate==6)
		{
			iStepUpdate=0;
			speed++;
			SetVelocity(270,speed);
			iStatus++;

			if (iStatus>=6) return FALSE;

		}

		UpdatePosition();
		break;

    //testo fisso  (es. descrizione del livello)
	case 1:
		
		iStepUpdate++;

		return iStepUpdate<iDelay;

		break;

	}

	return TRUE;
}

HRESULT CDynString::SetCharSet(CADXCharSet *pch)
{
	if (!pch) 
	{
		m_pch=NULL;
		return S_OK;
	}

	if (pch->GetStatus()<=0) return E_FAIL;

	m_pch=pch;

	return S_OK;
}


void CDynString::SetText(TCHAR *txt)
{
	SAFE_DELETE_ARRAY(m_szText);
	if (txt)
	{
		size_t len;
		len=strlen(txt);
		m_szText=new TCHAR[len/sizeof(TCHAR)+1];
		memset(m_szText,0,sizeof(TCHAR)*(len+1));
		memcpy(m_szText,txt,len);
	}
}

HRESULT CDynString::Draw(void)
{
	if (m_szText && m_pch)
	{

		UpdateAbsXY();
		//in questo caso c'� un testo da stampare
		m_pch->TextOutFast(xabs,yabs,m_pimgOut,m_szText);

		return S_OK;
	}

	//metodo di rendering standard
	else return CSBObject::Draw();

}

void CDynString::SetDelay(int ival)
{
	if (ival>=0) iDelay=ival;
}
	
int CDynString::GetDelay(void)
{
	return iDelay;
}

/////////////////////////////////// classe CSmoke ////////////////////////////////////////////////

CSmoke::CSmoke(void)
{
	dwType=19;
	dwClass=CL_NONE; //non colpibile
	SetUpdateFreq(8);
	CEnemy::Reset();
	pSmokeSource=NULL;
	Reset();
}

void  CSmoke::Reset(void)
{
	CEnemy::Reset();
	iStepUpdate=0;
}

void CSmoke::SetSmokeVelocity(int iangle,int vel)
{	
	m_iangle=iangle % 360;
	if (m_iangle<0) m_iangle=360+m_iangle;
	m_ivel=vel;
}

//imposta lo stack con le nuvolette di fumo
HRESULT CSmoke::SetSmokeStack(CObjectStack *pStack)
{
	if (!pStack) return E_FAIL;
	pSmokeSource=pStack;
	return S_OK;
}


//fumo che esce continuamnete verso l'alto
BOOL CSmoke::Update(void)
{
    iStepUpdate++;

	if (iStepUpdate==iUpdateFreq)
	{
		CDebris *pobj=NULL;

		iStepUpdate=0;
		if (pSmokeSource)
		{
			//crea una nuvola di fumo
           	pobj=(CDebris *)pSmokeSource->Pop();
			if (pobj)
			{
   				pobj->Reset();
				pobj->SetPosition(x,y);
				//imposta la velocit� 
				pobj->SetVelocity(m_iangle,m_ivel);
				//annulla l'accelerazione di gravit� per questo oggetto
				pobj->SetG(0); 
				//imette la nuvola nello stack degli oggetti
				m_ActiveStack->Push(pobj);

			}
		}	
	}

	UpdatePosition();

	return (x>-40);
}

////////////////////////////////// classe CGameOver /////////////////////////////////////////////

CGameOver::CGameOver(void)
{	
	int a=0;
	int r;

	m_imax=200;
	xcur=0;
	iDelay=100;
	iDelayStep=0;


	//definisce il path di spostamanto dei caratteri
    //m_imax=dimensione dei vettori m_xp e m_yp
	for(int x=0;x<m_imax;x++)
	{		
		r=(int)((m_imax-x)*2);
        a+=2; //angolo
		//definisce una spirale
		//converte da coordinate polari a rettangolari
		m_xp[x]=(int)(r*m_math.CosTable[a%360]);
		m_yp[x]=(int)(r*m_math.SinTable[a%360]);
	}


	pQuitLoopFlag=NULL;
	pCharSet=NULL;
	m_szText=NULL;
	m_szText=new TCHAR[10];
    memset(m_szText,0,sizeof(TCHAR)*10);
    memcpy((void *)m_szText,"GAME OVER",sizeof(TCHAR)*9);

}

CGameOver::~CGameOver(void)
{
	SAFE_DELETE_ARRAY(m_szText);
}

void CGameOver::Reset(void)
{
	CSBObject::Reset();
	if (pQuitLoopFlag)
	{
		*pQuitLoopFlag=FALSE;
	}

	xcur=0;//indice nei path
	iDelayStep=0;
	//fa in modo di centrare l'etichetta nello schermo
	m_xout=(int)((SCREEN_WIDTH-pCharSet->GetTextWidth("GAME OVER"))*0.5);
	m_yout=(int)((SCREEN_HEIGHT-pCharSet->GetCharHeight())*0.5);
}


BOOL CGameOver::Update(void)
{

	if (iDelayStep==0) xcur++;	

	if (xcur>m_imax-2)
	{
		//alla fine del path la scritta sta ferma al centro per un po'
		iDelayStep++;

		if (iDelayStep==iDelay)
		{
			//termina 
			//imposta il flag che fa uscire dal ciclo
			//il flag � il membro bExitGame del Player che viene impostato quando si inizializza il player
			//questo flag viene monitorato dalla funzione HeartBeat
			if (pQuitLoopFlag) *pQuitLoopFlag=TRUE;
			return FALSE;
		}

		
	}

	return TRUE;	
}


HRESULT CGameOver::Draw(void)
{

	//esegue il blitting dei caratteri su una superficie con clipper
	pCharSet->TextOutFast(m_xout+m_xp[xcur],m_yout+m_yp[xcur],m_pimgOut,"GAME OVER");	

	return S_OK;

}

////////////////////////////////////////// classe CDebris //////////////////////////////////////////


CDebris::CDebris(void)
{
	dwClass=CL_NONE; //oggetto non colpibile
	Reset();
	m_iConfigFreq=15;
	SetG((float)9.8182); //accelerazione di gravit� di default

}

//impota l'accelerazione di gravit�
void CDebris::SetG(float g)
{
	g2=(float)(g*0.5);
}

HRESULT CDebris::SetMaxFrames(int iMax)
{
	if (iMax>=0)
	{
		m_iMaxFrames=iMax;
		return S_OK;
	}

	else return E_FAIL;

}

void CDebris::Reset(void)
{
	m_ft=0; //tempo
	m_iConfigStep=0;
	cur_frame=0;
	m_idx=delta_x[cur_frame][0]; //delta x e y per l'ouput
	m_idy=delta_y[cur_frame][0]; 
	m_iMaxFrames=0;
	old_x=x; //memorizza il punto iniziale della traiettoria
	old_y=y;
}

void CDebris::SetConfigFreq(int freq)
{
	if (freq>=0) m_iConfigFreq=freq;
}

//questo oggetto si sposta con moto parabolico
BOOL CDebris::Update(void)
{
	m_iConfigStep++;

	if (m_iConfigStep==m_iConfigFreq)
	{		
		
		m_iConfigStep=0;

		if (max_frame_index==0)
		{
			//oggetto con una sola frame
			return (m_ft<1.5);

		}
		else
		{
			//il detrito viene rimosso dallo stack quando raggiunge l'ultima frame
			if (FAILED(SetCurFrame(cur_frame+1))) return FALSE;
			m_idx=delta_x[cur_frame][0];
			m_idy=delta_y[cur_frame][0];
		}
	
	}

	//il moto del detrito � di tipo parabolico
	//legge del moto parabolico (m_it=tempo virtuale)
	//velocit� =[pixel/ciclo]
	//tempo=[ciclo]
   
    m_ft += (float)0.18; //incrementa il tempo virtuale
	x=(int)(iVelx*m_ft+old_x);
	y=(int)(g2*m_ft*m_ft+iVely*m_ft+old_y);

	return (m_iMaxFrames==0 || (cur_frame<=m_iMaxFrames));

}


HRESULT CDebris::Draw(void)
{
	static IMAGE_FRAME_PTR pimg;
	static LPDIRECTDRAWSURFACE7 lpDDS;
    
	UpdateAbsXY();

	pimg=frames[cur_frame];

	if (pimg)
	{	
		rcBoundBox.left=xabs-m_idx;
		rcBoundBox.top=yabs-m_idy;
		rcBoundBox.right=xabs+pimg->width-m_idx;
		rcBoundBox.bottom=yabs+pimg->height-m_idy;
		lpDDS=m_pimgOut->surface;			
		return lpDDS->Blt(&rcBoundBox,pimg->surface,NULL,DDBLT_KEYSRC,NULL);
	}	

	else return E_FAIL;		

}


//////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------------------ IsUpdDown -------------------------------------------
//Ci sono alcuni oggetti vincolati al terreno che hanno come sistema di riferimento 
//un asse y che va dal basso verso l'alto.Qusto serve a posizionarli correttamente anche con altre risuluzioni grafiche
//questa funzione rende true se il riferimento � dal basso verso l'alto per l'oggetto indicato
BOOL IsUpDown(int id)
{
	switch(id)
	{

		//lista oggetti vincolati al terreno
		case EN_ANTIAIRCRAFT:
		case EN_ANTIAIRCRAFT2:
		case EN_ANTIAIRCRAFT3:
		case EN_RADAR:
		case EN_AIRCRAFTCARRIER:
		case EN_TANK:
		case EN_SHIP:
        case EN_SHANTY1:
        case EN_SHANTY2:
        case EN_SHANTY3:
        case EN_SHANTY4:
		case EN_WALL:
		case EN_PALMS:
		case EN_PYLON:
		case EN_STREET:	
        case EN_SPIDER1:
        case EN_SPIDER2:
		case EN_SPIDER3:
		case EN_LAMBDAFIGHTER:	
        case EN_FACTORY:
		case EN_NIGHT_BUILDING1:
		case EN_NIGHT_BUILDING2:
		case EN_NIGHT_BUILDING3:
		case EN_NIGHT_BUILDING4:
		case EN_NIGHT_BRIDGE:
		case EN_SHIELD_BASE:
		case EN_SHELL_BASE:
		case EN_TURRET:
		case EN_SPACE_BUILD1:
		case EN_SPACE_BUILD2:
		case EN_SPACE_BUILD3:
		case EN_SPACE_BUILD4:
		case EN_RAIL:
		case EN_TRAIN:

			return TRUE;
	}

	return FALSE;

};

//------------------------------------ LoadSbLevel -----------------------------------------

//carica il livello DestLevel dal file szInputFile
int LoadSbLevel(char *szInputFile,CSbLevel *DestLevel)
{
	FILE *f;
	char i[5];
	SBL_GROUND_LAYER_PTR pl=NULL;
	SBL_FUNCTION_PTR pfn=NULL;
	SBL_BACK_LAYER_PTR pback=NULL;
	SBL_TRIGGER_PTR ptrigger=NULL,ptrigger1=NULL;
	SB_VALUE_LIST_PTR plist; //punta la lista di argomenti
	SBL_LEVEL sbl;	
	IMAGE_FRAME imgTemp;
	RECT rcTemp={0,0,0,0};
	HRESULT hr;
	CADXFastMath fm;
	int igrndindex=0; //contatore layer del terreno (il piu' vicino � su zero)
	int ibackindex=0; //contatore layer del background
	int itriggercount=0; //contatore trigger
	int count,cntargs;
	int ichunk;
	int params[5];
	int errcode=0;
	int resloadfailed=0;


#ifdef _DEBUG_
	_CrtCheckMemory();
#endif

#ifndef _MIDI_OFF_

	g_cm.StopMIDI();
	g_cm.FreeMIDI();

#endif

	if (!szInputFile) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Il file di input non � specificato!"):TEXT("Invalid file"));
		return 0;
	}
	if (!lstrlen(szInputFile)) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Il file di input non � specificato!"):TEXT("Invalid file"));
		return 0;
	}
	if (!DestLevel) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Puntatore al livello non valido!"):TEXT("Invalid pointer"));
		return 0;
	}
	if (!(f=fopen(szInputFile,"rb"))) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Impossibile aprire il file %s "):TEXT("Unable to open file %s"), szInputFile);
		return 0;
	}

	//si assicura che non ci siano risorse ausiliarie in memoria
	FreeAuxResources();

	//resetta il livello
	DestLevel->Release();
    //elimina l'oggetto star field se allocato
	SAFE_DELETE(g_pStarField);

	//resetta la struttura
	memset(&sbl,0,sizeof(SBL_LEVEL));

	//legge il marcatore del file SBL
	memset (i,0,4);

	fread(i,4,1,f);

	if (0!=strcmp(i,"sbl")) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Il file %s non ha un formato valido!"):TEXT("File %s has not a valid format"), szInputFile);
		return -1; //file non valido
	}
	
	//l'intestazione del file � all'inizio
	fread(&sbl.head,sizeof(SBL_HEAD),1,f);

	//legge i chunk del file
	while (!feof(f))
	{	
		ichunk=0;
		fread(&ichunk,sizeof(int),1,f);

		if (!ichunk) break; //esce se il chunk � zero 8fine file)

		//si riporta prima dell'id del chunk
		//goes back 
		fseek(f,-(long)sizeof(int),SEEK_CUR);

		switch(ichunk)
		{
		case 1: //ground layer

			if (!pl) 
			{
				sbl.pgrnd_layers=new SBL_GROUND_LAYER;
				memset(sbl.pgrnd_layers,0,sizeof(SBL_GROUND_LAYER));
				pl=sbl.pgrnd_layers;
				pl->next=NULL;				
			}
			else
			{
				pl->next=new SBL_GROUND_LAYER;
			    pl=pl->next;
				memset(pl,0,sizeof(SBL_GROUND_LAYER));
			}		

			fread(pl,sizeof(SBL_GROUND_LAYER),1,f);

			//carica la mappa dei tiles
			//loads the tile map
			if (pl->inumtiles<0) 
			{
				fclose(f);
				return -2;
			}

			pl->pielems=NULL;
			pl->pielems=new int[pl->inumtiles];

			if (!pl->pielems) 
			{
				fclose(f);
				return -4;
			}

			//legge dal file la mappa dei tile per questo layer
			fread(pl->pielems,sizeof(int)*pl->inumtiles,1,f);
			
			igrndindex++;

			break;

		case 2: //back layer (layer del cielo)

			if (!pback)
			{
				//inizializza
				sbl.pback_layers=new SBL_BACK_LAYER;
				pback=sbl.pback_layers;
				memset(pback,0,sizeof(SBL_BACK_LAYER));
				pback->next=NULL;
			}
			else
			{
				pback->next=new SBL_BACK_LAYER;
				pback=pback->next;
				memset(pback,0,sizeof(SBL_BACK_LAYER));
			}

			//legge il back layer da file
			fread(pback,sizeof(SBL_BACK_LAYER),1,f);

			ibackindex++;		

			break;

		case 3://trigger

			if (!ptrigger)
			{
				sbl.ptriggers=new SBL_TRIGGER;
				ptrigger=sbl.ptriggers;
				memset(ptrigger,0,sizeof(SBL_TRIGGER));
				ptrigger->next=NULL;
			}
			else
			{
				ptrigger->next=new SBL_TRIGGER;
				ptrigger=ptrigger->next;
				memset(ptrigger,0,sizeof(SBL_TRIGGER));
			}			

			//legge il trigger dal file
			fread(ptrigger,sizeof(SBL_TRIGGER),1,f);

			//Caso particolare per gli oggetti vincolati al terreno
			//per gli oggetti vincolati al terreno deve cambiare l'asse di riferimento verticale che � rivolto in alto 
			//ed ha origine sul fondo dello schermo
			
			if (IsUpDown(ptrigger->ienemy)) ptrigger->yrel=g_Environ.iScreenHeight-ptrigger->yrel; 

			//aggiorna il numero di nemici per ogni tipo presente
			DestLevel->m_Stats[ptrigger->ienemy].iTot++;

			itriggercount++;

			break;

		case 4://call : chiamata di funzione

			if (!pfn)
			{
				sbl.pfunctions=new SBL_FUNCTION;
				pfn=sbl.pfunctions;
			}
			else
			{
				pfn->next=new SBL_FUNCTION;
				pfn=pfn->next;						
				
			}

			memset(pfn,0, sizeof(SBL_FUNCTION));
			pfn->next=NULL;
			pfn->parguments=NULL;
			pfn->name=NULL;			
			//legge il chunk			
			fread(&pfn->chunk_type,sizeof(pfn->chunk_type),1,f);				
			//legge il nome della funzione dal file
			ReadString(&pfn->name,f);				
			//numero di parametri
			fread(&pfn->nargs,sizeof(pfn->nargs),1,f);		
			
			pfn->parguments=new SB_VALUE_LIST;
			plist=pfn->parguments;
		
			for (cntargs=0;cntargs<pfn->nargs;cntargs++)
			{
				if(cntargs>0)
				{
					plist->next=new SB_VALUE_LIST;				
					plist=plist->next;
				}

				memset(plist,0,sizeof(SB_VALUE_LIST));
				//legge il valore dal file
				ReadString(&plist->value,f);							
			}
			
			break;

		default:

			//chunk non valido
			DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Il file %s contiene un chunk non valido.Ricompilare il livello!"):TEXT("Invalid chunk in %s.Invalid file format."), szInputFile);
			fclose(f);
			return -6;
		}

	}			

	fclose(f);

    //imposta il graphic manager corrente
	DestLevel->SetGraphicManager(&g_gm);
	//fram manager da usare per il blitting ecc...
	DestLevel->SetFrameManager(&g_fm);
	//crea il livello con i dati letti
	//da fare: se � impostata la seconda lingua selezionare il nome nella seconda lingua
	//nome del livello
	DestLevel->SetLevelName(sbl.head.szlevel_name);
	//messaggio finale
	DestLevel->SetFinalMessage(sbl.head.szfinal_message);  
	//velocit� di scrolling
	DestLevel->SetScrollSpeed((int)(sbl.head.scroll_velocity)); //1 pixel a ciclo per il layer piu' vicino	
	//password
	DestLevel->SetPassword(sbl.head.password);
	//imposta i membri statici
	CSbLevel::g_scroll_speed=DestLevel->GetScrollSpeed();
	//acc. di gravit�
	CSbLevel::g_gravity=sbl.head.gravity; 
	//livello terreno
	CSbLevel::g_bottom=sbl.head.ibottom;
	CSBObject::g_iScreenBottomHeight=sbl.head.ibottom;
	//imposta la larghezza e altezza del video in pixel
	DestLevel->SetDisplay(g_Environ.iScreenWidth,g_Environ.iScreenHeight);
    //imposta il numero di background layers
	DestLevel->SetNumScrollBackground((DWORD)ibackindex);
	
	g_fm.FreeImgFrame(&imgTemp);

	//estrae i layer del cielo (esempio: nuvole che scrollano)
	if (sbl.head.ibackindex>0)
	{
		if (FAILED(g_fm.CreateImgFrameFromVPX(&imgTemp,sbl.head.szvpxbackground,(DWORD)sbl.head.ibackindex))) 
		{

			DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Impossibile caricare l'immagine compressa con indice %d dal file %s"):TEXT("Unable to load compressed image with index %d from %s"),sbl.head.ibackindex,sbl.head.szvpxbackground);
			return -7;
		}
	}	
  
	pback=sbl.pback_layers;

	count=0;

	if (pback && imgTemp.status==0)
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Errore nel file di definizione livello %s: non � definita l'immagine dei layer di background"):TEXT("Background layer image is not defined in file %s"),szInputFile);
		return -7;
	}

	while(pback)
	{
		//imposta il background
	    if (FAILED(errcode=DestLevel->SetScrollBackground(count,pback->iyout,pback->fparallax,pback->ircleft,pback->irctop,pback->ircleft+pback->itile_width,pback->irctop+pback->itile_height,&imgTemp))) 
		{		
	    //	dbg.WriteErrorFile("ERRORE count=%d pback->iyout=%d fparallax=%f left=%d top=%d left+tilewidth=%d top+height=%d imgw=%d imgh=%d imgstate=%d",count,pback->iyout,pback->fparallax,pback->ircleft,pback->irctop,pback->ircleft+pback->itile_width,pback->irctop+pback->itile_height,imgTemp.width,imgTemp.height,imgTemp.status);
		
			DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Impossibile impostare il layer %d per lo scrolling del cielo (errore %d)"):TEXT("Unable to set the sky scrolling layer %d (error %d)"),count,errcode);		
			return -8;
		}	
		count++;
		pback=pback->next;
	}

	//rilascia l'immagine temporanea
	g_fm.FreeImgFrame(&imgTemp);

	//imposta i layer del terreno
	//imposta il numero di layers del terreno
	DestLevel->SetNumScrollLayers((DWORD)igrndindex);
	
	pl=sbl.pgrnd_layers;

	count=0;

	while(pl)
	{
		//imposta il layer
		//nota bene: la yout salvata nel file sbl � riferita al margine inferiore dello schermo
		if (FAILED(DestLevel->SetScrollLayer(count,g_Environ.iScreenHeight-pl->iyout,pl->fparallax,pl->ircleft,pl->irctop,pl->icols,pl->irows,pl->itile_width,pl->itile_height,(UINT *)pl->pielems,(DWORD)pl->inumtiles))) 
		{
			DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Impossibile impostare il layer %d per lo scrolling del terreno"):TEXT("Unable to set the ground scrolling layer %d"),count);			
			return -9;
		}
		count++;
		pl=pl->next;
	}

	pl=NULL;	

	m_lBackImageTop=sbl.head.iback_img_top; //offset y dell'immagine di back ground

	//immagine fissa di sfondo
	if (lstrlen(sbl.head.szvpxsky)>0)
	{
		//utilizza una immagine di sfondo
		if (FAILED(g_fm.CreateImgFrameFromVPX(&m_imgBackGround,sbl.head.szvpxsky,sbl.head.iskyindex)))
		{
			DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Impossibile caricare l'immagine statica di sfondo con indice %d dal file %s"):TEXT("Unable to load compressed image with index %d from %s"),sbl.head.iskyindex,sbl.head.szvpxsky);	
			return -10;			
		}

		//rettangolo di blitting dell'immagine di background (le coordinate di output sono sempre 0,0)
		m_rcBack.left=0;
		m_rcBack.top=0;
		m_rcBack.bottom=m_imgBackGround.height;
		m_rcBack.right=m_imgBackGround.width;
	}


	//funzione di disegno del background
	switch(sbl.head.iclear_procedure)
	{
	case 1:

		//imposta la funzione di background con colore uniforme (usata solo per il livello1)
		DestLevel->SetBackgroundClearFn(ClearBack1);   
		break;
		
	case 10:

		g_pStarField=new CStarField(CSBObject::g_iScreenWidth,CSBObject::g_iScreenHeight,1800,6);

		//funzione di ridisegno standard+star field
		//utilizza l'immagine di sfondo + effetto star field
		DestLevel->SetBackgroundClearFn(StarField);	
		break;

	default:

		//imposta la funzione standard di background (usata per tutti i livelli)
		//utilizza l'immagine di sfondo 
		DestLevel->SetBackgroundClearFn(DrawLevelBackGround);   			
		break;
	}

	//crea una sequenza di numeri casuali 
	fm.Randomize(1000);

	//triggers
	ptrigger=sbl.ptriggers;

	//imposta il resource manager
	g_rm.SetGraphicManager(&g_gm);
	g_rm.SetFrameManager(&g_fm);

	//aggiunge un trigger
	while(ptrigger)	{	

		//tiene conto della probabilit� che si verifichi il trigger
		if (ptrigger->iprob==0 || ptrigger->iprob==100 || (int)fm.RndFast(100)<ptrigger->iprob)
		{			
			if (!FAILED(LoadAuxResource(ptrigger->ienemy,ptrigger->iflags)))//carica la risorsa se non � stata ancora caricata
			{
				DestLevel->m_lpFm=&g_fm;
				DestLevel->CreateTrigger(ptrigger->xabs,ptrigger->yrel,ptrigger->ipower,ptrigger->ienergy,ptrigger->iflags,g_pEnemy[ptrigger->ienemy],g_pBonus[ptrigger->ibonus],ptrigger->zlayer);		
			}
			else 
			{
				resloadfailed++;

				//si ammette una certa tolleranza di errori di caricamento risorse per 
				//portabilit� su sistemi scarsi (win98 con poca RAM)
				if (resloadfailed>2)
				{
					DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Errore caricando la risorsa ausiliaria %s"):TEXT("Error loading aux resource %s"),TranslateLabel(ptrigger->ienemy));			
					return -12;
				}
				else
				{

					//scrive lo stesso l'errore sul file di output
					g_cdb.WriteErrorFile((g_Environ.iLanguage==0) ? TEXT("Errore caricando la risorsa ausiliaria %s (errore non bloccante l'oggetto non sar� visualizzato)"):TEXT("Error loading aux resource %s (non-blocking error)"),TranslateLabel(ptrigger->ienemy));
				}
			}
		}

		ptrigger=ptrigger->next; //trigger succesivo
	}

//Nota: quando si edita il livello vengono caricate tutte le risorse disponibili
#ifdef _LEVELEDIT_
	
	//carica le risorse necessarie al level editor
	for (count=1;count<=EN_LAST;count++)
	{
		LoadAuxResource(count,0); //carica tutte le risorse ausiliarie
	}

#endif

	g_rm.FreeResources();

	//imposta la lunghezza dello schema in pixel
	DestLevel->SetXLimit(DestLevel->GetMaxX()-g_Environ.iScreenWidth*2);
	
	//crea il livello  
	hr=DestLevel->CreateLevel(sbl.head.szvpxground_tiles,0,&g_objStack);    

	//errore creando il livello
	if (FAILED(hr)) 
	{
		DestLevel->SetLastError((g_Environ.iLanguage==0) ? TEXT("Errore creando il livello usando il file di tiles %s"):TEXT("Error creating level with tiles file %d"),sbl.head.szvpxground_tiles);
		return -1;
	}

	//esegue le funzioni definite nel livello
	pfn=sbl.pfunctions;

	while(pfn)
	{
		if (0==strcmp("SetTileProps",pfn->name))
		{
			//imposta le propriet� dei tile (tipo di terreno)
			if (pfn->nargs>=2)
			{
				plist=pfn->parguments;
				if (plist)
				{
					params[0]=atoi(plist->value); //tipo del terreno
					
					plist=plist->next;

					//applica la propriet� alla lista dei tiles
					while(plist)
					{
						DestLevel->SetGroundType(atoi(plist->value),params[0]);
						plist=plist->next;
					}
				}
			}
		}

		//estendere qui la lista dei tipi di funzione
		
		pfn=pfn->next;
	}	

	//musica di background


	if (lstrlen(sbl.head.szmusic)>0 && g_Environ.bMusic)
	{

#ifndef _MIDI_OFF_

		g_cm.LoadMIDI(sbl.head.szmusic,NULL);
		//inizia a suonare la musica di background
		g_cm.PlayMIDI(DMUS_SEG_REPEAT_INFINITE);

#endif

	}

	//rilascia la memoria allocata
	FreeSBL(&sbl);

#ifdef _DEBUG_
	_CrtCheckMemory();
#endif

	return 1;
}


//////////////////////////////////// ReadString /////////////////////////////////////////////////

//legge una stringa che termina con zero dal file
int ReadString(char **dest,FILE *f)
{
	int count=0;
	int ch=0;

	do
	{
		if (!feof(f))
		{
			ch=fgetc(f);
			count++;
		}

	} while (ch);

	//alloca la stringa
	*dest=new char[count];
	memset(*dest,0,sizeof(char)*count);

	fseek(f,-count,SEEK_CUR);
	return fread(*dest,sizeof(char)*count,1,f);

}

//////////////////////////////////////////////////////////////////////////////////////////////////

//Crea un livello 
//se szLevelFile � != NULL dwLevel viene ignorato e carica direttamente il livello in base al file 
//indicato.szLevel name contiene il path completo del file del livello esempio data\extra_levels\miolivello.sbl
HRESULT CreateSbLevel(DWORD dwLevel,CSbLevel *DestLevel,TCHAR *szLevelFile)
{

	//nota: vettore base 1 (il primo livello ha indice 1)
	static TCHAR *szLevels[]={
		
		TEXT(""), 
		TEXT("data\\lv1.sbl"),
        TEXT("data\\lv2.sbl"),
		TEXT("data\\lv3.sbl"),
		TEXT("data\\lv4.sbl"),
		TEXT("data\\lv5.sbl"),
		TEXT("data\\lv6.sbl"),
		TEXT("data\\lv7.sbl"),
		TEXT("data\\lv8.sbl")

	};

	//password dei livelli
	static int passwords[]={0,1,2,3,4,5,6,7,8};

	if (dwLevel>0 && dwLevel<=8 || szLevelFile)
	{	

		int ires;

		if (szLevelFile)
		{
			//livello "custom"
			ires=LoadSbLevel(szLevelFile,DestLevel);
		}
		else 
		{
			//livelli standard
			ires=LoadSbLevel(szLevels[dwLevel],DestLevel);
		}
		
		if (ires<=0) return E_FAIL;

		int pwd=DestLevel->GetPassword();

		if (pwd>0 && passwords[dwLevel]!=pwd)
		{
			//la password serve ad evitare che semplicemente rinominando il file del livello si 
			//possa accedere a qualsiasi livelo
			MsgOut(MB_ICONEXCLAMATION,"StarBlastrix",TranslateGameMsg(51,0));				
			return E_FAIL;
		}

		else return S_OK;
	}

	else return E_FAIL;

}

//////////////////////////////////////// GetJoyInput ////////////////////////////////////////////////
//Acquisisce l'input del giocatore dal joystick
void GetJoyInput(void)
{
	memset(&g_plInput,0,sizeof(PL_INPUT));
    //aggiorna lo stato del joystick
	g_ci.GetJoystickStatus();

	LONG res;
	
	res=g_ci.JoystickX();

	if (res>0) g_plInput.x_axis = Player1.iDisplacement;
	else if (res<0) g_plInput.x_axis = -Player1.iDisplacement;

	res=g_ci.JoystickY();

	if (res>0) g_plInput.y_axis = Player1.iDisplacement;
	else if (res<0) g_plInput.y_axis = -Player1.iDisplacement;

	//g_cdb.WriteErrorFile("jx=%d jy=%d",g_ci.JoystickX(),g_ci.JoystickY());

	//per il fuoco usa i primi tre tasti del joystick
	if (g_ci.JoystickButtons(0)) g_plInput.fire1=1;
	if (g_ci.JoystickButtons(1)) g_plInput.fire2=1;
    if (g_ci.JoystickButtons(2)) g_plInput.fire3=1;

	//aggiorna lo stato della tastiera
	g_ci.GetKybStatus();
	//anche se � impostato il joystick, per uscire si puo' usare esc o q
	if (g_ci.KeyDown(DIK_Q) || g_ci.KeyDown(DIK_ESCAPE)) 
	{	
		if (!(Player1.IsInvisible() && !Player1.IsBlinking()))
		{
			AskForExit();		//tasto Esc chiede di uscire dal gioco
		}
	}

#ifdef _TESTGAME_

		//DEBUG
		if (g_ci.KeyDown(DIK_D)) 
		{
			g_cdb.WriteErrorFile("---------------begin --------------------");
			DisplayStack(&g_objStack,TEXT("active stack"));
			//DisplayStack(&g_sDBomb1,TEXT("drop bomb1"));
			//DisplayStack(&g_sExplMedium,TEXT("exp medium"));
			DisplayStack(&g_sEShell5,TEXT("Weapon A"));
			g_bDebug1=!g_bDebug1;
		}

  		if (g_ci.KeyPress(DIK_P)) g_cdb.WriteErrorFile("posizione: %d",CCurLevel.GetCurrentX());

		if (g_ci.KeyDown(DIK_R)) g_objStack.DeleteAll(); //resetta lo stack

//attiva un flag di debug premendo T
#ifdef _TRACE_

		static BOOL bTFlag=FALSE;

		//trace
		if (g_ci.KeyDown(DIK_T) || g_ci.JoystickButtons(3))
		{
			if (!bTFlag)
			{
				bTFlag=TRUE;
				g_bTraceOn=!g_bTraceOn;
			}
		}
		else bTFlag=FALSE;



#endif
	

#endif

	//invia l'input del giocatore
	Player1.SendInput(&g_plInput);
	
}

/////////////////////////////////////// GetKybInput ///////////////////////////////////////////////

//Acquisisce l'input del giocatore dalla tastiera e lo invia all'oggetto Player1
void GetKybInput(void)
{
		
		memset(&g_plInput,0,sizeof(PL_INPUT));

		//aggiorna lo stato della tastiera
		g_ci.GetKybStatus();
	
		if (g_ci.KeyDown(g_Environ.iMoveUp)) g_plInput.y_axis =-Player1.iDisplacement;
		if (g_ci.KeyDown(g_Environ.iMoveDown)) g_plInput.y_axis =+Player1.iDisplacement;
		if (g_ci.KeyDown(g_Environ.iMoveLeft)) g_plInput.x_axis =-Player1.iDisplacement;
		if (g_ci.KeyDown(g_Environ.iMoveRight)) g_plInput.x_axis =+Player1.iDisplacement;
		if (g_ci.KeyDown(g_Environ.iFire1Key)) g_plInput.fire1=1; //fuoco primario
		if (g_ci.KeyDown(g_Environ.iFire2Key)) g_plInput.fire2=1; //fuoco secondario
		if (g_ci.KeyDown(g_Environ.iFire3Key)) g_plInput.fire3=1;
	
#ifdef _TESTGAME_

		//DEBUG
		if (g_ci.KeyDown(DIK_D)) 
		{
			DisplayStack(&g_objStack,TEXT("active stack"));
		//	DisplayStack(&g_sDBomb1,TEXT("drop bomb1"));
		//	DisplayStack(&g_sFwMissile1,TEXT("fw missile"));
		//	DisplayStack(&g_sEShell7,TEXT("weapon A"));
			DisplayStack(&g_sEShell5,TEXT("weapon A"));
		}

		if (g_ci.KeyDown(DIK_R)) g_objStack.DeleteAll(); //resetta lo stack

		if (g_ci.KeyPress(DIK_P)) g_cdb.WriteErrorFile("posizione: %d",CCurLevel.GetCurrentX());

		
#ifdef _TRACE_

		static BOOL bTFlag=FALSE;

		//trace
		if (g_ci.KeyDown(DIK_T))
		{
			if (!bTFlag)
			{
				bTFlag=TRUE;
				g_bTraceOn=!g_bTraceOn;
			}
		}
		else bTFlag=FALSE;


#endif

#endif

		if (g_ci.KeyDown(DIK_Q) || g_ci.KeyDown(DIK_ESCAPE)) 
		{	
			//durante la fase game over non si puo' uscire con escape
			if (!(Player1.IsInvisible() && !Player1.IsBlinking()))
			{
				AskForExit();		//tasto Esc chiede di uscire dal gioco
			}
		}
	
		//invia l'input del giocatore
		Player1.SendInput(&g_plInput);
}

//////////////////////////////////////////// SelectEditorObjct /////////////////////////////////////

//Seleziona un oggetto da inserire nella mappa
//lindex � l'indice dello stack degli oggetti come definito in CreateResHandler

void SelectEditorObject(LONG lindex)
{
	if (lindex<0) lindex=EN_LAST; //ultimo indice
	else if (lindex>EN_LAST) lindex=1; //ciclico

    g_lCurStackIndex=lindex;

	if (g_pobjEditor) 
	{
		g_pobjEditor->bActive=FALSE;
		g_pobjEditor->dwStatus=0;
	}

	if (g_lCurStackIndex>0 && g_pEnemy[g_lCurStackIndex])
	{

		//seleziona l'oggetto
		g_pobjEditor=g_pEnemy[g_lCurStackIndex]->Pop();
	}
}

/////////////////////////////////// DrawEditorSelectedObject ///////////////////////////////

//Disegna l'oggetto selezionato corrente alle coordinate del mouse
void DrawEditorSelectedObject(IMAGE_FRAME_PTR pimgDest)
{
		static BOOL bBtu=FALSE;
		BOOL bBtuCur=FALSE;
		DWORD dwEnergy,dwPower;

		//acquisisce lo stato del mouse
		g_ci.GetBufferedMouseState();
		
		if (g_bEditorSelMode)
		{
			//modalit� di selezione degli oggetti nell'editor
			DrawPointer(pimgDest,g_ci.MouseX(),g_ci.MouseY());

		}

		//g_pobjEditor � l'oggetto corrente da piazzare sulla mappa
		else if (g_pobjEditor)
		{		
			//posizionamento oggetti nell'editor
			g_pobjEditor->SetDisplay(pimgDest);
			//posiziona l'oggetto selezionato alle coordinate del mouse
			g_pobjEditor->SetPosition(g_ci.MouseX(),g_ci.MouseY());
			//disegna l'oggetto
			g_pobjEditor->Draw();	
		}
	
		bBtuCur=g_ci.MouseLeftButton();

		//controlla se deve inserire un trigger
		if (!bBtuCur && bBtu)
		{
			if (g_bEditorSelMode)
			{
				//siamo in modalit� di selezione oggetti
				//seleziona un oggetto esistente
				g_SelectedTrigger=CCurLevel.GetTriggerAt(g_ci.MouseX()+CCurLevel.GetCurrentX(),g_ci.MouseY());

			}
			else if (g_pobjEditor)
			{
				//acquisisce i valori di potere ed energia da associare per default a questo trigger 
				GetDefaultTriggerValues(g_lCurStackIndex,&dwEnergy,&dwPower);
				//crea un trigger
				CCurLevel.CreateTrigger(CCurLevel.GetCurrentX()+g_pobjEditor->x,g_pobjEditor->y,dwPower,dwEnergy,0,g_pEnemy[g_lCurStackIndex],NULL);
				CCurLevel.SortTriggers();
			}

		}

		bBtu=bBtuCur;	
}

/////////////////////////////////////////// GetDefaultTriggerValues /////////////////////////////////

void GetDefaultTriggerValues(int stack_index,DWORD *dwEnergy,DWORD *dwPower)
{
	switch(stack_index)
	{
	case EN_UFO:
		
		*dwPower=6;
		*dwEnergy=4;
		break;

    case EN_ANTIAIRCRAFT2:
	case EN_ANTIAIRCRAFT:

		*dwEnergy=18;
		*dwPower=10;
		break;

    case EN_SHIP:
		
		*dwEnergy=45;
		*dwPower=10;
		break;

	case EN_AIRCRAFTCARRIER:

		*dwEnergy=60;
		*dwPower=10;
		break;

	case EN_TANK:

		*dwEnergy=26;
		*dwPower=8;
		break;

	case EN_BLASTER:

		*dwEnergy=8;
		*dwPower=0;
		break;

	case EN_RADAR:

		*dwEnergy=10;
		*dwPower=8;
		break;

	case EN_SHANTY1:
	case EN_SHANTY2:
	case EN_SHANTY3:
	case EN_SHANTY4:
	case EN_STREET:
	case EN_PYLON:
	case EN_PALMS:
	case EN_WALL:

		*dwEnergy=20;
		*dwPower=8;
		break;	
	
	case EN_FACTORY:
			
		*dwEnergy=18;
		*dwPower=8;
		break;

	case EN_NIGHT_BUILDING1:
	case EN_NIGHT_BUILDING2:

		*dwEnergy=25;
		*dwPower=10;		
		break;

	case EN_NIGHT_BUILDING3:
	case EN_NIGHT_BUILDING4:

		*dwEnergy=38;
		*dwPower=12;
		break;


	case EN_NIGHT_BRIDGE:

		*dwEnergy=14;
		*dwPower=8;
		break;

    case EN_SCREW_FIGHTER:

		*dwEnergy=8;
		*dwPower=10;
		break;

	case EN_UFO3:

		*dwEnergy=10;
		*dwPower=10;
		break;

	case EN_LFIGHTER:

		*dwEnergy=10;
		*dwPower=10;
		break;

	case EN_ASTEROID_SMALL:

		*dwEnergy=4;
		*dwPower=8;
		break;

	case EN_ASTEROID_MEDIUM:

		*dwEnergy=18;
		*dwPower=12;
		break;

	case EN_ASTEROID_BIG:

		*dwEnergy=24;
		*dwPower=8;
		break;

	case EN_RAIL:

		*dwEnergy=1000;
		*dwPower=8;
		break;

	case EN_SPACE_BUILD1:

		*dwEnergy=60;
		*dwPower=8;
		break;

	case EN_SPACE_BUILD2:
		
		*dwEnergy=30;
		*dwPower=8;
		break;	

	case EN_SPACE_BUILD3:

		*dwEnergy=48;
		*dwPower=8;
		break;

	case EN_SPACE_BUILD4:

		*dwEnergy=40;
		*dwPower=10;
		break;

	case EN_TURRET:

		*dwEnergy=48;
		*dwPower=8;
		break;

	case EN_JELLYSHIP:

		*dwEnergy=200;
		*dwPower=10;
		break;

	case EN_SHIELD_BASE:
	case EN_SHELL_BASE:

		*dwEnergy=60;
		*dwPower=15;
		break;

	case EN_HTRUSS:
	case EN_VTRUSS:

		*dwEnergy=220;
		*dwPower=50;
		break;

	case EN_RING:

		*dwEnergy=20;
		*dwPower=20;
		break;

	case EN_MAMMOTH:

		*dwEnergy=15;
		*dwPower=10;
		break;

	case EN_BLEEZER:

		*dwEnergy=80;
		*dwPower=15;
		break;

	case EN_HFIGHTER:

		*dwEnergy=2;
		*dwPower=8;
		break;

	case EN_LIFTER:

		*dwEnergy=120;
		*dwPower=10;
		break;

	default:
		*dwEnergy=2;
		*dwPower=6;
		break;
	}

}

//--------------------------------------- WriteTriggers(FILE *fout)---------------------
//Scrive i triggers sul file di script del livello
void WriteTriggers(FILE *fout)
{
	SB_TRIGGER_PTR pTrigger=CCurLevel.GetTriggers();
	SB_TRIGGER_PTR pTopTrigger=CCurLevel.GetTopTrigger();
	
	int yt;
	int resid;
	int bonusid;

	if (pTrigger)
	{
		do
		{			
			resid=GetResId(pTrigger->pSrcStack);

			bonusid=GetResId(pTrigger->pBonusStack);

			//oggetti vincolati a terra che hanno l'asse y rivolto verso l'alto
			//NB. quando si aggiunge un elemento qui si deve aggiungere anche alla 
			//lista corrispondente nella funzione LoadSbLevel
			
		
			if (IsUpDown(resid)) yt=g_Environ.iScreenHeight-pTrigger->y; 
			else yt=pTrigger->y;				
			
			//scrive il layer,la prob. sempre impostata a 100 il bonus ecc...
			//tutti i parametri specificati
			if (pTrigger->zlayer>0)
			{	
				if (bonusid<=0)
				{
		    		fprintf(fout,"TRIGGER:%d,%d,%d,%d,%d,%s,%d,0,100,%d;\n",pTrigger->xabs,0,yt,pTrigger->dwPower,pTrigger->dwEnergy,TranslateLabel(resid),pTrigger->dwFlags,pTrigger->zlayer);				    		
				}
				else
				{
					fprintf(fout,"TRIGGER:%d,%d,%d,%d,%d,%s,%d,%s,100,%d;\n",pTrigger->xabs,0,yt,pTrigger->dwPower,pTrigger->dwEnergy,TranslateLabel(resid),pTrigger->dwFlags,TranslateLabel(bonusid),pTrigger->zlayer);				    
				}
			}
			else if (bonusid>0)
			{
				//flags e bonus
				//nota: non scrive la probabilit�
				fprintf(fout,"TRIGGER:%d,%d,%d,%d,%d,%s,%d,%s;\n",pTrigger->xabs,0,yt,pTrigger->dwPower,pTrigger->dwEnergy,TranslateLabel(resid),pTrigger->dwFlags,TranslateLabel(bonusid));				
            }
			else if (pTrigger->dwFlags>0)
			{
				//solo flags non bonus
				fprintf(fout,"TRIGGER:%d,%d,%d,%d,%d,%s,%d;\n",pTrigger->xabs,0,yt,pTrigger->dwPower,pTrigger->dwEnergy,TranslateLabel(resid),pTrigger->dwFlags);				
      
			}
			//no flags non bonus
			else fprintf(fout,"TRIGGER:%d,%d,%d,%d,%d,%s;\n",pTrigger->xabs,0,yt,pTrigger->dwPower,pTrigger->dwEnergy,TranslateLabel(resid));				
      

			if (pTrigger==pTopTrigger) break; //se c'� un solo trigger, significa che siamo alla fine della linked list

			pTrigger++; //passa al trigger successivo

		} while (pTrigger != pTopTrigger);

	}	

}

////////////////////////////////////////// GetKeybEditor ///////////////////////////////////////////
//gestione dell'input in modalit� level editor

void GetKeybEditor(void)
{

	static const int iDelta=6;
	BOOL bch=FALSE;
	LONG xcur=CCurLevel.GetCurrentX();  
	LONG iTile=-1;
	FILE *fout;
	int inum,ilayers,icount,icount1;

   	//aggiorna lo stato della tastiera
	g_ci.GetKybStatus(); 

    if (g_ci.KeyDown(DIK_LEFT)) {xcur-=iDelta;bch=TRUE;}
	
	if (g_ci.KeyDown(DIK_RIGHT)) 
	{xcur += iDelta;
	bch=TRUE;
	}

	//spostamento veloce
	if (g_ci.KeyDown(DIK_J)) {xcur+=640;bch=TRUE;}
	if (g_ci.KeyDown(DIK_H)) {xcur-=640;bch=TRUE;}

	if (g_ci.KeyDown(DIK_F)) 
	{
		xcur=g_lLimit;bch=TRUE;
	}

	if (g_ci.KeyPress(DIK_UP)) {g_dwLayerEditor++;bch=TRUE;} //passa al layer superiore
	if (g_ci.KeyPress(DIK_DOWN)) {g_dwLayerEditor--;bch=TRUE;} //passa al layer inferiore
	if (g_ci.KeyDown(DIK_ESCAPE)) 
	{			
			AskForExit();		//tasto Esc chiede di uscire dal gioco
	}

	if (g_ci.KeyPress(DIK_0)) {iTile=0;bch=TRUE;}
	if (g_ci.KeyPress(DIK_1)) {iTile=1;bch=TRUE;}
	if (g_ci.KeyPress(DIK_2)) {iTile=2;bch=TRUE;}
	if (g_ci.KeyPress(DIK_3)) {iTile=3;bch=TRUE;}
	if (g_ci.KeyPress(DIK_4)) {iTile=4;bch=TRUE;}
	if (g_ci.KeyPress(DIK_5)) {iTile=5;bch=TRUE;}
	if (g_ci.KeyPress(DIK_6)) {iTile=6;bch=TRUE;}
	if (g_ci.KeyPress(DIK_7)) {iTile=7;bch=TRUE;}
	if (g_ci.KeyPress(DIK_8)) {iTile=8;bch=TRUE;}
	if (g_ci.KeyPress(DIK_9)) {iTile=9;bch=TRUE;}
	if (g_ci.KeyPress(DIK_Q)) {iTile=10;bch=TRUE;}
	if (g_ci.KeyPress(DIK_W)) {iTile=11;bch=TRUE;}
	if (g_ci.KeyPress(DIK_E)) {iTile=12;bch=TRUE;}
	if (g_ci.KeyPress(DIK_R)) {iTile=13;bch=TRUE;}
	if (g_ci.KeyPress(DIK_T)) {iTile=14;bch=TRUE;}
	if (g_ci.KeyPress(DIK_Y)) {iTile=15;bch=TRUE;}
	if (g_ci.KeyPress(DIK_U)) {iTile=16;bch=TRUE;}
	if (g_ci.KeyPress(DIK_I)) {iTile=17;bch=TRUE;}
	if (g_ci.KeyPress(DIK_O)) {iTile=18;bch=TRUE;}
	if (g_ci.KeyPress(DIK_P)) {iTile=19;bch=TRUE;}
	if (g_ci.KeyPress(DIK_INSERT)) 	{

		g_bEditorSelMode=!g_bEditorSelMode;
		g_SelectedTrigger=NULL; //nessun trigger selezionato

	} //cambia la modalit� di selezione degli oggetti nell'editor
	if (g_ci.KeyPress(DIK_DELETE) && g_bEditorSelMode && g_SelectedTrigger)
	{
		CCurLevel.RemoveTrigger(g_SelectedTrigger);
	}

	//selezione oggetti da collocare sulla mappa
	if (g_ci.KeyPress(DIK_LBRACKET)) {SelectEditorObject(--g_lCurStackIndex);}
    else if (g_ci.KeyPress(DIK_RBRACKET)) {SelectEditorObject(++g_lCurStackIndex);}
	
	//premendo s salva la mappa dei tiles su un file di testo
	if (g_ci.KeyPress(DIK_S)) 
	{
		//elimina map.txt se esiste gi�
		unlink(TEXT("map.txt"));

		if (fout=fopen(TEXT("map.txt"),"wt"))
		{

			ilayers=CCurLevel.GetNumScrollLayers();

			for (icount=0;icount<ilayers;icount++)
			{

				fprintf(fout,"Layer %d\n",icount);

				//numero di tiles nel layer
				inum=CCurLevel.GetTilesCount(icount);

				for (icount1=0;icount1<inum;icount1++)
				{
					//scrive sul file i dati formattati
					fprintf(fout,"%d,",CCurLevel.GetTile(icount,icount1));
				}

				fprintf(fout,"\n");
			}

			//scrive i triggers
			fprintf(fout,"#Elenco dei triggers:\n");
			fprintf(fout,"#TRIGGER xabs,xrel,yrel,ipower,ienergy,ienemy,iflags,ibonus,iprob,zlayer\n");
            
			WriteTriggers(fout);
		
			fclose(fout);

		}
	}


	if (bch)
	{
		if (xcur<0) xcur=0;
		else if(xcur>g_lLimit) xcur=g_lLimit;
		if (g_dwLayerEditor>=MAX_SCROLL_LAYERS) g_dwLayerEditor=MAX_SCROLL_LAYERS-1;
		else if (g_dwLayerEditor<0) g_dwLayerEditor=0;
		CCurLevel.SetPosition(xcur); //cambia la posizione del livello

		if (iTile>=0) CCurLevel.SetTile(iTile,CCurLevel.GetCurrentTileIndex(g_dwLayerEditor)+1,g_dwLayerEditor);
    
		UpdateStatusBar();

	}	
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
//chiede se si vuol continuare
BOOL Continue(void)
{
	CImgBuffer16 cbuff,destbuff;

	WORD *pw=NULL;    
	
	//cattura lo schermo corrente
	if (FAILED(g_fm.CaptureScreen(&m_imgCurScreen))) return FALSE;
		
	m_frmContinue.ShowMouse();	

	m_frmContinue.Open();

	while(m_frmContinue.IsActive())
	{
		//attiva la form
		m_frmContinue.Listen();
	}

	g_fm.FreeImgFrame(&m_imgCurScreen);

	return TRUE;

}
////////////////////////////////////////////////////////////////////////////////////////////////////////
//Interrompe il gioco e chide se si vuol uscire
BOOL AskForExit(void)
{

	CImgBuffer16 cbuff,destbuff;
	IMAGE_FRAME imgDark;

	WORD *pw=NULL;
    
	//cattura lo schermo corrente
	if (FAILED(g_fm.CaptureScreen(&m_imgCurScreen))) return FALSE;
	
	g_fm.CreateFrameSurface(&imgDark,SCREEN_WIDTH,SCREEN_HEIGHT);
	//crea una superficie scura
	g_gm.Cls(g_gm.CreateRGB(212,60,2),&imgDark);

	LinkBuffer(&destbuff,&m_imgCurScreen);
	
	LinkBuffer(&cbuff,&imgDark);
    
	//imposta il formato pixel prima di eseguire il blending
	cbuff.SetPixelFormat(&g_gm.GetPixelFormat());
	
	cbuff.SetDestBuffer(&destbuff);

	cbuff.SetAlphaBlending(3);
	
	cbuff.BltAlphaBlend(0,0);

	g_fm.ReleaseBuffer(&imgDark);

	g_fm.ReleaseBuffer(&m_imgCurScreen);

	g_fm.FreeImgFrame(&imgDark);			

	m_frmExit.MoveTo(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);

	m_frmExit.ShowMouse();	

	m_frmExit.Open();

	while(m_frmExit.IsActive())
	{
		//attiva la form
		m_frmExit.Listen();
	}

	g_fm.FreeImgFrame(&m_imgCurScreen);
//	delete [] g_wBuffer;

	return TRUE;
}


//disegna il background quando si visualizza un menu
void DrawBackGround(IMAGE_FRAME_PTR img)
{
	POINT p;

	p.x=0;
	p.y=0;

#ifdef _DEBUG_

	::ClientToScreen(g_hwnd,&p);

#endif
	
	//imgScreen[1] ha un clipper
	if (m_imgCurScreen.status>0) 
	{
		g_fm.PutImgFrameRect(p.x,p.y,img,&m_imgCurScreen,&g_rcScreen,0);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void PlFire1(LONG x,LONG y)
{
	CSBObject *pobj=NULL;
    
	pobj=g_sShell.Pop(); //prende un proiettile dalla collezione
	//se non ci sono oggetti disponibili nella collezione rende NULL
	
	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=2;	
		pobj->SetVelocity(0,11);
		pobj->SetPosition(x+30,y+19); //imposta la pos. iniziale
		g_objStack.Push(pobj);	 //immette il proiettile nella coda degli oggetti attivi
		g_cs.PlaySound(g_snFire1,0,0); //effetto sonoro

	}
	
}

//Seconda funzione per il fuoco del player
void PlFire2(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sShell1.Pop(); //prende il proiettile dal secondo contenitore
	
	if (pobj)
	{	
		pobj->SetInitialEnergy(1);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19); //imposta la pos. iniziale
		pobj->SetVelocity(0,11);
		pobj->dwPower=4;		
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snFire1,0,0); //effetto sonoro		
		UpdateStatusBar(); //aggiorna la status bar	
	}
	
}


//Seconda funzione per il fuoco del player
void PlFire3(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sShell2.Pop(); //prende il proiettile dal secondo contenitore
	
	if (pobj)
	{	
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19); //imposta la pos. iniziale
		pobj->dwPower=6;
		pobj->SetInitialEnergy(2);
		pobj->Reset();
		pobj->SetVelocity(0,12);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snFire1_1,0,0); //effetto sonoro		
		UpdateStatusBar(); //aggiorna la status bar	
	}
	
}

void PlFire4(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sShell3.Pop(); //proiettili gialli orizzontali

	if (pobj)
	{
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19); //imposta la pos. iniziale
		pobj->dwPower=8;
		pobj->SetVelocity(0,12);
		pobj->SetInitialEnergy(3);	
		pobj->Reset();
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snFire1_2,0,0); //effetto sonoro		
		UpdateStatusBar(); 
	}

}

//proiettili rossi orizzontali
void PlFire5(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sShell4.Pop();

	if (pobj)
	{
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19);
		pobj->dwPower=12;
		pobj->SetInitialEnergy(9);
		pobj->Reset();
		pobj->SetVelocity(0,13);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snPlLaser,0,0);
		UpdateStatusBar();
	}
}


//thunder
void PlFire6(LONG x,LONG y)
{
	CThunderBolt *pTh;
	CSBObject **pList;
	int icnt=0;
	
	pTh=(CThunderBolt *)g_sThunderBolt.Pop();

	if (pTh)
	{
		pTh->Reset();
		pTh->AddTargetXY(320,400);
		pTh->SetSource((CSBObject *)&Player1);
		pTh->dwClass=CL_PLFIRE;
		pTh->SetDuration(16);

		pList=g_objStack.m_objStack;

		for (int count=0;count<MAX_OBJECTS;count++)
		{
			if (pList[count])
			{
				//non inserisce nella lista gli oggetti fermi a terra tipo palazzi
				if (pList[count]->dwClass==CL_ENEMY && pList[count]->GetType()!=11)
				{
					pTh->AddTarget(pList[count]);
					icnt++;
				}
			}

			if (icnt>=3) break;
		}

		g_cs.PlaySound(g_snFire10,0,0);
		g_objStack.Push(pTh);
		UpdateStatusBar();

	}
}

//laser
void PlFire7(LONG x,LONG y)
{
	CLaser *pL=NULL;

	pL=(CLaser *)g_sLaser.Pop();

	if (pL)
	{
		pL->Reset();
		pL->dwClass=CL_PLFIRE;
		pL->SetPosition(x+30,y+16);
		pL->SetEnergy(6);
		pL->dwPower=40;
		g_objStack.Push(pL);
		g_cs.PlaySound(g_snFire4,0,0);
		UpdateStatusBar();
	}
}


//acquisisce dei target pr il player cercandoli nella lista dei nemici
int GetTargets(CSBObject *pTargets[],int inum)
{
	if (!pTargets || inum<=0) return 0;
	int icnt=0;
	CSBObject **pList;

	for (icnt=0;icnt<inum;icnt++) pTargets[icnt]=NULL;
	icnt=0;

	//punta allo stack degli oggetti ativi
	pList=g_objStack.m_objStack;

	for (int count=0;count<MAX_OBJECTS;count++)
	{
		if (pList[count])
		{
			if (pList[count]->dwClass==CL_ENEMY)
			{
				pTargets[icnt++]=pList[count];			
			}
		}

		if (icnt>=inum) break;
	}

	//se non ci sono abbastanza nemici allora ...
	for (count=1;count<inum;count++) if (!pTargets[count]) pTargets[count]=pTargets[count-1];

	return icnt;
}


//spara un gruppo di missili inseguitori contro i bersagli nemici
void PlFire8(LONG x,LONG y)
{
	CFwMissile *pobj;
	CSBObject **pList;
	CSBObject *pTargets[6];
	int icnt=0;

	pTargets[0]=NULL;
	pTargets[1]=NULL;
	pTargets[2]=NULL;
	pTargets[3]=NULL;
	pTargets[4]=NULL;
	pTargets[5]=NULL;

	//punta allo stack degli oggetti ativi
	pList=g_objStack.m_objStack;

	//acquisisce i primi 6 oggetti nemici
	for (int count=0;count<MAX_OBJECTS;count++)
	{
		if (pList[count])
		{
			if (pList[count]->dwClass==CL_ENEMY)
			{
				pTargets[icnt]=pList[count];
				//strae un missile dallo stack
				pobj=(CFwMissile *)g_sFwMissile1.Pop();

				if (pobj)
				{	
					pobj->Reset();
					pobj->dwClass=CL_PLFIRE;
					pobj->SetCurFrame(0);
					pobj->SetPosition(x+20,y+15);
					pobj->SetVelocity(0,5);
					//imposta il beraglio da inseguire
					pobj->SetTarget(pTargets[icnt]); 
					g_objStack.Push(pobj);
					g_cs.PlaySound(g_snFwMissile,0,0);
					icnt++;
				}
			}
		}

		if (icnt>=6) break;
	}

	if (!icnt)
	{
		//se non ci sono nemici in vista spara a caso
		pobj=(CFwMissile *)g_sFwMissile1.Pop();

		if (pobj)
		{	
			pobj->Reset();
			pobj->dwClass=CL_PLFIRE;
			pobj->SetCurFrame(0);
			pobj->SetPosition(x+20,y+15);
			pobj->SetVelocity(0,5);		
			g_objStack.Push(pobj);
			g_cs.PlaySound(g_snFwMissile,0,0);
			icnt++;
		}
	}

	UpdateStatusBar();
}

//spara una rosa di proiettili (WeaponA)
void PlFire9(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	for (int icnt=0;icnt<24;icnt++)
	{
		pobj=g_sEShell7.Pop();

		if (pobj)
		{
			pobj->SetInitialEnergy(3);
			pobj->Reset();
			pobj->dwClass=CL_PLFIRE;
			pobj->dwPower=5;					
			pobj->SetCurFrame(0);
			pobj->SetPosition(x+20,y+15);
			pobj->SetVelocity(15*icnt,10);
			g_objStack.Push(pobj);
			g_cs.PlaySound(g_snMagnetic,0,0);
		}

	}

	UpdateStatusBar();

}

//tre proietili rossi
void PlFire10(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sEShell4.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=15;		
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(0,12);
		g_objStack.Push(pobj);
	}

	pobj=g_sEShell4.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=15;	
		pobj->SetCurFrame(1);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(15,12);
		g_objStack.Push(pobj);
	}

	pobj=g_sEShell4.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=15;	
		pobj->SetCurFrame(23);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(345,12);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snFire11,0,0);
	UpdateStatusBar();

}

//falce
void PlFire11(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	g_cs.PlaySound(g_snFire11,0,0);

	pobj=g_sEShell9.Pop();

	if (pobj)
	{
	
		pobj->SetInitialEnergy(1);
		pobj->Reset();	
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(0,20);
		g_objStack.Push(pobj);
	}

	pobj=g_sEShell8.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();	
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(180,20);
		g_objStack.Push(pobj);
	}

	pobj=g_sEShell6.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();	
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(90,9);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snBlip,0,0);
	}

	pobj=g_sEShell6.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();		
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(80,9);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snBlip,0,0);
	}

	pobj=g_sEShell6.Pop();

	if (pobj)	
	{
			pobj->SetInitialEnergy(1);
		pobj->Reset();	
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(70,9);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snBlip,0,0);
	}

	pobj=g_sEShell6.Pop();

	if (pobj)
	{
			pobj->SetInitialEnergy(1);
		pobj->Reset();	
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(60,9);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snBlip,0,0);
	}

	pobj=g_sEShell6.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(1);
		pobj->Reset();		
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(50,9);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snBlip,0,0);
	}

	UpdateStatusBar();
}

void PlFire12(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sEShell10.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetInitialEnergy(1);
		pobj->dwClass=CL_PLFIRE;
		pobj->dwPower=20;
		pobj->SetAnimSpeed(0.4f);
		pobj->SetCurFrame(0);
		pobj->SetPosition(x+20,y+15);
		pobj->SetVelocity(0,13);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snFire11,0,0);

	}
}


//proiettile blue
void PlFire13(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sShellBlue.Pop();

	if (pobj)
	{
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19);
		pobj->dwPower=15;
		pobj->SetInitialEnergy(5);
		pobj->Reset();
		pobj->SetVelocity(0,14);
		g_objStack.Push(pobj);
		g_cs.PlaySound(g_snFire1_2,0,0);		
	}

}

//tre p laser avanti indietro su e giu'
void PlFire14(LONG x,LONG y)
{
	CSBObject* pobj=NULL;
	
	//avanti 
	pobj=g_sShellBlue.Pop();

	if (pobj)
	{
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19);
		pobj->dwPower=15;
		pobj->SetInitialEnergy(5);
		pobj->Reset();
		pobj->SetVelocity(0,14);
		g_objStack.Push(pobj);	
		
	}

	//indietro
	pobj=g_sShellBlue.Pop();

	if (pobj)
	{
		pobj->dwClass=CL_PLFIRE;
		pobj->SetPosition(x+30,y+19);
		pobj->dwPower=15;
		pobj->SetInitialEnergy(5);
		pobj->Reset();
		pobj->SetVelocity(180,14);
		g_objStack.Push(pobj);	

		g_cs.PlaySound(g_snFire1_1,0,0);
		
	}

	//in alto
	pobj=g_sEShell11.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(5);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;				
		pobj->dwPower=15;			
		pobj->SetCurFrame(6);
		pobj->SetPosition(x+20,y-8);
		pobj->SetVelocity(270,13);
		g_objStack.Push(pobj);			
	}

	//in basso
	pobj=g_sEShell11.Pop();

	if (pobj)
	{
		pobj->SetInitialEnergy(5);
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;				
		pobj->dwPower=15;			
		pobj->SetCurFrame(6);
		pobj->SetPosition(x+20,y-8);
		pobj->SetVelocity(90,13);
		g_objStack.Push(pobj);			
	}
	UpdateStatusBar();
}

//fuoco verso il basso
void PlFireGrnd(LONG x,LONG y)
{
	CShell *pshell;

	pshell=(CShell*)g_sEShell5.Pop();

	if (pshell)
	{		
		pshell->dwClass=CL_PLFIRE;
		pshell->SetInitialEnergy(1);
		pshell->Reset();
		pshell->dwPower=6;
		pshell->SetVelocity(45,12);
		pshell->SetPosition(x,y);
		pshell->SetCurFrame(9);
		g_objStack.Push(pshell);
		g_cs.PlaySound(g_snFire1,0,0); //effetto sonoro		

	}

}

//proiettili inseguitori
void PlFireFw(LONG x,LONG y)
{	
	CSBObject *targets[4];

	CFwBullet *p=(CFwBullet *)g_sFwBullet.Pop();

	//acquisisce 5 bersagli 
	GetTargets(targets,4);

	if (p)
	{
		p->Reset();
		p->SetPosition(x+10,y+8);
		p->SetVelocity(225,4);
		p->SetTarget(targets[0]);	
		p->SetTTL(120); //time to live
		p->dwClass=CL_PLFIRE;
		p->SetEnergy(1);
		p->SetUpdateFreq(4);
		p->dwPower=10;
		g_objStack.Push(p);
	}

	p=(CFwBullet *)g_sFwBullet.Pop();

	if (p)
	{
		p->Reset();
		p->SetPosition(x+10,y+8);
		p->SetVelocity(135,4);
		p->SetTarget(targets[1]);	
		p->SetTTL(120); //time to live
		p->dwClass=CL_PLFIRE;
		p->SetEnergy(1);
		p->SetUpdateFreq(4);
		p->dwPower=10;
		g_objStack.Push(p);
	}

	p=(CFwBullet *)g_sFwBullet.Pop();

	if (p)
	{
		p->Reset();
		p->SetPosition(x+10,y+8);
		p->SetVelocity(45,4);
		p->SetTarget(targets[2]);	
		p->SetTTL(120); //time to live
		p->dwClass=CL_PLFIRE;
		p->SetEnergy(1);
		p->SetUpdateFreq(4);
		p->dwPower=10;
		g_objStack.Push(p);
	}

	p=(CFwBullet *)g_sFwBullet.Pop();

	if (p)
	{
		p->Reset();
		p->SetPosition(x+10,y+8);
		p->SetVelocity(315,4);
		p->SetTarget(targets[3]);	
		p->SetTTL(120); //time to live
		p->dwClass=CL_PLFIRE;
		p->SetEnergy(1);
		p->SetUpdateFreq(4);
		p->dwPower=10;
		g_objStack.Push(p);
	}
	UpdateStatusBar();

}

void PlTripleFire(LONG x,LONG y)
{
	CShell *pshell;

	pshell=(CShell*)g_sEShell5.Pop();

	if (pshell)
	{		
		pshell->dwClass=CL_PLFIRE;
		pshell->SetInitialEnergy(1);
		pshell->Reset();
		pshell->dwPower=6;
		pshell->SetVelocity(45,12);
		pshell->SetPosition(x+10,y+8);
		pshell->SetCurFrame(9);
		g_objStack.Push(pshell);	

	}

	
	pshell=(CShell*)g_sEShell5.Pop();

	if (pshell)
	{		
		pshell->dwClass=CL_PLFIRE;
		pshell->SetInitialEnergy(1);
		pshell->Reset();
		pshell->dwPower=6;
		pshell->SetVelocity(315,12);
		pshell->SetPosition(x+10,y+8);
		pshell->SetCurFrame(3);
		g_objStack.Push(pshell);		
	}


	g_cs.PlaySound(g_snFire1,0,0); //effetto sonoro	


}

void PlFireMissile(LONG x,LONG y)
{
	CMissile *pmis=NULL;

	pmis=(CMissile*)g_sSMissile.Pop();

	if (pmis)
	{
		pmis->SetVelocity(0,7);
		pmis->SetInitialEnergy(5);
		pmis->dwPower=8;
		pmis->SetFlags(0);
		pmis->Reset();
		pmis->dwClass=CL_PLFIRE;
		pmis->SetCurFrame(0);
		pmis->SetPosition(x,y);
		g_objStack.Push(pmis);
		g_cs.PlaySound(g_snMissile,0,0);

	}

}

//(Player) sgancia una bomba tipo 1
void PlDropBomb1(LONG x,LONG y)
{
	CDropBomb *pobj=NULL;

	if (g_bDebug1)
	{
		g_bDebug1=TRUE;
	}

	pobj=(CDropBomb *)g_sDBomb1.Pop(); //prende una bomba dalla collezione

	if (pobj)
	{
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->SetCurFrame(0); //frame iniziale
		pobj->SetPosition(x+19,y+16);
		pobj->dwPower=12;	
		pobj->SetType(50);
		pobj->SetEnergy(1);		

		if (CSbLevel::g_gravity>0)
		{
			pobj->SetVelocity(90,3); //cade verticalmente a 3 pixel/ciclo 
			
		}
		else 
		{
			//in assenza di gravit�
				
			pobj->SetVelocity(45,3); //bomba ferma

		}

		g_objStack.Push(pobj); //immette la bomba nello stack degli oggetti attivi		
       	g_cs.PlaySound(g_snDrop,0,0); //effetto sonoro	
		UpdateStatusBar(); //aggiorna la status bar	
	}
}


//(Player) sgancia una bomba tipo 2
void PlDropBomb2(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sDBomb2.Pop(); //prende una bomba dalla collezione

	if (pobj)
	{
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->SetCurFrame(0); //frame iniziale
		pobj->SetPosition(x+19,y+16);
		pobj->dwPower=18;
		pobj->SetEnergy(3);
		pobj->SetInitialEnergy(3);
		if (CSbLevel::g_gravity>0)
		{
			pobj->SetVelocity(90,3); //cade verticalmente a 3 pixel/ciclo 
		
		}
		else 
		{
			//in assenza di gravit�
				
			pobj->SetVelocity(45,3); //bomba ferma

		}
		g_objStack.Push(pobj); //immette la bomba nello stack degli oggetti attivi		
       	g_cs.PlaySound(g_snDrop,0,0); //effetto sonoro	
		UpdateStatusBar(); //aggiorna la status bar	
	}	

}

//(Player) sgancia una bomba tipo 3
void PlDropBomb3(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sDBomb3.Pop(); //prende una bomba dalla collezione

	if (pobj)
	{
		pobj->Reset();
		pobj->dwClass=CL_PLFIRE;
		pobj->SetCurFrame(0); //frame iniziale
		pobj->SetPosition(x+19,y+16);
		pobj->dwPower=28;
		pobj->SetEnergy(7);
		if (CSbLevel::g_gravity>0)
		{
			pobj->SetVelocity(90,3); //cade verticalmente a 3 pixel/ciclo 
		
		}
		else 
		{
			//in assenza di gravit�
			
			pobj->SetVelocity(45,3); //bomba ferma

		}
		g_objStack.Push(pobj); //immette la bomba nello stack degli oggetti attivi		
       	g_cs.PlaySound(g_snDrop,0,0); //effetto sonoro	
		UpdateStatusBar(); //aggiorna la status bar	
	}	

}

////////////////////////////////// GameOver /////////////////////////////////////////////////////////
//Immette un oggetto game over nello stack
void GameOver(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	UpdateStatusBar();

	//estrae un oggetto dallo stack game over...
	pobj=g_sGameOver.Pop();
	if (pobj)
	{
		pobj->Reset();

		//...e lo immette nello stack degli oggetti attivi
		g_objStack.Push(pobj);
	}
}



/////////////////////////////// PushDebris //////////////////////////////////////////////////

void PushDebris1(LONG x,LONG y)
{
	CDebris *pobj=NULL;
	
	pobj=(CDebris *)g_sDebris1.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(230,12);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}
	
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(260,11);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}
	
	pobj=(CDebris *)g_sDebris4.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(250,17);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

}

///////////////////////////////// PushExpl1 /////////////////////////////////////////////////////////

void PushExpl1(LONG x,LONG y)
{
	CExplosion *pobj=NULL;
    //type cast per poter usare la funzione SetTrigger
	//prende un oggetto dalla collezione di esplosioni tipo 1
	pobj=(CExplosion *)g_sExplSmall.Pop(); 

	if (pobj)
	{
		pobj->SetCurFrame(0);
		pobj->SetPosition(x,y);
		pobj->SetTrigger(4);
		//la velocit� deve essere opposta a quella di avanzamento dello schema
		pobj->SetVelocity(0,-CSbLevel::g_scroll_speed); 
		//imposta il suono e il ritardo
		pobj->Reset(g_snExplo1,0);
		g_objStack.Push(pobj); //immette l'oggetto nello stack degli oggetti attivi
	}

	if (y>CSBObject::g_iScreenHeight-70) GrndImpact(x,y);
	
}

////////////////////////////// PushExpl2 //////////////////////////////////////////////////////
//esplosione con detriti
void PushExpl2(LONG x,LONG y)
{
	PushExpl4(x,y);
	PushDebris1(x,y);
}

///////////////////////////// PushExpl3 /////////////////////////////////////////////////////
//explosione a terra
void PushExpl3(LONG x,LONG y)
{
	CDebris *pobj=NULL;

	PushExpl7(x-10,y);
	PushExpl7(x+10,y);

	//detriti
	pobj=(CDebris *)g_sDebris1.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(244,2);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//detriti
	pobj=(CDebris *)g_sDebris2.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(270,4);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//detriti
	pobj=(CDebris *)g_sDebris1.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(270,12);
		pobj->Reset();	
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}
	 
    //detriti
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(296,12);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	 //detriti
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(278,22);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	 //detriti
	pobj=(CDebris *)g_sDebris6.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(261,16);		
		pobj->Reset();	
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//detriti
	pobj=(CDebris *)g_sDebris4.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(3200,12);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//fumo
	PushSmoke(x-16,y-16);
	PushSmoke(x,y);
	PushSmoke(x+16,y+16);
}





///////////////////////////////// PushExpl4 /////////////////////////////////////////////////////////////
//esplosione media
void PushExpl4(LONG x,LONG y)
{
	CExplosion *pobj=NULL;

	if (y>g_Environ.iScreenHeight-70)
	{
		if (LV_LIQUID==GrndImpact(x,y)) return; //in questo caso la bomba cade in acqua senza esplodere

	}
	
	pobj=(CExplosion *)g_sExplMedium.Pop();
	
	if (pobj)
	{
		pobj->SetCurFrame(0);
		pobj->SetPosition(x,y);
		pobj->SetTrigger(4);
		pobj->SetVelocity(0,-CSbLevel::g_scroll_speed);
		pobj->Reset(g_snExplo2,0);
		g_objStack.Push(pobj);	

	}
}

///////////////////////////// PushExpl5 /////////////////////////////////////////////////////
//explosione a terra
void PushExpl5(LONG x,LONG y)
{
	CDebris *pobj=NULL;
	
	PushExpl8(x,y);	

	for (int count=0;count<6;count++)
	{
		//detriti
		pobj=(CDebris *)g_sDebris6.Pop();
		//si deve prima impostare la velocit� e poi resettare
		if (pobj) 
		{
			pobj->SetPosition(x,y);
			pobj->SetVelocity(240+count*20,22);
			pobj->Reset();
			pobj->SetG(CSbLevel::g_gravity);
			g_objStack.Push(pobj); //immette nello stack di oggetti attivi
		}

	}

	//detriti
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(198,12);
		pobj->Reset();		
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}


	 
    //detriti
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(296,12);
		pobj->Reset();		
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	

	 //detriti
	pobj=(CDebris *)g_sDebris3.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(278,22);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	for (count=0;count<3;count++)
	{
		//detriti
		pobj=(CDebris *)g_sDebris7.Pop();
		//si deve prima impostare la velocit� e poi resettare
		if (pobj) 
		{
			pobj->SetPosition(x,y);
			pobj->SetVelocity(190+count*20,12);
			pobj->Reset();			
			g_objStack.Push(pobj); //immette nello stack di oggetti attivi
		}

	}

	//fumo
	PushSmoke(x-16,y-16);	


}

///////////////////////////////////PushExpl6 //////////////////////////////////////////

//esplosione a terra con fiamme

void PushExpl6(LONG x,LONG y)
{
		CDebris *pobj=NULL;

	PushExpl7(x-10,y);
	PushExpl4(x+10,y);
	PushExpl7(x+30,y);

	//detriti
	pobj=(CDebris *)g_sDebris1.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(244,2);
		pobj->Reset();
		pobj->SetG(CSbLevel::g_gravity);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//detriti
	pobj=(CDebris *)g_sDebris2.Pop();
	//si deve prima impostare la velocit� e poi resettare
	if (pobj) 
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(270,4);
		pobj->Reset();
		pobj->SetG(0);
		g_objStack.Push(pobj); //immette nello stack di oggetti attivi
	}

	//fiamme
	PushFlames(x,y,4);
}

////////////////////////////////// PushExpl7 ////////////////////////////////////////////////
//esplosione media (nemico copito)

void PushExpl7(LONG x,LONG y)
{
	CExplosion *pobj=NULL;

	pobj=(CExplosion *)g_sExplMediumHit.Pop();
	
	if (pobj)
	{
		pobj->SetCurFrame(0);
		pobj->SetPosition(x,y);
		pobj->SetTrigger(4);
		pobj->SetVelocity(0,-CSbLevel::g_scroll_speed);
		pobj->Reset(g_snExplo2,0);
		g_objStack.Push(pobj);	

	}
}

/////////////////////////////////// PushExpl8 //////////////////////////////////
//esplosione grande
void PushExpl8(LONG x,LONG y)
{
	CExplosion *pobj=NULL;

	pobj=(CExplosion *)g_sExplBig.Pop();
	
	if (pobj)
	{
 		pobj->SetCurFrame(0);
		pobj->SetPosition(x,y);
		pobj->SetTrigger(4);
		pobj->SetVelocity(0,-CSbLevel::g_scroll_speed);
		pobj->Reset(g_snExplo2,0);
		g_objStack.Push(pobj);	

	}
}

//esplosione grande con detriti
void PushExpl9(LONG x,LONG y)
{
	CDebris *pobj=NULL;
     PushExpl8(x,y);

	for (int count=180;count<360;count+=30)
	{
		//detriti
		pobj=(CDebris *)g_sDebris7.Pop();
		//si deve prima impostare la velocit� e poi resettare
		if (pobj) 
		{
			pobj->SetPosition(x,y);
			pobj->SetVelocity(count,10);
			pobj->SetG(CSbLevel::g_gravity);
			pobj->Reset();			
			g_objStack.Push(pobj); //immette nello stack di oggetti attivi
		}

	}
}

//collisione di un ogetto con la barriera elettrica
void PushCollideElectro(LONG x,LONG y)
{
	g_cs.PlaySound(g_snElectro3,0,0);
}

//Esplosioni multiple sull'oggetto
//in posizioni casuali all'interno del bound box
//psrc=oggetto che sta esplodendo
//num_explosions=numero di esplosioni da generare
void PushExplMulti(CADXSprite *psrc,int num_explosions)
{
	if (psrc)
	{
		LONG w,h;
		LONG x,y;
		IMAGE_FRAME_PTR pframe=NULL;
			
		pframe=psrc->GetFramePtr(psrc->GetCurFrame());		

		if (pframe)
		{

			//corrdinate attuali del'oggetto
			x=psrc->x;
			y=psrc->y;
			w=pframe->width;
			h=pframe->height;
			CExplosion* pexpl=NULL;
			LONG xrn,yrn;
			int type;

			for (int count=0;count<num_explosions;count++)
			{
				xrn=x+g_fmath.RndFast(w);
				yrn=y+g_fmath.RndFast(h);
				type=g_fmath.RndFast(3);

				switch (type)
				{
				case 0:

					PushExpl7(xrn,yrn);
					break;

				case 1:

					PushExpl2(xrn,yrn);
					break;

				default:
					
					if (g_fmath.RndFast(2)==1)
					{
						pexpl=(CExplosion *)g_sExplMediumHit.Pop();										
					}
					else
					{
						pexpl=(CExplosion *)g_sExplBig.Pop();
					}

	
					if (pexpl)
					{
 						pexpl->SetCurFrame(0);
						pexpl->SetPosition(xrn,yrn);
						pexpl->SetTrigger(7);
						pexpl->SetVelocity(0,-CSbLevel::g_scroll_speed);
						pexpl->Reset(g_snExplo2,g_fmath.RndFast(45)+5);
						g_objStack.Push(pexpl);	
					}

					break;
				}

			}

		}
	}
}

//effetto di collisione (sup. metalliche)
void PushCollideMetal(LONG x,LONG y)
{
	//suono dovuto alla colisione di due oggetti
	g_cs.PlaySound(g_snHit1,0,0);
}




/////////////////////////////////// PushFlames /////////////////////////////////////

void PushFlames(LONG x,LONG y,DWORD dwNumFlames)
{
	DWORD dwCount;	
	CGroundObject *pobj=NULL;
	CSmoke *pSmk=NULL;
	LONG xdest,ydest;

	if (dwNumFlames==0) dwNumFlames=1;	

	for (dwCount=0;dwCount<dwNumFlames;dwCount++)
	{
		pobj=(CGroundObject *)g_sFire.Pop();

		if (pobj)
		{
			
			
			xdest=x+dwCount*20;
			ydest=y+30+(dwCount % 2)*5;	
	
			if (dwCount==1 || dwCount==3 || dwCount==5)
			{

				pSmk=(CSmoke *)g_sSmokeSrc.Pop();
				
				if (pSmk)
				{					
					pSmk->Reset();
					pSmk->SetPosition(xdest,ydest);
					pSmk->SetVelocity(180,CSbLevel::g_scroll_speed); //la sorgente si sposta da destra a sinistra
					//immette l'oggetto nello stack
					g_objStack.Push(pSmk);
				}
			}
			
			pobj->Reset();
			pobj->SetPosition(xdest,ydest);
			pobj->SetCurFrame(dwCount % pobj->GetMaxFramesIndex());	
			pobj->SetAnimSpeed(1);
			pobj->SetUpdateFreq(3);
			pobj->SetVelocity(0,0);
			pobj->dwClass=CL_NONE;
			pobj->SetEnergy(1);
			g_objStack.Push(pobj);			
			
			
		}
	}

	g_cs.PlaySound(g_snFFlames,0,0);
}




//fumo che si muove a 0�
void PushSmoke0(LONG x,LONG y)
{
	CDebris *pobj=NULL;

	pobj=(CDebris *)g_sSmoke.Pop();

	if (pobj)
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(0,8);
		pobj->Reset();
		pobj->SetG(0);
		g_objStack.Push(pobj);

	}

}


//fumo che si muove verso l'alto
void PushSmoke270(LONG x,LONG y)
{
	CDebris *pobj=NULL;

	pobj=(CDebris *)g_sSmoke.Pop();

	if (pobj)
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(270,6);
		pobj->Reset();
		pobj->SetG(0);
		g_objStack.Push(pobj);

	}

}

//fumo che si muove indietro
void PushSmoke180(LONG x,LONG y)
{
	CDebris *pobj=NULL;

	pobj=(CDebris *)g_sSmoke.Pop();

	if (pobj)
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(180,16);
		pobj->Reset();
		pobj->SetG(0);
		g_objStack.Push(pobj);

	}

}

//fumo fermo
void PushSmoke(LONG x,LONG y)
{
	CDebris *pobj=NULL;

	pobj=(CDebris *)g_sSmoke.Pop();

	if (pobj)
	{
		pobj->SetPosition(x,y);
		pobj->SetVelocity(180,CSbLevel::g_scroll_speed);
		pobj->Reset();
		pobj->SetG(0);
		g_objStack.Push(pobj);

	}

}

///////////////////////////////// GrndImpact //////////////////////////////////////////////////////////
//crea l'effetto dell'impatto al suolo
int GrndImpact(LONG x,LONG y)
{
		CSBObject *pobj=NULL;

        //acquisisce il tipo di terreno nel punto dell'impatto
		int iGroundType=CCurLevel.GetGroundType(x+18);
	
		switch(iGroundType)
		{
			case LV_LIQUID:		
			//acqua esegue uno "splash"
			pobj=g_sSplash.Pop();

			if (pobj)
			{
				pobj->Reset();
				pobj->SetPosition(x+18,y+20);			
				pobj->SetVelocity(180,6*CSbLevel::g_scroll_speed);
				g_objStack.Push(pobj);
				g_cs.PlaySound(g_snFire12,0,0);
			}

			break;

			case LV_SOLID:

			pobj=g_sGrndImpact.Pop();

			if (pobj)
			{
				pobj->Reset();
				pobj->SetPosition(x+18,y+20);			
				pobj->SetVelocity(180,6);
				g_objStack.Push(pobj);
			}		
				
			break;

			case LV_ICE:

			pobj=g_sIceHole.Pop();

			if (pobj)
			{
				pobj->Reset();
				pobj->dwClass=CL_NONE;
				pobj->SetVelocity(0,0);
				pobj->SetPosition(x,y+20);
				g_objStack.Push(pobj);
			}

			break;

		}

		return iGroundType;

}

///////////////////////////////// PushBonus ///////////////////////////////////////////////////////////
//Funzioni che danno il bonus al giocatore, viene attivata quando prende il bonus

//power up (aumenta la potenza di fuoco primaria)
void PushBonusFire1(LONG x,LONG y)
{	
	
	static CSBObject *pobj=NULL;
	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	pobj=g_sLabelPowerUp.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	//incrementa la potenza dell'arma primaria
	Player1.dwWeapon1++;

	if (Player1.dwSpecialShells>0)
	{
		//sta usando un'arma speciale quindi aumenta il numero di p.
		Player1.dwSpecialShells+=15;
	}
	//se ha un'aram piu' potente non la cambia
	else if (Player1.Fire1 != PlFire13)
	{

		switch (Player1.dwWeapon1)
		{
		case 6:	 
		case 3:
		case 1: //prima volta che viene preso questo tipo di bonus
			//aumenta la frequenza di fuoco
			Player1.iFire1Delay-=5;
			if (Player1.iFire1Delay<MIN_FIRE_DELAY) Player1.iFire1Delay=MIN_FIRE_DELAY;
			break;

		case 2: //seconda volta che viene preso questo bonus
			
			//aumenta la potenza di fuoco impostando una funzione diversa
			Player1.iFire1Delay-=2;
			if (Player1.iFire1Delay<MIN_FIRE_DELAY) Player1.iFire1Delay=MIN_FIRE_DELAY;
			Player1.Fire1=PlFire2; 

			break;    
    
		case 4:
			
			//terzo tipo di fuoco
			Player1.Fire1=PlFire3;
			break;
    
		case 5:
        
			Player1.iFire1Delay+=5;
			Player1.Fire1=PlFire4;
			break;
    
		case 7:

			Player1.iFire1Delay+=5;
			Player1.Fire1=PlFire5;
			break;

			
		default:

			Player1.dwWeapon1=0;
			break;

		}
	
	}
}

//aumenta la potenza di fuoco dell'arma secondaria
void PushBonusFire2(LONG x,LONG y)
{
	
	static CSBObject *pobj=NULL;
	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	pobj=g_sLabelPowerUp.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	//incrementa la potenza dell'arma secondaria
	Player1.dwWeapon2++;

	switch(Player1.dwWeapon2)
	{
		
		case 3:
		case 1:

			Player1.iFire2Delay -= 4;
			if (Player1.iFire2Delay<1) Player1.iFire2Delay=1;
			break;

		case 2:

            //imposta un tipo di bombe piu' potenti
			Player1.Fire2=PlDropBomb2;
			break;		        

		case 4:

			Player1.Fire2=PlDropBomb3;
			break;			

		default:

			Player1.dwWeapon2=0;
			break;
	}

}



//aumenta il numero di proiettili dell'arma primaria
void PushBonusShells(LONG x,LONG y)
{

//da fare

}



//aumenta il numero di bombe (+10)
void PushBonusBombs10(LONG x,LONG y)
{
	CSBObject *pobj=NULL;
	
	//etichetta
	pobj=g_sLabelBombs10.Pop();
	
	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		//inserisce l'etichetta nello stack
		g_objStack.Push(pobj);
		
	}

	g_cs.PlaySound(g_snBonus,0,0); 

	Player1.dwShells2 += 10;

	UpdateStatusBar();

}

//aumenta il numero di bombe (+50)
void PushBonusBombs50(LONG x,LONG y)
{
	CSBObject *pobj=NULL;
	
	//etichetta
	pobj=g_sLabelBombs50.Pop();
	
	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		//inserisce l'etichetta nello stack
		g_objStack.Push(pobj);
		
	}

	g_cs.PlaySound(g_snBonus,0,0); 

	Player1.dwShells2 += 50;

	UpdateStatusBar();

}

//+punti
void PushBonusScore(LONG x,LONG y)
{
	CSBObject *pobj=NULL;
	
	//etichetta
	pobj=g_sLabelScore.Pop();
	
	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		//inserisce l'etichetta nello stack
		g_objStack.Push(pobj);
		
	}

	g_cs.PlaySound(g_snBonus,0,0); 

	Player1.dwScore += 1000;

	//aggiorna la status bar
	UpdateStatusBar();	

}

//proiettile laser blu
void PushBonusBlueShell(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		//inserisce l'etichetta nello stack
		g_objStack.Push(pobj);
		
	}

	g_cs.PlaySound(g_snBonus,0,0); 

	Player1.pfnStoredFire=Player1.Fire1;
	Player1.dwSpecialShells=60; //numero di proiettili speciali a disposizione
	//proiettili ausiliari(non finiscono fino a che non muore)	
	Player1.Fire1=PlFire13;
	Player1.iStoredDelay1=Player1.iFire1Delay;
	Player1.iFire1Delay=10; //dimezza la frequenza di fuoco

}

void PushBonusLaser3(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=80;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=9; //icona 9
	Player1.Fire3=PlFire14;

	UpdateStatusBar();
}

//il player vince la barriera elettrica
void PushBonusEB(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		//inserisce l'etichetta nello stack
		g_objStack.Push(pobj);
		
	}

	g_cs.PlaySound(g_snBonus,0,0); 

	CAnimatedObject *pAo=(CAnimatedObject *)g_sElectricBarrier.Pop();
	
	if (pAo)
	{		
		//imposta la durata della barriera in numero di ciclo
		pAo->SetInitialEnergy(3000);
		pAo->Reset();	
		pAo->dwPower=20;
		pAo->pParent=(CSBObject *)&Player1; //vincola la barriera al Player
		pAo->SetVelocity(0,0);
		pAo->SetPosition(-100,-90);
		pAo->SetCycles(800);
		pAo->dwClass=CL_FRIEND;
		g_objStack.Push(pAo);

	}

}

void PushBonusThrust(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelThrust.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0);

	//aumenta la velocit� di movimento del player
	if (Player1.iDisplacement<8)
	{
		Player1.iDisplacement++;
		Player1.bNozzleFlames=TRUE; //fa apparire le fiamme del post bruciatore
		g_cs.PlaySound(g_snNozzle,0,0);
	}

}


//crea una rampa di velocit� che fa variare la velocit� di scrolling del livello
void PushBonusBooster(LONG x,LONG y)
{
	CBooster *pobj1=(CBooster *)g_sBooster.Pop();
	CSBObject *pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	if (pobj1)
	{
		pobj1->Reset();
		pobj1->m_pLv=&CCurLevel;
		g_objStack.Push(pobj1);
		g_cs.PlaySound(g_snBooster,0,0);
	}

}

//+barriera protettiva
void PushBonusShield(LONG x,LONG y)
{
	const long r=20; //distanza a cui viene posta la barriera rispetto all player
	static int delta=0;
	long xb,yb;
	int ang;
	CShield *psh;
	CSBObject *pobj=NULL;

	pobj=g_sLabelShield.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	for (int count=0;count<8;count++)
	{
		ang=count*45+delta;
		//corrdinate relative all'oggetto parente
		xb=FIXEDTOLONG(r*g_fmath.CosFast[ang]);
		yb=FIXEDTOLONG(r*g_fmath.SinFast[ang]);

		psh=(CShield *)g_sShield.Pop();	

		if (psh)
		{
			//oggetto rispetto al quale si muove
			psh->pParent=(CSBObject *)&Player1;
			psh->Reset();
			psh->SetCurFrame(count*2);
			psh->pfm=&g_fmath; //fast math
			psh->SetVelocity(ang,2);
			psh->SetPosition(xb,yb);
			//immette l'oggetto nello stack degli oggetti attivi
			g_objStack.Push(psh);
		}

	}

	delta += 15;

	if (delta>45) delta=0;

}

//fuoco verso terra
void PushBonusGrndFire(LONG x,LONG y)
{

	CSBObject *pobj;
	
	if (Player1.bTripleFire)
	{
		pobj=g_sLabelScore.Pop();
	}
	else pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	//se ha gi� il fuoco triplo non lo setta
	if (!Player1.bTripleFire)
	{		
		Player1.bGrndFire =TRUE;
		Player1.dwShells5=45; 
	}
	else
	{
		Player1.dwScore += 1000;
		UpdateStatusBar();
	}
	
}

void PushBonusTripleFire(LONG x,LONG y)
{

	CSBObject *pobj;
	
	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	//se ha gi� il fuoco triplo non lo setta
	Player1.bTripleFire =TRUE;
	Player1.dwShells5=45; //numero di colpi disponibili in questa modalit�
}

void PushBonusFwShells(LONG x,LONG y)
{
	CSBObject *pobj;
	
	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=35;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=7; 
	Player1.Fire3=PlFireFw;

	UpdateStatusBar();
}

void PushBonusStrightMissile(LONG x,LONG y)
{
	CSBObject *pobj;
	
	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	Player1.dwShells4=30; // n� di missili disponibili
	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	//se ha gi� il fuoco triplo non lo setta
	Player1.bStrightMissile = TRUE;
}


//bonus fuoco amico (� quello che gira intorno e spara ai nemici puntandone uno a caso)
void PushBonusGyro(LONG x,LONG y)
{
	CSBObject *pobj;
	CGyro *pGyr=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus


	pobj=g_sGyros.Pop();

	if (pobj)
	{
		pGyr=(CGyro *)pobj;
		//questo oggetto � solidale al sistema di riferimento del giocatore	
		pGyr->Reset();
		pobj->pParent=(CSBObject *)&Player1;
		pobj->SetCurFrame(5);
		pGyr->pfm=&g_fmath;
		pGyr->Cannon.SetCurFrame(4);
		pGyr->Cannon.pParent=(CSBObject *)&Player1; //imposta l'oggetto genitore per il cannone
		pGyr->SetActiveStack(&g_objStack); //lo stack attivo serve per cercare i nemici a cui sparare
		pobj->SetVelocity(0,3);
		pobj->SetPosition(0,20);
        //immette l'oggetto nello stack degli oggetti attivi 
		g_objStack.Push(pobj);
		
	}

}


//bonus energia
void PushBonusEnergy(LONG x,LONG y)
{
	CSBObject *pobj=NULL;
	
	pobj=g_sLabelEnergy.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);

	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus
	if (Player1.GetEnergy()<100) Player1.IncrEnergy(32); //quattro tacche di energia

	UpdateStatusBar();

}


//bonus +1 vita
void PushBonus1Up(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabel1Up.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snFinal,0,0); //effetto sonoro del bonus
	g_cs.PlaySound(g_snBonus,0,0);
	Player1.dwLives++; //incrementa il n� di vite

	UpdateStatusBar();//aggiorna la status bar

}

//fulmine
void PushBonusThunder(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=30;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.Fire3=PlFire6;
	Player1.dwWeapon3=0;

	UpdateStatusBar();

}

void PushBonusLaser(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=20;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=4; //icona 4
	Player1.Fire3=PlFire7;

	UpdateStatusBar();

}

void PushBonusMissile(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=60;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=3; //icona 3
	Player1.Fire3=PlFire8;

	UpdateStatusBar();
}

void PushBonusWeaponA(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=15;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=1; //icona 1
	Player1.Fire3=PlFire9;

	UpdateStatusBar();
}


//proiettili lunghi rossi
void PushBonusWeaponB(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=25;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=5; //icona 5
	Player1.Fire3=PlFire10;

	UpdateStatusBar();
}

//falce
void PushBonusWeaponC(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=25;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=2; //icona 2
	Player1.Fire3=PlFire11;

	UpdateStatusBar();
}

void PushBonusWeaponD(LONG x,LONG y)
{
	CSBObject *pobj=NULL;

	pobj=g_sLabelSpecial.Pop();

	if (pobj)
	{
		pobj->Reset();
		pobj->SetPosition(x-20,y);
		g_objStack.Push(pobj);
	}

	g_cs.PlaySound(g_snBonus,0,0); //effetto sonoro del bonus

	Player1.dwShells3=25;
	Player1.dwShells3Full=Player1.dwShells3;
	Player1.dwWeapon3=6; //icona 6
	Player1.Fire3=PlFire12;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//esegue la scansione della coda e aggiorna lo stato degli oggetti
//e li disegna sul display
void RefreshObjects(IMAGE_FRAME_PTR imgDisplay)
{
	register DWORD dwCount;
	register int iCountLayer;
	register DWORD dwTop=g_objStack.GetTop(); 
	CSBObject **pList;
	static CSBObject *pobj;
	HRESULT hr;
	static int iCollisionStep=0;
	const int iCollFreq=3;
	static dwCollMask=CL_PLFIRE+CL_FRIEND+CL_PLAYER;
	BOOL bColl=FALSE;
	LONG lgrndX=CCurLevel.GetCurrentX();
	//DWORD dwNumObjects;

#ifdef _TRACE_

	if (g_bTraceOn)
	{
		g_bTraceOn=TRUE;
		g_cdb.WriteErrorFile("inizio ciclo top=%d",dwTop);
	}

#endif

    //punta allo stack degli oggetti
	pList = g_objStack.m_objStack;	

	//il primo ciclo serve a disegnare gli oggetti prima sui layer piu' lontani (vedere membro m_zlayer)
	//e poi quelli piu' vicini 
	for (iCountLayer=3;iCountLayer>=0;iCountLayer--)
	{

		for (dwCount=0;dwCount<=dwTop;dwCount++)
		{
			if (pList[dwCount])
			{
				pobj=pList[dwCount];
				if (iCountLayer==pobj->m_zlayer)
					{
					//imposta la x e la y del target per far sapere dove si trova il giocatore
					pobj->lTargetX=Player1.x;
					pobj->lTargetY=Player1.y;
					pobj->lGroundX=lgrndX;
					//g_dwCurTime viene popolato nella funzione HeartBeat
					if (!pobj->Update()) //la funzione update rende false se l'oggetto sparisce dallo schermo o esplode o deve essere eliminato per altro motivo
					{					
						//rende l'oggetto di nuovo disponibile
						pobj->dwStatus=0; 
						//elimina l'oggetto				
						g_objStack.Delete(dwCount);
					}			
					else 
					{				
						pobj->m_pimgOut=imgDisplay;
						hr=pobj->Draw();	
						//il controllo delle collisioni viene fatto solo per gli oggetti appartenenti al gicatore 
						//(i nemici non possono collidere fra loro)
						
						//nota: qui la collisione deve essere testata per ogni oggetto con tutti gli altri
						//a causa del fatto che alcuni oggetti sono "composti" da piu' parti non hanno un bound box rettangolare
						//es. un proiettile deve eesere testato con un oggetto composto ma anche vicerversa vedere funzione DoCollisionWith

						//if (pobj->dwClass & dwCollMask)
						//{
							bColl |= DetectCollisions(pobj,0,dwTop); //controlla le collisioni fra i proiettili del player e tutti gli altri oggetti					
						//}
					}
				}
			}
		}
		
	}

	//controlla le collisioni fra il player e gli altri oggetti
	bColl |= DetectCollisions(&Player1,0,dwTop);
	
	//se c'� una collisione aggiorna la status bar
	if (bColl) UpdateStatusBar();	
}

//----------------------------------------- DetectCollisions -----------------------------------------

//Controlla le collisioni fra gli oggetti dello stack attivo  (g_objStack)
//Un oggetto puo' collidere solo con alcuni tipi di oggetti.
//Il vettore g_dwClMatch indica per ogni classe di oggetto, gli oggetti che possono entrare in collisione.
//dwStart � l'indice del vettore dal quale si parte per determinare le collisioni
//dwNumObjects � il numero di elementi da controllare

BOOL DetectCollisions(CSBObject *pobj,DWORD dwStart,DWORD dwTop)
{
	DWORD dwCurClass;
	register DWORD dwCount;
	DWORD dwMatch;
	CSBObject **pList;
	static int absx,absy;
	CSBObject *pobj2;
	BOOL bCollide=FALSE;
	BOOL bChanged=FALSE;
		
	dwCurClass=pobj->dwClass; //tipo di oggetto corrente (es. CL_ENEMY,CL_PLFIRE ecc...)
	dwMatch=g_dwClMatch[dwCurClass]; //estrae i tipi che collidono con il tipo corrente

	pList=g_objStack.m_objStack;

	//se l'oggetto non ha energia non deve collidere
	if (pobj->GetEnergy()<=0) return FALSE;

	for (dwCount=dwStart;dwCount<=dwTop;dwCount++)
	{
		if (pList[dwCount]) 
		{			
			pobj2=pList[dwCount];
            
			if (pobj2)
			{			
				//Solo alcuni oggetti possono collidere fra loro			
				if ((pobj2->dwClass & dwMatch)>0 && pobj2->dwClass!=dwCurClass)
				{			
					if (pobj2->GetEnergy()>0)
					{						

						if (pobj2->DoCollisionWith(pobj))
						{
#ifdef _TRACE_
							if (g_bTraceOn)
							{
								g_cdb.WriteErrorFile("------ collisione ---");
								g_cdb.WriteErrorFile("oggetto base type=%d index=%d class=%d x=%d y=%d en=%d",pobj->GetType(),pobj->GetIndex(),pobj->dwClass,pobj->x,pobj->y,pobj->GetEnergy());
								g_cdb.WriteErrorFile("box base (%d,%d,%d,%d)",pobj->rcBoundBox.left,pobj->rcBoundBox.top,pobj->rcBoundBox.right,pobj->rcBoundBox.bottom);
								g_cdb.WriteErrorFile("oggetto corrente type=%d index=%d class=%d x=%d y=%d en=%d",pobj2->GetType(),pobj2->GetIndex(),pobj2->dwClass,pobj2->x,pobj2->y,pobj2->GetEnergy());
								g_cdb.WriteErrorFile("box corrente (%d,%d,%d,%d)",pobj2->rcBoundBox.left,pobj2->rcBoundBox.top,pobj2->rcBoundBox.right,pobj2->rcBoundBox.bottom);
							
							}
#endif
						
							bChanged=TRUE; //indica di aggiornare la status bar
				
							if (pobj2->GetEnergy() <=0 && pobj2->dwClass==CL_ENEMY)
							{	
								//aumenta i punti del giocatore
								Player1.dwScore += pobj2->dwScore;	
								//incrementa il contatore delle statistiche
								CCurLevel.m_Stats[pobj2->dwResId].iHit++;						
						
							}

							if (pobj->GetEnergy() <=0 && pobj->dwClass==CL_ENEMY)
							{
								//aumenta i punti del giocatore
								Player1.dwScore += pobj->dwScore;	
								//incrementa il contatore delle statistiche
								CCurLevel.m_Stats[pobj->dwResId].iHit++;		

							}

						}
				
					}
				}
			}
		}
	}

	return bChanged; //rende true se il palyer � stato colpito
}

////////////////////////////////////////// GameLoop ///////////////////////////////////////////////
//Funzione che inizializza il ciclo del gioco
//restituisce 1 se il player completa il livello corrente

int GameLoop(void)
{	
	
	DWORD dwCount;	
	IMAGE_FRAME_PTR pimg=NULL;   
	UINT iScrn;
	DWORD dwScreens;

	//Crea la tabella di default dei punteggi
	//CreateDefaultHighScores();
	//resetta lo stack per sicurezza


	//elimina dallo stack eventuali oggetti residui del ciclo precedente
	//Attenzione! Se si chiama questa procedura quando lo stack � gi� stato popolato ed alcuni oggetti sono
	//sono stati rilasciati (es. FreeAuxRes) si incorre in errore (memory leakage)
	g_objStack.DeleteAll();

	//rilascia le risose ausiliarie che evntualmente sono state caricate dallo schema precedente
	//serve a liberare memoria 
	//NB: questa chiamata potrebbe essere causa di memory leaks
	//FreeAuxResources();

#ifdef _DEBUG_

	//	_CrtDumpMemoryLeaks();
		_CrtCheckMemory();

#endif

#ifdef _TESTGAME_

	g_cdb.WriteErrorFile("---- inizio main loop ------");     
	g_cdb.WriteErrorFile("tot mem=%d",g_fm.GetTotmem());

#endif

	//numero di superfici video (front buffer + back buffer)
	dwScreens=g_gm.GetScreenNum();

    g_cs.PlaySound(g_snBonus,0,0);

	imgScreen=new IMAGE_FRAME[dwScreens];

	if (imgScreen==NULL)
	{
		MsgOut(MB_ICONEXCLAMATION,"Errore durante l'allocazione di memoria nel ciclo del gioco.","");

		g_Environ.game_status=GM_SHUTDOWN;

		g_Environ.hasError=TRUE;

		return 0;
	}

	CCurLevel.SetGraphicManager(&g_gm);

	CCurLevel.SetFrameManager(&g_fm);

    //crea il livello
	if (FAILED(CreateSbLevel(g_iLevelSequence[g_Environ.iLevel-1],&CCurLevel,g_szLoadCustomLevel))) 
	{
		g_cdb.WriteErrorFile(TranslateGameMsg(55,123),g_Environ.iLevel,CCurLevel.GetLastError());
		//MsgOut(MB_ICONEXCLAMATION,"Errore",TranslateGameMsg(54,123),g_Environ.iLevel,CCurLevel.GetLastError());
		
		g_Environ.hasError=TRUE;

		g_Environ.game_status=GM_SHUTDOWN;

		return 0;
	} 

#ifdef _DEBUG_
	
//	_CrtDumpMemoryLeaks();
	_CrtCheckMemory();

#endif

	//GetXLimit restituisce l'ascissa del punto estremo del livello nel layer 0
	//si deve sottrarre la larghezza dello schermo
	g_lLimit=CCurLevel.GetXLimit()-SCREEN_WIDTH-100;
		
    //imposta le superfici della flipping chain
	for (dwCount=0;dwCount<dwScreens;dwCount++) 
	{
		pimg=g_gm.GetScreenImage(dwCount);			
		
		memcpy(&imgScreen[dwCount],pimg,sizeof(IMAGE_FRAME));
		//imgScreen[dwCount]=g_gm.GetScreenImage(dwCount);

	//	lpDDS[dwCount]=imgScreen[dwCount].surface;
	}    

	g_gm.UseBackBuffer();
	
	//acquisisce l'indice della superficie corrente
	iScrn=g_gm.GetDrawingScreen();	

	//prova frames-----------------
/*	g_gm.UseBackBuffer();
	g_gm.Cls(0);
	
	for (dwCount=0;dwCount<7;dwCount++) g_fm.PutImgFrame(10+dwCount*30,40,&p23[dwCount]);


//	g_fm.PutImgFrame(80,80,&m_imgDebris5);

	g_gm.ShowBackBuffer();	  
	 
	   
    
    for (dwCount=0;dwCount<12;dwCount++) g_fm.PutImgFrame(dwCount*98,10,&m_imgScrewFighter[dwCount]);


	g_fm.PutImgFrame(100,100,&m_imgNightBuilding[1][1]);
	do
	{
	g_ci.GetKybStatus();

	} while (!g_ci.KeyDown(DIK_Q));
	*/

     
	//---------fine prova

	dwCount=0;  //posizione iniziale del livello: impostare su un numero >0 per saltare in un punto del livello corrente	

	//cattura l'immagine corrente
	g_fm.CaptureScreen(&m_imgCurScreen);
	
    //aggiorna la barra in alto con punti energia ecc...
	UpdateStatusBar();    
    //imposta la superficie di output 
	Player1.m_pimgOut=&imgScreen[1];
	//imposta la posizione del livello corrente
	CCurLevel.SetPosition(1);
    //renderizza il livello nella posizione corrente
	CCurLevel.Render(&imgScreen[1]); 

    //si puo' continuare la massimo una volta per ogni schema
	g_iContinue=0;
    
	//Resetta il player
	//il reset viene fatto solo al primo schema

	if (g_Environ.iLevel<=1)
	{
		//Impostazioni del player all'inizio del gioco

		//Imposta lo stato iniziale del Player (questo settaggio viene fatto ogni volta che inizia una partita)		
		Player1.Reset();  	
	
		Player1.dwLives=5; 

		//(il controllo della vincita della vita viene fatto da U
		Player1.dwNextLife=100000; //punteggio al quale si vince la vita

		Player1.dwScore=0;
		//imposta le funzioni di fuoco primario e secondario
		Player1.Fire1=PlFire1;	   //fuoco primario
		//bombe	
		Player1.Fire2=PlDropBomb1; 
		//arma speciale
		Player1.Fire3=NULL;
 		
		Player1.pfnGameOver=GameOver; //punta la funzione game over
		
		Player1.pfnSmoke=PushSmoke180; //funzione fumo
		
		Player1.pfnUpdateBar=UpdateStatusBar; //funzione di aggiornamento barra di stato
		//imposta la funzione di input
		//EN_JOYSTICK=2
		if (g_Environ.iInput==EN_JOYSTICK) Player1.lpfnGetInput=GetJoyInput; //input da joystick
		 
		else Player1.lpfnGetInput=GetKybInput; //inpust da tastiera		
	
	}

	//reset da fare ogni cambio di livello
	Player1.pNozzleFlames->SetDisplay(&imgScreen[1]);	
	Player1.SetPosition(20,120); //posizione iniziale
	Player1.dwClass=CL_PLAYER; 
	Player1.SetEnergy(80); //imposta il valore massimo dell'energia		
	

	//disegna il player
	Player1.Draw();	

	UpdateStatusBar();	
	
	//cattura l'immagine corrente
	g_fm.CaptureScreen(&m_imgCurScreen);
		
    //effetto di transizione tra lo sfondo con la barra e l'immagine del livello
	ScreenTransition(&imgBackMenu,&m_imgCurScreen);
	
	g_fm.FreeImgFrame(&m_imgCurScreen);
     
    g_Environ.dwLimit=CCurLevel.GetMaxX()-SCREEN_WIDTH;

	//immette nello stack il messaggio di inizio livello
	//push the initial level message into the active stack
	//messaggio di inizio livello
	g_LevelMessage.SetGraphicManager(&g_gm);

	g_LevelMessage.SetFrameManager(&g_fm);
	
	g_LevelMessage.Reset();
	
	g_LevelMessage.iMode=1;
	
	g_LevelMessage.SetCharSet(&g_chLittleBlue);
	
	g_LevelMessage.SetText(CCurLevel.GetLevelName());
	
	g_LevelMessage.SetPosition((LONG)((g_Environ.iScreenWidth-g_chLittleBlue.GetTextWidth(CCurLevel.GetLevelName()))*0.5f),(LONG)(g_Environ.iScreenHeight*0.5f));
	
	g_LevelMessage.SetDelay(75); 

	//messaggio iniziale che visualizza il nome del livello (es. livello 1 ecc....)
	g_LevelName.SetGraphicManager(&g_gm);

	g_LevelName.SetFrameManager(&g_fm);

	g_LevelName.Reset();

	g_LevelName.iMode=1;

	g_LevelName.SetCharSet(&g_chLittleRed);

	if (g_Environ.iLanguage==1)	_stprintf(g_szLevelName,"LEVEL %d",g_Environ.iLevel);
	else _stprintf(g_szLevelName,"LIVELLO %d",g_Environ.iLevel);

	g_LevelName.SetText(g_szLevelName);

	g_LevelName.SetPosition((LONG)((g_Environ.iScreenWidth-g_chLittleRed.GetTextWidth(g_szLevelName))*0.5f),(LONG)(g_Environ.iScreenHeight*0.5f-30));
	
	g_LevelName.SetDelay(75); 
	
	g_objStack.Push(&g_LevelMessage); 

	g_objStack.Push(&g_LevelName);
	//resetta il cronometro
	g_tm=timeGetTime();
    //resetta il contatore frames
	g_dwFrameCnt=0;

#ifndef _LEVELEDIT_


	//DEBUG:sposta la posizione del livello
	//CCurLevel.SetPosition(30000);
	//CCurLevel.SetTriggerPosition(30000);

	//attiva il ciclo base del gioco
    g_Environ.game_status=GM_HEARTBEAT; 

#else
	
	g_Environ.game_status=GM_LEVELEDITOR;

#endif

	return 1;
}

///////////////////////////////////////// LevelCompleted ////////////////////////////////////

//Sequenza di livello completato, passa al successivo o finisce il gioco

void LevelCompleted(void)
{

    //nota bene ! Non usare variabili locali in questa funzione  
	//do not use local variables inside this function!
	static IMAGE_FRAME_PTR pimg=NULL;
    static HRESULT hr;   

	//quando la posizione raggiunge g_lLimit significa che siamo alla fine del livello
	//e quindi deve avviare la procedura di fine livello
	//g_lLimit viene settato da GameLoop
    if (Player1.x>g_Environ.iScreenWidth || CCurLevel.GetCurrentX()>=CCurLevel.GetMaxX())
	{	
		g_Environ.lCurrentScore=Player1.dwScore; //memorizza il punteggio ottenuto dal giocatore		   
	   
	    //cattura lo schermo
	    if (!FAILED(g_fm.CaptureScreen(&m_imgCurScreen)))
		{
			//effetto di transizione
			ScreenTransition(&m_imgCurScreen,&imgBackMenu);
			//rilascia l'immagine
			g_fm.FreeImgFrame(&m_imgCurScreen);
		}

		//rilascia i vettori allocati all'interno del game loop
		SAFE_DELETE_ARRAY(imgScreen);
	
		if (g_Environ.iLevel==NUM_LEVELS)
		{
			//se � arrivato all'ultimo livello il gioco finisce e fa vedere una sequenza finale
			g_Environ.game_status=GM_GAMECOMPLETED; //gioco completato: fa vedere la sequenza finale

			//visualizza la pagina finale e attende che il player prema un tasto o che scada il tempo
			GameCompleted();

			g_Environ.lCurrentScore=Player1.dwScore;
			if (GetHighScorePosition(Player1.dwScore)>=0) HighScore(m_pActiveForm);
			
			//Resetta lo stato del player

			g_Environ.iLevel=1;

			//Il player ha chiesto di uscire dal livello:rimette in stato console
			g_Environ.game_status=GM_CONSOLE;
			//resetta lo stack principale	

		}
		else
		{
			
			if (g_snFinal>0)
			{
				//jingle di fine livello
				g_cs.PlaySound(g_snFinal,0,0);			

			}		

			//visualizza il panello con le statistiche 
			ShowStats(&CCurLevel);
			g_Environ.iLevel++; //passa al livello successivo					
			g_Environ.game_status=GM_GAMELOOP;
		}

		//resetta il livello corrente
	    CCurLevel.Release(); 
	
	}
	
	else
	{
		//il livello � fermo e il player va avanti in questa fase finale
		//Sequenza di fine livello: il player si sposta a destra senza che il giocatore lo possa controllare
		//punta il back buffer
	    pimg=&imgScreen[1];
		CCurLevel.Render(pimg);
        //aggiorna lo stack degli oggetti attivi 
		RefreshObjects(pimg);
        //disegna il giocatore	
		Player1.Draw();
        //disegna la barra di stato con energia, armi ecc...
		DrawStatus(pimg);
       //acquisisce l'input del giocatore dal dispositivo scelto (tastiera o joystick)				
    	Player1.x+=4; //endInput(&g_plInput); 
		Player1.Update();
	
#ifdef _DEBUG_
        //flipping windowed  
		hr=g_gm.FlipScreen(1);
#else	

		do
		{
			//flipping fullscreen
			hr=imgScreen[0].surface->Flip(NULL,DDFLIP_DONOTWAIT);

		} while FAILED(hr);
#endif  	
    }
}

/////////////////////////////////////// GameCompleted ///////////////////////////////////////////////////
//funzione chiamata alla fine del gioco
void GameCompleted(void)
{

	TCHAR *tx1_1=TEXT("MISSIONE COMPIUTA!");
	TCHAR *tx1_2=TEXT("GIOCO COMPLETATO");
	TCHAR *tx2_1=TEXT("MISSION ACCOMPLISHED!");
	TCHAR *tx2_2=TEXT("GAME COMPLETED");
	TCHAR *tx3=TEXT("EXTRA BONUS 500000");
	TCHAR *ptx1,*ptx2;
	DWORD dwTime;
	const int extra_bonus=500000; //extra bonus di completamento del gioco

	if (g_Environ.iLanguage==0)
	{
		ptx1=tx1_1;
		ptx2=tx1_2;

	}
	else
	{
		ptx1=tx2_1;
		ptx2=tx2_2;
	}

	LONG final_sound=g_cs.CreateSound("WAVS\\endg.wav",1);

	if (final_sound>0) g_cs.PlaySound(final_sound,0,0);

	g_gm.UseBackBuffer();
	g_fm.PutImgFrame(0,0,&imgBackMenu,0);


	//messaggio finale
	g_chRed.TextOut((int)((g_Environ.iScreenWidth - g_chRed.GetTextWidth(ptx1))*0.5f),80,ptx1);
	
	g_chRed.TextOut((int)((g_Environ.iScreenWidth - g_chRed.GetTextWidth(ptx2))*0.5f),150,ptx2);

	g_chBlue.TextOut((int)((g_Environ.iScreenWidth - g_chBlue.GetTextWidth(tx3))*0.5f),220,tx3);

	Player1.dwScore+=extra_bonus;

	//visualizza il back buffer
	g_gm.ShowBackBuffer();

	dwTime=timeGetTime();

	g_ci.ResetKyb();

	//attende fino a che non si preme un tasto per continuare oppure � passato un certo tempo
	do {
		
		g_ci.GetKybStatus();

	} while(!g_ci.GetCurrentKeyDown(0,255) && timeGetTime()-dwTime<19000);


}

/////////////////////////////////////// HeartBeat ///////////////////////////////////////////////////////
 
//ciclo principale di animazione
void HeartBeat(void)
{
    //nota bene ! Non usare variabili locali in questa funzione  
	//do not use local variables inside this function!
	static IMAGE_FRAME_PTR pimg=NULL;
    static HRESULT hr;  
	DWORD dwTm;
	
	dwTm=timeGetTime();

	//limita il refresh rate del gioco (il gioco � pensato per girare a 60 FPS)
	if (dwTm - g_dwCurTime < 16) return;
	
	g_dwCurTime=dwTm;
    
	//quando la posizione raggiunge g_lLimit significa che siamo alla fine del livello
	//e quindi deve avviare la procedura di fine livello
	//g_lLimit viene settato da GameLoop
	
    if (CCurLevel.GetCurrentX()>g_lLimit)  
	{
		//imposta la sequenza di fine livello
   		g_Environ.game_status=GM_LEVELCOMPLETED;
		//immette nello stack il mesaggio di fine livello
		g_LevelMessage.Reset();	
		g_LevelMessage.iMode=1;	
		g_LevelMessage.SetCharSet(&g_chLittleBlue);
		if (g_Environ.iLanguage==0)	g_LevelMessage.SetText(TEXT("LIVELLO COMPLETATO"));	
		else g_LevelMessage.SetText(TEXT("LEVEL COMPLETED"));
		g_LevelMessage.SetPosition(120,(LONG)(g_Environ.iScreenHeight*0.5f));	
		g_LevelMessage.SetDelay(95);
		g_LevelMessage.bActive=TRUE;
		g_objStack.Push(&g_LevelMessage);
		memset(&g_plInput,0,sizeof(PL_INPUT));
		Player1.SendInput(&g_plInput); //azzera lo stato dell'input
		Player1.dwClass=CL_NONE; //impedisce le collisioni in questa fase

	}
	else if(Player1.bExitGame)
	{	

	    IMAGE_FRAME imgTmp;	
		g_fm.CaptureScreen(&imgTmp);
        //avvia la transizione dello schermo verso il menu
		ScreenTransition(&imgTmp,&imgBackMenu);
        g_fm.FreeImgFrame(&imgTmp);

		//nota: non si puo' continuare piu' di una volta per ogni schema
		//il flag viene settato da ContinueGame
		//se il player � in classifica punteggi non puo' continuare e deve scrivere il proprio nome
		if (Player1.dwLives==0 && g_iContinue<2 && !(GetHighScorePosition(Player1.dwScore)>=0))
		{
			//chiede se si vuol continuare
			//polling sulla form
			//g_iContinue viene incrementato dalla funzione seguente
			Continue();
		}

		//la funzione Continue() imposta il flag bExitGame su true se il player decide di non continuare
		if (Player1.bExitGame)
		{

			Player1.bExitGame=FALSE;
			//elimina dalla cache le risorse ausiliarie
			//FreeAuxResources();
			ShowMainMenu(NULL);
			//se il punteggio � sufficiente fa scrivere il nome del player nella lista punteggi
			g_Environ.lCurrentScore=Player1.dwScore;
			if (GetHighScorePosition(Player1.dwScore)>=0) HighScore(m_pActiveForm);
			//il player ha chiesto di uscire dal livello:rimette in stato console
			g_Environ.game_status=GM_CONSOLE;
			//resetta lo stack principale	

	#ifdef _TESTGAME_
			
			DWORD dwElapsed=timeGetTime()-g_tm;
			g_cdb.WriteErrorFile("tempo trascorso (ms): %d",dwElapsed);
			g_cdb.WriteErrorFile("frames: %d ",g_dwFrameCnt);
			g_cdb.WriteErrorFile("FPS= %f",(float)g_dwFrameCnt*1000/dwElapsed);


	#endif
		}

	}
	else
	{
		//punta il back buffer
	    pimg=&imgScreen[1];
		CCurLevel.ScrollOn(pimg);
        //aggiorna lo stack degli oggetti attivi 
		RefreshObjects(pimg);
        //disegna il giocatore	
		Player1.Draw();
        //disegna la barra di stato con energia, armi ecc...
		DrawStatus(pimg);
        //acquisisce l'input del giocatore dal dispositivo scelto (tastiera o joystick)		
		//al'invia all'oggetto
		Player1.lpfnGetInput();
		//aggiorna il Player1
		Player1.Update();
		//genera un evento (solo se c'� un evento in coda)
		CCurLevel.RaiseEvent(); 


#ifdef _DEBUG_
        //flipping windowed  
		hr=g_gm.FlipScreen(1);
#else	

		do
		{
			//flipping fullscreen
			hr=imgScreen[0].surface->Flip(NULL,DDFLIP_DONOTWAIT);

		} while FAILED(hr);
#endif  	

#ifdef _TESTGAME_

		g_dwFrameCnt++; //incrementa il contatore frames

#endif
     		
	}

}


//////////////////////////////////// LevelEditor ///////////////////////////////////////////////////

//modalit� level editor
void LevelEditor(void)
{
    //nota bene ! Non usare variabili locali in questa funzione  
	//do not use local variables inside this function!
	static IMAGE_FRAME_PTR pimg=NULL;
    static HRESULT hr;   
     
	//la gestione dei tasi del mouse � in DrawSelectedObject
	
	if(Player1.bExitGame)
	{
	    
		ShowMainMenu(NULL);
		//il player ha chiesto di uscire dal livello:rimette in stato console
		g_Environ.game_status=GM_CONSOLE;

	}
	else
	{

		//heart beat per il level editor
		//punta il back buffer
	    pimg=&imgScreen[1];
		CCurLevel.Render(pimg);
		CCurLevel.RenderTriggers(pimg,g_SelectedTrigger);
        //disegna la barra di stato 
		DrawStatus(pimg);
        //acquisisce l'input del giocatore dal dispositivo scelto (tastiera o joystick)		
		//al'invia all'oggetto
		GetKeybEditor();
		//disegna l'eventuale oggetto selezionato
		DrawEditorSelectedObject(pimg);
		g_chLittleBlue.TextOut(10,100,pimg,"LEVEL EDITOR");
		g_chLittleRed.TextOut(10,130,pimg,"LEFT,RIGHT=MUOVE A DESTRA E SINISTRA");
		g_chLittleRed.TextOut(10,160,pimg,"UP,DOWN=CAMBIA LAYER");
		g_chLittleRed.TextOut(10,190,pimg,"0-9 Q-P=INSERISCE UN TILE");
		g_chLittleRed.TextOut(10,220,pimg,"S = SALVA LA MAPPA SU MAP.TXT");
		g_chLittleRed.TextOut(10,250,pimg,"PAR. QUADRATE = SELEZIONE OGGETTI");
		g_chLittleRed.TextOut(10,280,pimg,"INSERT = MODALITA SELEZIONE ON - OFF");
		g_chLittleRed.TextOut(10,310,pimg,"DELETE = ELIMINA OGGETTO SELEZIONATO");
		g_chLittleBlue.TextOut(10,340,pimg,"LAYER: %d",g_dwLayerEditor);


#ifdef _DEBUG_
        //flipping windowed  
		hr=g_gm.FlipScreen(1);
#else	

		do
		{
			//flipping fullscreen
			hr=imgScreen[0].surface->Flip(NULL,DDFLIP_DONOTWAIT);

		} while FAILED(hr);
#endif  	
     		
	}

}

///////////////////////////////////// ClearBack1 //////////////////////////////////////////////////
//funzioni di disegno del bckground del livello

void ClearBack1(IMAGE_FRAME_PTR pimg)
{
	pimg->surface->Blt(&g_rcFill,NULL,NULL,DDBLT_COLORFILL,&g_ddbltfx);
}


/////////////////////////////////// DrawBackground ////////////////////////////////////////////////
//funzione standard per il disegno del background dello schema
void DrawLevelBackGround(IMAGE_FRAME_PTR pimg)
{
	//m_imgBackGround e  m_lBackImageTop vengono caricati da LoadSbLevel
	g_fm.PutImgFrameRect(0,m_lBackImageTop,pimg,&m_imgBackGround,&m_rcBack,0);
}

/////////////////////////////////// StarField //////////////////////////////////////////////////////
//effetto star field
//pimg � l'immagine di destinazione
//prima disegna il background e poi fa l'effetto starfield
void StarField(IMAGE_FRAME_PTR pimg)
{
	//disegna il background (uguale a DrawLevelBackground)
	g_fm.PutImgFrameRect(0,m_lBackImageTop,pimg,&m_imgBackGround,&m_rcBack,0);
	//star field
	g_pStarField->SetDestImage(pimg);
	g_pStarField->Update();
	g_pStarField->Render();
}

//------------------------------ CPlayer::UpdateStatusBar ----------------------------------------
//aggiorna la barra di stato con i valori dei punti energia ecc...
void UpdateStatusBar(void)
{
	//cancella il contenuto della s.b.
	g_lpStatusBar->Blt(NULL,NULL,NULL,DDBLT_COLORFILL,&g_dbxSb);
	
#ifdef _LEVELEDIT_
	
	//visualizza l'ascissa del livello
	g_chLittleRed.TextOut(5,10,&m_imgStatusBar,"%d",CCurLevel.GetCurrentX());

	g_chLittleBlue.TextOut(105,10,&m_imgStatusBar,"%d",CCurLevel.GetCurrentTileIndex(0));

	g_chLittleRed.TextOut(165,10,&m_imgStatusBar,"%d",CCurLevel.GetCurrentTileIndex(1));

	g_chLittleBlue.TextOut(225,10,&m_imgStatusBar,"%d",CCurLevel.GetCurrentTileIndex(2));

	g_chLittleRed.TextOut(290,10,&m_imgStatusBar,"%d",CCurLevel.GetCurrentTileIndex(3));
      
#else
	
	DWORD dwCount;
	DWORD dwCnt;
	DDBLTFX *pbltfx;
	const LONG lspx=495;
	
	if (Player1.dwScore>Player1.dwNextLife && Player1.dwNextLife!=0)
	{
		//una vinta vinta con i punti			
		CSBObject *pobj=NULL;

		//visualizza la label 1up
		pobj=g_sLabel1Up.Pop();

		if (pobj)
		{
			pobj->Reset();
			pobj->SetPosition((LONG)(g_Environ.iScreenWidth*0.5),(LONG)(g_Environ.iScreenHeight*0.5));
			g_objStack.Push(pobj);
		}

		g_cs.PlaySound(g_snFinal,0,0); //effetto sonoro del bonus
		Player1.dwLives++; //incrementa il n� di vite

		//imposta il punteggio successivo
		Player1.dwNextLife+=Player1.dwNextLife+Player1.dwNextLife+50000;
	

	}

    //vite
    g_lpStatusBar->Blt(&g_rcShip,g_imgLabelShip.surface,NULL,DDBLT_KEYSRC,NULL);
	g_chLittleRed.TextOut(59,10,&m_imgStatusBar,"X%d",Player1.dwLives);
	//bombe (arma secondaria)
	g_chLittleBlue.TextOut(100,10,&m_imgStatusBar,"B=%d",Player1.dwShells2);    
	//(energia
	g_chLittleRed.TextOut(178,10,&m_imgStatusBar,"E");    
	//punteggio
	if (g_Environ.iLanguage==1) g_chLittleBlue.TextOut(290,10,&m_imgStatusBar,"SCORE %d",Player1.dwScore);
	else g_chLittleBlue.TextOut(290,10,&m_imgStatusBar,"PUNTI %d",Player1.dwScore);
	    
	if (Player1.GetEnergy()>=0)
	{

		dwCount=Player1.GetEnergy() >> 3; //divide per 8 (il risultato deve essere compreso fra 0 e 10)

		pbltfx=(dwCount<4) ? &g_blxEn1 : &g_blxEn;

		for (dwCnt=1;dwCnt<=dwCount;dwCnt++)
		{
			g_lpStatusBar->Blt(&g_rcEnergy[dwCnt-1],NULL,NULL,DDBLT_COLORFILL,pbltfx);		
		}

	}

	//arma speciale
	if (Player1.dwShells3>0)
	{	
		RECT rc;
		rc.left=0;
		DWORD dw3;
		DWORD dwLv;

		dw3=Player1.dwShells3;

		dwLv=(DWORD)((float)dw3*78/Player1.dwShells3Full);

		g_fm.PutImgFrameClip(lspx,3,&m_imgStatusBar,&m_imgWpIcons[Player1.dwWeapon3]);
		g_fm.PutImgFrameClip(lspx+29,3,&m_imgStatusBar,&m_imgWpFrame);		
		//diseng al'indicatore dell'arma speciale
		g_fm.PutImgFrameResize(lspx+31,6,&m_imgStatusBar,&m_imgWpLevel,dwLv);

	}

#endif

}
//------------------------------- DrawStatus ---------------------------------------------

//disegna su una sup. della flipping chain la status bar
HRESULT DrawStatus(IMAGE_FRAME_PTR imgDisplay)
{

	LPDIRECTDRAWSURFACE7 lpDDS;
	lpDDS=imgDisplay->surface;

	return lpDDS->Blt(&g_rcBar,m_imgStatusBar.surface,NULL,DDBLT_KEYSRC,NULL);
}

//////////////////////////////// Funzioni usate per debug ///////////////////////////////////////////

void DisplayStack(CObjectStack *pStack,TCHAR *title)
{
	
	
#ifdef _TESTGAME_

	DWORD dwIndex,dwCount;
	CSBObject *pobj;
    
	dwCount=pStack->ItemCount();
    
	g_cdb.WriteErrorFile("Stack di %d oggetti (%s) stack top=%d: ",dwCount,title,pStack->GetTop());

	for (dwIndex=0;dwIndex<dwCount;dwIndex++)
	{
		if (pStack->m_objStack[dwIndex])
		{
			pobj=pStack->m_objStack[dwIndex];
			g_cdb.WriteErrorFile("Index=%d Type=%d Status=%d Active=%d x=%d y=%d energy=%d",dwIndex,pobj->GetType(),pobj->dwStatus,pStack->m_objStack[dwIndex]->bActive,pobj->x,pobj->y,pobj->GetEnergy());
		}		
	}

#endif

}

//////////////////////////////////// LinkBuffer ////////////////////////////////////////

HRESULT LinkBuffer(CImgBuffer16 *pDestBuff16,IMAGE_FRAME_PTR pImgSrc)
{
	BYTE *pb=NULL;
	LONG lPitch=0;

	if (pImgSrc == NULL) return E_FAIL;
	if (pImgSrc->status != 1) return E_FAIL;

	//acquisisce il puntatore al buffer della superficie DD
	pb=g_fm.GetBuffer(pImgSrc,&lPitch);
	
	if (pb)
	{

		//imposta il buffer
		pDestBuff16->SetBuffer((WORD *)pb,lPitch/2,pImgSrc->height,pImgSrc->width);
		return S_OK;

	}
	return E_FAIL;		
}


///////////////////////////////////// CloseForm1 //////////////////////////////////////////////////////

//chiude la form pform
int CloseFormExit (void *pform)			
{
	m_frmExit.Close();		
	return 1;
}

int CloseFormContinue (void *pform)
{
	m_frmContinue.Close();
	return 1;
}

////////////////////////////////////// BreakGameLoop /////////////////////////////////////////////////
//esce dal ciclo del gioco e torna alla console
int BreakGameLoop(void *pform)
{
	m_frmExit.Close();
	Player1.bExitGame=TRUE;
	Player1.dwScore=0; //azzera il punteggio 
	return 1;
}

//////////////////////////////////////// ContinueGame ////////////////////////////////////////////////
int ContinueGame(void *pform)
{
	m_frmContinue.Close();

	ScreenTransition(&m_imgCurScreen,&imgBackMenu);

	g_iContinue++;
	Player1.Reset();
	Player1.dwScore=0;
	Player1.dwLives=5;
	Player1.bExitGame=FALSE;
	//riparte in modalit� lampeggio
	Player1.SetBlinking();
	return 1;
}

//----------------------------- AddPath ------------------------------------------------
//Aggiunge un elemento ad un path cinematico di un oggetto
//Angle=angolo
//Velocity=velocit�
//pPath=puntatore al percorso
//La prima volta che si chiama la funzione per un path si deve inizializzare impostando bInit su true
//per le restanti chiamate, non occorre passare pPath e bInit fino a che non si cambia path

HRESULT AddPath(int Angle,int Velocity,int Displ,TP_PATH_DATA_PTR pPath,BOOL bInit)
{
	static LONG dwCount=0;
	static TP_PATH_DATA_PTR pPath1=NULL;

	if (bInit)
	{
		//inizializza per un nuovo path
		dwCount=-1;
		pPath1=pPath;
	}

	if(!pPath1) return E_FAIL;

	dwCount++;

	pPath1[dwCount].Angle=Angle;
	pPath1[dwCount].Velocity=Velocity;
	pPath1[dwCount].Displ=Displ;
	
	return S_OK;
}

//-------------------------- ProgressBar ------------------------------

void ProgressBar(int x,int y,float perc,int width)
{
	g_gm.UseBackBuffer();
	g_gm.DrawRectangle(x,y,x+width,y+22,g_gm.CreateRGB(128,128,128));
	g_gm.DrawRectangle(x+1,y+1,(LONG)(x+(width-2)*perc),y+21,g_gm.CreateRGB(0,80,200));
	g_gm.ShowBackBuffer();
}

//---------------------------- ShowStats ---------------------------------------
//Visualizza le statistiche
void ShowStats(CSbLevel *level)
{
	TCHAR *tx1=TEXT("LIVELLO %d COMPLETATO");
	TCHAR *tx2=TEXT("LEVEL %d COMPLETED");
	TCHAR *tx3=TEXT("OGGETTI NEMICI ABBATTUTI");
	TCHAR *tx4=TEXT("ENEMIES DESTROYED");
	TCHAR *ptx=NULL;
	TCHAR *ptx1;
	int iy=50;
	int dy=0;
	int extra=0; //bonus extra
	float perc;
	DWORD dwTime;
	
	g_gm.UseBackBuffer();
	g_fm.PutImgFrame(0,0,&imgBackMenu,0);

	ptx=g_Environ.iLanguage==0 ? tx1:tx2;
	ptx1=g_Environ.iLanguage==0 ? tx3:tx4;

	//livello x completato
	g_chLittleRed.TextOut((int)((g_Environ.iScreenWidth - g_chLittleRed.GetTextWidth(ptx))*0.5f),30,ptx,g_Environ.iLevel);
	//nemici distrutti
	g_chLittleRed.TextOut((int)((g_Environ.iScreenWidth - g_chLittleRed.GetTextWidth(ptx1))*0.5f),60,ptx1,g_Environ.iLevel);

	iy=90;

	//intestazione tabella
	if (g_Environ.iLanguage == 0)
	{
		//IT
		g_chLittleRed.TextOut(5,iy,"TIPO");
		g_chLittleRed.TextOut(230,iy,"DISTRUTTI");
		g_chLittleRed.TextOut(420,iy,"TOTALI");	

	}
	else
	{
		//EN
		g_chLittleRed.TextOut(5,iy,"TYPE");
		g_chLittleRed.TextOut(230,iy,"DESTROYED");
		g_chLittleRed.TextOut(420,iy,"TOTAL");	

	}

	g_chLittleRed.TextOut(530,iy,"BONUS");

	//visualizza i risultati della statistica
	for (int cnt=1;cnt<EN_LAST;cnt++)
	{
		if (level->m_Stats[cnt].iTot && level->m_Stats[cnt].iHit && dy<400)
		{			
			iy=dy*20+140;

			dy++;
		
			g_chLittleBlue.TextOut(5,iy,GetEnemyName(cnt,g_Environ.iLanguage));				
		
			if (level->m_Stats[cnt].iHit>level->m_Stats[cnt].iTot) level->m_Stats[cnt].iHit=level->m_Stats[cnt].iTot;
		
			g_chLittleRed.TextOut(300,iy,"%d",level->m_Stats[cnt].iHit);
			g_chLittleBlue.TextOut(420,iy,"%d",level->m_Stats[cnt].iTot);
			perc=(float)level->m_Stats[cnt].iHit*100/level->m_Stats[cnt].iTot;
			
			//bonus extra
            if (perc>85 && perc<95) 
			{
				g_chLittleRed.TextOut(500,iy,"500");
				extra+=500;
			}
			else if (perc>=95) 
			{
				extra+=1000; //oltre questa percentuale da 1000 punti in piu'
				g_chLittleBlue.TextOut(500,iy,"1000");		
			}
		
		}
	}

	g_chLittleRed.TextOut(5,g_Environ.iScreenHeight - 30,"EXTRA BONUS %d ",extra);
	Player1.dwScore += extra; //bonus extra
	if (g_Environ.iLanguage == 1) g_chLittleBlue.TextOut(400,g_Environ.iScreenHeight - 30,"SCORE %d",Player1.dwScore);
	else g_chLittleBlue.TextOut(400,g_Environ.iScreenHeight - 30,"PUNTI %d",Player1.dwScore);

	//visualizza il back buffer
	g_gm.ShowBackBuffer();

	dwTime=timeGetTime();

	g_ci.ResetKyb();

	//attende fino a che non si preme un tasto per continuare oppure � passato un certo tempo
	do {
		
		g_ci.GetKybStatus();

	} while(!g_ci.GetCurrentKeyDown(0,255) && timeGetTime()-dwTime<19000);

}


//----------------------------------- GetEnemyName ----------------------------------------

//restituisce il nome del nemico dato l'id e il linguaggio
TCHAR *GetEnemyName(int id,int language)
{
	static int iu=sizeof(m_enemy_names)/sizeof(m_enemy_names[0]); //dimensione vettore

	for (int cnt=0;cnt<iu;cnt++)
	{
		if (m_enemy_names[cnt].ivalue==id)
		{
			return (language==0 ? m_enemy_names[cnt].svalue1:m_enemy_names[cnt].svalue2);
		}
	}

	return NULL;
}


void CreateAuxForms(void)
{
	
	//controlli della form che viene visualizzata per chiedere se si
	//vuol uscire dal gioco (vedi fun. AskForExit)
	mnuExitTitle.SetGraphicManager(&g_gm);
	mnuExitTitle.SetFrameManager(&g_fm);
	mnuExitTitle.SetCharSet(&g_chRed);
	//USCIRE DAL GIOCO?
	mnuExitTitle.Create(50,70,TranslateGameMsg(33),0);

	mnuExitYes.SetGraphicManager(&g_gm);
	mnuExitYes.SetFrameManager(&g_fm);
	mnuExitYes.SetCharSet(&g_chRed);
	//SI
	mnuExitYes.Create(120,200,TranslateGameMsg(42),0);
	mnuExitYes.SetAction(BreakGameLoop); //premendo Si si esce dal ciclo e torna alla console

	mnuExitNo.SetGraphicManager(&g_gm);
	mnuExitNo.SetFrameManager(&g_fm);
	mnuExitNo.SetCharSet(&g_chRed);
	//NO
	mnuExitNo.Create(480,200,TranslateGameMsg(43),0);
	mnuExitNo.SetAction(CloseFormExit);   

	m_frmExit.SetGraphicManager(&g_gm);
	m_frmExit.SetFrameManager(&g_fm);
	m_frmExit.AddControl(&mnuExitTitle,FALSE);
	m_frmExit.AddControl(&mnuExitYes,TRUE);
	m_frmExit.AddControl(&mnuExitNo,TRUE);
	m_frmExit.MoveTo(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);

	//form continue
	mnuContinueTitle.SetGraphicManager(&g_gm);
	mnuContinueTitle.SetFrameManager(&g_fm);
	mnuContinueTitle.SetCharSet(&g_chRed);
	//USCIRE DAL GIOCO?
	mnuContinueTitle.Create(100,120,TranslateGameMsg(47),0); //continuare ?

	mnuContinueYes.SetGraphicManager(&g_gm);
	mnuContinueYes.SetFrameManager(&g_fm);
	mnuContinueYes.SetCharSet(&g_chRed);
	//SI
	mnuContinueYes.Create(120,250,TranslateGameMsg(42),0);
	mnuContinueYes.SetAction(ContinueGame); //premendo Si si esce dal ciclo e torna alla console

	mnuContinueNo.SetGraphicManager(&g_gm);
	mnuContinueNo.SetFrameManager(&g_fm);
	mnuContinueNo.SetCharSet(&g_chRed);
	//NO
	mnuContinueNo.Create(480,250,TranslateGameMsg(43),0);
	mnuContinueNo.SetAction(CloseFormContinue);    	
		
	m_frmContinue.SetGraphicManager(&g_gm);
	m_frmContinue.SetFrameManager(&g_fm);
	m_frmContinue.AddControl(&mnuContinueTitle,FALSE);
	m_frmContinue.AddControl(&mnuContinueYes,TRUE);
	m_frmContinue.AddControl(&mnuContinueNo,TRUE);
	m_frmContinue.MoveTo(SCREEN_WIDTH/2,SCREEN_HEIGHT/2);

}




