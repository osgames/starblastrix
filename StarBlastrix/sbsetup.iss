;Script per creazione pachetto di installazione di Starblastrix
;Code by Leonardo Berti (c) 2006

[Setup]
AppName=Starblastrix
AppVerName=Starblastrix 1.0
DefaultDirName={pf}\Starblastrix
DefaultGroupName=Starblastrix
UninstallDisplayIcon={app}\Starblastrix.exe
OutputBaseFileName=sb_setup
OutputDir=Setup
WizardImageFile="wizlogo.bmp"
LicenseFile=license.txt

Uninstallable=yes

[Files]
Source: "Starblastrix.exe"; DestDir: "{app}";
Source: "data\*.*"; DestDir: "{app}\data";
Source: "data\extra_levels\*.sbl"; DestDir: "{app}\data\extra_levels";
Source: "music\*.mid"; DestDir: "{app}\music";
Source: "wavs\*.wav"; DestDir: "{app}\wavs";
Source: "risorse\score.bin"; DestDir: "{app}";
Source: "readme.txt"; DestDir: "{app}";
Source: "leggimi.txt"; DestDir: "{app}";
Source: "license.txt"; DestDir: "{app}";
Source: "risorse\config.bin"; DestDir: "{app}";

[Icons]
Name: "{group}\Starblastrix"; WorkingDir: "{app}"; Filename: "{app}\Starblastrix.exe"

[Run]
Filename: "{app}\Starblastrix.exe"; WorkingDir: "{app}"; Description: "Esegui Starblastrix"; Flags:postinstall unchecked
Filename: "{app}\leggimi.txt"; Description: "Apri il file leggimi.txt"; Flags:postinstall shellexec



