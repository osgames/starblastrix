//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  tent.cpp -- tentacolo meccanico
///////////////////////////////////////////////////////////////////////*/

#ifndef _TENTACLE_INCLUDE_
#define _TENTACLE_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "mcasmbl.h"

typedef enum TENTACLE_STATE_TAG
{
	TN_FOLLOW_TARGET=0, //insegue il player
    TN_FOLLOW_PATTERN=1, //si sta muovendo secondo un pattern prestabilito
	TN_MOVE_JAWS=2	

} TENTACLE_STATE;

//il tentacolo puo' avere o no testa e/o tenaglie

class CTentacle:public CMechAssembly
{
private:


protected:

	int m_iNumItems; //numero di elementi che compongono il corpo
	CMechComponent m_pHead;
	CMechComponent m_pLeftJaw; //tenaglia di sinistra
	CMechComponent m_pRightJaw; //tenaglia di destra
	CMechComponent *m_pmc; //general purpose
	CADXFastMath *m_pmath;
	VERTEX2D m_vBodyJoints[2];
	VERTEX2D m_vLeftJawJoints[2]; //giunti di collegamento della tenaglia 
	VERTEX2D m_vRightJawJoints[2];
	VERTEX2D m_vHeadJoints; 
	bool m_bBodyJoints; //flag che indicano se i vari giunti sono stati impostati
	bool m_bLeftJawJoints;
	bool m_bRightJawJoints;
	bool m_bHeadJoints;	
	int *m_ialpha;
	int *m_itarget_config;
	int m_inumalpha; //numero di elementi in g_ialpha
//	void DamageComponent(CMechComponent *pmc,CSBObject *pobj); //danneggia un elemento 
	int m_seq1[30];
	int *m_pcurseq;
	int m_iseqindex;
	TENTACLE_STATE m_tentacle_state;
	int m_iper1; //usato d algo AI
	int m_iperwidth; //usato da algo AI
	int m_ijaws_state; //stto della bocca 0=normale 1=si sta prendo 2=si ste richiudendo
	int m_ijaws_step; //passo apertura/chiusura bocca
	int m_isteady; //fermo perch� � vicono al player
//	void ExplodeChain(CMechComponent *pmc);

public:

	CMechComponent *m_pBody; //corpo
	CTentacle(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,IMAGE_FRAME_PTR pimgJawLeft=NULL,IMAGE_FRAME_PTR pimgJawRight=NULL);
	CTentacle(void);
	virtual ~CTentacle(void);
	HRESULT Create(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,IMAGE_FRAME_PTR pimgJawLeft=NULL,IMAGE_FRAME_PTR pimgJawRight=NULL);
	void SetBodyJoints(int x1,int y1,int x2,int y2);
	void SetLeftJawJoints(int xb,int yb,int xj,int yj); //coordinate delle coppie rotoidali sulla corpo e sulla tenaglia
	void SetRightJawJoints(int xb,int yb,int xj,int yj);
	void SetHeadJoint(int xh,int yh); //giunto sulla testa
	void DecayAlpha(float decay); //funzioni di movimento
	void PushAlpha(int alpha); //impone un agolo di rotazione alla testa che poi si propaga su tutta la lunghezza
	void ResetAlpha(void);
	CMechComponent *GetRootComponent(void);
	HRESULT Draw(void);
	BOOL Update(void);
	void Reset(void);
	void SetEnergy(LONG energy);
	BOOL DoCollisionWith(CSBObject *pobj);
	void SetMathModule(CADXFastMath *pmath);
	void Explode(void);

};


#endif