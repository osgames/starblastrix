//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mecharm.h -- braccio meccanico
///////////////////////////////////////////////////////////////////////*/

#ifndef _MECHARM_INCLUDE_
#define _MECHARM_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "mcasmbl.h"

//braccio meccanico composta da tre bielle, un pistone e il cannone finale
class CMechArm:public CMechAssembly
{
private:

	CMechComponent *m_pmc; //general purpose
	BOOL m_bConfigCompleted;
	BOOL m_bAutoConfig;
	int m_ibktr;
	int m_ibkstep;

protected:

	int m_iEnergy[5]; //energia di ogni singolo elemento del braccio meccanico	
//	void DamageComponent(CMechComponent *pmc,CSBObject *pobj);
	float m_fNextConfiguration[4]; //vettore con le coordinate lagrangiane che definisce la successiva configurazione
	float m_fCurrentConfiguration[4]; //configurazione corrente
	float m_fConfigDeltas[4]; //delta necessari a raggiungere la config. successiva	
	BOOL m_bRandom; //introduce movimenti random

public:

	//componenti del braccio meccanico
	CMechComponent m_Crank1,m_Crank2,m_Crank3; //m_Crank1=root component
	CMechComponent m_Piston;
	CMechComponent m_Cannon;
	CMechArm(void);
	void Reset(void);
	BOOL Update(void);
	HRESULT Draw(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	void Explode(void);
	//imposta l configurazione alla quale si vuol arrivare
	void SetNextConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston);
	//imposta la configurazione corrente
	void SetCurrentConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston);
    //imposta la configurazione a cui si deve arrivare
	void SetTargetConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston,int delta=15,int freq=6);
	//aggiorna i delta
	void UpdateDeltas(int isteps=10);
	//aggiorna i delta (da usare con UpdateConfig1)
	void UpdateDeltas1(int idelta=15,int isteps=30);
	//acquisisce le coordinate lagrangiane correnti
	void StoreCurrentConfig(void);
	//varia la configurazione, se ha raggiunto la configurazione finale rende true altrimenti false
    BOOL UpdateConfig(void);
	//� come Update config solo che quando un elemento raggionge la posizione finale 
	//non si sposta piu' 
	BOOL UpdateConfig1(void);
	//applica la configurazione corrente
	void ApplyCurrentConfig(void);
	void SetDefaultConfig(void);
	//rende true se la configurazione target � stata raggiunta
	BOOL IsConfigCompleted(void);
//	void ExplodeChain(CMechComponent *pmc);
	//imposta il flag di aggiornamento autometico della configurazione
	void SetAutoConfig(BOOL bVal);
	//imposta il movimento random
	void SetRandom(BOOL val);

};

//crea uno stack di inum oggetti di tipo mecharm
//si ocupa anche di caricare le frames
HRESULT CreateMechArmStack(CObjectStack *pstk,int inum);
HRESULT LoadMechArmResources(CResourceManager *prm);
HRESULT FreeMechArmResources(CADXFrameManager *pfm);
HRESULT CreateMechArm(CMechArm *p,CADXFrameManager *pfm,int itype=0);

#endif