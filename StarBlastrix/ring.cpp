//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  ring.cpp -- anello nemico
///////////////////////////////////////////////////////////////////////*/

#include "ring.h"
#include "sbengine.h"

#define NRINGFRAMES 18

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern CObjectStack g_sEShell11;
//stack oggetti attivi
extern CObjectStack g_objStack;
extern CADXSound g_cs;
extern LONG g_snFire9;
extern void PushExplMulti(CADXSprite*,int);


IMAGE_FRAME m_imgRing[NRINGFRAMES];
BOOL m_bRingResLoaded=FALSE;

HRESULT LoadRingRes(CResourceManager* prm)
{
	if (m_bRingResLoaded) return S_OK;

	if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

	CADXFrameManager* fm=prm->GetFrameManager();

	if (NULL==fm) return E_FAIL;

	//immagine corrente
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (FAILED(fm->GrabFrameArray(m_imgRing,pimg,12,0,602,100,172))) return E_FAIL;
	if (FAILED(fm->GrabFrameArray(&m_imgRing[12],pimg,6,0,774,100,172))) return E_FAIL;

	m_bRingResLoaded=TRUE;

	return S_OK;

}

void FreeRingRes(void)
{
	if (m_bRingResLoaded)
	{
		for (int count=0;count<NRINGFRAMES;count++) g_fm.FreeFrameArray(m_imgRing,NRINGFRAMES);
		m_bRingResLoaded=FALSE;
	}
}


HRESULT CreateRingStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	if (!prm) return E_FAIL;

	CRing* ps=NULL;

	if (!pstk) return E_FAIL;
    //carica le risorse per il serpente di tipo 2
	if (FAILED(LoadRingRes(prm))) return E_FAIL;
	
	if (FAILED(pstk->Create(inumitems))) return E_FAIL;	

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CRing;
		ps=(CRing*)pstk->m_objStack[count];
		ps->bActive=FALSE;
		ps->SetGraphicManager(prm->m_lpGm);
		ps->SetFrameManager(prm->m_lpFm);
		ps->SetUpdateFreq(40);
		ps->SetEnergy(25);
		ps->dwPower=10;
		ps->dwScore=2000;
		ps->dwResId=EN_RING;
		ps->lpfnExplode=PushExpl9;
		ps->pfnCollide=PushCollideMetal;
		ps->SetActiveStack(&g_objStack);		
		ps->SetCartridgeStack(&g_sEShell11); //proiettili rossi	
		ps->SetFireSound(g_snFire9);
		//serpente rosso di default
		for (DWORD j=0;j<NRINGFRAMES;j++)
		{
			ps->AddFrame(&m_imgRing[j]);
		};

		//imposta la posizione della bocca del cannone per le varie frames
		ps->SetFrameHotSpot(0,0,0,18);
		ps->SetFrameHotSpot(1,0,0,20);
		ps->SetFrameHotSpot(2,0,0,27);
		ps->SetFrameHotSpot(3,0,0,46);
		ps->SetFrameHotSpot(4,0,0,70);
		ps->SetFrameHotSpot(5,0,0,98);
		ps->SetFrameHotSpot(6,0,0,125);
		ps->SetFrameHotSpot(7,0,0,144);
		ps->SetFrameHotSpot(8,0,0,150);
		ps->SetFrameHotSpot(9,0,0,154);
		ps->SetFrameHotSpot(10,0,0,149);
		ps->SetFrameHotSpot(11,0,0,133);
		ps->SetFrameHotSpot(12,0,0,114);
		ps->SetFrameHotSpot(13,0,0,96);
		ps->SetFrameHotSpot(14,0,0,75);
		ps->SetFrameHotSpot(15,0,0,56);
		ps->SetFrameHotSpot(16,0,0,43);
		ps->SetFrameHotSpot(17,0,0,32);
		
	}


	return S_OK;

};


//--------------------------------------- CRing ------------------------------------------------
/*
class CRing:public CEnemy
{
private:
	

public:

	void Reset(void);
	BOOL Update(void);

};
*/

CRing::CRing()
{
	dwClass=CL_ENEMY;

};

void CRing::Reset(void)
{
	CEnemy::Reset();
	SetCurFrame(0);
	SetVelocity(180,2);
	SetAnimSpeed(0.2f);
	m_anim_acc=0.0f;
	iUpdateFreq=190;
	memset(&rcLimitBox,0,sizeof(RECT));
	bBoxActive=FALSE;
	bFollowPlayer=FALSE;
	burst_cnt=0;
};


void CRing::UpdateAnimSpeed(void)
{
	float s=GetAnimSpeed();

	s+=m_anim_acc;

	//limita la velocit� di animazione;
	if ((s>0.6f && m_anim_acc>0) || (s<-0.6f && m_anim_acc<0)) s-=m_anim_acc;
	
	SetAnimSpeed(s);

};

BOOL CRing::Update(void)
{
	if (++iStepUpdate>iUpdateFreq)
	{
		iStepConfig++;
		iStepUpdate=0;
		bBoxActive=FALSE;
		bFollowPlayer=FALSE;

		switch (iStepConfig)
		{
		case 1:

			SetVelocity(0,0);
			iUpdateFreq=150;
			m_anim_acc=-0.005f;
			break;

		case 2:

			iUpdateFreq=40;
			if (lTargetY<y-50) SetVelocity(270,2);
			else if (lTargetY>y+60) SetVelocity(90,2);
			m_anim_acc=0.005f;
			rcLimitBox.left=0;
			rcLimitBox.top=0;
			rcLimitBox.bottom=450;
			rcLimitBox.right=640;
			//appena esce fuori dal box ricalcola la traiettoria
			bBoxActive=TRUE;
			break;

		case 3:

			iUpdateFreq=150;
			m_anim_acc=0.0f;
			rcLimitBox.left=0;
			rcLimitBox.top=0;
			rcLimitBox.bottom=450;
			rcLimitBox.right=640;
			//appena esce fuori dal box ricalcola la traiettoria
			bBoxActive=TRUE;

			if (x>320) SetVelocity(180,3);
			else SetVelocity(0,3);

			break;
			
		case 4:

			iUpdateFreq=120;
			//punta il player
			{
				int an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);
				SetVelocity(an,2);
			}
			m_anim_acc=0.005f;
			rcLimitBox.left=0;
			rcLimitBox.top=0;
			rcLimitBox.bottom=450;
			rcLimitBox.right=640;
			//appena esce fuori dal box ricalcola la traiettoria
			bBoxActive=TRUE;

			break;

		case 5:		

			m_anim_acc=-0.001f;
			bFollowPlayer=TRUE;
			iUpdateFreq=150;			

			break;

		case 7:

			m_anim_acc=-m_anim_acc;
			iStepConfig=g_fmath.RndFast(14);
			break;

		case 8:
		case 6:

			//si porta sul lato destro dello schermo
			SetVelocity(0,4);
			rcLimitBox.left=0;
			rcLimitBox.top=0;
			rcLimitBox.bottom=450;
			rcLimitBox.right=510;
			iUpdateFreq=240;
			//appena esce fuori dal box ricalcola la traiettoria
			bBoxActive=TRUE;
			m_anim_acc=(-m_anim_acc)*2;
			break;	


		case 9:

			//esce di scena
			SetVelocity(180,3);
			iUpdateFreq=500;

			break;

		default:

			iStepConfig=0;
			break;

		};
	};

	UpdateAnimSpeed();
	UpdateFrame();
	UpdatePosition();

	if (bFollowPlayer)
	{
		int an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);
		SetVelocity(an,2);

	};

	if (bBoxActive)
	{
		//controlla se va fuori dal box di contenimento
		//in questo caso deve ricalcolare la traiettoria
		if (y<rcLimitBox.top || y>rcLimitBox.bottom || x<rcLimitBox.left || x>rcLimitBox.right)
		{
			//lo ferma
			SetVelocity(0,0);
			//ricalcola la traiettoria al prossimo passaggio
			iStepUpdate=iUpdateFreq;
		}

	};

	if (abs(lTargetY-(y+80))<100 && burst_cnt<=0)
	{
		burst_cnt=8;
		iFireFreq=12;
		iStepFire=iFireFreq;
	}

	if (++iStepFire>iFireFreq)
	{
		iStepFire=0;

		if (burst_cnt>0 && m_Cartridges)
		{
			CShell* p=(CShell*)m_Cartridges->Pop(); 

			if (p)
			{

				p->Reset();
				p->SetCurFrame(0);
				p->SetVelocity(180,12);
				p->SetEnergy(2);
				p->dwPower=10;
				p->dwClass=CL_ENFIRE;
				LONG xf,yf;
				GetJointPosition(&xf,&yf,0);
				p->SetPosition(xf,yf);

				if (m_ActiveStack)
				{
					m_ActiveStack->Push(p);
					g_cs.PlaySound(m_lFireSound,0,0);

				}

				burst_cnt--;

			}

		}
	}

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return (x>-200 && x<1000 && y>-200 && y<500);
};

void CRing::Explode(void)
{
	PushExplMulti(this,5);
}