//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  resman.cpp -- resource manager, funzioni per caricamento risorse
///////////////////////////////////////////////////////////////////////*/

#include "a32graph.h"
#include "resman.h"
#include <tchar.h>

//costruttore
CResourceManager::CResourceManager(void)
{
	g_imgAuxImg.status=0;
	g_szAuxResFile=NULL;
	g_dwCurVpxFrame=0;
	tmp=NULL;
}

//distruttore
CResourceManager::~CResourceManager(void)
{
	this->FreeResources();
}

//carica in memoria il file di immagini.
//Se il file di immagini � gi� caricato non lo ricarica
HRESULT CResourceManager::LoadAuxResFile(TCHAR *szFileName,DWORD dwFrame)
{
	if (!szFileName) return E_FAIL;

	if (!m_lpFm) return E_FAIL; //frame manager non settato

	if (g_szAuxResFile && (0==_tcscmp(szFileName,g_szAuxResFile)) && (dwFrame==g_dwCurVpxFrame)) return S_OK; //file gi� caricato

	else
	{
		//rilascia l'immagine precedentemente caricata e si prepara a caricarne una nuova
		this->FreeResources();
		//carica il file di risorse grafiche
		if (FAILED(m_lpFm->CreateImgFrameFromVPX(&g_imgAuxImg,szFileName,dwFrame))) return E_FAIL;
        //frame corrente caricata
		g_dwCurVpxFrame=dwFrame; 

		g_szAuxResFile=new TCHAR[_tcsclen(szFileName)+1];

		memset(g_szAuxResFile,0,(_tcsclen(szFileName)+1)*sizeof(TCHAR));
        //imposta il nome del file di risorse corrente
		memcpy(g_szAuxResFile,szFileName,sizeof(TCHAR)*_tcsclen(szFileName));

		return S_OK;
	}
}

//Carica nel vettore di frames pArray un striscia orizzontale di dwFrames frames
//a partire dal punto px,py e con dimensioni di w,h
HRESULT CResourceManager::GrabFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h)
{
	DWORD dwCount;
	RECT rc;
	HRESULT hr;

	if (!g_imgAuxImg.status) return E_FAIL;
	if (!m_lpFm) return E_FAIL;

	if (pArray)
	{
	
		for (dwCount=0;dwCount<dwFrames;dwCount++)
		{
			rc.left=px+(dwCount*w);
			rc.top=py;
			rc.bottom=rc.top+h;
			rc.right=rc.left+w;

			hr=m_lpFm->GrabFrame(&pArray[dwCount],&rc,&g_imgAuxImg);

			if (FAILED(hr)) return hr;
			
		}
	}

	else return E_FAIL;
	return S_OK;

}


//Carica un vettore di frame disposte in senso vertcale
//i parametri hanno lo stesso significato della funzione precedente
HRESULT CResourceManager::GrabVFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h)
{
	DWORD dwCount;
	RECT rc;
	HRESULT hr;

	if (!m_lpFm) return E_FAIL;

	if (g_imgAuxImg.status>0 && pArray)
	{
	
		for (dwCount=0;dwCount<dwFrames;dwCount++)
		{
			rc.left=px;
			rc.top=py+(dwCount*h);
			rc.bottom=rc.top+h;
			rc.right=rc.left+w;

			hr=m_lpFm->GrabFrame(&pArray[dwCount],&rc,&g_imgAuxImg);

			if (FAILED(hr)) return hr;
			
		}
	}

	else return E_FAIL;
	return S_OK;

}


//rilascia l'immagine temporanea ed evntuale altra memoria allocata
void CResourceManager::FreeResources(void)
{
	if (m_lpFm)
	{
		m_lpFm->FreeImgFrame(&g_imgAuxImg);

		SAFE_DELETE_ARRAY(g_szAuxResFile);

		SAFE_DELETE_ARRAY(tmp); //nome temporaneo del file di risorse

		g_dwCurVpxFrame=0;
	}
}


IMAGE_FRAME_PTR CResourceManager::GetCurrentImage(void)
{	
	return &g_imgAuxImg;
}


//restituisce il nome del file di risorse corrente
TCHAR *CResourceManager::GetCurrentFileName(void)
{
	size_t sz;

	SAFE_DELETE_ARRAY(tmp);

	if (!g_szAuxResFile) return NULL;
	
	sz=_tcsclen(g_szAuxResFile)+1;
	
	tmp=new TCHAR[sz*sizeof(TCHAR)];

	memset(tmp,0,sz);

	memcpy(tmp,g_szAuxResFile,(sz-1)*sizeof(TCHAR));

	return tmp;
}


//restituice l'indice dell'immagine corrente
DWORD CResourceManager::GetCurrentImageIndex(void)
{
	return g_dwCurVpxFrame;
}