/* 
   Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
thunder.cpp - fulmine da oggetto a oggetto  
*/

#include "a32graph.h"
#include "a32object.h"
#include "a32sprit.h"
#include "thunder.h"
#include "enemy.h"
#include "sbengine.h"

extern CPlayer Player1;

//////////////////////////////////////////// classe CThunderBolt /////////////////////////////////////////

CThunderBolt::CThunderBolt(void)
{	

	m_iUpdateFreq=1;
	m_iDuration=5;
	m_ifrleft=m_ifrright=m_ifrtop=m_ifrbottom=-1;

	dwClass=CL_NONE;
	
	CSBObject::Reset();
	this->Reset();
}

void CThunderBolt::Reset(void)
{
	int count;
	m_itcount=0;
	for (count=0;count<MAX_TARGET;count++) m_Targets[count]=NULL;
	m_itx=m_ity=0;
	m_pSource=NULL;
	m_itcount=0;
	m_iItemCount=0;
	m_iStepUpdate=m_iUpdateFreq-1;
	m_iTimer=0;
	m_hx=m_hy=0;
}

//imposta le frame finali ileft=frame rivolta verso sinistra,top=rivolta in alto ecc...
HRESULT CThunderBolt::SetFinalFrames(int ileft,int iright,int itop,int ibottom)
{
	if (ileft<=max_frame_index && iright<=max_frame_index && ibottom<=max_frame_index)
	{
		m_ifrleft=ileft;
		m_ifrtop=itop;
		m_ifrbottom=ibottom;
		m_ifrright=iright;

		return S_OK;
	}

	else return E_FAIL;
}

//aggiunge un target alla lista
HRESULT CThunderBolt::AddTarget(CSBObject *pTarget)
{
	if (m_itcount>=MAX_TARGET || !pTarget) return E_FAIL;

	m_Targets[m_itcount++]=pTarget;

	return S_OK;
}

HRESULT CThunderBolt::AddTargetXY(int tx,int ty)
{
	m_itx=tx;
	m_ity=ty;
	return S_OK;
}

HRESULT CThunderBolt::SetSource(CADXSprite *pSource)
{
	if (!pSource) return E_FAIL;

	m_pSource=pSource;

	//coordinate hot spot del punto di partenza del fulmine
	m_hx=m_hy=0;

	return S_OK;
}

/*
Imposta la sorgente , hx e hy sono le coordinate dell'hot spot
*/

HRESULT CThunderBolt::SetSource(CADXSprite *pSource,LONG hx,LONG hy)
{

	if (!pSource) return E_FAIL;

	m_pSource=pSource;

	//coordinate hot spot del punto di partenza del fulmine
	m_hx=hx;
	m_hy=hy;

	return S_OK;

}

//imposta la frequenza di aggiornamento
void CThunderBolt::SetUpdateFreq(int freq)
{
	m_iUpdateFreq=freq;
}

//durata in cicli del fulmine
void CThunderBolt::SetDuration(int idur)
{
	m_iDuration=idur;
}

BOOL CThunderBolt::Update(void)
{
	int xs,ys;
	int count;
	int tx,ty;
	int xs1,ys1;
	int ilast;
	int iind;
	static ifrag=0;	

	m_iStepUpdate++;

	if (m_iStepUpdate>=m_iUpdateFreq)
	{
		m_iItemCount=0;
		m_iStepUpdate=0;
     
		//acquisisce le coordinate assolute dell'oggetto
		xs=m_pSource->x;
		ys=m_pSource->y;

		//aggiunge la traslazione dovuta all'hot spot
		xs+=m_hx;
		ys+=m_hy;

		ilast=m_iItemCount;

		if (m_itx || m_ity)
		{
			CreateThunder(xs,ys,m_itx,m_ity);
		}

		ifrag++;
		
		if (ifrag>=4) ifrag=0; //serve a rendere meno potente il fulmine altrimenti distrugge tutto subito!

		for (count=0;count<m_itcount;count++)
		{
			m_pobj=m_Targets[count];
			if (m_pobj)
			{
			
				if (m_pobj->bActive)
				{
					//coordinate assolute del bersaglio
					m_pobj->GetAbsXY(&tx,&ty);
					
					if (ifrag==0) 
					{
						//toglie energia al nemico colpito
						m_pobj->IncrEnergy(-2);
						if (m_pobj->GetEnergy() <= 0)
						{
							
							if (m_pobj->lpfnExplode) 
							{
								int absx,absy;
								//acquisisce le coordinate assolute dell'oggetto
								m_pobj->GetAbsXY(&absx,&absy);
								m_pobj->lpfnExplode(absx,absy);					
							}

							if (dwClass==CL_PLFIRE) 
							{
								Player1.dwScore+=m_pobj->dwScore;

							}

						}
					}

					if (m_iItemCount-ilast>2)
					{
						iind=ilast+2;
						xs1=m_xt[iind]+delta_x[m_iFrame[iind]][1];
						ys1=m_yt[iind]+delta_y[m_iFrame[iind]][1];
					}

					else 
					{
						xs1=xs;ys1=ys;
					}

					ilast=m_iItemCount;
					CreateThunder(xs1,ys1,tx,ty);					
				}
			}			
		}

	}

	return (++m_iTimer<m_iDuration);
}

//crea un fulmine da xs,ys a xd,yd
BOOL CThunderBolt::CreateThunder(int xs,int ys,int xd,int yd)
{
	int xl;
	int xout,yout;
	int count;
	int x1,y1;
	int icur;
	int xlcur;
	int dx,dy,d1,d2;
	int ifr;
	int iHotSpotIn;
	int iHotSpotOut;
			
	if (m_iItemCount>=MAX_THITEMS) return FALSE; //numero item esauriti

	x1=xs,y1=ys;

	icur=0;
	xlcur=5000;

	while(m_iItemCount<MAX_THITEMS && xlcur>4000)
	{
		//cerca la miglior frame 
		for (count=0;count<max_frame_index;count++)
		{
			ifr=count;
			dx=delta_x[ifr][1]-delta_x[ifr][0];
		    dy=delta_y[ifr][1]-delta_y[ifr][0];
		    xout=x1+dx;
		    yout=y1+dy;

			d1=xd-xout;
		    d2=yd-yout;

		    //calcola la distanza fra il punto di uscita per questa frame e il target
		    xl=d1*d1+d2*d2;

		    if (count==0) 
			{
			   iHotSpotIn=0;
			   xlcur=xl;icur=ifr;
			}
		    else if (xl<xlcur) 
			{
			   //miglior frame fino a questo punto
               iHotSpotIn=0;
			   xlcur=xl; //distanza dal punto di arrivo al quadrato
			   icur=ifr;
			}

			//prova la frame scambiando l'ingresso e l'uscita (ingresso hotspot=1 uscita hotspot=0)
			dx=delta_x[ifr][0]-delta_x[ifr][1];
		    dy=delta_y[ifr][0]-delta_y[ifr][1];
		    xout=x1+dx;
		    yout=y1+dy;

			d1=xd-xout;
		    d2=yd-yout;

		    //calcola la distanza fra il punto di uscita per questa frame e il target
		    xl=d1*d1+d2*d2;

		    if (count==0) 
			{
			   iHotSpotIn=1;
			   xlcur=xl;icur=ifr;
			}
		    else if (xl<xlcur) 
			{
			   //miglior frame fino a questo punto
               iHotSpotIn=1;
			   xlcur=xl; //distanza dal punto di arrivo al quadrato
			   icur=ifr;
			}
		}

		//determina l'hotspot di uscita
		iHotSpotOut = (iHotSpotIn == 1) ? 0 : 1;
		//inserisce il primo elemento in coda
		m_iFrame[m_iItemCount]=icur;
		//coordinate assolute di output del primo tile
		//ingresso
		m_xt[m_iItemCount]=x1-delta_x[icur][iHotSpotIn];
		m_yt[m_iItemCount]=y1-delta_y[icur][iHotSpotIn];
		//uscita
		x1=m_xt[m_iItemCount]+delta_x[icur][iHotSpotOut];
		y1=m_yt[m_iItemCount]+delta_y[icur][iHotSpotOut];

		m_iItemCount++; //incrementa il contatore stack	
	}

	//immette la frame finale
	//determina la direzione dell'ultima frame
	d1=x1-m_xt[m_iItemCount];
	d2=y1-m_yt[m_iItemCount];
	icur=-1;

	if (abs(d1)>abs(d2))
	{
		//orizzontale
		if (d1>0 && m_ifrright>=0) 
		{	//rivolta a destra
			icur=m_ifrright;
		}
		else if (m_ifrleft>=0)
		{
			//rivolta a sinistra
			icur=m_ifrleft;
		}
	}
	else
	{
		//verticale
		if (d2>0)
		{
			//rivolta in basso
			icur=m_ifrbottom;
		}
		else
		{
			//rivolta in alto
			icur=m_ifrtop;
		}
	}

	if (icur>=0)
	{
		//aggiunge una frame finale
		if (m_iItemCount<MAX_THITEMS)
		{		
			m_xt[m_iItemCount]=x1-delta_x[icur][0];
		    m_yt[m_iItemCount]=y1-delta_y[icur][0];
			m_iFrame[m_iItemCount]=icur;
			m_iItemCount++;
		}

	}

	return TRUE;
}

HRESULT CThunderBolt::Draw(void)
{
	HRESULT hr;
	static IMAGE_FRAME_PTR pimg=NULL;

	for (int count=0;count<m_iItemCount;count++)
	{
		pimg=frames[m_iFrame[count]];

		if (pimg)
		{
			hr=m_lpFm->PutImgFrameRect(m_xt[count],m_yt[count],m_pimgOut,pimg,&pimg->rcClip);
		}
	}

	return hr;
}

