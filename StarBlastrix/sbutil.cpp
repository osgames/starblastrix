////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
///////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  GUI basata su arcade32
/*--------------------------------------------------------------------*/


#define WIN32_LEAN_AND_MEAN   

//nota: la seguente definizione � stata necessaria per 
//compilare con l'sdk directx 9
//utilizza la versione 7 di direct input
#define DIRECTINPUT_VERSION 0x0700


#include "sbutil.h"
#include <tchar.h>
#ifdef _DEBUG_
#include <crtdbg.h>
#endif

//////////////////////////////////// CSbControl ///////////////////////////////

//Costruttore
CSbControl::CSbControl(void)
{
	iType=0;
	iStyle=0;
	szText=NULL;
	pCChar=NULL;
	lLeft=0;
	lTop=0;
	lWidth=0;
	lHeight=0;
	m_bActive=FALSE;
	m_bHasFocus=FALSE;
	this->m_iStatus=0;
}

//Distruttore
CSbControl::~CSbControl(void)
{
	this->m_iStatus=0;
	iType=0;
	iStyle=0;
	m_bActive=FALSE;
	szText=NULL;
	pCChar=NULL;
	lLeft=0;
	lTop=0;
	lWidth=0;
	lHeight=0;
	this->m_iStatus=0;
}


//----------------------------- Create ----------------------------------

HRESULT CSbControl::Create(LONG lLeft,LONG lTop,TCHAR *szText,int iStyle)
{		
	this->szText=szText;    
	this->lWidth=0;
	this->SetLeft(lLeft);
	this->SetTop(lTop);
	this->lHeight=0;

	if (pCChar)
	{
		if (pCChar->GetStatus() > 0)
		{
			//popola anche larghezza e altezza
			SetText(szText);
		}
	}
	
	if (iStyle>=0) this->iStyle=iStyle;
	else return E_FAIL;

	return S_OK;
}

//--------------- Left,Top,Width,Height -------------------------------

LONG CSbControl::GetLeft(void)
{
	return lLeft;
}

LONG CSbControl::GetTop(void)
{
	return lTop;
}

LONG CSbControl::GetWidth(void)
{
	return lWidth;
}

LONG CSbControl::GetHeight(void)
{
	return lHeight;
}

//------------------------------- SetLeft,SetTop --------------------------------------

void CSbControl::SetLeft(LONG newval)
{
    this->lLeft=newval;
}

void CSbControl::SetTop(LONG newval)
{
	this->lTop=newval;	
}

//--------------------------------- Justify -----------------------------------------
//modifica la posizione del controllo, lref=larghezza di riferimento
HRESULT CSbControl::Justify(int imode,LONG lref)
{
	if (lref<=0) return E_FAIL; 

	switch(imode)
	{
	case JU_CENTER:
			this->SetLeft((LONG)(0.5f*(lref-lWidth)));
			break;
	case JU_LEFT:
			this->SetLeft(0);
			break;
    case JU_RIGHT:
			this->SetLeft(lref-lWidth);
			break;	
	}

	return S_OK;
}

//-------------------------------- SetCharSet -------------------------

//Iposta il carattere,
//in cs devono  essere gi� caricati i caratteri
HRESULT CSbControl::SetCharSet(CADXCharSet *cs)
{
	if (cs)
	{
		if (cs->GetStatus() > 1)
		{
			//il carattere puo' essere impostato
			pCChar=cs;
			this->m_iStatus=1;
			//la larghezza del controllo � uguale a quella del testo che lo rappresenta
    		//nel caso di menu
			this->lWidth=pCChar->GetTextWidth(szText);
			this->lHeight=pCChar->GetCharHeight();


			return S_OK;
		}

		else return E_FAIL;
	}

	else return E_FAIL;
}

//------------------------ GetType ----------------------------------
//restituisce il tipo di controllo
int CSbControl::GetType(void)
{
	return iType;
}

//---------------------- MoveTo ---------------------------------------
//Cambia la posizione del controllo (senza ridisegnarlo)
void CSbControl::MoveTo(LONG x,LONG y) 
{
	this->lLeft=x;
	this->lTop=y;
}

//------------------------ Text --------------------------------------
//Ritorna il testo contenuto nel controllo

TCHAR *CSbControl::Text(void)
{
	return szText;
}

//-------------------------- SetText ---------------------------------

void CSbControl::SetText(TCHAR *txt)
{
	
	if (!txt) this->lWidth=0;	
	else this->lWidth=pCChar->GetTextWidth(txt);

	this->lHeight=pCChar->GetCharHeight();

	szText=txt;	
}

//----------------------------- Render ----------------------------------

void CSbControl::Render(IMAGE_FRAME_PTR img)
{
	if (this->GetStatus() > 0)
	{
		pCChar->TextOut(lLeft,lTop,szText);
	}
}

//--------------------------- SetAction ----------------------------

//Imposta il puntatore alla funzione che definisce l'azione da intraprendere
HRESULT CSbControl::SetAction(int (*lpfnActionFn)(void *ctrl))
{
	if (lpfnActionFn)
	{
		m_lpfnAction=lpfnActionFn;
		return S_OK;
	}

	else return E_FAIL;
}

//----------------------------------- CSbcontrol::Active ----------------------------
//restituisce lo stato attivo del controllo
BOOL CSbControl::Active(void)
{
	return m_bActive;
}

//----------------------------------- CSbControl::Activate ---------------------------

void CSbControl::Activate(BOOL bVal)
{
	if (GetStatus() > 0)
	{
		m_bActive=bVal;
	}
}

//---------------------------------- CSbControl::SetFocus------------------------------

void CSbControl::SetFocus(BOOL bFocus)
{
	m_bHasFocus=bFocus;
}

//---------------------------------- CSbControl::HasFocus ----------------------------

BOOL CSbControl::HasFocus(void)
{
	return m_bHasFocus;

}
//--------------------------------- Activate -----------------------------------------
//Esegue la funzione che � associata al controllo
void CSbControl::Execute(void)
{
	if (GetStatus()) if (m_lpfnAction) m_lpfnAction(this);
}

///////////////////////////////// CSbStaticText //////////////////////////

//per ora non implementata: usa i metodi della classe base

/////////////////////////////// CSbButton ////////////////////////////////

CSbButton::CSbButton(void)
{

	m_lTextX=0;
	m_lTextY=0;
	this->m_iStatus=0;
	this->iType=SB_CTRL_BUTTON;
	m_pimgBtu=NULL;
}


//------------------------------- CSbButton::Create -----------------------------------

//Crea il pulsante lLeft e lTop definiscono la posizione,szText il testo
//imgBtu � la frame del pulsante, definisce anche le dimensioni dello stesso

HRESULT CSbButton::Create(LONG lLeft,LONG lTop,TCHAR *szText,IMAGE_FRAME_PTR pimgBtu,int iStyle)
{

	LONG lTextWidth;
	LONG lLen;

	if (this->m_iStatus > 0)
	{
		if (pimgBtu)
		{
			if (pimgBtu->status > 0)
			{
				this->lWidth=pimgBtu->width;
				this->lHeight=pimgBtu->height;
				this->lLeft=lLeft;
				this->lTop=lTop;
				this->iStyle=iStyle;
				this->szText=szText;
			    this->m_pimgBtu=pimgBtu;
				this->m_iStatus=2;
				
				lLen=szText ? strlen(szText) : 0;


				if (pCChar)
				{

					//determina la larghezza del testo in pixel
					lTextWidth=lLen*(pCChar->GetCharWidth() + pCChar->GetCharSpacing());                
					//calcola il text padding
					this->m_lTextX=(this->lWidth - lTextWidth) / 2;
					this->m_lTextY=(this->lHeight - pCChar->GetCharHeight()) / 2;
				}

				else
				{
					lTextWidth=this->m_lTextX=this->m_lTextY=0;

				}

				return S_OK;
			}

			else return E_FAIL;
		}		
		else return E_FAIL; //immagine non valida
	}

	else return E_FAIL;
}

//---------------------- CSbButton:GetTop,GetLeft ... ---------------------------

LONG CSbButton::GetTop()
{
	return this->lTop;
}

LONG CSbButton::GetLeft()
{
    return this->lLeft;	
}

LONG CSbButton::GetWidth()
{
	return this->lWidth;
}

LONG CSbButton::GetHeight()
{
	return this->lHeight;
}

//---------------------- CSbButton::Render ------------------------------

void CSbButton::Render(IMAGE_FRAME_PTR img)
{
	if (GetStatus()>=2)
	{
		//disegna lo sfondo del pulsante
		m_lpFm->PutImgFrameClip(lLeft,lTop,img,m_pimgBtu);
        //disegna il testo del pulsante
		pCChar->TextOut(lLeft+m_lTextX,lTop+m_lTextY,m_pimgBtu,szText);
	}
}

/////////////////////////// CSbStaticText ////////////////////////////////////////

CSbStaticText::CSbStaticText(void)
{
	
	this->iType=SB_CTRL_STATIC;
}


//Implementa un testo statico usato come voce di menu o titolo

void CSbStaticText::Render(IMAGE_FRAME_PTR img)
{
	if (GetStatus())
	{
		pCChar->TextOut(lLeft,lTop,img,szText);
	}
}

///////////////////////// CSbOption ////////////////////////////////////

//Implementa il controllo option

CSbOption::CSbOption(void)
{
	m_bValue=FALSE;
	m_pimgOn=NULL;
	m_pimgOff=NULL;
	this->iType=SB_CTRL_OPTION;
}

//------------------------------------ CSbOption::Create --------------------------------------------------

HRESULT CSbOption::Create(LONG lLeft,LONG lTop,TCHAR *szText,BOOL bInitialState,IMAGE_FRAME_PTR pimgOn,IMAGE_FRAME_PTR pimgOff)
{
	if (!pimgOn || !pimgOff) return E_FAIL;

	if (!pimgOn->status || !pimgOff->status) return E_FAIL;

	SetValue(bInitialState); //imposta il valore iniziale

	this->szText=szText;
	this->lLeft=lLeft;
	this->lTop=lTop;

	m_pimgOn=pimgOn;
	m_pimgOff=pimgOff;
    m_lpfnAction=NULL;

	return S_OK;
}
	
//------------------------------------------- SetValue ---------------------------------------------------------

HRESULT CSbOption::SetValue(BOOL bNewVal)
{	
		this->m_bValue=bNewVal;
		return S_OK;
}

//--------------------------------------------- Value ------------------------------------------

BOOL CSbOption::Value(void)
{
	return m_bValue;
}

//-------------------------------------------- Render --------------------------------------------
//Disegna il controllo
void CSbOption::Render(IMAGE_FRAME_PTR img)
{
	LONG lDelta ;

	if (GetStatus())
	{
		if (m_bValue==TRUE) 
		{
			m_lpFm->PutImgFrameClip(lLeft,lTop,img,m_pimgOn); //immagine on
			lDelta=m_pimgOn->width;
		}
		else 
		{
			m_lpFm->PutImgFrameClip(lLeft,lTop,img,m_pimgOff); //immagine off       
			lDelta=m_pimgOff->width;
		}

		//scrive il testo
		pCChar->TextOut(lLeft+lDelta,lTop,img,szText);

	}

}

//------------------------ csboption::execute ------------------------------

void CSbOption::Execute(void)
{
	if (GetStatus()) 
	{
		m_bValue=!m_bValue; //cambia lo stato
		if (m_lpfnAction) m_lpfnAction(this);
	}
}
//////////////////////// CSbInputText /////////////////////////////////////

//costruttore
CSbInputText::CSbInputText(void)
{

	//imposta il numero di caratteri di default
	szInputText=NULL;
    szText=NULL;
	m_pTextCharSet=NULL;
	m_lSelStart=0;
	dwMaxLen=0;
	//mem leaks!!
    //SetMaxLen(100); //imposta il numero max. di caratteri di default
	bSetText=FALSE;
	iType=SB_CTRL_TEXT;
	m_imgCaret.status=0;
	m_lCaptWidth=0;

}

//numero max. di caratteri digitabili
DWORD CSbInputText::GetMaxLen(void)
{
	return dwMaxLen;
}

//imposta il massimo numero di caratteri digitabili
HRESULT CSbInputText::SetMaxLen(DWORD dwValue)
{
	if (bSetText) SAFE_DELETE_ARRAY(szInputText);
	szInputText=new TCHAR[dwValue+1]; //l'ultimo caratter � lo zero finale
	
	if (szInputText) 
	{

		//azzera il testo contenuto
		dwMaxLen=dwValue;
		memset(szInputText,0,sizeof(TCHAR)*(dwValue));


		return S_OK;
	}

	return E_FAIL;
}

//imposta il char set per il testo digitato
HRESULT CSbInputText::SetTextCharSet(CADXCharSet *cs)
{
	if (cs)
	{
		if (cs->GetStatus() > 1)
		{
			m_pTextCharSet=cs;
			return S_OK;
		}
	}

	return E_FAIL;
}


//invia un tasto al controllo
//iKey=scan code del tasto
//rende true se il tasto viene accettato flase altrimenti
BOOL CSbInputText::SendKey(int iKey)
{
	LONG lLen=0;
	TCHAR *temp=NULL;
	CADXDebug dbg;

//	dbg.SetErrorFile("gui.txt",FALSE);

	if (iKey==0) return FALSE;

	lLen=lstrlen(szInputText);


	if (lLen<(LONG)dwMaxLen || iKey==8) 
	{
		switch(iKey)
		{
		case 8:
			//backspace
			if (lLen>0)
			{
				lLen--;
				temp=new TCHAR[lLen+1];
				memset(temp,0,sizeof(TCHAR)*(lLen+1));
				memcpy(temp,szInputText,lLen);
				SAFE_DELETE_ARRAY(szInputText);
				szInputText=new TCHAR[lLen+1]; //viene allocato un caratter inpiu' per lo zero finale
				memset(szInputText,0,sizeof(TCHAR)*(lLen+1));
				memcpy(szInputText,temp,(size_t)lLen);
				SAFE_DELETE_ARRAY(temp);
			}


			break;
		case 13:
			//return



			break;				
		default:
			
			
			lLen++;

			if (szInputText)
			{
				//aggiunge il carattere digitato alla fine del testo
				temp=new TCHAR[lLen+1];
				memset(temp,0,sizeof(TCHAR)*lLen+1);
				memcpy(temp,szInputText,lLen); //copia il contenuto attuale del testo
				SAFE_DELETE_ARRAY(szInputText);
			}

			szInputText=new TCHAR[lLen+1];
			memset(szInputText,0,sizeof(TCHAR)*(lLen+1));
			if (temp) memcpy(szInputText,temp,lLen);
			szInputText[lLen-1]=iKey; //aggiunge il carattere alla fine
			SAFE_DELETE_ARRAY(temp);
			
			break;
		}

		return TRUE;
	}

//	dbg.WriteErrorFile("dopo len=%d key=%d %d - %d - %d - %d",lLen,iKey,szInputText[0],szInputText[1],szInputText[2],szInputText[3]);

	return FALSE; 
}

HRESULT CSbInputText::SetInputText(TCHAR *szTxt)
{
	size_t dwLen;

	if (!szTxt) return E_FAIL;

	dwLen=strlen(szTxt)/sizeof(TCHAR);
    
	//taglia il testo se necessario
	dwLen=(dwLen>dwMaxLen) ? dwLen : dwMaxLen;
    //pulisce il testo di input
    if (bSetText) SAFE_DELETE_ARRAY(szInputText);
    //alloca la memoria
	szInputText=new TCHAR[dwLen+1];
	memset(szInputText,0,sizeof(TCHAR)*dwLen);
	//copia il testo nel testo di input
	memcpy(szInputText,szTxt,dwLen);
	bSetText=TRUE;

	return S_OK;
}

void CSbInputText::SetText(TCHAR *szText)
{
	CSbControl::SetText(szText);
	m_lCaptWidth=0; //azzera la larghezza della caption in modo da aggiornarla automaticamente durante il rendering
}

//--------------------------------- GetWidth,GetHeight,GetLeft,GetTop ------------------------

LONG CSbInputText::GetWidth(void) //acquisisce le dimensioni del controllo
{
	return lWidth+m_lCaptWidth;	

}

LONG CSbInputText::GetHeight(void)
{
	return lHeight;
}

LONG CSbInputText::GetLeft(void)
{
	return lLeft-m_lCaptWidth;

}

LONG CSbInputText::GetTop(void)
{
	return lTop;
}
	
//------------------------------- CSbInputText::Create -------------------

HRESULT CSbInputText::Create(LONG lLeft,LONG lTop,TCHAR *szCaption,int iStyle)
{
	HRESULT hr;
	if (FAILED(hr=CSbControl::Create(lLeft,lTop,szCaption,iStyle))) return hr;

	return S_OK;

}
//----------------------------- GetInputText -----------------------------

TCHAR *CSbInputText::GetInputText(void)
{
	return (szInputText);
}

CSbInputText::~CSbInputText(void)
{
	if (m_imgCaret.status>0) m_lpFm->FreeImgFrame(&m_imgCaret);
	SAFE_DELETE_ARRAY(szInputText);
	this->m_iStatus=0;
	dwMaxLen=0;	
}


//------------------------ CSbInputText::Render --------------------------

void CSbInputText::Render(IMAGE_FRAME_PTR img)
{
//	LONG lDelta;

	static iBlinker=0;
	LONG lw=0;

	if (!pCChar) return;
	//acquisisce la larghezza in pixel della caption
	if (!m_lCaptWidth) m_lCaptWidth=pCChar->GetTextWidth(szText);
	if (!m_pTextCharSet) m_pTextCharSet=pCChar;

	m_pTextCharSet->TextOut(lLeft,lTop,img,szInputText);
	pCChar->TextOut(lLeft-m_lCaptWidth,lTop,img,szText);

	if (m_bActive && m_bHasFocus)
	{
		iBlinker++;

		if (iBlinker==30)
		{
			iBlinker=0;
		}

		if (iBlinker<15)
		{
			//visualizza il caret
			if (m_imgCaret.status==0)
			{
				if (!FAILED(m_lpFm->CreateFrameSurface(&m_imgCaret,3,pCChar->GetCharHeight())))
				{
					m_lpFm->Cls(&m_imgCaret,m_lpGm->CreateRGB(255,255,255));
				}
			}

			lw=pCChar->GetTextWidth(szInputText);
			if (m_imgCaret.status>0) m_lpFm->PutImgFrameClip(lw+lLeft,lTop,img,&m_imgCaret);
		}
	}	
}

//------------------------ GetSelStart --------------------------------

LONG CSbInputText::GetSelStart(void)
{
	return m_lSelStart;
}

//------------------------ SetSelStart ----------------------------------

HRESULT CSbInputText::SetSelStart(LONG new_val)
{
	if (new_val>=0 && new_val<(LONG)dwMaxLen) 
	{
		m_lSelStart=new_val;
	    return S_OK;
	}
	else return E_FAIL;
}

///////////////////////////// CSbKeyCode /////////////////////////////////////

CSbKeyCode::CSbKeyCode(void)
{
	m_iKeyCode=0;
	szInputText=NULL;
	dwMaxLen=10; //nel caso di questo controllo la lunghezza del testo non riflette il numero di inserimenti che � sempre uno
	             //qui la lunghezza max. impostata a 10 perch� ci sono tasti che hanno una descrizione lunga (tipo "SPACE") ecc... 
	iType=SB_CTRL_KEYCODE;
	

}

CSbKeyCode::~CSbKeyCode(void)
{
	m_iKeyCode=0;
}

HRESULT CSbKeyCode::SetMaxLen(DWORD dwValue)
{
	//in questo caso questa funzione non deve essere attiva in quanto max len � sempre 1
	return S_OK;

}

//nota: iKey in questo caso � il codice virtuale del tasto secondo directx
//e non lo scancode windows
BOOL CSbKeyCode::SendKey(int iKey)
{
	int iscan;
	BOOL bres=TRUE;

	//scan code del tasto
	iscan=m_ci.GetKeyASCII(iKey);

	//backspace
	//cancella il key code : in questo caso non c'� nessun codice di tasto
	if (iscan==8)
	{
		SAFE_DELETE_ARRAY(szInputText);
		m_iKeyCode=0;
	}
	else
	{	
		
		if (m_iKeyCode) return FALSE; //c'� gi� un tasto

		return (S_OK==SetKeyCode(iKey));	
	}

	return bres;

}

//restituisce il codice del tasto (codice DirectX e non scan code)	
int CSbKeyCode::GetKeyCode(void)
{
	return m_iKeyCode;
}


//imposta il tasto nel controllo
HRESULT CSbKeyCode::SetKeyCode(int iKey)
{
	if (iKey>=0)
	{
		m_iKeyCode=iKey;
		
		//crea una definzione descrittiva del tasto
		int iscan=m_ci.GetKeyASCII(iKey);

		switch(iKey)
		{
		case DIK_SPACE:
			//barra
			SetInputText(TEXT("SPACE"));
			break;
		case DIK_UP:
			//freccia in alto
			SetInputText(TEXT("CUR UP"));
			break;
		case  DIK_RIGHT:
			SetInputText(TEXT("CUR RIGHT"));
			break;
		case DIK_DOWN:
			SetInputText(TEXT("CUR DOWN"));
			break;
		case DIK_LEFT:
			SetInputText(TEXT("CUR LEFT"));
			break;
        case DIK_LCONTROL:
			SetInputText(TEXT("LCTRL"));
			break;
		case DIK_RCONTROL:
			SetInputText(TEXT("RCTRL"));
			break;
		case DIK_RALT:
			SetInputText(TEXT("R ALT"));
			break;
		case DIK_LALT:
			SetInputText(TEXT("L ALT"));
			break;
        case DIK_RETURN:
			SetInputText(TEXT("RETURN"));
			break;
        case DIK_HOME:
			SetInputText(TEXT("HOME"));
			break;
        case DIK_END:
			SetInputText(TEXT("END"));
            break;
        case DIK_CAPITAL:
			SetInputText(TEXT("CAPITAL"));
			break;
        case DIK_NUMPAD1:
			SetInputText(TEXT("NUMPAD1"));
			break;
		case DIK_NUMPAD2:
			SetInputText(TEXT("NUMPAD2"));
			break;
		case DIK_NUMPAD3:
			SetInputText(TEXT("NUMPAD3"));
			break;
		case DIK_NUMPAD4:
			SetInputText(TEXT("NUMPAD4"));
			break;
		case DIK_NUMPAD5:
			SetInputText(TEXT("NUMPAD5"));
			break;
		case DIK_NUMPAD6:
			SetInputText(TEXT("NUMPAD6"));
			break;
		case DIK_NUMPAD7:
			SetInputText(TEXT("NUMPAD7"));
			break;
		case DIK_NUMPAD8:
			SetInputText(TEXT("NUMPAD8"));
			break;
		case DIK_NUMPAD9:
			SetInputText(TEXT("NUMPAD9"));
			break;
		case DIK_F1:
			SetInputText(TEXT("F1"));
			break;
		case DIK_F2:
			SetInputText(TEXT("F2"));
			break;
		case DIK_F3:
			SetInputText(TEXT("F3"));
			break;
		case DIK_F4:
			SetInputText(TEXT("F4"));
			break;
		case DIK_F5:
			SetInputText(TEXT("F5"));
			break;
		case DIK_F6:
			SetInputText(TEXT("F6"));
			break;
		case DIK_F7:
			SetInputText(TEXT("F7"));
			break;
		case DIK_F8:
			SetInputText(TEXT("F8"));
			break;
		case DIK_F9:
			SetInputText(TEXT("F9"));
			break;
		case DIK_F10:
			SetInputText(TEXT("F10"));
			break;
		case DIK_F11:
			SetInputText(TEXT("F11"));
			break;
		case DIK_F12:
			SetInputText(TEXT("F12"));
			break;
		case DIK_ADD:
			SetInputText(TEXT("ADD"));
			break;
        case DIK_MINUS:
			SetInputText(TEXT("MINUS"));
			break;
        case DIK_LSHIFT:
			SetInputText(TEXT("LSHIFT"));
			break;
        case DIK_RSHIFT:
			SetInputText(TEXT("RSHIFT"));
			break;
        case DIK_MULTIPLY:
			SetInputText(TEXT("MULTIPLY"));
			break;
        case DIK_SCROLL:
			SetInputText(TEXT("SCROLL"));
			break;

		default:
			if (iscan>0)
			{
				CSbInputText::SetInputText("");
				//esiste una codifica ascii del testo
				CSbInputText::SendKey(iscan);
			}
			else
			{
				//indica il codice del tasto premuto
				TCHAR szBuff[10];
				_stprintf(szBuff,"KEY %d",iKey);
				CSbInputText::SetInputText(szBuff);								
			}	
		}

		return S_OK;
	}

	else return E_FAIL;
}

///////////////////////////// CSbGroup /////////////////////////////////////

//------------------------------ CSbGroup::CSbGroup --------------------------

CSbGroup::CSbGroup(void)
{
		
	for (m_dwCount=0;m_dwCount<MAX_CTRLS;m_dwCount++) m_lpctrlList[m_dwCount]=NULL;
    m_dwCount=0;

	iType=SB_CTRL_GROUP;
}

//------------------------------- CSbGroup::~CSbGroup ------------------------

CSbGroup::~CSbGroup(void)
{
	
	m_iStatus=0;
}

//---------------------------------------------------------------------

//il controllo da aggiungere deve essere caricato
//se btab stop � true significa che il controllo � attivo
//il cursore puo' essere posizionato su di esso
HRESULT CSbGroup::AddControl(CSbControl *ctrl,BOOL bActive)
{
	
	if (ctrl)
	{
		if ((CSbControl *)ctrl->GetStatus() > 0)
		{
			if (m_dwCount < MAX_CTRLS && (CSbControl *)ctrl->GetType() > 0)
			{
				m_lpctrlList[m_dwCount++]=ctrl; //aggiunge il controllo nel vettore
			    
				ctrl->Activate(bActive); //cambia lo stato del controllo

				m_iStatus=1; //setta lo stato del gruppo a 1 perch� almeno un controllo � caricato
				
			}

			else return E_FAIL; //numero di controlli max. raggiunto
		}

		else return E_FAIL; //controllo non inizializzato

	}

	else return E_FAIL;

	return S_OK; //ok

}


/*
//Implementa un contenitore controlli
//che attende l'input dell'utente
class CSbForm:public virtual CSbGroup
{

private:


protected:

	void *m_lpctrlList; //vettore che contiene tutti i controlli
	SPRITE_PTR m_psprPointer; //sprite del puntatore del mouse
	SPRITE_PTR m_psprSel; //sprite dell selettore
	DWORD m_dwCount;  //numero di controlli all'interno del form
    RECT m_rcClientRect; //rettangolo che definisce l'area attiva del form
    BOOL m_bMouseOn; //eventi del mouse attivi

public:

	CSbForm(void);
	~CSbForm(void);
	void ShowMouse(void);
	void HideMouse(void);
	HRESULT SetMousePointer(SPRITE_PTR psprPoint);
	HRESULT SetSelector(SPRITE_PTR psprSel); //selettore che evidenzia il controllo corrente
	HRESULT SetActiveRect(LPRECT pRect);
	void Close(void); //chiude il form
	void Show(void);
	void Listen(void); //attende l'input dell'utente
};

#endif*/

/////////////////////////// CSbForm //////////////////////////////////////

CSbForm::CSbForm(void)
{
	m_iStatus=0;
	m_bMouseOn=TRUE;
	bMouseLeft=FALSE;
	m_dwCount=0;
	m_iSelectPosition=SB_CTRLLEFT;
	m_dwCurrentControl=MAX_CTRLS; //correntemente nessun controllo selezionato
	m_lpfnBackGround=NULL;
	m_bActive=FALSE;
	iType=SB_CTRL_FORM;
	m_bJoyCentered=FALSE;
	m_bJoyButtonReady=FALSE;
}

CSbForm::~CSbForm(void)
{
	m_lpFm->FreeImgFrame(&m_imgOut);
	m_iStatus=0;
}

//------------------------------- ShowMouse ---------------------------

//rende visibile il mouse
//se non � caricata nessuna frame per il mouse, allora mostra il puntatore standard
void CSbForm::ShowMouse(void)
{
	m_bMouseOn=TRUE;
}

//-------------------------------- HideMouse -----------------------------

void CSbForm::HideMouse(void)
{
   m_bMouseOn=FALSE;

}

//---------------------------------- SetMousePointer ----------------------

HRESULT CSbForm::SetMousePointer(SPRITE_PTR psprMouse)
{
	if (!psprMouse->GetStatus()) return E_FAIL; //lo sprite non � inizializzato
	 
	else m_psprPointer=psprMouse; //setta il puntatore del mouse
		
    return S_OK;	

}

//--------------------------------- SetSelector ------------------------------

//Imposta lo sprite che funge dal selettore 
//iPostion determina la posizione dello sprite rispetto al controllo
HRESULT CSbForm::SetSelector(SPRITE_PTR psprSel,int iPosition)
{

	IMAGE_FRAME_PTR pf;

	if (!psprSel) return E_FAIL;

	if (!psprSel->GetStatus()) return E_FAIL;

	else 
	{
	
		//prende la prima frame
		pf=psprSel->GetFramePtr(0);

		//imposta le dimensioni del selettore

		m_iSelWidth=pf->width;
		m_iSelHeight=pf->height;

		m_iSelectPosition=iPosition;	

		m_psprSel=psprSel;
	}

	return S_OK;
}

//----------------------------- CSbForm::Render ----------------------------------

void CSbForm::Render(IMAGE_FRAME_PTR img)
{
	DWORD dwcnt;
	CSbControl *ctrl;	

	for (dwcnt=0;dwcnt<m_dwCount;dwcnt++)
	{
		ctrl=(CSbControl *)m_lpctrlList[dwcnt];		
	    
		if (ctrl) ctrl->Render(img);	
		else break; //alla prima posizione vuota che trova termina il ciclo

	}

}


//------------------------------ CSbForm::GetNextActivecontrol ----------------------

//Nella lista dei controlli del form ritorna l'indice del controllo successivo
//che � in stato attivo
//dwStart � l'indice della lista da cui inizia a cercare
//se rende MAX_CTRLS allora non ha trovato controlli attivi

DWORD CSbForm::GetNextActiveControl(DWORD dwStart)
{
	DWORD dwCount;
	DWORD dwRes=MAX_CTRLS;

	//limita dwStart
	dwStart = dwStart > MAX_CTRLS-1 ? MAX_CTRLS-1 : dwStart;

	for (dwCount=dwStart;dwCount<m_dwCount;dwCount++)
	{
		if (m_lpctrlList[dwCount])
		{
			if (m_lpctrlList[dwCount]->Active())
			{
				dwRes=dwCount;
				break;
			}
		}
	}

	if (dwRes == MAX_CTRLS)
	{
		//cerca nella parte della lista che va da 0 a dwStart
		for (dwCount=0;dwCount<dwStart;dwCount++)
		{
			if (m_lpctrlList[dwCount]->Active()) 
			{
				dwRes=dwCount;
				break;
			}
		}
	}

	return dwRes;
}

//----------------------------- GetPrevActiveForm ----------------------
//Acquisisce il controllo attivo precedente a partire dal controllo
//con indice dwStart compreso

DWORD CSbForm::GetPrevActiveControl(DWORD dwStart)
{
	DWORD dwCount;
	DWORD dwRes=MAX_CTRLS;

	//limita dwStart
	dwStart = dwStart > MAX_CTRLS-1 ? MAX_CTRLS-1 : dwStart;

	for (dwCount=dwStart;dwCount>=0;dwCount--)
	{
		if (m_lpctrlList[dwCount])
		{
			if (m_lpctrlList[dwCount]->Active())
			{
				dwRes=dwCount;
				break;
			}
		}
	}

	if (dwRes == MAX_CTRLS)
	{
		//cerca nella parte della lista che va da 0 a dwStart
		for (dwCount=MAX_CTRLS-1;dwCount>dwStart;dwCount--)
		{
			if (m_lpctrlList[dwCount]->Active()) 
			{
				dwRes=dwCount;
				break;
			}
		}
	}

	return dwRes;

}

//----------------------- GetControlXY ---------------------------------
//Acquisisce il controllo alla poszione x,y
DWORD CSbForm::GetControlXY(LONG x,LONG y)
{
	DWORD dwCount;
	DWORD dwRes=MAX_CTRLS;
	RECT rc;

	for (dwCount=0;dwCount<MAX_CTRLS;dwCount++)
	{
		if (m_lpctrlList[dwCount])
		{
			if (m_lpctrlList[dwCount]->Active())
			{
				rc.left=m_lpctrlList[dwCount]->GetLeft();
				rc.top=m_lpctrlList[dwCount]->GetTop();
				rc.right=m_lpctrlList[dwCount]->GetWidth();
				rc.bottom=m_lpctrlList[dwCount]->GetHeight();
                
				if (x>=rc.left && x<rc.left+rc.right && y>=rc.top && y<=rc.top+rc.bottom)
				{
					dwRes=dwCount;
					break;				
				}			

			}
		}
	}

	return dwRes;
}

//---------------------- SetInputManager ------------------------------

HRESULT CSbForm::SetInputManager(CADXInput *ci)
{
	if (ci && m_iStatus>0)
	{
		if (ci->GetStatus() > 0)
		{
			m_pci=ci;
			this->m_iStatus=2;
			return S_OK;
		}
		else return E_FAIL;
	}
	else return E_FAIL;
}

//------------------------ CSbForm::Open --------------------------------------------------

//visualizza la form
void CSbForm::Open(void)
{

	DWORD dwFocus=0;
	HRESULT hr;

	m_bActive=FALSE;
	if (m_iStatus<1) return; //esce se non � stato configurato correttamente (occorre impostare l'input manager ,graphic manager ecc...)
	//i seguenti puntatori sono necessari
    if (!m_lpGm || !m_lpFm) return;

	//acquisisce  l'indice del controllo su cui impostare il fuoco
	dwFocus=this->GetNextActiveControl(0);
    //crea l'immagine su cui disegna
	hr=this->m_lpFm->CreateFrameSurface(&m_imgOut,m_lpGm->GetScreenWidth(),m_lpGm->GetScreenHeight());

	if (FAILED(hr)) return;

	if (m_psprSel) m_psprSel->SetDisplay(&m_imgOut);
	if (m_psprPointer) m_psprPointer->SetDisplay(&m_imgOut);

	//applica il selettore al primo controllo attivo
	SelectControl(dwFocus);
	
	m_bBreakListen=FALSE;
	m_bActive=TRUE;
    //resetta lo
    //stato dei tasti della tastiera
	
	m_pci->ResetKyb();
	m_pci->ResetMouse();
	
	
	//resetta lo stato del mouse
	bMouseLeft=FALSE; 

	this->Listen(); //esegue un ciclo per visualizzare la form

}


//----------------------------- CSbForm::Close ------------------------------------
//Imposta il flag che comunica di interrompere il ciclo di attesa
void CSbForm::Close(void)
{
	m_bActive=FALSE;
	this->m_bBreakListen=TRUE;
	if (m_lpFm) m_lpFm->FreeImgFrame(&m_imgOut);
}


//------------------------------- CSbForm::Listen ----------------------------------

//Ciclo di gestione input e visualizzazione su schermo. 
void CSbForm::Listen(void)
{
	DWORD dwFocus;   
	HRESULT hr;			
	LONG lX,lY;
	BOOL bBtu;
	int iType1=0;
//	CADXDebug dbg;
	static int iLastKey=0;
	int iKey=0;

	if (!m_bActive) return; //esce se la form non � stata attivata
	
	if (m_lpfnBackGround != NULL) 
	{
		m_lpfnBackGround(&m_imgOut);
	}
	else m_lpGm->Cls(0,&m_imgOut); //se non � stata impostata una funzione per il ridisegno del back buffer
                         //cancella il back buffer 

	//acquisisce lo stato della tastiera
	m_pci->GetKybStatus();    
	

	if (m_pci->JoystickAttached())
	{

		//acquisisce lo stato del joystick
		m_pci->GetJoystickStatus();

		if (m_pci->JoystickY()==0) m_bJoyCentered=TRUE;

		if (!m_pci->JoystickButtons(0)) m_bJoyButtonReady=TRUE; //effetto interruttore per il tasto 1 del joy
	
	}
	
	//acquisisce lo stato del mouse
	hr=m_pci->GetBufferedMouseState();        	

	//disegna i controlli della form sull'immagine di sfondo
	this->Render(&m_imgOut);

	SelectControl(m_dwCurrentControl);
  
	//aggiorna la frame corrente dello sprite che fa da selettore
	if (m_psprSel) m_psprSel->UpdateFrame();

	lX=m_pci->MouseX();
	lY=m_pci->MouseY();
	
	//aggiorna il puntatore del mouse
	if (m_psprPointer) m_psprPointer->RenderToDisplay(lX,lY);	   	

	//ciclo di disegno della form
	m_lpGm->UseBackBuffer();
	
	m_lpFm->PutImgFrame(0,0,&m_imgOut,0);

	m_lpGm->ShowBackBuffer();

	bBtu=m_pci->MouseLeftButton();

	if (bMouseLeft && !bBtu)
	{
		dwFocus=this->GetControlXY(lX,lY);
		if (dwFocus != MAX_CTRLS)
		{
			SelectControl(dwFocus);
			//esegue l'azione associata al controllo con un singolo click...
			m_lpctrlList[dwFocus]->Execute();
		}
	}

	//inserire qui il codice per gestire il doppio click (se serve)

	bMouseLeft=bBtu;


	
	if (m_pci->KeyPress(DIK_ESCAPE)) this->m_bBreakListen=TRUE; //chiude il menu premendo escape

	if (m_pci->KeyPress(DIK_UP) || (m_pci->JoystickAttached() && (m_pci->JoystickY()<0 && m_bJoyCentered)))
	{

		m_bJoyCentered=FALSE;
		//seleziona il controllo precedente
		dwFocus=this->GetPrevActiveControl(m_dwCurrentControl-1);

		SelectControl(dwFocus);		
		
	}
	else if (m_pci->KeyPress(DIK_DOWN) || (m_pci->JoystickAttached() && (m_pci->JoystickY()>0 && m_bJoyCentered)))
	{

		m_bJoyCentered=FALSE;

		//seleziona il controllo successivo
		dwFocus=this->GetNextActiveControl(m_dwCurrentControl+1);
		
		SelectControl(dwFocus);			
	
	}

	else if (m_pci->KeyPress(DIK_RETURN) || (m_pci->JoystickAttached() && (m_pci->JoystickButtons(0) && m_bJoyButtonReady)))
	{	
		m_bJoyButtonReady=FALSE;
		if (m_dwCurrentControl==MAX_CTRLS) this->Close();
		else m_lpctrlList[m_dwCurrentControl]->Execute();
	} 


	if (m_dwCurrentControl<MAX_CTRLS)
	{
		iType1=m_lpctrlList[m_dwCurrentControl]->GetType();
		
		if (iType1==SB_CTRL_TEXT) 
		{
			iKey=m_pci->GetCurrentKeyDown(0,255);
		
			if (iKey!=iLastKey)
			{				
				iLastKey=iKey;
				CSbInputText *pin=(CSbInputText *)m_lpctrlList[m_dwCurrentControl];								
				if (iKey>0) pin->SendKey(m_pci->GetKeyASCII(iKey));
				pin=NULL;
			}
		}

		else if (iType1==SB_CTRL_KEYCODE)
		{
			//invia il codice (codice directx e non scan code) del tasto ad un controllo di tipo keycode
			iKey=m_pci->GetCurrentKeyDown(0,255);
		
			if (iKey!=iLastKey)
			{					
				iLastKey=iKey;
				CSbKeyCode *pin=(CSbKeyCode *)m_lpctrlList[m_dwCurrentControl];				
				if (iKey>0) pin->SendKey(iKey);				
				pin=NULL;
			}
		}

	}			
}

//----------------------------------- CSbForm::IsActive -----------------------------

BOOL CSbForm::IsActive(void)
{
	return m_bActive;
}

//------------------------------- MouseUpdate --------------------------------

void CSbForm::MouseUpdate(void)
{
	MessageBox(NULL,"OK","M",MB_ICONINFORMATION);

}

//--------------------------------- SetActiveControl --------------------------------------

HRESULT CSbForm::SetActiveControl(CSbControl *pctrl) //imposta il fuoco sul controllo pctrl
{
	DWORD count;

	HRESULT res=E_FAIL;

	if (!pctrl) return E_FAIL;
	
	for (count=0;count<m_dwCount;count++)
	{
		if (pctrl==m_lpctrlList[count])
		{
			SelectControl(count);
			res=S_OK;
			break;
		}
	}

	return res;
}

//-------------------------------- GetActiveControl ---------------------------------------
//Restituisce il controllo attivo 
CSbControl *CSbForm::GetActiveControl(void)
{
	if (m_dwCurrentControl==MAX_CTRLS) return NULL;
	
	return m_lpctrlList[m_dwCurrentControl];

}

//----------------------------- SelectControl ---------------------------------------------

//img � l'immagine su cui si renderizza
void CSbForm::SelectControl(DWORD dwIndex) //posiziona il selettore su uno dei controlli della lista
{
	CSbControl *pcb;
	LONG lx,ly;

	if (dwIndex<m_dwCount && m_psprSel)
	{
		//punta il controllo
		pcb=(CSbControl *)m_lpctrlList[dwIndex];

        if (pcb)
		{

			for (DWORD cnt=0;cnt<m_dwCount;cnt++) m_lpctrlList[cnt]->SetFocus(FALSE);

			if (m_psprSel->GetStatus()>0)
			{
				m_dwCurrentControl=dwIndex;//controllo correntemente attivo
				lx=pcb->GetLeft();
				ly=pcb->GetTop();
				pcb->SetFocus(TRUE); //imposta il fuoco sul controllo selezionato

				switch (this->m_iSelectPosition)
				{

				case SB_CTRLLEFT:
					//(selettore allineato a dx)
                    //disegna il selettore
					m_psprSel->RenderToDisplay(lx-(LONG)m_iSelWidth,ly);

				break;

				case SB_CTRLRIGHT:
					//(selettore allineato a sx)
					//da fare
				break;


				case SB_CTRLCENTER:					
					//(selettore centrato rispetto al controllo)
					//da fare
				break;

				}					

			}
		}		
	}
}
	
//------------------- CSbForm :: SetBackGroundProc --------------------------------------
//Imposta la funzione che disegna il background mentre � in attesa dell'input dell'utente
//img � l'immagine su cui vengono renderizzati tutti i controlli compreso
//il form stesso

void CSbForm::SetBackGroundProc(void (*lpfnBack)(IMAGE_FRAME_PTR img))
{
	if (lpfnBack) m_lpfnBackGround=lpfnBack;
}


//---------------------- CSbForm::SetClickProc --------------------------------------------






