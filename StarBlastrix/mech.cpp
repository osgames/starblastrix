//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mech.cpp - implementa una catena cinematica aperta di corpi rigidi vincolati
///////////////////////////////////////////////////////////////////////*/


#include "a32graph.h"  //GraphicManager e FrameManager
#include "a32sprit.h"   //sprites
#include "mech.h"
#include <math.h>

static CADXFastMath g_mat;

//costruttore
CMechComponent::CMechComponent(void)
{
	m_pFirstItem=NULL;
	m_pCurrent=NULL;
	m_pParent=NULL;
	lambda=0;	
	m_pBaseLink=NULL;
	theta=0; //angolo di posizione corrente
	m_status=0;
	m_iTag1=m_iTag2=0;
	m_pWalk=NULL;
	delta_angle=0;
	m_from_angle=0;

}

//distruttore
CMechComponent::~CMechComponent(void)
{
	this->Release();
}

//aggiunge le frame al componente
//from_angle e to_angle definiscono l'angolo iniziale e finale associato a ciascuna frame
//Dopo che il componente � stato creato sono note il numero di frames e gli angoli
int CMechComponent::CreateComponent(int from_angle,int to_angle,IMAGE_FRAME_PTR pframes,int iNumFrames)
{
	if (from_angle>to_angle || from_angle<0 || iNumFrames==0 || !pframes) return 0;
	if (m_status) return 0; 

	delta_angle=(to_angle-from_angle)/iNumFrames;

	m_from_angle=from_angle;

	m_to_angle=to_angle;

	for (int count=0;count<iNumFrames;count++)
	{

		if (FAILED(this->AddFrame(&pframes[count]))) return 0;
	}	

	this->SetCurFrame(0);

	lambda=0;

	m_status=1;

	return 1;
}


//copia prismatica
//direction=direzione di spostmento in gradi
int CMechComponent::AddPrismCoupling(int xrel,int yrel,int direction,int displ1,int displ2)
{
	return AddCoupling(LK_PRISMATIC,xrel,yrel,displ1,displ2,direction);
}

//accoppiamento rigido
int CMechComponent::AddFixCoupling(int xrel,int yrel)
{
	return AddCoupling(LK_FIXED,xrel,yrel,0,0,0);
}

//Aggiunge una coppia rotoidale
//NOTA BENE: xrel e yrel sono le coordinate del giunto relative all'angolo sup. sinistro
//della frame che corrisponde ad angle1
int CMechComponent::AddRotCoupling(int xrel,int yrel,int angle1,int angle2,int iOpenLoop)
{
	return AddCoupling(LK_ROTOIDAL,xrel,yrel,angle1,angle2,0,iOpenLoop);
}

//Aggiunge un accoppiamento
//restituisce l'id dell'accoppiamento (il primo link ha indice zero)
//NOTA: il numero massimo di coppie cinematiche per ogni componente � uguale a MAX_FRAME_HOT_SPOT
//ogni frame ha larghezza e altezza uguali
int CMechComponent::AddCoupling(COUPLING_TYPE_ENUM iType,int xrel,int yrel,int lambda1,int lambda2,int direction,int iOpenLoop)
{
	int xt,yt,xt1,yt1;
	int w2,h2;
	float an;

	//se l'oggetto non � stato creato allora rende -1 perch� non � noto il numero di frames
	//e gli angoli
	//NB: se m_status == 0 significa che il componente non ancora stato creato con Create
	if (m_status < 1 ) return -1;

	int iprev=-1;

	if (!m_pCurrent)
	{
		//inizializza la linked list
		m_pFirstItem=new MECH_LINK_TYPE;		
		m_pCurrent=m_pFirstItem;
	}
	else
	{
		if (m_pCurrent->id>=MAX_FRAME_HOT_SPOT-1) return -1; //� stato superato il massimo numero di coppie
		m_pCurrent->next=new MECH_LINK_TYPE; //aggiunge un elemento alla linked list
		iprev=m_pCurrent->id; //ultimo id
		m_pCurrent=m_pCurrent->next; //aggiorna l'elemento corrente
		
	}

	memset(m_pCurrent,0,sizeof(MECH_LINK_TYPE));

	m_pCurrent->iType=iType;
	m_pCurrent->xrel=xrel;
	m_pCurrent->yrel=yrel;
	m_pCurrent->lambda1=lambda1;
	m_pCurrent->lambda2=lambda2;	
	m_pCurrent->direction=direction;
	m_pCurrent->id=iprev+1;
	m_pCurrent->pLinked=NULL; 
	m_pCurrent->bOpenLoop=(iOpenLoop>0 ? 1 : 0);

	//coordinate della coppia nel sistema di riferimento con centro frame
	w2=(int)(frames[0]->width*0.5);
	h2=(int)(frames[0]->height*0.5);
	xt=xrel-w2;
	yt=yrel-h2;


	//definisce le coordinate relative di questa coppia cinematica 
	//per ognuna delle frame ruotate in modo da non doverle calcolare ogni volta
	for (int count=0;count<=this->max_frame_index;count++)
	{
		an=(delta_angle*count)*DEG_TO_RADIANS;
		//coordinate nel sistema ruotato con centro nell'origine della frame
		xt1=(int)(xt*cos(an)-yt*sin(an));
		yt1=(int)(xt*sin(an)+yt*cos(an));
		//traslazione (le coordinate relative sono riferite all'angolo sup. sinistro della frame)
		xt1+=(int)(frames[count]->width*0.5);
		yt1+=(int)(frames[count]->height*0.5);
		this->SetFrameHotSpot(count,m_pCurrent->id,xt1,yt1);
	}

	return m_pCurrent->id; //restituisce l'id assegnato all'accoppiamento*/
}

//restituisce un puntatore all'accoppiamento
MECH_LINK_TYPE_PTR CMechComponent::GetCoupling(int iCoupling)
{
	MECH_LINK_TYPE_PTR p=m_pFirstItem;

	if (!p) return NULL;

	while(p)
	{
		if (p->id == iCoupling) return p;
		p=p->next;
	}

	return NULL;
}

//acquisisce le coordinate relative del giunto
int CMechComponent::GetCouplingPos(int iCoupling,int *xrel,int *yrel)
{
	MECH_LINK_TYPE_PTR p=GetCoupling(iCoupling);

	if (p)
	{
		*xrel=p->xrel;
		*yrel=p->yrel;
		return 1;
	}

	else return 0;
}


void CMechComponent::Release(void)
{	

	MECH_LINK_TYPE_PTR p=NULL;

	//elimina la linked list dei componenti
	p=m_pFirstItem;

	while(p)
	{
		m_pCurrent=p->next;		
		delete p;
		p=m_pCurrent;
	}

	CADXSprite::Release();

	m_pFirstItem=m_pCurrent=NULL;

	m_status=0;

}

int CMechComponent::SetLambda(int lambda)
{
	if (!m_pBaseLink) return 0;
	if (!m_pParent) return 0;

	if (lambda<m_pBaseLink->lambda1) 
	{
		if (!m_pBaseLink->bOpenLoop) this->lambda=m_pBaseLink->lambda1;
		else this->lambda=m_pBaseLink->lambda2-((m_pBaseLink->lambda1-lambda)%(m_pBaseLink->lambda2-m_pBaseLink->lambda1));

	}
	else if (lambda>m_pBaseLink->lambda2) 
	{
		if (!m_pBaseLink->bOpenLoop) this->lambda=m_pBaseLink->lambda2; //ha raggiunto l'estremo superiore dell'intervallo
		else
		{
			this->lambda=m_pBaseLink->lambda1+((lambda-m_pBaseLink->lambda2)%(m_pBaseLink->lambda2-m_pBaseLink->lambda1));
		}
	}
	else this->lambda=lambda;

	//se � una coppia rotoidale allora aggiorna l'angolo (e anche la frame)
	if (m_pBaseLink->iType==LK_ROTOIDAL)
	{
		//nota: lambda � un angolo relativo al sistema di riferimento dell'oggetto genitore
		SetTheta(m_pParent->GetTheta()+lambda);
	}
	
	return 1;
}

int CMechComponent::GetLambda(void)
{
	return lambda;
}



//collega questo elemento ad un elemento genitore 
//iCoupling=acoppiamento di questo elemento al quale si attacca pComponent
//iParentCoupling=accoppiamento dell'elemento pComponent al qualle attaccare questo elemento
//rende 1 se ok 0 se errore
int CMechComponent::LinkToComponent(int iCoupling,int iParentCoupling,CMechComponent *pComponent)
{
		
	if (!pComponent || !m_status || m_pBaseLink) return 0;	

	m_pBaseLink=this->GetCoupling(iCoupling);	

	if (!m_pBaseLink) return 0;
	
	m_pParentLink=pComponent->GetCoupling(iParentCoupling);	

	if (!m_pParentLink) return 0;
    //controlla che alla coppia non sia gi� collegato un elemento
	if (m_pParentLink->pLinked) {m_pBaseLink=m_pParentLink=NULL;return 0;}

	if (m_pParentLink->iType != m_pBaseLink->iType) {m_pBaseLink=m_pParentLink=NULL;return 0;} //le coppie devono essere dello stesso tipo

    //collega questo elemento alla coppia del componente genitore
	m_pParentLink->pLinked=(CMechComponent *)this;
	
	m_pParent=pComponent;

	m_status=2;
	
	return 1;
}

//scollega il componente dal componente genitore
void CMechComponent::Unlink(void)
{
	if (!m_pBaseLink) return;

	m_status=1;

	if (m_pParentLink) m_pParentLink->pLinked=NULL;
	m_pBaseLink=m_pParentLink=NULL;
	m_pParent=NULL;
}

//imposta l'angolo di posizione piu' vicino posbile a iangle
//(ad igni frame corrisponde un angolo)
void CMechComponent::SetTheta(int iangle)
{
	int dir,newframe;
	int xnew,ynew;
	
	if (!m_pParent) return; //il membro adice della catena cinematica puo' solo traslare

	if (delta_angle==0) return;

	if (iangle<0) iangle=360+iangle;

	iangle%=360;

	int delta=theta;
    //aggiorna l'angolo di rotazione

	theta=(iangle/delta_angle)*delta_angle;

	delta=theta-delta;
	
	if (theta<m_from_angle) theta=m_from_angle;

	else if (theta>m_to_angle) theta=m_to_angle;

	newframe=(theta-m_from_angle)/delta_angle;

	xnew=m_pParent->x+m_pParentLink->xrel;
	ynew=m_pParent->y+m_pParentLink->yrel;

	SetJointPosition(xnew,ynew,m_pBaseLink->id);
		
	this->SetCurFrame(newframe);

	m_pLinkTemp=m_pFirstItem;

	while(m_pLinkTemp)
	{		
		m_pcTemp=(CMechComponent *)m_pLinkTemp->pLinked;
	
		//aggiorna la direzione delle coppie prismatiche
		if (m_pLinkTemp->iType==LK_PRISMATIC) 
		{
			dir=m_pLinkTemp->direction + delta;
			if (dir<0) dir=360-dir;
			//aggiorna i coseni direttori della coppia prismatica
			m_pLinkTemp->cx=(float)cos(dir*DEG_TO_RADIANS);
			m_pLinkTemp->cy=(float)sin(dir*DEG_TO_RADIANS);
			m_pLinkTemp->direction=dir;
		}
		
		//aggiorna le coordinate assolute e relative di questo link 
		m_pLinkTemp->xrel=delta_x[cur_frame][m_pLinkTemp->id];
		m_pLinkTemp->yrel=delta_y[cur_frame][m_pLinkTemp->id];
		m_pLinkTemp=m_pLinkTemp->next;

		if (m_pcTemp)
		{
			//aggiorna l'angolo dell'elemento figlio collegato a questo link
			m_pcTemp->IncrTheta(delta);
		}
	}
}



//restituisce l'angolo di rotazione
int CMechComponent::GetTheta(void)
{
	return theta;
}

//incrementa l'angolo
void CMechComponent::IncrTheta(int delta)
{
	SetTheta(theta+delta);
}

//ridefinisce UpdatePosition della classe CADXsprite
HRESULT CMechComponent::UpdatePosition(void)
{
	CMechComponent *pmc;

	int xnew,ynew;

	if (m_pParent)
	{
		//l'oggetto � linkato ad un oggetto genitore
		if (m_pParentLink->iType==LK_PRISMATIC)
		{
			xnew=(int)(m_pParent->x+m_pParentLink->xrel+lambda*m_pParentLink->cx);
			ynew=(int)(m_pParent->y+m_pParentLink->yrel+lambda*m_pParentLink->cy);
			SetJointPosition(xnew,ynew,m_pBaseLink->id);
		}
		else
		{
			//accoppiamenti fissi e rotoidali
			SetJointPosition(m_pParent->x+m_pParentLink->xrel,m_pParent->y+m_pParentLink->yrel,m_pBaseLink->id);		
		}

	}
	else
	{
		CADXSprite::UpdatePosition();
	}

	//aggiorna in modo ricorsivo tutti gli elementi collegati
	m_pLinkTemp=m_pFirstItem;

	while(m_pLinkTemp)
	{
		if (m_pLinkTemp->pLinked) 
		{
			pmc=(CMechComponent *)m_pLinkTemp->pLinked;
			pmc->UpdatePosition();
		}
						
		m_pLinkTemp=m_pLinkTemp->next;
	}

	return S_OK;
}

//restituisce il successivo elemento linkato direttamente a questo
CMechComponent* CMechComponent::GetNextChild(int reset)
{
	if (reset || !m_pWalk)
	{
		m_pWalk=m_pFirstItem;	
	}
	else m_pWalk=m_pWalk->next;

	if (m_pWalk)
	{
		//ricerca il successivo elemento linkato
		while(m_pWalk && !m_pWalk->pLinked && m_pWalk->next) m_pWalk=m_pWalk->next;

		if (m_pWalk)
		{
			if (m_pWalk->pLinked)
			{
				return (CMechComponent *)m_pWalk->pLinked;								
			}
		}
	}

	return NULL;
}


BOOL CMechComponent::isWalking(void)
{
	return (m_pWalk!=NULL);
}

//disegna tutta la catena cinematica
int CMechComponent::RenderKinematicChain(void)
{
	//disegna l'elemento corrente
	if (FAILED(CADXSprite::RenderToDisplay())) return 0;

	m_pLinkTemp=m_pFirstItem;

	while(m_pLinkTemp)
	{
		m_pcTemp=(CMechComponent*)m_pLinkTemp->pLinked;
		
		if (m_pcTemp)
		{
			m_pcTemp->RenderKinematicChain();			
		}

		m_pLinkTemp=m_pLinkTemp->next;
	}

	return 1;
}


int CMechComponent::RenderKinematicChain(IMAGE_FRAME_PTR pDest)
{
	this->SetDisplay(pDest);
		//disegna l'elemento corrente
	CADXSprite::RenderToDisplay();

	m_pLinkTemp=m_pFirstItem;

	while(m_pLinkTemp)
	{
		m_pcTemp=(CMechComponent*)m_pLinkTemp->pLinked;
		
		if (m_pcTemp)
		{
			m_pcTemp->RenderKinematicChain(pDest);			
		}

		m_pLinkTemp=m_pLinkTemp->next;
	}

	return 1;
}


HRESULT CMechComponent::SetDisplay(IMAGE_FRAME_PTR imgDisplay)
{
	return CADXRender::SetDisplay(imgDisplay);
}


CMechComponent *CMechComponent::GetParentComponent(void)
{
	return m_pParent;
}


