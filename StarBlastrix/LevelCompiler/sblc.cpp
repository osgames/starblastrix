//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
/*  Coded by Leonardo Berti - Tutti i diritti riservati (c) 2002
/*                          - All Rights Reserved
/*
/*  sblc.cpp - compilatore e lavel editor per Start Blastrix
///////////////////////////////////////////////////////////////////////*/

#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include "sbl.h"


int main(int argc,char *argv[])
{

	//debug------

//#define _DEBUG_

#ifdef _DEBUG_
		
	argc=3;
    
	char f1[]="C:\\projects\\win32\\starblastrix\\risorse\\level1.txt";
	char f2[]="level1.sbl";

#endif
	

	//-----------

	if (argc<=1)
	{
		printf("sblc - Compilatore di livelli per StarBlastrix\n\rCode by Leonardo Berti (c) 2004\n\rUtilizzo: sblc.exe [file binario destinazione],[script sbl sorgente]");
        return 0;
	}
	else if (argc>=3)
	{
		//ok esegue la compilazione del livello

#ifndef _DEBUG_

		if (!CompileLevel(argv[2],argv[1]))

#else
		if (!CompileLevel(f1,f2))
#endif
		{
			printf("\n\rERRORE:\n\r %s",GetSbLastError());
			return 1;
		}
		else
		{
			printf("\n\rCompilazione completata con successo!");		
		}

	}
	else
	{
		printf("sblc - Compilatore di livelli per StarBlastrix\n\rCode by Leonardo Berti (c) 2004\n\rUtilizzo: sblc.exe [file binario destinazione],[script sbl sorgente]\n\rNumero di parametri non corretto!");
        return 1;
	}

	getch();

	return 0;
}