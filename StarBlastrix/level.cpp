//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  level.cpp - Classe per gestione dei livelli di gioco
///////////////////////////////////////////////////////////////////////*/

#include "a32graph.h" //libreria grafica 2D arcade 32 
#include "a32fastm.h" //libreria funzioni matematiche
#include "a32sprit.h" //classe sprite
#include "a32object.h" //oggetti e stack di oggetti astratti
#include "level.h"
#include "enemy.h" //nemici   
#include "sbengine.h"

extern CADXDebug g_cdb;

/////////////////////////////////// CSbLevel /////////////////////////////////////////////////

//membri statici privati
float CSbLevel::g_gravity=0;
int CSbLevel::g_bottom=0;
int CSbLevel::g_scroll_speed=0;
	

CSbLevel::CSbLevel(void)
{
	this->Release();
	m_lpFm=NULL;
	m_lpGm=NULL;
	m_iStatus=0;
	pTriggers=NULL;
	pCurTrigger=NULL;
	pTopTrigger=NULL;
	m_lpActiveStack=NULL;
	m_dwNumTriggers=0;
	m_piGroundType=NULL;
	m_iScrollSpeed=1;
	m_szFinalMessage=NULL;
	m_szLevelName=NULL;
	m_sBackLayer=NULL;
	m_sLayer=NULL;
	m_prcLayer=NULL;
	m_pmaps=NULL;
	m_pbackimg=NULL;
	m_lMaxX=0;
	m_lLimit=0;
	m_password=0;
	m_dwNumScrollLayers=0;
	m_dwNumScrollBackground=0;
	ResetStats(); //resetta il vettore con la statistica nemici colpiti
	//resetta le variabili statiche
	

}

CSbLevel::~CSbLevel(void)
{
	this->Release();
	m_lpFm=NULL;
	m_lpGm=NULL;
	m_iStatus=0;	
	SAFE_DELETE_ARRAY(pTriggers);
	SAFE_DELETE_ARRAY(m_piGroundType);
	SAFE_DELETE_ARRAY(m_szLevelName);
	SAFE_DELETE_ARRAY(m_szFinalMessage);
	SAFE_DELETE_ARRAY(m_sLayer);
	SAFE_DELETE_ARRAY(m_sBackLayer);
	SAFE_DELETE_ARRAY(m_prcLayer);
	SAFE_DELETE_ARRAY(m_pmaps);
	SAFE_DELETE_ARRAY(m_pbackimg);
	pTriggers=NULL;
	m_dwNumTriggers=0;
}

//--------------------------------------- ResetStats ---------------------------------
//resetta il vettore che contiene il numero di nemici colpiti
void CSbLevel::ResetStats(void)
{
	memset(&m_Stats,0,sizeof(SB_STAT)*(EN_LAST+1)); //resetta il vettore delle statistiche
}

//---------------------------------------- LevelName --------------------------------

void CSbLevel::SetLevelName(TCHAR *szLevelName)
{
	SAFE_DELETE_ARRAY(m_szLevelName)
	
	if (szLevelName) 
	{
		size_t len;
		len=strlen(szLevelName)+1; //lunghezza in bytes
		m_szLevelName=new TCHAR[len/sizeof(TCHAR)];
		memset(m_szLevelName,0,len);
		memcpy(m_szLevelName,szLevelName,len);

	}

}

//---------------------------------------- GetLevelName ---------------------------------

TCHAR *CSbLevel::GetLevelName(void)
{
	return m_szLevelName;
}

//--------------------------------------- LevelFinalMessage ------------------------------
//messaggio di fine livello

void CSbLevel::SetFinalMessage(TCHAR *szFinalMessage)
{
	SAFE_DELETE_ARRAY(m_szFinalMessage)
	if (szFinalMessage)
	{
		size_t len;
		len=strlen(szFinalMessage);
		m_szFinalMessage=new TCHAR[len/sizeof(TCHAR)];
		memset(m_szFinalMessage,0,len);
		memcpy(m_szFinalMessage,szFinalMessage,len);
	}
}

TCHAR *CSbLevel::GetFinalMessage(void)
{
	return m_szFinalMessage;
}
	
//restituisce il vettore dei trigger
SB_TRIGGER_PTR CSbLevel::GetTriggers(void)
{
	return pTriggers;
}

//restituisce il top trigger
SB_TRIGGER_PTR CSbLevel::GetTopTrigger(void)
{
	return pTopTrigger;
}

//-------------------------------------- m_pTop::Release ----------------------------------------------

void CSbLevel::Release(void)
{	
	DWORD count;

	m_lpFm->FreeImgFrame(&m_imgSource);	
	//rilascia i triggers
	SAFE_DELETE_ARRAY(pTriggers);
	SAFE_DELETE_ARRAY(m_piGroundType);
	SAFE_DELETE_ARRAY(m_sLayer);
	SAFE_DELETE_ARRAY(m_sBackLayer);
	m_dwNumTriggers=0;
	
	if (m_prcLayer)
	{	
		//rilascia i rettangoli di grabbing dei layer
		for (count=0;count<m_dwNumScrollLayers;count++) 
		{
			SAFE_DELETE_ARRAY(m_prcLayer[count].pRect);
			SAFE_DELETE_ARRAY(m_pmaps[count].pimap); 
		}
	}

	SAFE_DELETE_ARRAY(m_prcLayer);
	SAFE_DELETE_ARRAY(m_pmaps);
	SAFE_DELETE_ARRAY(m_pbackimg);
	    
	m_dwDispW=0;m_dwDispH=0;
	m_pDDSSource=NULL;
	m_lLevelCurrentX=0;
	pCurTrigger=NULL;

	ResetStats();
}

//-------------------------------------- CSbLevel::SetScrollLayer --------------------------------------
/*Imposta un layer di scrolling del terreno
dwLayer=indice del layer
lTop=ordinata di output del layer
fParallax=parallasse
lrcLeft,lrcTop=coordinate dell'angolo in alto a sinistra del rettangolo di grbbing dei tiles
lCols,lRows=colonne e riche nella griglia dei tiles per questo layer
lTileWidth,lTileHeight=larghezza e altezza dei tile
piArray=punta all'array che contiene la mappa dei tiles
dwNumItems=numero di elementi in piArray
*/

HRESULT CSbLevel::SetScrollLayer(DWORD dwLayer,LONG lTop,float fParallax,LONG lrcLeft,LONG lrcTop,LONG lCols,LONG lRows,LONG lTileWidth,LONG lTileHeight,UINT *piArray,DWORD dwNumItems)
{
	LONG lRects;
	LONG cr,cc;
	LPRECT prc;
	int index=0;

	if (dwLayer>=m_dwNumScrollLayers) return E_FAIL;
	if (lCols<=0 || lRows<=0) return E_FAIL;
	if (lTileWidth<=0 || lTileHeight<=0) return E_FAIL;
	if (!piArray) return E_FAIL;
	if (!dwNumItems) return E_FAIL;
	if (!m_prcLayer) return E_FAIL;

	m_iStatus=2;

	SAFE_DELETE_ARRAY(m_prcLayer[dwLayer].pRect);
	//crea il vettore con i rettangoli di grabbing

	lRects=lCols*lRows;

	m_prcLayer[dwLayer].pRect=new RECT[lRects];
	prc=(LPRECT)m_prcLayer[dwLayer].pRect;

	//Crea i rettangoli di grabbing
	//la matrice dei tiles � una griglia di lColsxlRows colonnexrighe
	//Il tile con indice zero � quello dell'angolo sup. sinistro il secondo � quello della stessa riga nella colonna adiacente ecc...

	//ciclo righe
	for (cr=0;cr<lRows;cr++)
	{
		//ciclo colonne
		for (cc=0;cc<lCols;cc++)
		{
			prc[index].left=lrcLeft+cc*lTileWidth;
			prc[index].top=lrcTop+cr*lTileHeight;
			prc[index].right=prc[index].left+lTileWidth;
			prc[index].bottom=prc[index].top+lTileHeight;
			index++;
		}
	}

	//crea la mappa dei tiles
	m_pmaps[dwLayer].pimap=new int[dwNumItems];
	memcpy(m_pmaps[dwLayer].pimap,piArray,dwNumItems*sizeof(int));

    if (FAILED(this->SetScrollTileArray(prc,(DWORD)lRects,dwLayer))) return E_FAIL;

	if (FAILED(this->SetScrollMap((UINT*)m_pmaps[dwLayer].pimap,dwNumItems,dwLayer,lTop,fParallax))) return E_FAIL;
	
    prc=NULL;

	return S_OK;
}

//------------------------------------- CsbLevel::SetScrollTileArray ------------------------------------

//imposta il vettore dei tiles per il livello dwLayer (dwLayer va da 0 a 3)
//Nota: i tiles nel vettore di un layer devono avere tutti le stesse dimensioni

HRESULT CSbLevel::SetScrollTileArray(LPRECT prcArray,DWORD dwNumTiles,DWORD dwLayer)
{	
	if (prcArray == NULL) return E_FAIL;
	if (dwNumTiles == 0) return E_FAIL;
	if (dwLayer >= m_dwNumScrollLayers) return E_FAIL;
	
    m_iStatus=2;

	m_sLayer[dwLayer].dwNumTiles=dwNumTiles;
	m_sLayer[dwLayer].prcScrollTile=prcArray; //punta al vettore che contiene i rettangoli di grabbing delle varie frame		
	m_sLayer[dwLayer].lTileWidth=prcArray[0].right-prcArray[0].left;//larghezza dei tile di questo livello

	if (dwLayer==0)
	{
		//quando imposta il layer di primo piano,resetta anche il vettore con le propriet� del terreno
		m_piGroundType=new int[dwNumTiles];

		if (!m_piGroundType) return E_FAIL;

		memset(m_piGroundType,LV_SOLID,dwNumTiles*sizeof(int));

	}

	return S_OK;
}

//------------------------------------ CSbLevel::SetGroundType ---------------------------------

HRESULT CSbLevel::SetGroundType(int iTileIndex,int iType)
{
	if (!m_piGroundType) return E_FAIL;
	if (m_iStatus<2) return E_FAIL; //il livello non � inizializzato

	if (iTileIndex>=0 && iTileIndex<(int)m_sLayer[0].dwNumTiles)
	{
		m_piGroundType[iTileIndex]=iType;
		return S_OK;
	}

	else return E_FAIL;
}


//---------------------------------- CSbLevel::GetGroundType -------------------------------------------

//restituisce il tipo di terreno in corrispondenza della ascissa xrel relativa alla x assoluta corrente
//se xrel=0 rende il tipo di terreno in corrispondenza della x assoluta corrente
int CSbLevel::GetGroundType(int xrel)
{	
	SCROLL_LAYER_PTR psl;
	int iTile=0;
	
    if (xrel<0) return 0;

	psl=&m_sLayer[0];
    iTile=psl->dwCurrentTile+(int)(xrel/psl->lTileWidth);
	
	return m_piGroundType[psl->piMap[iTile]];
}

//------------------------------------------- CSbLevel::SetScrollMap -----------------------------------------
//Imposta le mappe per ogni livello di scroll
//piArray= array di integer con gli indici dei tiles
//dwNumItems=numero di elementi nell'array
//dwLayer=indice del layer (livello di scrolling)
//lTop ordinata di output per il layer 
HRESULT CSbLevel::SetScrollMap(UINT *piArray,DWORD dwNumItems,DWORD dwLayer,LONG lTop,float fParallax)							 
{
	UINT count;

	if (dwLayer>= m_dwNumScrollLayers) return E_FAIL;
	if (piArray==NULL) return E_FAIL;
	if (dwNumItems==0) return E_FAIL;

	m_iStatus=2;

	//esegue un controllo per vedere se ci sono degli indici fuori range nella mappa
	for (count=0;count<dwNumItems;count++)
	{
		if (piArray[count]>=m_sLayer[dwLayer].dwNumTiles) 
		{
			this->SetLastError("Errore impostando la mappa: layer=%d elemento=%d il tile %d � fuori range!",dwLayer,count,piArray[count]);
			return E_FAIL;
		}

	}

	m_sLayer[dwLayer].piMap=piArray;	
	m_sLayer[dwLayer].lTop=lTop;
	m_sLayer[dwLayer].dwNumMapItems=dwNumItems;
	m_sLayer[dwLayer].fParallax=fParallax;

	return S_OK;
}


//------------------------------ CSbLevel::SetDisplay ------------------------------------------------------
//imposta la larghezza e l'altezza della superficie su cui viene fatto il rendering del livello
void CSbLevel::SetDisplay(DWORD dwDispW,DWORD dwDispH)
{	
	m_dwDispW=dwDispW;
	m_dwDispH=dwDispH;
	//definisce il rettangolo di clipping del display
	m_rcDisplay.left=0;
	m_rcDisplay.top=0;
	m_rcDisplay.right=dwDispW;
	m_rcDisplay.bottom=dwDispH;
}

//-------------------------- CSbLevel::SetScrollBackground --------------------------------------------------

//imposta l'immagine dei layer di background
/*
dwLyaer=layer da impostare (prima di chimare questa funzione si devono definire il numero di layer di background tramite SetNumScrollBackground)
lTop=ordinata di ouput del layer
fParallax=parallasse
lrcLeft,lrcTop ecc...=rettangolo di grabbing per l'immagine del background
imgSrc=immagine dalla quale estrae l'immagine del layer
*/

HRESULT CSbLevel::SetScrollBackground(DWORD dwLayer,LONG lTop,float fParallax,LONG lrcLeft,LONG lrcTop,LONG lrcRight,LONG lrcBottom,IMAGE_FRAME_PTR imgSrc)
{	
	RECT rcTemp;

	//prima di impostare il background si deve definire il numero di layer che lo costituiscono
	if (dwLayer>= m_dwNumScrollBackground) return -1;
	if (!m_lpFm) return E_FAIL;

	rcTemp.left=lrcLeft;
	rcTemp.top=lrcTop;
	rcTemp.bottom=lrcBottom;
	rcTemp.right=lrcRight;

	if (FAILED(m_lpFm->GrabFrame(&m_pbackimg[dwLayer],&rcTemp,imgSrc))) return -2;

	m_iStatus=1;

	m_sBackLayer[dwLayer].lTop=lTop;
	m_sBackLayer[dwLayer].pimgBack=&m_pbackimg[dwLayer];
	m_sBackLayer[dwLayer].fParallax=fParallax;
	
	return S_OK;
}

//--------------------------- CSbLevel::SetBackgroundClearFn ----------------------------------------------

//Imposta il puntatore alla funzione che disegna il background
HRESULT CSbLevel::SetBackgroundClearFn(void (*lpFn)(IMAGE_FRAME_PTR))
{	
	if (lpFn==NULL) return E_FAIL;
	else
	{		
		this->m_lpfnClear=lpFn;
	}

	return S_OK;
}

//------------------------------------- CSbLevel::GetNumScrollLayers ----------------------------
DWORD CSbLevel::GetNumScrollLayers(void)
{
	return m_dwNumScrollLayers;
}

//---------------------------------------- password ----------------------------------------------

void CSbLevel::SetPassword(int pwd)
{
	m_password=pwd;
}

int CSbLevel::GetPassword(void)
{
	return m_password;
}

//------------------------------------ CSbLevel::SetNumScrollLayers --------------------------------

BOOL CSbLevel::SetNumScrollLayers(DWORD dwNum) //imposta il numero di layer per il terreno
{
	if (m_iStatus<=1) //nota: se lo stato � >=2 allora significa che i layer sono gi� riempiti
	{
		m_dwNumScrollLayers=dwNum;

		if (dwNum==0)
		{
			//azzera il numero di layer
			m_iStatus=0; //resetta lo stato
			SAFE_DELETE_ARRAY(m_sLayer);
			SAFE_DELETE_ARRAY(m_prcLayer);
		}
		else
		{
			m_iStatus=1;			
			m_sLayer=new SCROLL_LAYER[dwNum];
			//crea un vettore di puntatori a strutture RECT			
			m_prcLayer=new SB_RECTARRAY[dwNum];
			//resetta tutti i puntatori del vettore
			memset(m_prcLayer,0,sizeof(SB_RECTARRAY)*dwNum);
			//mappe dei tiles
			m_pmaps=new SB_TILEMAP[dwNum];			
		}

		return TRUE;
	}

	else return FALSE;
}


//------------------------------------- CSbLevel::GetNumScrollBackground -----------------------------

DWORD CSbLevel::GetNumScrollBackground(void) //numero di layer del background
{	
	return m_dwNumScrollBackground;
}

//------------------------------------ CSbLevel::SetScrollBackground -------------------------------

BOOL CSbLevel::SetNumScrollBackground(DWORD dwNum) //imposta il numero di layer per il background
{

	if (m_iStatus<=1)
	{	
		m_dwNumScrollBackground=dwNum;

		if (dwNum==0)
		{
			m_iStatus=0;
			SAFE_DELETE_ARRAY(m_sBackLayer);

		}
		else
		{
			m_iStatus=1;
			m_sBackLayer=new SCROLL_LAYER[dwNum];
			memset(m_sBackLayer,0,sizeof(SCROLL_LAYER)*dwNum);
			m_pbackimg=new IMAGE_FRAME[dwNum];
			memset(m_pbackimg,0,sizeof(IMAGE_FRAME)*dwNum);
		}
		return TRUE;
	}

	return FALSE;	
}

//-------------------------- CSbLevel::GetCurrentX ---------------------------------------------------------

//rende la X corrente
LONG CSbLevel::GetCurrentX(void)
{
	return m_lLevelCurrentX;
}


//------------------------- ScrollSpeed --------------------------------------------------
//restituisce la velocit� di scrolling
int CSbLevel::GetScrollSpeed(void)
{
	return m_iScrollSpeed;
}

//imposta la velocit� di scrolling (pixel/ciclo)
void CSbLevel::SetScrollSpeed(int ispeed)
{
	CSbLevel::g_scroll_speed=ispeed;
	m_iScrollSpeed=ispeed;
}

//------------------------------------------- CSbLevel::CreateLevel -------------------------------------
//Crea il livello corrente
//szVPXSource � il file VPX che contiene l'immagine con i tiles
//dwFrame � l'indice dell'immagine nel file VPX
//pActiveStack � lo stack degli oggetti attivi

HRESULT CSbLevel::CreateLevel(TCHAR *szVPXSource,DWORD dwFrame,CObjectStack *pActiveStack)
{

	LONG lw,lh;
	DWORD dwCount;
	LPRECT pimg=NULL;  

	if (szVPXSource==NULL) return E_FAIL;
	if (m_dwDispW==0 || m_dwDispH==0) return E_FAIL;
	if (m_iStatus<2) return E_FAIL; //il livello non � inizializzato correttamente

	//crea l'immagine che contiene i tiles
	if (FAILED(m_lpFm->CreateImgFrameFromVPX(&m_imgSource,szVPXSource,dwFrame)))
	{
		return E_FAIL;
	}

	m_pDDSSource=m_imgSource.surface; //collega il puntatore alla superficie

	for (dwCount=0;dwCount<m_dwNumScrollLayers;dwCount++)
	{
		//esce al primo layer libero
		if (m_sLayer[dwCount].prcScrollTile == NULL) break;
		//in ogni layer che non sia null, ci devono essere immagini
		if (m_sLayer[dwCount].dwNumTiles == 0) return E_FAIL;		

		pimg = m_sLayer[dwCount].prcScrollTile;
		
		//determina la larghezza del tile di questo livello
		lw=pimg->right-pimg->left;
		lh=pimg->bottom - pimg->top;       
		

		if (lh<=0 || lw<=0) return E_FAIL;
		
        m_sLayer[dwCount].lTileWidth=lw; //larghezza dei tiles del livello
		m_sLayer[dwCount].dwCurrentX=0;//x corrente del livello di scrolling       
        
		m_sLayer[dwCount].dwCurrentTile=0; //tile iniziale		
		m_sLayer[dwCount].dwTilesOnScreen=m_dwDispW / lw + 2; //numero di tiles necessari a coprire lo schermo da lato a lato	
		m_sLayer[dwCount].dwDeltaClip=m_sLayer[dwCount].dwTilesOnScreen*lw - m_dwDispW;
		m_sLayer[dwCount].rcClipRectLeft.top=0;
		m_sLayer[dwCount].rcClipRectLeft.bottom=lh;
	}

	m_lLevelCurrentX=0; //x corrente = 0 
	
    //inizializza i layer del background (cielo scorrevole con 2 livelli di parallasse)
	for (dwCount=0;dwCount<m_dwNumScrollBackground;dwCount++)
	{
		if (m_sBackLayer[dwCount].pimgBack == NULL) break;
		
		m_sBackLayer[dwCount].rcViewPort.left=0;
		m_sBackLayer[dwCount].rcViewPort.top=0;
		m_sBackLayer[dwCount].rcViewPort.right=m_dwDispW;	
		m_sBackLayer[dwCount].rcViewPort.bottom=m_sBackLayer[dwCount].pimgBack->height;	
		m_sBackLayer[dwCount].pDDSBack=m_sBackLayer[dwCount].pimgBack->surface;
		m_sBackLayer[dwCount].dwCurrentX=0;
		m_sBackLayer[dwCount].lTileWidth=m_sBackLayer[dwCount].pimgBack->width;			
	}	

	lw=m_sLayer[0].prcScrollTile->right-m_sLayer[0].prcScrollTile->left;

	//calcola la larghezza del livello (� quella riferita al livello di scrolling 0 cio� quello piu' vicino
    m_lMaxX=lw*m_sLayer[0].dwNumMapItems;	
	m_lLimit=m_lMaxX;

	this->SetPosition(0); //imposta il livello sulla posizione iniziale (x=0)

	//imposta i triggers
	//prima di creare il livello deve essere impostato lo stack attivo

	m_lpActiveStack=pActiveStack;

	//ordina i triggers dal piu' vicino al piu' lontano
	SortTriggers();

	if (pTriggers && m_lpActiveStack) 
	{
		pCurTrigger=pTriggers; //il trigger corrente � il primo
	}

	m_iStatus=3;

	return S_OK;
}

//----------------------------- CSBLevel::RaiseEvent --------------------------------------
//In base alla x assoulta dello schema,determina quali triggers deve eseguire
//e sposta in avanti il puntatore NextEvent
int CSbLevel::RaiseEvent(void)
{

#ifdef _TRACETRIGGERS_
	LONG index;
#endif

	//lo stack degli eventi deve contenere tutti oggetti con classe derivata da 
	CEnemy *pobj;
	CObjectStack *pstack;
	
	if (pCurTrigger)
	{
		m_lLevelXTrigger=m_lLevelCurrentX+m_dwDispW+70;

		while (m_lLevelXTrigger >= pCurTrigger->xabs)
		{

			pstack=pCurTrigger->pSrcStack;									

#ifdef _TRACETRIGGERS_

			g_cdb.WriteErrorFile("trigger x=%d y=%d stack=%s",pCurTrigger->xabs,pCurTrigger->y,TranslateLabel(GetResId(pstack)));			
#endif
			
			if (pstack)
			{
				//estrae un oggetto dallo stack del trigger
				//occorre un cast esplicito perch� pstack � uno stack di oggetti CSbObject
				pobj=(CEnemy *)pstack->Pop();

				if (pobj)
				{
#ifdef _TRACETRIGGERS_
					g_cdb.WriteErrorFile("oggetto disponibile");
#endif
					//imposta i flags aggiuntivi (specificano il comportamento)
					pobj->SetFlags(pCurTrigger->dwFlags);			
					//resetta le variabili di stato dell'oggetto
					pobj->Reset();
					//imposta la posizione iniziale dell'oggetto (deve calcolare la x relativa rispetto alla x corrente del livello)
					pobj->SetPosition(pCurTrigger->xabs-m_lLevelCurrentX,pCurTrigger->y);			
					//imposta l'energia dell'oggetto
					pobj->SetEnergy(pCurTrigger->dwEnergy);
					//imposta il "potere" dell'oggetto
					pobj->dwPower=pCurTrigger->dwPower;
					//bonus (se pBonusStack del trigger � nullo, il nemico non contiene bonus)
					pobj->m_pBonusStack=pCurTrigger->pBonusStack;
					//zlayer
					pobj->m_zlayer=pCurTrigger->zlayer;
					//immette l'oggetto nello stack attivo					
					

#ifdef _TRACETRIGGERS_
					index=m_lpActiveStack->Push(pobj); 												
					g_cdb.WriteErrorFile("oggetto creato con indice %d",index);
#else
					m_lpActiveStack->Push(pobj); 		
#endif
				
				}
			}
			
			if (pCurTrigger==pTopTrigger) 
			{
				pCurTrigger=NULL; //fine dei trigger
				break; //esce quando si arriva all'ultimo trigger
			}

			//passa al trigger successivo
			pCurTrigger++;
		}
	}

	return 0;
}

//---------------------------------------- CSbLevel::CreateTrigger ---------------------------------------------
/*
	xabs=ascissa assolute in cui si verifica il trigger
	xrel,yrel=coordinate di putting dell'oggetto
	dwPower=potere da associare all'oggetto creato
	dwEnergy=energia dell'oggetto creato
	dwFlags=opzioni
	pEnemyStack=collezione di oggetti dalla quale preleva l'oggetto da inserire nello stack attivo
	pBonusStack=viene impostato sull'oggetto bonus che sar� rilasciato quando in nemico verr� distrutto
*/
//Nota: andrebbe fatto con le linked list 
HRESULT CSbLevel::CreateTrigger(LONG xabs,LONG yrel,DWORD dwPower,DWORD dwEnergy,DWORD dwFlags,CObjectStack *pEnemyStack,CObjectStack *pBonusStack,int zlayer)
{
	SB_TRIGGER_PTR pTempTriggers=NULL;
	DWORD dwCount;
	DWORD dwNewIndex;
	size_t trsize;

	m_dwNumTriggers++;

	pTempTriggers=new SB_TRIGGER[m_dwNumTriggers];

	if (!pTempTriggers) return E_FAIL; //errore di allocazione di memoria

	trsize=sizeof(SB_TRIGGER);

	if (!pTriggers) pTriggers=new SB_TRIGGER[1]; //inizializza il vettore

	if (!pTriggers) 
	{
		SAFE_DELETE_ARRAY(pTempTriggers);
		return E_FAIL; //errore durante l'inizializzazione del vettore dei triggers
	}

	//copia il vettore originale nel vettore temporaneo cha ha un elemento in piu' rispetto all'originale
	for (dwCount=0;dwCount<m_dwNumTriggers-1;dwCount++)
	{
		memcpy(&pTempTriggers[dwCount],&pTriggers[dwCount],trsize);
	}

    dwNewIndex=m_dwNumTriggers-1; 

	//inserisce un nuovo trigger nel vettore temporaneo
	pTempTriggers[dwNewIndex].xabs=xabs;
//	pTempTriggers[dwNewIndex].x=xrel;
	pTempTriggers[dwNewIndex].y=yrel;
	pTempTriggers[dwNewIndex].dwPower=dwPower;
	pTempTriggers[dwNewIndex].dwEnergy=dwEnergy;
	pTempTriggers[dwNewIndex].dwFlags=dwFlags;
	pTempTriggers[dwNewIndex].pSrcStack=pEnemyStack;
	pTempTriggers[dwNewIndex].pBonusStack =pBonusStack;
	pTempTriggers[dwNewIndex].zlayer=zlayer;

	//a questo punto ricrea il vettore originale con un elemento in piu' (redim) e ci ricopia il vettore temporaneo
	SAFE_DELETE_ARRAY(pTriggers);
	
	pTriggers=new SB_TRIGGER[m_dwNumTriggers];
	memcpy(pTriggers,pTempTriggers,trsize*m_dwNumTriggers);

	//rilascia il vettore temporaneo
	SAFE_DELETE_ARRAY(pTempTriggers); 
    //pTopTrigger punta l'ultimo elemento della lista	
	pTopTrigger=&pTriggers[dwNewIndex];

	return S_OK;	
}	

//--------------------------------------- CSbLevel::RemoveTrigger --------------------------------
//Elimina il trigger dalla lista

//Nota: andrebbe fatto con le linked list 

HRESULT CSbLevel::RemoveTrigger(SB_TRIGGER_PTR pTrigger)
{
	if (m_dwNumTriggers==0) return E_FAIL;

	SB_TRIGGER_PTR pTempTriggers=NULL;
	size_t trsize;
	DWORD count;
	DWORD count1=0;

	pTempTriggers=new SB_TRIGGER[m_dwNumTriggers];

	if (!pTempTriggers) return E_FAIL; //errore di allocazione di memoria

	trsize=sizeof(SB_TRIGGER);

	for (count=0;count<m_dwNumTriggers;count++)
	{
		if (&pTriggers[count] != pTrigger)
		{
			memcpy(&pTempTriggers[count1],&pTriggers[count],trsize);
			count1++;
		}	
	}

	if (count1<m_dwNumTriggers)
	{
		SAFE_DELETE_ARRAY(pTriggers);

		m_dwNumTriggers=count1;
        //cre un vettore piu' corto di una unit�
		pTriggers=new SB_TRIGGER[m_dwNumTriggers];

		memcpy(pTriggers,pTempTriggers,trsize*m_dwNumTriggers);

		pTopTrigger=&pTriggers[m_dwNumTriggers-1];

	}

	SAFE_DELETE_ARRAY(pTempTriggers);

	return S_OK;

}

//--------------------------------------- CSbLevel::SortTriggers ---------------------------------
//Ordina i triggers dal piu' vicino al piu' lontano in base alla ascissa del livello
void CSbLevel::SortTriggers(void)
{	
	LONG count1;
	DWORD dwCurIndex;

	SB_TRIGGER_PTR pTempTrigger=NULL;
	SB_TRIGGER trSwap;

	pTempTrigger=pTriggers;

	for (LONG count=0;count<(LONG)m_dwNumTriggers;count++)
	{
		dwCurIndex=0;

		for (count1=0;count1<(LONG)m_dwNumTriggers-count;count1++)
		{
			if (pTriggers[count1].xabs>pTriggers[dwCurIndex].xabs)
			{
				dwCurIndex=count1;
			}		
		}

		//swap
		memcpy(&trSwap,&pTriggers[(LONG)m_dwNumTriggers-count-1],sizeof(SB_TRIGGER));
		memcpy(&pTriggers[(LONG)m_dwNumTriggers-count-1],&pTriggers[dwCurIndex],sizeof(SB_TRIGGER));
		memcpy(&pTriggers[dwCurIndex],&trSwap,sizeof(SB_TRIGGER));
	}

}
//--------------------------------------- CSbLevel::SetXLimit ------------------------------------

//punto finale del livello in cui inizia la sequenza di fine livello
void CSbLevel::SetXLimit(LONG limit)
{
    if (limit>=0 && limit<m_lMaxX) m_lLimit=limit;
}

LONG CSbLevel::GetXLimit(void)
{
    return m_lLimit;
}
//---------------------------------------- CSbLevel::ScrollOn ------------------------------------------
void CSbLevel::ScrollOn(IMAGE_FRAME_PTR pimgDest)
{
	SetPosition(m_lLevelCurrentX+m_iScrollSpeed);
	Render(pimgDest);
}

//-------------------------------- CSbLevel::RenderTriggers ------------------------------------
//Funzione usata nel level editor per visualizzare i triggers
//visualizza i nemici in modo statico 
//pSelected indica il trigger selezionato corrente
HRESULT CSbLevel::RenderTriggers(IMAGE_FRAME_PTR pimgDest,SB_TRIGGER_PTR pSelected)
{
	SB_TRIGGER_PTR pTrigger=NULL;
	CSBObject *pobj=NULL;
	IMAGE_FRAME_PTR pFrame;

	pTrigger=this->pTriggers;

	if (!pimgDest) return E_FAIL;
	if (!pimgDest->status) return E_FAIL;
	
	for (DWORD cnt=0;cnt<m_dwNumTriggers;cnt++)
	{
		pTrigger=&this->pTriggers[cnt];

		if (pTrigger)
		{
		
		if (pTrigger->xabs>-100 && pTrigger->xabs<m_lLevelCurrentX+(LONG)m_dwDispW+40)
		{
			if (pTrigger->pSrcStack)
			{
				pobj=(CSBObject *)pTrigger->pSrcStack->Pop();

				if (pobj)
				{

					pobj->SetPosition(pTrigger->xabs-m_lLevelCurrentX,pTrigger->y);

					pobj->SetDisplay(pimgDest);

					pobj->Draw();
				
					if (pSelected && pTrigger==pSelected)
					{

						m_lpGm->UseScreen(1);

						pFrame=pobj->GetFramePtr(pobj->GetCurFrame());
						if (pFrame)
						{
							
							//disegna la frame di selezione intorno all'oggetto
							m_lpGm->DrawRectangle(pobj->x,pobj->y,pobj->x+pFrame->width,pobj->y+pFrame->height,m_lpGm->CreateRGB(255,0,0));
						}
						else
						{
							//disegna la frame di selezione intorno all'oggetto
							m_lpGm->DrawRectangle(pobj->x,pobj->y,pobj->x+80,pobj->y+80,m_lpGm->CreateRGB(255,0,0));		

						}

					}			

					pobj->bActive=FALSE;

					pobj->dwStatus=0;
				
				}

			}
		}

		} // if pTrigger

		
	}

	return S_OK;

}

//------------------------------------- CSbLevel::GetTriggerAt ------------------------------------
//Hit test per vedere quale trigger corrisponde alle coordinate x y
SB_TRIGGER_PTR CSbLevel::GetTriggerAt(LONG x,LONG y)
{
	IMAGE_FRAME_PTR pFrame;
	SB_TRIGGER_PTR pTrigger=this->pTriggers;
	CSBObject *pobj;
	int counter=0;

	while (pTrigger != NULL && pTrigger!=pTopTrigger)
	{
		if (pTrigger->pSrcStack)
		{
			pobj=NULL;
			pobj=(CSBObject *)pTrigger->pSrcStack->Pop();
			
			if (pobj)
			{

				counter++;

				pFrame=pobj->GetFramePtr(0);

				if (pFrame)
				{
					if (x>=pTrigger->xabs && x<=pTrigger->xabs+(LONG)pFrame->width && y>=pTrigger->y && y<=pTrigger->y+(LONG)pFrame->height)
					{
						pobj->bActive=FALSE;
						pobj->dwStatus=0;
						return pTrigger; //hit test ok
					}
				}
				else
				{
					if (x>=pTrigger->xabs && x<=pTrigger->xabs+80 && y>=pTrigger->y && y<=pTrigger->y+80)
					{
		
						pobj->bActive=FALSE;
						pobj->dwStatus=0;
						return pTrigger; //hit test ok

					}
				}

				pobj->bActive=FALSE;

				pobj->dwStatus=0;
			}


		}

		pTrigger++; //passa al trigger successivo

	}

	return NULL; 

}

//------------------------------------ CSbLevel::Render --------------------------------------------

//Disegna lo schema sull'immagine pimgDest
//pimgDest deve avere un clipper attaccato
void CSbLevel::Render(IMAGE_FRAME_PTR pimgDest)
{

	LPDIRECTDRAWSURFACE7 lpDDS;	
	SCROLL_LAYER_PTR psl;
	LPRECT prcTile;
	LONG dwCount; //questo deve essere LONG ! 
	DWORD iCur;	
	DWORD dwCurIndex;
	DWORD dwTileW;
	LONG dwOut;
	DWORD dwNumTiles;
	UINT *piMap;
	DWORD dwTop;
	DWORD dwBltFlags;
	LONG lxout;
	HRESULT hr;

	dwBltFlags=DDBLT_KEYSRC | DDBLT_WAIT;
	
	lpDDS=pimgDest->surface; //punta alla superficie di destinazione

	//pulisce il background
	m_lpfnClear(pimgDest);

	//disegna i layer del terreno partendo da quello piu' lontano
	//(usa l'algoritmo del pittore disegnando i layer piu' lontani per primi)
	for (dwCount=m_dwNumScrollLayers-1;dwCount>=0;dwCount--)
	{
		//punta il layer corrente 
		psl=&m_sLayer[dwCount];								
	    //larghezza di un tile nel layer corrente
		dwTileW=psl->lTileWidth;
		//numero di tiles nel layer
		dwNumTiles=psl->dwTilesOnScreen;
		//indice corrente del primo tile visibile in questo layer (il primo da sinistra)
		dwCurIndex=psl->dwCurrentTile;
        //punta la mappa (vettore con indici dei tile nel layer corrente)
		piMap=psl->piMap;
		//indice del tile corrente
		iCur=piMap[dwCurIndex];
		//punta il tile
		prcTile=&psl->prcScrollTile[iCur];		
        //x di output del tile (dwClip � la lunghezza che esce dallo schermo sul lato sinistro)
		dwOut=-(LONG)psl->dwClip;
        //top del layer corrente
		dwTop=psl->lTop;

		//esegue un ciclo di rendering disegnando tile da sinistra  destra fino a che non ha coperto tutto
		//il viewport
		while (dwOut<(LONG)m_dwDispW)
		{
			//nota: la larghezza dei tile dovrebbe essere sempre divisibile per 8
			//esegue il blitting sull'immagine di sfondo (imgDest) del tile corrente
			//i tile sono aontenuti tutti nell'immagine m_imgSource carica in memoria
			hr=m_lpFm->PutImgFrameRect(dwOut,dwTop,pimgDest,&m_imgSource,prcTile,dwBltFlags);
	
			dwCurIndex++;
            //prende il tile succesivo
			iCur=piMap[dwCurIndex];
            //rettangolo di grabbing del tile corrente
			prcTile=&psl->prcScrollTile[iCur];
            //incrementa l'ascissa di output di una quantit� pari alla larghezza del tile
			dwOut += dwTileW;			
		}
	}

	//renderizza i layer del background (cielo)
	for (dwCount=m_dwNumScrollBackground-1;dwCount>=0;dwCount--)
	{
		psl=&m_sBackLayer[dwCount];
		
		lxout=-(LONG)psl->dwCurrentX;

		dwTop=psl->lTop;
		
		hr=m_lpFm->PutImgFrameRect(lxout,dwTop,pimgDest,psl->pimgBack,&psl->pimgBack->rcClip,dwBltFlags);
        
		//nota: la larghezza dei tile dovrebbe essere sempre divisibile per 8
		hr=m_lpFm->PutImgFrameRect(lxout+psl->lTileWidth,dwTop,pimgDest,psl->pimgBack,&psl->pimgBack->rcClip,dwBltFlags);
	}
}

//------------------------------------- CSbLevel::GetCurrentTileIndex -------------------------------------
//restituisce l'indice del primo tile visibile per il layer iLayer
int CSbLevel::GetCurrentTileIndex(int iLayer)
{
   if (iLayer>=0 && iLayer<(int)m_dwNumScrollLayers)
   {
		return (int)m_sLayer[iLayer].dwCurrentTile;
   }

   else return 0;
}

//------------------------------------- CSbLevel::SetTile --------------------------------------------
//imposta un tile di un layer
//iTile=codice del tile
//iIndex=indice in cui settare il tile
//iLayer=layer 
int CSbLevel::SetTile(int iTile,int iIndex,int iLayer)
{
	if (iLayer>=0 && iLayer<(int)m_dwNumScrollLayers)
	{
		if (iIndex>=0 && iIndex<(LONG)m_sLayer[iLayer].dwNumMapItems)
		{
			if (iTile>=0 && iTile<=(LONG)m_sLayer[iLayer].dwNumTiles)
			{
				m_sLayer[iLayer].piMap[iIndex]=iTile; //imposta il tile nel vettore
				return 1;
			}
			
			else return 0;
		}

		else return 0;	

	}

	else return 0;
}

//------------------------------------- CSbLevel::GetTile--------------------------------------------------

int CSbLevel::GetTile(int iLayer,int iIndex)
{
	if (iLayer>=0 && iLayer<(int)m_dwNumScrollLayers)
	{
		if (iIndex>=0 && iIndex<(LONG)m_sLayer[iLayer].dwNumMapItems)
		{
			return m_sLayer[iLayer].piMap[iIndex];
		}

	}

	return 0;
}

//------------------------------------- CSbLevel::GetTilesCount -----------------------------------------

//restituisce il numero di tiles in un layer
int CSbLevel::GetTilesCount(int iLayer)
{
	if (iLayer>=0 && iLayer<(int)m_dwNumScrollLayers)
	{
		return m_sLayer[iLayer].dwNumMapItems;
	}

	return 0;
}

//------------------------------------- CsbLevel::SetCurrentX ---------------------------------------------

//Imposta la posizione x dello schema
void CSbLevel::SetPosition(DWORD dwPosX)
{
	DWORD dwCount;
	SCROLL_LAYER_PTR psl;
	DWORD dwD;

	m_lLevelCurrentX=dwPosX;

	//layer 0 (quello piu' vicino)
	psl=&m_sLayer[0];
	psl->dwCurrentX=dwPosX;
	psl->dwCurrentTile=dwPosX / psl->lTileWidth;
	psl->dwClip=dwPosX-(psl->dwCurrentTile) * psl->lTileWidth;


	for (dwCount=1;dwCount<m_dwNumScrollLayers;dwCount++)
	{
		psl=&m_sLayer[dwCount];
		psl->dwCurrentX=(DWORD)(m_sLayer[dwCount-1].dwCurrentX*psl->fParallax); //x corrente
		psl->dwCurrentTile=psl->dwCurrentX/psl->lTileWidth; //tile corrente per questo layer	                
		psl->dwClip=psl->dwCurrentX-(psl->dwCurrentTile) * psl->lTileWidth;	
	}
	
	for (dwCount=0;dwCount<m_dwNumScrollBackground;dwCount++)
	{
		psl=&m_sBackLayer[dwCount];
		dwD=psl->lTileWidth;
		psl->dwCurrentX= (DWORD) (dwPosX * psl->fParallax) % dwD;     

	}

	psl=NULL;
}

//----------------------------------- CSbLevel::SetTriggerPosition ------------------------------

//imposta il puntatore al trigger corrente in base alla posizione dello schema 
//(usata per debug)
void CSbLevel::SetTriggerPosition(DWORD dwPos)
{

	pCurTrigger=&pTriggers[0];

	if (pCurTrigger)
	{
		while (pCurTrigger->xabs<(LONG)dwPos && pCurTrigger!=pTopTrigger)
		{
			pCurTrigger++;
		}

	}
}

//----------------------------------- CSbLevel::GetMaxX -----------------------------------------

//restituisce la larghezza in pixel del livello
LONG CSbLevel::GetMaxX(void)
{
	return m_lMaxX;
}