//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  boss1.h -- boss n�1
///////////////////////////////////////////////////////////////////////*/

#ifndef _BOSS1_INCLUDE_
#define _BOSS1_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "enemy.h"
#include "mech.h"
#include "mecharm.h"
#include "tent.h"

//crea uno stack di inum oggetti di tipo mecharm
//si ocupa anche di caricare le frames
HRESULT CreateBoss1Stack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags=0);
HRESULT LoadBoss1Resources(CResourceManager *prm);
HRESULT FreeBoss1Resources(CADXFrameManager *pfm);
void FreeBoss1Stack(CObjectStack *pstk);

class CBoss1:public CEnemy
{
private:

	CADXFastMath *m_pmath;
	int m_ist1,m_ist2; //stato (general purpose)
	int m_iAttackType;
	bool m_bsteady;
	int m_isteadyfreq;
	
protected:

	CMechComponent *pmc;
	void SetTargetPoint(int xtg,int ytg,int vel,bool steady=false,int freq=0);
	void FollowTarget(void);

public:

	CMechComponent m_Frame; //frame principale alla quale viene attaccato il braccio
	CMechArm *m_pArm; //braccio meccanico
	CTentacle *m_pTentacle; //tentacolo
	HRESULT Draw(void);
	CBoss1(void);
	void Reset(void);
	BOOL Update(void);	
	BOOL DoCollisionWith(CSBObject *pobj);
	HRESULT SetMathModule(CADXFastMath *pmath);
	void Explode(void);	
};

#endif