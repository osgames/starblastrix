//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  lifter.h -- 
///////////////////////////////////////////////////////////////////////*/

#ifndef _LIFTER_INCLUDE_
#define _LIFTER_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "resman.h"
#include "enemy.h"
#include "sbengine.h"
#include "mammoth.h"
#include "mech.h"
#include "mcasmbl.h"
#include "modular.h"


HRESULT LoadLifterRes(CResourceManager* prm);
void FreeLifterRes(void);
HRESULT CreateLifterStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);

//fsm per le due parti laterali
typedef enum
{
	SH_CLOSED=0,
    SH_OPEN,
	SH_CLOSING,
	SH_OPENING,
	SH_STEADY

} SHIELD_STATE;

class CLifter:public CModularEnemy
{
private:	
	
	int step_shield;
	int step_smoke;
	RECT rcLimitBox;
		//indica che c'� un box di limite movimento attivo
	BOOL bBoxActive;	
	int ttl;
	CAnimatedObject *m_pFlames;
	BOOL bGround;
	BOOL m_bDoFlames;
	CObjectStack *m_pNozzleFlame;
	SHIELD_STATE stLeftShield,stRightShield;
	//stato desiderato delle due barriere
	SHIELD_STATE m_prefLeft,m_prefRight;
	int m_closed_left,m_closed_right,m_open_left,m_open_right;
	LONG tmpx,tmpy; //general purpose
	void SetShieldState(int sh,SHIELD_STATE state);
	CMechComponent *pLeftShield,*pRightShield;
	void UpdateShieldPosition(CADXSprite *pShield,int incDispl,int openlimit,int closedlimit,SHIELD_STATE &state);
	void CLifter::ResetBBox();
	


public:
	

	CLifter(void);

	//questo nemico � composto da un corpo centrale, due pistoni laterali e due schermi (barriere) 
	//attaccate ai pistoni
	CModularEnemy m_LeftShield,m_RightShield,m_RightPiston,m_LeftPiston;	
    
	void SetClosedPos(int left,int right);
	void SetOpenPos(int left,int right);
	void SetNozzleFlameStack(CObjectStack *pstack);
	void Explode(void);
	void Collide(void);
	//BOOL DoCollisionWith(CSBObject *pobj);
	void Reset(void);
	BOOL Update(void);	
	HRESULT Draw(void);	

};

#endif