//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////*/

#ifndef _SB_GFIGHTER_
#define _SB_GFIGHTER_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "resman.h"
#include "enemy.h"

#define GF_STATE_NONE 0
//sta eseguendo il rolling intorno all'asse x (rotazione completa su se stesso)
#define GF_STATE_ROLLING_LOOP 1
//sta eseguendo un rolling parziale
#define GF_STATE_ROLLING 2
//sta eseguendo un loop nel piano del video
#define GF_STATE_LOOPING 3
//va a dritto
#define GF_STATE_STRIGHT 4
//stato in cui si deve riportare la traiettoria in stato iniziale
#define GF_STATE_TRANSITION 5
//stato che indica che sta uscendo di scena
#define GF_STATE_LEAVING 6
//moto parabolico (sta perdendo quota e si schianta al suolo)
#define GF_STATE_CRASHING 7

class GFighter:public CEnemy
{
private:

	//indice iniziale del gruppo di frame che visualizzano l'oggetto ruotato nel piano del video
	//qusto gruppo di frames deve contenere 360/15=24 frames 
	int m_iframeset_rot;
	//indice iniziale del gruppo di frame che visualizzano l'oggetto in fase di ribaltamento rispetto all'asse x video
	int m_iframeset_roll;
	//numero di frames che compongono la sequenza di ribaltamento
	int m_inum_roll_frames; 
	CADXFastMath *m_pmath;
	BOOL m_bFollowPath; //flag di stato che � impostato su true se sta seguendo un path e su false se va con l'algo AI
	BOOL m_bResettingTrim; //flag che indica che si sta riportando l'assetto nella configurazione di default (prozzontale)
	int m_istate; //stato algo AI
	int m_irollcnt;
	int m_ittl; //"time to live" ontatore usato per assicurarsi che ad un certo punto l'oggetto esca di scena
	int m_iloopincr;	

protected:


public:

	GFighter(void);
	~GFighter(void);
	void Reset(void);
	HRESULT Create(IMAGE_FRAME_PTR pimgRot,IMAGE_FRAME_PTR pimgRoll,int iRollNum);
	void SetMathModule(CADXFastMath *pmath);
	BOOL Update(void);

};

class CFlashBomb:public CEnemy
{
private:

	CADXFastMath *m_pmath;
	LONG m_iLife;

protected:	

public:

	CFlashBomb(void);
	void Reset(void);
	BOOL Update(void);
	void SetMathModule(CADXFastMath *pmath);

};

HRESULT LoadGFighterRes(CResourceManager *prm);
HRESULT CreateGFighter(GFighter *g,CResourceManager *prm);
HRESULT CreateGFighterStack(CResourceManager *prm,CObjectStack *pstack,DWORD numitems,int iflags);
void FreeGFighterRes(CADXFrameManager *fm);
HRESULT LoadFlashBombRes(CResourceManager *prm);
void FreeFlashBombRes(CADXFrameManager *fm);
HRESULT CreateFlashBombStack(CResourceManager *prm,CObjectStack *pstack,DWORD numitems,int iflags);

#endif