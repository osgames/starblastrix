
/*

    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
   Oggetti nemici linkabili tra loro, tutti ereditano da CModularEnemy

*/


#include "modenemy.h"
#include "a32sprit.h"
#include "a32util.h"
#include "a32fastm.h"
#include "sbengine.h"
#include "a32object.h"

extern CADXFrameManager g_fm;
extern CADXGraphicManager g_gm;
extern CADXFastMath g_fmath;
extern LONG g_snBfire;
extern void PushExplMulti(CADXSprite *psrc,int num_explosions);
extern CADXSound g_cs;
extern void PushExpl2(LONG x,LONG y);
extern CObjectStack g_sEShell11;
extern CObjectStack g_sEShell7;
extern CObjectStack g_sEShell4;
extern CObjectStack g_sNozzleFlames;
extern CObjectStack g_objStack;
//effetti sonori
extern LONG g_snFire11;
extern LONG g_snFire8;
extern LONG g_snFire14;
extern LONG g_snFire1;
extern LONG g_snMech1;
extern LONG g_snMech2;
extern LONG g_snNozzle;
extern CPlayer Player1;

//classi interne a modenemy.cpp
//oggetti nemici linkabili (tutti devono ereditare da CModularEnemy)

//testa del nemico componibile: � quella che imposta la traiettoria di tutto l'oggetto
class CEnemyRoot:public CModularEnemy
{

private:

	int m_iTgPosX[15],m_iTgPosY[15]; //vettore successive posizioni
	int m_iTgCount;

public:

	CEnemyRoot::CEnemyRoot(void)
	{
		dwClass=CL_ENEMY;
		dwType=31;
	}

	void SetEnergy(LONG energy)
	{
		//gli elementi collegati sono meno resistenti
		CModularEnemy::SetEnergy((LONG)(energy*0.67f));
		this->dwEnergy=energy;
	}

	void Reset(void)
	{
		CModularEnemy::Reset();
		Frame.SetPosition(x,y); //la posizione iniziale della frame coincide con quella del'oggetto che la incapsula
		Frame.UpdatePosition();
		Frame.SetVelocity(180,2);
		iUpdateFreq=90;
		iStepConfig=0;

		/*

            valori m_dwFlags:
			0=va sempre dritto senza mai fermarsi
			1=movimento casuale
			2=movimento usando target position

			//quando si usa target pos. il contatore iStepConfig viene incrementato fino a m_iTgCount
			//dopo di che l'oggetto esce di scena con velocit� 180,v
			

		*/		

		//DEBUG
		//m_dwFlags=1;	

		int iprob=g_fmath.RndFast(5);

		switch (m_dwFlags)
		{

		case 2:

			//succesive posizioni da raggiungere
			m_iTgCount=14;

			switch(iprob)
			{
			case 1:

				m_iTgPosX[0]=-520;
				m_iTgPosY[0]=240;
				break;

			case 2:

				m_iTgPosX[0]=-120;
				m_iTgPosY[0]=120;
				break;

			case 3:

				m_iTgPosX[0]=-590;
				m_iTgPosY[0]=30;
				break;

			default:

				m_iTgPosX[0]=-420;
				m_iTgPosY[0]=180;
				break;
			}

			m_iTgPosX[1]=-120; //sta fermo
			m_iTgPosY[1]=190;
			m_iTgPosX[2]=350;
			m_iTgPosY[2]=110;
			m_iTgPosX[3]=80;
			m_iTgPosY[3]=110;
			m_iTgPosX[4]=80;
			m_iTgPosY[4]=220;
			m_iTgPosX[5]=80;
			m_iTgPosY[5]=250;
			m_iTgPosX[5]=500;
			m_iTgPosY[5]=250;
			m_iTgPosX[6]=-1; //indica una posizione casuale
			m_iTgPosY[6]=-1;
			m_iTgPosX[7]=-40;
			m_iTgPosY[7]=35;
			m_iTgPosX[8]=-40;
			m_iTgPosY[8]=35;
			m_iTgPosX[9]=-40;
			m_iTgPosY[9]=234;
			m_iTgPosX[10]=-40;
			m_iTgPosY[10]=254;
			m_iTgPosX[11]=-240;
			m_iTgPosY[11]=254;
			m_iTgPosX[12]=-40+g_fmath.RndFast(300);
			m_iTgPosY[12]=-20+g_fmath.RndFast(100);
			m_iTgPosX[13]=-40+g_fmath.RndFast(300);
			m_iTgPosY[13]=-20+g_fmath.RndFast(100);
			break;

		case 3:

			//succesive posizioni da raggiungere
			m_iTgCount=4;

			//si sposta avanti poi fermo e poi riparte
			m_iTgPosX[0]=10;
			m_iTgPosY[0]=180;
			m_iTgPosX[1]=10;
			m_iTgPosY[1]=180;
			m_iTgPosX[2]=10;
			m_iTgPosY[2]=180;
			m_iTgPosX[3]=10;
    		m_iTgPosY[3]=180;


			break;


		}
	}	
	

	void Explode(void)
	{
		CModularEnemy::Explode();
	};

	BOOL Update(void)
	{
		//nota bene: per evitare l'effetto "sfarfallio" dei moduli collegati
		//si deve prima aggiornare i moduli collegati e poi aggiornare la posizione della root
		BOOL bres=UpdateAI();		
		//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
		Frame.UpdatePosition();
		this->x=Frame.x;
		this->y=Frame.y;		
		return bres;
	}

	BOOL UpdateAI(void)
	{
		BOOL bres=FALSE;

		//esegue l'aggiornamento di tutti gli oggetti collegati
		bres |= CModularEnemy::UpdateAI();
		
		iStepUpdate++;

		if (iStepUpdate>iUpdateFreq)
		{
			iStepUpdate=0;

			int iprob=g_fmath.RndFast(12);
			int ang;

			switch(m_dwFlags)
			{
			case 1:
				
				switch(iprob)
				{
				case 1:

					Frame.SetVelocity(170,1);
					break;

				case 2:

					Frame.SetVelocity(190,3);
					break;

					//fall-through

				case 4:
				case 3:

					if (Frame.x<200)
					{
						Frame.SetVelocity(0,2);
					}
					else Frame.SetVelocity(180,2);

					break;

				case 5:

					Frame.SetVelocity(10,1);
					break;

				case 7:
				case 6:

					if (Frame.y<100)
					{
						Frame.SetVelocity(90,1);
					}
					else Frame.SetVelocity(270,1);
					break;							

				default:

					//sta fermo
					Frame.SetVelocity(0,0);
					break;
				}
				
				break;

				//fall-through
			case 2:
			case 3:

				if (iStepConfig<m_iTgCount)
				{
					if (iStepConfig>0 && (m_iTgPosX[iStepConfig]==m_iTgPosX[iStepConfig-1] && m_iTgPosY[iStepConfig]==m_iTgPosY[iStepConfig-1]))
					{
						//sta fermo se il punto attuale coincide con quello precedente
						Frame.SetVelocity(0,0);
						iUpdateFreq=180;
					}
					else
					{
						if (m_iTgPosX[iStepConfig]==-1)
						{
							//imposta un valore casuale
							m_iTgPosX[iStepConfig]=g_fmath.RndFast(400)-80;
							m_iTgPosY[iStepConfig]=g_fmath.RndFast(200)-20;
						}										

						ang=g_fmath.GetAngle(Frame.x,Frame.y,m_iTgPosX[iStepConfig],m_iTgPosY[iStepConfig]);
						Frame.SetVelocity(ang,2);


						int d1=Frame.x-m_iTgPosX[iStepConfig];
						d1 *= d1; 
						int d2=Frame.y-m_iTgPosY[iStepConfig];
						d2 *= d2;

						//calcola il numero di cicli necessari ad arrivare al punto stabilito
						iUpdateFreq=(int)(sqrt(d1+d2)*0.5);
					}

					if (g_fmath.RndFast(4)==1)
					{
						//si ferma un attimo
						iStepConfig--;
						Frame.SetVelocity(0,0);
						iUpdateFreq=80; 
					}

				}
				else Frame.SetVelocity(180,3); //esce di scena

				iStepConfig++;

				break;

			default:

				//mantiene la traiettoria e la velocit�

				break;
			}							

		}	

		if (Frame.y>CSBObject::g_iScreenHeight-200)  SetVelocity(270,1);
		else if (Frame.y<-80) SetVelocity(90,2);

		if (dwEnergy<=0)
		{
			CreateBonus();
			return FALSE;
		}

		return ((x>-300 && x<CSBObject::g_iScreenWidth+300 && y>-200 && y<CSBObject::g_iScreenHeight+100) || bres);
	}	

};

//un modulo che non fa nulla serve solo come frame di supporto per altri oggetti
class CEnemyModule:public CModularEnemy
{

private:

	//fiamme ugello orizzontali
	CObjectStack *m_pHNozzleFlames;
	CAnimatedObject *m_pHFlames;
	int m_iHFlamesJoint; //giunto al quale collega le fiamme orizzontali
	BOOL m_bDoHFlames;

public:

	CEnemyModule::CEnemyModule(void)
	{
		dwClass=CL_ENEMY;
		m_iHFlamesJoint=0;
		m_pHFlames=NULL;
		m_pHNozzleFlames=NULL;
		m_bDoHFlames=FALSE;
	}

	//imposta lo stck delle fiamme orizzontali e l'hotspot al quale vengono collegate
	void SetHNozzleFlamesStack(CObjectStack *pflames,int hotspot)
	{
		m_iHFlamesJoint=hotspot;
		m_pHNozzleFlames=pflames;
	}

	//nota: questo metodo viene chiamato solo se il modulo � la radice del nemico.
	void Reset(void)
	{
		CModularEnemy::Reset();
		dwPower=10;
		SetVelocity(180,2);
		m_bDoHFlames=FALSE;
		iUpdateFreq=40;

		if (m_pHNozzleFlames)
		{
			m_pHFlames=(CAnimatedObject *)m_pHNozzleFlames->Pop();
			if (m_pHFlames)
			{
				//attacca le fiamme a questo oggetto
				m_pHFlames->pParent=this;
				LONG xj,yj;	
				//acquisisce la posizione relativa dell'ugello
				Frame.GetFrameHotSpot(0,m_iHFlamesJoint,&xj,&yj);
				//imposta la posizione relativa delle fiamme
				m_pHFlames->SetPosition(xj,yj-5);		
			}
		}
	}
	

	BOOL UpdateAI(void)
	{	
		BOOL bres=FALSE;

		//esegue l'aggiornamento di tutti gli oggetti collegati
    	bres |= CModularEnemy::UpdateAI();
	
		if (++iStepUpdate>=iUpdateFreq)
		{
			iStepUpdate=0;
		
			if (m_pHFlames && g_fmath.RndFast(4)==1 && !m_bDoHFlames)
			{
				m_bDoHFlames=TRUE;
				g_cs.PlaySound(g_snNozzle,0,0);
			}

			else m_bDoHFlames=FALSE;
		}

		//non fa nulla (� solo un modulo di supporto per altri oggetti)
		if (dwEnergy<=0) 
		{
			return FALSE;
		}
		
		return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);
	}

	//nota: questo metodo viene chiamato solo se il modulo � la radice del nemico.
	BOOL Update(void)
	{
		//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
		Frame.UpdatePosition();
		this->x=Frame.x;
		this->y=Frame.y;

		if (m_bDoHFlames && m_pHFlames)
		{
			m_pHFlames->Update();
		}

		//algoritmo AI	
		return UpdateAI();
	}

	HRESULT Draw(void)
	{
		CModularEnemy::Draw();
		if (m_bDoHFlames)
		{
			//disegna le fiamme dell'ugello
			m_pHFlames->SetDisplay(m_pimgOut);
			m_pHFlames->Draw();
		}

		return S_OK;
	}

};


//cannone rotante
class CRotCannon:public CModularEnemy
{

private:

	//algoritmo corrente
	int m_icuralgo;

public:

	CRotCannon::CRotCannon(void)
	{
		dwClass=CL_ENEMY;
		m_icuralgo=0;
	}

	void Reset(void)
	{
		CModularEnemy::Reset();
		iUpdateFreq=12;
	}


	BOOL UpdateAI(void)
	{
		if (dwEnergy<0) return FALSE;

		BOOL bres=FALSE;

		//esegue l'aggiornamento di tutti gli oggetti collegati
    	bres |= CModularEnemy::UpdateAI();

		iStepUpdate++;

		if (iStepUpdate>=iUpdateFreq)
		{
			int angle_target=g_fmath.GetAngle(Frame.x,Frame.y,lTargetX,lTargetY);
			int l=Frame.GetLambda();
			int diff;
			
			iStepConfig++;
			iStepUpdate=0;

			if (iStepConfig>=9)
			{
				//cambia algoritmo AI
				iStepConfig=0;	
				iStepUpdate=12;
				m_icuralgo=g_fmath.RndFast(6);			

				switch(m_icuralgo)
				{
				case 1:
					iFireFreq=12; //aumenta la frequenza di fuoco
					iStepUpdate=8;
                    break;
				case 2:
					iFireFreq=40;
					break;
                case 3:
					iFireFreq=g_fmath.RndFast(50)+50; //non spara quasi piu'
					break;
				case 4:
					iFireFreq=g_fmath.RndFast(20)+10;
					iStepUpdate=19;
				default:
					iFireFreq=20;
					break;
				}               

			}

			switch (m_icuralgo)
			{


			case 1:
            case 4:

				//ruota su se stesso
				Frame.SetLambda(Frame.GetLambda()+15);
			
				break;

			case 2:

				//resta fermo
				break;

			default:

				//usa una tolleranza di 30� gradi per puntare il player
				diff=g_fmath.GetAngleDiff(l,angle_target,30);
				
				l+=diff*15;

				iStepUpdate=0;					
						
				Frame.SetLambda(l);

			break;
			}//fine switch
			
		}


		iStepFire++;

		//fuoco
		if (iStepFire>=iFireFreq)
		{
			iStepFire=0;

			if (m_Cartridges)
			{

				CSBObject *pShell;
				LONG xf,yf;
				
				int lambda;

				pShell=(CSBObject *)m_Cartridges->Pop();
				
				if (pShell)
				{
					pShell->dwClass=CL_ENFIRE;
					g_cs.PlaySound(g_snBfire,0,0);	
					//acquisisce la bocca del cannone
					Frame.GetJointPosition(&xf,&yf,1);						
					lambda=Frame.GetCurFrame();
					pShell->SetCurFrame(lambda);						
					pShell->SetJointPosition(xf,yf,0);
					pShell->SetVelocity(lambda*15,10),
					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=12;
					m_ActiveStack->Push(pShell);

				}

			}

		}
		
		return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);
	}

	HRESULT Draw(void)
	{
		Frame.SetDisplay(m_pimgOut);
		Frame.RenderToDisplay();
		return S_OK;
	}

	BOOL Update(void)
	{
		//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
		Frame.UpdatePosition();
		this->x=Frame.x;
		this->y=Frame.y;
		//algoritmo AI	
		return UpdateAI();
	}
};

class CTurret:public CModularEnemy
{
private:

	CObjectStack *m_pWeapons[4]; //la torretta ha 4 cannoni diversi
	bool m_bBusy;
	int m_iBurst;

public:

	CMechComponent m_Turret;

	CTurret(void)
	{
		dwClass=CL_ENEMY;
		dwPower=15;
		for (int cnt=0;cnt<4;cnt++) m_pWeapons[cnt]=NULL;
	}

	~CTurret(void)
	{

	}

	void Reset(void)
	{
		CModularEnemy::Reset();
		m_Turret.SetCurFrame(0);
		m_Turret.m_iTag1=70;				
		iStepConfig=1; //incrementa la frame				
		iUpdateFreq=20;	
		m_bBusy=false;
		m_iBurst=0;
	}

	void SetWeapon(int index,CObjectStack *pWeapon)
	{
		if (index>=0 && index<4) m_pWeapons[index]=pWeapon;
	}

	//fa sparare il cannone con indice iCannon
	//cannon va da 1 a 4
	void FireCannon(int iCannon,int angle)
	{	
		CSBObject *pShell=NULL;
		int xp,yp;

		//nota: lo stck attivo deve essere impostato dalla funzione che crea gli oggetti ausiliari
		if (!m_ActiveStack) return;

		switch(iCannon)
		{
		case 1:
			
			//proiettile sferico
			
			if (m_pWeapons[0])
			{
				pShell=m_pWeapons[0]->Pop();

				if (pShell)
				{
					//cannone 1 orizzontale
					if (angle==180)	m_Turret.GetCouplingPos(1,&xp,&yp);
					else m_Turret.GetCouplingPos(6,&xp,&yp); //verticale
					
					//trasforma da coord. relative del giunto in assolute
					xp+=m_Turret.x;
					yp+=m_Turret.y;

					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=10;
					pShell->SetCurFrame(0); //� un p. sferico
					pShell->SetPosition(xp-6,yp-5);
					pShell->SetVelocity(angle,6);
					g_cs.PlaySound(g_snFire8,0,0);
					m_ActiveStack->Push(pShell);
				}

			}

			break;

		case 2:

			//p. laser
			if (m_pWeapons[1])
			{
				pShell=m_pWeapons[1]->Pop();

				if (pShell)
				{
					//cannone 2 orizzontale
					if (angle==180)	m_Turret.GetCouplingPos(5,&xp,&yp);
					else 
					{
						m_Turret.GetCouplingPos(2,&xp,&yp); //verticale
					}

					//trasforma da coord. relative del giunto in assolute
					xp+=m_Turret.x;
					yp+=m_Turret.y;
					
					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=10;
					pShell->SetCurFrame(angle==180 ? 0 : 6); 
					pShell->SetPosition(xp,yp);
					pShell->SetVelocity(angle,8);
					g_cs.PlaySound(g_snFire11,0,0);
					m_ActiveStack->Push(pShell);
				}

			}

			break;

		case 3:

			//proiettile rosso lungo
			
			if (m_pWeapons[2])
			{
				pShell=m_pWeapons[2]->Pop();

				if (pShell)
				{
					//cannone 3 orizzontale
					if (angle==180)	m_Turret.GetCouplingPos(3,&xp,&yp);
					else m_Turret.GetCouplingPos(3,&xp,&yp); //Nota: cannone tre verticale inesistente 
					
					//trasforma da coord. relative del giunto in assolute
					xp+=m_Turret.x;
					yp+=m_Turret.y;

					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=10;
					pShell->SetCurFrame(12); 
					pShell->SetPosition(xp-8,yp-2);
					pShell->SetVelocity(angle,15);
					g_cs.PlaySound(g_snFire11,0,0);
					m_ActiveStack->Push(pShell);
				}

			}

			break;

		case 4: 

			//verticale (p. sferico)
			if (m_pWeapons[3])
			{
				pShell=m_pWeapons[3]->Pop();

				if (pShell)
				{
					//cannone 4 verticale
					if (angle==180)	m_Turret.GetCouplingPos(4,&xp,&yp);
					else m_Turret.GetCouplingPos(4,&xp,&yp); 
					
					//trasforma da coord. relative del giunto in assolute
					xp+=m_Turret.x;
					yp+=m_Turret.y;

					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=10;
					pShell->SetCurFrame(0); 
					pShell->SetPosition(xp-3,yp-2);
					pShell->SetVelocity(angle,8);
					g_cs.PlaySound(g_snFire8,0,0);
					m_ActiveStack->Push(pShell);
				}

			}



		}
	}

	BOOL UpdateAI(void)
	{
		BOOL bres=FALSE;

		iStepUpdate++;

		//esegue l'aggiornamento di tutti gli oggetti collegati
    	bres |= CModularEnemy::UpdateAI();

		if (iStepUpdate>=iUpdateFreq)
		{
			iStepUpdate=0;

			if (m_Turret.m_iTag1>0)
			{
				int iprob=g_fmath.RndFast(8);

				if (!m_bBusy)
				{

					if (iStepConfig==1)
					{
						if (FAILED(m_Turret.IncFrame())) iStepConfig=-1;						
					}
					else if (iStepConfig==-1)
					{
						if (FAILED(m_Turret.DecFrame())) iStepConfig=1;					
					}										

					int cf=m_Turret.GetCurFrame();

					m_bBusy = (cf==0 || cf==6 || cf==12);
					
					if (m_bBusy) 
					{
						if (g_fmath.RndFast(6)==1) m_bBusy=false; //in questo caso non si ferma a sparare
					}
				}
			

			}
		}

	
		if (m_bBusy)
		{		

			//la torretta puo' sparare solo quando si trova in una 
			//delle 3 posizioni base in cui i cannoni sono allineati
			switch(m_Turret.GetCurFrame())
			{
			case 0:

				//cannoni 1 orizzontale e 2 verticale 
				if (m_iBurst<=0)
				{
					m_bBusy=false;
					m_iBurst=5;
					iFireFreq=g_fmath.RndFast(8)+20; 
					iStepFire=0;
					g_cs.PlaySound(g_snMech1,0,0);

				}
				else 
				{					
					m_bBusy=true; //resta fermo in questa posizione
				
					if (++iStepFire>=iFireFreq)
					{
						iStepFire=0;
						m_iBurst--;
						FireCannon(1,180); //cannone 1 orizz.	
						FireCannon(2,270); //cannone 2 vert.
					}
				}
				
				break;
				
				
			case 6:

				//cannone 3 orizz. e 4 vert.			
				if (m_iBurst<=0)
				{
					m_bBusy=false;
					m_iBurst=5;
					iFireFreq=g_fmath.RndFast(16)+10; 
					iStepFire=0;
					g_cs.PlaySound(g_snMech1,0,0);
				}
				else 
				{					
					m_bBusy=true; //resta fermo in questa posizione
					

					if (++iStepFire>=iFireFreq)
					{
						iStepFire=0;
						m_iBurst--;
						FireCannon(3,180); 
						FireCannon(4,270);
					
					}
				}								


				break;


			case 12:

				//cannone 2 orizzontale e 1 verticale
				//cannoni 1 orizzontale e 2 verticale 
				if (m_iBurst<=0)
				{
					m_bBusy=false;
					m_iBurst=g_fmath.RndFast(8)+6;
					iFireFreq=19; 
					iStepFire=0;
					g_cs.PlaySound(g_snMech1,0,0);

				}
				else 
				{					
					m_bBusy=true; //resta fermo in questa posizione
				
					if (++iStepFire>=iFireFreq)
					{
						iStepFire=0;
						m_iBurst--;
						FireCannon(1,270); 
						FireCannon(2,180); 
					}
				}								


				break;					
		
				
			}//fine switch cur frame

		}
	

		return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);
	}
	

	BOOL Update(void)
	{
		//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
		Frame.UpdatePosition();
		this->x=Frame.x;
		this->y=Frame.y;

		//algoritmo AI	
		return UpdateAI();
	}

	BOOL DoCollisionWith(CSBObject *pobj)
	{
		BOOL bres=FALSE;

		bres |= CModularEnemy::DoCollisionWith(pobj);

		if (m_Turret.m_iTag1>0)
		{

			if (m_Turret.DetectCollision((CADXSprite *)pobj))
			{
				DamageComponent(&m_Turret,pobj);			
			}

		}

		return bres;	

	}

	HRESULT Draw(void)
	{	
		if (Frame.m_iTag1>0)
		{
			Frame.SetDisplay(m_pimgOut);
			Frame.RenderToDisplay();
		}
		if (m_Turret.m_iTag1>0)
		{		
			m_Turret.SetDisplay(m_pimgOut);
			m_Turret.RenderToDisplay();
		}
		return S_OK;
	}

};

//Cannone che spara una rosa di proiettili circolari

class CCircularCannon:public CModularEnemy
{
private:

	int xj,yj;	
	int m_iCurFireAng;

	//spara una palla inseguitrice
	void FireFwBullet(int angle,int vel)
	{
		//spara due palle insegutrici (un in alto l'altra in basso)
		CFwBullet *p=(CFwBullet *)m_Cartridges->Pop();

		if (p)
		{
			p->Reset();
			p->SetPosition(xj,yj);
			p->SetVelocity(angle,vel);
			p->SetTarget(&Player1);	
			p->SetTTL(90); //time to live
			p->dwClass=CL_ENEMY;
			p->SetEnergy(1);
			p->dwPower=10;
			m_ActiveStack->Push(p);

		}
	}

	//spara un proiettile sferico
	void FireShell(int angle,int vel)
	{
		CSBObject *p=m_pWeapon1->Pop();

		if (p)
		{
			p->Reset();
			p->SetPosition(xj,yj);
			p->SetVelocity(angle,vel);
			p->dwClass=CL_ENFIRE;
			p->SetEnergy(3);
			p->dwPower=7;
			m_ActiveStack->Push(p);

		}
	}
	
public:

	CCircularCannon(void)
	{
	}

	void Reset(void)
	{
		Frame.SetAnimRange(0,3);
		CModularEnemy::Reset();
		iFireFreq=100;
		iStepWeapon1=0; //tipo di arma
		m_iCurFireAng=0;
		iUpdateFreq=210;
	}	

	BOOL UpdateAI(void)
	{
		BOOL bres=FALSE;

		bres |= CModularEnemy::UpdateAI();

		if (++iStepUpdate>=iUpdateFreq)
		{
			//se � in corso la spirale non deve aggiornare
			if (!(iStepWeapon1==5 || iStepWeapon1==6 || iStepWeapon1==7))
			{		
			
				iStepWeapon1=g_fmath.RndFast(12);
									

				switch(iStepWeapon1)
				{
					//fall-through
				case 1:
				case 2:


					iFireFreq=g_fmath.RndFast(200)+100;
					break;

				case 7:	
				case 5:

					//spirale
					m_iCurFireAng=0;
					iFireFreq=10;
					break;

				case 6:

					m_iCurFireAng=0;
					iFireFreq=30;

					break;

				default:

					iFireFreq=140;
					break;
				}

			}

			iStepUpdate=0;			
		}	
		
		if (iStepFire==iFireFreq-30 && iStepWeapon1<8)
		{
			//inizio lampeggio prima di sparare
			Frame.SetAnimSpeed(0.2f);
		}
	 
		if (++iStepFire>=iFireFreq)
		{
			//acquisisce la posizione della bocca del cannone
			Frame.GetCouplingPos(0,&xj,&yj);

			xj+=x;
			yj+=y;

			iStepFire=0;

			int i;

			if (iStepWeapon1>3)
			{
				//proiettile sferico
				if (m_pWeapon1)
				{
					switch(iStepWeapon1)
					{
					case 4:

						g_cs.PlaySound(g_snFire14,0,0);
						//spara una rosa di proiettili
						for (i=0;i<360;i+=15)
						{
							FireShell(i,5);
						}
						break;

						//fall-through
					case 7:
					case 5:

						//spara una spirale
						FireShell(m_iCurFireAng,5);
						m_iCurFireAng+=15;
						g_cs.PlaySound(g_snFire1,0,0);

						if (m_iCurFireAng>360)
						{
							//aggiorna
							m_iCurFireAng=0;
							iStepWeapon1=0;
							iStepUpdate=iUpdateFreq;
						}
						break;

					case 6:

						g_cs.PlaySound(g_snFire1,0,0);

						//spara 4 proiettili
						FireShell(m_iCurFireAng,5);
						FireShell(m_iCurFireAng+90,5);
						FireShell(m_iCurFireAng+180,5);
						FireShell(m_iCurFireAng+270,5);
						m_iCurFireAng+=15;
						if (m_iCurFireAng>90)
						{
							//aggiorna
							m_iCurFireAng=0;
							iStepWeapon1=0;
							iStepUpdate=iUpdateFreq;
						}
						break;

					}
				}

			
			}

			else
			{
				//palle inseguitrici					
				if (m_Cartridges)
				{					

					FireFwBullet(270,3);
					FireFwBullet(90,3);				

					if (iStepWeapon1>=1)
					{
						//4 palle
						FireFwBullet(0,3);
						FireFwBullet(180,3);

						if (iStepWeapon1>=2)
						{
							//6 palle
							FireFwBullet(45,5);
							FireFwBullet(225,5);

						}
					}

				}

			} //fine iStepWeapon1

			//fine lampeggio
			Frame.SetAnimSpeed(0.0f); 
			Frame.SetCurFrame(0);

		}

		return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);

	}

	BOOL Update(void)
	{
		Frame.UpdatePosition();
		Frame.UpdateFrame();
		this->x=Frame.x;
		this->y=Frame.y;
		//algoritmo AI	
		return UpdateAI();
	}
};

//cannone fisso,spara sempre nella stessa direzione
class CStaticCannon:public CModularEnemy
{
private:

	int m_iFireAngle; //angolo di fuoco
	int m_iShellSpeed; //velocit� proiettile
	int m_iFireCoupling; //hot spot della bocca del cannone
	LONG m_lFireSound; //rumore sparo
	int m_xf,m_yf; //coordinate relative bocca di fuoco

public:

	CStaticCannon(void)
	{
		dwClass=CL_ENEMY;
		m_iFireAngle=m_iShellSpeed=m_iFireCoupling=0;		
		m_lFireSound=0;
	}

	void Reset(void)
	{
		CModularEnemy::Reset();
		iUpdateFreq=100;
		iFireFreq=90;
		Frame.GetCouplingPos(m_iFireCoupling,&m_xf,&m_yf);
		Frame.m_iTag1=40;
		dwPower=40;
		iConfigFreq=0;
		iStepConfig=0;
		SetEnergy(25);
		
	}

	void SetCannon(int angle,int shell_speed,int coupling,LONG sound)
	{
		m_iFireAngle=angle;
		m_iShellSpeed=shell_speed;
        m_iFireCoupling=coupling;
		m_lFireSound=sound;
	}

	BOOL UpdateAI(void)
	{
		BOOL bres=FALSE;

		bres |= CModularEnemy::UpdateAI();

		if (++iStepUpdate>=iUpdateFreq)
		{
			iStepUpdate=0;
		}

		//ciclo di fuoco
		if (++iStepFire>=iFireFreq)
		{
			iStepFire=0;

			if (m_Cartridges)
			{

				if (iStepConfig<=0)
				{
					//imposta il tipo di raffica
					int iprob=g_fmath.RndFast(8);				

					switch(iprob)
					{
					case 0:

						iFireFreq=35;
						iStepConfig=3;					
						break;

					case 1:

						if (abs(lTargetY-m_yf-y)<40)
						{
							iFireFreq=8;
							iStepConfig=8;							

						}
						else
						{
							iFireFreq=30;
							iStepConfig=10;

						}

						break;

					case 2:

						iFireFreq=280;
						iStepConfig=2;
						break;

					case 3:

						iFireFreq=10;
						iStepConfig=20;
						break;						

					default:

						iFireFreq=90;
						iStepConfig=0;
						break;

					}

				}

				else iStepConfig--;

				//estrae un proiettile				
				CSBObject *p=m_Cartridges->Pop();

				if (p && m_ActiveStack)
				{
					p->Reset();
					p->dwClass=CL_ENFIRE;
					p->SetEnergy(2);
					p->dwPower=8;
					p->SetCurFrame(0); //frame base
					p->SetPosition(m_xf+x,m_yf+y);
					p->SetVelocity(m_iFireAngle,m_iShellSpeed);
					if (m_lFireSound) g_cs.PlaySound(m_lFireSound,0,0);
					m_ActiveStack->Push(p);
				
				}

			}

		}

		if (dwEnergy<=0) 
		{
			return FALSE;
		}
		
		return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);

	}

	BOOL Update(void)
	{
		Frame.UpdatePosition();
		this->x=Frame.x;
		this->y=Frame.y;

		return UpdateAI();
	}

};

CObjectStack m_ModEnemies[MOD_ENEMY_MAX];
IMAGE_FRAME m_imgMod[MOD_ENEMY_MAX];
IMAGE_FRAME m_imgRotCannon3[24];
IMAGE_FRAME m_imgTurret[13]; //torretta rotante
IMAGE_FRAME m_imgCircularCannon[2]; 
BOUND_POLY m_bpMod[MOD_ENEMY_MAX];
DWORD m_iStackTop[MOD_ENEMY_MAX];


HRESULT LoadRes(CResourceManager *prm)
{
	if (FAILED(prm->LoadAuxResFile("data\\data5.vpx",0))) return E_FAIL;
	return S_OK;
}

//restituisce un componente nemico linkabile tramite un resource manager e un ID
CModularEnemy *GetModEnemy(CResourceManager *prm,MOD_ENEMY_ID id)
{

	if (!prm) return NULL;

	if (m_ModEnemies[id].ItemCount()>0) 
	{
		//il vettore StackTop serve a fa rin modo di non restituire oggetti gi� in uso
		if (m_iStackTop[id]<m_ModEnemies[id].ItemCount())
		{
			return (CModularEnemy *)m_ModEnemies[id].m_objStack[m_iStackTop[id]++];
		}

		else return NULL; //non ci sono piu' oggetti disponibili
	}
	
	//crea lo stack 
	m_iStackTop[id]=0;

	//carica le risorse
	if (FAILED(LoadRes(prm))) return NULL; 
    //immagine corrente caricata
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (!pimg) return NULL;

	CObjectStack *pstk=&m_ModEnemies[id];
	CModularEnemy *pen;
	CEnemyModule *pmod;
	CStaticCannon *psc;
	CTurret *pt;
	BOUND_POLY_PTR pb;
	IMAGE_FRAME imgTemp;
	int numel;
	int count=0;
	int count1;
	
	switch(id)
	{
	case MOD_ENEMY_ROOT:

		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],150,477,308,575,pimg))) return NULL;

		m_bpMod[id].poly=new VERTEX2D[21];
		m_bpMod[id].inum=0;

		pb=&m_bpMod[id];
		AddVertex(pb,0,46);AddVertex(pb,37,31);AddVertex(pb,54,12);AddVertex(pb,54,3);
		AddVertex(pb,77,3);AddVertex(pb,83,0);AddVertex(pb,115,0);AddVertex(pb,120,4);
		AddVertex(pb,141,4);AddVertex(pb,143,12);AddVertex(pb,156,12);AddVertex(pb,156,82);
		AddVertex(pb,122,87);AddVertex(pb,113,98);AddVertex(pb,80,97);AddVertex(pb,75,86);
		AddVertex(pb,54,86);AddVertex(pb,52,81);AddVertex(pb,35,75);AddVertex(pb,18,77);
		AddVertex(pb,0,80);

		numel=12;				
		
		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyRoot();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);
			pen->Frame.SetBoundPoly(0,&m_bpMod[id]);
			pen->SetCurFrame(0);
			pen->Frame.AddFixCoupling(157,47); //accoppiamento di coda		
			pen->Frame.AddFixCoupling(0,67); //accoppiamento di testa
			pen->dwScore=4500;
			pen->SetActiveStack(&g_objStack);
			pen->lpfnMultiExplosion=PushExplMulti;
		}		
		
		break;

	case MOD_MODULE1:

		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],85,378,188,469,pimg))) return NULL;

		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=10;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);
			//un giunto in testa uno in coda
			pen->Frame.AddFixCoupling(0,44); 
			pen->Frame.AddFixCoupling(102,44);
			pen->dwScore=1000;
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->lpfnExplode=PushExpl2;
		}		

		break;

	case MOD_MODULE2:

		//modulo a 4 giunti fissi piu' uno rotante

		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],201,372,309,473,pimg))) return NULL;

		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=10;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(0,53);  //giunto di testa (0)
			pen->Frame.AddFixCoupling(108,53); //giunto di coda (1)
			pen->Frame.AddFixCoupling(45,0); //giunto in alto (2)
			pen->Frame.AddFixCoupling(45,100); //giunto in basso (3)
			pen->Frame.AddRotCoupling(46,53,0,360,1); //giunto rotante (4)
			pen->Frame.AddFixCoupling(46,53); //giunto fisso centrale (5)
			pen->dwScore=1000;
			pen->lpfnExplode=PushExpl2;
			pen->lpfnMultiExplosion=PushExplMulti;
			
		}			

		break;

	case MOD_MODULE3:

		//modulo grande a 5 giunti fissi
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],162,142,305,243,pimg))) return NULL;

		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=15;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(0,50);  //giunto di testa (0)
			pen->Frame.AddFixCoupling(142,50); //giunto di coda (1)
			pen->Frame.AddFixCoupling(75,0); //giunto in alto (2)
			pen->Frame.AddFixCoupling(75,100); //giunto in basso (3)	
			pen->Frame.AddFixCoupling(75,50); //giunto fisso centrale (4)
			pen->dwScore=1000;
			pen->lpfnExplode=PushExpl2;
			pen->lpfnMultiExplosion=PushExplMulti;
		}	

		break;

	case MOD_NOZZLE:

		//ugello
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],319,381,403,460,pimg))) return NULL;

		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=10;

		pstk->Create(numel);		
				
		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(0,40);  //giunto di testa (0)
			pen->Frame.AddFixCoupling(83,32); //hot spot fiamme ugello (1)
			pen->dwScore=1000;
			pen->lpfnMultiExplosion=PushExplMulti;		
			pmod=(CEnemyModule *)pen;
			pen->lpfnExplode=PushExpl2;
			pmod->SetHNozzleFlamesStack(&g_sNozzleFlames,1);

		}

		break;

	case MOD_SLIDE:



		break;

	case MOD_ROT_CANNON:

		//cannone rotante
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],90,148,157,229,pimg))) return NULL;

		//la frame iniziale � a 90�
		//crea l prima frame
		if (FAILED(g_fm.CreateRotFrame(90,&m_imgRotCannon3[0],&m_imgMod[id]))) return NULL;

		//crea le frames ruotate
		for (count=1;count<24;count++)
		{
			if (FAILED(g_fm.CreateRotFrame(count*15,&m_imgRotCannon3[count],&m_imgRotCannon3[0]))) return NULL;
		}
	
		m_bpMod[id].poly=NULL;	
		m_bpMod[id].inum=0;

		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CRotCannon();
			pen=(CRotCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,360,m_imgRotCannon3,24);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddRotCoupling(33,30,0,360,1);  //rotante centrale
			pen->Frame.AddFixCoupling(80,30); //bocca del cannone
			pen->dwScore=1000;
			pen->lpfnExplode=PushExpl2;
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->SetActiveStack(&g_objStack);
		}		

		break;
		
	case MOD_CANNON1:

		//cannone fisso centrale
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],120,0,280,68,pimg))) return NULL;
		
		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;
	
		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CStaticCannon();
			pen=(CRotCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(129,32); //base
			pen->Frame.AddFixCoupling(0,33); //bocca del cannone
			pen->dwScore=100;
			pen->lpfnMultiExplosion=PushExplMulti;
			psc=(CStaticCannon *)pen;
			psc->SetCannon(180,10,1,g_snFire8);
			psc->SetCartridgeStack(&g_sEShell11);
			psc->SetActiveStack(&g_objStack);
			pen->lpfnExplode=PushExpl2;
		}	

		
		break;

	case MOD_CANNON2:

		//cannone rivolto indietro
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],237,252,384,307,pimg))) return NULL;
		
		m_bpMod[id].poly=new VERTEX2D[12];
		m_bpMod[id].inum=0;

		//usa per tutti il poligono del n� 4 poi lo trasforma
		pb=&m_bpMod[id];
		AddVertex(pb,0,13);AddVertex(pb,62,13);AddVertex(pb,62,0);AddVertex(pb,134,0);
		AddVertex(pb,135,26);AddVertex(pb,44,35);AddVertex(pb,135,41);AddVertex(pb,135,52);
		AddVertex(pb,97,52);AddVertex(pb,62,40);AddVertex(pb,62,33);AddVertex(pb,0,33);

		MirrorPolyH(pb,pb,&m_imgMod[id]);	
		MirrorPolyV(pb,pb,&m_imgMod[id]);	

		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CStaticCannon();
			pen=(CRotCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(46,54); //base
			pen->Frame.AddFixCoupling(145,30); //bocca del cannone
			pen->dwScore=100;
			pen->lpfnMultiExplosion=PushExplMulti;
			psc=(CStaticCannon *)pen;
			psc->SetCannon(0,10,1,g_snFire8);
			psc->SetCartridgeStack(&g_sEShell11);
			psc->SetActiveStack(&g_objStack);
			pen->lpfnExplode=PushExpl2;
		}	

		break;

	case MOD_CANNON3:

		//cannone a testa in su'
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],89,252,235,307,pimg))) return NULL;
		
		m_bpMod[id].poly=new VERTEX2D[12];
		m_bpMod[id].inum=0;

		//usa per tutti il poligono del n� 4 poi lo trasforma
		pb=&m_bpMod[id];
		AddVertex(pb,0,13);AddVertex(pb,62,13);AddVertex(pb,62,0);AddVertex(pb,134,0);
		AddVertex(pb,135,26);AddVertex(pb,44,35);AddVertex(pb,135,41);AddVertex(pb,135,52);
		AddVertex(pb,97,52);AddVertex(pb,62,40);AddVertex(pb,62,33);AddVertex(pb,0,33);

		MirrorPolyV(pb,pb,&m_imgMod[id]);		

		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CStaticCannon();
			pen=(CRotCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(103,54); //base
			pen->Frame.AddFixCoupling(0,30); //bocca del cannone
			pen->dwScore=100;
			pen->lpfnMultiExplosion=PushExplMulti;
			psc=(CStaticCannon *)pen;
			psc->SetCannon(180,10,1,g_snFire8);
			psc->SetCartridgeStack(&g_sEShell11);
			psc->SetActiveStack(&g_objStack);
			pen->lpfnExplode=PushExpl2;
		}	

		break;

	case MOD_CANNON4:

		//cannone a testa in gi�
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],89,313,235,366,pimg))) return NULL;
		
		m_bpMod[id].poly=new VERTEX2D[12];
		m_bpMod[id].inum=0;

		pb=&m_bpMod[id];
		AddVertex(pb,0,13);AddVertex(pb,62,13);AddVertex(pb,62,0);AddVertex(pb,134,0);
		AddVertex(pb,135,26);AddVertex(pb,44,35);AddVertex(pb,135,41);AddVertex(pb,135,52);
		AddVertex(pb,97,52);AddVertex(pb,62,40);AddVertex(pb,62,33);AddVertex(pb,0,33);
		
		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CStaticCannon();
			pen=(CRotCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(103,0); //base
			pen->Frame.AddFixCoupling(0,22); //bocca del cannone
			pen->dwScore=100;
			pen->lpfnMultiExplosion=PushExplMulti;
			psc=(CStaticCannon *)pen;
			psc->SetCannon(180,10,1,g_snFire8);
			psc->SetCartridgeStack(&g_sEShell11);
			psc->SetActiveStack(&g_objStack);
			pen->lpfnExplode=PushExpl2;
		}

		break;

		//cannone circolare
	case MOD_CANNON5:

		//frame base
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],420,0,420+70,70,pimg))) return NULL;
		//frame per effetto flash 
		if (FAILED(g_fm.GrabVFrameArray(m_imgCircularCannon,pimg,2,420,63,70,63))) return NULL;
		
		m_bpMod[id].poly=NULL;	
		m_bpMod[id].inum=0;

		numel=8;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CCircularCannon();
			pen=(CCircularCannon *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->Frame.AddFrame(&m_imgCircularCannon[0]); //frame con luce centrale: servono per effetto flash
			pen->Frame.AddFrame(&m_imgCircularCannon[1]);
			pen->SetInitialEnergy(20);			
		    pen->Frame.AddFixCoupling(35,31);  //centro			
			pen->dwScore=1000;
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->SetActiveStack(&g_objStack);
			pen->lpfnExplode=PushExpl2;
		}		
		
		break;

	case MOD_BELL:


		break;

	case MOD_COCKPIT:

		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],328,479,494,556,pimg))) return NULL;		

		m_bpMod[id].poly=new VERTEX2D[17];
		m_bpMod[id].inum=0;

		pb=&m_bpMod[id];
		AddVertex(pb,0,36);AddVertex(pb,19,36);AddVertex(pb,36,26);AddVertex(pb,52,20);
		AddVertex(pb,78,13);AddVertex(pb,119,6);AddVertex(pb,139,6);AddVertex(pb,147,9);
		AddVertex(pb,153,0);AddVertex(pb,165,0);AddVertex(pb,164,43);AddVertex(pb,142,61);
		AddVertex(pb,85,61);AddVertex(pb,85,75);AddVertex(pb,21,75);
		AddVertex(pb,21,61);AddVertex(pb,0,61);

		numel=4;

		pstk->Create(numel);		
				
		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(52,57);  //giunto in basso
			pen->Frame.SetBoundPoly(0,&m_bpMod[id]);
			pen->dwScore=1000;			
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->lpfnExplode=PushExpl2;
		}

		break;

	case MOD_HFRAME:

		//traliccio orizzontale
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],120,74,292,142,pimg))) return NULL;		
        
		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=10;

		pstk->Create(numel);		
				
		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(0,33);  //giunto di testa
            pen->Frame.AddFixCoupling(170,33); //giunto di coda
			pen->Frame.SetBoundPoly(0,&m_bpMod[id]);
			pen->dwScore=1000;			
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->lpfnExplode=PushExpl2;
		}

		break;

	case MOD_VFRAME:

		//traliccio verticale
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],309,0,375,118,pimg))) return NULL;		
        
		m_bpMod[id].poly=NULL;
		m_bpMod[id].inum=0;

		numel=8;

		pstk->Create(numel);		
				
		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CEnemyModule();
			pen=(CModularEnemy *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);		
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(33,0);  //giunto in alto
            pen->Frame.AddFixCoupling(33,118); //giunto in basso
			pen->Frame.SetBoundPoly(0,&m_bpMod[id]);
			pen->dwScore=1000;			
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->lpfnExplode=PushExpl2;
		}

		break;

	case MOD_TURRET:
		
		//torretta rotante a 45�		
		if (FAILED(g_fm.GrabFrame(&m_imgMod[id],11,4,97,72,pimg))) return NULL;

		//immagine torretta
		if (FAILED(g_fm.CreateImgFrameFromVPX(&imgTemp,TEXT("data\\data5.vpx"),2))) return NULL;

		g_fm.GrabFrameArray(m_imgTurret,&imgTemp,6,0,0,130,130);
		g_fm.GrabFrameArray(&m_imgTurret[6],&imgTemp,6,0,130,130,130);
		g_fm.GrabFrame(&m_imgTurret[12],0,130*2,130,130*3,&imgTemp);

		g_fm.FreeImgFrame(&imgTemp);	

		//poligono di contenimento usato per le collisioni
		m_bpMod[id].poly=new VERTEX2D[15];
		pb=&m_bpMod[id];
		AddVertex(pb,0,88);AddVertex(pb,47,88);AddVertex(pb,80,56);AddVertex(pb,95,56);
		AddVertex(pb,89,23);AddVertex(pb,106,23);AddVertex(pb,108,57);AddVertex(pb,128,59);
		AddVertex(pb,112,81);AddVertex(pb,119,90);AddVertex(pb,88,120);AddVertex(pb,81,114);
		AddVertex(pb,59,129);AddVertex(pb,56,105);AddVertex(pb,0,106);
		
		numel=12;

		pstk->Create(numel);

		for (count=0;count<numel;count++)
		{
			pstk->m_objStack[count]=new CTurret();
			pen=(CTurret *)pstk->m_objStack[count];
			pen->bActive=FALSE;
			pen->SetFrameManager(&g_fm);			
			pen->Frame.SetFrameManager(&g_fm);
			//l'oggetto trasla senza ruotare
			pen->Frame.CreateComponent(0,0,&m_imgMod[id],1);				
			pen->SetCurFrame(0);
			pen->SetInitialEnergy(20);			
			pen->Frame.AddFixCoupling(17,19);  //0] giunto attacco torretta
			pen->Frame.AddFixCoupling(36,67);  //1] giunto in basso
			pen->Frame.AddFixCoupling(86,40);  //2] giunto a destra
			pt=(CTurret*)pen;

			pt->m_Turret.SetFrameManager(&g_fm);
			//crea la torretta
			if (!pt->m_Turret.CreateComponent(0,0,&m_imgTurret[0],1)) return NULL;
			//giunto della torretta
			pt->m_Turret.AddFixCoupling(105,105);  //(0)
			//bocche dei cannoni
			pt->m_Turret.AddFixCoupling(14,100); //1 orizzontale (1)
			pt->m_Turret.AddFixCoupling(98,2); //2 verticale (2)

			pt->m_Turret.AddFixCoupling(0,100); //3 orizz (3)
			pt->m_Turret.AddFixCoupling(103,18); //4 vert (4)

			pt->m_Turret.AddFixCoupling(1,101); //2 orizz (5)
			pt->m_Turret.AddFixCoupling(100,13); //1 vert (6)

			pt->m_Turret.SetBoundPoly(0,&m_bpMod[id]);

			for (count1=1;count1<13;count1++)
			{
				pt->m_Turret.AddFrame(&m_imgTurret[count1]);
				pt->m_Turret.SetFrameHotSpot(count1,0,105,105);
				pt->m_Turret.SetBoundPoly(count1,&m_bpMod[id]);
			}

			//imposta le armi
			pt->SetWeapon(0,&g_sEShell7); //cannone 1
			pt->SetWeapon(1,&g_sEShell11); //cannone 2 (p.laser)
			pt->SetWeapon(2,&g_sEShell4); //cannone 3  (p. rosso direz.)
			pt->SetWeapon(3,&g_sEShell7); //cannone 4 (p. sferico)
			pt->m_Turret.LinkToComponent(0,0,&pen->Frame);
			pen->dwScore=1000;
			pen->lpfnMultiExplosion=PushExplMulti;
			pen->lpfnExplode=PushExpl2;
			pen->SetActiveStack(&g_objStack);

		
		}		

		break;

	default:

		//id non valido
		return NULL;	
	}

	return (CModularEnemy *)m_ModEnemies[id].m_objStack[m_iStackTop[id]++];
}


//attacca un modulo ad un altro estraendolo dallo stack dei moduli
HRESULT AttachEnemy(CResourceManager *prm,int iCoupling,int iChildCoupling,CModularEnemy *parent,MOD_ENEMY_ID child_type)
{
	if (!parent) return E_FAIL;

	CModularEnemy *pmod;

	//estrae l'elemento da attaccare
	pmod=GetModEnemy(prm,child_type);
	
	if (pmod)
	{
		if (parent->AttachEnemy(iCoupling,iChildCoupling,pmod)) return S_OK;	
	}

	return E_FAIL; 	
}


//rilascia le risorse allocate
void FreeModEnemies(void)
{
	//rot cannon
	if (m_ModEnemies[MOD_ROT_CANNON].ItemCount())
	{
		g_fm.FreeFrameArray(m_imgRotCannon3,24);
	}

	if (m_ModEnemies[MOD_TURRET].ItemCount())
	{
		g_fm.FreeFrameArray(m_imgTurret,13);
	}

	if (m_ModEnemies[MOD_CANNON5].ItemCount())
	{
		g_fm.FreeFrameArray(m_imgCircularCannon,2); //frame addizionali
	}


	for (int count=0;count<MOD_ENEMY_MAX;count++)
	{
		if (m_ModEnemies[count].ItemCount()>0)
		{
			m_ModEnemies[count].Clear(TRUE);
			g_fm.FreeImgFrame(&m_imgMod[count]);
			SAFE_DELETE_ARRAY(m_bpMod[count].poly);
			m_bpMod[count].inum=0;
		}
	}
}


