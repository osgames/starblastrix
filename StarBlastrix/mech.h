//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mech.h Macchina costituita da piu' componenti
///////////////////////////////////////////////////////////////////////*/

#include "a32sprit.h"

#ifndef _MECH_INCLUDE_
#define _MECH_INCLUDE_

//coppie ad un grado di libert�
typedef enum COUPLING_TYPE_ENUM
{
	LK_FIXED=0,   //acoppiamento fisso
	LK_ROTOIDAL=1, //coppia rotoidale
	LK_PRISMATIC=2  //coppia prismatica

} COUPLING_TYPE_ENUM;


class CMechComponent;

//accopiamento 
typedef struct MECH_LINK_TYPE
{
	COUPLING_TYPE_ENUM iType;
	int xrel,yrel; //coordinare relative (nel sistema di riferimento del membro) della coppia corrente
	float cx,cy;    //coseni direttori della direzione (in caso di coppia prismatica)
	int direction;  //direzione (angolo) solo coppie prismatiche
	MECH_LINK_TYPE *next; //usato nelle linked list
	int lambda1,lambda2; //intervallo valido per il parametro lambda
	void *pLinked;       //punta il componente linkato a questa coppia 
	int id; //id dell'accoppiamento
	int bOpenLoop; //indica che l'oggetto esegue una rotazione ripetuta (non ci sono estremi per i valori di lambda)

} MECH_LINK_TYPE,*MECH_LINK_TYPE_PTR;


class CMechComponent: public virtual CADXSprite
{
private:

	//l'elemento radice della catena cinematica � quello ch non ha elemnti genitori
	CMechComponent *m_pParent; //elemento genitore a cui � collegato questo elemento
	MECH_LINK_TYPE_PTR m_pParentLink,m_pBaseLink; //link base (accoppiamento  cui � collegato il genitore)	
	int lambda;	//coordinata lagrangiana di questo elemento
	int delta_angle;	
	int m_status;
	int m_from_angle; //intervallo dell'angolo di posizione
	int m_to_angle;
	int theta; //angolo di rotazione assoluto
	
protected:


	MECH_LINK_TYPE_PTR m_pFirstItem; //primo elemento della linked list
	MECH_LINK_TYPE_PTR m_pCurrent;   //elemento corrente nella linked list
	MECH_LINK_TYPE_PTR m_pLinkTemp;  //variabile temporanea
	CMechComponent     *m_pcTemp;    //variabile temporanea
	MECH_LINK_TYPE_PTR m_pWalk;     //usato da GetNextChild

	void SetTheta(int iangle);
	void SetDisplacement(int lambda);
	void IncrTheta(int delta); //incrementa l'angolo
	//acquisisce il puntatore alla coppia cinematica passando un indice
	MECH_LINK_TYPE_PTR GetCoupling(int iCoupling);
	//aggiunge un accoppiamento geniroco
	//lambda1 e lambda2 definiscono il range della coordinata lagrangiana per questa coppia
	//direction � usata solo per le coppia prismatica e definisce la direzione relativa di traslazione
	int AddCoupling(COUPLING_TYPE_ENUM iType,int xrel,int yrel,int lambda1,int lambda2,int direction,int iOpenLoop=0);

	
public:

	int m_iTag1,m_iTag2; //general purpose
	CMechComponent(void);
	virtual ~CMechComponent(void);
	//rende 1 se va tutto bene
	int CreateComponent(int from_angle,int to_angle,IMAGE_FRAME_PTR pframes,int iNumFrames);
	//rende l'indice dell'accopiamento (-1 se va in errore)
	//coppia rotoidale
	int AddRotCoupling(int xrel,int yrel,int angle1=0,int angle2=360,int iOpenLoop=0);
	//copia prismatica
	int AddPrismCoupling(int xrel,int yrel,int direction,int displ1,int displ2);
	//accoppiamento rigido
	int AddFixCoupling(int xrel,int yrel);
	//disaccopia il componente dal genitore
	void Unlink(void);
	//collega questo elemento ad un elemento genitore 
	int LinkToComponent(int iCoupling,int iParentCoupling,CMechComponent *pComponent);
	//imposta la coordinata lagrangiana per questo elemento
	int SetLambda(int lambda);
	int GetLambda(void);
	//acquisice le coordinate relative del giunto
	int GetCouplingPos(int iCoupling,int *xrel,int *yrel);
	//disegna tutta la catena cinematica
	int RenderKinematicChain(void);
	//disengna la catena cinematica usando pDest come immagine di output
	int RenderKinematicChain(IMAGE_FRAME_PTR pDest);
	//rilascia la memoria allocata
	void Release(void);
    //rende true se � attiva l'eplorazione della catena dell'elemento
	BOOL isWalking(void); 
	//ridefinisce set display
	HRESULT SetDisplay(IMAGE_FRAME_PTR imgDisplay);
	//agiorna la catena cinematica
	HRESULT UpdatePosition(void);
	//angolo di rotazione assoluto
	int GetTheta(void);
	//restituisce tutti gli elementi figli
	//se reset � zero riparte dal primo altrimenti li rende in successione
	CMechComponent *GetNextChild(int reset=0);
	CMechComponent *GetParentComponent(void);
};

#endif
