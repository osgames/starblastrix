//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    
	Starblastrix - side scrolling shoot'em up
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  sbl.cpp - compilatore e level editor per Start Blastrix
///////////////////////////////////////////////////////////////////////*/


#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include "sbl.h"

//macro che restituisce il numero di simboli nella tabella delle etichette
#define NUM_SYMBOLS ((int) (sizeof m_label_hash / sizeof m_label_hash[0])) 

//hash di corrispondenze fra etichette e descrizione
//(tabella dei simboli)

/*
nota: quando si aggiungono id modificare enemy_enum in sbl.h,m_label_hash in sbl.cpp e
e la funzione CreateResHanler in sbengine.cpp e m_enemy_names[] in sbengine.cpp
*/
struct 
{
	int ivalue;
	char *svalue;
}

m_label_hash[]=
{

	EN_UFO,"EN_UFO",
    EN_GOLDENFIGHTER,"EN_GOLDENFIGHTER",
	EN_ANTIAIRCRAFT,"EN_ANTIAIRCRAFT",
    EN_ANTIAIRCRAFT2,"EN_ANTIAIRCRAFT2",
	EN_RADAR,"EN_RADAR",
	EN_SNAKE,"EN_SNAKE",
	EN_XWING,"EN_XWING",
	EN_LAMBDAFIGHTER,"EN_LAMBDAFIGHTER",
	EN_MFIGHTER,"EN_MFIGHTER",
	EN_AIRCRAFTCARRIER,"EN_AIRCRAFTCARRIER",
	EN_TANK,"EN_TANK",
	EN_SHIP,"EN_SHIP",
	EN_UFO2,"EN_UFO2",
	EN_BLASTER,"EN_BLASTER",
	EB_POWERUP1,"EB_POWERUP1",
	EB_SPECIAL,"EB_SPECIAL",
	EB_SHIELD,"EB_SHIELD",
	EB_ELECTRIC_BARRIER,"EB_ELECTRIC_BARRIER",
	EB_ENERGY,"EB_ENERGY",
	EB_POWERUP2,"EB_POWERUP2",
	EB_BOMBS10,"EB_BOMBS10",
	EB_BOMBS50,"EB_BOMBS50",
	EB_SCORE,"EB_SCORE",
	EB_THRUST,"EB_THRUST",
	EB_BOOSTER,"EB_BOOSTER",
	EB_THUNDER,"EB_THUNDER",
	EB_MISSILE,"EB_MISSILE",
	EB_LASER,"EB_LASER",
	EB_WEAPONA,"EB_WEAPONA",
	EB_WEAPONB,"EB_WEAPONB",
	EB_WEAPONC,"EB_WEAPONC",
	EB_WEAPOND,"EB_WEAPOND",
	EB_GYROS,"EB_GYROS",
	EB_1UP,"EB_1UP",
	EB_BLUE_SHELL,"EB_BLUE_SHELL",
	EB_FWSHELLS,"EB_FWSHELLS",
	EB_LASER3,"EB_LASER3",
	EN_SHANTY1,"EN_SHANTY1",
	EN_SHANTY2,"EN_SHANTY2",
	EN_SHANTY3,"EN_SHANTY3",
	EN_SHANTY4,"EN_SHANTY4",
	EN_FACTORY,"EN_FACTORY",
	EN_PYLON,"EN_PYLON",
	EN_STREET,"EN_STREET",
	EN_PALMS,"EN_PALMS",
	EN_WALL,"EN_WALL",
	EN_BOSS1,"EN_BOSS1",
	EN_BOSS2,"EN_BOSS2",
	EN_BOSS3,"EN_BOSS3",
	EN_BOSS4,"EN_BOSS4",
	EN_BOSS5,"EN_BOSS5",
	EN_SNAKERED,"EN_SNAKERED",
	EN_SPIDER1,"EN_SPIDER1",
	EN_SPIDER2,"EN_SPIDER2",
	EN_SPIDER3,"EN_SPIDER3",
	EN_ANTIAIRCRAFT3,"EN_ANTIAIRCRAFT3",
	EN_GFIGHTER,"EN_GFIGHTER",
	EB_GRND_FIRE,"EB_GRND_FIRE", //fuoco verso terra
	EB_TRIPLE_FIRE,"EB_TRIPLE_FIRE", //fuoco triplo
	EB_STRIGHT_MISSILE,"EB_STRIGHT_MISSILE", //missile
	EN_BUILDING1,"EN_BUILDING1",
	EN_BUILDING2,"EN_BUILDING2",
	EN_BUILDING3,"EN_BUILDING3",
	EN_BUILDING4,"EN_BUILDING4",
	EN_ICEBERG,"EN_ICEBERG",
	EN_UFO3,"EN_UFO3",
	EN_SCREW_FIGHTER,"EN_SCREW_FIGHTER",
	EN_NIGHT_BUILDING1,"EN_NIGHT_BUILDING1",
	EN_NIGHT_BUILDING2,"EN_NIGHT_BUILDING2",
	EN_NIGHT_BUILDING3,"EN_NIGHT_BUILDING3",
	EN_NIGHT_BUILDING4,"EN_NIGHT_BUILDING4",
	EN_NIGHT_BRIDGE,"EN_NIGHT_BRIDGE",
	EN_LFIGHTER,"EN_LFIGHTER",
	EN_BLASTER2,"EN_BLASTER2",
	EN_BLASTER3,"EN_BLASTER3",
	EN_MODULAR1,"EN_MODULAR1",
	EN_MODULAR2,"EN_MODULAR2",
	EN_MODULAR3,"EN_MODULAR3",
	EN_MODULAR4,"EN_MODULAR4",
	EN_MODULAR5,"EN_MODULAR5",	
	EN_MODULAR6,"EN_MODULAR6",
	EN_MODULAR7,"EN_MODULAR7",
	EN_MODULAR8,"EN_MODULAR8",
	EN_SNAKERED1,"EN_SNAKERED1",
	EN_SNAKERED2,"EN_SNAKERED2",
	EN_JELLYSHIP,"EN_JELLYSHIP",
	EN_ASTEROID_SMALL,"EN_ASTEROID_SMALL",
	EN_ASTEROID_MEDIUM,"EN_ASTEROID_MEDIUM",
	EN_ASTEROID_BIG,"EN_ASTEROID_BIG",
	EN_SHIELD_BASE,"EN_SHIELD_BASE",
	EN_SHELL_BASE,"EN_SHELL_BASE",
	EN_TURRET,"EN_TURRET",
	EN_TRAIN,"EN_TRAIN",
	EN_RAIL,"EN_RAIL",
	EN_SPACE_BUILD1,"EN_SPACE_BUILD1",
	EN_SPACE_BUILD2,"EN_SPACE_BUILD2",
	EN_SPACE_BUILD3,"EN_SPACE_BUILD3",
	EN_SPACE_BUILD4,"EN_SPACE_BUILD4",
	EN_HTRUSS,"EN_HTRUSS",
	EN_VTRUSS,"EN_VTRUSS",
	EN_RING,"EN_RING",
	EN_MAMMOTH,"EN_MAMMOTH",
	EN_LIFTER,"EN_LIFTER",
	EN_BLEEZER,"EN_BLEEZER",
	EN_HFIGHTER,"EN_HFIGHTER"

};

char m_szLastError[MAX_ERR];

//----------------------------- CompileLevel ------------------------------------

//Crea un file binario sbl a partire da uno script di definizione livello (compilatore di script sbl)
int CompileLevel(char *szInputFile,char *szOutPutFile)
{
	FILE *fin,*fout;
	SBL_LEVEL sbl;
	SB_VALUE_LIST list;
	SB_VALUE_LIST_PTR plist=NULL,plist1=NULL;
	SB_VALUE_LIST_PTR parg=NULL;
	SBL_GROUND_LAYER_PTR pl=NULL;
	SBL_BACK_LAYER_PTR pback=NULL;
	SBL_TRIGGER_PTR ptr=NULL;
	SBL_FUNCTION_PTR pfn=NULL;
	int ch=0;
	int icount=0,icount1=0;
	int ilinecnt=1; //contatore linee
	int ires=0;
	int istate=0;
	int ilen;
	char sToken[80];
	char line[1024];
	char *pch=NULL;
	char *dest;
	int *idest;
	float *fdest;
	int bloop=1;
	size_t sz;

    //inizializza la lista
	list.next=NULL;
	list.value=NULL;

	//azzera l'errore
	SetSbLastError(NULL);

	//resetta la struttura del livello
	memset(&sbl,0,sizeof(SBL_LEVEL));
	
	if (!(fin=fopen(szInputFile,"r"))) 
	{
		FreeSBL(&sbl);
		SetSbLastError("impossibile trovare il file di input %s.",szInputFile);
		return 0;
	}
	
	if (!(fout=fopen(szOutPutFile,"wb")))
	{
		FreeSBL(&sbl);
		fclose(fin);
		SetSbLastError("impossbile aprire in scrittura binaria il file di output %s",szOutPutFile);
		return 0;
	}
	
	//ciclo di lettura del file
	while (!feof(fin))
	{
		do
		{
			ch=fgetc(fin);
			if ((char)ch=='\n') ilinecnt++; //nuova linea
						
		} while (isspace(ch) || iscntrl(ch)); //scarta i tbs e gli spazi e tutti i caratteri di controllo

		if ((char)ch=='#')
		{
			//� un commento legge tutta la riga
			fgets(line,1024,fin);
			ilinecnt++;
		}
		else
		{
			memset(sToken,0,80);
			icount=0;
			istate=0;
			sToken[0]=(char)ch;
			//legge il token ; il token � del tipo identifier:
			do
			{
				ch=(char)fgetc(fin);
				if (isspace(ch) || ch==':') istate=1;

				if (istate==1)
				{
					//� in attesa di trovare due punti :
					if (!(isspace(ch) || ch==':'))
					{
						fclose (fin);
						fclose (fout);
						FreeSBL(&sbl);
						SetSbLastError("Identificatore non valido alla riga %d ",ilinecnt);
						return 0;
					}
				}
				else
				{
					sToken[++icount]=(char)ch;					
				}

			} while ((char)ch!=';' && (!feof(fin)) && (char)ch!=':' && icount<79);

			if (feof(fin)) break; //esce dal ciclo


			if ((char)ch!=':') 
			{
				fclose (fin);
				fclose (fout);
				FreeSBL(&sbl);
				SetSbLastError("Errore di sintassi alla linea %d del file %s : ogni identificatore deve essere seguito da due punti.",ilinecnt,szInputFile);
				return 0;
			}

			pch=sToken;

			//converte il token in maiuscolo
			while ((int)*pch!=0)
			{
				*pch=toupper(*pch); 
				pch++;
			}


			//resetta la linked list dei valori
			//nota: il primo elemento della lista non va eliminato perch� non � istanziato con new
			plist=&list;
			SAFE_DELETE_ARRAY(plist->value);
			plist=plist->next;
			plist1=NULL;
				
			//resetta la lista			
			while(plist)
			{
				plist1=plist->next;
				SAFE_DELETE_ARRAY(plist->value); //cancella il nome del simbolo
				SAFE_DELETE(plist);
				plist=plist1;
			}						

			dest=NULL;
			idest=NULL;
			fdest=NULL;
			list.next=NULL;
		
			//popola un campo diverso della struttura sbl a seconda del token
			//valori scalari stringa
			if (0==strcmp(sToken,"NAME")) dest=sbl.head.szlevel_name; //nome del livello
			else if (0==strcmp(sToken,"NAME1")) dest=sbl.head.szlevel_name1; // nome del livello nella 2� lingua
			else if (0==strcmp(sToken,"MSG")) dest=sbl.head.szfinal_message; //messaggio finale
			else if (0==strcmp(sToken,"MSG1")) dest=sbl.head.szfinal_message1; //messaggio finale nella 2� lingua
			else if (0==strcmp(sToken,"GROUND_FILE")) dest=sbl.head.szvpxground_tiles; //file vpx dei tiles
			else if (0==strcmp(sToken,"BACK_FILE")) dest=sbl.head.szvpxbackground; //file vpx del background
			//valori scalari interi
			else if (0==strcmp(sToken,"BACK_FILE_INDEX")) idest=&sbl.head.ibackindex; //frame del file vpx di back ground
			else if (0==strcmp(sToken,"GROUND_FILE_INDEX")) idest=&sbl.head.igrndindex; //frame del file vpx dei tiles
            else if (0==strcmp(sToken,"LEVEL")) idest=&sbl.head.ilevel; //numero del livello
			else if (0==strcmp(sToken,"CLEAR_FN")) idest=&sbl.head.iclear_procedure; //procedura che ridisegna il back ground
			else if (0==strcmp(sToken,"BACK_IMAGE")) dest=sbl.head.szvpxsky; //immag�ne fissa da usare come sfondo (file vpx)
			else if (0==strcmp(sToken,"BACK_IMAGE_INDEX")) idest=&sbl.head.iskyindex; //indice frame immagine di sfondo 
			else if (0==strcmp(sToken,"SCROLL_VELOCITY")) fdest=&sbl.head.scroll_velocity;
			else if (0==strcmp(sToken,"MUSIC"))  dest=sbl.head.szmusic; //file midi (musica di background)
			else if (0==strcmp(sToken,"BACK_IMAGE_TOP")) idest=&sbl.head.iback_img_top; //ordinata di ouput dell'immagine di background
			else if (0==strcmp(sToken,"GRAVITY")) fdest=&sbl.head.gravity; //accelerazione di gravit�
			else if (0==strcmp(sToken,"BOTTOM")) idest=&sbl.head.ibottom; //fondo solido del livello
			else if (0==strcmp(sToken,"PASSWORD")) idest=&sbl.head.password; //password

			else
			{

				ires=GetValueList(fin,&list);
				if (ires==-1)
				{
					fclose (fin);
					fclose (fout);
					FreeSBL(&sbl);
					SetSbLastError("Era atteso ; alla fine dell'istruzione %s alla linea %d",sToken,ilinecnt);
					return 0;

				}

				//valori vettoriali				
				//GROUND_LAYER yout,left,top,width,height,rows,cols,parallax,mapitems             
				if (0==strcmp(sToken,"GROUND_LAYER"))
				{				
					if (ires!=9)
					{
					    fclose (fin);
					    fclose (fout);
					    FreeSBL(&sbl);
					    SetSbLastError("Il numero di parametri della struttura GROUND_LAYER non � corretto.(riga %d)",ilinecnt);
					    return 0;
					}				

			/*		if (sbl.inumgrnd_layers>=6)
					{
						fclose (fin);
					    fclose (fout);
					    FreeSBL(&sbl);
					    SetSbLastError("E' stato superato il numero massimo di ground layers disponibili.(riga %d)",ilinecnt);
					    return 0;
					}*/

					if (!pl)
					{
						sbl.pgrnd_layers=new SBL_GROUND_LAYER;
						pl=sbl.pgrnd_layers;
						memset(pl,0,sizeof(SBL_GROUND_LAYER));					

					}
					else
					{
						pl->next=new SBL_GROUND_LAYER;
						pl=pl->next;
						memset(pl,0,sizeof(SBL_GROUND_LAYER));
					}
				
					plist=&list;

					pl->ichunk_type=1;
					pl->iyout=atoi(plist->value);
					plist=plist->next;
					pl->ircleft=atoi(plist->value);
					plist=plist->next;
					pl->irctop=atoi(plist->value);
					plist=plist->next;
					pl->itile_width =atoi(plist->value);
					plist=plist->next;
					pl->itile_height=atoi(plist->value);
					plist=plist->next;
					pl->irows=atoi(plist->value);
					plist=plist->next;
					pl->icols=atoi(plist->value);
					plist=plist->next;
					pl->fparallax=(float)atof(plist->value);
					plist=plist->next;
					pl->inumtiles=0; //verr� inserito in seguito					
					sbl.inumgrnd_layers++;
					plist=NULL;
				}

				// BACK_LAYER yout,left,top,width,height,parallax 
				else if (0==strcmp(sToken,"BACK_LAYER"))
				{				
					if (ires!=6)
					{
						fclose (fin);
					    fclose (fout);
					    FreeSBL(&sbl);
					    SetSbLastError("Il numero di parametri della struttura BACK_LAYER non � corretto.(riga %d)",ilinecnt);
					    return 0;
					}			

					if (!pback)
					{
						sbl.pback_layers=new SBL_BACK_LAYER;
						pback=sbl.pback_layers;
						memset(pback,0,sizeof(SBL_BACK_LAYER));
					}
					else
					{
						pback->next=new SBL_BACK_LAYER;
						pback=pback->next;
						memset(pback,0,sizeof(SBL_BACK_LAYER));
					}
				
					plist=&list;

					pback->ichunk_type=2;
					pback->iyout=atoi(plist->value); //y out a partire dall'alto del video asse rivolto in basso
					plist=plist->next;
					pback->ircleft=atoi(plist->value);
					plist=plist->next;
					pback->irctop=atoi(plist->value);
					plist=plist->next;
					pback->itile_width=atoi(plist->value);
					plist=plist->next;
					pback->itile_height=atoi(plist->value);
					plist=plist->next;
					pback->fparallax=(float)atof(plist->value);
					sbl.inumbacklayers++; //incrementa il numero di back layers

				}

				//MAP [sequenza valori dell mappa]
				//l'istruzione MAP deve essere inserita dopo una istruzione ground layer e serve a definire la lista dei tiles dell'ultimo ground layer inserito
				else if (0==strcmp(sToken,"MAP"))
				{
				
					if (ires>0) 
					{						
						pl->pielems=new int[ires];
						int *pitm=pl->pielems;
						pl->inumtiles=ires; //numero di tile nella mappa di questo layer
						memset(pitm,0,sizeof(int)*ires);
						//copia la mappa 
						plist=&list;
						icount=0;
						while (plist) {pitm[icount++]=atoi(plist->value);plist=plist->next;}
						plist=NULL;
					}

				}

				//CALL:chiamata di una funzione
				else if (0==strcmp(sToken,"CALL"))
				{
					if (ires<1)
					{
						fclose (fin);
					    fclose (fout);
					    FreeSBL(&sbl);
					    SetSbLastError("Il numero di parametri della chiamata di funzione CALL non � corretto.(riga %d) era atteso almeno il nome della funzione da chiamare",ilinecnt);
					    return 0;
					}

					if (!pfn)
					{
						//inizializza la lista					
						sbl.pfunctions=new SBL_FUNCTION;					
						pfn=sbl.pfunctions;
						memset(pfn,0,sizeof(SBL_FUNCTION));
						pfn->next=NULL;
					}
					else
					{
						pfn->next=new SBL_FUNCTION;
						pfn=pfn->next;
						memset(pfn,0,sizeof(SBL_FUNCTION));					
						pfn->next=NULL;
					}

					pfn->nargs=0;
					plist=&list;
					//nome della funzione
					ilen=strlen(plist->value);
					pfn->name=new char[ilen+1];
					memset(pfn->name,0,(ilen+1)*sizeof(char));
					memcpy(pfn->name,plist->value,ilen*sizeof(char));
					plist=plist->next;
					parg=NULL;

					while(plist)
					{
						if (!parg)
						{
							//inizializza la lista degli argomenti
							pfn->parguments=new SB_VALUE_LIST;
							parg=pfn->parguments;
							parg->next=NULL;
						}
						else
						{
							parg->next=new SB_VALUE_LIST;
							parg=parg->next;
							parg->next=NULL;
						}
						
						ilen=strlen(plist->value);
						parg->value=new char[ilen+1];
						memset(parg->value,0,(ilen+1)*sizeof(char));
						memcpy(parg->value,plist->value,ilen*sizeof(char));	
						pfn->nargs++; //incrementa il numero di argomenti
						plist=plist->next;
				
					}//fine while
				}

			    /*TRIGGER xabs,xrel,yrel,ipower,ienergy,ienemy,iflags,ibonus,iprob		
				  nota per quanto riguarda i trigger, l'id del nemico (ienemy) oltre che in forma numerica,
				  puo' essere rappresentato con una etichetta per rendere piu' leggibile il codice
				  l'elenco delle etichette � definito in sbl.h
				*/

				else if (0==strcmp(sToken,"TRIGGER"))
				{				
					if (!(ires>=6 && ires<=10))
					{
						fclose (fin);
					    fclose (fout);
					    FreeSBL(&sbl);
					    SetSbLastError("Il numero di parametri della struttura TRIGGER non � corretto.(riga %d)",ilinecnt);
					    return 0;
					}

					if (!ptr)
					{
						//inizializza la lista dei trigger
						sbl.ptriggers=new SBL_TRIGGER;
						sbl.ptriggers->next=NULL;
						ptr=sbl.ptriggers;

					}
					else
					{
						//aggiunge un lemento alla lista dei trigger
						ptr->next=new SBL_TRIGGER;
						ptr=ptr->next;
						ptr->next=NULL;
					}

					//inizializza i parametri opzionali
					ptr->iflags=0;
					ptr->iprob=100; //100% probbilit� per default
					ptr->ibonus=0;
					ptr->zlayer=0;

					plist=&list;
					ptr->xabs=atoi(plist->value);
					plist=plist->next;
					ptr->xrel=atoi(plist->value);
					plist=plist->next;
					ptr->yrel=atoi(plist->value);
					plist=plist->next;
					ptr->ipower=atoi(plist->value);
					plist=plist->next;
					ptr->ienergy=atoi(plist->value);
					plist=plist->next;
					if (isalpha(plist->value[0]))
					{
						//il nemico � identificato da una etichetta
						ptr->ienemy=TranslateId(plist->value);
						if (!ptr->ienemy)
						{
							//l'etichetta non appartiene alla tabella dei simboli
							fclose (fin);
					        fclose (fout);
					        FreeSBL(&sbl);
					        SetSbLastError("Il simbolo %s non � valido.(riga %d)",plist->value,ilinecnt);
					        return 0;
						}
					}
					else ptr->ienemy=atoi(plist->value);

					plist=plist->next;
					//inizio parametri opzionali
					if (plist)
					{
						ptr->iflags=atoi(plist->value);
						plist=plist->next;
						if (plist)
						{
							//bonus 
							//controlla se deve interpretare il simbolo
							if (isalpha(plist->value[0]))
							{
								ptr->ibonus=TranslateId(plist->value);
								if (!ptr->ibonus)
								{
									//l'etichetta non appartiene alla tabella dei simboli
									fclose (fin);
									fclose (fout);
									FreeSBL(&sbl);
									SetSbLastError("Il simbolo %s non � valido.(riga %d)",plist->value,ilinecnt);
									return 0;
								}
							}
							else
							{
								ptr->ibonus=atoi(plist->value);
							}
							
							plist=plist->next;
							if (plist)
							{
								//probabilit� (da 1 a 100)
								ptr->iprob=atoi(plist->value);
								if (ptr->iprob>100) ptr->iprob=100;
								else if (ptr->iprob<0) ptr->iprob=0;

								//zlayer
								plist=plist->next;
								if (plist)
								{
									ptr->zlayer=atoi(plist->value);
									if (ptr->zlayer<0) ptr->zlayer=0;
								}

							}
						}
					}				
				}
			}

			if (dest)
			{
				//copia un valore stringa
				ires=GetValueList(fin,&list);
				if (ires<=0) 
				{
					fclose (fin);
					fclose (fout);
					FreeSBL(&sbl);
					SetSbLastError("Non � stato definito un valore di tipo stringa alla riga %d",ilinecnt);
					return 0;
				}

				else 
				{
					sz=strlen(list.value)+1;				
					memcpy(dest,list.value,sz-1);
				}

			}//fine if (dest)

			else if (idest) 
			{
				//aggiorna un valore numerico intero
				//copia un valore stringa
				ires=GetValueList(fin,&list);
				if (ires<=0) 
				{
					fclose (fin);
					fclose (fout);
					FreeSBL(&sbl);
					SetSbLastError("Non � stato definito un valore di tipo numerico alla riga %d",ilinecnt);
					return 0;
				}

				else 
				{
					*idest=atoi(list.value);				
				}
			}
			else if (fdest)
			{
				ires=GetValueList(fin,&list);
				if (ires<=0)
				{
					fclose (fin);
	    			fclose (fout);
					FreeSBL(&sbl);
					SetSbLastError("Non � stato definito un valore di tipo numerico alla riga %d",ilinecnt);
					return 0;
				}
				else
				{
					*fdest=(float)atof(list.value);
				}
			}			
		}
	}	

	//scrive la struttura compilata nel file
	
	//id del file
	fwrite("sbl\x0",4,1,fout);
	sbl.head.ichunk_type=0;
	fwrite(&sbl.head,sizeof(SBL_HEAD),1,fout);	

	//ground layers
	
	pl=sbl.pgrnd_layers;

	while (pl) 
	{
		pl->ichunk_type=1;
		fwrite(pl,sizeof(SBL_GROUND_LAYER),1,fout);
		//scrive la mappa relativa al layer
		fwrite(pl->pielems,sizeof(int)*pl->inumtiles,1,fout);
		pl=pl->next;
	}

	//back layers

	pback=sbl.pback_layers;

	while(pback) 
	{
		pback->ichunk_type=2;
		fwrite(pback,sizeof(SBL_BACK_LAYER),1,fout);
		pback=pback->next;
	}

	//scrive i trigger nel file binario
	ptr=sbl.ptriggers;
	while(ptr)
	{
		ptr->ichunk_type=3;
		fwrite(ptr,sizeof(SBL_TRIGGER),1,fout);
		ptr=ptr->next;
	}

	//scrive le chiamate di funzioni
	pfn=sbl.pfunctions;
	while(pfn)
	{
		pfn->chunk_type=4;
		//chunk
		fwrite(&pfn->chunk_type,sizeof(pfn->chunk_type),1,fout); 
		//nome della funzione
		fwrite(pfn->name,sizeof(char)*(strlen(pfn->name)+1),1,fout);
		//numero di argomenti
		fwrite(&pfn->nargs,sizeof(pfn->nargs),1,fout);
		//scrive gli argomenti (ogni argomento � una stringa che termina con 0) 
		parg=pfn->parguments;

		while(parg)
		{
			fwrite(parg->value,(strlen(parg->value)+1)*sizeof(char),1,fout);
			parg=parg->next;
		}

		pfn=pfn->next; //si sposta sulla funzione successiva
	}

	fclose(fin);

	fclose(fout);

	FreeSBL(&sbl);

	return 1;
}

//------------------------------- GetValueList ------------------------------------

//Acquisisce la lista di valori di un identifier durante la cmpilazione del file sbl
//la lista di valori � separata da virgola
int GetValueList(FILE *f,SB_VALUE_LIST_PTR list)
{
	char ch;
	char val[256]; //un valore puo' al massimo avere un lunghezza di 256 bytes
	char *pch;
	size_t sz;
	int icnt=0;
	int bescape;
	SB_VALUE_LIST_PTR plist=list;

	if (!f) return 0;
	if (!list) return 0;

	pch=val;

	memset(val,0,256);
	
	do 
	{
		
		ch=fgetc(f);
		
		if (ch=='\\') bescape=1; //carattere di escape
		
		if ((char)ch==':' && !bescape) 
		{
			return -1; //in questo caso probabilemte � stato saltato il ; finale il caratter : non � ammesso tra i valori
		}

		bescape=0;

		if (ch!=',' && ch!=';') {*pch=ch;pch++;}
		else
		{
			//immette il valore corrente nella list
			if (icnt>0)
			{
				//il primo elemento lo mette direttamente in list
				plist->next=new SB_VALUE_LIST;
				plist=plist->next;
				plist->next=NULL;
			}		
		
			sz=pch+1-val;
			plist->value=new char[sz];
			memset(plist->value,0,sz);
			memcpy(plist->value,val,sz);
			icnt++;	
			//prepara l'input per il nuovo valore
			memset(val,0,256);
			pch=val;
		}		

		//da fare gestire l'errore quando pch-val >19 o feof(fin)
	} while (ch!=';' && !feof(f) && (pch-val)<255); //si ferma quando trova il ;

	return icnt;

}


//---------------------------------------- GetLastError ----------------------------

char *GetSbLastError(void) //restituisce l'ultimo errore che si � verificato
{
	return m_szLastError;
}

//------------------------------------------ SetSbLastError ---------------------------

void SetSbLastError(char *szFormat,...)
{
	va_list pArgList;

	memset(m_szLastError,0,MAX_ERR);

	if (!szFormat) return;
	
	va_start(pArgList,szFormat);
	//va_start � una macro che consente di puntare pArgList all'inizio dell'elenco dei parametri opzionali
	//la funzione _vsnprintf � basata sulla lunghezza del buffer che viene passato e non sullo zero finale come wsprintf ecc...
	_vsnprintf(m_szLastError,sizeof(m_szLastError) / sizeof(char),szFormat,pArgList);
     
	va_end(pArgList);

}

//---------------------------- FreeSBL -----------------------------------------

//rilascia una struttura SBL_LEVEL
void FreeSBL(SBL_LEVEL *psbl)
{
	SBL_GROUND_LAYER_PTR pl,pl1;
	SBL_BACK_LAYER_PTR pback,pback1;
	SBL_TRIGGER_PTR ptr,ptr1;
	SBL_FUNCTION_PTR pfn,pfn1;
	SB_VALUE_LIST_PTR plist,plist1;
	ptr=psbl->ptriggers;

	//ground layers
	pl=psbl->pgrnd_layers;

	while(pl)
	{
		pl1=pl->next;   
		SAFE_DELETE_ARRAY(pl->pielems);
		//dealloca il ground layer
		SAFE_DELETE(pl);
		pl=pl1;
	}

	//back layers
	pback=psbl->pback_layers;

	while(pback)
	{
		pback1=pback->next;
		SAFE_DELETE(pback);
		pback=pback1;
	}

	//rilascia la linked list dei trigger
	while (ptr)
	{
		ptr1=ptr->next;
		SAFE_DELETE(ptr);
		ptr=ptr1;
	}

	//rilascia la lista di funzioni
	pfn=psbl->pfunctions;
	while (pfn)
	{
		pfn1=pfn->next;
		SAFE_DELETE_ARRAY(pfn->name);

		plist=pfn->parguments;

		while(plist)
		{
			plist1=plist->next;
			SAFE_DELETE_ARRAY(plist->value);
			SAFE_DELETE(plist);
			plist=plist1;
		}

		SAFE_DELETE(pfn);
		pfn=pfn1;
	}
	
	memset(psbl,0,sizeof(SBL_LEVEL));

}


//---------------------------- TranslateId ----------------------------------------

//converte una etichetta ch identifica una risorsa, nel numero di risorsa corrispondente

int TranslateId(char *value)
{
	for (int count=0;count<NUM_SYMBOLS;count++)
	{
		if (0==strcmp(m_label_hash[count].svalue,value)) return m_label_hash[count].ivalue;
	}

	//simbolo non trovato
	return 0;
}

//--------------------------- TranslateLabel ---------------------------------------
//rende la descrizione che corrisponde all'id del nemico
char * TranslateLabel(int enemy_id)
{
	for (int count=0;count<NUM_SYMBOLS;count++)
	{
		if (m_label_hash[count].ivalue==enemy_id) 
		{
			return m_label_hash[count].svalue;
		}
	}

	return NULL;

}
