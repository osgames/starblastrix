//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mecharm.cpp -- braccio meccanico
///////////////////////////////////////////////////////////////////////*/


#include "a32graph.h"
#include "a32sprit.h"
#include "a32util.h"
#include "a32audio.h"
#include "mech.h"
#include "mecharm.h"
#include "sbengine.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern void PushExpl3(LONG x,LONG y);
extern void PushCollideMetal(LONG x,LONG y);
extern CADXSound g_cs;
extern CObjectStack g_sEShell11,g_sEShell12;
extern LONG g_snBfire,g_snMagnetic; //rumore sparo

IMAGE_FRAME g_imgCrank[24];  //manovella con due coppie rotoidali
IMAGE_FRAME g_imgCrank1[24]; //manovella con una coppia rotoidale e una prismatica
IMAGE_FRAME g_imgPiston[24]; //pistone (coppia prismatica)
IMAGE_FRAME g_imgCannon[24]; //cannone da inserire alla fine del braccio

//poligoni di contenimento
BOUND_POLY m_bpArm1[24];
BOUND_POLY m_bpArm2[24];
BOUND_POLY m_bpPiston[24];
BOUND_POLY m_bpCannon[24];
                                          
BOOL g_bResLoaded=FALSE;
LONG g_snRobotArm=0; //rumore del braccio meccanico

//carica le frames necessarie
HRESULT LoadMechArmResources(CResourceManager *prm)
{

	IMAGE_FRAME_PTR pimg;
	CADXFrameManager *pfm;

	if (!prm) return E_FAIL;
	if (g_bResLoaded) return S_OK; //risorse gi� caricate

	//se il file di risorse necessario a questo oggetto � gi� in memoria
	//evita di ricaricalo
	if (!(0==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),0))) return E_FAIL;
	}

	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;
    //ritaglia le immagini
	if (FAILED(pfm->GrabFrame(&g_imgCrank[0],483,426,483+150,426+28,pimg))) return E_FAIL;

	if (FAILED(pfm->GrabFrame(&g_imgCrank1[0],161,427,241,454,pimg))) return E_FAIL;

	if (FAILED(pfm->GrabFrame(&g_imgPiston[0],242,433,242+70,433+15,pimg))) return E_FAIL;

	if (FAILED(pfm->GrabFrame(&g_imgCannon[0],408,422,464,460,pimg))) return E_FAIL;

	//crea i bounding polygons
	m_bpArm1[0].poly=new VERTEX2D[4];
	m_bpArm1[0].inum=4;
	m_bpArm1[0].poly[0].x=0;
	m_bpArm1[0].poly[0].y=0;
	m_bpArm1[0].poly[1].x=150;
	m_bpArm1[0].poly[1].y=0;
	m_bpArm1[0].poly[2].x=150;
	m_bpArm1[0].poly[2].y=28;
	m_bpArm1[0].poly[3].x=0;
	m_bpArm1[0].poly[3].y=28;

	m_bpArm2[0].poly=new VERTEX2D[4];
	m_bpArm2[0].inum=4;
	m_bpArm2[0].poly[0].x=0;
	m_bpArm2[0].poly[0].y=0;
	m_bpArm2[0].poly[1].x=78;
	m_bpArm2[0].poly[1].y=0;
	m_bpArm2[0].poly[2].x=78;
	m_bpArm2[0].poly[2].y=28;
	m_bpArm2[0].poly[3].x=0;
	m_bpArm2[0].poly[3].y=28;

	m_bpPiston[0].poly=new VERTEX2D[4];
	m_bpPiston[0].inum=4;
	m_bpPiston[0].poly[0].x=0;
	m_bpPiston[0].poly[0].y=0;
	m_bpPiston[0].poly[1].x=70;
	m_bpPiston[0].poly[1].y=0;
	m_bpPiston[0].poly[2].x=70;
	m_bpPiston[0].poly[2].y=18;
	m_bpPiston[0].poly[3].x=0;
	m_bpPiston[0].poly[3].y=18;

	m_bpCannon[0].poly=new VERTEX2D[4];
	m_bpCannon[0].inum=4;
	m_bpCannon[0].poly[0].x=0;
	m_bpCannon[0].poly[0].y=0;
	m_bpCannon[0].poly[1].x=52;
	m_bpCannon[0].poly[1].y=0;
	m_bpCannon[0].poly[2].x=52;
	m_bpCannon[0].poly[2].y=28;
	m_bpCannon[0].poly[3].x=0;
	m_bpCannon[0].poly[3].y=28;

	//crea le frames ruotate (step 15�)
	for (int count=15;count<360;count+=15)
	{
		if (FAILED(pfm->CreateRotFrame(count,&g_imgCrank[count/15],&g_imgCrank[0],TRUE))) return E_FAIL;
		if (FAILED(pfm->CreateRotFrame(count,&g_imgCrank1[count/15],&g_imgCrank1[0],TRUE))) return E_FAIL;
		if (FAILED(pfm->CreateRotFrame(count,&g_imgPiston[count/15],&g_imgPiston[0],TRUE))) return E_FAIL;
		if (FAILED(pfm->CreateRotFrame(count,&g_imgCannon[count/15],&g_imgCannon[0],TRUE))) return E_FAIL;

		RotatePoly(&m_bpArm1[count/15],&m_bpArm1[0],&g_imgCrank[0],count,TRUE);
		RotatePoly(&m_bpArm2[count/15],&m_bpArm2[0],&g_imgCrank1[0],count,TRUE);
		RotatePoly(&m_bpPiston[count/15],&m_bpPiston[0],&g_imgPiston[0],count,TRUE);
		RotatePoly(&m_bpCannon[count/15],&m_bpCannon[0],&g_imgCannon[0],count,TRUE);
	}

	g_snRobotArm=g_cs.CreateSound("WAVS\\robotarm.wav",1);
	if (g_snRobotArm<0) return E_FAIL;

	g_bResLoaded=TRUE;

	return S_OK;

}

//rilascia le immagini
HRESULT FreeMechArmResources(CADXFrameManager *pfm)
{
	if (!pfm) return E_FAIL;
	if (!g_bResLoaded) return E_FAIL;

	for (int count=0;count<24;count++)
	{
		pfm->FreeImgFrame(&g_imgCrank[count]);
		pfm->FreeImgFrame(&g_imgCrank1[count]);
		pfm->FreeImgFrame(&g_imgPiston[count]);
		pfm->FreeImgFrame(&g_imgCannon[count]);
		
		//rilascia i bounding polygon
		SAFE_DELETE_ARRAY(m_bpArm1[count].poly);
		SAFE_DELETE_ARRAY(m_bpArm2[count].poly);
		SAFE_DELETE_ARRAY(m_bpPiston[count].poly);
		SAFE_DELETE_ARRAY(m_bpCannon[count].poly);
	}

	g_bResLoaded=FALSE;

	return S_OK;
}


//rilascia lo stack di oggetti
HRESULT FreeMechArmStack(CObjectStack *pstk)
{
	if (!pstk) return E_FAIL;
	
	int iu=pstk->ItemCount();

	CMechArm *p;

	for (int count=0;count<iu;count++)
	{
		p=(CMechArm *)pstk->m_objStack[count];
		//rilascia ognuno dei membri che compongono il braccio meccanico
		p->m_Crank1.Release();
		p->m_Crank2.Release();
		p->m_Crank3.Release();
		p->m_Piston.Release();
		p->m_Cannon.Release();
	}

	pstk->Clear(TRUE);

	return S_OK;
}


//crea un braccio meccanico
//itype=tipo di braccio da creare
HRESULT CreateMechArm(CMechArm *p,CADXFrameManager *pfm,int itype)
{
	int count;

	if (!pfm) return E_FAIL;
	if (!p) return E_FAIL;
	if (!g_bResLoaded) return E_FAIL;
	
	switch(itype)
	{
	case 0:

		p->SetFrameManager(pfm);
		p->m_Crank1.SetFrameManager(pfm); //root component
		p->m_Crank2.SetFrameManager(pfm);
		p->m_Crank3.SetFrameManager(pfm);
		p->m_Piston.SetFrameManager(pfm);
		p->m_Cannon.SetFrameManager(pfm);
		p->lpfnExplode=PushExpl3;
		p->pfnCollide=PushCollideMetal;
		if (!p->m_Crank1.CreateComponent(0,360,g_imgCrank,24)) return E_FAIL;
		if (!p->m_Crank2.CreateComponent(0,360,g_imgCrank,24)) return E_FAIL;	
		if (!p->m_Crank3.CreateComponent(0,360,g_imgCrank1,24)) return E_FAIL;
		if (!p->m_Piston.CreateComponent(0,360,g_imgPiston,24)) return E_FAIL;
		if (!p->m_Cannon.CreateComponent(0,360,g_imgCannon,24)) return E_FAIL;

		//imposta i poligoni per il collision detection
		for (count=0;count<24;count++)
		{
			p->m_Crank1.SetBoundPoly(count,&m_bpArm1[count]);
			p->m_Crank2.SetBoundPoly(count,&m_bpArm1[count]);
			p->m_Crank3.SetBoundPoly(count,&m_bpArm2[count]);
			p->m_Piston.SetBoundPoly(count,&m_bpPiston[count]);
			p->m_Cannon.SetBoundPoly(count,&m_bpCannon[count]);
		}
		
		//aggiunge le coppie prismatiche per collegare gli elementi tra di loro
		p->m_Crank1.AddRotCoupling(14,14,0,360,1);
		p->m_Crank1.AddRotCoupling(137,14,0,360,1);
		p->m_Crank2.AddRotCoupling(14,14,0,360,1);
		p->m_Crank2.AddRotCoupling(137,14,0,360,1);
		//manovella con una coppia rotoidale e una prismatica
		p->m_Crank3.AddRotCoupling(14,14,0,360,1);
		p->m_Crank3.AddPrismCoupling(79,15,0,-40,0);
		//pistone: una coppia prismatica e una rotoidale
		p->m_Piston.AddPrismCoupling(0,9,0,-40,0);
		//accopiamento fisso
		p->m_Piston.AddFixCoupling(70,9);
		//cannone (elemento finale della catena cinematica)
		p->m_Cannon.AddFixCoupling(0,20);
		//punto di fuoco
		p->m_Cannon.AddFixCoupling(51,20);
		//collega gli elementi tra di loro
		p->m_Crank2.LinkToComponent(0,1,&p->m_Crank1);
		p->m_Crank3.LinkToComponent(0,1,&p->m_Crank2);
		p->m_Piston.LinkToComponent(0,1,&p->m_Crank3);
		p->m_Cannon.LinkToComponent(0,1,&p->m_Piston);
		p->SetCartridgeStack(&g_sEShell11); //proiettili rossi
		p->SetWeapon1(&g_sEShell12); //plasma
		
		break;

	default:
		
		return E_FAIL; //ti po non valido
		break;

	}

	return S_OK;

}


//crea uno stack di inum oggetti di tipo mecharm
//si ocupa anche di caricare le frames
HRESULT CreateMechArmStack(CObjectStack *pstk,int inum)
{

	CMechArm *p;
	pstk->Create((DWORD)inum);

	for (int count=0;count<inum;count++)
	{
		pstk->m_objStack[count]=new CMechArm;
		p=(CMechArm*)pstk->m_objStack[count];
		p->bActive=FALSE;
		p->SetFrameManager(&g_fm);	
		if (FAILED(CreateMechArm(p,&g_fm))) return E_FAIL;
	
	}

	return S_OK;

}



//----------------------------------- CMechArm -------------------------------------------


CMechArm::CMechArm(void)
{
	dwScore=5000;
	dwClass=CL_ENEMY;
	m_bConfigCompleted=FALSE;
	m_bAutoConfig=TRUE;
	lTargetX=0;
	lTargetY=0;
	this->Reset();					
}	

//imposta lconfigurazione successiva e altri parametri
void CMechArm::SetTargetConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston,int delta,int freq)
{
	iUpdateFreq=freq;
	SetNextConfig(lcrank1,lcrank2,lcrank3,lpiston);
	UpdateDeltas1(delta);
	g_cs.PlaySound(g_snRobotArm,0,0);
}

void CMechArm::SetRandom(BOOL val)
{
	m_bRandom=val;
}

BOOL CMechArm::Update(void)
{

	LONG xf,yf;
	int lambda;
	int ang;
	
	iStepUpdate++;
	   
	if (iStepUpdate>=iUpdateFreq)
	{	
		//esegue un passo di aggiornamento della configurazione
		m_bConfigCompleted=UpdateConfig1();
		ApplyCurrentConfig();

		iStepUpdate=0;	
		
		if (m_bConfigCompleted && m_bAutoConfig)
		{
			//va al passo successivo solo dopo aver raggiunto la configurazione			
			
			switch(iStepPath)
			{
			case 0:			
					
					iUpdateFreq=6;
					SetNextConfig(30,150,360,-40);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
			

				break;

			case 1:

				
					iStepPath++;
					iUpdateFreq=60;
					
			
				break;

				
			case 2:

			
					iStepPath++;
					iUpdateFreq=6;
					SetNextConfig(75,75,45,0);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
			

				break;		

			case 3:
			
				    
					iStepPath++;
					iUpdateFreq=9;

					if (m_bRandom)
					{
						SetNextConfig(g_fmath.RndFast(23)*15,g_fmath.RndFast(23)*15,g_fmath.RndFast(23)*15,-40);
					}
					else
					{
						SetNextConfig(345,285,255,-40);
					}
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
				

				break;		

			
			case 8:

				//punta il player
				ang=g_fmath.GetAngle(m_Crank1.x,m_Crank1.y,lTargetX,lTargetY);

				ang=((int)(ang/15))*15;

				SetNextConfig(ang,(g_fmath.RndFast(3)==1 ? 0:175),0,-20);
				
				iStepPath++;
				
				iUpdateFreq=9;
				
				UpdateDeltas1(15);

				g_cs.PlaySound(g_snRobotArm,0,0);			

				break;

					//fall-through
			case 10:
			case 4:

			
					iStepPath++;
					iUpdateFreq=80;
				

				break;

			case 5:

			
					iStepPath++;
					iUpdateFreq=4;
					SetNextConfig(315,180,45,30);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
				

				break;

			case 6:

			
					iStepPath++;
					iUpdateFreq=4;
					SetNextConfig(0,180,180,0);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
			

				break;

			case 7:

					
				
					iStepPath++;
					iUpdateFreq=4;

					if (m_bRandom)
					{
						SetNextConfig(g_fmath.RndFast(23)*15,g_fmath.RndFast(23)*15,g_fmath.RndFast(23)*15,-40);
					}
					else
					{
						SetNextConfig(720,120,60,0);
					}
					
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);					
				
				break;

			case 9:

				
					iStepPath++;
					iUpdateFreq=4;
					SetNextConfig(180,345,345,0);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
				

				break;

			case 11:

				
					iStepPath=1;
					iUpdateFreq=4;
					SetNextConfig(720,345,345,0);
					UpdateDeltas1(15);
					g_cs.PlaySound(g_snRobotArm,0,0);
			

				break;

			case -1:

				break;
			}

			iStepPath++;


		}//fine config completed

	}	


	//rinculo del cannone
	int lmb=m_Piston.GetLambda();

	if (m_ibkstep>0)
	{
		m_ibkstep--;
		m_Piston.SetLambda(lmb+m_ibktr);
		
	}

	else
	{
		if (m_ibktr<0)
		{
			m_ibktr=1;
			m_ibkstep=20;
		}
	}	
	
	iStepFire++;

	if (iStepFire==iFireFreq)
	{
		if (m_Cannon.m_iTag1>0)
		{
			iStepFire=0; 		

			//spara solo se il bersaglio � in posizione opportuna

			iStepWeapon1++;

			if (iStepWeapon1<4) iFireFreq=8;
			else {iStepWeapon1=0;iFireFreq=60;}
		
			if (m_Cartridges)
			{
				m_ibktr=-2;
				m_ibkstep=10;

				CShell *pShell;

				pShell=(CShell *)m_Cartridges->Pop();
				if (pShell)
				{
					g_cs.PlaySound(g_snBfire,0,0);	
					m_Cannon.GetJointPosition(&xf,&yf,1);
					lambda=m_Cannon.GetCurFrame();	
					pShell->SetCurFrame(lambda);						
					pShell->SetJointPosition(xf,yf,0);
					pShell->SetVelocity(lambda*15,10),
					pShell->dwClass=CL_ENFIRE;
					pShell->SetEnergy(1);
					pShell->dwPower=12;
					m_ActiveStack->Push(pShell);
				}	

			}
		}//m_Cannon.m_iTag1;
	}

	//funzione fuoco ausiliario
	else if (iStepFire>iFireFreq-12)
	{
		if (m_Cannon.m_iTag1>0) 
		{
			m_Cannon.GetJointPosition(&xf,&yf,1);
			
			lambda=m_Cannon.GetCurFrame()*15;	
			
			int ang=g_fmath.GetAngle(xf,yf,lTargetX,lTargetY);
			
			ang=lambda-ang;

			//se il player � davanti entro un certo angolo) fa fuoco
			if (ang<45 && ang>-45)
			{
				if (m_pWeapon1)
				{
					CSBObject *pobj;

					//estrae un proiettile ausiliario dallo stack
					pobj=(CSBObject *)m_pWeapon1->Pop();

					if (pobj)
					{				
						g_cs.PlaySound(g_snMagnetic,0,0);
						pobj->Reset();
						pobj->SetCurFrame(0);			
						//posiziona il proiettile sulla bocca di fuoco 
						pobj->SetJointPosition(xf,yf,0);
						//nota: la velocit� va impostata dopo per permattere il cacolo corretto della traiettoria
						pobj->SetVelocity(lambda,6);
						pobj->dwClass=CL_ENFIRE;
						pobj->SetEnergy(8);
						pobj->dwPower=20;
						m_ActiveStack->Push(pobj);
					}

				}
			}
		}//m_Cannon.m_iTag1
	}

	//controlla in modo gerarchico l'energia di ogni elemento
	if (m_Cannon.m_iTag1>0) return TRUE;
	if (m_Piston.m_iTag1>0) return TRUE;
	if (m_Crank3.m_iTag1>0) return TRUE;
	if (m_Crank2.m_iTag1>0) return TRUE;
	if (m_Crank1.m_iTag1>0) return TRUE;
	return FALSE;
}


BOOL CMechArm::IsConfigCompleted(void)
{
	return m_bConfigCompleted;
}

void CMechArm::Reset(void)
{
	//imposta l'energia per ogni elemento del braccio
	m_Crank1.m_iTag1=30;
	m_Crank2.m_iTag1=30;
	m_Crank3.m_iTag1=30;
	m_Piston.m_iTag1=30;
	m_Cannon.m_iTag1=30;
	//braccio tutto piegato
	SetCurrentConfig(0,180,180,-40);
	SetNextConfig(0,180,180,-40);
	UpdateDeltas1();
	ApplyCurrentConfig();
	iStepUpdate=0;
	iUpdateFreq=420;
	iStepPath=0;
	iStepFire=0;
	iStepWeapon1=0;
	iFireFreq=120;
	m_ibktr=0;
	dwPower=10;
	lTargetX=0;
	lTargetY=0;
	
}

//disegna il braccio meccanico
HRESULT CMechArm::Draw(void)
{
	//per ogni elemento controlla che non sia stato distrutto
	//se un elemento � distrutto non disegna neanche gli elementi figli nella catena cinematica
	if (m_Crank1.m_iTag1>0)
	{		
		if (m_Crank2.m_iTag1>0)
		{		
			if (m_Crank3.m_iTag1>0)
			{			
				if (m_Piston.m_iTag1>0)
				{				
					if (m_Cannon.m_iTag1>0)
					{
						m_Cannon.SetDisplay(m_pimgOut);
						m_Cannon.RenderToDisplay();
					}

					m_Piston.SetDisplay(m_pimgOut);
    				m_Piston.RenderToDisplay();

				}

				m_Crank3.SetDisplay(m_pimgOut);
				m_Crank3.RenderToDisplay();

			}
			
			m_Crank2.SetDisplay(m_pimgOut);
    		m_Crank2.RenderToDisplay();

		}

		m_Crank1.SetDisplay(m_pimgOut);
		m_Crank1.RenderToDisplay();
	}

	return S_OK;
}

//se si imposta su FALSE il braccio meccanico non cambia configurazione automaticamente
void CMechArm::SetAutoConfig(BOOL bVal)
{
	m_bAutoConfig=bVal;
}

BOOL CMechArm::DoCollisionWith(CSBObject *pobj)
{
	//esegue una collisione considerando il bounding polygon di m_Crank1
	if (m_Crank1.m_iTag1>0)
	{
		if (m_Crank1.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Crank1,pobj);
			return TRUE;
		}
	}

	if (m_Crank2.m_iTag1>0)
	{
		//esegue una collisione considerando il bounding polygon di m_Crank2
		if (m_Crank2.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Crank2,pobj);
			return TRUE;
		}
	}

	if (m_Crank3.m_iTag1>0)
	{
		//esegue una collisione considerando il bounding polygon di m_Crank3
		if (m_Crank3.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Crank3,pobj);
			return TRUE;
		}
	}

	if (m_Piston.m_iTag1>0)
	{
		if (m_Piston.DetectCollision(pobj)) 
		{
			DamageComponent(&m_Piston,pobj);
			return TRUE;
		}
	}

	if (m_Cannon.m_iTag1>0)
	{
		if (m_Cannon.DetectCollision(pobj))
		{
			DamageComponent(&m_Cannon,pobj);
			return TRUE;
		}
	}


	return FALSE;
}

//esegue il danneggiamento su una parte del braccio meccanico
/*void CMechArm::DamageComponent(CMechComponent *pmc,CSBObject *pobj)
{
	pmc->m_iTag1-=pobj->dwPower; //toglie energia al componente colpito
	pobj->dwEnergy -= this->dwPower;

	if (pmc->m_iTag1<0)
	{
		ExplodeChain(pmc); //fa esplodere tutti gli oggeti figli compreo l'oggetto colpito
	}

	if (pobj->dwEnergy<0) pobj->Explode();
	if (pfnCollide) pfnCollide(x,y); //effetto collisione
}*/

//fa esplodere il braccio
void CMechArm::Explode(void)
{
	if (lpfnExplode)
	{
		lpfnExplode(m_Cannon.x,m_Cannon.y);
		lpfnExplode(m_Piston.x,m_Piston.y);
		lpfnExplode(m_Crank3.x,m_Crank3.y);
		lpfnExplode(m_Crank2.x,m_Crank2.y);
		lpfnExplode(m_Crank1.x,m_Crank1.y);
	}

	m_Cannon.m_iTag1=m_Piston.m_iTag1=m_Crank3.m_iTag1=m_Crank2.m_iTag1=m_Crank1.m_iTag1=0;
}


//imposta l configurazione alla quale si vuol arrivare
void CMechArm::SetNextConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston)
{
	m_fNextConfiguration[0]=(float)lcrank1;
	m_fNextConfiguration[1]=(float)lcrank2;
	m_fNextConfiguration[2]=(float)lcrank3;
	m_fNextConfiguration[3]=(float)lpiston;
}

//imposta la configurazione corrente
void CMechArm::SetCurrentConfig(int lcrank1,int lcrank2,int lcrank3,int lpiston)
{
	m_fCurrentConfiguration[0]=(float)lcrank1;
	m_fCurrentConfiguration[1]=(float)lcrank2;
	m_fCurrentConfiguration[2]=(float)lcrank3;
	m_fCurrentConfiguration[3]=(float)lpiston;
}

//aggiorna i delta
//isteps=numero di cicli che si vogliono impiegare per raggiungere la configurazione successiva
void CMechArm::UpdateDeltas(int isteps)
{
	if (isteps<=0) isteps=1;

	m_fConfigDeltas[0]=(m_fNextConfiguration[0]-m_fCurrentConfiguration[0])/isteps;
	m_fConfigDeltas[1]=(m_fNextConfiguration[1]-m_fCurrentConfiguration[1])/isteps;
	m_fConfigDeltas[2]=(m_fNextConfiguration[2]-m_fCurrentConfiguration[2])/isteps;
	m_fConfigDeltas[3]=(m_fNextConfiguration[3]-m_fCurrentConfiguration[3])/isteps;
	//imposta il numero di passi
	iConfigFreq=isteps;
	iStepConfig=0;
}

//aggirna i delta (delta costante)
//isteps � usato come limite se non si riesce a raggiungere la posizione di equilibrio
void CMechArm::UpdateDeltas1(int delta,int isteps)
{
	delta=(int)fabs(delta);
	iConfigFreq=isteps;
	iStepConfig=0;
	float diff;

	diff=m_fNextConfiguration[0]-m_fCurrentConfiguration[0];
	if (fabs(diff)<5) m_fConfigDeltas[0]=0;
	else m_fConfigDeltas[0]=diff>0 ? (float)delta : -(float)delta;

	diff=m_fNextConfiguration[1]-m_fCurrentConfiguration[1];
	if (fabs(diff)<5) m_fConfigDeltas[1]=0;
	else m_fConfigDeltas[1]=diff>0 ? (float)delta : -(float)delta;
    
	diff=m_fNextConfiguration[2]-m_fCurrentConfiguration[2];
	if (fabs(diff)<5) m_fConfigDeltas[2]=0;
	else m_fConfigDeltas[2]=diff>0 ? (float)delta : -(float)delta;
    //pistone
	diff=m_fNextConfiguration[3]-m_fCurrentConfiguration[3];
	if (fabs(diff)<1) m_fConfigDeltas[3]=0;
	else m_fConfigDeltas[3]=diff>0 ? (float)1 : -(float)1;

    
}

//acquisisce e salva la configurazione corrente
void CMechArm::StoreCurrentConfig(void)
{
	m_fCurrentConfiguration[0]=(float)m_Crank1.GetLambda();
   	m_fCurrentConfiguration[1]=(float)m_Crank2.GetLambda();
	m_fCurrentConfiguration[2]=(float)m_Crank3.GetLambda();
	m_fCurrentConfiguration[3]=(float)m_Piston.GetLambda();

}

//varia la configurazione, se ha raggiunto la configurazione finale rende true altrimenti false
BOOL CMechArm::UpdateConfig(void)
{
	if (iStepConfig>=iConfigFreq) 
	{
		m_fConfigDeltas[0]=m_fConfigDeltas[1]=m_fConfigDeltas[2]=m_fConfigDeltas[3]=0;
		return TRUE;	
	}

	iStepConfig++;
	m_fCurrentConfiguration[0] += m_fConfigDeltas[0];	
	m_fCurrentConfiguration[1] += m_fConfigDeltas[1];
	m_fCurrentConfiguration[2] += m_fConfigDeltas[2];
	m_fCurrentConfiguration[3] += m_fConfigDeltas[3];

	return (iStepConfig==iConfigFreq);
}

//� come Update config solo che quando un elemento raggionge la posizione finale 
//non si sposta piu' 
BOOL CMechArm::UpdateConfig1(void)
{
	bool bcompleted=true;

	iStepConfig++;

	if (iStepConfig>=30)
	{
		//evita loop infiniti dovuti ad errori di impostazione di configurazione (es. angoli non divisibili per delta)
		iStepConfig=0;
		return true;
	}

	m_fCurrentConfiguration[0] += m_fConfigDeltas[0];	
	m_fCurrentConfiguration[1] += m_fConfigDeltas[1];
	m_fCurrentConfiguration[2] += m_fConfigDeltas[2];
	m_fCurrentConfiguration[3] += m_fConfigDeltas[3];

	if (fabs(m_fCurrentConfiguration[0]-m_fNextConfiguration[0])<2) m_fConfigDeltas[0]=0;
	else bcompleted=false;

	if (fabs(m_fCurrentConfiguration[1]-m_fNextConfiguration[1])<2) m_fConfigDeltas[1]=0;
	else bcompleted=false;

	if (fabs(m_fCurrentConfiguration[2]-m_fNextConfiguration[2])<2) m_fConfigDeltas[2]=0;
	else bcompleted=false;

	if (fabs(m_fCurrentConfiguration[3]-m_fNextConfiguration[3])<2) m_fConfigDeltas[3]=0;
	else bcompleted=false;

	return (bcompleted); //true se � stata raggiunta la configurazione finale	

}


void CMechArm::ApplyCurrentConfig(void)
{
	m_Crank1.SetLambda((int)m_fCurrentConfiguration[0]);
	m_Crank2.SetLambda((int)m_fCurrentConfiguration[1]);
	m_Crank3.SetLambda((int)m_fCurrentConfiguration[2]);
	m_Piston.SetLambda((int)m_fCurrentConfiguration[3]);
}
