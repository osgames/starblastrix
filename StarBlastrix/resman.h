//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  resman.h -- resource manager, funzioni per caricamento risorse
///////////////////////////////////////////////////////////////////////*/

#ifndef _RESMANH_INCLUDE_
#define _RESMANH_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include <tchar.h>

class CResourceManager:public CADXBase
{
private:


protected:

TCHAR *g_szAuxResFile; //file che contiene la risorsa
IMAGE_FRAME g_imgAuxImg;    //immagine temporanea che contiene la risorsa grafica ausiliaria da caricare
DWORD g_dwCurVpxFrame;      //frame corrente del file vpx
TCHAR *tmp;

public:

CResourceManager(void);
~CResourceManager(void);
HRESULT LoadAuxResFile(TCHAR *szFileName,DWORD dwFrame); //carica in memoria il file di risorse (file di immagini vpx)
//carica un vettore di frame (una striscia orizzontale di frames dall'immagine caricata)
HRESULT GrabFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
//carica un vettore di frame da una striscia verticale di immagini
HRESULT GrabVFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
//restituisce un iferimento all'immagine caricata
IMAGE_FRAME_PTR GetCurrentImage(void);
void FreeResources(void); //rilascia le immagini temporanee
TCHAR *GetCurrentFileName(void);
DWORD GetCurrentImageIndex(void); //carica l'indice dell'immagine corrente

};


#endif
