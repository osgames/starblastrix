//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  grndauxen.cpp -- nemici a terra ausiliari
///////////////////////////////////////////////////////////////////////*/


#include "sbengine.h"
#include "grndauxen.h"
#include "resman.h"


extern CObjectStack g_objStack;
extern CADXFastMath g_fmath;
extern CADXFrameManager g_fm;
extern CADXGraphicManager g_gm;
extern CADXSound g_cs;
extern CObjectStack g_sEShell7;
extern CObjectStack g_sEShell11;
extern CObjectStack g_sEShell5;
extern CObjectStack g_sEShell3;
extern CObjectStack g_sSmokeSrc;
extern LONG g_snFire3;
extern LONG g_snFire6;
extern LONG g_snMech1;

BOOL g_bInitAuxEn=FALSE;
IMAGE_FRAME m_imgShieldBase[6]; //base con cupola
IMAGE_FRAME m_imgShellBase[7]; //base con portelli laterali
IMAGE_FRAME m_imgLaserTurret[11];
IMAGE_FRAME m_imgYellowTank[3]; //build2
IMAGE_FRAME m_imgSpacePortal[2]; //build1
IMAGE_FRAME m_imgTSilos[3]; //build3
IMAGE_FRAME m_imgHSilos[2]; //build4
IMAGE_FRAME m_imgHTruss;
IMAGE_FRAME m_imgVTruss;

void FreeGrndAuxRes(void)
{
	if (g_bInitAuxEn)
	{
		g_fm.FreeFrameArray(m_imgShieldBase,6);
		g_fm.FreeFrameArray(m_imgShellBase,7);
		g_fm.FreeFrameArray(m_imgLaserTurret,11);
		g_fm.FreeFrameArray(m_imgYellowTank,3);
		g_fm.FreeFrameArray(m_imgSpacePortal,2);
		g_fm.FreeFrameArray(m_imgTSilos,3);
		g_fm.FreeFrameArray(m_imgHSilos,2);
		g_fm.FreeImgFrame(&m_imgHTruss);
		g_fm.FreeImgFrame(&m_imgVTruss);
		g_bInitAuxEn=FALSE;
	}
}

HRESULT LoadGrndAuxEnRes(int resid,CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg=NULL;

	if (!g_bInitAuxEn)
	{
		size_t sz=sizeof(IMAGE_FRAME);
		memset(m_imgShieldBase,0,sz*6);
		memset(m_imgShellBase,0,sz*7);
		memset(m_imgLaserTurret,0,sz*11);
		memset(m_imgYellowTank,0,sz*3);
		memset(m_imgSpacePortal,0,sz*2);
		memset(m_imgTSilos,0,sz*3);
		memset(m_imgHSilos,0,sz*2);
		memset(&m_imgHTruss,0,sz);
		memset(&m_imgVTruss,0,sz);

		g_bInitAuxEn=TRUE;
	}

	if (!(3==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data6.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data6.vpx"),3))) return E_FAIL;
	}

	pimg=prm->GetCurrentImage();

	switch(resid)
	{
	case EN_SHIELD_BASE:

		if (m_imgShieldBase[0].status>0) return S_OK; //gi� caricata

		if (FAILED(g_fm.GrabFrameArray(m_imgShieldBase,pimg,6,0,0,164,82))) return E_FAIL;

		break;

	case EN_SHELL_BASE:

		if (m_imgShellBase[0].status>0) return S_OK;

		if (FAILED(g_fm.GrabVFrameArray(m_imgShellBase,pimg,7,780,92,130,92))) return E_FAIL;
		
		break;

	case EN_TURRET:

		if (m_imgLaserTurret[0].status>0) return S_OK;

		if (FAILED(g_fm.GrabFrameArray(m_imgLaserTurret,pimg,7,0,560,110,112))) return E_FAIL;
		if (FAILED(g_fm.GrabFrameArray(&m_imgLaserTurret[7],pimg,4,0,672,110,112))) return E_FAIL;

		break;


	case EN_SPACE_BUILD1:

		if (m_imgSpacePortal[0].status>0) return S_OK;

		if (FAILED(g_fm.GrabFrame(&m_imgSpacePortal[0],0,171,0+80,171+85,pimg))) return E_FAIL;
		if (FAILED(g_fm.GrabFrame(&m_imgSpacePortal[1],0,268,0+80,268+85,pimg))) return E_FAIL;


		break;

	case EN_SPACE_BUILD2:

		if (m_imgYellowTank[0].status>0) return S_OK;
		
		if (FAILED(g_fm.GrabFrameArray(m_imgYellowTank,pimg,3,114,148,114,74))) return E_FAIL;


		break;

	case EN_SPACE_BUILD3:

		if (m_imgTSilos[0].status>0) return S_OK;

		if (FAILED(g_fm.GrabFrame(&m_imgTSilos[0],114,245,114+161,245+126,pimg))) return E_FAIL;
		if (FAILED(g_fm.GrabFrame(&m_imgTSilos[1],294,245,294+161,245+126,pimg))) return E_FAIL;
		if (FAILED(g_fm.GrabFrame(&m_imgTSilos[2],480,245,480+161,245+126,pimg))) return E_FAIL;
				
		break;

	case EN_SPACE_BUILD4:

		if (m_imgHSilos[0].status>0) return S_OK;
		
		if (FAILED(g_fm.GrabFrame(&m_imgHSilos[0],3,404,3+172,404+82,pimg))) return E_FAIL;
		if (FAILED(g_fm.GrabFrame(&m_imgHSilos[1],182,404,182+172,404+82,pimg))) return E_FAIL;


		break;

	case EN_HTRUSS:

		if (m_imgHTruss.status>0) return S_OK;

		if (FAILED(g_fm.GrabFrame(&m_imgHTruss,437,401,527,447,pimg))) return E_FAIL;
		break;

	case EN_VTRUSS:

		if (m_imgVTruss.status>0) return S_OK;

		if (FAILED(g_fm.GrabFrame(&m_imgVTruss,381,401,427,491,pimg))) return E_FAIL;
		break;

	default:

		return E_FAIL; //risorsa non valida
	}

	return S_OK;

}

HRESULT CreateGrndStack(CObjectStack *pstack,int numitems,IMAGE_FRAME_PTR pimgFrames,int numframes,int energy,int power,int bonus,void ( *fnExpl)(LONG,LONG),CObjectStack *pCartridges)
{
	int count,count1;
	if (!pstack) return E_FAIL;
	CGroundObject* pobj=NULL;

	pstack->Create(numitems);

	for (count=0;count<numitems;count++)
	{
		pstack->m_objStack[count]=new CGroundObject();
		pobj=(CGroundObject *)pstack->m_objStack[count];
		pobj->bActive=FALSE;
		pobj->SetGraphicManager(&g_gm);
		pobj->SetFrameManager(&g_fm);
		for (count1=0;count1<numframes;count1++)
		{
			pobj->AddFrame(&pimgFrames[count1]);
		}
		pobj->SetInitialEnergy(energy);
		pobj->dwPower=power;
		pobj->SetCurFrame(0);
		pobj->SetVelocity(0,0);
		pobj->lpfnExplode=fnExpl;
		pobj->SetCartridgeStack(pCartridges);
		pobj->SetActiveStack(&g_objStack);		
		pobj->m_bDontRemove=TRUE;
	}

	//pLastStack=pstack;

	return S_OK;



}

void AddWoundedStateStack(CObjectStack *pstack,DWORD energy,DWORD frame)
{
	int len=pstack->ItemCount();
	CEnemy *p=NULL;

	for (int i=0;i<len;i++) 
	{
		p=(CEnemy *)pstack->m_objStack[i];

		p->AddWoundedState(energy,frame);
	}

}


HRESULT CreateGrndAuxStack(int resid,int numitems,CObjectStack *pstack,CResourceManager* prm,int flags)
{
	int count,count1;

	if (!pstack) return E_FAIL;
	if (!prm) return E_FAIL;
	CGroundObject *pgrnd=NULL;

	if (FAILED(LoadGrndAuxEnRes(resid,prm))) return E_FAIL;

	switch(resid)
	{

    //cupola viola
	case EN_SHIELD_BASE:		
		{
			CShieldBase* psb=NULL;

			pstack->Create(numitems);

			for (count=0;count<(int)pstack->ItemCount();count++)
			{
				pstack->m_objStack[count]=new CShieldBase();
				psb=(CShieldBase *) pstack->m_objStack[count];
				psb->dwResId=EN_SHIELD_BASE;
				psb->bActive=FALSE;
				psb->SetGraphicManager(&g_gm);
				psb->SetFrameManager(&g_fm);
				psb->SetShotSound(g_snFire3);
				psb->lpfnExplode=PushExpl9;
				psb->SetActiveStack(&g_objStack);
				
				for (count1=0;count1<6;count1++)
				{
					psb->AddFrame(&m_imgShieldBase[count1]);
					//bocca del cannone
					psb->SetFrameHotSpot(count1,0,82,12);

				}

				//le frame utili sono da 0 a 4 la 5 � esplosa
				psb->SetFrameRange(0,4); 
				psb->SetInitialEnergy(100);
				psb->AddWoundedState(15,5);
				psb->SetCartridgeStack(&g_sEShell7);
				psb->m_bDontRemove=TRUE; //non rimuove dopo averlo distrutto
				psb->SetSmokeStack(&g_sSmokeSrc);

			}

		}

		break;

	case EN_SHELL_BASE:

		{

		CShieldBase* psb=NULL;

		pstack->Create(numitems);

		for (count=0;count<(int)pstack->ItemCount();count++)
		{
				pstack->m_objStack[count]=new CShieldBase();
				psb=(CShieldBase *) pstack->m_objStack[count];
				psb->bActive=FALSE;
				psb->dwResId=EN_SHELL_BASE;
				psb->SetGraphicManager(&g_gm);
				psb->SetFrameManager(&g_fm);
				psb->lpfnExplode=PushExpl9;
				psb->SetActiveStack(&g_objStack);
				
				for (count1=0;count1<7;count1++)
				{
					psb->AddFrame(&m_imgShellBase[count1]);
					//bocca del cannone 1
					psb->SetFrameHotSpot(count1,0,53,13);
					//bocca del cannone 2
    				psb->SetFrameHotSpot(count1,1,80,13);

				}

				//le frame utili sono da 0 a 4 la 5 � esplosa
				psb->SetFrameRange(0,5); 
				psb->SetInitialEnergy(100);
				psb->AddWoundedState(15,6);
				psb->SetShotSound(g_snFire6);
				psb->SetDoorSound(g_snMech1);
				psb->SetCartridgeStack(&g_sEShell3);
				psb->m_bDontRemove=TRUE; //non rimuove dopo averlo distrutto
				psb->SetSmokeStack(&g_sSmokeSrc);

			}

		}

	break;

	case EN_TURRET:
		{
			CAntiAircraft2* pac=NULL;
			
			pstack->Create(numitems);
		
			for (count=0;count<(int)pstack->ItemCount();count++)
			{
				pstack->m_objStack[count]=new CAntiAircraft2();
				pac=(CAntiAircraft2 *) pstack->m_objStack[count];
				pac->dwResId=EN_TURRET;
				pac->bActive=FALSE;
				pac->SetGraphicManager(&g_gm);
				pac->SetFrameManager(&g_fm);
				pac->lpfnExplode=PushExpl9;
				pac->SetActiveStack(&g_objStack);
				pac->iStartFireFreq=65;
				pac->SetToleranceAngle(10);	
				pac->m_pfm =&g_fmath;
				pac->AddFrame(&m_imgLaserTurret[9]);
				pac->AddFrame(&m_imgLaserTurret[10]);
				pac->m_sprCannon.SetFrameManager(&g_fm);
		     	pac->m_sprCannon.SetBlitEffects(DDBLT_WAIT | DDBLT_KEYSRC);
				pac->SetRotJoint(56,106);
							
				for (count1=0;count1<=8;count1++)
				{
					pac->m_sprCannon.AddFrame(&m_imgLaserTurret[8-count1]);
					//punto di attacco della base
					pac->m_sprCannon.SetFrameHotSpot(count1,0,56,106);
					//imposta le coordinate della bocca del cannone									

				}

				pac->AddFrame(1,&m_imgLaserTurret[9]); //frame con danneggiamento
				pac->AddFrame(2,&m_imgLaserTurret[10]);

				pac->m_sprCannon.SetFrameHotSpot(8,1,5,33);
				pac->m_sprCannon.SetFrameHotSpot(7,1,15,20);
				pac->m_sprCannon.SetFrameHotSpot(6,1,27,11);		
				pac->m_sprCannon.SetFrameHotSpot(5,1,39,6);
				pac->m_sprCannon.SetFrameHotSpot(4,1,56,2);
				pac->m_sprCannon.SetFrameHotSpot(3,1,70,6);
				pac->m_sprCannon.SetFrameHotSpot(2,1,83,12);
				pac->m_sprCannon.SetFrameHotSpot(1,1,94,21);	
				pac->m_sprCannon.SetFrameHotSpot(0,1,104,33);

				//le frame utili sono da 0 a 4 la 5 � esplosa
				//pac->SetFrameRange(0,5); 
				pac->SetInitialEnergy(100);
				pac->AddWoundedState(20,1);
				pac->AddWoundedState(8,2);
				pac->SetBaseCannonFrame(4);
				pac->SetCurFrame(4);
				//psb->SetShotSound(g_snFire6);
				pac->SetCartridgeStack(&g_sEShell11);
				pac->SetWeapon1(&g_sEShell3);
				pac->DontDrawBottom(FALSE);
				pac->m_bDontRemove=TRUE; //non rimuove dopo averlo distrutto
				pac->SetSmokeStack(NULL);
			}
		

		}

		break;

		//portale
	case EN_SPACE_BUILD1:

		CreateGrndStack(pstack,numitems,m_imgSpacePortal,2,55,8,0,PushExpl9,0);

		AddWoundedStateStack(pstack,25,1);
	
	
		break;

		//silos giallo
	case EN_SPACE_BUILD2:

		CreateGrndStack(pstack,numitems,m_imgYellowTank,3,35,8,0,PushExpl5,0); //3
		
		AddWoundedStateStack(pstack,21,1);

		AddWoundedStateStack(pstack,10,2);

		
		break;

		//triplo silos
	case EN_SPACE_BUILD3:

		CreateGrndStack(pstack,numitems,m_imgTSilos,3,65,8,0,PushExpl5,0); //3
		
		AddWoundedStateStack(pstack,25,1);

		AddWoundedStateStack(pstack,12,2);		

	
		break;

	case EN_SPACE_BUILD4:

		CreateGrndStack(pstack,numitems,m_imgHSilos,2,45,8,0,PushExpl9,0); //3
		
		AddWoundedStateStack(pstack,21,1);		
		
		break;

	case EN_HTRUSS:

		CreateGrndStack(pstack,numitems,&m_imgHTruss,1,10,10,0,PushExpl9,0);

		break;

	case EN_VTRUSS:

		CreateGrndStack(pstack,numitems,&m_imgVTruss,1,10,10,0,PushExpl9,0);

		break;

	default:

		return E_FAIL;

	}

	return S_OK;
}
				

///////////////////////////// CShieldBase /////////////////////////////////////////////////////////
/*

  Base a terra che si apre e si chiude

*/

CShieldBase::CShieldBase(void)
{
	m_ifstart=m_ifend=0;
	iUpdateFreq=180;
	m_pSmoke=NULL;
	m_shot_sound=0;
	m_door_sound=0;
}

void CShieldBase::Reset(void)
{
	CGroundObject::Reset();
	//base attualmente chiusa
	m_state=bs_closed;
	SetCurFrame(m_ifstart);	
	iUpdateFreq=10;
	
}

void CShieldBase::SetShotSound(LONG sound)
{
	if (sound>=0) m_shot_sound=sound;
	else m_shot_sound=0;

}

void CShieldBase::SetDoorSound(LONG sound)
{
	if (sound>0) m_door_sound=sound;
	else m_door_sound=0;
}

HRESULT CShieldBase::SetFrameRange(int fstart,int fend)
{
	if (fstart>=0 && fend>=fstart)
	{
		m_ifstart=fstart;
		m_ifend=fend;
		return S_OK;
	}

	else return E_FAIL;
}

BOOL CShieldBase::Update(void)
{
	BOOL bres=CGroundObject::Update();

	if (++iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;

		if (UpdateWoundedState() && m_pSmoke)
		{
			
			CSmoke* pSmk=(CSmoke *)m_pSmoke->Pop();
				
			if (pSmk)
			{
				pSmk->Reset();
				pSmk->SetPosition(x+40,y+20);
				pSmk->SetVelocity(180,CSbLevel::g_scroll_speed); //la sorgente si sposta da destra a sinistra
				//immette l'oggetto nello stack
				m_ActiveStack->Push(pSmk);
			}

		}

		if (iWoundedState==0)
		{

			switch(m_state)
			{
			case bs_closed:

				if (g_fmath.RndFast(15)==1)
				{
					//inizia ad aprirsi
					m_state=bs_opening;
					g_cs.PlaySound(m_door_sound,0,0);
				}

				break;

			case bs_open:

				if (g_fmath.RndFast(18)==1)
				{
					//inizia a chiudersi
					m_state=bs_closing;
				}

				break;

			case bs_opening:

				IncFrame();
				if (GetCurFrame()==m_ifend) m_state=bs_open;

				break;

			case bs_closing:
				
				DecFrame();
				if (GetCurFrame()==m_ifstart) 
				{
					g_cs.PlaySound(m_door_sound,0,0);
					m_state=bs_closed;
				}

				break;
			}

		}
	}

	if (iStepFire++>=iFireFreq)
	{
		iStepFire=0;

		if (m_state==bs_open && m_Cartridges && m_ActiveStack && iWoundedState==0)
		{
			//puo' sparare solo se � aperta
			LONG fx=0,fy=0;
			int fr=GetCurFrame();
			int hs=0;

			GetFrameHotSpot(fr,hs,&fx,&fy);

			CSBObject *pshell=NULL;
			
			//ogni hot spot � una bocca di un cannone, ci possono essere piu' cannoni
			while (fx>0 && hs<MAX_FRAME_HOT_SPOT)
			{
				pshell=m_Cartridges->Pop();

				if (pshell)
				{

					pshell->dwClass=CL_ENFIRE;
					pshell->dwPower=8;
					pshell->SetEnergy(1);
					//direzione verticale
					pshell->SetVelocity(260,8);
					pshell->SetPosition(x+fx-6,y+fy-6);				
					//immette il proiettile nello stack
					m_ActiveStack->Push(pshell);
					g_cs.PlaySound(m_shot_sound,0,0);

				}

				++hs;

				GetFrameHotSpot(fr,hs,&fx,&fy);

			}

		}

	}

	//aggiorna le coordinate assolute
	GetAbsXY(&absx,&absy);

	return bres;
}


HRESULT CShieldBase::SetSmokeStack(CObjectStack *pStack) //imposta lo stack fumo
{
	if (pStack) 
	{
		m_pSmoke=pStack;
		return S_OK;
	}

	else return E_FAIL;
}


