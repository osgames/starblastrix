//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  enemy.h Nemico generico 
///////////////////////////////////////////////////////////////////////*/

#ifndef _SB_ENEMY_
#define _SB_ENEMY_

#include "a32object.h"

//tipi di oggetti
#define CL_NONE 0x0 //non specificato 
#define CL_PLAYER 0x1 //giocatore
#define CL_PLFIRE 0x2 //l'oggetto � un proiettile del giocatore
#define CL_ENFIRE 0x4 //fuoco nemico
#define CL_ENEMY 0x8 //nemico
#define CL_BONUS 0x10 //bonus
#define CL_FRIEND 0x20 //componente amico

//elemento di un path per il movimento di un oggetto
//un path � un vettore di elementi TP_PATH_DATA
typedef struct
{
	int Angle; //angolo
	int Displ; //spostamento
	int Velocity; //velocit�

} TP_PATH_DATA,*TP_PATH_DATA_PTR;


//----------------------------- Classe CEnemy -----------------------------------------------------
//Definisce un nemico generico dotato di armi e funzioni necessarie al puntamento del bersaglio

#define MAX_DLV 3
#define MAX_AUX_PATH 6

//Nemico generico
class CEnemy:public CSBObject
{
protected:

	int iStepPathNum;
	TP_PATH_DATA *pPath; //punta la posizione corrente del path
	TP_PATH_DATA *m_pAuxPath[MAX_AUX_PATH]; //path ausiliari impostabili tramite iFlags
	int m_iAuxSteps[MAX_AUX_PATH];
	TP_PATH_DATA *pFollowPath; //punt il path da seguire
	int iStepFire;	
	int iStepConfig; //quando iStepConfig raggiunge iConfigFreq � il momento di cambiare frame (o configurazione)
	CObjectStack *m_ActiveStack; //stack degli oggetti attivi 
	CObjectStack *m_Cartridges;  //stack proiettili
	CObjectStack *m_pWeapon1; //armi ausiliarie
	CObjectStack *m_pWeapon2; 
	DWORD GetTargetAngle(LONG x,LONG y); //restituisce l'angolo con il cui si deve sparare per colpire il giocatore	
	DWORD m_dwFlags; //opzioni
	void UpdateTrim(void); //aggiorna l'assetto
	BOOL SetNextPathStep(void); //passa al passo successivo del path cinematico
	int iFireFreq; //frequenza di fuoco
	int iStepPath; //contatore di aggiornamento path	
	int iStepUpdate; //contatore di aggiornamento direzione
	int iWoundedState; //1=ferito sta precipitando
	int iUpdateFreq;
	int iStartUpdateFreq; //frequenza di aggiornamento iniziale	
	int m_ivx0,m_ivy0; //velocit� iniziale moto parabolico
	float m_t;
	//stato di danneggiamento del nemico
	int m_idestlevel[MAX_DLV];
	int m_iframe[MAX_DLV];
	int m_icurlevel;
	int iStepWeapon1;
	BOOL m_bEscapingAlgo; //utilizza un algoritmo per evitare i proiettili del player
	BOOL m_bEscaping; //sta sfuggendo
	BOOL m_bBonusCreated;
	//acquisisce i parametri necessari per sfuggile al fuoco del player
	BOOL GetEscapingParms(int *angle,int *vel,int *steps);
	LONG m_lFireSound;
	LONG m_lFireSound1;

public:
	
	CEnemy(void);	
//	int iWounded; //limite di energia entro il quale il nemico � ferito ma non distrutto	
	int iBaseFrame; //indice della frame base (inclinazione 0)
	int iStartFireFreq; //frequenza di aggiornamento iniziale
	int iConfigFreq; //frequenza con la quale cambia frame o configurazione	
	CObjectStack *m_pBonusStack; //bonus contenuti nel nemico, se questo � NULL il nemico non contiene bonus
	HRESULT SetPath(TP_PATH_DATA *Path,DWORD dwNumSteps);
	HRESULT SetAuxPath(int index,TP_PATH_DATA *pPath,int steps); //imposta un path addizionale
    HRESULT SetActiveStack(CObjectStack *pStack); //imposta lo stack degli oggetti visibili, � in questo stack che deve immettere i proiettili (o altro)
	HRESULT SetCartridgeStack(CObjectStack *pStack); //stack dal quale preleva i proiettili
	HRESULT SetWeapon1(CObjectStack *pStack); //Eventuali stack con armi derivate
	HRESULT SetWeapon2(CObjectStack *pStack);
	//rumore fuoco primario
	void SetFireSound(LONG sound);
	//rumore fuoco secondario
	void SetFireSoundAux(LONG sound);
	//HRESULT SetSmokeStack(CObjectStack *pStack); 
	BOOL AddWoundedState(DWORD dwEnergyThreshold,DWORD dwFrame);
	int GetWoundedState(void); //restituisce il livello di danneggiamento corrente
	void SetUpdateFreq(int iFreq); //imposta la frequenza di aggiornamento
	void (*pfnSmoke)(LONG x,LONG y); //stack che contiene il fumo
	void (*pfnCollide)(LONG x,LONG y); //funzione di collisione
	void CreateBonus(void); //inserisce il bonus nello stack se � presente
	virtual BOOL Update(void); //IA
	BOOL UpdateWoundedState(void);
	virtual void Reset(void);
	virtual void SetFlags(DWORD dwFlags); //flags che impostano un coportamento opzionale
};

#endif