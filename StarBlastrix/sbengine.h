//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  Engine del gioco
///////////////////////////////////////////////////////////////////////*/

#ifndef _SBENGINE_INCLUDE_
#define _SBENGINE_INCLUDE_

//numero totali di livelli da completare per terminare il gioco
#define NUM_LEVELS 8

//opzioni per debug
//attenzione! in modalit� windowed e' conisgliabile avere un bpp non superiore a 16
//altrimenti occorre troppa memoria per le superfici
//nota: il flag DEBUG puo' essere usato solo se la configurazione 
//dell'applicazione � "Debug"
//#define _DEBUG_      //Il programma � in modalit� debug e gira windowed
//#define _TESTGAME_
//#define _MIDI_OFF_   //disabilita la musica MIDI
//#define _WAV_OFF_    //disabilita gli effetti sonori 
//#define _LEVELEDIT_    //modalit� level editor
//#define _TRACE_


#ifdef _LEVELEDIT_
//la munsica non serve nell'editor
#define _MIDI_OFF_
#endif

#define MAX_FRAMES_PER_LAYER 4
#define SCREEN_WIDTH 640 //512 //risoluzione usata
#define SCREEN_HEIGHT 480 //384

#define MAX_PLAYER_SHELLS 7 //numero di tipi di arma primaria del giocatore

//tipo di input
#define EN_KEYBOARD 0
#define EN_MOUSE 1
#define EN_JOYSTICK 2

//tipi di proiettile disponibili
#define SH_SHELL1 0x1
#define SH_SHELL2 0x2
#define SH_SHELL3 0x3
#define SH_SHELLRED 0x4
#define SH_SHELLBALL 0x5

#include "a32graph.h" //libreria grafica 2D arcade 32 
#include "a32audio.h" //wrapper direct sound e direct music
#include "a32fastm.h" //libreria funzioni matematiche
#include "a32sprit.h" //classe sprite
#include "a32img16.h" //libreria per manipolazione buffer grafici a 16 bit
#include "a32object.h"
#include "ceffcts.h" //effetti grafici
#include "enemy.h"
#include "level.h"

//nota: la seguente definizione � stata necessaria per 
//compilare con l'sdk directx 9
//utilizza la versione 7 di direct input
#define DIRECTINPUT_VERSION 0x0700


#include "mmsystem.h"
#include "ccharset.h"
#include "sbutil.h"

//stato attuale del gioco
typedef enum GAME_STATUS_ENUM
{
	GM_CONSOLE=0,
	GM_GAMELOOP=1,       //loop del gioco 
	GM_SHUTDOWN=2,
	GM_INITIALIZE=3,
	GM_HEARTBEAT=4,      //ciclo base animazione
	GM_LEVELCOMPLETED=5, //livello completato (sequenza finale del livello)
	GM_GAMECOMPLETED=6,     //gioco completato
	GM_LEVELEDITOR=7,    //modalit� editor di livelli

} GAME_STATUS_TYPE;

//Stato del gioco
typedef struct 
{
	int iLevel;
	int iInput;
	BOOL bFullScreen;
	int iScreenWidth;  //larghezza schermo in pixel
	int iScreenHeight; //altezza schermo in pixel
	int iScreenBpp;          //bit per pixel
	BOOL bSound;       //sonoro abilitato
	BOOL bMusic;       //musica abilitata
	int iMoveLeft;     //tasto associato al movimento a sinistra (codice direct x del tasto)
    int iMoveRight;    //tasto associato al movimento a destra (codice direct x del tasto)
	int iMoveUp;
	int iMoveDown;
	int iFire1Key; //arma primaria
	int iFire2Key;
	int iFire3Key;
	GAME_STATUS_TYPE game_status;
	LONG dwLimit;
	int iLanguage;  //lingua 0=default 1=inglese
	LONG lCurrentScore; //punteggio corrente
	BOOL hasError; //errore critico (occorre avvertire all'uscita del gioco)

} TP_ENVIRON;


//Stato dell'input del giocatore
typedef struct 
{
	int x_axis; //asse x premuto
	int y_axis; //asse y
	int fire1; //fuoco primario
	int fire2; //fuoco secondario
	int fire3; //arma speciale
	
} PL_INPUT,*PL_INPUT_PTR;


//localization
typedef struct
{
	TCHAR *msg;
	TCHAR *msg1;

} LOCALE_TABLE,*LOCALE_TABLE_PTR;

//------------------------------------------------------------------------------------------------

#define MAX_SHOOT 60                   //numero massimo di proiettili che puo' contenere la coda del fuoco
#define MAX_SHELL_TYPE 20              //numero di tipi di proiettile
#define MIN_FIRE_DELAY 4               //delay minimo fuoco primario del player 


//------------------------------ Classe CAnimatedObject -------------------------------------
//Oggetto animato, cambia frame in modo ciclico
class CAnimatedObject:public CSBObject
{
private:

	int m_iTotalCycles; //numero di cicli prima di essere eliminato (se � zero continua all'infinito)
	int m_iCycleStep;	

public:

	CAnimatedObject(void);
	virtual void SetCycles(int icyc); 
	void Reset(void);
	BOOL Update(void);
};

//------------------------------ Classe CShell ------------------------------------------------
//proiettile che si muove in senso orizzontale
class CShell:public CSBObject
{
public: 

    CShell(void);
	BOOL Update(void);
};

//------------------------------------ CFwObject --------------------------------------------------
//oggetto inseguitore
class CFwBullet:public CEnemy
{
private:

	CSBObject *m_pTarget; //target da inseguire
	CSBObject *m_pNextTg[1];
	CObjectStack *m_pSmoke; //fumo
	int m_ittl;

public:

	CFwBullet(void);	
	virtual ~CFwBullet(void);
	void SetTarget(CSBObject *target);
	void SetSmoke(CObjectStack *smoke);
	void SetTTL(int ttl); //"time to live" 
	BOOL Update(void);
	void Reset(void);
};

//----------------------------------- Classe CLaser -----------------------------------------------

class CLaser:public CSBObject
{

private:

	int iUpdateStep;	
	LONG m_lOrigW;
	LONG m_lLaserLength;
//	BOUND_POLY m_bp;
//	VERTEX2D *m_pv;

public:

	int iLasts; //quanto dura
	CLaser(void);
	~CLaser(void);
	BOOL Update(void);
	void Reset(void);
	//imposta la larghezza originale della frame e la lunghezza del laser
	void SetProps(LONG orig_width,LONG length);
	HRESULT Draw(void);

};

//--------------------------------- Classe CExplosion ---------------------------------------------

//esplosione
class CExplosion:public CSBObject
{
private:

	int iStartTrigger;
	int iTrigger;
	int idelay; //cicli di ritaro prima di visualizzare la sequenza
	int isound_id; //id del suono dell'esplosione
	BOOL bstarted; //flag che indica che l'esplosione � stata avviata e il rumore � stato emesso

public:
	
	CExplosion(void);
	void SetTrigger(int iTrigger);	
	int GetDelay(void);
	void SetDelay(int delay);
	void Reset(int sound,int delay);
	HRESULT Draw(void);
	BOOL Update(void);
};

//------------------------------- Classe CDropBomb --------------------------------------------
//bomba a caduta

class CDropBomb:public CSBObject
{
private:

	int iChangeDir;
	int ig; //aumento della velocit� per via della forza di gravit�
	
public:	

	void Reset(void);
	CDropBomb(void);
	BOOL Update(void);
};

//------------------------------- Classe CFwMissile ----------------------------------------------
//missile inseguitore

class CFwMissile:public CSBObject
{
private:

	int iStep; //passo corrente dell'algoritmo
	int iUpdateStep;
	void RefreshParams(void); //aggiorna la direzione in modo da puntare sul bersaglio
	CSBObject *m_pTarget;
	CADXFastMath m_fn;

public:

	CFwMissile(void);  //costruttore
	int iStartFwStep; //passo in cui inizia a inseguire il bersaglio
	int iFinishFwStep; //passo in cui nn modifica piu' la sua traiettoria (smette di inseguire)
	int iUpdateFreq;
	HRESULT SetTarget(CSBObject *pTarget); //target del missile
	BOOL Update(void);
	void Reset(void);
};

//---------------------------------- classe CObjectStack ------------------------------------------

//numero massimo di oggetti nello stack attivo
//coincide con il numero max. di oggetti visualizzabili contemporaneamente
#define MAX_OBJECTS 150  

//----------------------------- Classe CBonus -----------------------------------------------------


//bonus:power up, punti ecc...
class CSbBonus:public CSBObject
{	
private:
	
	int iStepClock;
	int iStepUpdate;
	int iStepConfig;

public:
    	
//	int iBonus; //definisce il tipo di bonus //non usato
	BOOL Update(void);
	CSbBonus(void);
	void Reset(void);

};

//----------------------------- Classe CDynString -------------------------------------------------
//testo dinamico usato per visualizzare i bonus,informazioni durante il gioco ecc...
class CDynString:public CSBObject
{

private:
	
	int iStepUpdate;
	int iStatus;
	int iStepConfig;
	int iBlinkCnt;
	int iDelay; //persistenza in cicli
	CADXCharSet *m_pch;
	TCHAR *m_szText;

public:

	int iMode; //0=movimento verso l'alto,1=ferma 
	int iBlink; //iBlink=0 non lampeggiante iBlink>0 lampeggiante con ritardo che aumenta all'aumentare di iBlink
	BOOL Update(void);
	CDynString(void);
	virtual ~CDynString(void);
	HRESULT SetCharSet(CADXCharSet *pch);
	void SetText(TCHAR *txt); //nota: se viene impostato il testo e il char set esegue anche il blitting della stringa di testo
	TCHAR *GetText(void);
	void Reset();
	void SetDelay(int ival);
	int GetDelay(void);
	void FreeText(void);
	HRESULT Draw(void);

};

//----------------------------- Classe CGameOver --------------------------------------------------
//esegue la visualizzazione della etichetta game over
class CGameOver:public CSBObject
{
private:

	int m_xp[200];
	int m_yp[200];
	CADXFastMath m_math;
	int xcur;
	int m_imax;
	int iDelay; 
	int iDelayStep; //contatore che mantiene per un po' la scritta ferma al centro dello schermo
	int m_xout,m_yout; //coordinate di output dell'etichetta
	TCHAR *m_szText;
	IMAGE_FRAME m_img; //immagine su cui viene disegnata l'etichetta game over

public:

	CGameOver(void); //costruttore
	~CGameOver(void);
	CADXCharSet *pCharSet; //set di caratteri usati per scrivere "game over"
	BOOL *pQuitLoopFlag; //punta alla variabile che indica di terminare il loop del gioco 
	void Reset(void);	
	BOOL Update(void);
	HRESULT Draw(void);

};

//-------------------------------------- Classe CGroundObject --------------------------------------

//Generico oggetto che si muove rispetto al terreno
class CGroundObject:public CEnemy
{

private:

	int m_lW;

protected:

	LONG lCurX;

public:

	CGroundObject(void);	
	BOOL Update(void);
	void Reset(void);
	//flag che indica che quando ha raggiunto l'ultimo livello di danneggiamento
	//non deve essere rimosso dallo sack di oggetti attivi fino a che 
	//non esce dal video
	BOOL m_bDontRemove; 

};


//-------------------------------- Classe CUfo2 ------------------------------------------------------

class CUfo2:public CEnemy
{

private:

	int m_iAIAlgo;
	int m_iAlgoStep;
	int m_il1,m_il2,m_il3;
	int m_iCurAlgo;
	int m_iFreq; //limite contatore frequesza raffica (piu' � alto e minore � la frequenza)
	int m_iFreqCnt; //contatore frequenza
	int m_iShots;
	int m_iShotsCnt;

public:

	CADXFastMath *m_pfm;
	CUfo2(void);
	void SetAI(int iAlgo);
	BOOL Update(void);
    void Reset(void);
	//imposta il tipo di raffica iShots=numero di colpi iFreq=frequenza
    void SetBurst(int iShots,int iFreq);
	void SetFlags(DWORD dwFlags);

};

//--------------------------------------- Classe CUfo3 -----------------------------------------------

class CUfo3:public CEnemy
{

private:

	int m_iAIAlgo;
	int m_iNextAlgo;
	int m_iNextFreq;
	int m_iW2; //semilarghezza dell'ufo
	float m_fTgSpin; //velocit� di rotazione target
	float m_fSpinStep; //acc. di rotazione
	LONG m_lTgX,m_lTgY; //punto target da raggiungere
	void SetTgSpin(float fSpin); //imposta la velocit� di rotazione target (e il relativo step)


public:

	CUfo3(void);
	BOOL Update(void);
	void Reset(void);

};

//-------------------------------- Classe CGoldenFighter ---------------------------------------------
//Golden Fighter
class CGoldenFighter:public CEnemy
{
private:



public:
	
	CGoldenFighter(void);
	int IAType; //algoritmo IA
	int IAStep;
	BOOL Update(void);
	void Reset(void);
};

//----------------------------------- CScrewFighter --------------------------------------------------

class CScrewFighter:public CEnemy
{
private:

	int m_iStepDir;
	int m_iChgDir;
	

public:

	CScrewFighter(void);
	BOOL Update(void);
	void Reset(void);
};

//--------------------------------- Classe CShield ----------------------------------------

class CShield:public CEnemy
{
private:  

	LONG lradius;
	int iTotalStep;

public:    
	
	CShield(void);
	CADXFastMath *pfm;
	void Reset(void);
	BOOL Update(void);

};

//------------------------------------ Classe CBooster ---------------------------------------------
//booster: modifica la velocit� di scrolling del livello tramite rampe di accelerazione e decelerazione
class CBooster:public CSBObject
{
private:

	int m_iWidth,m_iDeltaSpeed,m_iLength; //spazio di accelerazione e dec. in pixel,variazione della velocit�,durata rampa in cicli
	float m_fCurSpeed; //vel. corrente
	float m_fAcc; //accelerazione
	LONG m_lx1,m_lx2; //x inizio rampa di acc. e inizio rampa di dec.
	int m_iUpdateStep;
	int m_iUpdateFreq;
	int m_iStartSpeed;
	int m_iStatus; //stato corrente


public:

	CBooster(void);
	BOOL Update(void);
	HRESULT SetRamp(int iUpdateFreq,int iDeltaSpeed,int iLength); 
	HRESULT Draw(void); //non disegna nulla (� fittizia)
	CSbLevel *m_pLv; //livello da accelerare
	void Reset(void);

};

//-------------------------------------Classe CGyro -------------------------------------------------
//fuoco amico
class CGyro:public CEnemy
{
private:

	LONG lradiusx;
	LONG lradiusy;
	int iTotalStep;
	int iAnglePhase;
	int iLastTarget;
	int iCannonTargetFrame;
	int iRepeatFire;
	int iFrmIncr;
	int cf,ang;
	int iax,iay;
	int iLstTgType;

protected:

	//restituisce il "valore" di un oggetto nemico (indica l'importanza del nemico)
	int GetObjectValue(CSBObject *pobj);	
	
public:

	CGyro(void);
	CADXFastMath *pfm;
	CSBObject Cannon;	
	BOOL Update(void);
	HRESULT Draw(void);
	void Reset(void);
};

//------------------------------------ Classe CDebris ----------------------------------------------
//detriti causati da un'esplosione
class CDebris:public CSBObject
{
private:
	
	int m_iConfigFreq; //frequenza aggiornamento frame
	int m_iConfigStep; //contatore aggiornamento frame
	int m_iMaxFrames; //indice della frame a cui si ferma (0=fa vedere tutte le frames)
	int m_idx,m_idy;
	float m_ft; //tempo
	float g2; //accelerazione di gravit� / 2

public:	

	CDebris(void);
	void SetG(float g); //accelerazione di gravit� per il moto del detrito
	void Reset(void);
	void SetConfigFreq(int freq);
	BOOL Update(void);	
	HRESULT SetMaxFrames(int iMax);
	HRESULT Draw(void);
};

//----------------------------------- Classe CMissile ----------------------------------------------

class CMissile:public CEnemy
{
private:
	
	int iIAStep;
	int iIAStart;
	int iSmokeStep;
	CObjectStack *m_pSmoke;
	CADXFastMath m_fm;

public:

	int mode; //0 va dritto ; 1=follow path 2=algoritmo ia
	CMissile(void);
	BOOL Update(void);
	void Reset(void);
	HRESULT SetSmokeStack(CObjectStack *pstack);

};

//-------------------------------- Classe CSnakeBody -----------------------------------------

class CSnakeBody:public CEnemy
{
private:

	CImgBuffer16 m_imgSrc16,m_imgBuff16;
	
public:

	int m_ixBound; //ascissa in corrispondenza della quale inizia a seguire il path
	WORD *pwDestBuffer; //buffer di destinazione
	CSBObject *m_pHead;
	LONG lDestW;
	LONG lDestH;
	LONG lDestPitch;
	BOOL bFollowPath;
	int m_iFrameStep; //contatore aggiornamento frame
	int m_iUpdateFrameFreq; //frequenza aggiornamento frame
	CSnakeBody(void);
	~CSnakeBody(void);
	BOOL Update(); 
	HRESULT Draw();
};


//-------------------------------- Classe CSnake ----------------------------------------------------
//Snake
class CSnake: public CEnemy
{
private:

	DWORD m_iNumElems; //numero di elementi esclusa la testa
	CSBObject *m_objBody[10];//elemnti del corpo del serpente	
	BOOL bInit; //viene settato su true quando i vari elementi del corpo sono stati immessi nello stack attivo
	int m_iUpdateFrameFreq; //freq. di aggirnamento frame
    int m_iFrameStep;
	IMAGE_FRAME_PTR pimg; //general purpose

	
public:

	CSnake(void);
	~CSnake(void);
	CObjectStack *pBodyStack; //stack dal quale preleva gli elementi del corpo
	BOOL Update(void);
	void Reset(void);
	void Explode(void);
	HRESULT Draw(void);//ridefinisce la funzione di disegno 
	BOOL DoCollisionWith(CSBObject *pobj);
	void SetFlags(DWORD dwFlags);
	
};

//--------------------------------- Classe CAntiAircraft ---------------------------------------------
//contraerea
class CAntiAircraft:public CEnemy
{
private:

	LONG lCurx; //serve per tener conto del moto di trascinamento dello schema
	int iFreq1;
	
public:
    
	CAntiAircraft(void);
	BOOL Update(void);
	void Reset(void);
	
};

//-------------------------------- Classe CRadar -------------------------------------------------
//radar
class CRadar:public CGroundObject
{
private:

	LONG lCurx;	

public:			

	CRadar(void);
	void Reset(void);
	BOOL Update(void);

};

//---------------------------------- Classe CSmoke -------------------------------------------------
//sorgente di fumo
class CSmoke:public CEnemy
{
private:
	
	int m_iangle; //angolo della velocit�
	int m_ivel; //velocit� delle nuvole di fumo
	CObjectStack *pSmokeSource; //stack sorgente del fumo

public:
	
	CSmoke(void);
	void Reset(void);
	void SetSmokeVelocity(int iangle,int vel);
	HRESULT SetSmokeStack(CObjectStack *pStack);
	BOOL Update(void);
	
};

//---------------------------------- Classe CLambda -------------------------------------------------
//Lambda Fighter
class CLambda:public CEnemy
{
private:
	
	int iIAStep;
	LONG lCurx; //ascissa riferita allo schema
	int iTakeOffDelay; //limite del conttore per il decollo assegnare valori all'incirca superiori a 30
	BOOL bFollowPath; //flag che indica di seguire un path
	BOOL bGround;//flag che indica che � vincolato al suolo
	CSBObject *pShell;
	CSBObject *pShell1;
	CSBObject *pShell2;
	
public:

	CLambda(void);
	BOOL Update(void);
	void Reset(void);  

};

//---------------------------------- Classe MFighter---------------------------------------------------

class CMFighter:public CEnemy
{

protected:

	int iIAStep;
	int iStepSeq;
	int iStepSeq1;
	int iStepFire1;
	LONG lCurx;
    CShell *pShell;
	CEnemy *pBomb;
	CMissile *pM;
	CADXFastMath m_fm;
	BOOL bFollowPath;

public:

	CMFighter(void);
	BOOL Update(void);
	void Reset(void);
	virtual void SetFlags(DWORD dwFlags);
};



//--------------------------------- Classe CBlaster --------------------------------------------------

class CBlaster:public CMFighter
{
private:

	int iAnimIncr; //incremento animazione frame (+1 o -1 o 0)
	int iRotStep; //contatore per cambio verso di rotzione
	int iChRotFreq; //frequenza cambio di roatazione
	int iFireCnt;
	
public:

	CBlaster(void);
	BOOL Update(void);
	void Reset(void);
	void SetFlags(DWORD dwFlags);
};

//---------------------------------- Classe CLFighter ---------------------------------------

class CLFighter:public CEnemy
{
private:
	
	int m_iStartY;

public:

	CLFighter(void);
	BOOL Update(void);
	void Reset(void);
	void SetDontRemoveFlag(BOOL bVal);

};

//-------------------------------- Classe CAntiAircraft2 ---------------------------------------------
//contraerea formata da una torretta piu' un cannone
//� vincolabile ad un altro oggetto
class CAntiAircraft2:public CGroundObject
{
private:

	int m_iFrmCannonCnt; //numero di frame del cannone
	int m_iBaseCannonFrame; //frame base del cannone
	int absx,absy; //coordinate assolute
	int m_iCurCannonAngle; //angolo corrente del cannone
	int m_iAngTolerance; //angolo di tolleranza nel puntamento, piu' � piccolo e piu' il nemico � reattivo
    int m_iShots; //numero di colpi per ogni raffica
	int m_iFreq; //limite contatore frequesza raffica (piu' � alto e minore � la frequenza)
	int m_iFreqCnt; //contatore frequenza
	int m_iShotsCnt; //contatore colpi
	int m_iJntX,m_iJntY; //coordinate relative dell'asse di rotazione della canna del cannone
	LONG lx,ly;      //var. temp.
	CObjectStack *m_pSmoke; //stack del fumo
	BOOL m_bDontDrawBottom; //se viene settato, non disegna la parte inferiore

public:

	CADXFastMath *m_pfm;
	CAntiAircraft2(void);
	CADXSprite m_sprCannon;
	BOOL Update(void);
	void SetToleranceAngle(int iTol); 
	void Reset(void);
	void SetBurst(int iShots,int iFreq); //imposta le caratteristiche della raffica
	void CAntiAircraft2::SetRotJoint(int jx,int jy); //imposta l'asse di rotazione della canna
	void SetBaseCannonFrame(int iframe); //frame base del cannone (� quella a 270�)
	void DontDrawBottom(BOOL bval); 
	HRESULT SetSmokeStack(CObjectStack *pStack); //imposta lo stack fumo
	HRESULT Draw(void);
};

//-------------------------------- Classe CTank ------------------------------------------------------

class CTank:public CGroundObject
{
private:

	int m_iRotAnim;
	CObjectStack *m_pAAStack;
	int absx,absy;
	int m_iAvgSpeed; //velocit� di spostamento
	int m_imaxc; //maxframe index del cingolo
	int m_iExtentLeft; //distanza massima di movimento verso destra e sinistra dal punto di immissione
	int m_iExtentRight; 
	int m_iInit;
	int m_ixl,m_ixr; //limiti di movimento in coordinate relative
	CAntiAircraft2 *m_paa; //puntatore alla torretta con il cannone

public:
	
	CADXSprite m_sprTrackCrawler; //parte inferiore (cingoli)
	CTank(void);
	HRESULT SetAAStack(CObjectStack *pStack);
	HRESULT Draw(void);
	BOOL Update(void);
	void Reset(void);
	void SetExtents(int left,int right);

};

//-------------------------------- Classe CShip ------------------------------------------------------
//nave
class CShip:public CGroundObject
{
private:

	int iWreck;
	IMAGE_FRAME_PTR pimgWake;
	int iWakeCnt;
	int iWakeNum;
	int iAANum; //numro di torette contraeree
	int iWakeStepUpdate;
	CAntiAircraft2 *m_paa;
	CObjectStack *m_pAAStack;

public:
	
	CShip(void);
	HRESULT SetWakeStack(IMAGE_FRAME_PTR pWake,int inum);
	HRESULT SetAAStack(CObjectStack *pStack); //imposta lo stack che contiene le torrette contraeree
	HRESULT Draw(void);	
	void SetNumAA(int inum); //imposta il numero di torrette
	void Reset(void);
	void Explode(void);
	BOOL Update(void);
	void SetFlags(DWORD dwFlags);
};



//-------------------------------- Classe Player -----------------------------------------------------
//Giocatore

class CPlayer:public CSBObject
{
private:

protected:

	//stato dell'input del giocatore
	PL_INPUT m_sInput;
	DWORD m_dwTimer1; //timer
	BOOL bBlinking; //se impostato su true il player � temporaneamente invulnerabile
	int iBlkCount; //countatore per il blinking (invulnerabile)
	int iBlkStep; //altro contatore per modalit�  blinking
	int iy; //delta y dovuta l'input
	BOOL bInvisible; //il player � correntemente invisibile
	BOOL bExploding; 

public:

	LONG dwLives; //vite
	BOOL bExitGame; //il player richiede di uscire dal gioco e tornare alla console
	BOOL bNozzleFlames; //fiamme ugello
	TCHAR *szName;
	DWORD dwWeapon1; //tipo arma corrente primaria (stato dell'arma primaria, viene incrementata ogni volta che si prende un bonus relativo a quest'arma)
	DWORD dwWeapon2; //tipo arma corrente secondaria
	DWORD dwWeapon3; //tipo di arma speciale corrente 
	DWORD dwShells1; //proiettili disponibili per ogni arma primaria	
	DWORD dwShells2; //proiettili arma 2
	DWORD dwShells3; //proiettili arma speciale
	DWORD dwShells4; //proiettili extra per i missili a dritto
	DWORD dwShells5; //proiettili extra per il fuoco extra verso terra e verso l'alto	
	LONG dwSpecialShells; //proiettili speciali (es. blue shells)
	DWORD dwShells3Full; //numero di colpi arma speciale quando � piena (serve per il disegno della status bar)
	DWORD dwNextLife; //punteggio al quale si vince la vita  
	CPlayer(void);	
	int iDisplacement; //spostamento direzionale (in pixel) in seguito all'input (all'aumentare di questo parametro aumenta la fluidit� di spostamanto)
	int iFire1Delay; //ritardo minimo per il fuoco primario (al diminuire di questo aumenta la frequenza di fuoco)
	int iFire2Delay; //ritardo minimo per il fuoco secondario
	int iFire3Delay;
	int iStoredDelay1; //mem. dalay1
	void (*lpfnGetInput)(void); //funzione di input (puo' essere tramite tastiera o joy)
	CADXSprite* pNozzleFlames; //fiamme ugello
	//la funzione puntata restituisce l'input tramite SendInput

	HRESULT SendInput(PL_INPUT_PTR pInput); //invia l'input dell'utente all'oggetto		
	void ClearInput(void); //azzera lo stato dell'input 
	BOOL Update(void);	
	void (* Fire1)(LONG x,LONG y);
	void (* Fire2)(LONG x,LONG y);
	void (* Fire3)(LONG x,LONG y); //arma speciale
	void (* pfnSmoke)(LONG x,LONG y);//funzione che crea il fumo quando l'energia � bassa	
	void (* pfnGameOver)(LONG x,LONG y); //funzione game over
	void (* pfnUpdateBar)(void); //funzione di aggiornamento barra di stato
	void (* pfnStoredFire)(LONG x,LONG y); //usata per memrizare fuoco precedente
	void Explode(void);
	//flag armi extra (vengono sparate insieme al fuoco principale)
	BOOL bGrndFire; //fuoco verso terra
	BOOL bTripleFire; //triplo fuoco
	BOOL bElectricBarrier; //barriera elettrica attiva
	BOOL bStrightMissile; //missile
	BOOL IsBlinking(void); //restituisce TRUE se il player � visibile (non blinking)	
	void SetBlinking(void); //imposta la modalit� lampeggiante invulnerabile
	void Reset(void);
	HRESULT Draw(void);
	BOOL IsInvisible(); //rende true se non � visibile
};

//------------------------------- classe CFastChars ----------------------------------------

//Disegna una stringa con il char set impostato
class CFastChars:public CADXCharSet
{
public:

	void BlitString(LONG xout,LONG yout,TCHAR *szText,IMAGE_FRAME_PTR imgDest);
};

//------------------------------ classe StackManager ---------------------------------------
template <class T>
class StackManager
{
	T *pobj;
	CObjectStack *pLastStack;

public:

  StackManager(void);
  HRESULT CreateStack(CObjectStack *pstack,int numitems,IMAGE_FRAME_PTR pimgFrames,int numframes,int energy,int power,int bonus=0,void ( *fnExpl)(LONG,LONG)=NULL,CObjectStack *pCartridges=NULL);
  BOOL AddWoundedState(DWORD dwEnergyThreshold,DWORD dwFrame);
  //imposta il flag che indica di non rimuovere l'oggetto dopo che � morto
  void SetDontRemoveFlag(BOOL bVal);
  //imposta il fumo dopo l'esplosione
  //void SetSmokeStack(CObjectStack *psmoke);

};

/////////////////////////////// FUNZIONI  //////////////////////////////////////////////////

//esegue una transizione fra una schermata e l'altra
//imgOut=immgine attuale che viene dissolta
//imgIn=nuova immagine da visualizzare al posto di quella attuale
//uno dei due argomenti puo' essere NULL
void ScreenTransition(IMAGE_FRAME_PTR imgOut,IMAGE_FRAME_PTR imgIn=NULL);
HRESULT GrabSprites(void); //acquisisce gli sprite principali
void FreeSprites(void); //rilascia gli sprite principali
//Crea un livello
HRESULT CreateSbLevel(DWORD dwLevel,CSbLevel *DestLevel,TCHAR *szLevelFile=NULL);
//acquisizione input per editing del livello
void GetKeybEditor(void);
//ciclo gioo
int GameLoop(void);
//funzione principale di animazione del gioco (ciclo base)
void HeartBeat(void);
//modalit� level editor
void LevelEditor(void);
//sequenza di livello completato
void LevelCompleted(void);
//sequenza di gioco completato
void GameCompleted(void);
//Inizializza lo sprite del giocatore e gli altri oggetti necessari
HRESULT InitData();
//crea gli handler per le risorse che vengono usati dalla funzione LoadSbLevel
void CreateResHandler(void);
//restituisce l'id del nemico a partire dallo stack
int GetResId(CObjectStack *pstack);
//Crea tutti gli stack degli oggetti del gioco
HRESULT CreateObjects();
//crea gli effetti sonori (WAV)
HRESULT CreateSounds();
//funzioni per il disegno del background
void ClearBack1(IMAGE_FRAME_PTR pimg); //I schema
void DrawLevelBackGround(IMAGE_FRAME_PTR pimg);
//StarField
void StarField(IMAGE_FRAME_PTR pimg);
//aggiorna la status bar
void UpdateStatusBar(void);
//DrawStatus
HRESULT DrawStatus(IMAGE_FRAME_PTR imgDisplay);
//Aggiorna tutti gli oggetti presenti sulla lista e li disegna sul display
void RefreshObjects(IMAGE_FRAME_PTR imgDisplay);
//rilascia la memoria allocata da InitData
void FreeData(void);
//rilascia gli oggetti creati da CreateObjects
void FreeObjects(void);
//rilascia i suoni caricati
void FreeSounds();
//funnzioni di fuoco giocatore 
void PlFire1(LONG x,LONG y);
void PlFire2(LONG x,LONG y);
void PlFire3(LONG x,LONG y);
void PlFire4(LONG x,LONG y);
void PlFire5(LONG x,LONG y);
void PlFire6(LONG x,LONG y);
void PlFire7(LONG x,LONG y);
void PlFire8(LONG x,LONG y);
void PlFire9(LONG x,LONG y);
void PlFire10(LONG x,LONG y);
void PlFire11(LONG x,LONG y);
void PlFire12(LONG x,LONG y);
void PlFire13(LONG x,LONG y);
void PlFire14(LONG x,LONG y);
void PlFireGrnd(LONG x,LONG y);
void PlFireFw(LONG x,LONG y);
void PlTripleFire(LONG x,LONG y);
void PlFireMissile(LONG x,LONG y);
//lancia una bomba tipo 1
void PlDropBomb1(LONG x,LONG y);
//lancia una bomab tipo 2
void PlDropBomb2(LONG x,LONG y);
//lancia una bomba tipo 3
void PlDropBomb3(LONG x,LONG y);
//Le seguenti funzioni immettono nello stack degli oggetti attivi
//i vari tipi di esplosioni, x e y sono le coordinate del centro dell'esplosione
void PushExpl1(LONG x,LONG y);
void PushExpl2(LONG x,LONG y);
void PushExpl3(LONG x,LONG y);
void PushExpl4(LONG x,LONG y);
void PushExpl5(LONG x,LONG y);
void PushExpl6(LONG x,LONG y);
void PushExpl7(LONG x,LONG y);
void PushExpl8(LONG x,LONG y);
void PushExpl9(LONG x,LONG y);
void PushExpl10(LONG x,LONG y);
void PushExpl8(LONG x,LONG y);
void PushDebris1(LONG x,LONG y);
void PushDebris2(LONG x,LONG y);
void PushDebris3(LONG x,LONG y);
void PushDebris4(LONG x,LONG y);
void PushSmoke0(LONG x,LONG y);
void PushSmoke270(LONG x,LONG y); //fumo verso l'alto
void PushSmoke180(LONG x,LONG y);
void PushSmoke(LONG x,LONG y); //fumo fermo
void PushFlames(LONG x,LONG y,DWORD dwNumFlames); //fiamme
//effetto di collisione (sup. metalliche)
void PushCollideMetal(LONG x,LONG y);
void PushCollideElectro(LONG x,LONG y);
//controlla le collisioni fra gli oggetti
BOOL DetectCollisions(CSBObject *pobj,DWORD dwStart,DWORD dwNumObjects);
//mostra lo stack sul file di debug
void DisplayStack(CObjectStack *pStack,TCHAR *title);
//chide se si vuol uscire dal gioco
BOOL AskForExit(void);
//proc. che chiede di continuare
BOOL Continue(void);
int CloseFormContinue (void *pform);
//disengna il back ground quando si visualizza un menu
void DrawBackGround(IMAGE_FRAME_PTR img);
//collega un buffer 16 bit ad una immagine
HRESULT LinkBuffer(CImgBuffer16 *pDestBuff16,IMAGE_FRAME_PTR pImgSrc);
//chiude una form
int CloseFormExit (void *pform);
//chiude il gioco
int QuitGame(void *pform);
//Aggiunge un lmento ad un path cinematico 
HRESULT AddPath(int Angle,int Velocity,int Displ,TP_PATH_DATA_PTR pPath=NULL,BOOL bInit=FALSE);
//chiude il ciclo del gioco e torna alla console
int BreakGameLoop(void *pform);
//prosegue il gioco dopo aver risposto "S�" al messaggio che chiede di continuare
int ContinueGame(void *pform);
//funzioni per il bonus
void PushBonusFire1(LONG x,LONG y);
void PushBonusFire2(LONG x,LONG y);
void PushBonusEnergy(LONG x,LONG y);
void PushBonusShells(LONG x,LONG y);
void PushBonusBombs10(LONG x,LONG y);
void PushBonusBombs50(LONG x,LONG y);
void PushBonusScore(LONG x,LONG y);
void PushBonusShield(LONG x,LONG y);
void PushBonusGyro(LONG x,LONG y);
void PushBonusBooster(LONG x,LONG y);
void PushBonusEB(LONG x,LONG y);
void PushBonusThrust(LONG x,LONG y);
void PushBonus1Up(LONG x,LONG y);
void PushBonusThunder(LONG x,LONG y);
void PushBonusLaser(LONG x,LONG y);
void PushBonusMissile(LONG x,LONG y);
void PushBonusWeaponA(LONG x,LONG y);
void PushBonusWeaponB(LONG x,LONG y);
void PushBonusWeaponC(LONG x,LONG y);
void PushBonusWeaponD(LONG x,LONG y);
void PushBonusFwShells(LONG x,LONG y);
void PushBonusGrndFire(LONG x,LONG Y);
void PushBonusTripleFire(LONG x,LONG y);
void PushBonusLaser3(LONG x,LONG y);
void PushBonusBlueShell(LONG x,LONG y);
void PushBonusStrightMissile(LONG x,LONG y);
int GrndImpact(LONG x,LONG y);
void GameOver(LONG x,LONG y);
int GetHighScorePosition(LONG lScore);
int HighScore(void *pform); //high score
int ShowMainMenu(void *pform); //mostra la prima pagina del menu principale (quella con "inizia partita")
int ReadString(char **dest,FILE *f);
TCHAR *TranslateGameMsg(int msgid,int iErrorCode=0);
HRESULT LoadAuxResource(int resid,int iflags=0);
void FreeAuxResources(void);
void ProgressBar(int x,int y,float perc,int width);
void SelectEditorObject(LONG lindex);
void DrawEditorSelectedObject(IMAGE_FRAME_PTR pimgDest);
void DrawPointer(IMAGE_FRAME_PTR pimgDest,LONG x,LONG y); //definita in starblastrix.cpp
void GetDefaultTriggerValues(int stack_index,DWORD *dwEnergy,DWORD *dwPower);
void ShowStats(CSbLevel *level);
TCHAR *GetEnemyName(int id,int language);
//esplosioni multiple sull'oggetto
//in posizioni casuali all'interno del bound box
void PushExplMulti(CADXSprite *psrc,int num_explosions);
//crea le form ausiliarie (uscita gioco ecc...)
void CreateAuxForms(void);
int GetTargets(CSBObject *pTargets[],int inum);
int StartCustomLevel(void *ctrl);
BOOL IsUpDown(int id);
void CreateDefaultHighScores(void);
#endif

