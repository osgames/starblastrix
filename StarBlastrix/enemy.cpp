//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  enemy.cpp Nemico generico 
///////////////////////////////////////////////////////////////////////*/


#include "a32graph.h"  //GraphicManager e FrameManager
#include "a32sprit.h"   //sprites
#include "a32audio.h"
#include "a32object.h" //oggetti astratti 
#include "enemy.h"
#include "level.h"

extern void PushExpl3(LONG x,LONG y);
extern CADXSound g_cs;
extern LONG g_snCrash;
extern CADXFastMath g_fmath;
extern CSbLevel CCurLevel;
extern int GrndImpact(LONG x,LONG y);

CADXFastMath m_math;

/////////////////////////////////////////// classe CEnemy ////////////////////////////////////////////////////

//Questa � la classe base per i nemici
//segue un path predefinito per il movimento e non ha algoritmi di IA

CEnemy::CEnemy(void)
{
	pPath=pFollowPath=NULL;
	iFireFreq=30;
	iStepPath=0; //passo corrente
	iStepFire=0;
	iStepUpdate=0;
	iConfigFreq=6;
	iStepConfig=0;
	iStepPathNum=0; //numero di passi nel path
	iUpdateFreq=4; //acquisisce un nuovo elemento dal path ogni iUpdateFreq frames
	iBaseFrame=0;
	iStartFireFreq=20;
	iStartUpdateFreq=2;
	iWoundedState=0;
	m_icurlevel=0; //varibili stato di danneggiamento
	m_ActiveStack=NULL;
	m_pBonusStack=NULL;
	m_Cartridges=NULL;
	m_pWeapon1=NULL;
	m_bEscapingAlgo=FALSE;
	m_bBonusCreated=FALSE;
	m_pWeapon2=NULL;
	dwType=2;
	m_bEscaping=FALSE;
	m_dwFlags=0;
	dwScore=250;
	pfnCollide=NULL;
	pfnSmoke=NULL;
	m_lFireSound=m_lFireSound1=0;
	memset (m_pAuxPath,0,sizeof(TP_PATH_DATA*)*MAX_AUX_PATH);

}

//resetta i dati privati che vengono modificati dalla funzione ::Update
//riporta i contatori alla posizione originale
void CEnemy::Reset(void)
{
	iStepFire=0;
	iStepPath=0;
	iStepUpdate=0;
	iStepConfig=0;	
	dwStatus=1;
	iUpdateFreq=iStartUpdateFreq;
	iFireFreq=iStartFireFreq;
	if (m_dwFlags>=1 && m_dwFlags<MAX_AUX_PATH+1)
	{
		iFireFreq = (int) (iFireFreq * 0.5f); //rende piu' rapido il fuoco
		
		SetPath(m_pAuxPath[m_dwFlags-1],m_iAuxSteps[m_dwFlags-1]); //imposta un path addizionale	
	}
	//path default
	else SetPath(m_pAuxPath[0],m_iAuxSteps[0]);

	m_bEscapingAlgo=(m_dwFlags>=3);//algoritmo per evitare i proiettili del player
	m_bEscaping=FALSE;

	//m_ivy0 � usato per posizioneare il proiettile al momento del fuoco
	GetFrameHotSpot(0,0,(LONG *)&m_ivx0,(LONG *)&m_ivy0);

	iWoundedState=0;
	pParent=NULL;
	iStepWeapon1=0;
	m_bBonusCreated=FALSE;
	dwEnergy=m_lInitialEnergy;
	pPath=pFollowPath; //riporta nella posizione originale	
//	SetCurFrame(iBaseFrame);	
}

//----------------------------------------------CEnemy::SetPath-------------------------------------
//Imposta il path da fa seguire all'oggetto
HRESULT CEnemy::SetPath(TP_PATH_DATA *Path,DWORD dwNumSteps)
{
	if (Path)
	{
		this->pFollowPath=Path; //path da seguire
		this->pPath=Path; //punta la posizione corrente
		this->iStepPathNum=dwNumSteps;
		return S_OK;
	}
	else return E_FAIL;
}

//------------------------------------------------- CEnemy::SetAuxPath --------------------------------

HRESULT CEnemy::SetAuxPath(int index,TP_PATH_DATA *pPath,int steps)
{
	if (index>=0 && index<MAX_AUX_PATH)
	{
		m_pAuxPath[index]=pPath;
		m_iAuxSteps[index]=steps;
		return S_OK;
	}

	return E_FAIL;

}
//--------------------------------------------- CEnemy::SetActiveStack ------------------------------------
//Imposta lo stack degli oggetti attivi
HRESULT CEnemy::SetActiveStack(CObjectStack *pStack)
{
	if (pStack)
	{
		m_ActiveStack=pStack;
		return S_OK;
	}

	else return E_FAIL;

}

//----------------------------------------------- CEnemy::SetWeapon1 ----------------------------------------
//arma ausiliaria n� 1
HRESULT CEnemy::SetWeapon1(CObjectStack *pStack)
{
	if (pStack)
	{
		m_pWeapon1=pStack;
		return S_OK;
	}

	else return E_FAIL;
}

//------------------------------------------------- CEnemy::SetWeapon2 ----------------------------------------
//arma ausiliaria n�2
HRESULT CEnemy::SetWeapon2(CObjectStack *pStack)
{
	if (pStack)
	{
		m_pWeapon2=pStack;
		return S_OK;
	}

	else return E_FAIL;
}

//--------------------------------------------- CEnemy::SetCartridgeStack -----------------------------------
//Imposta lo stack da dove preleva i proiettili da sparare
HRESULT CEnemy::SetCartridgeStack(CObjectStack *pStack)
{
	m_Cartridges=pStack;
	return S_OK;
}

//--------------------------------------------- CEnemy::SetUpdateFreq -------------------------------
//imposta la frequenza di aggiornamento
void CEnemy::SetUpdateFreq(int iFreq)
{
	iStartUpdateFreq=iFreq;
	iUpdateFreq=iStartUpdateFreq;
}

//-------------------------------------------- CEnemy::AddWoundedState -----------------------------
//aggiunge un livello di danneggiamento
//dwEnergy=soglia di energia sotto la quale l'oggetto � in stato danneggiato
//Nota bene:inserire i livelli da quello con dwEnergy piu' alto al piu' basso
BOOL CEnemy::AddWoundedState(DWORD dwEnergyThreshold,DWORD dwFrame)
{
	if (m_icurlevel<MAX_DLV)
	{
		m_idestlevel[m_icurlevel]=dwEnergyThreshold;
		m_iframe[m_icurlevel]=dwFrame;
		m_icurlevel++; //aumenta lo stack - top
		return TRUE;
	}

	return FALSE;
}

//--------------------------------------------- CEnemy::GetWoundedState ------------------------------
//Acquisisce il livello di danneggiamento corrente
int CEnemy::GetWoundedState(void)
{
	return iWoundedState;
}

//-------------------------------------------- CEnemy::SetNextPathStep ------------------------------

BOOL CEnemy::SetNextPathStep(void)
{
	//imposta l'nagolo e la velocit� correnti
	this->SetVelocity(pPath->Angle,pPath->Velocity);
	//imposta il numero di passi che deve fare in queste condizioni
	iUpdateFreq=pPath->Displ; 

	this->UpdatePosition(); //aggiorna la posizione
	//controlla la direzione di movimento per descidere la frame da utilizzare	
	
	iStepPath++;
	
	if (iStepPath<iStepPathNum)
	{
		pPath++; //punta il record successivo
		return TRUE;
	}
	else
	{
		//� arrivato alla fine del path: deve eliminare l'oggetto dallo stck			
		return FALSE;
	}
}

//---------------------------------------------- CEnemy::SetFlags -----------------------------
void CEnemy::SetFlags(DWORD dwFlags)
{
	m_dwFlags=dwFlags;
}

//---------------------------------------------- CEnemy::Update -----------------------------------
//Questo tipo di oggetto segue il path impostato
BOOL CEnemy::Update(void)
{
	CSBObject *pShell;
	int an,vl,stp;			

	iStepUpdate++;
	
	if (!iWoundedState) 
	{
		if (m_bEscapingAlgo)
		{
			//algoritmo che evita i protiettili del player
			if (!m_bEscaping && GetEscapingParms(&an,&vl,&stp))
			{
					//imposta l'nagolo e la velocit� correnti
					this->SetVelocity(an,vl);
					//imposta il numero di passi che deve fare in queste condizioni
					iStepUpdate=0;
					iUpdateFreq=stp; 
					m_bEscaping=TRUE;
			}
		}

		//nemico vivo	
		if (iStepUpdate >= iUpdateFreq)
		{	

			m_bEscaping=FALSE;
			iStepUpdate=0; //resetta			
			if (!SetNextPathStep())
			{
				if (x>=-170 && x<=CSBObject::g_iScreenWidth+40 && y>-50 && y<=CSBObject::g_iScreenHeight)
				{
					//non puo' terminare il ciclo perch� l'oggetto � ancora nell'area di visibilit�
					iStepUpdate=iUpdateFreq;
				}

				else return FALSE;				
			}
		}

		iStepFire++;
		//Controlla il contatore del fuoco
		if (iStepFire == iFireFreq)
		{
			//estrae un proiettile dalla collezione di proiettili
			//SPARA!!
			pShell=m_Cartridges->Pop();

			if (pShell)
			{
				//imposta posizione iniziale e velocit� proiettile
				//m_ivy0 � calcolato al momento del reset � la y dell'hospot 0 della frame 0
				pShell->SetPosition(x,y+m_ivy0);
				pShell->dwClass=CL_ENFIRE;
				pShell->SetInitialEnergy(1);
				pShell->dwPower=4;
				pShell->SetVelocity(180,8);
				pShell->Reset();
				//immette il projettile nello stack attivo
				m_ActiveStack->Push(pShell);

				g_cs.PlaySound(m_lFireSound,0,0);
			}
			
			iStepFire=0;
		}

		this->UpdatePosition(); //aggiorna la posizione

	}

	else
	{	
		//if (iWoundedState)
		//{
			//il nemico sta precipitando con moto parabolico
			m_t += (float)0.1;
			x+=iVelx;
			y=(int)(m_t*m_t*CSbLevel::g_gravity+iVely*m_t+old_y);
		
			//l'oggetto sta precipitano
			if (iStepUpdate >= iUpdateFreq)
			{
					iStepUpdate=0; //resetta
					//emette fumo mentre precipita
					if (pfnSmoke) pfnSmoke(x+15,y+10);										
			}
		//}

	}
	
	this->UpdateTrim(); //aggiorna l'assetto	

	if (dwEnergy<=1) 
	{
		if (dwEnergy<=0)
		{
			if (g_fmath.RndFast(3)==1) dwEnergy=1; //cade a terra con scia di fumo			
		}

		if (dwEnergy<=0)
		{
			CreateBonus();
			return FALSE;
		}
		else
		{  	
			if (!iWoundedState)
			{
				iUpdateFreq=(int)(iUpdateFreq*0.5); //aumenta la frequenza di aggiornamento
				iWoundedState=1;
				m_t=0;
				old_x=x;
				old_y=y;		
			}

			if (y>CSBObject::g_iScreenHeight-CSbLevel::g_bottom)
			{	

				 //acquisisce il tipo di terreno nel punto dell'impatto
				int iGroundType=CCurLevel.GetGroundType(x+18);

				if (iGroundType==LV_INCONSISTENT)
				{
					//cade nel baratro
					return (y<CSBObject::g_iScreenHeight);
				}
				else
				{

					if (iGroundType==LV_LIQUID)
					{
						GrndImpact(x,y);
						GrndImpact(x-10,y);
					}
					else
					{
						//si schianta al suolo
						
						g_cs.PlaySound(g_snCrash,0,0);					
						PushExpl3(x,y);
					}
					CreateBonus();

					return FALSE;

				}
			}

			return (x>-170);		   
		}
	
	}

	return (x>-170);
}

//----------------------------------- CEnemy::UpdateWoundedState ------------------------
//Aggiorna lo stato di danneggiamento, cambia frame in base al livello di energia corrente
//rende true se lo stato � cambiato

BOOL CEnemy::UpdateWoundedState(void)
{

	int iprev;

	iprev=iWoundedState;
	//m_icurlevel contiene il numero di stati di danneggiamento possbili per questo oggetto
	if (m_icurlevel>=1 && dwEnergy<m_idestlevel[0]) {iWoundedState=1;SetCurFrame(m_iframe[0]);}
	if (m_icurlevel>=2 && dwEnergy<m_idestlevel[1]) {iWoundedState=2;SetCurFrame(m_iframe[1]);}
	if (m_icurlevel>=3 && dwEnergy<m_idestlevel[2]) {iWoundedState=3;SetCurFrame(m_iframe[2]);}
	if (iWoundedState==0) UpdateFrame();		
	iStepUpdate=0;
	
	if (iprev!= iWoundedState) 
	{
		int iwe,ihe;
		int icx,icy;

		iwe=frames[cur_frame]->width;
		ihe=frames[cur_frame]->height;

		//ricopre l'oggetto di esplosioni
		for (icx=0;icx<iwe;icx+=50)
		{
			for (icy=0;icy<ihe;icy+=50)
			{
				UpdateAbsXY();
				//esplosione
				//vengono aggiunte delle coordinate di "disturbo" casuali per non ottenere un pattern perfettamente
				//rettangolare
				if (lpfnExplode) lpfnExplode(xabs+icx+m_math.RndFast(20)-10,yabs+icy+m_math.RndFast(20)-10);
			}
		}

		return TRUE;

	}

	return FALSE;
}

//--------------------------------------- CEnemy::UpdateTrim -------------------------------
//Aggiorna l'assetto (cambia frame)
void CEnemy::UpdateTrim(void)
{
	
	LONG lDirection=0; //0=orizzontale 1=up 2=down
	LONG cf;
	LONG lY;
	
	lY=old_y; //confronta con la y precedente
	iStepConfig++;
    
    //controlla se deve eseguire un cambio di frame o configurazione
    if (iStepConfig == iConfigFreq)
	{
		cf=cur_frame;
		iStepConfig=0; //resetta il contatore
		//calcola la direzione corrente
		if (y>lY) lDirection=-1; //down

		else if (y<lY) lDirection=+1; //up
		
		if (lDirection==0)
		{
			//se si muove orizzontalmente tende a ristabilire il roll
			if (cur_frame<iBaseFrame) cf++; //prende una frame piu' inclinata verso l'alto 
			else if (cur_frame>iBaseFrame) cf--; //prende una frame piu' inclinata verso il basso

		}
		else cf += lDirection;	
		

		if (cf>m_iAnimEndFrame) cf=m_iAnimEndFrame;
		else if (cf<m_iAnimStartFrame) cf=m_iAnimStartFrame;

		SetCurFrame(cf); //imposta la frame
	}   
}

//------------------------------------------ CEnemy::GetEscapingParms -----------------------
//Acquisisce velocit�,angolo e numero di cicli necessari a sfuggire al fuoco del player
//rende FALSE se non ci sono proiettili che minacciano l'oggetto
BOOL CEnemy::GetEscapingParms(int *angle,int *vel,int *steps)
{
	CSBObject **pList=m_ActiveStack->m_objStack;
	CSBObject *pobj=NULL;
	int refy=this->y+15;

	pobj=pList[0];
		
	//acquisisce il numero di elementi attivi
	int iMax=m_ActiveStack->ItemCount();
		
	//ricerca un nemico a cui sparare all'interno dello stack degli oggetti attivi
	for (int count=0;count<iMax;count++)
	{				
		if (pobj && pobj->bActive && pobj->dwClass==CL_PLFIRE)
		{
			//si considera che il proiettile si sposti in senso orizzonatale
			if (abs(pobj->x-this->x)<70)
			{
				if (abs(pobj->y-refy)<45)
				{
					//se sta gi� spostandosi non deve cambiare direzione
					if (!(this->GetAngle()==270 || this->GetAngle()==90))
					{
						*angle=refy>CSBObject::g_iScreenHeight*0.5 ? 270 : 90;
					}

					*vel=4;
					*steps=20;

					return TRUE;

				}

			}			
		}

		pobj=pList[count];

	}//fine for

	return FALSE;

}

//------------------------------------------ CEnemy::SetSound ------------------------------

void CEnemy::SetFireSound(LONG sound)
{
	if (sound>0) m_lFireSound=sound;
	else m_lFireSound=0;
};

//------------------------------------------ CEnemy::SetSoundAux ------------------------------

void CEnemy::SetFireSoundAux(LONG sound)
{
	if (sound>0) m_lFireSound1=sound;
	else m_lFireSound1=0;

};

//------------------------------------------ CEnemy::CreateBonus ---------------------------
//inserisce il bonus nello stack se � presente
void CEnemy::CreateBonus(void)
{
	static CSBObject *pBn=NULL;

	//se il bonus � gi� stato creato esce
	if (m_bBonusCreated) return; 

	if (m_pBonusStack)
	{		
		pBn=m_pBonusStack->Pop();		

		if (pBn)
		{
			m_bBonusCreated=TRUE;
			pBn->Reset();
			pBn->SetPosition(this->x,this->y);						
		
			if (m_ActiveStack) m_ActiveStack->Push(pBn);			
		}
	}	
}