//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  train.h -- treno
///////////////////////////////////////////////////////////////////////*/

#ifndef _TRAIN_INCLUDE_
#define _TRAIN_INCLUDE_

#include "sbengine.h"
#include "grndauxen.h"
#include "resman.h"
#include "modular.h"

//tipi di vagoni
enum wagon_enum
{
	base_wagon=0,
	tank_wagon=1,
	missile_wagon=2,
    cannon_wagon=3,
	locom_wagon=4,
};

void FreeTrainRes(void);
HRESULT LoadTrainRes(CResourceManager *prm);
HRESULT CreateTrainStack(int numitems,CObjectStack *pstack,CResourceManager* prm,int flags);

class CWagon;

class CTrain:public CModularEnemy
{
private:

protected:
	
	int m_iStartDelay;
	int m_AI;
	LONG m_lgrndx;
	CModularEnemy* m_pCurWagon; 
	BOOL UpdateAI(void);
	HRESULT AttachWagon(wagon_enum wagonid);

public:

	CTrain(void);
	void Reset(void);
	BOOL Update(void);
	HRESULT Draw(void);
};


class CWagon:public CModularEnemy
{
private:


protected:

	int m_shell_frame;
	int m_shell_angle;
	int m_shell_hot_spot;
	int m_burst_cnt;
	int m_burst;	
	LONG m_weapon_sound;	
	BOOL UpdateAI(void);

public:	

	void SetWeapon(int shell_frame,int shell_angle,int hot_spot,LONG sound,int burst);
	CWagon(void);
	BOOL Update(void);
	void Reset(void);
};

#endif