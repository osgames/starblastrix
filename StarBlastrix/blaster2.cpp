/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
	blaster2.cpp
*/

#include "blaster2.h"
#include "mecharm.h"

extern LONG g_snNozzle;
extern CADXSound g_cs;

//////////////////////////////////////////// classe CBlaster2 ///////////////////////////////////////////

CBlaster2::~CBlaster2(void)
{
	if (m_pArm)
	{
		//rilascia il braccio meccanico
		m_pArm->m_Crank1.Release();
		m_pArm->m_Crank2.Release();
		m_pArm->m_Crank3.Release();
		m_pArm->m_Piston.Release();
		m_pArm->m_Cannon.Release();
		SAFE_DELETE(m_pArm);
	}

}

CBlaster2::CBlaster2(void)
{
	m_pmc=NULL;
	m_pArm=NULL;
	dwClass=CL_ENEMY;
	m_pNozzleFlames=NULL;
	m_pFlames=NULL;
	inextp=0;
}

void CBlaster2::SetNozzleFlamesStack(CObjectStack *pstack)
{
	m_pNozzleFlames=pstack;
}

void CBlaster2::Reset(void)
{
	CSBObject::Reset();
	CEnemy::Reset();
	m_bDoFlames=FALSE;
	SetVelocity(180,2);
	Body.m_iTag1=dwEnergy;
	Body.SetVelocity(0,0);
	iUpdateFreq=150;
	inextp=0;
	m_ttl=25;
	SetCurFrame(0);//in realt� lo sprite � il body	
	if (m_pArm)
	{
		//mette il braccio meccanico nella posizione iniziale
		m_pArm->Reset();
		m_pArm->SetRandom(TRUE);
	}

	if (m_pNozzleFlames)
	{
		m_pFlames=(CAnimatedObject *)m_pNozzleFlames->Pop();
		if (m_pFlames)
		{
			//attacca le fiamme a questo oggetto
			m_pFlames->pParent=this;
			LONG xj,yj;	
			//acquisisce la posizione relativa dell'ugello
			Body.GetFrameHotSpot(0,1,&xj,&yj);
			//imposta la posizione relativa delle fiamme
			m_pFlames->SetPosition(xj,yj-5);		
		}
	}

}


BOOL CBlaster2::Update(void)
{
	iStepUpdate++;

	if (iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;

		m_bDoFlames=FALSE;  //accende l'ugello solo quando si sposta avanti

		m_ttl--;

		if (m_ttl<0)
		{
			//esce di scena
			SetVelocity(180,5); 
			m_bDoFlames=TRUE;
		}

		else
		{
		
			if (m_pmath)
			{
				//se non � gi� impostato l'algoritmo successivo estrae un algoritmo a caso			
				int prob=(inextp>0 ? inextp : m_pmath->RndFast(10));

				inextp=0;

				switch (prob)
				{
				case 0:

					SetVelocity(180,2);
					m_bDoFlames=TRUE;	
					g_cs.PlaySound(g_snNozzle,0,0);
					break;

				case 3:
				case 2:
				case 1:

					if (x<0)
					{
						//si riposiziona verso il centro e poi sta fermo
						SetVelocity(0,2);
						inextp=1; 
					}
					else
					{
						if (x>CSBObject::g_iScreenWidth-80)
						{
							SetVelocity(180,2);
							m_bDoFlames=TRUE;
							g_cs.PlaySound(g_snNozzle,0,0);
							inextp=1;
						}
						else
						{
							//fermo
							SetVelocity(180,0);
						}
					}

					break;

				case 4:

					SetVelocity(190,1);				
					m_bDoFlames=TRUE;
					g_cs.PlaySound(g_snNozzle,0,0);
					break;

				case 5:

					SetVelocity(170,1);
					m_bDoFlames=TRUE;
					g_cs.PlaySound(g_snNozzle,0,0);
					break;

				case 6:

					SetVelocity(0,1);
					break;

				case 7:

					if (m_pmath->RndFast(4)==1)
					{
						//esce di scena
						SetVelocity(180,4);
						m_bDoFlames=TRUE;
						g_cs.PlaySound(g_snNozzle,0,0);

					}

					else inextp=1;

					break;

				case 8:

					if (x<50)
					{
						SetVelocity(0,1);
						break;
					}

					else SetVelocity(180,0);
					break;

				default:

					if (x<0)
					{
						SetVelocity(0,2);
					}
					else
					{
						SetVelocity(180,0);
					}

					break;

				}				

			}//fine math

		
		}//fine ttl
		

	}

	if (m_pArm)
	{
		m_pArm->lTargetX=this->lTargetX;
		m_pArm->lTargetY=this->lTargetY;
		m_pArm->Update();
	}

	if (m_pFlames) 
	{
		
		m_pFlames->Update();
	
	}


	UpdatePosition();

	Body.SetPosition(this->x,this->y);	
	//aggiorna la posizione di eventuali altri elementi collegati
	Body.UpdatePosition();

	if (dwEnergy<=0)
	{		
		CreateBonus();
		if (m_pFlames) m_pFlames->bActive=FALSE;
		return FALSE;
	}

	return (x>-300 && y>-180 && y<CSBObject::g_iScreenHeight);
}


void CBlaster2::Explode(void)
{

	if (lpfnMultiExplosion) 
	{
		lpfnMultiExplosion(&Body,8);	
	}
	else CSBObject::Explode();
};

BOOL CBlaster2::DoCollisionWith(CSBObject *pobj)
{
	BOOL bres=FALSE;

	Body.m_iTag1=dwEnergy;

	//deve controllare la collisione fra pobj e il corpo
	if (Body.DetectCollision((CADXSprite *)pobj))
	{
		DamageComponent(&Body,pobj);					
		dwEnergy=Body.m_iTag1;

		if (m_pArm)
		{
			m_pArm->DoCollisionWith(pobj);
		}

		bres=TRUE;
	}
	
	//collisioni con il braccio
	if (m_pArm)
	{
		bres |= m_pArm->DoCollisionWith(pobj);
	}
	
	
	if(dwEnergy<=0)
	{
		if (lpfnMultiExplosion) lpfnMultiExplosion(&Body,8);
	}

	return bres;	
}


//renderizza tutta la catena cinematica
HRESULT CBlaster2::Draw(void)
{
	Body.SetDisplay(m_pimgOut);
	Body.RenderToDisplay();

	if (m_pArm)
	{
		m_pArm->SetDisplay(m_pimgOut);
		m_pArm->Draw();
	}

	if (m_pFlames && m_bDoFlames)
	{
		m_pFlames->SetDisplay(m_pimgOut);
		m_pFlames->Draw();
	}

	return S_OK;
}


HRESULT CBlaster2::SetMathModule(CADXFastMath *pmath)
{
	if (pmath)
	{
		m_pmath=pmath;
		return S_OK;
	}

	return E_FAIL;

}
