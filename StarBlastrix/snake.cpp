//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  snake.cpp -- funzioni di caricamento risorse per serpenti
///////////////////////////////////////////////////////////////////////*/

#include "snake.h"
#include "a32sprit.h"
#include "a32graph.h"
#include "a32audio.h"
#include "sbengine.h"

extern LONG g_snBfire; //fuoco primario
extern LONG g_snFire10; //tuono
extern CADXSound g_cs;

CSnake2::CSnake2(void)
{
	m_pBody=NULL;
	dwClass=CL_ENEMY;
	dwType=35;
	memset(m_vHeadCannons,0,sizeof(VERTEX2D)*2);
	dwStatus=0;
	m_iNumItems=0;
    m_bBodyHotSpots=m_bHeadHotSpot=FALSE;  //flag che indica che gli hot spot del corpo sono impostati	
	m_iangle_vel=NULL;
	m_pmath=NULL;
	m_ivelocity=2;//velocit�	
	m_ps=NULL;
}

CSnake2::~CSnake2(void)
{
	dwStatus=0;
	SAFE_DELETE_ARRAY(m_pBody);
	SAFE_DELETE_ARRAY(m_iangle_vel);
}


HRESULT CSnake2::Create(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,int iflags,int delta_angle)
{
	int inframes=0;
	int an;
	CADXSprite *pspr;
	int count,count1;

	if (dwStatus) return E_FAIL;
	if (!m_lpFm) return E_FAIL;
	if (!pimghead || !pimgbody) return E_FAIL;
	if (!m_bHeadHotSpot || !m_bBodyHotSpots) return E_FAIL;
	
	m_iNumItems=numitems;
	m_pBody=new CSBObject[m_iNumItems];
	m_iangle_vel=new int[m_iNumItems+1];

    //numero di frames nei vettori di immagini
	inframes=360/delta_angle;	

    //testa del serpente 
	m_Head.SetFrameManager(m_lpFm);
	m_Head.AddFrame(pimghead);
	m_Head.SetFrameHotSpot(0,0,m_vHeadHotSpot.x,m_vHeadHotSpot.y); //giunto
	//cannoni
	m_Head.SetFrameHotSpot(0,1,m_vHeadCannons[0].x,m_vHeadCannons[0].y);
	m_Head.SetFrameHotSpot(0,2,m_vHeadCannons[1].x,m_vHeadCannons[1].y);
	//numero di volte che ogni elemento deve spostarsi con m_iangle_vel direzione
	//2=velocit�
	m_irepeat=(int)(abs(m_vBodyHotSpots[1].x-m_vBodyHotSpots[0].x)/m_ivelocity); 

	
	for (count=1;count<inframes;count++)
	{
		m_Head.AddFrame(&pimghead[count]);
		m_Head.RotateFrameHotSpot(count,0,0,(float)count*delta_angle);
		m_Head.RotateFrameHotSpot(count,1,1,(float)count*delta_angle);
		m_Head.RotateFrameHotSpot(count,2,2,(float)count*delta_angle);
	}

	
	for (count=0;count<m_iNumItems;count++)
	{
		pspr=(CADXSprite *)&m_pBody[count];
		pspr->SetFrameManager(m_lpFm);

		for (count1=0;count1<inframes;count1++)
		{
			pspr->AddFrame(&pimgbody[count1]);

			if (count1==0)
			{
				pspr->SetFrameHotSpot(count1,0,m_vBodyHotSpots[1].x,m_vBodyHotSpots[1].y);
				pspr->SetFrameHotSpot(count1,1,m_vBodyHotSpots[0].x,m_vBodyHotSpots[0].y);
			}
			else
			{
				an=count1*delta_angle;
				pspr->RotateFrameHotSpot(count1,0,0,(float)an);
				pspr->RotateFrameHotSpot(count1,1,1,(float)an);
			}	
		
		}
	}

	dwStatus=1;

	return S_OK;

}

void CSnake2::Reset(void)
{
	CEnemy::Reset();

	//regola la cattiveria del serpente
	switch(m_dwFlags)
	{
	case 1:

		SetVelocity(3);
		iConfigFreq=280;
		iFireFreq=3;
		dwScore=2500;
		break;

	case 2:

		SetVelocity(4);
		iConfigFreq=350;
		iFireFreq=2;
		dwScore=3000;
		break;	
	
	default:

		SetVelocity(3);
		iConfigFreq=180; 
		iFireFreq=4;
		dwScore=2000;
		break;
	}

	m_Head.SetVelocity(0,m_ivelocity);
	m_Head.dwPower=15;
	iStepPath=0;

	for (int count=0;count<m_iNumItems;count++)
	{			
		m_pBody[count].dwPower=dwPower;
		m_pBody[count].SetVelocity(0,m_ivelocity);	
		m_iangle_vel[count]=180;
	}

	m_iangle_vel[m_iNumItems]=180;
	iStepConfig=0;
	m_bIsClosing=FALSE;	

}

void CSnake2::SetPosition(const LONG x,const LONG y)
{
	LONG xj,yj;

	m_Head.SetJointPosition(x,y,0);
	m_Head.SetVelocity(180,m_ivelocity);
	m_Head.SetCurFrame(180/15);
	m_Head.GetJointPosition(&xj,&yj,0);


	for (int count=m_iNumItems-1;count>=0;count--)
	{
		//imposta la posizion orizzontale
		m_pBody[count].SetCurFrame(180/15); 
		//imposta la posizione del giunto in modo da collegarlo all'elemento precedente
		m_pBody[count].SetJointPosition(xj,yj,1);
		//acquisisce la posizione del giunto a cui collegare l'elemento successivo
		m_pBody[count].GetJointPosition(&xj,&yj,0);
		//imposta la velocit�
		m_pBody[count].SetVelocity(180,m_ivelocity);
	}
}

void CSnake2::Explode(void)
{
	if (lpfnExplode)
	{
		lpfnExplode(m_Head.x-10,m_Head.y-20);
		lpfnExplode(m_Head.x+20,m_Head.y+20);
		lpfnExplode(m_Head.x,m_Head.y);
		//fa esplodere tutta la catena
		for (int count1=0;count1<m_iNumItems;count1++)
		{
			m_pobj=&m_pBody[count1];				
			if (m_pobj->GetEnergy()>0) lpfnExplode(m_pobj->x,m_pobj->y);
			m_pobj->SetEnergy(0);
		}
	}

};

BOOL CSnake2::DoCollisionWith(CSBObject *pobj)
{
	//collisione con la testa della serpe
	m_pobj=&m_Head;

	if (m_pobj->DoCollisionWith(pobj))
	{	
		dwEnergy=m_pobj->GetEnergy();

		if (dwEnergy<=0)
		{
			
			this->Explode();			

			this->x=m_Head.x;
			this->y=m_Head.y;
			CreateBonus();
		}

		return TRUE;
	}

	//collisioni con il resto del corpo
	for (int count=m_iNumItems-1;count>=0;count--)
	{	
		m_pobj=&m_pBody[count];

		if (m_pobj->GetEnergy()>0)
		{
 			if (m_pobj->DoCollisionWith(pobj))
			{							
				if (m_pobj->GetEnergy()<=0)
				{
					if (lpfnExplode)
					{
					//esplode la catena di oggetti a valle di questo
						for (int count1=count;count1>=0;count1--)
						{
							m_pobj=&m_pBody[count1];
							lpfnExplode(m_pobj->x+10,m_pobj->y+10);
							m_pobj->SetEnergy(0);

						}
					}
				}

				return TRUE;
			}
		}
	}



	return FALSE;	

}


BOOL CSnake2::Update(void)
{
	int count;
	
	iStepPath++;

	if (iStepPath>m_irepeat)
	{
		int curan;
		int de;
		iStepPath=0; 
		//aggiorna la direzione di movimento della testa
		for (count=0;count<m_iNumItems;count++)
		{			
			m_ps=&m_pBody[count];
			m_iangle_vel[count]=m_iangle_vel[count+1];
			curan=m_iangle_vel[count];
			m_ps->SetVelocity(curan,m_ivelocity);
			m_ps->SetCurFrame(curan/15);
		}

		//algoritmo AI: insegue il player
		if (!m_bIsClosing)
		{
			int an=m_pmath->GetAngle(m_Head.x,m_Head.y,lTargetX,lTargetY);
			curan=m_Head.GetAngle();

			de=abs(an-curan);
			if (de>35)
			{
				int da=m_pmath->GetAngleDiff(curan,an)*15;
				curan+=da;
				if (curan<0) curan=360+curan;
				else if (curan>360) 
				{
					curan%=360;
				}
				m_iangle_vel[m_iNumItems]=curan;
			}
			else
			{
				iStepFire++;

				if (iStepFire>=iFireFreq)
				{
					if (m_Cartridges)
					{
						CShell *pShell;
						LONG xf,yf;
						int lambda;

						pShell=(CShell *)m_Cartridges->Pop();
						if (pShell)
						{
							g_cs.PlaySound(g_snBfire,0,0);	
							m_Head.GetJointPosition(&xf,&yf,1);						
							lambda=m_Head.GetCurFrame();
							pShell->SetCurFrame(lambda);						
							pShell->SetJointPosition(xf,yf,0);
							pShell->SetVelocity(lambda*15,10),
							pShell->dwClass=CL_ENFIRE;
							pShell->SetEnergy(1);
							pShell->dwPower=12;
							m_ActiveStack->Push(pShell);

							pShell=NULL;
							pShell=(CShell *)m_Cartridges->Pop();

							//secondo proiettile
							if (pShell)
							{
								m_Head.GetJointPosition(&xf,&yf,2);	
								pShell->SetJointPosition(xf,yf,0);
								pShell->SetVelocity(lambda*15,10),
								pShell->SetCurFrame(lambda);
								pShell->dwClass=CL_ENFIRE;
								pShell->SetEnergy(1);
								pShell->dwPower=12;
								m_ActiveStack->Push(pShell);
							}
						}	

					}//fine if m_Cartridges
				}
			}			

		}
		else
		{
			if (iStepConfig>iConfigFreq+30) 
			{
				return FALSE; //elimina l'oggetto dallo stack attivo
			}
		}

		m_Head.SetVelocity(curan,m_ivelocity);
		m_Head.SetCurFrame(curan/15);

		iStepConfig++;
		m_bIsClosing=(iStepConfig>iConfigFreq); //dopo un po' esce di scena

	}

	m_Head.UpdatePosition();

	for (count=0;count<m_iNumItems;count++)
	{
		m_pBody[count].UpdatePosition();		
	}    
	
	return (dwEnergy>0);
}

HRESULT CSnake2::Draw(void)
{

	m_Head.SetDisplay(m_pimgOut);
	m_Head.Draw();

	//l'elemento con indice piu' alto � quello attaccato alla testo;l'elemento con indce 0 � la coda
	for (int count=m_iNumItems-1;count>=0;count--)
	{
		m_pobj=&m_pBody[count];
		if (m_pobj->GetEnergy()<=0) break;
		m_pBody[count].SetDisplay(m_pimgOut);
		m_pBody[count].Draw();
	}


	return S_OK;
}

//imposta l'energia di ogni elemento del corpo+la testa
void CSnake2::SetEnergy(LONG energy)
{
	dwEnergy=energy;
	m_Head.SetEnergy(energy);	
    for (int count=0;count<m_iNumItems;count++) m_pBody[count].SetEnergy((LONG)(energy*0.5f));

}

//imposta gli hot spot delle bocche di fuoco della testa
void CSnake2::SetCannonsHotSpot(int x1,int y1,int x2,int y2)
{
	m_vHeadCannons[0].x=x1;
	m_vHeadCannons[0].y=y1;
	m_vHeadCannons[1].x=x2;
	m_vHeadCannons[1].y=y2;
}

//imposta gli hot spot del corpo, al giunto x2,y2 viene attaccato l'elemento successivo mentre a x1 y1 il precedente
void CSnake2::SetBodyHotSpots(int x1,int y1,int x2,int y2)
{
	m_vBodyHotSpots[0].x=x1;
	m_vBodyHotSpots[0].y=y1;
	m_vBodyHotSpots[1].x=x2;
	m_vBodyHotSpots[1].y=y2;
	m_bBodyHotSpots=TRUE;
}

void CSnake2::SetHeadHotSpot(int x1,int y1)
{
	m_vHeadHotSpot.x=x1;
	m_vHeadHotSpot.y=y1;
	m_bHeadHotSpot=TRUE;
}

//impostare un valore delle velocit� pari
void CSnake2::SetVelocity(int ivel)
{
	if (ivel>0) 
	{
		m_ivelocity=ivel;
		m_irepeat=(int)(abs(m_vBodyHotSpots[1].x-m_vBodyHotSpots[0].x)/m_ivelocity); 
	}

}

