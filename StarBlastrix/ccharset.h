/*   Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
*/

#ifndef _CADX_CHARSET_INCLUDE_
#define _CADX_CHARSET_INCLUDE_

#include "a32graph.h"

/*Classe CharSet
 
   La tabella che contiene i caratteri � un immagine ed ogni carattere � all'interno
   di una griglia le cui dimensioni sono rappresentate dai membri m_lCharWidth e m_lCharHeight
   i caratteri vengono caricati all'interno di un vettore di immagini
*/


class CADXCharSet : public virtual CADXDebug,public virtual CADXBase
{

protected:
	            
	LONG m_lCharWidth;   //larghezza ed altezza in pixel di ogni carattere
	LONG m_lCharHeight;
	LONG m_lNumCharX;
	LONG m_lNumCharY;
	LONG m_lVertexGridX;
	LONG m_lVertexGridY;
	LONG m_lCharSpacing; //spazio fra i caratteri
	int m_iScale; //scala con cui vengono disegnati i caratteri, 1 indica scala naturale
	IMAGE_FRAME m_imgCharTable; //immagine che contiene i caratteri
    IMAGE_FRAME m_imgChars[256];//vettore che contiene i caratteri 
	int m_iFirstAscii;  //codice del primo carattere ascii dal quale inizio a caricare
	
public:

	CADXCharSet(void); //si possono usare due costruttori diversi
	
	CADXCharSet(CADXFrameManager *fm);
	
	~CADXCharSet(void);
	
	HRESULT SetScale(int iScale); //scala 100 indica dimensioni normali
	
	int GetScale(void);
	
	HRESULT SetCharSpacing(LONG lcs); //imposta e restituisce charspacing
	
	LONG GetCharSpacing(void);
	
	HRESULT LoadCharSet(IMAGE_FRAME_PTR img,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY); //carica i caratteri grabbandoli da una immagine gi� in memoria
	
	HRESULT LoadCharSet(TCHAR *szBmp,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY); //carica i caratteri grabbandoli da un bitmap
    
	HRESULT LoadCharSet(TCHAR *szVPX,DWORD dwImg,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY); //carica i caratteri grabbandoli da un file compresso in formato VPX
	
	void FreeCharTable(void); //rilascia i caratteri

	void CDECL TextOut(int xout,int yout,TCHAR *szFormat, ...); //stampa una stringa formattata
	
	void CDECL TextOut(int xout,int yout,IMAGE_FRAME_PTR img,TCHAR *szFormat, ...); //stampa una stringa formattata

	void TextOutFast(int xout,int yout,IMAGE_FRAME_PTR imgDest,TCHAR *szText); //stampa una stringa non formattata

	LONG GetCharWidth(void);
	
	LONG GetTextWidth(TCHAR *szText);
	
	LONG GetCharHeight(void);
	
};

#endif