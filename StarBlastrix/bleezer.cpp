//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  bleezer.cpp -- 
///////////////////////////////////////////////////////////////////////*/

#include "bleezer.h"
#include "sbengine.h"
#include "a32util.h" //utility per l'engine arcade 32
#include "a32object.h"
#include "thunder.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
//stack oggetti attivi
extern CObjectStack g_objStack;
extern CObjectStack g_sShellBlue;
extern CObjectStack g_sEShell5;
extern CObjectStack g_sThunderBolt;
extern CADXSound g_cs;
//p. laser orientabile
extern void PushExplMulti(CADXSprite*,int);
extern void PushExpl9(LONG x,LONG y);
extern LONG g_snFire9,g_snFire8,g_snFire10;

#ifdef _TRACE_

extern BOOL g_bTraceOn;

#endif

static BOOL g_bBleezerResLoaded=FALSE;
IMAGE_FRAME g_imgBleezer,g_imgBleezerHelper;
BOUND_POLY g_bpBleezer,g_bpBleezerHelper;
CObjectStack g_sElectro;

HRESULT LoadBleezerRes(CResourceManager* prm)
{
	if (g_bBleezerResLoaded) return S_OK;
		
	if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

	CADXFrameManager* fm=prm->GetFrameManager();

	if (NULL==fm) return E_FAIL;

	//immagine corrente
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (FAILED(fm->GrabFrame(&g_imgBleezer,806,0,933,121,pimg))) return E_FAIL;
	//satellite
	if (FAILED(fm->GrabFrame(&g_imgBleezerHelper,959,1,1015,57,pimg))) return E_FAIL;

	g_bpBleezer.poly=new VERTEX2D[37];

	BOUND_POLY_PTR bp=&g_bpBleezer;
	bp->inum=0;

    AddVertex(bp,0,42);AddVertex(bp,0,32);AddVertex(bp,46,31);
	AddVertex(bp,53,27);AddVertex(bp,44,17);AddVertex(bp,45,0);
	AddVertex(bp,65,0);AddVertex(bp,77,15);AddVertex(bp,77,19);
	AddVertex(bp,94,19);AddVertex(bp,109,0);AddVertex(bp,127,0);
	AddVertex(bp,126,18);AddVertex(bp,113,33);AddVertex(bp,118,52);
	AddVertex(bp,117,72);AddVertex(bp,105,82);AddVertex(bp,109,90);
	AddVertex(bp,126,105);AddVertex(bp,126,121);AddVertex(bp,109,123);
	AddVertex(bp,92,105);AddVertex(bp,79,106);AddVertex(bp,63,123);
	AddVertex(bp,46,122);AddVertex(bp,46,104);AddVertex(bp,61,91);
	AddVertex(bp,62,81);AddVertex(bp,38,91);AddVertex(bp,0,91);
	AddVertex(bp,0,79);AddVertex(bp,43,79);AddVertex(bp,37,70);
	AddVertex(bp,4,70);AddVertex(bp,5,54);AddVertex(bp,40,52);
	AddVertex(bp,40,42);

	//poligono di contenimento del satellite
	bp=&g_bpBleezerHelper;
	bp->inum=0;

	bp->poly=new VERTEX2D[20];
	AddVertex(bp,0,28);AddVertex(bp,4,20);AddVertex(bp,14,20);AddVertex(bp,19,13);
	AddVertex(bp,19,3);AddVertex(bp,28,0);AddVertex(bp,37,3);AddVertex(bp,37,14);
	AddVertex(bp,44,21);AddVertex(bp,53,22);AddVertex(bp,57,29);AddVertex(bp,51,38);
	AddVertex(bp,41,38);AddVertex(bp,36,43);AddVertex(bp,36,52);AddVertex(bp,28,56);
	AddVertex(bp,19,51);AddVertex(bp,20,42);AddVertex(bp,13,38);AddVertex(bp,2,38);	

	bp=NULL;

	g_bBleezerResLoaded=TRUE;

	g_sElectro.Create(24);

	CElectro *pm=NULL;

	for (int count=0;count<24;count++)
	{		
		
		g_sElectro.m_objStack[count]=new CElectro;
		pm=(CElectro *)g_sElectro.m_objStack[count];
		pm->bActive=FALSE;
		pm->SetGraphicManager(prm->m_lpGm);
		pm->SetFrameManager(prm->m_lpFm);
		pm->SetUpdateFreq(40);
		pm->SetEnergy(25);
		pm->dwPower=10;
		pm->dwResId=0;
		pm->lpfnExplode=PushExpl9;
		pm->pfnCollide=PushCollideMetal;
		pm->SetActiveStack(&g_objStack);
		pm->SetCentralHotSpot(0);
		pm->AddFrame(&g_imgBleezerHelper);
	
	}

	pm=NULL;

	return S_OK;

}

void FreeBleezerRes(void)
{
	if (g_bBleezerResLoaded)
	{
		g_fm.FreeImgFrame(&g_imgBleezerHelper);
		g_fm.FreeImgFrame(&g_imgBleezer);

		SAFE_DELETE_ARRAY(g_bpBleezer.poly);
		g_bpBleezer.inum=0;

		SAFE_DELETE_ARRAY(g_bpBleezerHelper.poly);
		g_bpBleezer.inum=0;

		g_sElectro.Clear(TRUE);
		
		g_bBleezerResLoaded=FALSE;

	}

}

//-------------------------------------- CElectro -------------------------------------------------

CElectro::CElectro(void)
{
	dwClass=CL_ENEMY;
	dwType=44;
	m_ichotspot=-1;
	m_centerx=m_centery=0.0f;
	
}

void CElectro::Reset(void)
{
	//questo oggetto deve sempre essere vincolato ad un oggetto
	//genitore rispetto al quale � un satellite
	SetPosition(0,0);
	pParent=NULL;
	m_ralpha=0;
	this->SetAlphaStep(1);	
	//resetta i parametri di moto
	this->SetParams(0,0,0,0,0);
	//oggetto holder position (deve essere attivo per fare da base al fulmine)
	pholder.bActive=TRUE; 
	//resetta la variazione dei parametri di moto
	this->ResetDelta();
	iUpdateFreq=10;
	

}

//alpha corrente in radianti

float CElectro::GetAlpha(void)
{
	return m_ralpha;
}

//step alpha in gradi

int CElectro::GetAlphaStep(void)
{
	return m_alpha_step;
}

void CElectro::SetAlphaStep(int step)
{
	m_alpha_step=step;
	m_ralpha_step=DEG_TO_RADIANS*m_alpha_step;
}

float CElectro::GetRx(void)
{
	return Rx;
}

float CElectro::GetRy(void)
{
	return Ry;
}

float CElectro::GetKx(void)
{
	return Kx;
}

float CElectro::GetKy(void)
{
	return Ky;
}

BOOL CElectro::Update(void)
{
	if (++iStepUpdate>iUpdateFreq)
	{
		iStepUpdate=0;		

		//varia i parametri di moto se non siamo ancora alla configurazione di default
		if (m_targetStep<m_targetEnd)
		{

#ifdef _TRACE_

			if (g_bTraceOn)
			{
				g_bTraceOn=TRUE;
			}

#endif

			m_targetStep++;

			Rx+=dRx;
			Ry+=dRy;
			Kx+=dKx;
			Ky+=dKy;
			m_ralpha_step+=dAlphaStep;
		}

	}	

	m_ralpha+=m_ralpha_step;

	x=(LONG)(Rx*cos(Kx*m_ralpha+m_rphi)+m_centerx);
	y=(LONG)(Ry*sin(Ky*m_ralpha+m_rphi)+m_centery);

	
	if (!pParent) return FALSE;

	if (pParent->GetEnergy()<=0) 
	{
		this->Explode();
		return FALSE;
	}

	int xs,ys;

	//aggiorna l'oggetto position holder (serve solo come oggetto di appoggio per il fulmine)
	this->GetAbsXY(&xs,&ys);

	pholder.SetPosition(xs,ys);

	return (dwEnergy>0);
}


//Imposta i parametri del moto armonico
void CElectro::SetParams(float rx,float ry,float kx,float ky,float phi)
{
	this->Rx=rx;
	this->Ry=ry;
	this->Kx=kx;
	this->Ky=ky;
	this->phi=phi;
	this->m_rphi=DEG_TO_RADIANS*phi;
}

void CElectro::SetTargetParams(float trx,float tr,float tkx,float tky,float talpha,int steps)
{

		m_targetEnd=steps; //step necessari a raggiungere la configurazione target
		m_targetStep=0;

		if (trx!=0.0f) dRx=(trx-Rx)/(float)steps;
		else dRx=0.0f;

		if (tr!=0.0f) dRy=(tr-Ry)/(float)steps;
		else dRy=0.0f;

		if (tkx!=0.0f) dKx=(tkx-Kx)/(float)steps;
		else dKx=0.0f;

		if (tky!=0.0f) dKy=(tky-Ky)/(float)steps;
		else dKy=0.0f;

		if (talpha!=0.0f) dAlphaStep=(talpha*DEG_TO_RADIANS-m_ralpha_step)/(float)steps;
		else dAlphaStep=0.0f;

}

//Restituisce TRUE se ha raggiunto la configurazione target
BOOL CElectro::ParamConfigComplete(void)
{
	return (m_targetStep>=m_targetEnd);
}

void CElectro::ResetDelta(void)
{
	m_targetStep=m_targetEnd=0;
	dRx=dRy=dKx=dKy=dAlphaStep=0.0f;
}

/*


	int m_alpha_step; //step in gradi dell'angolo
	int m_ichotspot; //hot spot del parent intorno al quale ruotano i satelliti
	float m_ralpha; //alfa in radianti
	float m_ralpha_step; //step alfa in radianti	
	float m_rphi;
	float Rx,Ry,Kx,Ky,phi;
	float m_centerx,m_centery;
	//delta per i parametri di moto
	float dRx,dRy,dKx,dKy,dAlphaStep;
	int m_targetStep;
	int m_targetEnd;
	
public:
	
	CSBObject pholder;
	void SetParams(float rx,float ry,float kx,float ky,float phi);	
	void SetTargetParams(
	float GetAlpha(void);
	int GetAlphaStep(void);
	void SetAlphaStep(int step);	
	void SetCentralHotSpot(int hotspot);
	float GetRx(void);
	float GetRy(void);
	CElectro(void);
	BOOL Update(void);
	void Reset(void);
	void ResetDelta(void);
	

};
*/


void CElectro::SetCentralHotSpot(int hotspot)
{
	m_ichotspot=hotspot;

	if (m_ichotspot<0 || pParent==NULL)
	{
		m_centerx=m_centery=0.0f;
	}
	else 
	{
		LONG x1,y1;
		pParent->GetFrameHotSpot(0,m_ichotspot,&x1,&y1);
		m_centerx=(float)x1;
		m_centery=(float)y1;
	}
}

//-------------------------------------- CBleezer -------------------------------------------------

CBleezer::CBleezer(void)
{
	dwClass=CL_ENEMY;
	
	dwType=43;

	for (int count=0;count<MAX_SAT;count++) m_psat[count]=NULL;
}

void CBleezer::Reset(void)
{
	CEnemy::Reset();
	SetVelocity(180,3);
	iStepConfig=0;
	iUpdateFreq=80;
	ttl=35; //"time to live"
	burst1=burst2=delay1=delay2=0;

	int phi=0;
	int dphi=(int)(360/MAX_SAT);

	//acquisisce la posizione del centro dell'oggetto
	GetFrameHotSpot(0,0,&m_cx,&m_cy); 


	for (int count=0;count<MAX_SAT;count++)
	{
		m_psat[count]=(CElectro *)g_sElectro.Pop();		
		
		if (m_psat[count])
		{		
			m_psat[count]->Reset();
			m_psat[count]->pParent=this;
			//imposta i parametri iniziali del moto armonico
			m_psat[count]->SetParams(20.0f,20.0f,1.0f,1.0f,(float)phi);	
			m_psat[count]->SetAlphaStep(2);
			m_psat[count]->SetEnergy(20);
			m_psat[count]->SetCentralHotSpot(0);

		
			phi+=dphi;	
			m_ActiveStack->Push(m_psat[count]);
		}
	}
}

void CBleezer::SetSatelliteTargetParams(float tRx,float tRy,float tKx,float tKy,float tAlphaStep,int steps)
{

	CElectro *pel;

	int satellite;

	//controlla che tutti i satelliti abbiano raggiunto la configurazione target
	for (satellite=0;satellite<MAX_SAT;satellite++)
	{
		pel=m_psat[satellite];

		if (pel && pel->bActive)
		{
			//configurazione non ancora raggiunta
			if (!pel->ParamConfigComplete()) return; 
		}
	}


	for (satellite=0;satellite<MAX_SAT;satellite++)
	{
		pel=m_psat[satellite];

		if (pel && pel->bActive)
		{
			pel->SetTargetParams(tRx,tRy,tKx,tKy,tAlphaStep,steps);

		}
	}
}

BOOL CBleezer::Update(void)
{
	if (++iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;

		int iprob=g_fmath.RndFast(8);
		int an;
		iUpdateFreq=70;

		switch (++iStepConfig)
		{
			case 0:
			case 1:

				SetSatelliteTargetParams(200,200,0.0f,0.0f,0.0f,50);

				if (x<0)
				{
					SetVelocity(0,5);
				}
				else
				{
					SetVelocity(180,3);
				}
				break;

			case 2:

				switch(iprob)
				{
				case 0:
					SetSatelliteTargetParams(200,50,0.0f,0.0f,0.0f,120);
					break;
				case 1:
					SetSatelliteTargetParams(50,210,0.0f,0.0f,0.0f,90);
					break;
				case 2:
					an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);
					SetVelocity(an,2);
					SetSatelliteTargetParams(150,200,0.0f,0.0f,0.0f,80);
					break;
				case 3:
					SetSatelliteTargetParams(40,240,0.0f,0.0f,0.0f,80);
					break;
				default:
					SetVelocity(0,2);
					break;

				}
				
				break;

			case 3:

				SetSatelliteTargetParams(200,100,0.0f,0.0f,0.0f,100);
				SetVelocity(270,1);
				break;

			case 4:
			case 5:

				SetSatelliteTargetParams(200,200,0.0f,0.0f,0.0f,150);
				iUpdateFreq=120;
				SetVelocity(0,0);
				break;

			default:

			
				an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);
				SetVelocity(an,2);
				iStepConfig=0;

				break;
		}

		//esce di scena
		if (--ttl<=0) SetVelocity(180,4);

	}

	//contatore per raffiche
	if (burst1>0)
	{
		if (++delay1>=8)
		{
			delay1=0;
			burst1--;
			FireCentral();
		}
	}


	if (burst2>0)
	{
		if (++delay2>=8)
		{
			delay2=0;
			burst2--;
			FireCannons();
		}
	} 

	if (++iStepFire>iFireFreq)
	{
		iStepFire=0;

		int prob=g_fmath.RndFast(24);

		switch (prob)
		{
		case 1:

			FireThunder(0,35);
			FireThunder(1,35);
			FireThunder(2,35);
			FireThunder(3,35);			

	      break;

		case 2:

			FireThunder(0,18);
			FireThunder(2,18);
			break;

		case 3:

			FireThunder(0,140);
			FireThunderToPlayer(3);
			break;

		case 4:

			FireThunder(0,95);
			FireThunder(1,95);
			FireThunder(2,95);
			FireThunder(3,95);	
			break;

		case 5:

			FireThunder(0,40);
			FireThunder(1,15);
			FireThunder(2,15);
			FireThunder(3,40);			
			break;

		case 6:

			FireThunder(0,10);
			FireThunder(1,10);
			FireThunder(2,10);
			FireThunder(3,10);
			break;
		  

		  case 7:

			  FireThunderToPlayer(1);
			  FireThunderToPlayer(3);
			  break;

		  case 8:

			  if (lTargetX<x-80)
			  { 
                if (!burst1)
				{
					delay1=0;
					burst1=5; //raffica di 5 colpi
					FireCentral();
				}

				if (!burst2)
				{
					delay2=0;
					burst2=4;
					FireCannons();
				}
			  }
			  
			  break;

		  case 9:

			  if (lTargetX<x-80 && !burst2)
			  { 				
				FireCannons();
			  }
			  
			  break;

		  case 10:

			  if (lTargetX<x-80 && !burst1)
			  { 				
				FireCentral();
			  }

			  break;

		  case 11:

			  if (lTargetX<x-80 && !burst1)
			  { 				
                burst1=8;
				delay1=0;
				FireCentral();
			  }
			  break;

		  case 12:

			  if (lTargetX<x-80 && !burst2)
			  { 				
                burst2=8;
				delay2=0;
				FireCannons();
			  }
			  
			  break;

		  default:
			  

		  break;
		}//fine switch
	}

	UpdatePosition();

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return (x>-300 && x<CSBObject::g_iScreenWidth+300 && y>-300 && y<CSBObject::g_iScreenHeight);

}

//spara con il cannone centrale
void CBleezer::FireCentral(void)
{
	
	CShell* p=(CShell*)m_Cartridges->Pop(); 

	if (p)
	{

		p->Reset();
		p->SetCurFrame(0);				
		p->SetEnergy(2);
		p->dwPower=10;
		p->dwClass=CL_ENFIRE;
		LONG xf,yf;
		GetJointPosition(&xf,&yf,6);
		p->SetPosition(xf,yf);
		p->SetVelocity(180,12);

		if (m_ActiveStack)
		{
			m_ActiveStack->Push(p);
			g_cs.PlaySound(m_lFireSound,0,0);
		}
	}  

}

//Spara con i cannoni ausiliari
void CBleezer::FireCannons(void)
{
	if (!m_pWeapon1) return;

	CShell* p=(CShell*)m_pWeapon1->Pop(); 

	LONG xf,yf;

	if (p)
	{

		p->Reset();
		p->SetCurFrame(0);				
		p->SetEnergy(2);
		p->dwPower=10;
		p->dwClass=CL_ENFIRE;		 
		GetJointPosition(&xf,&yf,5); //cannone in alto
		p->SetPosition(xf,yf);
		p->SetVelocity(180,9);

		if (m_ActiveStack)
		{
			m_ActiveStack->Push(p);
			g_cs.PlaySound(m_lFireSound,0,0);
		}
	} 

	p=(CShell*)m_pWeapon1->Pop(); 

	if (p)
	{

		p->Reset();
		p->SetCurFrame(0);				
		p->SetEnergy(2);
		p->dwPower=10;
		p->dwClass=CL_ENFIRE;	
		GetJointPosition(&xf,&yf,7); //cannone in basso
		p->SetPosition(xf,yf);
		p->SetVelocity(180,9);

		if (m_ActiveStack)
		{
			m_ActiveStack->Push(p);
			g_cs.PlaySound(m_lFireSound,0,0);
		}
	} 

}

void CBleezer::FireThunderToPlayer(int hotspot)
{
	if (m_pWeapon2)
	{
		
			CElectro *pel;

			pel=m_psat[hotspot-1];

			if (pel->bActive)
			{
				//spara contro il player solo se il satellite � attivo

				//fulmine
				CThunderBolt *pTh=(CThunderBolt*) m_pWeapon2->Pop();

				if (pTh)
				{				

					LONG xp,yp;

					//ottiene l'hot dpot di una delle punte
					this->GetFrameHotSpot(0,hotspot,&xp,&yp);

					pTh->Reset();
					pTh->dwClass=CL_ENFIRE;
					pTh->AddTargetXY(lTargetX+15,lTargetY+15);
					pTh->SetSource(this,xp,yp);
					pTh->dwClass=CL_ENFIRE;
					pTh->SetDuration(25);			
					g_cs.PlaySound(g_snFire10,0,0);
					m_ActiveStack->Push(pTh);			
				}

			}

	}

}

/*
 Genera un fulmine fra uno dei satelliti e l'astronave
*/
	
void CBleezer::FireThunder(int satellite,int thduration)
{
	if (satellite>=MAX_SAT) return;

	CElectro *pel;

	pel=m_psat[satellite];

	if (pel && pel->bActive && (pel->GetRx()>180.0f || pel->GetRy()>180.0f))
	{
		if (m_pWeapon2)
		{
			//fulmine
			CThunderBolt *pTh=(CThunderBolt*) m_pWeapon2->Pop();

			if (pTh)
			{				

				LONG xp,yp;

				//ottiene l'hot dpot di una delle punte
				this->GetFrameHotSpot(0,satellite+1,&xp,&yp);

				pTh->Reset();
				pTh->dwClass=CL_ENFIRE;
				pTh->AddTarget(&pel->pholder);
				pTh->SetSource(this,xp,yp);
				pTh->dwClass=CL_ENFIRE;
				pTh->SetDuration(thduration);			
				g_cs.PlaySound(g_snFire10,0,0);
				m_ActiveStack->Push(pTh);			
			}

		}

	}

};


HRESULT CreateBleezerStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	if (!prm) return E_FAIL;

	CBleezer* pm=NULL;

	if (!pstk) return E_FAIL;
    //carica le risorse per il serpente di tipo 2
	if (FAILED(LoadBleezerRes(prm))) return E_FAIL;
	
	if (FAILED(pstk->Create(inumitems))) return E_FAIL;	

	for (DWORD count=0;count<inumitems;count++)
	{

		pstk->m_objStack[count]=new CBleezer;
		pm=(CBleezer*)pstk->m_objStack[count];
		pm->bActive=FALSE;
		pm->SetGraphicManager(prm->m_lpGm);
		pm->SetFrameManager(prm->m_lpFm);
		pm->SetUpdateFreq(40);
		pm->SetEnergy(25);
		pm->dwPower=10;
		pm->dwScore=5000;
		pm->dwResId=EN_BLEEZER;
		pm->lpfnExplode=PushExpl9;
		pm->pfnCollide=PushCollideMetal;
		pm->SetActiveStack(&g_objStack);		
		pm->SetCartridgeStack(&g_sShellBlue); //proiettili rossi	
		pm->SetFireSound(g_snFire9);
		pm->SetWeapon1(&g_sEShell5);
		pm->SetWeapon2(&g_sThunderBolt);
		pm->AddFrame(&g_imgBleezer);
		pm->SetBoundPoly(0,&g_bpBleezer);	
	
		pm->SetFrameHotSpot(0,0,83-45,63-28); //centrale
		pm->SetFrameHotSpot(0,1,48,1);  //punta 1
		pm->SetFrameHotSpot(0,2,126,1); //punta 2
		pm->SetFrameHotSpot(0,3,127,122); //punta 3
		pm->SetFrameHotSpot(0,4,47,122); //punta 4
		pm->SetFrameHotSpot(0,5,4,37); //cannone in alto
		pm->SetFrameHotSpot(0,6,10,62); //cannone medio
		pm->SetFrameHotSpot(0,7,4,87); //cannone in basso
	
	}

	return S_OK;
}




