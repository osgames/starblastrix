//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  ring.h -- anello nemico
///////////////////////////////////////////////////////////////////////*/

#ifndef _MAMMOTH_INCLUDE_
#define _MAMMOTH_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "resman.h"
#include "enemy.h"
#include "sbengine.h"


HRESULT LoadMammothRes(CResourceManager* prm);
void FreeMammothRes(void);
HRESULT CreateMammothStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);

class CMammoth:public CEnemy
{
private:


	RECT rcLimitBox;
	BOOL bFollowPlayer;
	//indica che c'� un box di limite movimento attivo
	BOOL bBoxActive;
	int burst_cnt;  //raffica arma primaria
	int burst_cnt1; //raffica arma secondaria
	int m_trim;
	int	m_iHFlamesJoint;
	CObjectStack *m_pHNozzleFlames;
	LONG m_lNozzleNoise;
	CAnimatedObject* m_pHFlames;
	BOOL m_bDoHFlames;
	int m_ttl;

public:

	CMammoth(void);
	void Reset(void);
	BOOL Update(void);
	void Explode(void);
	void SetHNozzleFlamesStack(CObjectStack *pflames,int hotspot);
	void SetNozzleNoise(LONG sound);
	HRESULT Draw(void);	

};


#endif