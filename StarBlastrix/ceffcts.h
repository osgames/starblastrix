////////////////////////////////////////////////////////////////////////
//// Effetti grafici di transizione
//// Code by L.Berti 2002-2006
/////////////////////////////////////////////////////////////////////////

#ifndef _CEFFCTS_INCLUDE_
#define _CEFFCTS_INCLUDE_

#define WIN32_LEAN_AND_MEAN   

#include "a32graph.h"         //libreria grafica 2D arcade32
#include "a32fastm.h"         //funzioni matematiche 

//Classe base per tutti gli effetti
//La classe CADXEffcts ha lo scopo di applicare una trasformazione geometrica
//o grafica all'immagine sorgente e copiare il risaultato su quella di destinazione
//L'immagine sorgente non viene alterata
//Per impostare l'immagine sorgente usare il metodo SetSourceImg
//per impostare l'immagine su cui verr� copiato l'out put usare il metodo ereditato SetDisplay 
//ATTENZIONE! L'immagine sorgente e quella destinazione non possono coincidere !!!!

class CADXEffect:public virtual CADXRender
{
protected:

	int m_iSteps; //numero di passi da eseguire prima di terminare l'effetto
	IMAGE_FRAME_PTR m_pimgSrc; //immagine sorgente	
	LONG m_lPixelSize;//dimensione di un pixel nella modalit� grafica impostata       
	BYTE *m_pSrcBuff,*m_pDestBuff; //buffer immagine sorgente e destinazione
	LONG m_lPitchSrc,m_lPitchDest; //pitch della superficie sorgente e destinazione
    LONG m_lSrcWidth,m_lSrcHeight;
    LONG m_lDestWidth,m_lDestHeight;
	LONG m_lDestBuffSize; //dimensione del buffer di destinazione in bytes
	LONG m_lSrcBuffSize; //dimensione buffer immagine originale

	HRESULT GetBuffs(void); //blocca le superfici e ottiene i puntatori pImgSrc e pImgDest
	HRESULT ReleaseBuffs(void); //sblocca le superfici

public:

	CADXEffect(void);
	~CADXEffect(void);
	HRESULT SetSourceImg(IMAGE_FRAME_PTR imgSource);	//imposta l'immagine sorgente della trasformazione
	HRESULT SetDisplay(IMAGE_FRAME_PTR imgDisplay);     //imposta l'immagine di output della trasformazione
	int GetSteps(void); //restituisce il numero di volte che � stata eseguita la trasformazione sull'immagine di origine
	virtual void Clear(void); //resetta i paramatri  

	//per esegure la trasformazione a partire dall'immagine sorgente
	//chiamare il metodo RenderToDisplay
};


class CADXBubble:public virtual CADXEffect,private virtual CADXFastMath
{
public:	
//	CADXBubble(void);
//	~CADXBubble(void);
	virtual HRESULT RenderToDisplay(void);
//	virtual void Clear(void);
};

class CEffectZ:public virtual CADXEffect,private virtual CADXFastMath
{
protected:

	BOOL m_bInit;
	LONG m_lHalfW,m_lHalfH;

public:	

    CEffectZ(void);
	virtual void Clear(void);
	virtual HRESULT RenderToDisplay(void);
};

class CEffectIn:public virtual CADXEffect,private virtual CADXFastMath
{
protected:

	BOOL m_bInit;
	LONG m_lHalfW,m_lHalfH;

public:	

    CEffectIn(void);
	virtual void Clear(void);
	virtual HRESULT RenderToDisplay(void);
};
#endif