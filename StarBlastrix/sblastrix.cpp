////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
///////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////
/* File sorgenti che compongono il progetto:
/*  
/* sblastrix.cpp arcade32.cpp 
/*
/* File header :
/*
/*
/* Librerie da includere nel progetto:
/*
/* kernel32.lib user32.lib gdi32.lib winspool.lib 
/* advapi32.lib shell32.lib ole32.lib oleaut32.lib 
/* uuid.lib ddraw.lib dxguid.lib dinput.lib dsound.lib winmm.lib  
/*
/*
/*
//////////////////////////////////////////////////////////////////////////////

//-------------------------- sblastrix.cpp : modulo principale ---------------------------              

////////////////////////////////// INCLUDES ///////////////////////////////////////////*/


#define WIN32_LEAN_AND_MEAN

                              //non includer� alcuni header che non sono necessari come dde.h dlgs.h ecc. ecc.

#include <windows.h>          //contiene definizioni base per windows
#include <windowsx.h>         //contiene la definizione di alcune macro importanti in windows
#include <mmsystem.h>
#include <iostream.h>  
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <math.h>
#include <io.h>
#include <fcntl.h>
#include <tchar.h>
#include "a32graph.h" //libreria grafica 2D arcade 32

//nota: la seguente definizione � stata necessaria per 
//compilare con l'sdk directx 9
//utilizza la versione 7 di direct input
#define DIRECTINPUT_VERSION 0x0700

#include <dinput.h>           //include direct input
#include "a32audio.h"
#include "a32input.h"
#include "ccharset.h"
#include "sbutil.h"
#include "ceffcts.h"
#include "sbengine.h"
#include "a32img16.h"
#include "resource.h"
#ifdef _DEBUG_
#include <crtdbg.h>
#endif

////////////////////////// DEFINES ////////////////////////////////////

#define WINDOW_WIDTH    64
#define WINDOW_HEIGHT   48
#define WINDOW_CLASS_NAME "WINXCLASS"  // class name


/////////////////////////// GLOBALS ///////////////////////////////////


HWND g_hwnd=NULL;          //handle della finestra principale
HINSTANCE g_instance;      //instanza pricipale
BOOL g_bExitLoop=FALSE;
TCHAR g_szGameTitle[]=TEXT("Star Blastrix");
IMAGE_FRAME imgTitle;   //titolo
IMAGE_FRAME imgBackMenu; //sfondo del menu
IMAGE_FRAME m_img1,m_img2;
BOOL m_bDoLoop=FALSE;
TCHAR *g_szLoadCustomLevel=NULL; //quando viene impostato questo puntatore carica il livello custom
CSbForm *m_pActiveForm=NULL;

#define MAX_FILES 10

WIN32_FIND_DATA g_wfd[MAX_FILES]; //file dei livelli ausiliari

//#ifdef _TESTGAME_

//nella versione release usa err.txt per inviare messaggi di errore
CADXDebug g_cdb;

int ListFiles(LPCSTR szFileName,WIN32_FIND_DATA *dest,int maxfiles);

//impostazioni ambiente (contiene tutte le informazioni globali
//che definiscono le impostazioni correnti)
TP_ENVIRON g_Environ =
{
	0,
	EN_KEYBOARD,
#ifdef _DEBUG_
	FALSE,
#else
	TRUE,
#endif
	SCREEN_WIDTH,SCREEN_HEIGHT,16,
	FALSE,
	FALSE,
	DIK_LEFT,
	DIK_RIGHT,
	DIK_UP,
	DIK_DOWN,
	DIK_LCONTROL, //arma primaria
	DIK_SPACE,    //arma secondaria
	DIK_Z,         //arma speciale  
	GM_INITIALIZE,  //stato iniziale del gioco
	0,              //dwLimit 
	1,               //lingua
	0,               //punteggio
	FALSE             //errore critico
};

CADXGraphicManager g_gm;  //gestore grafica direct draw
CADXFrameManager g_fm;    //gestore immagini e sprite 
CADXSound g_cs;           //gestore effetti sonori
CADXMusic g_cm;           //gestore file MIDI
CADXInput g_ci;           //gestione device di input
CADXFastMath g_fmath;     //engine matematico
CADXCharSet g_chRed;      //caratteri arancioni
CADXCharSet g_chBlue;     //caratteri blu
CADXCharSet g_chLittleBlue; //caatteri piccoli blu
CADXCharSet g_chLittleRed;  //caratteri piccoli rossi


//Menu principale
struct MainMenu_Tag
{
	IMAGE_FRAME imgCur[10]; //frame del selettore
	IMAGE_FRAME imgAux; //immagini ausiliarie
	IMAGE_FRAME imgHand; //	
	IMAGE_FRAME imgOn,imgOff; //immagini on e off per le opzioni
	IMAGE_FRAME imgButton; //bottone OK del menu
    CADXSprite sprCur;



	CADXSprite sprHand;
	RECT rc;
	CSbStaticText mnuStart,mnuConfig,mnuExit,mnuHelp,mnuConfigKeyb;
	CSbForm frmMain; //prima pagina del menu    
	CSbForm frmConfig; //pagina di configurazione
	CSbForm frmHelp; //help
	CSbForm frmCredits; //credits
	CSbForm frmHScore; //high score
	CSbForm frmControls;
	CSbForm frmMessage; //una semplice message form con un tasto ok
	CSbForm frmLanguage; //imposta la lingua
	CSbForm frmChooseLevel; //pagina di scelta del livello di partenza
	CSbForm frmChooseExtraLevel; //selezione livelli extra
	CSbOption mnuSound,mnuJoy,mnuKyb,mnuIt,mnuEn;
	CSbButton btuOk; //bottone ok della message form
	CSbStaticText mnuChooseTitle;
	CSbStaticText mnuLevel1,mnuLevel2,mnuLevel3,mnuLevel4,mnuLevel5,mnuLevel6,mnuLevel7,mnuLevel8; //livelli di partenza
	CSbStaticText mnuExtraLevel[10];
	CSbStaticText mnuCustomLevel; //livelli aggiuntivi
	CSbStaticText mnuBack;
	CSbStaticText mnuHelpTitle;
	CSbStaticText mnuCredits;
	CSbStaticText mnuCreditsTitle;
	CSbStaticText mnuCreditsL1;
	CSbStaticText mnuCreditsL2;
	CSbStaticText mnuHScore;
	CSbStaticText mnuLanguage;
	CSbStaticText mnuMessage,mnuMessage1; //messaggio usato dalla form frmMessage (1 e seconda riga)
	CSbStaticText mnuHScoreTitle;
	CSbInputText txtScore;
	CSbKeyCode mnuLeft,mnuUp,mnuRight,mnuDown,mnuFire1,mnuFire2,mnuFire3;
	CSbStaticText mnuSaveConfig,mnuDefaults;
	LONG lsndClick; //suono click

} strcMainMenu;

//High score
typedef struct HIGH_SCORE_TAG
{
	char szName[4];
	long lScore;

} HIGH_SCORE,*HIGH_SCORE_PTR;


#define HIGH_SCORE_COUNT 10

HIGH_SCORE m_HighScore[HIGH_SCORE_COUNT];

//LONG g_lCurrentScore=0; //punteggio riportato dal player alla fine del gioco
int m_iHighScoreIndex;

static LOCALE_TABLE g_Msg[]=
{
	TEXT("INIZIA PARTITA"),TEXT("NEW GAME"),   //0
    TEXT("OPZIONI"),TEXT("OPTIONS"),   //1 
	TEXT("AIUTO"),TEXT("HELP"),                //2 
	TEXT("CREDITS"),TEXT("CREDITS"),           //3
	TEXT("ESCI"),TEXT("QUIT"),                 //4 
    TEXT("PUNTEGGI"),TEXT("HIGH SCORES"),      //5
	TEXT("MUSICA"),TEXT("MUSIC"),              //6 
	TEXT("USA LA TASTIERA"),TEXT("KEYBOARD"),    //7
	TEXT("USA IL JOYSTICK"),TEXT("JOYSTICK"),    //8
	TEXT("INDIETRO<-"),TEXT("BACK<-"),                      //9 
	TEXT("CONFIG. CONTROLLI"),TEXT("INPUT SETUP"),             //10
	TEXT("SU"),TEXT("MOVE UP"),                             //11
	TEXT("GIU"),TEXT("MOVE DOWN"),                          //12
	TEXT("DESTRA"),TEXT("MOVE RIGHT"),                      //13
	TEXT("SINISTRA"),TEXT("MOVE LEFT"),                     //14
	TEXT("FUOCO"),TEXT("FIRE"),          //15 
	TEXT("BOMBE"),TEXT("BOMBS"),                            //16
	TEXT("ARMA SPECIALE"),TEXT("EXTRA WEAPON"),             //17 
	TEXT("SALVA"),TEXT("SAVE"),                             //18
	TEXT("RIPRISTINA DEFAULTS"),TEXT("RESTORE DEFAULTS"),   //19
	TEXT("Errore inizializzando la modalit� grafica.Occorre installare una scheda grafica Direct X compatibile."),TEXT("Video system initialization error.You need a DirectX compatible graphic card."),  //20
    TEXT("Errore inizializzando Direct Sound."),TEXT("Direct Sound initialization error."), //21
    TEXT("Errore inizializzando Direct Music."),TEXT("Direct Music initialization error."), //22 
    TEXT("Errore inizializzando Direct Input"),TEXT("Direct Input initialization error"),   //23 
	TEXT("Impossibile caricare le risorse dal file %s"),TEXT("Unable to load resources from file %s"),  //24
    TEXT("Impossibile acquisire l'immagine %d dal file %s."),TEXT("Cannot load the image #%d from file %s"), //25
	TEXT("CARICAMENTO IN CORSO..."),TEXT("LOADING..."),  //26
	TEXT("Errore caricando gli sprite comuni"),TEXT("Cannot load common sprites."),  //27
	TEXT("Errore durante l'inizializzazione degli effetti sonori"),TEXT("Unable to load some sound effects."), //28
	TEXT("Errore creando gli oggetti comuni"),TEXT("Cannot create common objects."),  //29
	TEXT("Errore inizializzando gli oggetti comuni"),TEXT("Unable to initialize common objects"),  //30
	TEXT("CONFIGURAZIONE SALVATA!"),TEXT("CONFIGURATION SAVED!"), //31
	TEXT("PUNTI"),TEXT("SCORE"), //32
	TEXT("USCIRE DAL GIOCO ?"),TEXT("QUIT THE GAME ?"), //33
	TEXT("ERRORE "),TEXT("ERROR "), //34
	TEXT("Errore inizializzando il set di caratteri %d"),TEXT("Charset #%d initialization error"), //35
	TEXT("Errore impostando il fattore di scala per il set di caratteri"),TEXT("Cannot set the aspect ratio for current charset"), //36
	TEXT("Impossibile caricare il set di caratteri %d dal file %s.\n\rControllare che il file sia presente."),TEXT("Cannot load charset #%d from file %s.\n\rThis file should exist."), //37
	TEXT("Errore caricando l'immagine del cursore animato (frame %d)"),TEXT("Error loading the animated cursor image (frame #%d)"),//38
	TEXT("Errore caricando l'immagine del puntatore del mouse"),TEXT("Error loading the mouse pointer image"), //39
	TEXT("Errore durante il ritaglio dell'immgine %s"),TEXT("Cannot grab the image %s"),//40
	TEXT("Errore inizializzando il menu principale"),TEXT("Cannot initialize the main menu"), //41
	TEXT("SI"),TEXT("YES"), //42
	TEXT("NO"),TEXT("NO"), //43
	TEXT("LINGUA"),TEXT("LANGUAGE"), //44
	TEXT("ITALIANO"),TEXT("ITALIANO"), //45
	TEXT("ENGLISH"),TEXT("ENGLISH"), //46
	TEXT("CONTINUARE ?"),TEXT("CONTINUE ?"), //47
	TEXT("SCEGLI LA MISSIONE"),TEXT("CHOOSE THE MISSION"), //48
	TEXT("LIVELLO BLOCCATO"),TEXT("THIS LEVEL IS CURRENTLY LOCKED!"), //49
	TEXT("COMPLETA I PRIMI 3 PER SBLOCCARLO"),TEXT("COMPLETE THE FIRST 3 LEVELS TO UNLOCK"), //50
	TEXT("PASSWORD NON VALIDA"),TEXT("INVALID PASSWORD"), //51
	TEXT("LIVELLI EXTRA"),TEXT("EXTRA LEVELS"), //52
	TEXT("Errore inizializzando il device audio"),TEXT("Can't initialize sound device"), //53
	TEXT("Errore inizializzando il device musicale"),TEXT("Can't initialize music device"), //54
	TEXT("Errore creando lo schema n� %d %s"),TEXT("Error creating level #%d %s"), //55
	TEXT("Si � verificato un errore critico, aprire il file err.txt per maggiori informazioni"),TEXT("Critical error.Open err.txt file for more info"), //56

};

static TCHAR g_szErrorMsg[2048];

#define NUM_MSG sizeof(g_Msg)/sizeof(g_Msg[0])

/////////////////////////////// EXTERNS ////////////////////////////

extern IMAGE_FRAME p1,pl[],es[];
extern LONG g_pbx,g_pby,g_pbw;
extern int g_iLevelSequence[]; 

///////////////////////////// FUNCTIONS ///////////////////////////


HRESULT InitMainMenu(void);
void FreeMainMenu(void);
HRESULT InitGame(HWND hwnd);     //Inizializza il gioco
HRESULT InitGraphics(HWND hwnd); //Inizializza Direct Draw
HRESULT InitSound(HWND hwnd);    //Inizializza Direct Sound
HRESULT InitMusic(HWND hwnd);    //Inizializza Direct Music
HRESULT InitChars(void);         //Inizializza il set di caratteri 
HRESULT InitInput(HINSTANCE hinst,HWND hwnd);    //Inizializza Direct Input
HRESULT GameMain(HWND hwnd);     //funzione principale del gioco
HRESULT CreateBufferFromImg(const IMAGE_FRAME_PTR pimg,CImgBuffer16 *pbuff);// Crea un oggetto buffer a 16 bit da una immagine
int QuitMenu(void *pform); //funzione di chiusura dei menu
void ShutDownGame(void); //rilascia tutti gli oggetti creati
void CloseForm(void *pform); //chiude una form del menu

void DrawBackMenuHelp(IMAGE_FRAME_PTR img);
int ShowMenuConfig(void *pform); //mostra il menu di configurazione
int ShowHelp(void *pform); //mostra la pagina di aiuto
int ShowCredits(void *pform); //mostra la form dei credits
int ShowLanguage(void *pform);
int ShowChooseLevel(void *pform);
int ShowChooseExtraLevel(void *pform);
int ShowFrmControls(void *pform);
int SetDefaultControls(void *pform);
int LinkSwitches1(void *pform); //collega due switch del menu configura fra loro
int LinkSwitches2(void *pform);
int SwitchLanguageIt(void *pform);
int SwitchLanguageEn(void *pform);
void UpdateLanguage(void);
int StartGame(int iLevel,void *pform); //Inizia il gioco
int StartGameLv1(void *pform); //avvia il gioco dal livello 1
int StartGameLv2(void *pform); //avvia dal livello 3
int StartGameLv3(void *pform); //...
int StartGameLv4(void *pform);
int StartGameLv5(void *pform);
int StartGameLv6(void *pform);
int StartGameLv7(void *pform);
int StartGameLv8(void *pform);
int MsgLevelLocked(void *pform); //messaggio livello bloccato
int SwitchSound(void *pform); //abilita/disabilita il sonoro quando l'utente cambia di stato allo switch sonoro nel menu
void DoIntro(void); //Mostra l'introduzione del gioco
int DoClick(void *pform);//esegue il suono click
int SaveHighScoreProc(void *pform); //procedura di salvataggio del punteggio
void ConsoleLoop(void);
int LoadHighScore(void);
int SaveHighScore(void);
void DrawBackMenuHighScore(IMAGE_FRAME_PTR img);
int LoadControlsConfig(void *pform);
int SaveControlsConfig(void *pform);
int SaveEnviron(void);
void ProgressBar(int x,int y,float perc,int width);


//Funzione per i messaggi formattati
int CDECL MsgOut(UINT uType,TCHAR *lpstrTitle,TCHAR *lpstrMsg,...);

//------------------------ WindowProc -----------------------------------

LRESULT CALLBACK WindowProc(  //WindowProc processa i messaggi inviati alla finestra
  

    HWND  hwnd,	    // handle of window
    UINT  msg,	    // message identifier
    WPARAM  wparam,	// first message parameter
    LPARAM  lparam 	// second message parameter
   ) 
{
	

  switch (msg) {

  case WM_MOUSEMOVE:	
	  {
		  if (g_Environ.game_status==GM_LEVELEDITOR)
		  {
			  



		  }
	  }


  case WM_CREATE:   //WM_CREATE viene inviato quando l'applicazione crea una nuova finestra, la WindowProc della nuova finestra
	  {             //riceve questo messaggio prima che la finsetra sia visibile e che la procedura CreateWindow o CreateWindowEx ritorni		
         return (0);      
	  } 
	  
	  break;  
      
  case WM_CLOSE:
	  
	  g_Environ.game_status=GM_SHUTDOWN;
	  

	  PostQuitMessage(0);
	 
	  return (0);
	  break;

  default:break;
	  
  } //fine switch
//processa un messaggio non contemplato nella lista switch DefWindowProc chiama la procedura di default per processare i messaggi
return (DefWindowProc(hwnd,msg,wparam,lparam)); 

}//fine WindowProc


//---------------------------------------------WinMain-----------------------------------
//WINAPI � usato al posto di FAR PASCAL
//funzione di inizio del programma
//program entry point

int WINAPI WinMain(	HINSTANCE hinstance,        //istanza corrente
					HINSTANCE hprevinstance,    //istanza precedente (sempre nulla nelle applicazioni Win-32)
					LPSTR lpcmdline,            //riga di comando
					int ncmdshow)               //modalit� di visualizzazione della finestra
{ 

#ifdef _DEBUG_

	_CrtCheckMemory();
	_CrtDumpMemoryLeaks();

#endif

	
WNDCLASS winclass;  //strtuttura che contiene la classe della finestra
HWND     hwnd;      //handle della finestra
MSG    msg;



//popola la struttura winclass

winclass.style        =CS_DBLCLKS | CS_OWNDC | 
                       CS_HREDRAW | CS_VREDRAW;
winclass.lpfnWndProc  =WindowProc;  //punta alla funzione che processa i messaggi
winclass.cbClsExtra   =0;
winclass.cbWndExtra   =0;
winclass.hInstance    =hinstance;
winclass.hIcon        =LoadIcon(NULL, MAKEINTRESOURCE(IDI_ICON1)); //carica l'icona dalle risorse
winclass.hCursor      =LoadCursor(NULL ,IDC_ARROW);
winclass.hbrBackground=(HBRUSH) GetStockObject(BLACK_BRUSH);
winclass.lpszMenuName =NULL;
winclass.lpszClassName=WINDOW_CLASS_NAME;  


//registra la classe della finestra (la registrazione � necessaria per poi poter chimare le funzioni createWindow ecc...)
if (!RegisterClass(&winclass)) return (0);

if (g_Environ.bFullScreen)
{

//crea la finestra per la modalit� fullscreen
if (!(hwnd=CreateWindow(WINDOW_CLASS_NAME,TEXT("Star Blastrix"),
	                    
                          WS_POPUP |WS_VISIBLE,
					 	  0,0,	   // x,y
						  WINDOW_WIDTH,  // width
                          WINDOW_HEIGHT, // height
						  NULL,	   // handle to parent 
						  NULL,	   // handle to menu
						  hinstance,// instance
						  NULL)))	return (0);// creation parms
} 
else
{
	//finestra per modalit� windowed 
	if (!(hwnd=CreateWindow(WINDOW_CLASS_NAME,"StarBlastrix",
	                    
                          WS_OVERLAPPEDWINDOW,
					 	  0,0,	   // x,y
						  640,  // width
                          480, // height
						  NULL,	   // handle to parent 
						  NULL,	   // handle to menu
						  hinstance,// instance
						  NULL)))	return (0);// creation parms
}

//handler della finestra
g_hwnd=hwnd; 

g_instance=hinstance;

ShowWindow(hwnd,ncmdshow);

UpdateWindow(hwnd);

//imposta la priorit� del thread corrente per evitare rallentamenti dovuti 
//a processi in background

HANDLE CurThread=GetCurrentThread();

if (CurThread) ::SetThreadPriority(CurThread,THREAD_PRIORITY_ABOVE_NORMAL);

//ciclo dei messaggi
while (!g_bExitLoop) {	 
   

     //PeekMessage a differenza di GetMessage non attende
	 //che un messaggio sia messo in coda
	//nota bene: questo ciclo deve essere elaborato costantemente se non si vuole che si blocchi tutto
	//(specie su WinXP) 
     if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	 {				
       if (msg.message==WM_QUIT) 
	   {	
		   g_bExitLoop=TRUE;  		  
		   PostQuitMessage(0);
	   }

       //traduce i tasti acceleratori
       TranslateMessage(&msg);
       //manda il messaggio alla WindowProc
       DispatchMessage(&msg);	  
     
     }//fine if PeekMessage    

	  //macchina allo stato finito del 
	 //game finite state machine
     
	 switch (g_Environ.game_status)
	 {
		case GM_HEARTBEAT:

			HeartBeat();
			break;

		case GM_INITIALIZE:
		
			//il gioco deve essere inizializzato
			GameMain(hwnd); 		
			break;
        
		case GM_CONSOLE:

			//menu iniziale
			ConsoleLoop();
			break;			

		case GM_GAMELOOP:	 
             
			//ciclo del gioco: inzializza il livello corrente
			GameLoop();
			break;	 

        case GM_LEVELCOMPLETED:
            //livello corrente completo: avvia la sequenza di fine livello
			LevelCompleted();
			break;

        case GM_GAMECOMPLETED:
            //il player ha finito il gioco completando tutti i livelli
			GameCompleted();
			break;

        case GM_LEVELEDITOR:
			
			//modalit� editor di livelli (funzione heart beat per editor livelli)
			LevelEditor();
			break;
        
        case GM_SHUTDOWN:
			
			//fine gioco
			ShutDownGame();
			PostQuitMessage(0);
	      	g_bExitLoop=TRUE;
			break;	 
	 }

}//fine while



return (msg.wParam);

}//fine WinMain 

//------------------------------ TranslateMsg ---------------------------------------
//Traduce un messaggio nella lingua corrente (parametro language di g_Environ 0=it 1=en
//iErrorCode=codice di errore che accompagna il messaggio
TCHAR *TranslateGameMsg(int msgid,int iErrorCode)
{
	if (msgid<0 || msgid>NUM_MSG) return NULL;
	if (!iErrorCode)
	{
		if (g_Environ.iLanguage==1) return g_Msg[msgid].msg1;	//en
		return g_Msg[msgid].msg; //it
	}
	else
	{
		//visualizza il codice del messaggio
		memset(g_szErrorMsg,0,sizeof(g_szErrorMsg)*sizeof(TCHAR));
		wsprintf(g_szErrorMsg,"(%s %d) %s",TranslateGameMsg(34,0),iErrorCode,TranslateGameMsg(msgid,0));
		return g_szErrorMsg; //restituisce il messaggio di errore con il codice di errore

	}
}

//------------------------------- InitGraphics --------------------------------------

//Inizializza il sitema grafico directdraw
//hwnd � l'handle della finsetra dell'applicazione
HRESULT InitGraphics(HWND hwnd)
{	
	HRESULT hr;   

	//Inizializza la modalit� grafica


	hr=g_gm.SetGraphMode(g_Environ.iScreenWidth,
		                 g_Environ.iScreenHeight,
						 g_Environ.iScreenBpp,
						 hwnd,
						 g_Environ.bFullScreen,2);
    
	//controlla se l'inzializzazione � andata a buon fine

	if (FAILED(hr)) return hr;
	//imposta nell'oggetto frame manager l'oggetto graphic manager corrente
    else hr=g_fm.SetGraphicManager(&g_gm);

	CSBObject::g_iScreenWidth=g_Environ.iScreenWidth;
	CSBObject::g_iScreenHeight=g_Environ.iScreenHeight;
	CSBObject::g_iScreenBottomHeight=70; //questa � per prova la deve impostare la funzione di caricamento del livello

	return hr;	
}

//------------------------------ InitSound ---------------------------------

HRESULT InitSound(HWND hwnd)
{

	HRESULT hr;

	hr=g_cs.Init(hwnd,NULL);  //inizializza direct sound

	if (!FAILED(hr)) 
	{
		g_Environ.bSound=TRUE;
		return S_OK;
	}

	else return hr;	
}

//------------------------------- InitMusic ---------------------------------

HRESULT InitMusic(HWND hwnd)
{
	HRESULT hr;

	hr=g_cm.Init(hwnd); //inizializza direct music
	
	if (!FAILED(hr))
	{
		g_Environ.bMusic=TRUE;

	}

	return hr;

}

//----------------------------- InitInput -------------------------

HRESULT InitInput(HINSTANCE hinst,HWND hwnd)
{
	HRESULT hr;

	hr=g_ci.Init(hinst,hwnd);

	g_ci.SwitchWinLogoKey(FALSE);

	//carica la configurazione dei tasti dal file di configurazione
	LoadControlsConfig(NULL);

	return hr;
}

//Inizializza i set di caratteri
HRESULT InitChars(void)
{
	HRESULT hr;
		//Inizializza i caratteri

	if (FAILED(g_chRed.SetFrameManager(&g_fm)))
	{
		//"Errore inizalizzando il set di caratteri 2."
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(35,2),2);
	    return E_FAIL;
	}

	if (FAILED(g_chBlue.SetFrameManager(&g_fm)))
	{
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(35,3),3);
		return E_FAIL;
	}

	g_chLittleRed.SetFrameManager(&g_fm);
	g_chLittleBlue.SetFrameManager(&g_fm);
	g_chLittleRed.SetGraphicManager(&g_gm);
	g_chLittleBlue.SetGraphicManager(&g_gm);

	//carica le tabelle di caratteri rossi
	hr=g_chRed.LoadCharSet("data\\charset.vpx",0,0,0,32,40,25,2);

	if (FAILED(hr))
	{		
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(37,8),2,TEXT("charset.vpx"));
		return hr;
	}

		//carica le tabelle di caratteri blu
	hr=g_chBlue.LoadCharSet("data\\charset.vpx",0,0,80,32,40,25,2);

	//hr=g_chBlue.LoadCharSet("caratteri.bmp",0,80,32,40,25,2);

	if (FAILED(hr))
	{
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(37,9),3,TEXT("charset.vpx"));
		return hr;
	}

	//caratteri piccoli blue
	hr=g_chLittleBlue.LoadCharSet("data\\charset.vpx",0,0,160,16,16,43,1);

	if (FAILED(hr))
	{
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(37,10),5,TEXT("charset.vpx"));
        return hr;
	}

	//caratteri piccoli rossi
	hr=g_chLittleRed.LoadCharSet("data\\charset.vpx",0,0,176,16,16,43,1);

	if (FAILED(hr))
	{
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(37,11),6,TEXT("charset.vpx"));
        return hr;
	}



	return S_OK;
}

//----------------------------- InitGame ------------------------------------

//Inizializza il gioco
HRESULT InitGame(HWND hwnd)
{

#ifdef _DEBUG_
	
	_CrtDumpMemoryLeaks(); //deve rendere 0
	_CrtCheckMemory(); //deve rendere 1

#endif

#ifdef _TESTGAME_

	g_cdb.WriteErrorFile("init game");

#endif

	HRESULT hr;
	
	hr=InitGraphics(hwnd);

#ifdef _DEBUG_
	
	_CrtDumpMemoryLeaks(); //deve rendere 0
	_CrtCheckMemory(); //deve rendere 1

#endif
	
    
    if (FAILED(hr))
	{
		//errore inizializzando la modalit� grafica
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(20,12));
		return hr;
	}

	g_fmath.InitTables(); //inizializza l'engine matematico
	g_gm.SetErrorFile(TEXT("debug.txt"),TRUE);

#ifdef _DEBUG_	
	
	g_gm.WriteErrorFile(TEXT("start:"));
	g_fm.SetErrorFile(TEXT("debug.txt"),FALSE);

#endif
	
#ifndef _WAV_OFF_
	
	hr=InitSound(hwnd);
	
	if (FAILED(hr))
	{
		//errore inizializzando il sonoro
		//MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(21,13));
		//se va in errore in questo caso da comunque la possibilit� di proseguire
		g_Environ.bSound=FALSE;	
		g_cdb.WriteErrorFile(TranslateGameMsg(53,120));
	}
	else g_Environ.bSound=TRUE;

#endif
#ifndef _MIDI_OFF_

	hr=InitMusic(hwnd);

	if (FAILED(hr))
	{
		g_Environ.bMusic=FALSE;
		g_cdb.WriteErrorFile(TranslateGameMsg(54,121));

	}
	else g_Environ.bMusic=TRUE;

#endif

	hr=InitInput(g_instance,hwnd);

	if (FAILED(hr))
	{
		//Questo � un errore critico: senza direct input non fa continuare
		MsgOut(MB_ICONEXCLAMATION,g_szGameTitle,TranslateGameMsg(23,14));
		return hr;
	}

	hr=InitChars();

	if (FAILED(hr)) return hr;

#ifdef _DEBUG_
	
	_CrtDumpMemoryLeaks(); //deve rendere 0
	_CrtCheckMemory(); //deve rendere 1

#endif
	
	//Inizializzazione OK!
    return S_OK;

}

//-------------------------------- ShutDownGame ----------------------------
//Rilascia gli oggetti e termina il gioco
void ShutDownGame(void)
{

#ifdef _TESTGAME_

	g_cdb.WriteErrorFile("shutdowngame");

#endif

		
	DWORD dwCount;

#ifndef _WAV_OFF_   
	FreeSounds(); //rilascia gli effetti sonori
#endif

#ifndef _MIDI_OFF_
	g_cm.StopMIDI();
	g_cm.FreeMIDI();
#endif

	//rilascia i dati (funzione opposta a InitData)
	FreeData();
	//delloca la memoria allocata da CreateObjects
	FreeObjects();
	//rilascia gli sprites (funzione opposta a GrabSprites)
	FreeSprites(); 
	//rilascia la memoria del main menu
	FreeMainMenu();
	g_fm.FreeImgFrame(&m_img2);
	g_fm.FreeImgFrame(&imgBackMenu);

	//rilascia i fonts
//	g_chBig.FreeCharTable();
	g_chRed.FreeCharTable();
	g_chBlue.FreeCharTable();
	g_chLittleRed.FreeCharTable();
	g_chLittleBlue.FreeCharTable();

	for (dwCount=0;dwCount<10;dwCount++) g_fm.FreeImgFrame(&strcMainMenu.imgCur[dwCount]);	

	g_gm.ShutDown();
	g_cs.ShutDown();
	g_cm.ShutDown();
	//riabilita il tasto win logo
	g_ci.SwitchWinLogoKey(TRUE);
	g_ci.ShutDown();

	if (g_Environ.hasError) MsgOut(MB_ICONEXCLAMATION,"StarBlastrix",TranslateGameMsg(56,0));	
		
	m_pActiveForm=NULL;		

}

//------------------------------- MsgOut -------------------------------------

//Funzione message box modificata in modo da consentire di output di testo formattato 
//in base ai parametri su un message box
int CDECL MsgOut(UINT uType,TCHAR *lpstrTitle,TCHAR *lpstrMsg,...)
{
	TCHAR szBuffer[2048]; //2k per il messaggio
	va_list pArgList;

	//Questa macro fa si che pArgList punti all'inizio del buffer
	//dei valori opzionali
	va_start(pArgList,lpstrMsg);
	
	//formatta e mette la stringa formattata nel buffer szBuffer
	_vsnprintf(szBuffer,sizeof(szBuffer) / sizeof(TCHAR) ,lpstrMsg,pArgList);
    
	va_end(pArgList);

	return MessageBox(g_hwnd,szBuffer,lpstrTitle,uType);

}

//---------------------- DoIntro ----------------------------------
//Introduzione
//(il titolo scrolla da da dx a sx)

void DoIntro(void)
{
	LONG lCx=0;  //posizione corrente del titolo
	LONG lStart,lEnd;
	IMAGE_FRAME imgTitle;	
    const LONG lYOut=100;

	g_gm.Cls(0); //cancella lo schermo visibile
	g_gm.UseBackBuffer(); //usa il back buffer  
	g_gm.Cls(0); //pulisce il backbuffer
	
	if (!FAILED(g_fm.CreateImgFrameFromVPX(&imgTitle,"data\\intro.vpx",0)))	
	{

		lStart=(LONG) g_Environ.iScreenWidth+200;
		lEnd= - (LONG) imgTitle.width;

		lCx=lStart;

		g_gm.UseBackBuffer(); //double buffer	
		
		g_ci.ResetKyb(); //resetta lo tato della tastiera, appena si preme un tasto interrompe il titolo e passa al menu

		//esegue lo scrolling orizzontale del titolo
		while (lCx>= lEnd)
		{
			lCx -=10;					
			g_gm.Cls(0);
			//esegue un blitting non trasparente per rendere il ciclo piu' fluido		
			g_fm.PutImgFrame(lCx,lYOut,&imgTitle,DDBLT_WAIT) ;			
			g_gm.ShowBackBuffer();	
			lCx -=10;	
			g_gm.Cls(0);
			g_fm.PutImgFrame(lCx,lYOut,&imgTitle,DDBLT_WAIT);											
			g_gm.ShowBackBuffer();	
			
			g_ci.GetKybStatus();
			if (g_ci.GetCurrentKeyDown(0,255)>0) 
			{
				IMAGE_FRAME img;
				g_fm.CaptureScreen(&img);
				ScreenTransition(&img,NULL);
				lCx=lEnd; //interrompe il titolo
				g_fm.FreeImgFrame(&img);
			}

		}

		g_gm.UseScreen(1);		        
		g_gm.Cls(0);		
		
	}

	else 
	{
		g_gm.UseScreen(0);
		g_gm.TextOut(20,20,TranslateGameMsg(25,15),0,TEXT("intro.vpx"));
		g_gm.FlipScreen(0);
	}

	//rilascia lo sprite del titolo
	g_fm.FreeImgFrame(&imgTitle);
}

//--------------------------- DrawBackMenu -------------------------
//Funzione che disegna il background durante la visualizzazione
//dell'interfaccia utente
void DrawBackMenu(IMAGE_FRAME_PTR img)
{
//	g_gm.Cls(0,img);
	g_fm.PutImgFrameClip(0,0,img,&imgBackMenu,0);
	g_fm.PutImgFrameClip(20,20,img,&imgTitle);	
}

void DrawBackMenuHelp(IMAGE_FRAME_PTR img)
{

	if (g_Environ.iLanguage==1)
	{
		g_fm.PutImgFrameClip(0,0,img,&imgBackMenu,0);
		g_chLittleRed.TextOut(10,150,img,TEXT("USE ARROW KEYS OR JOY TO MOVE THE SHIP."));
		g_chLittleRed.TextOut(10,170,img,TEXT("CHOOSE THE INPUT DEVICE FROM THE"));
		g_chLittleRed.TextOut(10,190,img,TEXT("<OPTIONS> MENU."));
		g_chLittleRed.TextOut(10,210,img,TEXT("THERE ARE THREE TYPES OF WEAPONS:"));
		g_chLittleRed.TextOut(10,230,img,TEXT("PRIMARY FIRE"));
		g_chLittleRed.TextOut(10,250,img,TEXT("BOMBS AND AN EXTRA WEAPON"));
		g_chLittleRed.TextOut(10,270,img,TEXT("THAT YOU GET COLLECTING POWER UPS"));
		g_chLittleRed.TextOut(10,290,img,TEXT("PRESS ESC OR Q KEY TO QUIT THE GAME."));
	
	}
	else
	{
		g_fm.PutImgFrameClip(0,0,img,&imgBackMenu,0);
		g_chLittleRed.TextOut(10,150,img,TEXT("USARE LE FRECCE O IL JOYSTICK."));
		g_chLittleRed.TextOut(10,170,img,TEXT("PER SPOSTARSI"));
		g_chLittleRed.TextOut(10,190,img,TEXT("E' POSSIBILE SELEZIONARE LA TASTIERA"));
		g_chLittleRed.TextOut(10,210,img,TEXT("O IL JOYSTICK DAL MENU <OPZIONI>"));
		g_chLittleRed.TextOut(10,230,img,TEXT("CI SONO TRE TIPI DI ARMA:"));
		g_chLittleRed.TextOut(10,250,img,TEXT("IL FUOCO PRIMARIO CHE NON SI ESAURISCE"));
		g_chLittleRed.TextOut(10,270,img,TEXT("LE BOMBE E L'ARMA SPECIALE"));
		g_chLittleRed.TextOut(10,290,img,TEXT("CHE SI OTTIENE RACCOGLIENDO I BONUS."));
		g_chLittleRed.TextOut(10,310,img,TEXT("PER TERMINARE IL GIOCO IN QUALSIASI"));	
		g_chLittleRed.TextOut(10,330,img,TEXT("MOMENTO PREMERE ESC O IL TASTO Q"));
	}
	
	g_fm.PutImgFrameClip(20,20,img,&imgTitle);	
}

//punteggi massimi (high score)
void DrawBackMenuHighScore(IMAGE_FRAME_PTR img)
{
	DrawBackMenu(img);

	for (int count=0;count<HIGH_SCORE_COUNT;count++)
	{
		g_chLittleRed.TextOut(50,165+count*24,img,"%d",m_HighScore[count].lScore);
		g_chLittleBlue.TextOut(190,165+count*24,img,m_HighScore[count].szName);
	}
}

//--------------------------- InitMainMenu -----------------------

//Inizializza la struttura MainMenu
//G.U.I
HRESULT InitMainMenu(void)
{
	DWORD dwCount;
	RECT rc;
	int extra_levels=0;
	const int CL1=200;
	const int CL2=490;

	//crea la frame con le immagini ausiliarie
	if (FAILED(g_fm.CreateImgFrameFromVPX(&strcMainMenu.imgAux,"data\\spraux.vpx",0)))
	{
		//termina se fallisce
		g_cdb.WriteErrorFile(TranslateGameMsg(25,16),0,TEXT("spraux.vpx"));
		return E_FAIL;
	}

	strcMainMenu.sprCur.SetFrameManager(&g_fm);
	strcMainMenu.sprHand.SetFrameManager(&g_fm);	

	for (dwCount=0;dwCount<10;dwCount++)
	{
		//esegue il grabbing del cursore animato dei menu
		rc.left=dwCount*32;
		rc.top=0;
		rc.right=rc.left+31;
		rc.bottom=32;

		if (FAILED(g_fm.GrabFrame(&strcMainMenu.imgCur[dwCount],&rc,&strcMainMenu.imgAux)))
		{
			//termina se non riesce a caricare il cursore
			g_cdb.WriteErrorFile(TranslateGameMsg(38,17),dwCount);
			MsgOut(MB_ICONEXCLAMATION,TEXT("Errore"),TranslateGameMsg(38,17),dwCount);
			return E_FAIL;
		}

		//aggiunge la frame allo sprite del cursore
		strcMainMenu.sprCur.AddFrame(&strcMainMenu.imgCur[dwCount]);
		
	}	

	//imposta la velocit� di animazione delle frame che compongono lo sprite
	//0.5 significa che cambia la frame ogni due cicli di aggiornamento
	strcMainMenu.sprCur.SetAnimSpeed(0.4f);

	rc.left=32;
	rc.top=68;
	rc.bottom=rc.top+32*4;
	rc.right=rc.left+90;

	//crea lo sprite del puntatore del mouse (mano)
    if (FAILED(g_fm.GrabFrame(&strcMainMenu.imgHand,&rc,&strcMainMenu.imgAux)))
	{
		//termina se non riesce a caricare il cursore
			g_cdb.WriteErrorFile(TranslateGameMsg(38,18));
			MsgOut(MB_ICONEXCLAMATION,TEXT("Errore"),TranslateGameMsg(38,18));
			return E_FAIL;
	}

	rc.left=447;rc.top=67;rc.right=511;rc.bottom=123;
	g_fm.GrabFrame(&strcMainMenu.imgOn,&rc,&strcMainMenu.imgAux); //ritaglia l'immagine da visualizzare quando lo switch � ON
	rc.left=512;rc.top=67;rc.right=576;rc.bottom=123;
	g_fm.GrabFrame(&strcMainMenu.imgOff,&rc,&strcMainMenu.imgAux); //immagine OFF
	
	
	//frame bottone ok
	if (FAILED(g_fm.GrabFrame(&strcMainMenu.imgButton,200,57,300,114,&strcMainMenu.imgAux)))
	{
		//termina se non riesce a caricare il cursore
		MsgOut(MB_ICONEXCLAMATION,TEXT("Errore"),TranslateGameMsg(40,19),TEXT("imgButton"));
		return E_FAIL;
	}


	//rilascia l'immagine dei controlli
	g_fm.FreeImgFrame(&strcMainMenu.imgAux);

	//aggiunge la frame allo sprite del cursore
	strcMainMenu.sprHand.AddFrame(&strcMainMenu.imgHand);
	strcMainMenu.sprHand.SetFrameHotSpot(0,0,32,5);

	 //crea il menu principale
	strcMainMenu.mnuStart.SetGraphicManager(&g_gm);
	strcMainMenu.mnuStart.SetFrameManager(&g_fm);
	//INIZIA PARTITA
	strcMainMenu.mnuStart.Create(120,110,TranslateGameMsg(0),0); //crea la voce del menu da visualizzare alle coordinate specificate
	strcMainMenu.mnuStart.SetCharSet(&g_chRed); //imposta il carattere di questo controllo
	strcMainMenu.mnuStart.SetAction(ShowChooseLevel); //funzione di inzio gioco
    //CONFIGURA
	strcMainMenu.mnuConfig.Create(120,160,TranslateGameMsg(1),0);
	strcMainMenu.mnuConfig.SetCharSet(&g_chRed);
    strcMainMenu.mnuConfig.SetAction(ShowMenuConfig);
    //AIUTO
	strcMainMenu.mnuHelp.Create(120,210,TranslateGameMsg(2),0);
	strcMainMenu.mnuHelp.SetCharSet(&g_chRed);
	strcMainMenu.mnuHelp.SetAction(ShowHelp);	

	//CREDITS
	strcMainMenu.mnuCredits.SetGraphicManager(&g_gm);
	strcMainMenu.mnuCredits.SetFrameManager(&g_fm);
	strcMainMenu.mnuCredits.Create(120,260,TranslateGameMsg(3),0);
	strcMainMenu.mnuCredits.SetCharSet(&g_chRed);
	strcMainMenu.mnuCredits.SetAction(ShowCredits);


	strcMainMenu.mnuCreditsL1.Create(120,180,"PROGRAMMING: LEONARDO BERTI",0);
	strcMainMenu.mnuCreditsL1.SetCharSet(&g_chLittleRed);
	strcMainMenu.mnuCreditsL1.SetAction(NULL);

	strcMainMenu.mnuCreditsL2.Create(120,220,"GRAPHICS: LEONARDO BERTI",0);
	strcMainMenu.mnuCreditsL2.SetCharSet(&g_chLittleRed);
	strcMainMenu.mnuCreditsL2.SetAction(NULL);

	//INDIETRO<-
	strcMainMenu.mnuBack.SetCharSet(&g_chBlue);
	strcMainMenu.mnuBack.Create(70,410,TranslateGameMsg(9),0);
	strcMainMenu.mnuBack.SetAction(ShowMainMenu);
 	
	//ESCI
	strcMainMenu.mnuExit.Create(120,380,TranslateGameMsg(4),0);
	strcMainMenu.mnuExit.SetCharSet(&g_chBlue);
	strcMainMenu.mnuExit.SetAction(QuitMenu); //imposta la funzione di uscita
    //PUNTEGGI
	strcMainMenu.mnuHScore.Create(120,310,TranslateGameMsg(5),0);
	strcMainMenu.mnuHScore.SetCharSet(&g_chRed);
	strcMainMenu.mnuHScore.SetAction(HighScore);
	//LIVELLI DI PARTENZA
	strcMainMenu.mnuChooseTitle.Create(40,110,TranslateGameMsg(48),0); //scegli la missione
	strcMainMenu.mnuChooseTitle.SetCharSet(&g_chLittleRed);
	strcMainMenu.mnuChooseTitle.SetAction(NULL);
	strcMainMenu.mnuLevel1.Create(40,140,"1 CRYSTAL ISLANDS",0);
	strcMainMenu.mnuLevel1.SetCharSet(&g_chLittleBlue);
    strcMainMenu.mnuLevel1.SetAction(StartGameLv1);
	strcMainMenu.mnuLevel2.Create(40,170,"2 RED ROCK DESERT",0);
	strcMainMenu.mnuLevel2.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel2.SetAction(StartGameLv2);
	strcMainMenu.mnuLevel3.Create(40,200,"3 ANTARTICA",0);
	strcMainMenu.mnuLevel3.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel3.SetAction(StartGameLv3);
	strcMainMenu.mnuLevel4.Create(40,230,"4 STARLIGHT CITY",0);	
	//strcMainMenu.mnuLevel4.SetAction(MsgLevelLocked);
	strcMainMenu.mnuLevel4.SetAction(StartGameLv4);
	strcMainMenu.mnuLevel4.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel5.Create(40,260,"5 LOW EARTH ORBIT",0);	
	strcMainMenu.mnuLevel5.SetAction(StartGameLv5);
	strcMainMenu.mnuLevel5.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel6.Create(40,290,"6 DEEP SPACE",0);	
    strcMainMenu.mnuLevel6.SetAction(StartGameLv6);
	strcMainMenu.mnuLevel6.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel7.Create(40,320,"7 CANDOR CHASMA - MARS",0);	
    strcMainMenu.mnuLevel7.SetAction(StartGameLv7);
	strcMainMenu.mnuLevel7.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLevel8.Create(40,350,"8 PLANET KUNOS",0);
	strcMainMenu.mnuLevel8.SetAction(StartGameLv8);
	strcMainMenu.mnuLevel8.SetCharSet(&g_chLittleBlue);
  
    //livelli extra 
	strcMainMenu.mnuCustomLevel.Create(40,380,TranslateGameMsg(52),0);
	strcMainMenu.mnuCustomLevel.SetCharSet(&g_chLittleRed);
	strcMainMenu.mnuCustomLevel.SetAction(ShowChooseExtraLevel);


		
	//N.B. quando si passa da un controllo all'altro con il cursore,viene rispettato
	//l'ordine con cui i controlli sono aggiunti alla form

	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuStart,TRUE);
	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuConfig,TRUE);
	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuHelp,TRUE);
	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuCredits,TRUE);
	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuHScore,TRUE);
	strcMainMenu.frmMain.AddControl(&strcMainMenu.mnuExit,TRUE);
	//imposta lo sprite che indica il selettore
	strcMainMenu.frmMain.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmMain.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmMain.SetInputManager(&g_ci);
	//imposta la funzione di background 
    strcMainMenu.frmMain.SetBackGroundProc(DrawBackMenu);

	//frmChooseLevel
	strcMainMenu.frmChooseLevel.SetGraphicManager(&g_gm);
	strcMainMenu.frmChooseLevel.SetFrameManager(&g_fm);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuChooseTitle,FALSE); //titolo non selezionabile
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel1,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel2,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel3,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel4,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel5,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel6,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel7,TRUE);
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuLevel8,TRUE);


	//menu livelli extra
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuCustomLevel,TRUE);	
	strcMainMenu.frmChooseLevel.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmChooseLevel.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmChooseLevel.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmChooseLevel.SetInputManager(&g_ci);
	//imposta la funzione di background 
    strcMainMenu.frmChooseLevel.SetBackGroundProc(DrawBackMenu);

	//EXTRA LEVELS
	strcMainMenu.frmChooseExtraLevel.SetGraphicManager(&g_gm);
	strcMainMenu.frmChooseExtraLevel.SetFrameManager(&g_fm);
	strcMainMenu.frmChooseExtraLevel.AddControl(&strcMainMenu.mnuChooseTitle,FALSE);

	//livelli custom nella directory extra_levels
	//estrae i primi 10 files in questa directory
	extra_levels=ListFiles("data\\extra_levels\\*.sbl",g_wfd,MAX_FILES);

	for (int count=0;count<extra_levels;count++)
	{
		strcMainMenu.mnuExtraLevel[count].Create(40,140+count*30,_tcsupr((TCHAR*)g_wfd[count].cFileName),0);
		strcMainMenu.mnuExtraLevel[count].SetCharSet(&g_chLittleBlue);
		strcMainMenu.mnuExtraLevel[count].SetAction(StartCustomLevel);
		strcMainMenu.frmChooseExtraLevel.AddControl(&strcMainMenu.mnuExtraLevel[count],TRUE);		
	}

	strcMainMenu.frmChooseExtraLevel.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmChooseExtraLevel.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmChooseExtraLevel.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmChooseExtraLevel.SetInputManager(&g_ci);
    strcMainMenu.frmChooseExtraLevel.SetBackGroundProc(DrawBackMenu);
    
	//LINGUA
	strcMainMenu.mnuLanguage.Create(70,100,TranslateGameMsg(44),0);
	strcMainMenu.mnuLanguage.SetCharSet(&g_chRed);
	strcMainMenu.mnuLanguage.SetAction(ShowLanguage);	
	
	//SONORO
	strcMainMenu.mnuSound.Create(70,160,TranslateGameMsg(6),TRUE,&strcMainMenu.imgOn,&strcMainMenu.imgOff);
	strcMainMenu.mnuSound.SetCharSet(&g_chRed);
	strcMainMenu.mnuSound.SetAction(SwitchSound); //imposta la funzione da eseguire quando l'utente cambia di stato allo switch sonoro
    
	//USA LA TASTIERA
	strcMainMenu.mnuKyb.Create(70,220,TranslateGameMsg(7),TRUE,&strcMainMenu.imgOn,&strcMainMenu.imgOff);
	strcMainMenu.mnuKyb.SetCharSet(&g_chRed);
	strcMainMenu.mnuKyb.SetAction(LinkSwitches1);

	//USA IL JOYSTICK
	strcMainMenu.mnuJoy.Create(70,280,TranslateGameMsg(8),FALSE,&strcMainMenu.imgOn,&strcMainMenu.imgOff);
	strcMainMenu.mnuJoy.SetCharSet(&g_chRed);
	strcMainMenu.mnuJoy.SetAction(LinkSwitches2);

	//CONFIG. CONTROLLI
	strcMainMenu.mnuConfigKeyb.Create(70,340,TranslateGameMsg(10),0);
	strcMainMenu.mnuConfigKeyb.SetCharSet(&g_chRed);
	strcMainMenu.mnuConfigKeyb.SetAction(ShowFrmControls);

	//SU
	//form di configurazione del'input da tastiera
	strcMainMenu.mnuUp.Create(CL2,120,TranslateGameMsg(11),0);
	strcMainMenu.mnuUp.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuUp.SetTextCharSet(&g_chLittleRed);
	strcMainMenu.mnuUp.SetMaxLen(1);
	strcMainMenu.mnuUp.SetFrameManager(&g_fm);
	//GIU'
	strcMainMenu.mnuDown.Create(CL2,160,TranslateGameMsg(12),0);
	strcMainMenu.mnuDown.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuDown.SetMaxLen(1);
	strcMainMenu.mnuDown.SetTextCharSet(&g_chLittleRed);
	strcMainMenu.mnuDown.SetFrameManager(&g_fm);
    //DESTRA
	strcMainMenu.mnuRight.Create(CL1,160,TranslateGameMsg(13),0);
	strcMainMenu.mnuRight.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuRight.SetMaxLen(1);
	strcMainMenu.mnuRight.SetFrameManager(&g_fm);
	strcMainMenu.mnuRight.SetTextCharSet(&g_chLittleRed);
    //SINISTRA
	strcMainMenu.mnuLeft.Create(CL1,120,TranslateGameMsg(14),0);
	strcMainMenu.mnuLeft.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuLeft.SetMaxLen(1);
	strcMainMenu.mnuLeft.SetFrameManager(&g_fm);
	strcMainMenu.mnuLeft.SetTextCharSet(&g_chLittleRed);
    //FUOCO PRIMARIO
	strcMainMenu.mnuFire1.Create(CL1,200,TranslateGameMsg(15),0);
	strcMainMenu.mnuFire1.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuFire1.SetMaxLen(1);
	strcMainMenu.mnuFire1.SetFrameManager(&g_fm);
	strcMainMenu.mnuFire1.SetTextCharSet(&g_chLittleRed);
    //FUOCO SECONDARIO
	strcMainMenu.mnuFire2.Create(CL1,240,TranslateGameMsg(16),0);
	strcMainMenu.mnuFire2.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuFire2.SetMaxLen(1);
	strcMainMenu.mnuFire2.SetFrameManager(&g_fm);
	strcMainMenu.mnuFire2.SetTextCharSet(&g_chLittleRed);
    //ARMA SPECIALE
	strcMainMenu.mnuFire3.Create(CL1+50,280,TranslateGameMsg(17),0);
	strcMainMenu.mnuFire3.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuFire3.SetMaxLen(1);
	strcMainMenu.mnuFire3.SetFrameManager(&g_fm);
	strcMainMenu.mnuFire3.SetTextCharSet(&g_chLittleRed);
    //SALVA
	strcMainMenu.mnuSaveConfig.Create(60,320,TranslateGameMsg(18),0);
	strcMainMenu.mnuSaveConfig.SetAction(SaveControlsConfig);
	strcMainMenu.mnuSaveConfig.SetCharSet(&g_chLittleRed);	
    //"RIPRISTINA I DEFAULTS"
	strcMainMenu.mnuDefaults.Create(60,360,TranslateGameMsg(19),0);
	strcMainMenu.mnuDefaults.SetAction(NULL);
	strcMainMenu.mnuDefaults.SetCharSet(&g_chLittleRed);
	strcMainMenu.mnuDefaults.SetAction(SetDefaultControls);

	strcMainMenu.frmControls.SetGraphicManager(&g_gm);
	strcMainMenu.frmControls.SetFrameManager(&g_fm);
	//imposta lo sprite che indica il selettore
	strcMainMenu.frmControls.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmControls.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuUp,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuDown,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuLeft,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuRight,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuFire1,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuFire2,TRUE);
    strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuFire3,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuSaveConfig,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuDefaults,TRUE);
	strcMainMenu.frmControls.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmControls.SetInputManager(&g_ci);
	//imposta la funzione di background 
    strcMainMenu.frmControls.SetBackGroundProc(DrawBackMenu);
	
	
    //aggiunge i controlli al menu di configurazione
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuSound,TRUE);
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuKyb,TRUE);
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuJoy,TRUE);
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuConfigKeyb,TRUE);
//	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuFire1,TRUE);
//	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuFire2,TRUE);
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmConfig.AddControl(&strcMainMenu.mnuLanguage,TRUE);

	//imposta lo sprite che indica il selettore per il menu di configurazione
	strcMainMenu.frmConfig.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmConfig.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmConfig.SetInputManager(&g_ci);

	strcMainMenu.frmConfig.SetBackGroundProc(DrawBackMenu);
	
	//crea il titolo centrato nello schermo
	strcMainMenu.mnuHelpTitle.Create((LONG)((g_Environ.iScreenWidth-g_chBlue.GetTextWidth("AIUTO"))*0.5),110,TranslateGameMsg(2),0);
	strcMainMenu.mnuHelpTitle.SetCharSet(&g_chBlue);

	//form help
	strcMainMenu.frmHelp.AddControl(&strcMainMenu.mnuHelpTitle,FALSE);
	strcMainMenu.frmHelp.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmHelp.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmHelp.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmHelp.SetInputManager(&g_ci);
	strcMainMenu.frmHelp.SetBackGroundProc(DrawBackMenuHelp);

	//form credits
	strcMainMenu.mnuCreditsTitle.Create((LONG)((g_Environ.iScreenWidth-g_chBlue.GetTextWidth("CREDITS"))*0.5),110,"CREDITS",0);
	strcMainMenu.mnuCreditsTitle.SetCharSet(&g_chBlue);
	strcMainMenu.frmCredits.AddControl(&strcMainMenu.mnuCreditsL1,FALSE);
	strcMainMenu.frmCredits.AddControl(&strcMainMenu.mnuCreditsL2,FALSE);
	strcMainMenu.frmCredits.AddControl(&strcMainMenu.mnuCreditsTitle,FALSE);
	strcMainMenu.frmCredits.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmCredits.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmCredits.SetMousePointer(&strcMainMenu.sprHand);
	strcMainMenu.frmCredits.SetInputManager(&g_ci);
	strcMainMenu.frmCredits.SetBackGroundProc(DrawBackMenu);
    
	strcMainMenu.txtScore.Create(120,200,"",0);
	strcMainMenu.txtScore.SetMaxLen(3);	
	strcMainMenu.txtScore.SetCharSet(&g_chLittleBlue);
	strcMainMenu.txtScore.SetFrameManager(&g_fm);
	strcMainMenu.txtScore.SetAction(SaveHighScoreProc);
	//MIGLIORI PUNTEGGI
	strcMainMenu.mnuHScoreTitle.Create((LONG)((g_Environ.iScreenWidth-g_chBlue.GetTextWidth(TranslateGameMsg(5)))*0.5),110,TranslateGameMsg(5),0);
	strcMainMenu.mnuHScoreTitle.SetCharSet(&g_chBlue);
	strcMainMenu.frmHScore.AddControl(&strcMainMenu.mnuHScoreTitle,FALSE);
	strcMainMenu.frmHScore.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmHScore.AddControl(&strcMainMenu.txtScore,TRUE);
	strcMainMenu.frmHScore.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmHScore.SetMousePointer(&strcMainMenu.sprHand);
	strcMainMenu.frmHScore.SetInputManager(&g_ci);
	strcMainMenu.frmHScore.SetBackGroundProc(DrawBackMenuHighScore);

	//form message
	strcMainMenu.mnuMessage.Create(0,210,"MESSAGE",0); //messaggio generico da cambiare all'occorrenza
    strcMainMenu.mnuMessage.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuMessage.SetFrameManager(&g_fm);
	strcMainMenu.mnuMessage1.Create(0,240,"MESSAGE1",0); //seconda riga del messaggio generico
	strcMainMenu.mnuMessage1.SetCharSet(&g_chLittleBlue);
	strcMainMenu.mnuMessage1.SetFrameManager(&g_fm);

	strcMainMenu.btuOk.SetFrameManager(&g_fm);
	strcMainMenu.btuOk.SetGraphicManager(&g_gm);
	strcMainMenu.btuOk.Create(0,g_Environ.iScreenHeight-200,NULL,&strcMainMenu.imgButton,0);
	strcMainMenu.btuOk.Justify(JU_CENTER,g_Environ.iScreenWidth);
	strcMainMenu.btuOk.SetAction(ShowFrmControls);
	strcMainMenu.frmMessage.SetGraphicManager(&g_gm);
	strcMainMenu.frmMessage.SetFrameManager(&g_fm);
	strcMainMenu.frmMessage.SetInputManager(&g_ci);
	strcMainMenu.frmMessage.AddControl(&strcMainMenu.btuOk,TRUE);
	strcMainMenu.frmMessage.AddControl(&strcMainMenu.mnuMessage,TRUE);
	strcMainMenu.frmMessage.AddControl(&strcMainMenu.mnuMessage1,TRUE);
	strcMainMenu.frmMessage.SetBackGroundProc(DrawBackMenu);
	strcMainMenu.frmMessage.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmMessage.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmMessage.SetInputManager(&g_ci);

	//form lingua	
	strcMainMenu.mnuIt.Create(70,160,TranslateGameMsg(45),TRUE,&strcMainMenu.imgOn,&strcMainMenu.imgOff);
	strcMainMenu.mnuIt.SetCharSet(&g_chRed);
	strcMainMenu.mnuIt.SetAction(SwitchLanguageIt);
	strcMainMenu.mnuEn.Create(70,220,TranslateGameMsg(46),TRUE,&strcMainMenu.imgOn,&strcMainMenu.imgOff);
	strcMainMenu.mnuEn.SetCharSet(&g_chRed);
	strcMainMenu.mnuEn.SetAction(SwitchLanguageEn);
	strcMainMenu.frmLanguage.SetGraphicManager(&g_gm);
	strcMainMenu.frmLanguage.SetFrameManager(&g_fm);
	strcMainMenu.frmLanguage.SetSelector(&strcMainMenu.sprCur,SB_CTRLLEFT);
	strcMainMenu.frmLanguage.SetMousePointer(&strcMainMenu.sprHand); //imposta il puntatore del mouse
	strcMainMenu.frmLanguage.AddControl(&strcMainMenu.mnuIt,TRUE);
	strcMainMenu.frmLanguage.AddControl(&strcMainMenu.mnuEn,TRUE);
	strcMainMenu.frmLanguage.AddControl(&strcMainMenu.mnuBack,TRUE);
	strcMainMenu.frmLanguage.SetInputManager(&g_ci);
	strcMainMenu.frmLanguage.SetBackGroundProc(DrawBackMenu);
	strcMainMenu.lsndClick=g_cs.CreateSound("wavs\\click.wav",1);

	return S_OK;
}


//----------------------------------- FreeMainMenu ---------------------------------------------
//rilascia la memoria allocata per il main menu
void FreeMainMenu(void)
{
	for (DWORD dwCount=0;dwCount<10;dwCount++) g_fm.FreeImgFrame(&strcMainMenu.imgCur[dwCount]);

    g_fm.FreeImgFrame(&strcMainMenu.imgHand);
	g_fm.FreeImgFrame(&strcMainMenu.imgButton);

}

//---------------------------------------- GameMain ----------------------------------

//Funzione principale del gioco
HRESULT GameMain(HWND hwnd)
{	

#ifdef _TESTGAME_

	g_cdb.SetErrorFile("C:\\debug.txt",TRUE);

#else

	//error output nella versione release
	g_cdb.SetErrorFile("err.txt",TRUE);

#endif
#ifdef _DEBUG_
	_CrtCheckMemory();
	_CrtDumpMemoryLeaks();
#endif
	
	DWORD dwCount=0;    	
	//Inizializza il gioco
	if (FAILED(InitGame(hwnd))) 
	{
		g_Environ.game_status=GM_SHUTDOWN;

		g_Environ.hasError=TRUE;

		return E_FAIL;
	}

#ifdef _DEBUG_
	_CrtCheckMemory();
	_CrtDumpMemoryLeaks();
#endif

#ifndef _MIDI_OFF_
	
	if (g_Environ.bMusic)
	{
		//sigla iniziale
		g_cm.LoadMIDI(TEXT("\\music\\disco.mid"),NULL);
		//suona il midi in loop all'infinito
		g_cm.PlayMIDI(DMUS_SEG_REPEAT_INFINITE);
	}

#endif


#ifndef _DEBUG_
	//nasconde il puntatore del mouse
	ShowCursor(FALSE);

#endif

#ifndef _TESTGAME_

	DoIntro();

#endif
	
	//carica lo sfondo del menu
	g_fm.CreateImgFrameFromVPX(&m_img1,"data\\spraux.vpx",1);
	//_CrtDumpMemoryLeaks();
    //crea l'immagine di sfondo
    g_fm.CreateFrameSurface(&imgBackMenu,g_Environ.iScreenWidth,g_Environ.iScreenHeight);
	//ridimensiona lo sfondo del menu in base alla modalit� video corrente
	g_fm.PutImgFrameResize(0,0,&imgBackMenu,&m_img1,g_Environ.iScreenWidth,g_Environ.iScreenHeight);
	//rilascia l'immagine temporanea
	g_fm.FreeImgFrame(&m_img1);
    //titolo piccolo
	g_fm.CreateImgFrameFromVPX(&imgTitle,"data\\intro.vpx",1);


	//inizializza il menu principale	
	if (FAILED(InitMainMenu())) 
	{
		//errore inizializzando il menu principale
		MsgOut(MB_ICONEXCLAMATION,TranslateGameMsg(34),TranslateGameMsg(41,20));
        
		g_Environ.game_status=GM_SHUTDOWN;

		g_Environ.hasError=TRUE;

		return E_FAIL;
	}    

    //crea l'immagine di background per eseguire l'effetto di transizione
	g_fm.CreateFrameSurface(&m_img1,g_Environ.iScreenWidth,g_Environ.iScreenHeight); 	
	//disegna lo sfondo e il titolo sull'immagine
    DrawBackMenu(&m_img1);
	//disegna la form con tutti i controlli (menu principale)
	strcMainMenu.frmMain.Render(&m_img1);
	//esegue l'effetto di transizione in ingresso
	ScreenTransition(NULL,&m_img1);
	//pulisce il back buffer preparandolo per il menu principale
	g_gm.UseBackBuffer();
	//cancella il back buffer
	g_gm.Cls(0);	
    //imposta le coordinate iniziali del mouse
    g_ci.SetMousePosition((LONG)(g_Environ.iScreenWidth*0.5f),(LONG)(g_Environ.iScreenHeight*0.5f),0);
	    
    g_fm.FreeImgFrame(&m_img1);
		
	strcMainMenu.frmMain.SetGraphicManager(&g_gm); 

	strcMainMenu.frmMain.SetFrameManager(&g_fm);
	//visualizza il menu principale e attende l'input del'utente
	ShowMainMenu(NULL);
	//lo stato del gioco � in modalit� console
	g_Environ.game_status=GM_CONSOLE;

	return S_OK;
}

//---------------------------- QuitMenu ---------------------------------
//Chiude il menu ed esce (fine gioco)
int QuitMenu(void *pform)
{
	DWORD dwCount;

	if (m_pActiveForm) m_pActiveForm->Close();

	for (dwCount=0;dwCount<10;dwCount++) g_fm.FreeImgFrame(&strcMainMenu.imgCur[dwCount]);	

	g_fm.FreeImgFrame(&m_img1);

	g_fm.CaptureScreen(&m_img1);

	ScreenTransition(&m_img1,NULL);

	g_fm.FreeImgFrame(&m_img1);	

	g_Environ.game_status=GM_SHUTDOWN;

	return 0;
}

//imposta la sequenza dei livelli
void SetStartLevel(int iLevel)
{
	g_iLevelSequence[0]=iLevel;

	int j=0;

	for (int i=1;i<8;i++)
	{
		j++;

		if (j==iLevel) j++;
				
		g_iLevelSequence[i]=j;
	}

}

//avvia il livello custom selezionato
int StartCustomLevel(void *ctrl)
{
	int ilevel=0;

	static TCHAR szFileName[260];

	memset(szFileName,0,260*sizeof(TCHAR));

	CSbControl *p=(CSbControl*)ctrl;

	if (!ctrl) return 0;

	for (int i=0;i<MAX_FILES;i++)
	{
		//nota: questo � un confronto fra puntatori e non stringhe!!!
		if (p->Text()==strcMainMenu.mnuExtraLevel[i].Text())
		{
			ilevel=i;
			_tcscpy(szFileName,TEXT("data\\extra_levels\\"));
			g_szLoadCustomLevel=_tcscat(szFileName,(TCHAR*)g_wfd[i].cFileName);
			SetStartLevel(1);
			StartGame(1,ctrl);
			break;
		}

	}

	return ilevel;
}

int StartGameLv1(void *pform)
{
	//modifica la sequenza dei livelli
	SetStartLevel(1);

	return StartGame(1,pform);
}

int StartGameLv2(void *pform)
{

	SetStartLevel(2);
	return StartGame(1,pform);
}

int StartGameLv3(void *pform)
{
	SetStartLevel(3);
	return StartGame(1,pform);
}


int StartGameLv4(void *pform)
{
	SetStartLevel(4);
	return StartGame(1,pform);

}

int StartGameLv5(void *pform)
{
	SetStartLevel(5);
	return StartGame(1,pform);
}

int StartGameLv6(void *pform)
{
	SetStartLevel(6);
	return StartGame(1,pform);
}

int StartGameLv7(void *pform)
{
	SetStartLevel(7);
	return StartGame(1,pform);
}

int StartGameLv8(void *pform)
{
	SetStartLevel(8);
	return StartGame(1,pform);
}

int MsgLevelLocked(void *pform)
{
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmMessage;
	strcMainMenu.frmMessage.SetGraphicManager(&g_gm);
	strcMainMenu.frmMessage.SetFrameManager(&g_fm);
	strcMainMenu.mnuMessage.SetGraphicManager(&g_gm);
	strcMainMenu.mnuMessage.SetFrameManager(&g_fm);
	strcMainMenu.mnuMessage.SetText(TranslateGameMsg(49)); //LIVELLO BLOCCATO! 
	strcMainMenu.mnuMessage.Justify(JU_CENTER,g_Environ.iScreenWidth);	
	strcMainMenu.mnuMessage1.Activate(TRUE);
	strcMainMenu.mnuMessage1.SetText(TranslateGameMsg(50)); //completa i primi 3
	strcMainMenu.mnuMessage1.Justify(JU_CENTER,g_Environ.iScreenWidth);

	strcMainMenu.btuOk.SetAction(ShowChooseLevel); //ritorna alla pagina di selezione missione
	strcMainMenu.frmMessage.Open();

	return 0;
}


//---------------------------- StartGame ---------------------------------
//Inizia una nuova partita
//start game

int StartGame(int iLevel,void *pform)
{

#ifdef _DEBUG_

		
		#define new new(_NORMAL_BLOCK,__FILE__,__LINE__)
		int tmp=_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
		_CrtCheckMemory();	

#endif


	//flag che servono a non dover eseguire l'inizializzazione se � gi� stata fatta
	static BOOL bInitData=FALSE;
	
	HRESULT hr;  
	IMAGE_FRAME imgTmp;	
	int cnt;	

	strcMainMenu.frmMain.Close();

	g_fm.FreeImgFrame(&m_img1);

	g_fm.CaptureScreen(&m_img1);
	//duplica lo sfondo
	g_fm.DuplicateImgFrame(&imgTmp,&imgBackMenu);

	g_pby=238;

	g_pbx=190;

	g_pbw=250;

	//loading...
	g_chLittleBlue.TextOut(10,240,&imgTmp,TranslateGameMsg(26));
	
	//transizione che porta alla barra a livello iniziale
	ScreenTransition(&m_img1,&imgTmp);
    
	g_fm.FreeImgFrame(&m_img1); 
	
    //pulisce tutti i buffer grafici
	for (cnt=0;cnt<g_gm.GetScreenNum();cnt++)
	{
		g_gm.UseBackBuffer();
		g_fm.PutImgFrame(0,0,&imgTmp);
		ProgressBar(g_pbx,g_pby,0.0,g_pbw);
		g_gm.ShowBackBuffer();
	}	

	g_fm.FreeImgFrame(&imgTmp);

//	g_lCurrentScore=0; //azzera il  punteggio riportato dal player

	g_Environ.iLevel=iLevel; //inizia dal livello specificato dal parametro iLevel

	if (!bInitData)
	{	

#ifdef _DEBUG_

	_CrtCheckMemory();
	_CrtDumpMemoryLeaks();

#endif
		//carica gli sprite principali...
		hr=GrabSprites();

#ifdef _DEBUG_

	_CrtCheckMemory();
	_CrtDumpMemoryLeaks();

#endif
	
		if (FAILED(hr))
		{
			g_cdb.WriteErrorFile(TranslateGameMsg(27,21));
			g_Environ.hasError=TRUE;
			//esce
			QuitMenu(pform);
			return 0;
		}	

	
		#ifndef _WAV_OFF_    //disabilita gli effetti sonori     

		hr=CreateSounds();

		ProgressBar(g_pbx,g_pby,0.77f,g_pbw);
	
		if (FAILED(hr))
		{
			g_Environ.bSound=FALSE;
			g_cdb.WriteErrorFile(TranslateGameMsg(28,22));		
			g_Environ.hasError=TRUE;
		}		

		#endif

#ifdef _DEBUG_

		_CrtDumpMemoryLeaks();

#endif		
		
		//crea tutti gli stack di oggetti (alloca la memoria)
		hr=CreateObjects();

#ifdef _DEBUG_

		_CrtCheckMemory();
		_CrtDumpMemoryLeaks();

#endif
	
		if FAILED(hr)
		{
			MsgOut(MB_ICONEXCLAMATION,TranslateGameMsg(34),TranslateGameMsg(29,22));
			//esce
			QuitMenu(pform);

			return 0;
		}

		
		//inizializza gli sprites e le strutture necessarie per il gioco
	    //(non alloca nuova memoria)
	    hr=InitData(); 			
			
	
		if (FAILED(hr))
		{
			g_cdb.WriteErrorFile(TranslateGameMsg(30,23));
			//esce
			strcMainMenu.frmMain.Close();
			g_Environ.game_status=GM_SHUTDOWN;
			g_Environ.hasError=TRUE;
			return 0;

		}

		bInitData=TRUE;	
	}	

	CADXFastMath fn;

    //ciclo del gioco
	//game loop � definita in sbengine

	//indica di eseguire il loop del gioco
	g_Environ.game_status=GM_GAMELOOP;

	//progress bar al 100% su tutti i buffer
	for (cnt=0;cnt<g_gm.GetScreenNum();cnt++) 
	{
		ProgressBar(g_pbx,g_pby,1.0,g_pbw);
	}	

	return 0;
}

//-------------------------- CloseForm ----------------------------------

//chiude la form pform
void CloseForm (void *pform)			
{
	CSbForm *p=NULL;

	p=(CSbForm *)pform;
    //chiude
	if (p) p->Close();

	DoClick(pform); //esegue il suono "click!"
}

//---------------------------- HighScore ----------------------------------
//visualizza i migliori punteggi
int HighScore(void *pform)
{
	int count;

	DoClick(pform);

	//controlla se il giocatore entra nella "hall of fame"
	m_iHighScoreIndex=GetHighScorePosition(g_Environ.lCurrentScore);	

	if (m_iHighScoreIndex>-1 && g_Environ.lCurrentScore>0)
	{
		//shifta il vettore
		for (count=HIGH_SCORE_COUNT-1;count>m_iHighScoreIndex;count--)
		{
			memcpy(&m_HighScore[count],&m_HighScore[count-1],sizeof(HIGH_SCORE));
		}

		m_HighScore[m_iHighScoreIndex].lScore =g_Environ.lCurrentScore;
		memset(m_HighScore[m_iHighScoreIndex].szName,0,sizeof(TCHAR)*4);
		strcMainMenu.txtScore.MoveTo(190,165+m_iHighScoreIndex*24);
		strcMainMenu.txtScore.SetAction(SaveHighScoreProc);
		//attiva l'input text in cui scrivere il nome
		strcMainMenu.txtScore.Activate(TRUE);
		strcMainMenu.txtScore.SetInputText("");
	
	}
	else
	{
		//il player non � entrato nella "hall of fame"
		strcMainMenu.txtScore.Activate(FALSE);
		strcMainMenu.txtScore.SetAction(NULL);
	}


	strcMainMenu.frmHScore.SetGraphicManager(&g_gm);
	strcMainMenu.frmHScore.SetFrameManager(&g_fm);
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmHScore;
	m_pActiveForm->Open();

	if (m_iHighScoreIndex>-1 && g_Environ.lCurrentScore>0) 	//imposta il fuoco sul testo in cui il player scrive la propria sigla
	{
		strcMainMenu.frmHScore.SetActiveControl(&strcMainMenu.txtScore);
	}

	return 0;
}

//----------------------------- SaveHighScoreProc ----------------------------

//procedura che salva il punteggio e il nome del giocatore nell'high score
int SaveHighScoreProc(void *pform)
{
	if (m_iHighScoreIndex>=0 && m_iHighScoreIndex<HIGH_SCORE_COUNT)
	{		
		if (_tcscmp(strcMainMenu.txtScore.GetInputText(),TEXT(""))==0) return 1;//deve inserire almeno una lettera
		memcpy(m_HighScore[m_iHighScoreIndex].szName,strcMainMenu.txtScore.GetInputText(),3);		
	}

	//salva su file i punteggi
	SaveHighScore();
	strcMainMenu.frmHScore.SetActiveControl(&strcMainMenu.mnuBack);
	strcMainMenu.txtScore.Activate(FALSE);
	g_Environ.lCurrentScore=0;
	m_iHighScoreIndex=-1;

	return 0;
}

//---------------------------- ShowMenuConfig ----------------------------

//------------------------ FUNZIONI DEL MENU PRINCIPALE --------------------------------------
//Le seguenti funzioni sono chiamate dai vari controlli della GUI in risposta
//alle azioni dell'utente, ad esempion, quando viene cambiato lo stato di uno switch nel menu configurazione
//Mostra il menu di configurazione
//pform � la form attuale che verr� chiusa
int ShowMenuConfig(void *pform)
{
	DoClick(pform);
	strcMainMenu.frmConfig.SetGraphicManager(&g_gm);
	strcMainMenu.frmConfig.SetFrameManager(&g_fm);

	strcMainMenu.mnuJoy.SetValue(g_Environ.iInput==EN_JOYSTICK);
	strcMainMenu.mnuKyb.SetValue(g_Environ.iInput==EN_KEYBOARD);

	strcMainMenu.mnuBack.SetAction(ShowMainMenu);
	
	if (m_pActiveForm) m_pActiveForm->Close();	
	//attiva il menu di configurazione
	m_pActiveForm=&strcMainMenu.frmConfig;	
	m_pActiveForm->Open();
	
	return 0;
}

//mostra il menu principale
int ShowMainMenu(void *pform)
{
	DoClick(pform);
//	strcMainMenu.frmConfig.Close();	
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmMain;
	m_pActiveForm->Open();
	return 0;
}

//mostra la pagina di help
int ShowHelp(void *pform)
{
	DoClick(pform);
	strcMainMenu.frmHelp.SetGraphicManager(&g_gm);
	strcMainMenu.frmHelp.SetFrameManager(&g_fm);
//	strcMainMenu.frmMain.Close();
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmHelp;
	m_pActiveForm->Open();
//	strcMainMenu.frmHelp.Listen();	
	return 0;
}

//visualizza la form che permette di selezionare la lingua
int ShowLanguage(void *pform)
{
	DoClick(pform);
    
	strcMainMenu.mnuBack.SetAction(ShowMenuConfig);

	if (g_Environ.iLanguage==0) {strcMainMenu.mnuIt.SetValue(TRUE);strcMainMenu.mnuEn.SetValue(FALSE);}  //italiano 
	else {strcMainMenu.mnuEn.SetValue(TRUE);strcMainMenu.mnuIt.SetValue(FALSE);}

	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmLanguage;
	m_pActiveForm->Open();

	return 0;
}

//resetta i controlli con le impostazioni di default
int SetDefaultControls(void *pform)
{
	//DIK_UP,DIK_DOWN ecc... sono le costanti dei tasti directx definite in dinput.h
	strcMainMenu.mnuUp.SetKeyCode(DIK_UP);
	strcMainMenu.mnuDown.SetKeyCode(DIK_DOWN);
	strcMainMenu.mnuLeft.SetKeyCode(DIK_LEFT);
	strcMainMenu.mnuRight.SetKeyCode(DIK_RIGHT);
	strcMainMenu.mnuFire1.SetKeyCode(DIK_LCONTROL);
	strcMainMenu.mnuFire2.SetKeyCode(DIK_SPACE);
	strcMainMenu.mnuFire3.SetKeyCode(DIK_Z);

	return 0;
}

//salva le impostazioni correnti
int SaveEnviron(void)
{
	FILE *pf=NULL;
	TP_ENVIRON tmp;
	int res=0;
	const char fname[]="config.bin";

	//resetta la struttura
	memset(&tmp,0,sizeof(TP_ENVIRON));

	tmp.iFire1Key=g_Environ.iFire1Key;
	tmp.iFire2Key=g_Environ.iFire2Key;
	tmp.iFire3Key=g_Environ.iFire3Key;
	tmp.iMoveUp=g_Environ.iMoveUp;
	tmp.iMoveDown=g_Environ.iMoveDown;
	tmp.iMoveLeft=g_Environ.iMoveLeft;
	tmp.iMoveRight=g_Environ.iMoveRight;
	tmp.iLanguage=g_Environ.iLanguage;
	tmp.iInput=g_Environ.iInput;

	pf=fopen(fname,"wb");

	if (pf)
	{
		fwrite(&tmp,sizeof(TP_ENVIRON),1,pf);
		fclose(pf);
		return 1; //ok
	}

	else return 0; //errore
}


//salva la configurazione dei controlli
int SaveControlsConfig(void *pform)
{
	int res;

	g_Environ.iMoveUp=strcMainMenu.mnuUp.GetKeyCode();
    g_Environ.iMoveLeft =strcMainMenu.mnuLeft.GetKeyCode();
	g_Environ.iMoveRight=strcMainMenu.mnuRight.GetKeyCode();
	g_Environ.iMoveDown=strcMainMenu.mnuDown.GetKeyCode();
	g_Environ.iFire1Key=strcMainMenu.mnuFire1.GetKeyCode();
	g_Environ.iFire2Key=strcMainMenu.mnuFire2.GetKeyCode();
	g_Environ.iFire3Key=strcMainMenu.mnuFire3.GetKeyCode();
	
	//nel file deve salvare solo le informazioni relative alla configurazione tasti

	if (SaveEnviron())
	{
	
		res=1;

		if (m_pActiveForm) m_pActiveForm->Close();
		m_pActiveForm=&strcMainMenu.frmMessage;
		strcMainMenu.frmMessage.SetGraphicManager(&g_gm);
		strcMainMenu.frmMessage.SetFrameManager(&g_fm);
		strcMainMenu.mnuMessage.SetGraphicManager(&g_gm);
		strcMainMenu.mnuMessage.SetFrameManager(&g_fm);
		strcMainMenu.mnuMessage.SetText(TranslateGameMsg(31)); //CONFIGURAZIONE SALVATA!
		strcMainMenu.mnuMessage.Justify(JU_CENTER,g_Environ.iScreenWidth);
		strcMainMenu.mnuMessage1.Activate(FALSE); //la seconda riga non serve
		strcMainMenu.btuOk.SetAction(ShowFrmControls);
		strcMainMenu.frmMessage.Open();
	}

	return res;

}

//carica dal file di configurazione l'impostazione dei tasti
int LoadControlsConfig(void *pform)
{
	FILE *pf;
	TP_ENVIRON tmp;
	int res=0;
	size_t tsize=0;

	memset(&tmp,0,sizeof(TP_ENVIRON));

	int a=sizeof(TP_ENVIRON);

	//apre in lettura binaria il file
	pf=fopen("config.bin","rb");

	//se il file non esiste o non lo puo' aprire carica la configurazione di default
	if (pf)
	{
		tsize=fread(&tmp,sizeof(TP_ENVIRON),1,pf);

		a=sizeof(tmp);

		
		if (tsize)
		{
			//lettura ok
			g_Environ.iFire1Key=tmp.iFire1Key;
			g_Environ.iFire2Key=tmp.iFire2Key;
			g_Environ.iFire3Key=tmp.iFire3Key;
			g_Environ.iMoveDown=tmp.iMoveDown;
			g_Environ.iMoveLeft=tmp.iMoveLeft;
			g_Environ.iMoveRight=tmp.iMoveRight;
			g_Environ.iMoveUp=tmp.iMoveUp;
			g_Environ.iLanguage=tmp.iLanguage; //ricarica la lingua
			g_Environ.iInput=tmp.iInput;

			fclose(pf);

			return 1;

		}	

		fclose(pf);

	}

	//errore di lettura o file inesistente: usa i defaults
	g_Environ.iFire1Key=DIK_LCONTROL;
	g_Environ.iFire2Key=DIK_SPACE;
	g_Environ.iFire3Key=DIK_Z;
	g_Environ.iMoveDown=DIK_DOWN;
	g_Environ.iMoveLeft=DIK_LEFT;
	g_Environ.iMoveRight=DIK_RIGHT;
	g_Environ.iMoveUp=DIK_UP;

	return 0;

}


int ShowFrmControls(void *pform)
{
	DoClick(pform);
	strcMainMenu.frmControls.SetGraphicManager(&g_gm);
	strcMainMenu.frmControls.SetFrameManager(&g_fm);

	strcMainMenu.mnuBack.SetAction(ShowMenuConfig);

	//imposta il valore corrente nei controlli
	strcMainMenu.mnuUp.SetKeyCode(g_Environ.iMoveUp);
	strcMainMenu.mnuDown.SetKeyCode(g_Environ.iMoveDown);
	strcMainMenu.mnuLeft.SetKeyCode(g_Environ.iMoveLeft);
	strcMainMenu.mnuRight.SetKeyCode(g_Environ.iMoveRight);
	strcMainMenu.mnuFire1.SetKeyCode(g_Environ.iFire1Key);
	strcMainMenu.mnuFire2.SetKeyCode(g_Environ.iFire2Key);
	strcMainMenu.mnuFire3.SetKeyCode(g_Environ.iFire3Key);

	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmControls;
	m_pActiveForm->Open();
	return 0;
}


//mostra la pagina dei credits
int ShowCredits(void *pform)
{
	DoClick(pform);
	strcMainMenu.frmCredits.SetGraphicManager(&g_gm);
	strcMainMenu.frmCredits.SetFrameManager(&g_fm);
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmCredits;
	m_pActiveForm->Open();
	return 0;

}

int ShowChooseLevel(void *pform)
{
	//per default carica i livelli standard
	g_szLoadCustomLevel=NULL;

	//imposta il menu che torna alla pagina prec.
	strcMainMenu.mnuBack.SetAction(ShowMainMenu);
 
	DoClick(pform);		
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmChooseLevel;
	m_pActiveForm->Open();
	return 0;

}

int ShowChooseExtraLevel(void *pform)
{
	//premendo indietro da qui si torna alla pagina precedente
	strcMainMenu.mnuBack.SetAction(ShowChooseLevel);
 
	DoClick(pform);		
	if (m_pActiveForm) m_pActiveForm->Close();
	m_pActiveForm=&strcMainMenu.frmChooseExtraLevel;
	m_pActiveForm->Open();
	return 0;
}


int DoClick(void *pform)
{
	if (g_Environ.bSound)
	{
		g_cs.PlaySound(strcMainMenu.lsndClick,0,0);
	}

	return 0;
}

//l'utente ha cambiato lo switch usa la tastiera
int LinkSwitches1(void *pform)
{
	BOOL bv;

	bv=strcMainMenu.mnuKyb.Value();	
	strcMainMenu.mnuJoy.SetValue(!bv);
	
	if (!bv) g_Environ.iInput=EN_JOYSTICK; //il giocatore sceglie il joystick
	else g_Environ.iInput=EN_KEYBOARD; //ha scelto la tastiera

	DoClick(pform);
	
	return 0;

}

//l'utente ha cambiato lo switch "usa il joy"
int LinkSwitches2(void *pform)
{
	BOOL bv;

	bv=strcMainMenu.mnuJoy.Value();
	strcMainMenu.mnuKyb.SetValue(!bv);

	if (bv) g_Environ.iInput=EN_JOYSTICK;
	else g_Environ.iInput=EN_KEYBOARD;

	DoClick(pform);

	return 0;
}

//abilita/disabilita il sonoro
int SwitchSound(void *pform)
{
	DoClick(pform);

	g_Environ.bMusic=strcMainMenu.mnuSound.Value();
	if (g_Environ.bMusic) g_cm.PlayMIDI(DMUS_SEG_REPEAT_INFINITE);
	else g_cm.StopMIDI();

	return 0;
}

//cambia la lingua corrente
int SwitchLanguageIt(void *pform)
{
	DoClick(pform);
	strcMainMenu.mnuEn.SetValue(!strcMainMenu.mnuIt.Value());

    if (strcMainMenu.mnuIt.Value()) g_Environ.iLanguage=0; //default ITALIANO

	else g_Environ.iLanguage=1; //INGLESE

	UpdateLanguage();

	return 0;

}

int SwitchLanguageEn(void *pform)
{
   
	DoClick(pform);
	strcMainMenu.mnuIt.SetValue(!strcMainMenu.mnuEn.Value());

	if (strcMainMenu.mnuIt.Value()) g_Environ.iLanguage=0; //default ITALIANO

	else g_Environ.iLanguage=1; //INGLESE

	UpdateLanguage();

	return 0;

}

//aggiorna la lingua corrente
void UpdateLanguage(void)
{
	strcMainMenu.mnuStart.SetText(TranslateGameMsg(0));
	strcMainMenu.mnuConfig.SetText(TranslateGameMsg(1));
	strcMainMenu.mnuHelp.SetText(TranslateGameMsg(2));
	strcMainMenu.mnuCredits.SetText(TranslateGameMsg(3));
	strcMainMenu.mnuExit.SetText(TranslateGameMsg(4));
	strcMainMenu.mnuHScore.SetText(TranslateGameMsg(5));
	strcMainMenu.mnuHScoreTitle.SetText(TranslateGameMsg(5));
	strcMainMenu.mnuLanguage.SetText(TranslateGameMsg(44));
	strcMainMenu.mnuSound.SetText(TranslateGameMsg(6));
	strcMainMenu.mnuKyb.SetText(TranslateGameMsg(7));
	strcMainMenu.mnuJoy.SetText(TranslateGameMsg(8));
	strcMainMenu.mnuBack.SetText(TranslateGameMsg(9));
	strcMainMenu.mnuConfigKeyb.SetText(TranslateGameMsg(10));
	strcMainMenu.mnuUp.SetText(TranslateGameMsg(11));
	strcMainMenu.mnuDown.SetText(TranslateGameMsg(12));
	strcMainMenu.mnuRight.SetText(TranslateGameMsg(13));
	strcMainMenu.mnuLeft.SetText(TranslateGameMsg(14));
	strcMainMenu.mnuFire1.SetText(TranslateGameMsg(15));
	strcMainMenu.mnuFire2.SetText(TranslateGameMsg(16));
	strcMainMenu.mnuFire3.SetText(TranslateGameMsg(17));
	strcMainMenu.mnuSaveConfig.SetText(TranslateGameMsg(18));
	strcMainMenu.mnuDefaults.SetText(TranslateGameMsg(19));
	strcMainMenu.mnuChooseTitle.SetText(TranslateGameMsg(48));
	strcMainMenu.mnuCustomLevel.SetText(TranslateGameMsg(52));

	SaveEnviron();

}

//-----------------------------------------------------------------------------------------------

//crea un oggetto buffer a 16 bit da una immagine
HRESULT CreateBufferFromImg(const IMAGE_FRAME_PTR pimg,CImgBuffer16 *pbuff)
{
	WORD *wBuff;
	LONG lPitch;
	
	if (!pimg) return E_FAIL;
    
	if (pimg->status <1) return E_FAIL;    
	//blocca la superficie ed acquisisce il buffer
	wBuff=(WORD *)g_fm.GetBuffer(pimg,&lPitch);
	
	if (!wBuff) return E_FAIL;			
	
	pbuff->SetBuffer(wBuff,lPitch / sizeof(WORD), pimg->height,pimg->width);
	
	return S_OK;
}

//------------------------------------ ConsoleLoop ---------------------------------------------------
//ciclo del menu
void ConsoleLoop(void)
{
	if (m_pActiveForm)
	{
		m_pActiveForm->Listen();
	}
}


//------------------------------------ LoadHighScore ------------------------------------------------
int LoadHighScore(void)
{
/*
		m_HighScore[0].lScore=12310;
	memcpy(m_HighScore[0].szName,"CPT",3);
	m_HighScore[1].lScore=12300;
	memcpy(m_HighScore[1].szName,"CPU",3);
	m_HighScore[2].lScore=8990;
	memcpy(m_HighScore[2].szName,"SOK",3);
	m_HighScore[3].lScore=5000;
	memcpy(m_HighScore[3].szName,"PIX",3);
	m_HighScore[4].lScore=2500;
	memcpy(m_HighScore[4].szName,"ZSW",3);
	m_HighScore[5].lScore=2000;
	memcpy(m_HighScore[5].szName,"OP8",3);
	m_HighScore[6].lScore=9000;
	memcpy(m_HighScore[6].szName,"NEJ",3);
		m_HighScore[7].lScore=9000;
	memcpy(m_HighScore[7].szName,"LUG",3);
	m_HighScore[8].lScore=20000;
	memcpy(m_HighScore[8].szName,"BOS",3);
	m_HighScore[9].lScore=3560;
	memcpy(m_HighScore[9].szName,"JIM",3);
	
	SaveHighScore();*/	

	FILE *pf=NULL;
	int res=0;
	const char fname[]="score.bin";
	int iCur=-1;
	int count,count1;
	HIGH_SCORE swap;

	for (count=0;count<HIGH_SCORE_COUNT;count++) 
	{
		memset(&m_HighScore[count],0,sizeof(HIGH_SCORE));
	}

	pf=fopen(fname,"rb");

	if (pf==NULL) 
	{
		//crea il file per scriverci sopra i punteggi
		pf=fopen(fname,"wb");
		if (pf)	fwrite(m_HighScore,sizeof(HIGH_SCORE),HIGH_SCORE_COUNT,pf);
		fclose(pf);
		pf=fopen(fname,"rb");
	}

	if (pf)
	{
		//carica i punteggi in memoria
		fread(m_HighScore,sizeof(HIGH_SCORE),HIGH_SCORE_COUNT,pf);
		res=1;
	}

	//sorting del vettore dei punteggi
	for (count=0;count<HIGH_SCORE_COUNT;count++)
	{
		for (count1=HIGH_SCORE_COUNT;count1>count;count1--)
		{
			if (m_HighScore[count1].lScore>=m_HighScore[count].lScore)
			{
				memcpy(&swap,&m_HighScore[count],sizeof(HIGH_SCORE));
				memcpy(&m_HighScore[count],&m_HighScore[count1],sizeof(HIGH_SCORE));
				memcpy(&m_HighScore[count1],&swap,sizeof(HIGH_SCORE));
			}
		}
	}

	return res;
}

//---------------------------------- CreateDefaultHighScores ----------------------------------
//Crea la tabella di default dei punteggi massimi
void CreateDefaultHighScores(void)
{

	HIGH_SCORE *p;

	int init_score=50000;

	for (int i=0;i<HIGH_SCORE_COUNT;i++)
	{
		p=&m_HighScore[i];

		p->lScore=init_score+(i*50000);
		memset(p->szName,0,4);
		memcpy(p->szName,"CPU",3);

	}

	SaveHighScore();

};

//---------------------------------- SaveHighScore --------------------------------------------
//salva i punteggi memorizzati in m_HighScore

int SaveHighScore(void)
{
	int res=0;
	FILE* pf=NULL;
	const char fname[]="score.bin";
	
	pf=fopen(fname,"wb");

	if (pf)
	{
		fwrite(m_HighScore,sizeof(HIGH_SCORE),HIGH_SCORE_COUNT,pf);
		fclose(pf);
		res=1;
	}

	return res;
}


//---------------------- GetHighScorePosition ---------------------------
//Rende la posizione nel vettore high score del punteggio lScore
//se lScore non � sufficiente ad entrare nella "hall of fame", allora rende -1
int GetHighScorePosition(LONG lScore)
{
	int res=-1;

	//carica i punteggi nel vettore dei punteggi
	LoadHighScore();

	//controlla se il player � antrato nella "hall of fame"
	for (int count=0;count<HIGH_SCORE_COUNT;count++)
	{
		if (m_HighScore[count].lScore<= lScore)
		{
			res=count; //posizione del player
			break; 		
		}

	}

	return res;
}

//----------------------------- DrawPointer -------------------------------------
//visualizza il puntatore del mouse
void DrawPointer(IMAGE_FRAME_PTR pimgDest,LONG x,LONG y) //definita in starblastrix.cpp
{

	strcMainMenu.sprHand.SetDisplay(pimgDest);
	strcMainMenu.sprHand.RenderToDisplay(x,y);
}

//----------------------------------- ListFiles -------------------------------------------
//szPath=directory e wildcards es. C:\try\*.txt
//estrae la lista dei files e la mette in szFiles

int ListFiles(LPCSTR szFileName,WIN32_FIND_DATA *dest,int maxfiles)
{
	HANDLE h;
	int cnt=1;
	BOOL bLoop=FALSE;

	h=FindFirstFile(szFileName,&dest[0]);

	while (bLoop && cnt<maxfiles)
	{
		bLoop=FindNextFile(h,&dest[cnt++]);
	}

	return cnt;
}
