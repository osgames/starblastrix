//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  starfield.cpp -- source file per la classe CStarField
///////////////////////////////////////////////////////////////////////*/

#include "starfield.h"
#include "math.h"

extern CADXGraphicManager g_gm;
extern CADXFastMath g_fmath;


CStarField::CStarField(int scr_width,int scr_height,int num_particles,int max_velocity) : m_view_dist(500),m_tau(4.0f),m_range(6.0f),m_max_dist(2000)
{
	m_pscreen=NULL;	
	m_width=scr_width;
	m_height=scr_height;
	m_white=g_gm.CreateRGB(255,255,255);

	// alloca la memoria per lo struct e setta a zero tutti i bytes 
	DD_INIT_STRUCT(ddbfx);

	ddbfx.dwFillColor = m_white;

	//distanza del punto di osservazione sulla perpendicolare del piano del video (in pixel)

	m_refvel=max_velocity;

	if (num_particles>0)
	{
		m_nparticles=num_particles;
		m_pParticles=new PARTICLE_TYP[m_nparticles];
		//vettore delle posizioni iniziali: quando una particella esce dallo schermo,
		//gli viene riassegnata una posizion a caso copiata da questo vettore
		m_pTemplateParticles=new PARTICLE_TYP[m_nparticles];
		//popola il vettore con i valori iniziali
		InitStarField();
		//esegue una copia del vettore
		memcpy(m_pTemplateParticles,m_pParticles,sizeof(PARTICLE_TYP)*m_nparticles);
		//trasla le posizioni iniziali oltre il bordo destro delllo schermo
		for (int count=0;count<m_nparticles;count++)
		{
			m_pTemplateParticles[count].x+=m_width;			
		}

		m_last=&m_pParticles[m_nparticles-1];
	}

	else
	{
		m_pParticles=NULL;
		m_pTemplateParticles=NULL;
		m_nparticles=0;
		m_last=NULL;

	}
}

void CStarField::SetDestImage(IMAGE_FRAME_PTR pimg)
{
	//immagine di sfondo su cui disegna l'effetto
	m_pscreen=pimg;
	m_pDDS=m_pscreen->surface;
}

CStarField::~CStarField(void)
{
	SAFE_DELETE_ARRAY(m_pParticles);
	SAFE_DELETE_ARRAY(m_pTemplateParticles);
	m_nparticles=0;
}

//restituisce un numero nell'intervallo [0,1)
float CStarField::NormVal(float x)
{
	return (float)(1-exp(-fabs(x)/m_tau));
}

//numero casuale con la distribuzione definita da NormVal
float CStarField::RndNormVal(void)
{
	return NormVal((float)g_fmath.RndFast((long)m_range*1000)/1000);
}

//inizializza una nuova particella
void CStarField::CreateParticle(PARTICLE_TYP_PTR p)
{
	float n=RndNormVal();
	float dist=n*m_max_dist; //fra 0 e 2000

	p->y=(float)g_fmath.RndFast(m_height); //posizione casuale lungo l'altezza
	//posizione casuale lungo la lunghezza 
	p->x=(float)g_fmath.RndFast(m_width+40);
	p->velocity=(m_view_dist*m_refvel)/(m_view_dist+dist);

	if (dist<200) 
	{
		p->size=2;
	}
	else p->size=1;

	int imaxdarker=40; //valore minimo del valore di ciascon canale RGB (se si mette 0 le particelle piu' distanti risultano nere)
	int icol=(int)((float)(255-imaxdarker)*(((float)m_max_dist-dist)/m_max_dist)+imaxdarker);
	
	p->color=g_gm.CreateRGB(icol,icol,icol);
}

void CStarField::InitStarField(void)
{
	for (int cnt=0;cnt<m_nparticles;cnt++)
	{
		CreateParticle(&m_pParticles[cnt]);
	}

}
	
void CStarField::Update(void)
{
	if (!m_nparticles) return;
	
	m_p=m_pParticles;

	for (int count=0;count<m_nparticles;count++)
	{	
		m_p=&m_pParticles[count];
		//scrolla a sinistra
		m_p->x-=m_p->velocity;
		
		//quando oltrepassa il bordo sinistro ricrea la particella
		//riassegnandole a caso una posizione iniziale presa fra quelle create 
		if (m_p->x<-3) 
		{			
			memcpy(m_p,&m_pTemplateParticles[g_fmath.RndFast(m_nparticles-1)],sizeof(PARTICLE_TYP));
		
		}		
	}	

}

void CStarField::Render(void)
{
	if (!m_nparticles) return;

	m_p=m_pParticles;

	for (int count=0;count<m_nparticles;count++)
	{
		m_p=&m_pParticles[count];
		rcDest.left=(int)(m_p->x);
		rcDest.top=(int)(m_p->y+1);
		rcDest.right=rcDest.left+m_p->size;
		rcDest.bottom=rcDest.top+m_p->size;	
		ddbfx.dwFillColor=m_p->color;
	    // Blitting del rettangolo
		m_pDDS->Blt( &rcDest, NULL, NULL, DDBLT_WAIT | DDBLT_COLORFILL, &ddbfx );	

	}

}
