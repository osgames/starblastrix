//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  grndauxen.cpp -- nemici a terra ausiliari
///////////////////////////////////////////////////////////////////////*/

#ifndef _GRNDAUXEN_INCLUDE_
#define _GRNDAUXEN_INCLUDE_

#include "sbengine.h"
#include "enemy.h"
#include "resman.h"

typedef enum BASE_STATE_ENUM_TAG
{
	bs_closed=0,
	bs_opening=1,
	bs_open=2,
	bs_closing=3,


} BASE_STATE_ENUM;

class CShieldBase:public CGroundObject
{
private:

	int absx,absy; //coordinate assolute

protected:

	int m_ifstart,m_ifend;
	BASE_STATE_ENUM m_state;
	CObjectStack* m_pSmoke;
	LONG m_shot_sound;
	LONG m_door_sound;

public:

	CShieldBase(void);
	BOOL Update(void);
	void Reset(void);
	HRESULT SetSmokeStack(CObjectStack *pStack); //imposta lo stack fumo
	//imposta il range delle frame che definiscono l'intervallo di apertura e chiusura
	HRESULT SetFrameRange(int fstart,int fend); 
	//effetto sonoro dello sparo
	void SetShotSound(LONG sound);
	void SetDoorSound(LONG sound);
};

void FreeGrndAuxRes(void);
HRESULT LoadGrndAuxEnRes(int resid,CResourceManager *prm);
HRESULT CreateGrndAuxStack(int resid,int numitems,CObjectStack *pstack,CResourceManager* prm,int flags);


#endif