/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
    modular.cpp - classe base per gli oggetti modulari
*/

#include "modular.h"


CModularEnemy::CModularEnemy(void)
{
	m_iLinkCount=0;
	for (int count=0;count<MAX_MOD_LINKS;count++) m_LinkedEnemies[count]=NULL;
}

//attacca questo oggetto ad un oggetto genitore
int CModularEnemy::AttachEnemy(int iCoupling,int iParentCoupling,CModularEnemy *pModEnemy)
{
	if (pModEnemy && m_iLinkCount<MAX_MOD_LINKS)
	{
		//attacca il nemico a questo nemico
		if (pModEnemy->Frame.LinkToComponent(iParentCoupling,iCoupling,&this->Frame))
		{			
			m_LinkedEnemies[m_iLinkCount]=pModEnemy;
			m_iLinkCount++;
			return 1;
		}
	}

	return 0;
}


CModularEnemy* CModularEnemy::GetAttachedEnemy(int index)
{
	if (index>=0 && index<m_iLinkCount)
	{
		return m_LinkedEnemies[index];
	}

	else return NULL;

}

void CModularEnemy::Reset(void)
{
	CEnemy::Reset();

	for (int count=0;count<m_iLinkCount;count++)
	{
		if (m_LinkedEnemies[count])
		{
			m_LinkedEnemies[count]->Reset();			
		}
	}

}

void CModularEnemy::SetEnergy(LONG energy)
{
	dwEnergy=energy;

	for (int count=0;count<m_iLinkCount;count++)
	{
		if (m_LinkedEnemies[count])
		{
			m_LinkedEnemies[count]->SetEnergy(energy);			
		}
	}

}

//scollega tutti gli elementi
void CModularEnemy::Unlink(void)
{
	//scollega tutti i componenti
	for (int count=0;count<m_iLinkCount;count++)
	{
		if (m_LinkedEnemies[count])
		{
			m_LinkedEnemies[count]->Frame.Unlink();
			m_LinkedEnemies[count]=NULL;
		}
	}

	m_iLinkCount=0;
}

//aggiornamento di tutti gli oggetti nella lista (nota bene: aggiorna solo gli elementi collegati a questo oggetto poi 
//sta a gli elementi collegati a far proseguire il messaggio ai propri elementi collegati)
BOOL CModularEnemy::UpdateAI(void)
{
	if (dwEnergy<0) return FALSE;

	CModularEnemy *pen;

	BOOL bres=FALSE;

	for (int count=0;count<m_iLinkCount;count++)
	{
		pen=m_LinkedEnemies[count];
		if (pen)
		{
			//invia il messaggio che indica la posizione del player
			pen->lTargetX=lTargetX;
			pen->lTargetY=lTargetY;

			if (pen->dwEnergy>0)
			{
				bres |= pen->Update(); //Update del'oggetto collegato chiama UpdateAI a sua volta
			}
		}
	}

	//nota: fino a che c'� un modulo attivo non si deve rimuovere l'oggetto
	return bres;
}


void CModularEnemy::SetPosition(const LONG x,const LONG y)
{
	CADXSprite::SetPosition(x,y);
	Frame.SetPosition(x,y);

}

HRESULT CModularEnemy::Draw(void)
{
	Frame.SetDisplay(m_pimgOut);
	HRESULT hr=Frame.RenderToDisplay();

	for (int count=0;count<m_iLinkCount;count++)
	{
		if (m_LinkedEnemies[count]->dwEnergy>0)
		{
			m_LinkedEnemies[count]->SetDisplay(m_pimgOut);
			m_LinkedEnemies[count]->Draw();		
		}
	}

	return hr;
}

void CModularEnemy::Explode(void)
{
	if (lpfnMultiExplosion && Frame.m_iTag1>0) lpfnMultiExplosion(&Frame,8);
	dwEnergy=0;
	Frame.m_iTag1=0;

	for (int count=0;count<m_iLinkCount;count++)
	{
		m_LinkedEnemies[count]->Explode();
	}

}

//esegue la funzione di collisione per tutti gli elementi della lista che sono linkati a questo
//rende true se c'� collisione con questo o con uno degli oggetti linkati

BOOL CModularEnemy::DoCollisionWith(CSBObject *pobj)
{

	BOOL bres=FALSE;

	CModularEnemy *pen;

	for (int count=0;count<m_iLinkCount;count++)
	{
		pen=m_LinkedEnemies[count];

		if (pen)
		{	
			if (pen->dwEnergy>0)
			{
				bres |= pen->DoCollisionWith(pobj);			
			}
		}
	}

	Frame.m_iTag1=dwEnergy;

	if (dwEnergy>0)
	{

		//deve controllare la collisione fra pobj e il corpo
		if (Frame.DetectCollision((CADXSprite *)pobj))
		{
			DamageComponent(&Frame,pobj);					
			dwEnergy=Frame.m_iTag1;
			
			if(dwEnergy<=0)
			{			
				CModularEnemy::Explode();			
			}

			return TRUE;
		}

	}

	return bres;	
}



