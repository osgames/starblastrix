# Microsoft Developer Studio Project File - Name="StarBlastrix" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=StarBlastrix - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "StarBlastrix.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "StarBlastrix.mak" CFG="StarBlastrix - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "StarBlastrix - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "StarBlastrix - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "StarBlastrix - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Ob1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x410 /d "NDEBUG"
# ADD RSC /l 0x410 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib ddraw.lib dxguid.lib dinput.lib dsound.lib winmm.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "StarBlastrix - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x410 /d "_DEBUG"
# ADD RSC /l 0x410 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib ddraw.lib dxguid.lib dinput.lib dsound.lib winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "StarBlastrix - Win32 Release"
# Name "StarBlastrix - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\Arcade32\a32audio.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32debug.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32fastm.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32graph.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32img16.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32input.cpp
# End Source File
# Begin Source File

SOURCE=.\a32object.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32sprit.cpp
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32util.cpp
# End Source File
# Begin Source File

SOURCE=.\antia3.cpp
# End Source File
# Begin Source File

SOURCE=.\asteroid.cpp
# End Source File
# Begin Source File

SOURCE=.\blaster2.cpp
# End Source File
# Begin Source File

SOURCE=.\bleezer.cpp
# End Source File
# Begin Source File

SOURCE=.\boss1.cpp
# End Source File
# Begin Source File

SOURCE=.\ccharset.cpp
# End Source File
# Begin Source File

SOURCE=.\ceffcts.cpp
# End Source File
# Begin Source File

SOURCE=.\enemy.cpp
# End Source File
# Begin Source File

SOURCE=.\gfighter.cpp
# End Source File
# Begin Source File

SOURCE=.\grndauxen.cpp
# End Source File
# Begin Source File

SOURCE=.\jltent.cpp
# End Source File
# Begin Source File

SOURCE=.\level.cpp
# End Source File
# Begin Source File

SOURCE=.\lifter.cpp
# End Source File
# Begin Source File

SOURCE=.\mammoth.cpp
# End Source File
# Begin Source File

SOURCE=.\mcasmbl.cpp
# End Source File
# Begin Source File

SOURCE=.\mech.cpp
# End Source File
# Begin Source File

SOURCE=.\mecharm.cpp
# End Source File
# Begin Source File

SOURCE=.\modenemy.cpp
# End Source File
# Begin Source File

SOURCE=.\modular.cpp
# End Source File
# Begin Source File

SOURCE=.\resman.cpp
# End Source File
# Begin Source File

SOURCE=.\ring.cpp
# End Source File
# Begin Source File

SOURCE=.\sb.rc
# End Source File
# Begin Source File

SOURCE=.\sbengine.cpp
# End Source File
# Begin Source File

SOURCE=.\sbl.cpp
# End Source File
# Begin Source File

SOURCE=.\sblastrix.cpp
# End Source File
# Begin Source File

SOURCE=.\sbutil.cpp
# End Source File
# Begin Source File

SOURCE=.\snake.cpp
# End Source File
# Begin Source File

SOURCE=.\snakeres.cpp
# End Source File
# Begin Source File

SOURCE=.\spider.cpp
# End Source File
# Begin Source File

SOURCE=.\starfield.cpp
# End Source File
# Begin Source File

SOURCE=.\tent.cpp
# End Source File
# Begin Source File

SOURCE=.\thunder.cpp
# End Source File
# Begin Source File

SOURCE=.\train.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Arcade32\a32audio.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32debug.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32fastm.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32graph.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32img16.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32input.h
# End Source File
# Begin Source File

SOURCE=.\a32object.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32sprit.h
# End Source File
# Begin Source File

SOURCE=..\Arcade32\a32util.h
# End Source File
# Begin Source File

SOURCE=.\antia3.h
# End Source File
# Begin Source File

SOURCE=.\asteroid.h
# End Source File
# Begin Source File

SOURCE=.\blaster2.h
# End Source File
# Begin Source File

SOURCE=.\bleezer.h
# End Source File
# Begin Source File

SOURCE=.\boss1.h
# End Source File
# Begin Source File

SOURCE=.\ccharset.h
# End Source File
# Begin Source File

SOURCE=.\ceffcts.h
# End Source File
# Begin Source File

SOURCE=.\enemy.h
# End Source File
# Begin Source File

SOURCE=.\gfighter.h
# End Source File
# Begin Source File

SOURCE=.\grndauxen.h
# End Source File
# Begin Source File

SOURCE=.\jltent.h
# End Source File
# Begin Source File

SOURCE=.\level.h
# End Source File
# Begin Source File

SOURCE=.\lifter.h
# End Source File
# Begin Source File

SOURCE=.\mammoth.h
# End Source File
# Begin Source File

SOURCE=.\mcasmbl.h
# End Source File
# Begin Source File

SOURCE=.\mech.h
# End Source File
# Begin Source File

SOURCE=.\mecharm.h
# End Source File
# Begin Source File

SOURCE=.\modenemy.h
# End Source File
# Begin Source File

SOURCE=.\modular.h
# End Source File
# Begin Source File

SOURCE=.\resman.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\ring.h
# End Source File
# Begin Source File

SOURCE=.\sbengine.h
# End Source File
# Begin Source File

SOURCE=.\sbl.h
# End Source File
# Begin Source File

SOURCE=.\sbutil.h
# End Source File
# Begin Source File

SOURCE=.\snake.h
# End Source File
# Begin Source File

SOURCE=.\snakeres.h
# End Source File
# Begin Source File

SOURCE=.\spider.h
# End Source File
# Begin Source File

SOURCE=.\starfield.h
# End Source File
# Begin Source File

SOURCE=.\tent.h
# End Source File
# Begin Source File

SOURCE=.\thunder.h
# End Source File
# Begin Source File

SOURCE=.\train.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\icon1.ico
# End Source File
# End Group
# End Target
# End Project
