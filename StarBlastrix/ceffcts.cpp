////////////////////////////////////////////////////////////////////////
//// Gestione Effetti grafici di transizione
//// Code by L.Berti 2002-2006
/////////////////////////////////////////////////////////////////////////

#include "ceffcts.h"

//////////////////////// CADXEffcts /////////////////////////////////////////

CADXEffect::CADXEffect(void)
{

	Clear(); //resetta i parametri
}


CADXEffect::~CADXEffect()
{
	Clear();
}

//--------------------------- GetSteps ------------------------------------------

int CADXEffect::GetSteps(void)
{
	return m_iSteps;
}

//----------------------------  Clear --------------------------------------------

void CADXEffect::Clear(void)
{
	m_pimgSrc=NULL;
	m_iSteps=0;
	m_pimgSrc=NULL;
	m_lPixelSize=0;
	m_pimgSrc=NULL;
	m_pSrcBuff=NULL;
	m_pDestBuff=NULL;
	m_lSrcWidth=0;
	m_lSrcHeight=0;
	m_lDestWidth=0;
	m_lSrcWidth=0;
	m_lDestBuffSize=0;
	m_lSrcBuffSize=0;
}

//---------------------------- SetSourceImg --------------------------------------
//Imposta l'immagine sorgente su cui applicare la trasformazione
//(Questa immagine non viene alterata
HRESULT CADXEffect::SetSourceImg(IMAGE_FRAME_PTR imgSource)
{
	BYTE *buff;

	if (imgSource != NULL)
	{
		if (imgSource->status > 0)
		{
			m_pimgSrc=imgSource; //immagine sorgente impostata			
			m_lSrcWidth=imgSource->width;
			m_lSrcHeight=imgSource->height;

			//blocca la superficie dell'immagine per acquisirne il pitch
			buff=m_lpFm->GetBuffer(imgSource,&m_lPitchSrc);

			if (buff==NULL) return E_FAIL; //ha fallito nel bloccare la superficie

			//calcola la dimensione del buffer di ingresso
			m_lSrcBuffSize=m_lPitchSrc*m_lSrcHeight;

			m_lpFm->ReleaseBuffer(imgSource,buff);

			buff=NULL;
			
			m_iStatus=2; //l'immagine � valida

			return S_OK;
		}

		else return E_FAIL;
	}

	return E_FAIL; //puntatore nullo

}

//------------------------ SetDisplay -------------------------------------

//Ridefinisce la funzione SetDisplay
HRESULT CADXEffect::SetDisplay(IMAGE_FRAME_PTR imgDisplay)
{
	BYTE *buff;

	if (imgDisplay != NULL)
	{
		if (imgDisplay->status > 0)
		{
			m_pimgOut=imgDisplay; //imgout � ereditato da CADXRender			
			//profondit� colore
            m_lPixelSize=m_lpGm->GetScreenBitsPerPixel()/8;

			m_lDestWidth=imgDisplay->width;
			m_lDestHeight=imgDisplay->height;

			//blocca la superficie dell'immagine per acquisirne il pitch e l'indirizzo
			buff=m_lpFm->GetBuffer(imgDisplay,&m_lPitchDest);

			if (buff == NULL) return E_FAIL; //ha fallito nel bloccare la superficie

			//calcola la dimensione del buffer di ingresso
			m_lDestBuffSize=m_lPitchDest*m_lDestHeight;

			memset(buff,0,m_lDestBuffSize); //cancella il display

			m_lpFm->ReleaseBuffer(imgDisplay,buff);
			
			m_iStatus=2; //l'immagine � valida
			
			buff=NULL;

			return S_OK;
		}

		else return E_FAIL;
	}

	return E_FAIL; //puntatore nullo

}

//-------------------------------- GetBuffs ---------------------------------------------

HRESULT CADXEffect::GetBuffs(void) //blocca le superfici e ottiene i puntatori pImgSrc e pImgDest
{
	m_pSrcBuff=m_lpFm->GetBuffer(m_pimgSrc,&m_lPitchSrc);
	m_pDestBuff=m_lpFm->GetBuffer(m_pimgOut,&m_lPitchDest);
	
	if (m_pSrcBuff != NULL && m_pDestBuff !=NULL && m_pDestBuff != m_pSrcBuff) return S_OK;
	else return E_FAIL;	
}

//--------------------------------- ReleaseBuffs -------------------------------------------

HRESULT CADXEffect::ReleaseBuffs(void) //sblocca le superfici
{
	m_lpFm->ReleaseBuffer(m_pimgSrc,m_pSrcBuff);
	m_lpFm->ReleaseBuffer(m_pimgOut,m_pDestBuff);

	return S_OK;	
}

///////////////////////////// CADXBubble //////////////////////////////////////////

HRESULT CADXBubble::RenderToDisplay(void)
{
	register LONG x,y,x1,y1;
	LONG loffs,loffs1;
	LONG rpx,rpy,angle;

	GetBuffs();
	
	m_iSteps++;

	for (y=0;y<m_lDestHeight;y++)
	{
		angle=m_iSteps%360;

		rpy=240-y;

	 //   arg1=(unsigned) ((angle*2+(rpy/6))%360);  	  
	 //   arg4=(unsigned) ((angle*5+(rpy/4))%360);

		for (x=0;x<m_lDestWidth;x++)
		{
			 rpx=(x-320);    
		//	 arg2=(unsigned) ((angle*3+(rpx/7))%360);
		//	 arg3=(unsigned) ((angle*2+(x/6))%360);
	    
			 x1=(rpx*CosFast[angle]+rpy*SinFast[angle])/1000L;
			 y1=(rpx*SinFast[angle]-rpy*CosFast[angle])/1000L;
	     
			 x1=x1+320;
			 y1=240-y1;

			 loffs=x1*m_lPixelSize+m_lPitchSrc*y1;
			 loffs1=x*m_lPixelSize+m_lPitchDest*y;
				
			 if (loffs>=0 && loffs<m_lSrcBuffSize)
			 {
			 	memcpy(m_pDestBuff+loffs,m_pSrcBuff+loffs1,(size_t)m_lPixelSize);
			 }		
		}
	}

	ReleaseBuffs();

	return S_OK;
}

////////////////////////////////// CEffectZ //////////////////////////////////////////
//Esplode l'immagine

void CEffectZ::Clear(void)
{
	m_pimgSrc=NULL;
	m_iSteps=0;
	m_pimgSrc=NULL;
	m_lPixelSize=0;
	m_pimgSrc=NULL;
	m_pSrcBuff=NULL;
	m_pDestBuff=NULL;
	m_lSrcWidth=0;
	m_lSrcHeight=0;
	m_lDestWidth=0;
	m_lSrcWidth=0;
	m_lDestBuffSize=0;
	m_lSrcBuffSize=0;
	
	m_bInit=FALSE;
	m_lHalfH=0;
	m_lHalfW=0;
}

CEffectZ::CEffectZ(void)
{
	Clear();
}

HRESULT CEffectZ::RenderToDisplay(void)
{
	register LONG x,y,x1,y1,k;
	LONG loffs,loffs1;	
	LONG rpx,rpy;
	LONG ld;
	LONG ltmp;

	ltmp=m_lDestHeight-1;
    
	if (!m_bInit)
	{
		m_lpFm->PutImgFrameClip(0,0,m_pimgOut,m_pimgSrc);
		m_bInit=TRUE;
		m_lHalfW=m_lSrcWidth/2; //semilarghezza
		m_lHalfH=m_lSrcHeight/2; //semialtezza
	}

	GetBuffs();

	m_iSteps++;
	
    for (k=0;k<35000;k++) 
	{
		x=rand()%m_lDestWidth;
		y=rand()%ltmp;	//m_lDestHeight-1
		ld=rand()%2;

		//esrgue una traslazione degli assi in modo da centrare gli assi sullo schermo
		rpx=x-m_lHalfW;
		rpy=m_lHalfH-y;		
	    //ogni punto viene allontanato dal centro della superficie 
		x1=rpx+rpx*m_iSteps/6;
		y1=rpy+rpy*m_iSteps/6;

		//ripristina gli assi originali
		x1=x1+m_lHalfW;
		y1=m_lHalfH-y1;

		loffs=x*m_lPixelSize+m_lPitchSrc*y;
        
		if (x1>=0 && x1<m_lDestWidth && y1>=0 && y1<m_lDestHeight)
		{
	
			loffs1=x1*m_lPixelSize+m_lPitchSrc*y1;       
		
			memcpy(m_pDestBuff+loffs1,m_pSrcBuff+loffs,m_lPixelSize);
			
		}
	
		memset(m_pDestBuff+loffs,0,m_lPixelSize);	
	}	

	ReleaseBuffs();

	return S_OK;
}


////////////////////////////////// CEffectIn //////////////////////////////////////////
//Esplode l'immagine

void CEffectIn::Clear(void)
{
	m_pimgSrc=NULL;
	m_iSteps=0;
	m_pimgSrc=NULL;
	m_lPixelSize=0;
	m_pimgSrc=NULL;
	m_pSrcBuff=NULL;
	m_pDestBuff=NULL;
	m_lSrcWidth=0;
	m_lSrcHeight=0;
	m_lDestWidth=0;
	m_lSrcWidth=0;
	m_lDestBuffSize=0;
	m_lSrcBuffSize=0;
	
	m_bInit=FALSE;
	m_lHalfH=0;
	m_lHalfW=0;
}

CEffectIn::CEffectIn(void)
{
	m_pimgSrc=NULL;
	m_iSteps=0;
	m_pimgSrc=NULL;
	m_lPixelSize=0;
	m_pimgSrc=NULL;
	m_pSrcBuff=NULL;
	m_pDestBuff=NULL;
	m_lSrcWidth=0;
	m_lSrcHeight=0;
	m_lDestWidth=0;
	m_lSrcWidth=0;
	m_lDestBuffSize=0;
	m_lSrcBuffSize=0;
	
	m_bInit=FALSE;
	m_lHalfH=0;
	m_lHalfW=0;
}

HRESULT CEffectIn::RenderToDisplay(void)
{
	register LONG x,y,x1,y1,k,lLimit;
	LONG loffs,loffs1;	
	LONG rpx,rpy;
	LONG ld;
	LONG ld1;
    
	if (!m_bInit)
	{
	//	m_lpFm->PutImgFrame(0,0,m_pimgOut,m_pimgSrc);
		m_bInit=TRUE;
		m_lHalfW=m_lSrcWidth/2; //semilarghezza
		m_lHalfH=m_lSrcHeight/2; //semialtezza
	}

	GetBuffs();

	m_iSteps++;
	
    lLimit=m_iSteps*700;

    for (k=0;k<lLimit;k++) 
	{
		x=rand()%m_lSrcWidth-1;
		y=rand()%m_lSrcHeight-1;	
		ld=rand()%2;

		//esrgue una traslazione degli assi in modo da centrare gli assi sullo schermo
		rpx=x-m_lHalfW;
		rpy=m_lHalfH-y;		
		ld1=m_iSteps*m_iSteps*2;
	    //ogni punto viene allontanato dal centro della superficie 
		x1=rpx+rpx*(200/ld1);
		y1=rpy+rpy*(200/ld1);

		//ripristina gli assi originali
		x1=x1+m_lHalfW;
		y1=m_lHalfH-y1;

		loffs=x*m_lPixelSize+m_lPitchSrc*y;
        
		if (x1>=0 && x1<m_lDestWidth && y1>=0 && y1<m_lDestHeight)
		{
	
			loffs1=x1*m_lPixelSize+m_lPitchSrc*y1;       
		
			memcpy(m_pDestBuff+loffs1,m_pSrcBuff+loffs,m_lPixelSize);
			
		}
	
	//	memset(m_pDestBuff+loffs,0,m_lPixelSize);	
	}	

	ReleaseBuffs();

	return S_OK;
}