/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
*/

#ifndef _THUNDER_INCLUDE_
#define _THUNDER_INCLUDE_


#include "a32graph.h"

//--------------------------------- Classe CThunderBolt ---------------------------------------------

#define MAX_TARGET 5
#define MAX_TILE 15 //numero massimo di tile diversi che compongono il fulmine
#define MAX_LINK 15 //numero massimo di frame a cui puo' essere collegata ogni singola frame
#define MAX_THITEMS 40 //numero massimo di elementi che compongono il fulmine

//fulmine
class CThunderBolt:public CSBObject
{
private:

	CSBObject *m_Targets[MAX_TARGET]; //oggetti bersaglio
	CADXSprite *m_pSource; //sorgente
	CSBObject *m_pobj;
	int m_itx,m_ity; //coordinate del bersaglio fisso (oltra agli oggetti bersaglio, si puo' scaricare anche su un punto fisso)
	int m_itcount;
	int m_iItemCount; //numero di elementi nel fulmine
	int m_xt[MAX_THITEMS],m_yt[MAX_THITEMS],m_iFrame[MAX_THITEMS]; //coordinate di output e frame di ogni singolo tile che compone il fulmine
	int m_iStepUpdate,m_iUpdateFreq;
	int m_iTimer,m_iDuration; //durata (in cicli)
	int m_ifrleft,m_ifrtop,m_ifrright,m_ifrbottom; ///frame finali del fulmine
	LONG m_hx,m_hy; //coordinate hot spot della sorgente
	BOOL CThunderBolt::CreateThunder(int xs,int ys,int xd,int yd);

public:

    CThunderBolt(void);
	BOOL Update(void);
	void Reset(void);
	HRESULT Draw(void);
	HRESULT AddTarget(CSBObject *pTarget);
	HRESULT AddTargetXY(int tx,int ty);
	HRESULT SetSource(CADXSprite *pSource);
	HRESULT SetSource(CADXSprite *pSource,LONG hx,LONG hy);
	HRESULT SetFinalFrames(int ileft,int iright,int itop,int ibottom);
	void SetUpdateFreq(int freq);
	void SetDuration(int idur);
};

#endif