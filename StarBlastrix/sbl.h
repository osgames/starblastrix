//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  sbl.h - compilatore di livelli e level editor per StarBlastrix
///////////////////////////////////////////////////////////////////////*/


/*formato del file sbl (star blastrix level)

bytes

  4    id del file 's' 'b' 'l' '0x0'
  sezione intestazione
  {
	  4    (int) chunk type: 0 per l'intestazione
	  50   nome del livello  NAME
	  4    (int) numero del livello ; 0=livello custom fuori dalla sequneza standard
	  100  messaggio finale  MSG
	  20   (char) file vpx che contiene i tile del terreno GROUND_FILE
	  4    (int) indice della frame nel file vpx precedente GROUND_FILE_INDEX
	  20   (char) file vpx che contiene i tile del background BACK_FILE
	  4    (int) indice della frame nel file precedente BACK_FILE_INDEX
	  4    (int) procedura usata per cancellare il background CLEAR_FN
  }
  
  di queste sezioni ce ne possono essere un numero variabile
  sezione GROUND_LAYER
  la seguente struttura � ripetuta tante volte quanti sono i tile del background  
  {
  
	4 (int) chunk type=1
	4 (int) ordinta di output del layer 
	4 (float) parallasse
	4 (int) numero di elementi nella mappa del tile (sequenza indici)
	4 (int) left rettangolo di grabbing dei tiles
	4 (int) top rettangolo di grabbing
	4 (int) width larghezza dei tile
	4 (int) height altezza del tile
	4 (int) numero di righe
	4 (int) numero di colonne (il numero di tiles � dato da righe x colonne il primo tile ha indice 0
    4 (int) serie di elementi della mappa del tile

  }

  di queste sezioni ce ne possono essere un numero variabili
  sezione BACK_LAYER 
  {
     4 (int) chunk type=2
	 4 (int) ordinata di output del layer
	 4 (float) parallasse
	 4 (int) left rettangolo di grabbing
	 4 (int) top rettangolo di grabbing
	 4 (int) larghezza dell''unico tile che componel il layer
	 4 (int) altezza del tile
  }

	DWORD dwPower;	
	DWORD dwFlags;
	DWORD dwEnergy;
    LONG xabs; //ascissa assoluta in cui viene azionato il trigger
	LONG x,y; //cordinate video del punto in cui deve apparire l'oggetto
	CObjectStack *pSrcStack; //punta lo stack dal quale deve prelevare il nemico 
	CObjectStack *pBonusStack; //stack degli oggetti bonus , se � nullo non viene associato nessun bonus
}

//argomenti di CreateTrigger : (LONG xabs,LONG xrel,LONG yrel,DWORD dwPower,DWORD dwEnergy,DWORD dwFlags,CObjectStack *pEnemyStack);	
	

  sezione trigger (evento) il numero di eventi � variabile (non � necessario che vengano inseriti in ordine all'interno del file
  {
      4 (int) chunk type=3
	  4 (int) xabs x assoluta del trigger
	  4 (int) xrel x relativa del trigger	  
	  4 (int) yrel y relativa
	  4 (unigned int) power
	  4 (unsigned int) energy
	  4 (unsigned int) id enemy stack 
	  4 (unsigned int) flags
	  4 (unsigned int) bonus id
	  4 (unsigned int) probabilit� che il trigger venga inserito (1-100) 0 indica 100%
  }

  //sezione TILE_MAP
  //questa sezione deve seguire obbligatoriamente GROUND_LAYER e contiene


  //segue una sequenza di integer quanti sono
  
  //sezione back layers (nota: i layer del back ground non hanno una mappa) 


*/

//------------------------------------------------------------------------------------------------------

/*definizione del linguaggio usato per i file sbl 


<string identifier>:     NAME: nome del livello
                         NAME1:nome del livello nella seconda lingua
                         MSG:  messaggio finale
						 MSG:messaggio finale nella seconda lingua
						 GROUND_FILE: file vpx che contiene i tiles del terreno
						 BACK_FILE:file vpx che contiene i tile del back ground
						 CLEAR_FN:funzione speciale di disegno del background
						 BACK_IMAGE:immagine di sfondo (percorso relativo del file vpx che la contiene)
					
										   
<numeric identifier>:    LEVEL: numero del livello
                       	 GROUND_FILE_INDEX:indice della frame nel file vpx GROUNF_FILE
					     BACK_FILE_INDEX:indice della frame che contiene i tile del back ground
                         BACK_IMAGE_INDEX:indice della frame ell'immagine di sfondo
						 SCROLL_VELOCITY:veloci� di scroll(per default � 1)
                    

<comment>: # + linea di carateri                                 

<numeric value>: un numero interno
<string value>: una stringa senza apici
<float value>:un float

<structure>: GROUND_LAYER yout,left,top,width,height,rows,cols,parallax,mapitems
                          i tile del layer sono contenuti in una griglia che ha rows righe x cols colonne
						  left e top sono le coorrinate dell'angolo in alto a sinistra della griglia
						  parallax � un float che indica la parallasse per questo livello 
						  yout � l'ordinata di output del livello a partire dal fondo dello chermo (asse y rivolto in alto) 
						  La sequenza dei layer all'interno del script va dal layer piu' vicino a quello piu' lontano
             
             BACK_LAYER yout,left,top,width,height,parallax  (yout= asse y rivolto in basso con origine sul margine superiore dello schermo)
			              Layer del back ground
             MAP [sequenza di lunghezza variabile di interi] � la mappa che si riferisce all'ultimo ground layer inserito
		 	 CALL [nome funzione],[elenco parametri] ;value=valore della propriet� da assegnare alla lista dei tile; propriet� dei tile 0=solido 1=liquido 2=inconsistente
             TRIGGER xabs,xrel,yrel,ipower,ienergy,ienemy,iflags,ibonus,iprob
			              trigger
						  xabs=ascissa asoluta nel livello in cui si deve verificare l'evento
						  xrel,yrel=coordinate relative dell'evento (nel caso degli oggetti vincolati al terreno l'asse y � rivolto in alto con orgine sul fondo dello chermo)
						  ipower,ienergy=potenza e energia del nemico
						  ienemy=id del tipo di oggetto
						  iflags=numero intero opzionale che serve a specificare ulteriori propriet� dell'oggetto (il significato di iflags dipende dal tipo di oggetto)
						  ibonus=id del bonus associato (opzionale)
						  iprob=probabilit� che l'evento venga inserito nella lista (opzionale) per default la prob. � 100%         

//grammatica dello script sbl (nota: questa grammatica � ambigua e serve solo come esempio)
<script sbl>:<statement> | <script sbl><statement>
<statement>:<comment> | <instruction>
<instruction>:<numeric identifier>':'<numeric value>';' | <string identifier>:<string value>';' | <structure>':'<list of values>';'
<value>:<numeric value>|<string value>|<float value>
<list of values>:<single value> | <list of values>','<value> |<value>','|<list of values><list of values>


*/

#ifndef _SBL_
#define _SBL_

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif

//------------------------------------- strutture usate per il caricamento e il salvataggio dei livelli

//SBL file head
typedef struct 
{
	int ichunk_type;   // chunk type: 0 per l'intestazione
	char szlevel_name[50]; // nome del livello  NAME
	char szlevel_name1[50]; //nome del livello nella 2� lingua
	char szmusic[50]; //nome del file midi della musica di background
	int ilevel; //numero del livello; serve per non far caricare un livello nella sequenza sbagliata
	char szfinal_message[100]; //messaggio finale  nella 2� lingua
	char szfinal_message1[100];
	char szvpxground_tiles[40]; //(char) file vpx che contiene i tile del terreno GROUND_FILE
	int igrndindex; //indice della frame nel file vpx precedente GROUND_FILE_INDEX
	char szvpxbackground[40]; //(char) file vpx che contiene i tile del background BACK_FILE
	int ibackindex; //(int) indice dell'immagine di back ground
	int iclear_procedure; //procedura usata per cancellare il background CLEAR_FN
	char szvpxsky[40]; //immagine usata come sfondo (opzionale)
	int iskyindex; //indice della frame del file szvpxsky
	int iback_img_top; //ordinata di output dell'immagine di background
	float scroll_velocity; //velocit� di scrolling di default
	float gravity; //accelerazione di gravit� (default 4.9)
	int ibottom;  //livello solido del terreno (a partire dal basso)
	int password; //password per accedere al livello, se zero non richiede password


} SBL_HEAD,*SBL_HEAD_PTR;


//struttura che contiene informazioni su un layer del terreno caricato da file
typedef struct SBL_GROUND_LAYER
{
	int ichunk_type; //4 (int) chunk type=1
	int iyout; //ordinta di output del layer 
	float fparallax; //(float) parallasse
	int inumtiles; // numero di elementi nella mappa del tile (sequenza indici)
	int ircleft; //left rettangolo di grabbing dei tiles
	int irctop; //top rettangolo di grabbing
	int itile_width; //width larghezza dei tile
	int itile_height; //height altezza del tile
	int irows; //numero di righe
	int icols; //numero di colonne (il numero di tiles � dato da righe x colonne il primo tile ha indice 0    
	int *pielems;  //serie di elementi della mappa del tile
	SBL_GROUND_LAYER *next; //elemento successivo nella lista concatenata

} SBL_GROUND_LAYER,*SBL_GROUND_LAYER_PTR;

//nota: il chunk ground_layer � seguito da una serie di interi a 32 bit che indicano gli indici dei tile nella mappa
//il numero di indici � pari a inumtiles

//struttura che contiene la informazioni su un layer dello sfondo
typedef struct SBL_BACK_LAYER 
{
	int ichunk_type; //chunk type=2
	int iyout; //ordinata di output
	float fparallax; //parallasse
	int ircleft; //left rettangolo di grabbing dei tiles
	int irctop; //top rettangolo di grabbing
	int itile_width; //larghezza dell''unico tile che compone il layer
	int itile_height; //altezza del tile
    SBL_BACK_LAYER *next; //elemento successivo nella lista concatenata

} SBL_BACK_LAYER,*SBL_BACK_LAYER_PTR;


//struttura usata per caricare un trigger dal file trigger
typedef struct SBL_TRIGGER 
{
	int ichunk_type; // chunk type=3
    int xabs; //x assoluta del trigger
	int xrel; //x relativa del trigger	 
	int yrel; //yrel y relativa
	int ipower; //power
	int ienergy; // energy
	int ienemy;  //id enemy stack 
	int iflags; //flags
	int ibonus; //bonus id
	int iprob; //probabilit� che il trigger venga inserito (1-100) 0 indica 100%
	int zlayer;
	SBL_TRIGGER *next; //linked list

} SBL_TRIGGER,*SBL_TRIGGER_PTR;


//linked list usata per contenere i valori dei vettori definiti nel file di definizione del livello
typedef struct SB_VALUE_LIST
{
	char *value;
	SB_VALUE_LIST *next;

} SB_VALUE_LIST,*SB_VALUE_LIST_PTR;


//struttura di chiamata a funzione
typedef struct SBL_FUNCTION
{
	int chunk_type;  //chunk type=4 
	int nargs; //numero di argomenti della funzione
	char *name; //nome della funzione chiamata
	SB_VALUE_LIST_PTR parguments; //argomenti (in formato stringa)
	SBL_FUNCTION *next; //lista concatenata

} SBL_FUNCION,*SBL_FUNCTION_PTR;

//struttura usata per caricare un intero livello
typedef struct 
{
	SBL_HEAD head;
	SBL_GROUND_LAYER_PTR pgrnd_layers;
	int inumgrnd_layers; //numero di layers del background
	SBL_BACK_LAYER_PTR pback_layers;
	int inumbacklayers;
	SBL_TRIGGER_PTR ptriggers;
	SBL_FUNCTION_PTR pfunctions; //chiamate di funzioni in questo livello
	int inumtriggers;	

} SBL_LEVEL,*SBL_LEVEL_PTR;



//ognuna di queste etichette identifica uno stack di oggetti e non un tipo di oggetto
//stack di oggetti dello stesso tipo sono identificati da etichette diverse
//costanti che identificano i tipi di oggetti 
//nemici
/*
nota: quando si aggiungono id modificare enemy_enum in sbl.h,m_label_hash in sbl.cpp e
e la funzione CreateResHanler e m_enemy_names[] in sbengine.cpp
*/
enum enemy_enum
{
	EN_UFO=1,
    EN_GOLDENFIGHTER,
	EN_ANTIAIRCRAFT,
	EN_ANTIAIRCRAFT2,
	EN_RADAR,
	EN_SNAKE,
	EN_XWING,
	EN_LAMBDAFIGHTER,
	EN_MFIGHTER,
	EN_AIRCRAFTCARRIER,
	EN_TANK,
	EN_SHIP,
	EN_UFO2,
	EN_BLASTER,
	EN_SHANTY1,
	EN_SHANTY2,
	EN_SHANTY3,
	EN_SHANTY4,
	EN_FACTORY,
	EN_PYLON,
	EN_STREET,
	EN_PALMS,
	EN_WALL,
	EN_BOSS1,
	EN_BOSS2,
	EN_BOSS3,
	EN_BOSS4,
	EN_BOSS5,
	EN_SNAKERED,
	EN_SPIDER1,
	EN_SPIDER2,
	EN_SPIDER3,
	EN_ANTIAIRCRAFT3,
	EN_GFIGHTER,
	EN_BUILDING1,
	EN_BUILDING2,
	EN_BUILDING3,
	EN_BUILDING4,
	EN_ICEBERG,
	EN_UFO3,
	EN_SCREW_FIGHTER,
	EN_NIGHT_BUILDING1,
	EN_NIGHT_BUILDING2,
	EN_NIGHT_BUILDING3,
	EN_NIGHT_BUILDING4,
	EN_NIGHT_BRIDGE,
	EN_LFIGHTER,
	EN_BLASTER2,
	EN_BLASTER3,
	EN_MODULAR1,
	EN_MODULAR2,
	EN_MODULAR3,
	EN_MODULAR4,
	EN_MODULAR5,
	EN_MODULAR6,
	EN_MODULAR7,
	EN_MODULAR8,
	EN_SNAKERED1,
	EN_SNAKERED2,
	EN_JELLYSHIP,
	EN_ASTEROID_SMALL,
	EN_ASTEROID_MEDIUM,
	EN_ASTEROID_BIG,
	EN_SHIELD_BASE,
	EN_SHELL_BASE,
	EN_TURRET,
	EN_TRAIN,
	EN_RAIL,
	EN_SPACE_BUILD1,
	EN_SPACE_BUILD2,
	EN_SPACE_BUILD3,
	EN_SPACE_BUILD4,
	EN_HTRUSS,
	EN_VTRUSS,
	EN_RING,
	EN_MAMMOTH,
	EN_LIFTER,
	EN_BLEEZER,
	EN_HFIGHTER

};

//definisce l'ultimo indice dei nemici
//va aggiornato quando si amplia la lista
#define EN_LAST EN_HFIGHTER
//primo della lista
#define EN_FIRST EN_UFO

//bonus
enum bonus_enum
{
	EB_POWERUP1=EN_LAST+1, //power up arma primaria
    EB_SPECIAL=EN_LAST+2,    //bonus red (special)
	EB_SHIELD=EN_LAST+3,     //barriera
	EB_ENERGY=EN_LAST+4,     //energia
	EB_POWERUP2=EN_LAST+5,   //power up arma secondaria
	EB_BOMBS10=EN_LAST+6,    //+10 bombe
	EB_BOMBS50=EN_LAST+7,    //+50 bombe
	EB_SCORE=EN_LAST+8,      //punti
	EB_THRUST=EN_LAST+9,     //motore
	EB_BOOSTER=EN_LAST+10,    //after burner: accelera lo scrolling
	EB_THUNDER=EN_LAST+11,    //arma speciale thunder
	EB_MISSILE=EN_LAST+12,    //arma speciale missili
	EB_LASER=EN_LAST+13,      //arma speciale laser
	EB_WEAPONA=EN_LAST+14,    //arma speciale tipo "A"
	EB_WEAPONB=EN_LAST+15,    //arma speciale tipo "B"
	EB_WEAPONC=EN_LAST+16,    //arma speciale tipo "C"
	EB_WEAPOND=EN_LAST+17,    //arma speciale tipo "D"
	EB_GYROS=EN_LAST+18,      //fuoco amico
	EB_1UP=EN_LAST+19,        //vita
	EB_GRND_FIRE=EN_LAST+20, //fuoco verso terra
	EB_TRIPLE_FIRE=EN_LAST+21, //fuoco triplo
	EB_STRIGHT_MISSILE=EN_LAST+22, //missile
	EB_ELECTRIC_BARRIER=EN_LAST+23, //barriera elettrica
	EB_BLUE_SHELL=EN_LAST+24,      //proiettile laser blu
	EB_FWSHELLS=EN_LAST+25,        //proiettili inseguitori
	EB_LASER3=EN_LAST+26           //3 p. laser uno avanti uno indietro e due verticali in alto e in basso
	
};

#define MAX_ERR 1024

//////////////////////////////////// FUNZIONI ///////////////////////////////////////////////

//compila un livello sbl a partire da un file di definizione
int CompileLevel(char *szInputFile,char *szOutPutFile);
void SetSbLastError(char *szFormat, ...); //imposta il messaggio di errore
int LoadLevelFromFile(char *szFileName,SBL_LEVEL_PTR pLevel);
void FreeSBL(SBL_LEVEL *psbl); //resetta e rilascia la memoria allocata per una struttura SBL_LEVEL
//funzione usata da Compile Level per acquisire una lista di valori
int GetValueList(FILE *f,SB_VALUE_LIST_PTR list);
//restituisce l'ultimo errore ch si � verificato
char *GetSbLastError(void); 
int TranslateId(char *value);
char * TranslateLabel(int enemy_id);

#endif