//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  snake.cpp -- tentacolo meccanico
///////////////////////////////////////////////////////////////////////*/

#ifndef _SNAKE_INCLUDE_
#define _SNAKE_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "mcasmbl.h"

class CSnake2:public CEnemy
{
private:
	
	CSBObject *m_pBody;
	CSBObject m_Head;
	CSBObject *m_pobj; //general purpose
	VERTEX2D m_vHeadCannons[2]; //hot spot dei cannoni sulla testa
	VERTEX2D m_vBodyHotSpots[2]; //giunti di collegamento fra i vari elementi del serpente
	VERTEX2D m_vHeadHotSpot; //giunto di collegamento della testa
	CADXSprite *m_ps; //general purpose
	int m_iNumItems; //numero di elementi esclusa la testa
	BOOL m_bBodyHotSpots; //flag che indica che gli hot spot del corpo sono impostati	
	BOOL m_bHeadHotSpot;  //flag hot spot head
	BOOL m_bIsClosing; //sta uscendo di scena
	
	int *m_iangle_vel; //vettore path con gli angoli della vlocit�
	int m_irepeat; //numero di volte che un elemento del path va ripetuto
	int m_ivelocity;


protected:


public:

	CADXFastMath *m_pmath;
	CSnake2(void);
	~CSnake2(void);
	//numitems=numero di elementi nel corpo,pimghead e pimgbody puntano ai vettori di frames della testa e del corpo, delta_angle=step di rotazione di ogni elemento della testa e del corpo
	HRESULT Create(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,int iflags=0,int delta_angle=15);
	void SetCannonsHotSpot(int x1,int y1,int x2,int y2);
	BOOL Update(void);
	void Reset(void);
	void Explode(void);
	HRESULT Draw(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	void SetBodyHotSpots(int x1,int y1,int x2,int y2);	
	void SetHeadHotSpot(int x1,int y1);
	void SetEnergy(LONG energy);
	void SetPosition(const LONG x,const LONG y);
	void SetVelocity(int ivel);
};

#endif