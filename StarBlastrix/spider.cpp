//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  spider.cpp - ragno
///////////////////////////////////////////////////////////////////////*/

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "a32util.h"
#include "a32audio.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"
#include "spider.h"
#include "sbengine.h"
#include "tent.h"
#include "snakeres.h"
#include "antia3.h"

#ifdef _DEBUG_
#include <crtdbg.h>
#endif

extern CObjectStack g_objStack;
extern CADXFastMath g_fmath;
extern CADXFrameManager g_fm;
extern CADXSound g_cs;
extern CObjectStack g_sSmokeSrc;
extern LONG g_snBleep1;

BOOL g_bSpiderLoaded=FALSE;
IMAGE_FRAME g_imgLegFwd[10]; //zampa anteriore
IMAGE_FRAME g_imgLegBack[10]; //zampa posteriore
IMAGE_FRAME g_imgSpiderBody[2]; //corpo
BOUND_POLY g_bpLegFwd[10];  //poligoni di contenimento zampa in avanti
BOUND_POLY g_bpLegBack[10]; //poligoni di contenimento zampa indietro
BOUND_POLY g_bpBody[2]; //poligoni di contenimento del corpo
//giunti zampa anteriore
int g_iFwdHotSpot[20]={7,68,7,65,7,57,7,42,7,29,7,38,7,42,7,51,7,54,7,54};
//giunti zampa posteriore
int g_iBackHotSpot[20]; 

LONG g_snSpiderArm=0;

HRESULT LoadSpiderResources(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg=NULL;
	CADXFrameManager *pfm=NULL;
	BOUND_POLY_PTR pb;

	if (g_bSpiderLoaded) return S_OK; //risorse gi� caricate

	if (!prm->m_lpFm) return E_FAIL; //l'oggetto non � nello stato adatto

	//se il file di risorse necessario a questo oggetto � gi� in memoria
	//evita di ricaricalo
	//le zampe e il corpo del ragno si trovano in data3.vpx immagine con indice 1
	if (!(1==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),1))) return E_FAIL;
	}

	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	//carica la zampa anteriore
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[0],0,25,54,133,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[1],59,28,134,133,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[2],139,36,243,133,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[3],253,51,374,138,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[4],380,64,512,143,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[5],517,55,638,143,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[6],641,51,754,143,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[7],0,177,99,279,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[8],103,173,191,278,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgLegFwd[9],197,173,277,278,pimg))) return E_FAIL;

	//poligoni di contenimento
	//poligono per la frame 0
	g_bpLegFwd[0].poly=new VERTEX2D[6];
	g_bpLegFwd[0].inum=0; 
	pb=&g_bpLegFwd[0];
	AddVertex(pb,0,66);AddVertex(pb,7,3);AddVertex(pb,29,7);AddVertex(pb,51,72);AddVertex(pb,25,106);AddVertex(pb,0,106);
	g_bpLegFwd[1].poly=new VERTEX2D[6];
	g_bpLegFwd[1].inum=0;
	pb=&g_bpLegFwd[1]; 
	AddVertex(pb,0,66);AddVertex(pb,19,3);AddVertex(pb,33,2);AddVertex(pb,73,72);AddVertex(pb,73,104);AddVertex(pb,0,164);
	g_bpLegFwd[2].poly=new VERTEX2D[7];
	g_bpLegFwd[2].inum=0; 
	pb=&g_bpLegFwd[2];
	AddVertex(pb,0,58);AddVertex(pb,0,54);AddVertex(pb,40,2);AddVertex(pb,56,2);AddVertex(pb,103,66);AddVertex(pb,103,96);AddVertex(pb,0,96);
	g_bpLegFwd[3].poly=new VERTEX2D[6];
	g_bpLegFwd[3].inum=0; 
	pb=&g_bpLegFwd[3];
	AddVertex(pb,0,37);AddVertex(pb,47,0);AddVertex(pb,63,0);AddVertex(pb,119,55);AddVertex(pb,120,85);AddVertex(pb,0,85);
    g_bpLegFwd[4].poly=new VERTEX2D[7];
	g_bpLegFwd[4].inum=0;
	pb=&g_bpLegFwd[4];
	AddVertex(pb,0,23);AddVertex(pb,53,0);AddVertex(pb,69,0);AddVertex(pb,116,30);AddVertex(pb,131,48);AddVertex(pb,131,77);AddVertex(pb,0,41);
	g_bpLegFwd[5].poly=new VERTEX2D[6];
	g_bpLegFwd[5].inum=0;
	pb=&g_bpLegFwd[5];
	AddVertex(pb,0,34);AddVertex(pb,51,0);AddVertex(pb,65,0);AddVertex(pb,119,57);AddVertex(pb,118,86);AddVertex(pb,0,45);
	g_bpLegFwd[6].poly=new VERTEX2D[6];
	g_bpLegFwd[6].inum=0;
	pb=&g_bpLegFwd[6];
	AddVertex(pb,0,38);AddVertex(pb,47,0);AddVertex(pb,65,1);AddVertex(pb,113,61);AddVertex(pb,112,90);AddVertex(pb,0,90);
	g_bpLegFwd[7].poly=new VERTEX2D[7];
	g_bpLegFwd[7].inum=0;
	pb=&g_bpLegFwd[7];
	AddVertex(pb,0,48);AddVertex(pb,41,0);AddVertex(pb,57,0);AddVertex(pb,97,69);AddVertex(pb,97,89);AddVertex(pb,89,101);AddVertex(pb,1,58);
	g_bpLegFwd[8].poly=new VERTEX2D[6];
	g_bpLegFwd[8].inum=0;
	pb=&g_bpLegFwd[8];
	AddVertex(pb,0,50);AddVertex(pb,37,0);AddVertex(pb,53,1);AddVertex(pb,86,69);AddVertex(pb,86,103);AddVertex(pb,0,64);
	g_bpLegFwd[9].poly=new VERTEX2D[6];
	g_bpLegFwd[9].inum=0;
	pb=&g_bpLegFwd[9];
	AddVertex(pb,0,50);AddVertex(pb,37,0);AddVertex(pb,51,0);AddVertex(pb,80,75);AddVertex(pb,74,95);AddVertex(pb,0,64);

	//body frame
	g_bpBody[0].poly=new VERTEX2D[8];
	g_bpBody[0].inum=0;
	pb=&g_bpBody[0];
	AddVertex(pb,0,9);AddVertex(pb,42,9);AddVertex(pb,52,0);AddVertex(pb,105,0);AddVertex(pb,114,9);AddVertex(pb,147,9);AddVertex(pb,147,42);AddVertex(pb,0,42);
	//body frame danneggiata
	g_bpBody[1].poly=new VERTEX2D[9];
	g_bpBody[1].inum=0;
	pb=&g_bpBody[1];
	AddVertex(pb,0,43);AddVertex(pb,43,29);AddVertex(pb,46,22);AddVertex(pb,96,7);AddVertex(pb,108,12);AddVertex(pb,140,3);
	AddVertex(pb,147,20);AddVertex(pb,83,54);AddVertex(pb,5,69);

	//crea la zampa posteriore usando le frame di quella anteriore
	for (int count=0;count<10;count++)
	{
		if (FAILED(pfm->CreateMirrorFrame(&g_imgLegBack[count],&g_imgLegFwd[count]))) return E_FAIL;
		//if (FAILED(pfm->DuplicateImgFrame(&g_imgLegBack[count],&g_imgLegFwd[count]))) return E_FAIL;
        //crea l'hot spot per la zampa posteriore
		//x
		g_iBackHotSpot[count*2]=g_imgLegBack[count].width-g_iFwdHotSpot[count*2];
		//y
		g_iBackHotSpot[count*2+1]=g_iFwdHotSpot[count*2+1];	
		//crea i poligoni della zampa indietro 
		MirrorPolyH(&g_bpLegBack[count],&g_bpLegFwd[count],&g_imgLegFwd[count]);

	}

	//body
	if (FAILED(pfm->GrabFrame(&g_imgSpiderBody[0],0,298,148,353,pimg))) return E_FAIL;   //corpo del ragno
	if (FAILED(pfm->GrabFrame(&g_imgSpiderBody[1],161,298,310,378,pimg))) return E_FAIL; //corpo del ragno danneggiato

	//suono
	g_snSpiderArm=g_cs.CreateSound("WAVS\\robotarm.wav",2);

	g_bSpiderLoaded=TRUE;

	return S_OK;

}

//scarica le risorse
HRESULT FreeSpiderResources(CADXFrameManager *pfm)
{
	if (!g_bSpiderLoaded) return S_OK; //non ci sono risorse da scaricare	

	for (int count=0;count<10;count++)
	{
		SAFE_DELETE_ARRAY(g_bpLegFwd[count].poly);
		SAFE_DELETE_ARRAY(g_bpLegBack[count].poly);
	}

	SAFE_DELETE_ARRAY(g_bpBody[0].poly);
	SAFE_DELETE_ARRAY(g_bpBody[1].poly);

	pfm->FreeFrameArray(g_imgLegFwd,10);
	pfm->FreeFrameArray(g_imgLegBack,10);	
	pfm->FreeFrameArray(g_imgSpiderBody,2);		
	
	g_bSpiderLoaded=FALSE;

	return S_OK;
}


void FreeSpiderStack(CObjectStack *pstk)
{
	if (!pstk) return;
//	CSpider *p;

/*	for (DWORD count=0;count<pstk->ItemCount();count++)
	{
		p=(CSpider *)pstk->m_objStack[count];
		if (p) 
		{
			p->~CSpider(); //chiama il distruttore della classe spider
			SAFE_DELETE(p->m_pTentacle);
		}
		
	}*/

	pstk->Clear(TRUE);

}

HRESULT CreateSpiderStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{

	CSpider* ps=NULL;
	CMechComponent *pmc,*pmc1;
	int j1,j2;

	if (!pstk) return E_FAIL;

	if (FAILED(LoadSpiderResources(prm))) return E_FAIL;

	if (FAILED(pstk->Create(inumitems))) return E_FAIL;

    for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CSpider;

		ps=(CSpider *)pstk->m_objStack[count];
		ps->bActive=FALSE;
		ps->SetFrameManager(prm->m_lpFm);
		ps->SetUpdateFreq(10);
		ps->SetMathModule(&g_fmath);
		ps->SetEnergy(80);
		ps->SetSmokeStack(&g_sSmokeSrc);
		ps->lpfnExplMulti=PushExplMulti;

		switch (iflags)
		{
		case 1:
			ps->dwResId=EN_SPIDER1;
			break;
		case 2:
			ps->dwResId=EN_SPIDER2;
			break;
		default:
			ps->dwResId=EN_SPIDER3;
			break;
		}

		ps->dwPower=10;
		ps->lpfnExplode=PushExpl8;
		ps->pfnCollide=PushCollideMetal;
		ps->SetActiveStack(&g_objStack);
		if (FAILED(ps->Create(g_imgSpiderBody,g_imgLegFwd,g_iFwdHotSpot,g_imgLegBack,g_iBackHotSpot,10,2))) return E_FAIL;

		//aggiunge le zampe al ragno
		//imposta la posizione dei giunti 
		//zampa anteriore frontale
		ps->AddLegCouplings(133,13,(LEG_TYPE)(FORWARD | FRONT),0); 
		//zampa anteriore in secondo piano
		ps->AddLegCouplings(133,13,(LEG_TYPE)(FORWARD | BACK),5);
		//zampe posteriori
		ps->AddLegCouplings(13,13,(LEG_TYPE)(BACKWARD | FRONT),5);
		ps->AddLegCouplings(13,13,(LEG_TYPE)(BACKWARD | BACK),0);

			//a seconda dei flags aggiunge altri elementi
		switch(iflags)
		{

         //ragno con tentacolo
		case 1:

			ps->dwScore=4500; //quanti punti vale
			pmc=&ps->m_BodyFrame;		
			ps->m_pTentacle=new CTentacle;
			//aggiunge il tentacolo
			if (FAILED(CreateBlueTentacle(ps->m_pTentacle,prm))) return E_FAIL;
			pmc->AddRotCoupling(74,24,0,360,1); //aggiunge una coppia rotoidale per il tentacolo
			//acquisisce l'elemento radice del tentacolo
			pmc1=ps->m_pTentacle->GetRootComponent();
			//collega il tentacolo alla frame
			pmc1->LinkToComponent(0,4,pmc);
			pmc1=NULL;

			break;

			//ragno con coppia di bracci meccanici
		case 2:

			ps->dwScore=9000;
			if (FAILED(LoadMechArmResources(prm))) return E_FAIL;
			ps->m_pArm[0]=new CMechArm;
			ps->m_pArm[1]=new CMechArm;
			pmc=&ps->m_BodyFrame;
			//crea i giunti rotoidali per attaccare i bracci meccanici
			j1=pmc->AddRotCoupling(13,13,0,360,1); 
			j2=pmc->AddRotCoupling(133,13,0,360,1);
		
			//aggiunge il primo braccio
			if (!FAILED(CreateMechArm(ps->m_pArm[0],&g_fm,0)))
			{
				pmc1=&ps->m_pArm[0]->m_Crank1;
				pmc1->LinkToComponent(0,j1,pmc);
				ps->m_pArm[0]->SetActiveStack(&g_objStack);
				ps->m_pArm[0]->SetAutoConfig(FALSE);
			}

			//aggiunge il secondo braccio
			if (!FAILED(CreateMechArm(ps->m_pArm[1],&g_fm,0)))
			{
				pmc1=&ps->m_pArm[1]->m_Crank1;
				pmc1->LinkToComponent(0,j2,pmc);
				ps->m_pArm[1]->SetActiveStack(&g_objStack);
			}			
		
			pmc1=NULL;			
			break;
			
			//torretta 
		case 3:

			ps->dwScore=8000;	
			ps->m_pAA3=new CAntiAircraft3();
			ps->m_pAA3->SetFrameManager(&g_fm);
			if (FAILED(CreateAA3(ps->m_pAA3,prm))) return E_FAIL;
			pmc=&ps->m_BodyFrame;
			ps->m_pAA3->LinkToVehicle(pmc,pmc->AddFixCoupling(74,24)); //aggiunge una coppia rotoidale per il tentacolo
			ps->m_pAA3->SetActiveStack(&g_objStack);
			pmc=NULL;

			break;
		}
		
	}	

	return S_OK;
}

//------------------------------------ CSpider ----------------------------------------

CSpider::CSpider(void)
{	
	m_bLegCouplings=FALSE;
	m_iNumLegs=0;
	m_pmath=NULL;
	dwClass=CL_ENEMY;	
	m_pTentacle=NULL;
	m_pAA3=NULL;
	m_pArm[0]=m_pArm[1]=NULL;
	m_pSmoke=NULL;
	lpfnExplMulti=NULL;
}

CSpider::~CSpider(void)
{
	CMechArm *pm;
	m_BodyFrame.Release();
	if (m_pTentacle)
	{
	//	m_pTentacle->~CTentacle();
		SAFE_DELETE(m_pTentacle);
	}

	if (m_pAA3)
	{
		//m_pAA3->~CAntiAircraft3();
		SAFE_DELETE(m_pAA3);
	}

	pm=m_pArm[0];
	if (pm)
	{
		pm->m_Crank1.Release();
		pm->m_Crank2.Release();
		pm->m_Crank3.Release();
		pm->m_Piston.Release();
		pm->m_Cannon.Release();
		SAFE_DELETE(m_pArm[0]);
	}

	pm=m_pArm[1];
	if (pm)
	{
		pm->m_Crank1.Release();
		pm->m_Crank2.Release();
		pm->m_Crank3.Release();
		pm->m_Piston.Release();
		pm->m_Cannon.Release();
		SAFE_DELETE(m_pArm[1]);
	}
	
}


void CSpider::Reset(void)
{
	CEnemy::Reset();
	this->SetVelocity(180,1);
	m_BodyFrame.SetCurFrame(0);	
	m_BodyFrame.SetVelocity(0,0); //fermo inizialmente
	SetMovement(0,0);
	m_sprLegFwd.SetCurFrame(5);

	if (m_pTentacle)
	{
		//nota bene: il tentacolo possiede il codice di AI al suo interno e qundi non � manovrato dal CBoss1
		m_pTentacle->Reset();
		m_pTentacle->SetActiveStack(m_ActiveStack);
		m_pTentacle->SetEnergy(20);
		
	//	m_ActiveStack->Push((CSBObject *)m_pTentacle);//immette il tentacolo nello stack attivo
	}

	if (m_pAA3)
	{
		m_pAA3->SetEnergy(40);	
	}
}


HRESULT CSpider::Create(IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimgLegFwd,int *piFwdHotSpot,IMAGE_FRAME_PTR pimgLegBack,int *piBackHotSpot,int iLegFrames,int iBodyFrames)
{
	if (!pimgbody || !pimgLegFwd || !pimgLegBack || iLegFrames<=0 || iBodyFrames<=0) return E_FAIL;
	if (dwStatus) return E_FAIL;
	//crea il componente base
	m_BodyFrame.SetFrameManager(m_lpFm);
	m_sprLegFwd.SetFrameManager(m_lpFm);
	m_sprLegBack.SetFrameManager(m_lpFm);
	if (!m_BodyFrame.CreateComponent(0,0,pimgbody,iBodyFrames)) return E_FAIL;
	
	for (int count=0;count<iLegFrames;count++)
	{
		m_sprLegFwd.AddFrame(&pimgLegFwd[count]);
		m_sprLegFwd.SetFrameHotSpot(count,0,piFwdHotSpot[count*2],piFwdHotSpot[count*2+1]);
		m_sprLegBack.AddFrame(&pimgLegBack[count]);
		m_sprLegBack.SetFrameHotSpot(count,0,piBackHotSpot[count*2],piBackHotSpot[count*2+1]);    	

	}

	m_sprLegFwd.SetAnimSpeed(1.0f);
	m_sprLegBack.SetAnimSpeed(-1.0f);
	m_BodyFrame.SetAnimSpeed(0.0f); 
	
	dwStatus=1;

	return S_OK;
}

void CSpider::SetSmokeStack(CObjectStack *pStack)
{
	m_pSmoke=pStack;
}

HRESULT CSpider::AddLegCouplings(int xrel,int yrel,LEG_TYPE type,int start_frame)
{
	if (m_iNumLegs>=SPIDER_MAX_LEGS) return E_FAIL;
	if (!dwStatus) return E_FAIL;
	m_bLegCouplings=TRUE;

	m_legs[m_iNumLegs].type=type;
	
	m_legs[m_iNumLegs].xrel=xrel;
	
	m_legs[m_iNumLegs].yrel=yrel;

	m_pspr=&m_legs[m_iNumLegs].spr;

	if (type & FORWARD)	memcpy(m_pspr,&m_sprLegFwd,sizeof(CADXSprite));
	
	else memcpy(m_pspr,&m_sprLegBack,sizeof(CADXSprite));

	m_pspr->SetCurFrame(start_frame);
	
	m_pspr->SetAnimSpeed(0.0f);

	//gli accoppiamenti vengono usati come fissi in quanto gli oggetti da collegare sono di tipo CADXSprite e non CMechComponent
	m_legs[m_iNumLegs].coupling_id=m_BodyFrame.AddFixCoupling(xrel,yrel);
	m_iNumLegs++; //incrementa il numero delle zampe

	return S_OK;

}

void CSpider::Explode(void)
{
	if (lpfnExplode) 
	{
		lpfnExplode(m_BodyFrame.x+20,m_BodyFrame.y);
		lpfnExplode(m_BodyFrame.x+50,m_BodyFrame.y);
		
		if (lpfnExplMulti) lpfnExplMulti(&m_BodyFrame,8);

		if (m_pArm[0]) m_pArm[0]->Explode();
		if (m_pArm[1]) m_pArm[1]->Explode();
		if (m_pTentacle) m_pTentacle->Explode();


		//visualizza una colonna di fumo
		if (m_pSmoke)
		{
		   
		   CDebris *pd=(CDebris *)m_pSmoke->Pop();
		   if (pd)
		   {
			   //coordinate assolute della bocca del cannone					  
			   pd->Reset();
			   pd->SetG(-0.3f);
			   pd->SetPosition(m_BodyFrame.x+35,m_BodyFrame.y);
			   pd->SetMaxFrames(4); //termina alla frame 4
			   pd->SetVelocity(this->GetAngle(),speed);					  
			   m_ActiveStack->Push(pd);
		   }
    		   
		}

	}

};

BOOL CSpider::DoCollisionWith(CSBObject *pobj)
{
	if (m_BodyFrame.DetectCollision((CADXSprite *)pobj))
	{
		if (pfnCollide) pfnCollide(x,y); //rumore dell'impatto fra i due oggetti
		Damage(pobj);//danneggia l'oggetto che ha impattato la frame		

	
		if (dwEnergy<20) //10
		{
			Explode();
			SetMovement(0,0); //resta fermo
			dwStatus=2;
			m_BodyFrame.SetCurFrame(1);
					
		}

		return TRUE;
	}

	//collisione con le zampe
	for (int count=0;count<m_iNumLegs;count++)
	{
		if (m_legs[count].spr.DetectCollision((CADXSprite *)pobj))
		{
			Damage(pobj,0.3f);
			return TRUE;
		}
	}


	if (m_pTentacle)
	{
		if (m_pTentacle->DoCollisionWith(pobj))
		{
			return TRUE;

		}
	}

	if (m_pAA3)
	{
		if (m_pAA3->DoCollisionWith(pobj))
		{
			return TRUE;
		}
	}

	if (m_pArm[0])
	{
		if (m_pArm[0]->DoCollisionWith(pobj)) 
		{
			//Damage(pobj);
			return TRUE;
		}
	}


	if (m_pArm[1])
	{
		if (m_pArm[1]->DoCollisionWith(pobj)) 
		{
			//Damage(pobj);
			return TRUE;
		}
	}

	return FALSE;
}


HRESULT CSpider::Draw(void)
{
	int count;
	SPIDER_LEG_TYPE_PTR pl;

	for (count=0;count<m_iNumLegs;count++)
	{
		pl=&m_legs[count];

		//esegue il renderening delle zampe in secondo piano
		if (pl->type & BACK)
		{
			m_pspr=&pl->spr;
			m_pspr->SetDisplay(m_pimgOut);
			m_pspr->RenderToDisplay(); //xabs e yabs sono state calcolate dalla funzione Update

		}
	}

	if (m_pAA3)
	{
		m_pAA3->SetDisplay(m_pimgOut);
		m_pAA3->Draw();
	}

	//esegue il rendering del corpo
	m_BodyFrame.SetDisplay(m_pimgOut);
	m_BodyFrame.RenderToDisplay();

	//esegue il rendering del tentacolo se presente
	if (m_pTentacle)
	{
		m_pTentacle->SetDisplay(m_pimgOut);
		m_pTentacle->Draw();
	}	

	if (m_pArm[0])
	{
		m_pArm[0]->SetDisplay(m_pimgOut);
		m_pArm[0]->Draw();
	}

	if (m_pArm[1])
	{
		m_pArm[1]->SetDisplay(m_pimgOut);
		m_pArm[1]->Draw();
	}

	//esegue il rendering delle zampe in primo piano
	for (count=0;count<m_iNumLegs;count++)
	{
		pl=&m_legs[count];

		//esegue il renderening delle zampe in primo piano
		if (pl->type & FRONT)
		{
			m_pspr=&pl->spr;
			m_pspr->SetDisplay(m_pimgOut);
			m_pspr->RenderToDisplay(); //xabs e yabs sono state calcolate dalla funzione Update
		}
	}	

	return S_OK;
}


void CSpider::SetMovement(int iDirection,int ispeed) //imposta la direzione (0=fermo 1=avanti -1=indietro)
{
	float fanim;
	int idir;	

	if (iDirection==0)
	{
		//fermo
		fanim=0.0f;
		idir=180;
		ispeed=CSbLevel::g_scroll_speed;
	}

	else if (iDirection<0)
	{
		fanim=-1.0f;
		idir=180;
		ispeed=abs(ispeed);
	}

	else if (iDirection>0)
	{
		fanim=1.0f;
		idir=0;
		ispeed=abs(ispeed);
	}

	for (int count=0;count<m_iNumLegs;count++)
	{
		if (m_legs[count].type & FRONT) m_legs[count].spr.SetAnimSpeed(fanim);		
		else m_legs[count].spr.SetAnimSpeed(-fanim);
	}

	SetVelocity(idir,ispeed);
}

BOOL CSpider::Update(void)
{
	SPIDER_LEG_TYPE_PTR pl;
	int qx;
	int cf;

	if (++iStepUpdate>=iUpdateFreq)
	{	
		if (dwStatus<2)
		{
			iStepUpdate=0;		
			
			for (int count=0;count < m_iNumLegs;count++)
			{
				m_pspr=&m_legs[count].spr;
				//movimento zampe
				m_pspr->UpdateFrame();
		
				cf=m_pspr->GetCurFrame();

				if (cf==0 && !m_legs[count].bStartCycle)
				{
					g_cs.PlaySound(g_snSpiderArm,0,0);
					//flag che serve a non far ripetere il rumore ogni volta.
					//deve fare rumore solo una volta alla fine del ciclo di movimento
					m_legs[count].bStartCycle=TRUE;
				}
				else if (cf!=0) m_legs[count].bStartCycle=FALSE;			

			}			
					
			iStepConfig++;

			if (iStepConfig==15)
			{
				SetMovement(1,1);						
			}
			else if (iStepConfig==40)
			{
				SetMovement(-1,2);
			}
			else if(iStepConfig==54)
			{
				SetMovement(0,0);
			}
			else if (iStepConfig>54 && iStepConfig<350)
			{
				int rs=0;

				if (m_pmath)
				{
					rs=m_pmath->RndFast(10);
				}

				if (rs==4)
				{
					//fa un movimento csuale
					rs=m_pmath->RndFast(2)-1;
					SetMovement(rs,2);
				}

				else
				{
					//insegue il player

					if (lTargetX>x+100)
					{
						SetMovement(1,2);
					}
					else if (lTargetX<x-150)
					{
 						SetMovement(-1,2);
					}
					else
					{
						SetMovement(0,0);
					}

				}
			}
			else if (iStepConfig==350)
			{
				//chiude il ciclo
				SetMovement(-1,2);		
			}


			//aggiorna i bracci meccanici se presenti
			if (m_pArm[0])
			{			
				CMechArm *pm=m_pArm[0];
				if (pm->IsConfigCompleted())
				{
					qx=lTargetX-x-13;
					if (qx>200)
					{
						pm->SetTargetConfig(180,90,45,-20);
					}
					else if (qx>=80 && qx<=200)
					{
						pm->SetTargetConfig(240,90,35,0);
					}
					else if (qx>-80 && qx<80)
					{
						pm->SetTargetConfig(90,0,0,20);
					}
					else
					{
						pm->SetTargetConfig(335,30,60,0);
					}
				}

			}


		}//fine if dwStatus
	}

	int ifr=m_BodyFrame.GetCurFrame();

	if (m_pTentacle) 
	{
		m_pTentacle->lTargetX=this->lTargetX;
		m_pTentacle->lTargetY=this->lTargetY;
		m_pTentacle->Update();
	}

	if (m_pAA3)
	{
		m_pAA3->lTargetX=this->lTargetX;
		m_pAA3->lTargetY=this->lTargetY;
		m_pAA3->Update();
	}

	if (m_pArm[0])
	{
		CMechArm *pm=m_pArm[0];
		pm->lTargetX=this->lTargetX;
		pm->lTargetY=this->lTargetY;
		pm->Update();		
	}

	if (m_pArm[1])
	{
		CMechArm *pm1=m_pArm[1];
		pm1->lTargetX=this->lTargetX;
		pm1->lTargetY=this->lTargetY;
		pm1->Update();		
	}

	//!!!
	//aggiorna la posizione dell'oggetto
	this->UpdatePosition();
	//copia la posizione nella frame del corpo
	m_BodyFrame.SetPosition(this->x,this->y);
	//aggiorna la frame del corpo 
	m_BodyFrame.UpdateFrame();
	m_BodyFrame.UpdatePosition();


	//aggiorna le coordinate assolute per il blitting delle zampe
	for (int count=0;count<m_iNumLegs;count++)
	{
		pl=&m_legs[count];
		m_pspr=&pl->spr;		
		m_pspr->SetJointPosition(this->x+pl->xrel,this->y+pl->yrel,0);	
	}

	if (dwEnergy<=0) 
	{
		lpfnExplode(m_BodyFrame.x+60,m_BodyFrame.y-10);
		lpfnExplode(m_BodyFrame.x+10,m_BodyFrame.y+10);
		CreateBonus();
		return FALSE;
	}

	return (m_BodyFrame.x>-350);
}


HRESULT CSpider::SetMathModule(CADXFastMath *pmath)
{
	if (!pmath) return E_FAIL;
	if (!pmath->IsInitialized()) return E_FAIL;
	m_pmath=pmath;
	return S_OK;
}