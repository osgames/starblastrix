//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  jltent.h -- tentacolo estensibile 
///////////////////////////////////////////////////////////////////////*/


#ifndef _JLTENT_INCLUDE_
#define _JLTENT_INCLUDE_

#include "enemy.h"
#include "resman.h"

enum JlCommands
{
	jlNop=0,      //"no operation"
	jlStop=1,     //sta fermo (p1=numero di cicli di aggiornamento in cui deve stare fermo)
	jlSwing=2,    //pendolo (p1=numero oscillazioni ,p2=delta angolo in avanti p3=delta angolo indietro
	jlGotoDef=3,  //va alla posizione di default
	jlTarget=4,   //va alla posizione x,y (p1=x p2=y)
	jlRotate=5,   //ruota (p1=numero di rotazioni,p2=incremento angolare (es. +1 -1)
	jlFollowPl=6,  //insegue il player (p1=ogni quanti cicli di esecuzione aggiorna il target,p2=totale aggiornamenti prima di finire l'istruzione)
	jlRndOp=7, //random instruction : esegue una istruzione random
	jlRndSwing=8, //random swing
	jlFlash=9, //flash avanti e indietro (p1=numero di corse, p2=periodo di aggiornamento)
	jlFlashAll=10, //flash tutti gli elementi
	jlFlashOff, //disattiva il flash
	jlSaw //avvia la sega rotante (p1=numero di cicli)
};

//singola istruzione
typedef struct JL_COMMAND_TAG
{
	JlCommands opcode; //codice istruzione
	LONG p1;    //argomenti dell'istruzione (possono essere delle coordinate)
	LONG p2;
	LONG p3; 
	int config_freq; //frequenza di aggiornamento algoritmo corrente
	JL_COMMAND_TAG* next;

} JL_COMMAND,*JL_COMMAND_PTR;

class CJlTentacle:public CEnemy
{
private:

	CADXSprite **m_Items;	//vettore di oggetti sprite
	int *m_piEnergy;        //vettore che contiene il valore energia di ogni elemento
	CADXSprite *m_pHead;	//punta la testa del tentacolo
	CADXSprite *m_pParent;	//sprite genitore
	CADXSprite *m_Temp;		//general purpose
	CADXSprite *m_pTemp1;	//general purpose
	
	int m_item_dia;			//diametro elemento del corpo
	int m_head_dia;			//diametro testa
	int m_inumitems;		//elementi totali compreso la testa del tentacolo
	int m_iparent_hot_spot; //hot spot della frame dello sprite genitore

	LONG m_xinit,m_yinit;	//posizione iniziale 	
	LONG m_cx,m_cy;			//coordinate cartesiane correnti
	int m_cr,m_ct;			//coordinate polari correnti
	int m_xtg,m_ytg;        //coordinate del target
	int m_zero_angle;       //angolo zero nella modalit� pendolo
	int m_rhoinit,m_thetainit;
	int m_maxrho;           //distanza massima che puo' raggiungere la testa dal giunto
	int m_swing_step;       //incremento angolo
	int m_cnt1;             //contatori general purpose
	int m_cnt2;
	int m_cntflash;
	float m_sawanim;        //velocit� animazione testa
	int m_cntsaw;
	int m_iflash_item;      //elemento correntemente illuminato
	int m_iflash_step;
	int m_iflash_freq;
	int m_iflash_mode;      //0=nessun flash 1=avanti e indietro 2=tutti gli elementi
	int m_iflash_inc;       //1 avanti 2=indietro
	
	JL_COMMAND_PTR m_Ip;	//punta l'istruzione corrente
	JL_COMMAND_PTR m_Cmd;   //general purpose
	JL_COMMAND_PTR m_CmdTop; //top stack istruzioni
	JL_COMMAND m_Program;	//programma di movimento (elenco istruzioni da eseguire)
	int m_CntDown;          //conto alla rovescia cicli 

	BOOL m_bRunningCmd;      //indica che sta eseguendo una istruzione di movimento
	void SetRectPosition(int xd,int yd);
	void SetTargetPosition(int xd,int yd,int vel);
	void SetPolarPosition(int rho,int theta);	
	void UpdateBodyPosition(void);
	void GetPlayerTarget(int *xtg,int *ytg);
	void Flash(void);
	void HilightItem(int item,int inc);
	BOOL IsTooLong(void);   //rende true se si � allungato troppo

public:


	CJlTentacle(int numitems,IMAGE_FRAME_PTR pItem,int nframebody,IMAGE_FRAME_PTR pHeadItem,int nframehead,CADXFrameManager *pfm);
	~CJlTentacle(void);
	BOOL LinkTo(CADXSprite *parent,int parenthotspot);
	HRESULT Draw(void);
	BOOL Update(void);
	void Reset(void);
	//rende true se � occupato ad eseguire il programma
	BOOL IsBusy(void); 
	//resetta il programma in memoria	
	void ResetProgram(void);
	BOOL PushInstruction(JlCommands opcode,LONG p1,LONG p2,LONG p3,int config_freq=1); 
	BOOL DoCollisionWith(CSBObject *pobj);
	//punta alla funzione esplosione multipla
	void (*lpfnMultiExplosion)(CADXSprite *pspr,int num);

};


//numero di tentacoli
#define JELLY_TENT_NUM 4

class CJellyShip:public CEnemy
{
private:
	
	CJlTentacle *m_pTemp; //general purpose
	CJlTentacle *m_ptent[JELLY_TENT_NUM];	
	int m_iNumTent;
	int m_ttl;
	int m_repeat;
	int m_ai;

public:

	CJellyShip(void);
	BOOL AddJellyTentacle(CJlTentacle *tentacle,int parenthotspot);
	BOOL Update(void);
	void Reset(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	HRESULT Draw(void);
	void (*lpfnExplMulti)(CADXSprite *psrc,int num_explosions);
};

//help functions
HRESULT LoadJlTentacleRes(CResourceManager *prm);
HRESULT LoadJellyShipRes(CResourceManager *prm);
void FreeJlTentacleRes(CADXFrameManager *pfm);
void FreeJellyShipRes(CADXFrameManager *pfm);
HRESULT CreateJellyShipStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);

#endif