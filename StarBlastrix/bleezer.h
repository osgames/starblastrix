//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  bleezer.h -- 
///////////////////////////////////////////////////////////////////////*/

#ifndef _BLEEZER_
#define _BLEEZER_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "resman.h"
#include "enemy.h"
#include "sbengine.h"

HRESULT LoadBleezerRes(CResourceManager* prm);
void FreeBleezerRes(void);
HRESULT CreateBleezerStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);

#define MAX_SAT 4

class CElectro:public CEnemy
{
private:

	/*
      legge di moto dei satelliti che girano intorno a questo oggetto 
      x=Rx * cos (Kx * alpha + phi);
	  y=Ry * sin (Ky * alpha + phi);

	*/

	int m_alpha_step; //step in gradi dell'angolo
	int m_ichotspot; //hot spot del parent intorno al quale ruotano i satelliti
	float m_ralpha; //alfa in radianti
	float m_ralpha_step; //step alfa in radianti	
	float m_rphi;
	float Rx,Ry,Kx,Ky,phi;
	float m_centerx,m_centery;
	//delta per i parametri di moto
	float dRx,dRy,dKx,dKy,dAlphaStep;
	int m_targetStep;
	int m_targetEnd;
	
public:
	
	CSBObject pholder;
	void SetParams(float rx,float ry,float kx,float ky,float phi);	
	void SetTargetParams(float trx,float tr,float tkx,float tky,float talpha,int steps);
	float GetAlpha(void);
	int GetAlphaStep(void);
	void SetAlphaStep(int step);	
	void SetCentralHotSpot(int hotspot);
	float GetRx(void);
	float GetRy(void);
	float GetKx(void);
	float GetKy(void);
	CElectro(void);
	BOOL Update(void);
	void Reset(void);
	void ResetDelta(void);
	BOOL ParamConfigComplete(void);

};


class CBleezer:public CEnemy
{
private:

	int ttl;
	int burst1,burst2;
	int delay1,delay2;
	LONG m_cx,m_cy;
	//satelliti che girano intorno all'oggetto
	CElectro* m_psat[MAX_SAT];
	void FireThunder(int satellite,int thduration);
	void FireThunderToPlayer(int hotspot);
	void SetSatelliteTargetParams(float tRx,float tRy,float tKx,float tKy,float tAlphaStep,int steps);
    void FireCentral(void);
	void FireCannons(void);

public:

	CBleezer(void);
	void Reset(void);
	BOOL Update(void);
};


#endif