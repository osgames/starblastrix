/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

   Oggetti nemici linkabili tra loro, tutti ereditano da CModularEnemy

*/


#ifndef _MODULAR_ENEMIES_
#define _MODULAR_ENEMIES_

#include "modular.h"
#include "resman.h"


typedef enum mod_ememies
{
	MOD_ENEMY_ROOT=0,
	MOD_MODULE1,
	MOD_MODULE2,
	MOD_MODULE3,
	MOD_NOZZLE,
	MOD_SLIDE,
	MOD_ROT_CANNON,
	MOD_CANNON1,
	MOD_CANNON2,
	MOD_CANNON3,
	MOD_CANNON4,
	MOD_CANNON5,
	MOD_BELL,
	MOD_COCKPIT,
	MOD_HFRAME,
	MOD_VFRAME,
	MOD_TURRET,

} MOD_ENEMY_ID;

#define MOD_ENEMY_MAX MOD_TURRET+1

//restituisce un componente nemico linkabile tramite un resource manager e un ID
CModularEnemy *GetModEnemy(CResourceManager *prm,MOD_ENEMY_ID id);
//rilascia le risorse allocate
void FreeModEnemies(void);
//attacca un modulo ad un altro estraendolo dallo stack
HRESULT AttachEnemy(CResourceManager *prm,int iCoupling,int iChildCoupling,CModularEnemy *parent,MOD_ENEMY_ID child_type);


#endif