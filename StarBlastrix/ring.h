//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  ring.h -- anello nemico
///////////////////////////////////////////////////////////////////////*/

#ifndef _RING_INCLUDE_
#define _RING_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "resman.h"
#include "enemy.h"

HRESULT LoadRingRes(CResourceManager* prm);
void FreeRingRes(void);
HRESULT CreateRingStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);

class CRing:public CEnemy
{
private:

	//accelerazione animazione
	float m_anim_acc;
	void UpdateAnimSpeed(void);
	RECT rcLimitBox;
	BOOL bFollowPlayer;
	//indica che c'� un box di limite movimento attivo
	BOOL bBoxActive;
	int burst_cnt;

public:

	CRing(void);
	void Reset(void);
	BOOL Update(void);
	void Explode(void);

};

#endif