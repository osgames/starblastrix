//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  asteroid.cpp -- source file per la classe CAsteroid
///////////////////////////////////////////////////////////////////////*/


#include "asteroid.h"
#include "a32fastm.h"

#define MAX_ASTEROID_FRAMES 18

IMAGE_FRAME m_imgBig[MAX_ASTEROID_FRAMES];
IMAGE_FRAME m_imgMedium[MAX_ASTEROID_FRAMES];
IMAGE_FRAME m_imgSmall[MAX_ASTEROID_FRAMES];

extern CADXFastMath g_fmath;
extern void PushExpl2(LONG x,LONG y);
extern CObjectStack g_objStack;

static BOOL g_bAsteroidResLoaded=FALSE;

//Carica le immagini necessarie per l'asteroide
HRESULT LoadAsteroidRes(CResourceManager* prm)
{
	if (g_bAsteroidResLoaded) return S_OK;

	if (!prm) return E_FAIL;

	if (FAILED(prm->LoadAuxResFile(TEXT("data\\data5.vpx"),3))) return E_FAIL;

	IMAGE_FRAME_PTR pimg=NULL;
	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
	if (!pimg) return E_FAIL;
    //punta al frame manager
	CADXFrameManager *pfm=NULL;
	pfm=prm->m_lpFm;   
	if (!pfm) return E_FAIL;
	//big
	if (FAILED(pfm->GrabFrameArray(m_imgBig,pimg,7,0,0,120,178))) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(&m_imgBig[7],pimg,7,0,178,120,178))) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(&m_imgBig[14],pimg,4,0,178*2,120,178))) return E_FAIL;
	//medium
	if (FAILED(pfm->GrabFrameArray(m_imgMedium,pimg,12,0,560,70,80))) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(&m_imgMedium[12],pimg,6,0,560+80,70,80))) return E_FAIL;
	//small
	if (FAILED(pfm->GrabFrameArray(m_imgSmall,pimg,9,504,390,36,30))) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(&m_imgSmall[9],pimg,9,504,390+30,36,30))) return E_FAIL;
	
	g_bAsteroidResLoaded=TRUE;

	return S_OK;
}

//Rilascia le risorse se sono state allocate
void FreeAsteroidRes(CADXFrameManager *pfm)
{
	if (!pfm) return;

	if (g_bAsteroidResLoaded)
	{
		pfm->FreeFrameArray(m_imgBig,MAX_ASTEROID_FRAMES);
		pfm->FreeFrameArray(m_imgMedium,MAX_ASTEROID_FRAMES);
		pfm->FreeFrameArray(m_imgSmall,MAX_ASTEROID_FRAMES);

		g_bAsteroidResLoaded=FALSE;
	}
}

//Crea uno stack di oggetti asteroide.
//pBig e pSmall puntano a stack gi� creati che contengono gli asteroidi di taglia grande e piccola che vengono immessi
//nello stack degli oggetti attivi quando si disintegra l'asteroide corrente
//iflags: 0=crea l'asteroide piccolo, 1=crea quello medio 2=crea quello grande
HRESULT CreateAsteroidStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags,CObjectStack* pBig,CObjectStack* pSmall)
{
	if (!prm || !pstk) return E_FAIL;

	if (FAILED(LoadAsteroidRes(prm))) return E_FAIL;

	CAsteroid* p=NULL;

	CADXFrameManager* pfm=prm->m_lpFm;
    
	if (!pfm) return E_FAIL;

    pstk->Create(inumitems);

	IMAGE_FRAME_PTR pimg=NULL;

	switch (iflags)
	{
	case 0:
		pimg=m_imgSmall;
		break;
	case 1:
		pimg=m_imgMedium;
		break;
	case 2:
		pimg=m_imgBig;
		break;
	}

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CAsteroid(pBig,pSmall);
		p=(CAsteroid *)pstk->m_objStack[count];
		p->bActive=FALSE;
		p->SetFrameManager(pfm);
		p->SetUpdateFreq(40);
		p->SetInitialEnergy(4);
		p->dwPower=16;
		p->dwScore=100;
		p->lpfnExplode=PushExpl2;
		p->SetActiveStack(&g_objStack);

		for (DWORD count1=0;count1<MAX_ASTEROID_FRAMES;count1++)
		{
			p->AddFrame(&pimg[count1]);	
		}

		p->SetAnimSpeed(0.0f);
		p->SetCurFrame(0);
	}

	return S_OK;
}




CAsteroid::CAsteroid(CObjectStack* pBig,CObjectStack *pSmall)
{
	dwClass=CL_ENEMY;
	m_pBig=pBig;
	m_pSmall=pSmall;
	m_w=m_h=0;
}


void CAsteroid::Reset(void)
{
	CEnemy::Reset();

	int alpha=g_fmath.RndFast(80)-40;
	int vel=2;
	
	IMAGE_FRAME_PTR p=this->GetFramePtr(0);

	if (p)
	{
		//altezza e larghezza dell'asteroide
		m_w=p->width;
		m_h=p->height;
	}

	//i primi due bit definiscono la direzione
	switch (m_dwFlags & 0x3)
	{

	case 1:

		//va dall'alto in basso
		alpha+=90;
		break;

	case 2:

		//va dal basso in alto
		alpha+=270;
		break;

	case 3:

		//arriva alle spalle
		alpha+=0;
		break;

	default:

		//va in avanti
		alpha+=180;
		break;
	}

	//il 3� bit se settato, fa in modo che la velocit� sia random
	if ((m_dwFlags & 0xC)>>2)
	{
		vel=1+g_fmath.RndFast(3);
	}

	//imposta la velocit�
	SetVelocity(alpha,vel);

	if (g_fmath.RndFast(4)==1)
	{
		//spin random

		//imposta uno spin random
		switch(g_fmath.RndFast(2))
		{
		case 0:

			//spin positivo
			SetAnimSpeed((g_fmath.RndFast(50)+10)*0.01f);
			break;

		default:

			//spin negativo
			SetAnimSpeed(-((g_fmath.RndFast(50)+10)*0.01f));
			break;

		}

	}

	else
	{
		//spin lento
		switch(g_fmath.RndFast(4))
		{
		case 0:

			SetAnimSpeed(0.1f);
			break;

		case 1:

			SetAnimSpeed(-0.1f);
			break;

		case 2:

			SetAnimSpeed(0.2f);
			break;

		case 3:

			SetAnimSpeed(-0.2f);
			break;

		default:

			SetAnimSpeed(0.16f);
			break;

		}

	}

	SetCurFrame(0);
}

BOOL CAsteroid::DoCollisionWith(CSBObject *pobj)
{	
	
	return CSBObject::DoCollisionWith(pobj);
}

BOOL CAsteroid::Update(void)
{

	UpdateFrame();
	UpdatePosition();

	if (dwEnergy<=0)
	{
		CAsteroid *p=NULL;
		//l'asteroide si spezza in parti piu' piccole
		if (m_pBig && m_ActiveStack)
		{
			p=(CAsteroid *)m_pBig->Pop();

			if (p)
			{
				p->SetInitialEnergy(8);
				p->SetFlags(0);
				p->Reset();
				p->SetVelocity(this->GetAngle()+25,this->GetSpeed());
				p->SetPosition(this->x,this->y);
				m_ActiveStack->Push(p);	

			}

			p=(CAsteroid *)m_pBig->Pop();

			if (p)
			{
				p->SetInitialEnergy(6);
				p->SetFlags(0);
				p->Reset();
				p->SetVelocity(this->GetAngle()-25,this->GetSpeed());
				p->SetPosition(this->x+10,this->y+10);
				m_ActiveStack->Push(p);	

			}

		}

		if (m_pSmall && m_ActiveStack)
		{
			int da;
			int curan=this->GetAngle();
			int dx;
			int dy;

			for (int count=0;count<4;count++)
			{
				p=(CAsteroid *)m_pSmall->Pop();

				if (p)
				{
					dy=g_fmath.RndFast(m_h-10);
					dx=g_fmath.RndFast(m_w-10);
					da=g_fmath.RndFast(100)-50;				
					p->SetInitialEnergy(2);
					p->SetFlags(0);
					p->Reset();
					p->SetVelocity(curan+da,this->GetSpeed());
					p->SetPosition(this->x+dx,this->y+dy);
					m_ActiveStack->Push(p);	

				}

			}		

		}

		return FALSE;

	}
	return (x>-200 && x<CSBObject::g_iScreenWidth+250 && y>-200 && y<CSBObject::g_iScreenHeight+180);
}

