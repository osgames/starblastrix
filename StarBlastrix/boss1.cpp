//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  boss1.h -- boss n�1
///////////////////////////////////////////////////////////////////////*/

#include "sbengine.h"
#include "a32sprit.h"
#include "a32object.h"
#include "a32util.h"
#include "enemy.h"
#include "boss1.h"
#include "mecharm.h"
#include "tent.h"
#include "snakeres.h"

IMAGE_FRAME g_imgBoss1; //frame del boss1
BOOL g_bBoss1ResLoaded=FALSE;
BOUND_POLY m_bpFrame;
extern void PushExpl8(LONG x,LONG y);
extern void PushCollideMetal(LONG x,LONG y);
extern CADXSound g_cs;
extern CObjectStack g_objStack;
extern CADXFastMath g_fmath;

//aggiunge le risorse personalizzate allo stack di oggetti boss1 precedentemente creato in modo standard
/*
 flags:
 0=con braccio meccanico (default)
 1=con tentacolo blu

*/

HRESULT CreateBoss1Stack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	CBoss1 *pb;
	CMechComponent *pmc,*pmc1;

	if (!pstk) return E_FAIL;
	
	if (FAILED(LoadMechArmResources(prm))) return E_FAIL; //carica le risorse del braccio meccanico se non sono state gi� caricate
	
	if (FAILED(LoadBoss1Resources(prm))) return E_FAIL; //carica le risorse di Boss1 se non sono gi� in cache

	if (FAILED(pstk->Create(inumitems))) return E_FAIL;

	for (DWORD count=0;count<inumitems;count++)
	{		
		pstk->m_objStack[count]=new CBoss1;
    	pb=(CBoss1*)pstk->m_objStack[count];
		pb->bActive=FALSE;		
		pb->SetGraphicManager(prm->m_lpGm);
		pb->SetFrameManager(prm->m_lpFm);
		pb->SetUpdateFreq(40);
	//	pb->SetBoundPoly(0,&m_bpFrame); //applica il bounding polygon alla frame del boss1
		pb->SetInitialEnergy(40);
		pb->dwPower=8;			
		pb->m_pArm=new CMechArm;
		pb->lpfnExplode=PushExpl8;
		pb->pfnCollide=PushCollideMetal;
		pb->SetActiveStack(&g_objStack);
		pb->SetMathModule(&g_fmath);
        //crea il braccio meccanico
		if (FAILED(CreateMechArm(pb->m_pArm,prm->m_lpFm,0))) return E_FAIL;
		pmc=&pb->m_Frame;
		pmc->SetFrameManager(prm->m_lpFm);	
		if (!(pmc->CreateComponent(0,0,&g_imgBoss1,1))) return E_FAIL;
		pmc->SetBoundPoly(0,&m_bpFrame);	
		//coppia rotoidale sulla frame principale alla quale viene collegato il braccio meccanico
		pmc->AddRotCoupling(97,70,0,360);
		pmc1=&pb->m_pArm->m_Crank1;	
        //collega il braccio meccanico alla frame principale
		pmc1->LinkToComponent(0,0,pmc);		
		pmc1=NULL;

		//a seconda dei flags aggiunge altri elementi
		switch(iflags)
		{
		case 1:

			pb->m_pTentacle=new CTentacle;
			//aggiunge il tentacolo
			if (FAILED(CreateBlueTentacle(pb->m_pTentacle,prm))) return E_FAIL;
			pmc->AddRotCoupling(109,42,0,360,1); //aggiunge una coppia rotoidale per il tentacolo
			//acquisisce l'elemento radice del tentacolo
			pmc1=pb->m_pTentacle->GetRootComponent();
			//collega il tentacolo alla frame
			pmc1->LinkToComponent(0,1,pmc);
			pmc1=NULL;

			break;
		}

	}

	return S_OK;
}

void FreeBoss1Stack(CObjectStack *pstk)
{
	if (pstk)
	{
		CBoss1 *pb;
		CMechArm *parm;	
		DWORD dwN=pstk->ItemCount();

		for(DWORD count=0;count<dwN;count++)
		{
			pb=(CBoss1 *)pstk->m_objStack[count];
			parm=pb->m_pArm;
			pb->m_Frame.Release();			
			parm->m_Crank1.Release();
			parm->m_Crank2.Release();
			parm->m_Crank3.Release();
			parm->m_Piston.Release();
			parm->m_Cannon.Release();
			SAFE_DELETE(pb->m_pArm);
			//rilascia gli oggetti creati dal tentacolo (invoca il ditruttore)	
		/*	if (pb->m_pTentacle)
			{
				pb->m_pTentacle->~CTentacle();

			}*/
			SAFE_DELETE(pb->m_pTentacle);
		}

		pstk->Clear(TRUE);
	}
}

HRESULT LoadBoss1Resources(CResourceManager *prm)
{
	if (!prm) return E_FAIL;
	IMAGE_FRAME_PTR pimg;
	CADXFrameManager *pfm;

	if (!prm) return E_FAIL;
	if (g_bBoss1ResLoaded) return S_OK; //risorse gi� caricate

	//se il file di risorse necessario a questo oggetto � gi� in memoria
	//evita di ricaricalo
	//deve caricare il file
	if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),0))) return E_FAIL;
	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;   
	if (FAILED(pfm->GrabFrame(&g_imgBoss1,180,472,440,608,pimg))) return E_FAIL;

    //crea i poligoni per il collision detection con l'oggetto 


	m_bpFrame.poly=new  VERTEX2D[15];
	m_bpFrame.inum=0;	

	AddVertex(&m_bpFrame,0,135);
	AddVertex(&m_bpFrame,10,97);
	AddVertex(&m_bpFrame,30,89);
	AddVertex(&m_bpFrame,48,43);
	AddVertex(&m_bpFrame,56,33);
	AddVertex(&m_bpFrame,64,26);
	AddVertex(&m_bpFrame,77,22);
	AddVertex(&m_bpFrame,87,21);
	AddVertex(&m_bpFrame,111,1);
	AddVertex(&m_bpFrame,119,8);
	AddVertex(&m_bpFrame,258,5);
	AddVertex(&m_bpFrame,258,109);
	AddVertex(&m_bpFrame,121,109);
	AddVertex(&m_bpFrame,115,118);
	AddVertex(&m_bpFrame,109,115);

	g_bBoss1ResLoaded=TRUE;

	return S_OK;

}

HRESULT FreeBoss1Resources(CADXFrameManager *pfm)
{
	if (!g_bBoss1ResLoaded) return S_OK;
	if (!pfm) return E_FAIL;
	pfm->FreeImgFrame(&g_imgBoss1);
	g_bBoss1ResLoaded=FALSE;

	SAFE_DELETE_ARRAY(m_bpFrame.poly);
	m_bpFrame.inum=0;

	return E_FAIL;

}

//--------------------------------------- CBoss1 ----------------------------------------


CBoss1::CBoss1(void)
{
	dwClass=CL_ENEMY;
	dwType=23; //id del nemico
	dwScore=25000;
	m_pArm=NULL;	
	m_pmath=NULL;
	m_pTentacle=NULL;
	m_ist1=m_ist2=0;
}

HRESULT CBoss1::Draw(void)
{
	//disegna la catena cinematica
	m_Frame.SetDisplay(m_pimgOut);
    m_Frame.RenderToDisplay();

	if (m_pArm)
	{

		m_pArm->SetDisplay(m_pimgOut);
		//disegna il braccio meccanico
		m_pArm->Draw(); 

	}

	if (m_pTentacle)
	{
		m_pTentacle->SetDisplay(m_pimgOut);
		m_pTentacle->Draw();
	}

	//disegna tutta la catena cinematica
    return S_OK;	
}

void CBoss1::Reset(void)
{
	CEnemy::Reset();
	SetVelocity(180,1);
	iUpdateFreq=320;
	m_iAttackType=0; //tipo di attacco corrente
	m_ist1=m_ist2=0;
	m_bsteady=false;
	m_isteadyfreq=0;
	dwClass=CL_ENEMY;

	if (m_pArm)
	{

		m_pArm->Reset();
		m_pArm->SetRandom(FALSE);
		//braccio in posizione di riposo
		m_pArm->SetActiveStack(m_ActiveStack);  //fa conoscere al braccio lo stack attivo

	}

	if (m_pTentacle)
	{
		//nota bene: il tentacolo possiede il codice di AI al suo interno e qundi non � manovrato dal CBoss1
		m_pTentacle->Reset();
		m_pTentacle->SetActiveStack(m_ActiveStack);
	//	m_ActiveStack->Push((CSBObject *)m_pTentacle);//immette il tentacolo nello stack attivo
	}
}

BOOL CBoss1::Update(void)
{
	iStepUpdate++;

	if (m_pArm) 
	{
		//fa conoscere al braccio la posizione del player
		m_pArm->lTargetX=this->lTargetX;
		m_pArm->lTargetY=this->lTargetY;
		//aggiorna il braccio
		m_pArm->Update();
	}	

	if (m_pTentacle) 
	{
		m_pTentacle->lTargetX=this->lTargetX;
		m_pTentacle->lTargetY=this->lTargetY;
		m_pTentacle->Update();

	}

	if (iStepUpdate>=iUpdateFreq)
	{		
		iStepUpdate=0;
		m_iAttackType=0;

		if (m_bsteady)
		{
			//per un po' resta fermo: non incrementa la configurazione
			m_bsteady=false;
			iUpdateFreq=m_isteadyfreq;
			SetVelocity(0,0); //fermo

		}
		else
		{
			iStepConfig++;	

			//genera un numero casuale 
			int rn=m_pmath->RndFast(10);		
				
				switch(iStepConfig)
				{
				case 1:

					if (rn<5)
					{
						iUpdateFreq=192;
						SetVelocity(180,0);
					}
					else
					{
						iUpdateFreq=60;
						SetVelocity(160,1);
					}

					break;

				case 2:

					if (rn<2 || !m_pArm)
					{
						iUpdateFreq=150;
						SetVelocity(270,1);
					}
					else
					{
						//spara indietro
						SetTargetPoint(-15,180,1,true,80);
						m_pArm->SetTargetConfig(0,345,225,0,15,5);
						
					}

					break;			

				case 3:

					if (!m_pArm || rn<3)
					{
						FollowTarget();	
						m_iAttackType=1;
						iUpdateFreq=200;
					}
					else if (rn==6)
					{
						SetVelocity(85,2);
						if (m_pArm) m_pArm->SetTargetConfig(150,210,345,-20);
						iUpdateFreq=82;
					}
					else if (rn>5)
					{
						SetTargetPoint(210,-25,2,true,100);
						m_pArm->SetTargetConfig(270,60,120,0);
					}
					else if (rn==1)
					{
						SetVelocity(180,0); //fermo
						iUpdateFreq=120;

					}
					else
					{
						//si sposta in posizione d'attacco in alto a destra				
						m_pArm->SetTargetConfig(330,45,120,-10,15,8);
						SetTargetPoint(CSBObject::g_iScreenWidth-180,-10,2,true,100);
						
					}			
				
					break;

				case 4:

					if (!m_pArm)
					{
						SetVelocity(10,1);
						iUpdateFreq=45;
					}
					else
					{

						if (rn<=2)
						{
							//posizione d'attacco in basso a sinistra
							SetTargetPoint(-10,290,1,true,80);
							m_pArm->SetTargetConfig(30,60,210,0,15,8);
						}
						else if (rn>2 && rn<=7)
						{
							//posizione centrale
							SetTargetPoint((int)(CSBObject::g_iScreenWidth*0.5-130),(int)(CSBObject::g_iScreenHeight *0.5-75),2,true,45);
							m_pArm->SetTargetConfig(0,45,-45,-10,15,6);
						}
						else 
						{
							//spara indietro
							SetTargetPoint(-50,160,1,true,120);
							m_pArm->SetTargetConfig(0,335,225,0,15,6);
						}					

					}

					break;			

				case 5:

					if (rn<5)
					{
						//segue il bersaglio
						FollowTarget();	
						m_iAttackType=1;
						iUpdateFreq=395;


					}
					else
					{
						SetVelocity(1,0);
						iUpdateFreq=315;
					}
				
					break;

				case 6:

					//segue il bersaglio
					FollowTarget();	
					m_iAttackType=1;
					iUpdateFreq=115;					
					break;


				case 7:

					SetVelocity(0,0);
					iUpdateFreq=200;
					break;

				case 8:

					SetVelocity(90,1);
					iUpdateFreq=20;
					break;

				case 9:

					if (lTargetX<x+70) SetVelocity(180,3);
					else SetVelocity(0,3);
					iUpdateFreq=150;
					break;


				case 10:

					if (rn==6 || rn==10)
					{
						m_pArm->SetTargetConfig(15,0,0,0,15,10);

					}
					else if (rn<3)
					{
						m_pArm->SetTargetConfig(180,180,210,0,15,8);

					}

					iUpdateFreq=215;
					SetVelocity(156,1);
					break;


				case 11:

					if (lTargetY<y+40) SetVelocity(260,1);
					else SetVelocity(70,1);
					iUpdateFreq=120;
					break;

				default:

					iStepConfig=0;		
					SetVelocity(180,0);	
					break;

				} //fine switch (iStepConfig)

			
		}//fine steady

	} //fine if iStepUpdate>=iUpdateFreq

	if ((y+100)>CSBObject::g_iScreenHeight-CSBObject::g_iScreenBottomHeight) 
	{
		SetVelocity(270,1); 
	}

	if (x<-400) 
	{
		SetVelocity(0,2);
	}

	if (y<-210)
	{
		SetVelocity(90,2);
	}

	if (m_iAttackType==1)
	{
		//segue il player
		FollowTarget();
	}

	//aggiorna la posizione
	this->UpdatePosition();
	//trasmette la posizione dell'oggetto alla frame principale
	m_Frame.SetPosition(x,y);
	m_Frame.UpdatePosition();

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return TRUE;	
}

//controlla la collisione fra l'oggetto boss1 corrente e l'oggetto pobj
//se la collisione avviene allora rende TRUE altrimenti FALSE
BOOL CBoss1::DoCollisionWith(CSBObject *pobj)
{
	if (m_Frame.DetectCollision((CADXSprite *)pobj))
	{
		if (pfnCollide) 
		{
			int xd,yd;

			for (int count=0;count<8;count++)
			{
				xd=m_pmath->RndFast(120)-60;
				yd=m_pmath->RndFast(120)-60;
				pfnCollide(x+xd,y+yd); //rumore dell'impatto fra i due oggetti			
			}
		}
		Damage(pobj);		
		return TRUE;
	}

	if (m_pArm)
	{
		if (m_pArm->DoCollisionWith(pobj)) 
		{
			//Damage(pobj);
			return TRUE;
		}
	}

	if (m_pTentacle)
	{
		if (m_pTentacle->DoCollisionWith(pobj))
		{
			return TRUE;

		}
	}

	return FALSE;
}

HRESULT CBoss1::SetMathModule(CADXFastMath *pmath)
{
	if (!pmath) return E_FAIL;
	if (!pmath->IsInitialized()) return E_FAIL;
	m_pmath=pmath;
	return S_OK;
}

void CBoss1::Explode(void)
{
	if (lpfnExplode)
	{
		lpfnExplode(m_Frame.x,m_Frame.y);
		lpfnExplode(m_Frame.x+30,m_Frame.y+30);
		lpfnExplode(m_Frame.x+80,m_Frame.y+40);
		lpfnExplode(m_Frame.x+100,m_Frame.y+10);
	}

	if (m_pArm) m_pArm->Explode();
	if (m_pTentacle) 
	{
		m_pTentacle->Explode();
		m_pTentacle->SetEnergy(0);
	}
}

//imposta un punto di destinazione per il boss1
//steady indica se dopo aver raggiunto il punto si deve fermare per un po'
//freq rappresenta il periodo di "riposo"
void CBoss1::SetTargetPoint(int xtg,int ytg,int vel,bool steady,int freq)
{
	if (!m_pmath) return;
	if (vel<=0) return;

	//acquisisce la direzione del target point
	int ang=m_pmath->GetAngle(x,y,xtg,ytg);
	int dx=(x-xtg);
	int dy=(y-ytg);
	//calcola la distanza fra la posizione attuale e la destinazione
	unsigned int dist=m_pmath->FastSqrt(dx*dx+dy*dy);

	SetVelocity(ang,vel);
	iUpdateFreq=(int)((float)dist/speed);
	iStepUpdate=0;

	m_bsteady=steady;
	m_isteadyfreq=freq;
}


//aggiusta la velocit� in modo da seguire il bersaglio
void CBoss1::FollowTarget(void)
{
	if (!m_pmath) return;

	//acquisisce l'angolo della posizione del player
	int ang=m_pmath->GetAngle(x,y,lTargetX,lTargetY);

	//cambia la direzione del moto in modo da inseguire il player
	SetVelocity(ang,GetSpeed());

}
