/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  21-07-2004
  --------------------------------------------------------------
  A32OBJECT.H
  oggetti e stack di oggetti derivati dalla classe sprite

  ***************************************************************/


#ifndef _A32OBJECT_
#define _A32OBJECT_

#include "a32sprit.h"

//------------------------------- Classe CSBObject ----------------------------------------------------
//oggetto generico
class CSBObject:public CADXSprite
{

	friend class CObjectStack; //la classe object stack � amica di questa perche deve accedere al membro dwIndex 

private:


protected:

	static DWORD dwInstance; //contatore istanze	
	DWORD dwIndex;	
	LONG xabs,yabs; //coordinate assolute
	DWORD dwType; //indica il tipo di oggetto (ad ogni classe derivata corrisponde un tipo diverso)	
	void UpdateAbsXY(void);
	void Damage(CSBObject *pobj,float perc=1.0f); //perc=percentuale di power di pobj che viene tolta (piu' � grande e piu' l'oggetto viene danneggiato)
	LONG m_lInitialEnergy; //energia iniziale
	LONG dwEnergy;
	
public:

	static int g_iScreenWidth;  //altezza schermo
	static int g_iScreenHeight; //larghezza schermo
	static int g_iScreenBottomHeight; //altezza del terreno (se si va sotto viene eliminato dalla stack)

    BOOL  bActive; //indica che l'oggetto � attivo
	DWORD dwScore; //punteggio 	
	DWORD dwClass; //tipo, indica se � a terra ecc...
	DWORD dwStatus; //quando � 0 l'oggetto non � attivo e puo' essere rimosso (questo flag � usato con le operazioni di push e pop dello stack degli oggetti)	
	DWORD dwResId; //id della risorsa a cui appartiene (id di uso generale)
	int m_zlayer;  //z layer a cui appartiene l'oggetto 0=indica oggetto in primo piano
	LONG dwPower;	//potere distruttivo
	LONG lTargetX;  //coordinate del target da colpire (giocatore)
	LONG lTargetY; 
	LONG lGroundX; //coordinate correnti dello schema
	LONG lGroundY; 
//	IMAGE_FRAME_PTR m_imgDisplay; //display corrente (� la superficie su cui viene renderizzato)
	RECT rcBoundBox; //box che contiene la frame corrente
	void GetAbsXY(int *ax,int *ay);	
	CSBObject(void);
	virtual ~CSBObject(void);
	virtual HRESULT Draw(void); //disegna l'oggetto sul display imgDisplay
	virtual BOOL Update(void);	
	virtual void Reset(void); //resetta i membri privati (contatori ecc...)
	virtual BOOL DoCollisionWith(CSBObject *pobj); //esegue la collisione con un altro ogetto	
	virtual void Explode(void); //fa esplodere l'oggetto
	virtual void Collide(void); //collisione
	virtual LONG GetEnergy(void);
	virtual void IncrEnergy(LONG delta); //incrementa l'energia (valori negativi per decrementare)
	virtual void SetEnergy(LONG energy); //imposta l'energia corrente
	virtual void SetInitialEnergy(LONG energy); //imposta l'energia iniziale dell'oggetto	
	void (*lpfnExplode)(LONG x,LONG y); //punta alla funzione che inserisce una esplosione nello stack
	void (*lpfnCollide)(LONG x,LONG y); //funzione da chiamare in caso di collisione
	DWORD GetType(void); //restituisce il tipo di oggetto
	BOOL SetType(DWORD type); //imposta il tipo di oggetto
	DWORD GetStackIndex(void); //indice dell'oggetto all'interno dello stack attivo
	DWORD GetIndex(void); //posizione nello stack
	CSBObject *pParent; //oggetto genitore (ogni oggetto puo' avere un solo genitore)
	
};

typedef CSBObject *OBJECT_PTR;


//---------------------------------- classe CObjectStack ------------------------------------------

//coda degli oggetti visualizzati (comprende proiettili, nemici ecc...)
class CObjectStack
{
private:

	DWORD m_dwStackTop; //top dello stack
	DWORD m_dwStatus; //impostato su 1 quando viene creato lo stack
	DWORD m_dwNumElems; //numero di elementi nello stack
		
public:

	CSBObject **m_objStack; //(doppio ptr) vettore di puntatori agli oggetti che sono presenti nello stack
	CObjectStack(void);
	~CObjectStack(void);	
	LONG Push(CSBObject *pobj); //inserisce un oggetto nella lista (rende l'indice o -1)
	CSBObject *Pop(void); //rende il primo elemento non attivo (che ha dwStatus == 0)
	BOOL Delete(DWORD Index); //rimuove dalla coda l'elemento con indice Index
	void DeleteAll(void); //cancella tutti i riferimenti agli oggetti
	HRESULT Create(DWORD dwNumElems); //crea lo stack con dwNumElems elementi
	void Clear(BOOL bDestroyObjects);
	DWORD ItemCount(void); //rende il numero di elementi presenti
	DWORD GetTop(void); //stack top
};




#endif

