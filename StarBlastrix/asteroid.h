//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  asteroid.h -- header file per la classe CAsteroid
///////////////////////////////////////////////////////////////////////*/

#ifndef _ASTEROID_INCLUDE_
#define _ASTEROID_INCLUDE_

#include "enemy.h"
#include "resman.h"

class CAsteroid:public CEnemy
{
private:

	//quando l'asteroide viene distrutto, si spezzetta in asteroidi piu' piccoli
	//m_pBig � lo stack che contiene il set di asteroidi piu' grandi che vengono immessi
	//dopo che � stato distrutto e m_pSmall quelli di dimensione ancora piu' piccola.
	CObjectStack* m_pBig;
	CObjectStack* m_pSmall;
	int m_w,m_h; //larghezza e altezza del box frame

public:
		
	BOOL DoCollisionWith(CSBObject *pobj);
	CAsteroid(CObjectStack* pBig,CObjectStack *pSmall);
	void Reset(void);
	BOOL Update(void);	
};


HRESULT LoadAsteroidRes(CResourceManager* prm);
HRESULT CreateAsteroidStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags,CObjectStack* pBig,CObjectStack* pSmall);
void FreeAsteroidRes(CADXFrameManager* pfm);

#endif