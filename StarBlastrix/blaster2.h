/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
*/

#ifndef _BLASTER2_
#define _BLASTER2_

#include "mech.h"
#include "mcasmbl.h"
#include "mecharm.h"
#include "sbengine.h"

//---------------------------------- Classe CBlaster2 ------------------------------------------------

class CBlaster2:public CMechAssembly
{
private:

	CADXFastMath *m_pmath;
	CMechComponent *m_pmc;
	CObjectStack *m_pNozzleFlames;
	CAnimatedObject *m_pFlames;
	BOOL m_bDoFlames; //flag che indica che l'ugello � accesso
	int inextp; 
	int m_ttl;

public:

	//braccio meccanico
	CMechArm *m_pArm;
	//inclusione CMechComponent (non si puo' fare per ereditariet�
	//perch� CMechComponent e CEnemy hanno basi comuni e quindi la chiamata 
	//ad alcuni metodi tipo UpdatePosition risulta ambiguo (da rivedere)
	CMechComponent Body; 
	CBlaster2(void);
	~CBlaster2(void);
	void Reset(void);
	void SetNozzleFlamesStack(CObjectStack *pstack);
	BOOL Update(void);
	HRESULT Draw(void);
	void Explode(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	//ridefinisce set display (per evitare l'ereditariet� via dominance)
//	HRESULT SetDisplay(IMAGE_FRAME_PTR imgDisplay);
	HRESULT SetMathModule(CADXFastMath *pmath);

};

#endif