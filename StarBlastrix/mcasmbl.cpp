//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mcasmbl.cpp -- gestisce le collisioni per gli oggetti composti da piu' componenti
///////////////////////////////////////////////////////////////////////*/


#include "mcasmbl.h"

CMechAssembly::CMechAssembly(void)
{
	m_pCurWalk=NULL;
	lpfnMultiExplosion=NULL;
}


void CMechAssembly::ExplodeChain(CMechComponent *pmc)
{
	//fa esplodere tutta la catena cinematica a partire dal componente pmc
	if (lpfnExplode && pmc)
	{	
		//esplosioni sull'oggetto radice
		if (lpfnExplode) lpfnExplode(pmc->x,pmc->y);
		if (lpfnMultiExplosion) lpfnMultiExplosion((CADXSprite *)pmc,8);
		//fa esplodere tutti gli elementi figli
		pmc=pmc->GetNextChild(1);

		while(pmc)
		{
			pmc->m_iTag1=0;
			if (lpfnExplode) lpfnExplode(pmc->x,pmc->y);
			if (lpfnMultiExplosion) lpfnMultiExplosion((CADXSprite *)pmc,8);
			pmc=pmc->GetNextChild();
		}
	}
}

void CMechAssembly::SetEnergyChain(CMechComponent *pmc,LONG lEnergy)
{
	if (pmc)
	{
		pmc->m_iTag1=lEnergy; //imposta l'energia per l'elemento radice

		//resetta il walk
		CMechComponent* p=WalkChain(pmc);

		while(p)
		{
			p->m_iTag1=lEnergy;
			//va avanti nell'esplorazione
			p=WalkChain(NULL);

		}

	}
}




//Restituisce tutti gli elementi della catena cinematica a partire da pBase (non solo quelli
//collegati direttamente ma tutti), se pBase non � nullo resetta il Walk invocando con 
//pBase NULL si acquisisce l'elemento successivo 
CMechComponent* CMechAssembly::WalkChain(CMechComponent *pBase)
{
	if (pBase)
	{
		m_pCurWalk=pBase->GetNextChild(1); //resetta il walk		
	}
	else if (m_pCurWalk)
	{
		//questo codice � da verificare
		if (!m_pCurWalk->isWalking())
		{
			CMechComponent *ptmp=this->WalkChain(m_pCurWalk);
			if (!ptmp) m_pCurWalk=m_pCurWalk->GetNextChild(0); //passa ell'elemento successivo allo stesso livello
			else m_pCurWalk=ptmp;
		}

		else m_pCurWalk=m_pCurWalk->GetNextChild(0);

	}

	return m_pCurWalk;
}


void CMechAssembly::DamageComponent(CMechComponent *pmc,CSBObject *pobj)
{
    pmc->m_iTag1-=pobj->dwPower; //toglie energia al componente colpito
	pobj->IncrEnergy(-this->dwPower);

	if (pmc->m_iTag1<=0)
	{
		ExplodeChain(pmc); //fa esplodere tutti gli oggeti figli compreo l'oggetto colpito
	}

	if (pobj->GetEnergy()<=0) 
	{
		pobj->Explode();
	}
	if (pfnCollide) pfnCollide(x,y); //effetto collisione
}

