//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mcasmbl.h -- gestisce le collisioni per gli oggetti composti da piu' componenti
///////////////////////////////////////////////////////////////////////*/

#ifndef _MECH_ASSEMBLY_INCLUDE_
#define _MECH_ASSEMBLY_INCLUDE_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "enemy.h"
#include "mech.h"


class CMechAssembly: public CEnemy
{
private:

	CMechComponent* m_pCurWalk;

protected:

	//restituisce di volta in volta tutti gli elementi della catena cinematica in modo ricorsivo
	CMechComponent* WalkChain(CMechComponent *pBase);

public:

	//punta alla funzione esplosione multipla
	void (*lpfnMultiExplosion)(CADXSprite *pspr,int num);
	//costruttore
	CMechAssembly(void);
	virtual void ExplodeChain(CMechComponent *pmc);
	//esegue il danneggimanento dell'elemento pmc se collide con pobj
	virtual void DamageComponent(CMechComponent *pmc,CSBObject *pobj);
	//imposta m_iTag1 per tutti gli elementi
	void SetEnergyChain(CMechComponent *pmc,LONG lEnergy); 


};

#endif