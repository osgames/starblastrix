//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  snakeres.cpp -- funzioni di caricamento risorse per serpenti
///////////////////////////////////////////////////////////////////////*/

#include "a32graph.h"
#include "a32sprit.h"
#include "a32util.h"
#include "a32audio.h"
#include "snakeres.h"
#include "sbengine.h"
#include "tent.h"
#include "snake.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern CObjectStack g_sThunderBolt;
extern CObjectStack g_sEShell11;
extern CObjectStack g_objStack;

BOOL g_bRes1Loaded=FALSE;
BOOL g_bRes2Loaded=FALSE;
IMAGE_FRAME g_imgSnake1[24];
IMAGE_FRAME g_imgJaw1[24];
IMAGE_FRAME g_imgJaw2[24];
IMAGE_FRAME g_imgSnake2[24]; //serpente rosso
IMAGE_FRAME g_imgHead2[24];  //testa serpente rosso

//serpente corpo blu
HRESULT LoadSnake1Resources(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg;
	CADXFrameManager *pfm;

	if (!prm) return E_FAIL;
	if (g_bRes1Loaded) return S_OK; //risorse gi� caricate

	//se il file di risorse necessario a questo oggetto � gi� in memoria
	//evita di ricaricalo
	if (!(0==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),0))) return E_FAIL;
	}

	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	if (FAILED(pfm->GrabFrame(&g_imgJaw1[0],52,468,52+40,468+10,pimg))) return E_FAIL;

	if (FAILED(pfm->GrabFrame(&g_imgJaw2[0],52,490,52+40,490+10,pimg))) return E_FAIL;

	if (FAILED(pfm->GrabFrame(&g_imgSnake1[0],7,468,38,498,pimg))) return E_FAIL;


	for (int count=15;count<360;count+=15)
	{

		if (FAILED(pfm->CreateRotFrame(count,&g_imgJaw1[count/15],&g_imgJaw1[0],TRUE))) return E_FAIL;
	
		if (FAILED(pfm->CreateRotFrame(count,&g_imgJaw2[count/15],&g_imgJaw2[0],TRUE))) return E_FAIL;

		if (FAILED(pfm->CreateRotFrame(count,&g_imgSnake1[count/15],&g_imgSnake1[0],TRUE))) return E_FAIL;

	}

	g_bRes1Loaded=TRUE;

	return S_OK;
}

HRESULT FreeSnake1Resources(CADXFrameManager *pfm)
{

	if (!g_bRes1Loaded) return S_OK;

	g_fm.FreeFrameArray(g_imgSnake1,24);
	g_fm.FreeFrameArray(g_imgJaw1,24);
	g_fm.FreeFrameArray(g_imgJaw2,24);

	g_bRes1Loaded=false;

	return S_OK;
}

//serpente corpo rosso
HRESULT LoadSnake2Resources(CResourceManager *prm)
{
	IMAGE_FRAME_PTR pimg;
	CADXFrameManager *pfm;

	if (!prm) return E_FAIL;
	if (g_bRes2Loaded) return S_OK; //risorse gi� caricate

	//se il file di risorse necessario a questo oggetto � gi� in memoria
	//evita di ricaricalo
	if (!prm->GetCurrentFileName() || !(0==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),0))) return E_FAIL;
	}

	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
    //punta al frame manager
	pfm=prm->m_lpFm;

	if (FAILED(pfm->GrabFrame(&g_imgSnake2[0],530,298,530+35,298+38,pimg))) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&g_imgHead2[0],573,296,573+50,296+45,pimg))) return E_FAIL;

	for (int count=15;count<360;count+=15)
	{
		if (FAILED(pfm->CreateRotFrame(count,&g_imgSnake2[count/15],&g_imgSnake2[0],TRUE))) return E_FAIL;
		if (FAILED(pfm->CreateRotFrame(count,&g_imgHead2[count/15],&g_imgHead2[0],TRUE))) return E_FAIL;
	
	
	}

	g_bRes2Loaded=TRUE;

	return S_OK;	
}

void FreeSnake2Stack(CObjectStack *pstk)
{
	if (!pstk) return;
	pstk->Clear(TRUE);
}

HRESULT FreeSnake2Resources(CADXFrameManager *pfm)
{
	if (!g_bRes2Loaded) return S_OK;

	g_fm.FreeFrameArray(g_imgSnake2,24);
	g_fm.FreeFrameArray(g_imgHead2,24);

	g_bRes2Loaded=FALSE;

	return S_OK;
}


HRESULT CreateBlueTentacle(CTentacle *pTent,CResourceManager *prm)
{
	if (!pTent) return E_FAIL;

	if (FAILED(LoadSnake1Resources(prm))) return E_FAIL; //carica gli sprites pr questo tipo di tentacolo

	pTent->SetEnergy(10);
	pTent->SetGraphicManager(prm->m_lpGm);
	pTent->SetFrameManager(prm->m_lpFm);
	pTent->SetBodyJoints(4,15,28,15);
	pTent->SetLeftJawJoints(24,7,3,7);
	pTent->SetRightJawJoints(24,22,3,3);
	pTent->SetUpdateFreq(7);
	pTent->iConfigFreq=12; //mantenere questo parametro basso
	pTent->iStartFireFreq=60;
	pTent->SetMathModule(&g_fmath);
	pTent->lpfnExplode=PushExpl3;
	pTent->SetWeapon1(&g_sThunderBolt); //fulmine
	pTent->SetCartridgeStack(&g_sEShell11); //proiettili rossi

	if (FAILED(pTent->Create(15,g_imgSnake1,NULL,g_imgJaw1,g_imgJaw2))) return E_FAIL;

	return S_OK;
}

//inumitems=numero di elementi nello stack
//ibodyel=numero di elementi del corpo

HRESULT CreateSnakeRedStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int ibodyel=8)
{
	CSnake2 *ps;

	if (!pstk) return E_FAIL;
    //carica le risorse per il serpente di tipo 2
	if (FAILED(LoadSnake2Resources(prm))) return E_FAIL;
	
	if (FAILED(pstk->Create(inumitems))) return E_FAIL;	

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CSnake2;
		ps=(CSnake2*)pstk->m_objStack[count];
		ps->bActive=FALSE;
		ps->SetGraphicManager(prm->m_lpGm);
		ps->SetFrameManager(prm->m_lpFm);
		ps->SetUpdateFreq(40);
		ps->SetEnergy(25);
		ps->dwPower=10;
		ps->dwResId=EN_SNAKERED;
		ps->lpfnExplode=PushExpl9;
		ps->pfnCollide=PushCollideMetal;
		ps->SetActiveStack(&g_objStack);
		ps->m_pmath=&g_fmath;
		ps->SetCartridgeStack(&g_sEShell11); //proiettili rossi	
		//serpente rosso di default
		ps->SetBodyHotSpots(32,18,6,18);
		ps->SetHeadHotSpot(10,23);
		ps->SetCannonsHotSpot(48,16,48,29);
		if (FAILED(ps->Create(ibodyel,g_imgSnake2,g_imgHead2))) return E_FAIL;

		
	}

	return S_OK;
}


