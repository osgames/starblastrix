//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  jltent.cpp -- tentacolo estensibile 
///////////////////////////////////////////////////////////////////////*/

#include "jltent.h"
#include "resman.h"
#include "a32util.h"
#include "a32audio.h"

/* 
  
  Crea il tentacolo estensibile

  numitems=numero di elementi nel corpo del tentacolo (testa esclusa)
  pItem=frame (o vettore frame) per ogni elemento del corpo
  nframebody=numero di frame per ogni elemento del corpo (piu' frame per ottenere effetto lampeggio o movimento)
  pHeadItem=frame (o vettore frame) per la testa del tentacolo 
  nframehead=numero di frame in pHeadItem

*/

//frame per il corpo
#define JLTENT_BODY 3 
//frame per la testa
#define JLTENT_HEAD 3
//numero di tentacoli creati
#define MAX_TENTACLE 16

extern void PushExpl8(LONG x,LONG y);
extern void PushExpl2(LONG x,LONG y);
extern void PushCollideMetal(LONG x,LONG y);
extern CObjectStack g_objStack;
extern CADXFastMath g_fmath;
extern CADXFrameManager g_fm;
extern CADXSound g_cs;
extern void PushExplMulti(CADXSprite*,int);

BOOL m_bJlTentResLoaded=FALSE;
BOOL m_bJellyShipResLoaded=FALSE;
IMAGE_FRAME m_imgJlTentBody[JLTENT_BODY];
IMAGE_FRAME m_imgJlTentHead[JLTENT_HEAD];
IMAGE_FRAME m_imgJellyShip;
BOUND_POLY m_bp;
LONG m_snd1=-1;
CJlTentacle *m_pjlTent[MAX_TENTACLE];


HRESULT CreateJlTentacle(void)
{
	for (int count=0;count<MAX_TENTACLE;count++)
	{
		//crea un tentacolo con 6 elementi 
		m_pjlTent[count]=new CJlTentacle(7,m_imgJlTentBody,3,m_imgJlTentHead,3,&g_fm);
		m_pjlTent[count]->lpfnExplode=PushExpl2;

		if (!m_pjlTent[count]) return E_FAIL;
	}

	return S_OK;
}


void DestroyJlTentacle(void)
{
	for (int count=0;count<MAX_TENTACLE;count++)
	{
		SAFE_DELETE(m_pjlTent[count]);
	}
}


HRESULT LoadJlTentacleRes(CResourceManager *prm)
{
	if (m_bJlTentResLoaded) return S_OK;

	if (!prm) return E_FAIL;

	if (FAILED(prm->LoadAuxResFile(TEXT("data\\data5.vpx"),1))) return E_FAIL;

	IMAGE_FRAME_PTR pimg=NULL;
	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
	if (!pimg) return E_FAIL;
    //punta al frame manager
	CADXFrameManager *pfm=NULL;
	pfm=prm->m_lpFm;   
	if (!pfm) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(m_imgJlTentBody,pimg,3,56,223,28,28))) return E_FAIL;
	if (FAILED(pfm->GrabFrameArray(m_imgJlTentHead,pimg,3,558,124,62,62))) return E_FAIL;

	m_snd1=g_cs.CreateSound("WAVS\\ufosaw.wav",4);

	m_bJlTentResLoaded=TRUE;
	
	return S_OK;
}


HRESULT LoadJellyShipRes(CResourceManager *prm)
{
	if (m_bJellyShipResLoaded) return S_OK;

	if (!prm) return E_FAIL;

	if (FAILED(prm->LoadAuxResFile(TEXT("data\\data5.vpx"),1))) return E_FAIL;

	IMAGE_FRAME_PTR pimg=NULL;
	//puntatore all'immagine caricata in memoria
	pimg=prm->GetCurrentImage();
	if (!pimg) return E_FAIL;
    //punta al frame manager
	CADXFrameManager *pfm=NULL;
	pfm=prm->m_lpFm;   
	if (!pfm) return E_FAIL;
	if (FAILED(pfm->GrabFrame(&m_imgJellyShip,598,216,792,339,pimg))) return E_FAIL;

	//poligono di contenimento
	m_bp.inum=0;
	m_bp.poly=new VERTEX2D[25];
	AddVertex(&m_bp,5,70);
	AddVertex(&m_bp,14,55);
	AddVertex(&m_bp,30,54);
	AddVertex(&m_bp,44,46);
	AddVertex(&m_bp,52,37);
	AddVertex(&m_bp,63,16);
	AddVertex(&m_bp,79,4);
	AddVertex(&m_bp,92,0);
	AddVertex(&m_bp,105,1);
	AddVertex(&m_bp,124,6);
	AddVertex(&m_bp,136,20);
	AddVertex(&m_bp,148,42);
	AddVertex(&m_bp,166,54);
	AddVertex(&m_bp,187,64);
	AddVertex(&m_bp,192,72);
	AddVertex(&m_bp,180,79);
	AddVertex(&m_bp,186,90);
	AddVertex(&m_bp,193,90);
	AddVertex(&m_bp,169,122);
	AddVertex(&m_bp,137,105);
	AddVertex(&m_bp,44,104);
	AddVertex(&m_bp,30,122);
	AddVertex(&m_bp,0,90);
	AddVertex(&m_bp,10,89);
	AddVertex(&m_bp,17,78);

	return S_OK;
}

//Crea uno stack di oggetti
HRESULT CreateJellyShipStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	if (!prm) return E_FAIL;
	if (!pstk) return E_FAIL;

	if (FAILED(LoadJlTentacleRes(prm))) return E_FAIL;
	if (FAILED(LoadJellyShipRes(prm))) return E_FAIL;
	if (FAILED(CreateJlTentacle())) return E_FAIL;
    
	CJellyShip *pj=NULL;
	CADXFrameManager *pfm=prm->m_lpFm;

	int tentcnt=0;

	pstk->Create(inumitems);
    
	for (DWORD count=0;count<inumitems;count++)
	{	
		pstk->m_objStack[count]=new CJellyShip();
		pj=(CJellyShip *)pstk->m_objStack[count];
		pj->bActive=FALSE;
		pj->SetFrameManager(pfm);
		pj->SetUpdateFreq(40);
		pj->SetInitialEnergy(80);
		pj->dwPower=10;
		pj->lpfnExplode=PushExpl8;
		pj->AddFrame(&m_imgJellyShip);	
		pj->SetBoundPoly(0,&m_bp);
		pj->pfnCollide=PushCollideMetal;
		pj->lpfnExplMulti=PushExplMulti;
		pj->SetActiveStack(&g_objStack);
		//giunti per i tentacoli
		pj->SetFrameHotSpot(0,0,50,93);
		pj->SetFrameHotSpot(0,1,80,93);
		pj->SetFrameHotSpot(0,2,110,93);
		pj->SetFrameHotSpot(0,3,140,93);

		
		//attacca i tentacoli
		for (int i=0;i<4;i++)
		{			
			if (tentcnt<MAX_TENTACLE)
			{
				pj->AddJellyTentacle(m_pjlTent[tentcnt],i);
			}

			tentcnt++;
		} 
	}

	return S_OK;
}

void FreeJlTentacleRes(CADXFrameManager *pfm)
{
	if (!pfm) return;

	if (m_bJlTentResLoaded)
	{
		pfm->FreeFrameArray(m_imgJlTentBody,3);
		pfm->FreeFrameArray(m_imgJlTentHead,3);
		m_bJlTentResLoaded=FALSE;
	}

	if (m_snd1>0) g_cs.FreeSound(m_snd1);
	//rilascia la memoria allocata per i tentacoli
	DestroyJlTentacle();
}

void FreeJellyShipRes(CADXFrameManager *pfm)
{
	if (!pfm) return;

	if (m_bJellyShipResLoaded)
	{
		pfm->FreeImgFrame(&m_imgJellyShip);
		m_bJellyShipResLoaded=FALSE;
	}

	SAFE_DELETE_ARRAY(m_bp.poly);
	m_bp.inum=0;
}

//---------------------------------------------- CJlTentacle -------------------------------------------

CJlTentacle::CJlTentacle(int numitems,IMAGE_FRAME_PTR pItem,int nframebody,IMAGE_FRAME_PTR pHeadItem,int nframehead,CADXFrameManager *pfm)
{
	m_Items=NULL;
	m_item_dia=0;
	m_head_dia=0;
	m_inumitems=0;
	m_pHead=NULL;
	m_pParent=NULL;
	m_iparent_hot_spot=0;

	dwClass=CL_ENEMY;
	
	if (!pfm) return;

	m_Ip=m_CmdTop=NULL;
	m_Program.next=NULL;
	m_Program.opcode=jlNop;
	m_swing_step=0;
	m_Program.p1=m_Program.p2=m_Program.p3=0;
    m_cntflash=m_iflash_item=m_iflash_step=m_iflash_freq=m_iflash_mode=0;
	m_iflash_inc=1;    
	lpfnMultiExplosion=NULL;

	if (numitems>=1 && pItem && pHeadItem && nframebody>=1 && nframehead>=1) 
	{
		if (pItem->status && pHeadItem->status)
		{
			int dx,dy;

			//numero totale di elementi compreso la testa del serpente
			m_inumitems=numitems+1;
			//alloca un vettore di puntatori a sprite con tutti gli elementi del corpo piu' un elemento per la testa
			m_Items=new CADXSprite*[m_inumitems];
			//vettore energia iniziale di ogni elemento del tentacolo
			m_piEnergy=new int[m_inumitems];
			//gli elementi si considerano tutti uguali e di forma circolare
			m_item_dia=pItem[0].height;

			//istanzia gli elementi del tentacolo (solo per il corpo)
			for (int count=0;count<m_inumitems-1;count++) 
			{
				m_Items[count]=new CADXSprite();

				m_Items[count]->SetFrameManager(pfm);

				m_piEnergy[count]=25; //imposta l'energia iniziale
			
				for (int count1=0;count1<nframebody;count1++)
				{
					m_Items[count]->AddFrame(&pItem[count1]);
					dx=(int)(pItem[count1].width*0.5);
					dy=(int)(pItem[count1].height*0.5);
					//imposta come hot spot di ogni elemento il centro
					m_Items[count]->SetFrameHotSpot(count1,0,dx,dy);
				}

				m_Items[count]->SetCurFrame(0);
				
			}

			//testa
			m_Items[m_inumitems-1]=new CADXSprite();

			//la testa � l'ultimo elemento
			m_pHead=m_Items[m_inumitems-1]; 

			m_pHead->SetFrameManager(pfm);

			m_head_dia=pHeadItem[0].height;

			for (int count1=0;count1<nframehead;count1++) 
			{
				m_pHead->AddFrame(&pHeadItem[count1]);
				m_pHead->SetFrameHotSpot(count1,0,(int)(pHeadItem[count1].width*0.5),(int)(pHeadItem[count1].height*0.5));

			}
			
		}
	}
}


CJlTentacle::~CJlTentacle(void)
{

	ResetProgram();

	if (m_inumitems)
	{
		//dealloca ogni singolo sprite
		for (int count=0;count<m_inumitems;count++) SAFE_DELETE(m_Items[count]);
	}
	
	//dealloca il vettore di puntatori
	SAFE_DELETE_ARRAY(m_Items);
	SAFE_DELETE_ARRAY(m_piEnergy);
}

void CJlTentacle::Reset(void)
{
	//m_item_dia=diametro medio di un elemento del corpo
	int cy=-m_item_dia;

	//nota bene: x e y sono coordinate relative!!!
	CEnemy::Reset();
	//imposta la posizione di default verticale
	for (int count=0;count<m_inumitems-1;count++)
	{
		cy+=m_item_dia;
		m_Temp=m_Items[count];
		m_piEnergy[count]=18; //imposta l'energia iniziale di ogni elemento
		m_Temp->SetJointPosition(0,cy,0);		
	}

	//testa
	cy+=m_item_dia;
	m_pHead->SetJointPosition(0,cy,0);
	m_piEnergy[m_inumitems-1]=40;
	//posizione iniziale (per posizione si intendono le coordinate relative della testa del tentacolo)
	m_yinit=cy;
	m_xinit=0; 
	//coordinate polari iniziali
	m_thetainit=90;
	m_rhoinit=cy;
	//allungamento massimo cosentito
	m_maxrho=m_rhoinit; //(int)((float)m_rhoinit*1.20f); 
	m_cx=m_xinit;
	m_cy=m_yinit;
	m_cr=m_rhoinit;
	m_ct=m_thetainit;
	iStepConfig=1;
	iUpdateFreq=50;
	//azzera il programma di istruzioni di movimento e imposta l'istruzione corrente su NULL
	ResetProgram();


	//programma di default (nota bene: JellyShip esegue un suo programma personalizzato e non questo!!!)
	this->PushInstruction(jlFlash,10,8,0);
	this->PushInstruction(jlStop,5,0,0);	
	this->PushInstruction(jlSaw,350,0,0);
	this->PushInstruction(jlSwing,3,30,30,2);
	this->PushInstruction(jlFollowPl,50,3,0);
	this->PushInstruction(jlFlashAll,20,8,0);
	this->PushInstruction(jlStop,3,0,0);
	this->PushInstruction(jlGotoDef,0,0,0);
	this->PushInstruction(jlStop,3,0,0);
	this->PushInstruction(jlRndSwing,0,0,0,2);
	this->PushInstruction(jlSaw,550,0,0);
	this->PushInstruction(jlTarget,-180,80,0);
	this->PushInstruction(jlStop,1,0,0);
	this->PushInstruction(jlRotate,1,0,0);
	this->PushInstruction(jlTarget,80,-80,0);
	this->PushInstruction(jlGotoDef,0,0,0);
	this->PushInstruction(jlFollowPl,20,10,0);
	this->PushInstruction(jlFlash,10,8,0);
	this->PushInstruction(jlSwing,6,45,45);
	this->PushInstruction(jlSaw,350,0,0);
	this->PushInstruction(jlRotate,2,0,0,3);

}

BOOL CJlTentacle::IsBusy(void)
{
	return (m_Ip!=NULL);
}


BOOL CJlTentacle::DoCollisionWith(CSBObject *pobj)
{
	LONG xp,yp,xj,yj;
	
	BOOL bres=FALSE;

	if (m_pParent)
	{
		m_pParent->GetJointPosition(&xp,&yp,m_iparent_hot_spot);	
	}

	for (int count=0;count<m_inumitems;count++)
	{
		if (m_piEnergy[count]<=0) break;

		m_Temp=m_Items[count];

		//le coordinate x e y di m_pTemp sono relative al giunto di posizione xp,yp
		//posizione del centro del'elemento
		m_Temp->GetJointPosition(&xj,&yj,0); 

		m_Temp->SetJointPosition(xj+xp,yj+yp,0);

		if (pobj->DetectCollision(m_Temp))
		{
			bres=TRUE;

			if (lpfnCollide) lpfnCollide(m_Temp->x,m_Temp->y);
		
			pobj->SetEnergy(pobj->GetEnergy()-10);

			if (pobj->GetEnergy()<=0) pobj->Explode();

			m_piEnergy[count]-=pobj->dwPower;

			if (m_piEnergy[count]<=0)
			{
				if (lpfnExplode)
				{
					lpfnExplode(m_Temp->x,m_Temp->y);

					m_Temp->SetJointPosition(xj,yj,0);

					//fa esplodere tutto il tentacolo al di sotto dell'elemento distrutto
					for (int j=count+1;j<m_inumitems;j++)
					{
						if (m_piEnergy[j]>0)
						{
							m_piEnergy[j]=0;

							m_Temp=m_Items[j];

							m_Temp->GetJointPosition(&xj,&yj,0);

							lpfnExplode(xj+xp,yj+yp);
						}

					}

					break;
				}
			}
		}

		m_Temp->SetJointPosition(xj,yj,0);

	}	

	return bres;

}

//la prima istruzione ad essere inserita nella coda � anche la prima ad essere eseguita
BOOL CJlTentacle::PushInstruction(JlCommands opcode,LONG p1,LONG p2,LONG p3,int config_freq)
{
	if (!IsBusy())
	{
		if (m_CmdTop==NULL) m_CmdTop=&m_Program;
		else 
		{
			m_CmdTop->next=new JL_COMMAND;
			m_CmdTop=m_CmdTop->next; //si sposta sull'elemento creato
		}

		//le istruzioni sono concatenate in una linked list
		m_CmdTop->next=NULL;
		m_CmdTop->opcode=opcode;
		m_CmdTop->p1=p1;
		m_CmdTop->p2=p2;
		m_CmdTop->p3=p3;
		m_CmdTop->config_freq=config_freq;

		return TRUE;
	}

	else return FALSE;
}

//collega il tentacolo ad uno sprite genitore
BOOL CJlTentacle::LinkTo(CADXSprite *parent,int parenthotspot)
{
	if (parent)
	{
		m_pParent=parent;
		m_iparent_hot_spot=parenthotspot;
		return TRUE;
	}

	return FALSE;
}

void CJlTentacle::ResetProgram(void)
{

	//dealloca la linked list di istruzioni
	m_Ip=m_Program.next;

	while (m_Ip)
	{	
		m_Cmd=m_Ip;
		m_Ip=m_Ip->next;
		m_Cmd->next=NULL;
		SAFE_DELETE(m_Cmd);
	}

	m_Ip=m_CmdTop=NULL;	
	m_Program.next=NULL;
	m_Program.opcode=jlNop;
	m_bRunningCmd=FALSE;
	m_Program.p1=m_Program.p2=m_Program.p3=0;
	m_cntsaw=0;
	m_iflash_mode=0;
	m_CntDown=0;
}


BOOL CJlTentacle::Update(void)
{

	if (iStepUpdate++>iUpdateFreq)
	{
		iStepUpdate=0;

		if (m_CntDown)
		{
			//conto alla rovescia clicli:quando raggiunge zero chiede di passare alla
			//istruzione successiva
			m_CntDown--;
			if (!m_CntDown) m_bRunningCmd=FALSE;
		}
		
	}

	if (!m_bRunningCmd)
	{
		//passa all'istruzione successiva
		if (!m_Ip) m_Ip=&m_Program;
		else m_Ip=m_Ip->next;
		
		if (!m_Ip) 
		{
			//� arrivato alla fine del programma, quindi cancella il programma in memoria e ne attende un altro
			ResetProgram();
			m_bRunningCmd=FALSE;
		}
		else
		{
			//imposta l'algoritmo in base all'istruzione
			m_bRunningCmd=TRUE;
		    m_CntDown=0;
			iStepConfig=0;
			iConfigFreq=m_Ip->config_freq;

			//esegue l'istruzione
			switch(m_Ip->opcode)
			{
			case  jlNop:

				//no operation:chiede di passare alla istruzione successiva
				m_bRunningCmd=FALSE; 
				break;

			case jlStop:

				//sta fermo per un certo numero di cicli
				m_CntDown=m_Ip->p1;
				break;

			case jlRndSwing:
				
				m_swing_step=g_fmath.RndFast(1) ? 1 : -1;
				m_Ip->p1=4;
				m_Ip->p2=20+g_fmath.RndFast(30);
				m_Ip->p3=20+g_fmath.RndFast(30);
				//fall-through
				
			case jlSwing:

				m_Ip->opcode=jlSwing;
				if (m_Ip->opcode!=jlRndSwing) m_swing_step=1;
				//fall-through

			case jlRotate:
				
				//modalit� pendolo:inizia a oscillare dalla posizione in cui si trova
				m_pHead->GetJointPosition(&m_cx,&m_cy,0);
				//calcola il raggio attuale
				m_cr=g_fmath.FastSqrt(m_cx*m_cx+m_cy*m_cy);				
				//angolo corrente
				m_zero_angle=g_fmath.GetAngle(0,0,m_cx,m_cy);
				//imposta la posizione zero nella modalit� pendolo
				SetPolarPosition(m_cr,m_zero_angle);
			
				break;

			case jlGotoDef:
				
				//va nella posizione di default
				SetTargetPosition(m_xinit,m_yinit,1);
				break;

			case jlTarget:

				//va nella posizione specificata
				SetTargetPosition(m_Ip->p1,m_Ip->p2,1);
				break;	

			case jlFollowPl:

				GetPlayerTarget(&m_xtg,&m_ytg);
				SetTargetPosition(m_xtg,m_ytg,2);
				m_cnt1=m_cnt2=0;

				break;

			case jlRndOp:
				//istruzione random
				switch(g_fmath.RndFast(5))
				{
				case 0:

					m_Ip->opcode=jlGotoDef;
					break;

				case 1:

					m_Ip->opcode=jlStop;
					m_Ip->p1=3;
					m_CntDown=m_Ip->p1;
					break;

				case 2:

					m_Ip->opcode=jlTarget;
					m_Ip->p1=g_fmath.RndFast(200)-400;
					m_Ip->p2=g_fmath.RndFast(200)-400;
					SetTargetPosition(m_Ip->p1,m_Ip->p2,1);
					break;

				case 3:

					m_Ip->opcode=jlRotate;
					m_Ip->p1=2;
					m_Ip->p2=g_fmath.RndFast(1) ? 1 : -1;
					break;

				default:

					m_Ip->opcode=jlFollowPl;
					GetPlayerTarget(&m_xtg,&m_ytg);
					SetTargetPosition(m_xtg,m_ytg,2);
					m_Ip->p1=45;
					m_Ip->p2=10;
					m_cnt1=m_cnt2=0;
					break;
				}

				break; //fine jlRndOp

			case jlFlash:

				//flash avanti e indietro				
				m_iflash_mode=1;
				m_iflash_item=0;
				m_iflash_inc=1;
				m_iflash_step=0;
				m_iflash_freq=m_Ip->p2; //freq. aggiornamento
				m_cntflash=m_Ip->p1; //numero di corse
				m_bRunningCmd=FALSE; //passa subito alla istruzione successiva
				

				break;	
				
			case jlFlashAll:

				//flash contemporaneo di tutti gli elementi
				m_iflash_mode=2;
				m_iflash_item=0;
				m_iflash_inc=1;
				m_iflash_freq=m_Ip->p2; //freq. aggiornamento
				m_cntflash=m_Ip->p1; //numero di corse
				m_bRunningCmd=FALSE; //passa subito alla istruzione successiva
				break;

			case jlFlashOff:
				//disattiva la modalit� flash
				m_iflash_mode=3;
				m_iflash_freq=10;				
				m_bRunningCmd=FALSE;
				break;

			case jlSaw:

				//se la testa � eliminata evit di animarla
				if (m_piEnergy[m_inumitems-1]>=0)
				{
					//attiva la sega per p1 cicli
					if (m_snd1>0) g_cs.PlaySound(m_snd1,0,0);
					m_cntsaw=m_Ip->p1;
					m_pHead->SetAnimSpeed(0.0f);
					m_sawanim=0.0f;				
				}
				
				m_bRunningCmd=FALSE;
				break;

			default:
				
				//istruzione non valida
				m_bRunningCmd=FALSE; 
				break;
			}
		}
		
	}
	else
	{
		int d;

		if (++iStepConfig>=iConfigFreq)
		{
			iStepConfig=0;

			//esegue l'istruzione corrente
			switch(m_Ip->opcode)
			{

			case jlSwing:
				//pendolo

				d=(m_ct-m_zero_angle);

				if (d>m_Ip->p2)
				{
					m_swing_step=-1;
					m_Ip->p1--;
					if (m_Ip->p1<=0) m_bRunningCmd=FALSE; //il numero di oscillazioni � stato raggiunto
				}
				else if (d<-m_Ip->p3)
				{
					m_swing_step=1;
					m_Ip->p1--;				
				}

			
				iStepConfig=0;
				m_ct+=m_swing_step;
				SetPolarPosition(m_cr,m_ct);
			
				break;

			case jlRotate:
				//ruota intorno al giunto
				
				m_ct+=m_Ip->p2;

				if (m_ct>=360) m_ct-=360;
				else if (m_ct<0) m_ct=360+m_ct;

				if (m_ct==m_zero_angle)
				{
					//ha compiuto una rotazione
					m_Ip->p1--;
					
					if (m_Ip->p1<=0) m_bRunningCmd=FALSE; //ha compiuto il numero di rotazioni richieste			

				}

				SetPolarPosition(m_cr,m_ct);

				break;

				//va nella posizione di default
			case jlGotoDef: 

				m_pHead->UpdatePosition();
				UpdateBodyPosition();
				//quando raggiunge il bersaglio, o diventa troppo lungo termina l'esecuzione
				if ((abs(m_xtg-m_cx)<6 && abs(m_ytg-m_cy)<6))
				{
					m_bRunningCmd=FALSE;
					//lo posiziona esattamente
					SetRectPosition(m_xinit,m_yinit);
				}
				else if (IsTooLong()) m_bRunningCmd=FALSE;

				break;

			case jlTarget:
				//va in una posizione specificata			

				m_pHead->UpdatePosition();
				UpdateBodyPosition();
				//quando raggiunge il bersaglio, o diventa troppo lungo termina l'esecuzione
				if ((abs(m_xtg-m_cx)<3 && abs(m_ytg-m_cy)<3) || IsTooLong()) m_bRunningCmd=FALSE;

				break;

			case jlFollowPl:
				
				//jlFollowPl=6,  //insegue il player (p1=ogni quanti cicli di esecuzione aggiorna il target,p2=totale aggiornamenti prima di finire l'istruzione)
				m_pHead->UpdatePosition();
				UpdateBodyPosition();

				if (++m_cnt1>=m_Ip->p1)
				{
					m_cnt1=0;
					//� il momento di aggiornare le coordinate del target
					GetPlayerTarget(&m_xtg,&m_ytg);
					SetTargetPosition(m_xtg,m_ytg,2);														

					//fine inseguimento: passa alla istruzione successiva
					if (++m_cnt2>=m_Ip->p2) m_bRunningCmd=FALSE; 
				}
				
				//se raggiunge il target o diventa troppo lungo riaggiorna le coordinate del bersaglio
				//al prossimo ciclo
				//if ((abs(m_xtg-m_cx)<3 && abs(m_ytg-m_cy)<3) || IsTooLong()) m_cnt1=m_Ip->p1;

				break;				

			}
		}
	}
	
	//� in corso il flash
	if (m_iflash_mode) 
	{
		if (++m_iflash_step>=m_iflash_freq)
		{
			m_iflash_step=0;
			this->Flash();
		}
	}

	//controlla se � attiva la sega
	if (m_cntsaw>0)
	{
		if (m_cntsaw>30 && m_sawanim<0.4f)
		{
			//accelera
			m_sawanim+=0.004f;
			m_pHead->SetAnimSpeed(m_sawanim);
		}
		else if(m_cntsaw<30 && m_sawanim>0.01f)
		{
			//decelera
			m_sawanim-=0.01f;
			m_pHead->SetAnimSpeed(m_sawanim);
		}

		m_cntsaw--;
		m_pHead->UpdateFrame();

	}

	return TRUE;
}

void CJlTentacle::GetPlayerTarget(int *xtg,int *ytg)
{
	LONG xp=0,yp=0;

	if (m_pParent)
	{
		m_pParent->GetJointPosition(&xp,&yp,m_iparent_hot_spot);	
	}

	int an=g_fmath.GetAngle(xp,yp,lTargetX,lTargetY);

	int d1=lTargetX-xp;
	d1*=d1;
	int d2=lTargetY-yp;
	d2*=d2;
	int r=g_fmath.FastSqrt(d1+d2);

	if (r<m_maxrho)
	{
		//il player � all'interno dell'area di azione del tentacolo
		*xtg=lTargetX-xp;
		*ytg=lTargetY-yp;
	}
	else
	{
		*xtg=(int)(m_maxrho*cos(an*DEG_TO_RADIANS));
		*ytg=(int)(m_maxrho*sin(an*DEG_TO_RADIANS));
	}

}

BOOL CJlTentacle::IsTooLong(void)
{
	//controlla che il tentacolo non sia troppo lungo
	return (abs(m_pHead->x)>m_maxrho || abs(m_pHead->y)>m_maxrho);
}


//imposta le coordinate relative del target
void CJlTentacle::SetTargetPosition(int xd,int yd,int vel)
{
	m_pHead->GetJointPosition(&m_cx,&m_cy,0);
	
	int an=g_fmath.GetAngle(m_cx,m_cy,xd,yd);

	//imposta la velocit� necessaria a raggiungere l'obiettivo
	m_pHead->SetVelocity(an,vel);

	m_xtg=xd;
	m_ytg=yd;
	
}

//imposta la posizione della parte finale del tentacolo in coordinate polari
//riferite all'origine del tentacolo (giunto a cui � attaccato)
void CJlTentacle::SetPolarPosition(int rho,int theta)
{
	float a= theta * DEG_TO_RADIANS;
	//aggiorna le coordinate polari correnti
	m_ct=theta;
	m_cr=rho;
	SetRectPosition((int)(rho*cos(a)),(int)(rho*sin(a)));
}

//imposta la posizione della parte finale del tentacolo in coordinate rettangolari
//riferite all'origine del tentacolo (giunto a cui � attaccato)
void CJlTentacle::SetRectPosition(int xd,int yd)
{
	//nota bene: le coordinate qui espresse si intendono relative al giunto di attacco!!!
	m_pHead->SetJointPosition(xd,yd,0);

	//aggiorna la posizione corrente
	m_cx=xd;
	m_cy=yd;

	UpdateBodyPosition();

}

//aggiorna la posizione degli oggetti del corpo in base alla posizione della testa
void CJlTentacle::UpdateBodyPosition(void)
{
	LONG x1,y1;
	LONG x2,y2;
	register LONG xd,yd;
	
	m_pHead->GetJointPosition(&m_cx,&m_cy,0);
	xd=m_cx;
	yd=m_cy;

	for (int count=m_inumitems-2;count>0;count--)
	{
		m_Temp=m_Items[count];
		m_Temp->GetJointPosition(&x1,&y1,0);
		m_pTemp1=m_Items[count-1];
		m_pTemp1->GetJointPosition(&x2,&y2,0);

		//fa la media della posizione fra l'elemento precedente e quello successivo
		xd=(int)((xd+x2)*0.5f);
		yd=(int)((yd+y2)*0.5f);
		
		m_Temp->SetJointPosition(xd,yd,0);	
	}
}
/*
	int m_cntflash;
	int m_iflash_item;      //elemento correntemente illuminato
	int m_iflash_step;
	int m_iflash_freq;
	int m_iflash_mode;      //0=nessun flash 1=avanti e indietro 2=tutti gli elementi
*/


//illumina un elemento
void CJlTentacle::HilightItem(int item,int inc)
{
	if (item>=0 && item<=m_inumitems-2)
	{
		if (inc>0)
		{
			m_Items[item]->IncFrame();
		}
		else m_Items[item]->DecFrame();
	}
}


//effetto illuminazione progressiva
void CJlTentacle::Flash(void)
{
	BOOL bFlash=FALSE;
	int count;

	switch(m_iflash_mode)
	{
	case 1:
		
		//avanti e indietro						
		m_iflash_item+=m_iflash_inc;

		if (m_iflash_item<0)
		{
			m_iflash_item=0;
			m_iflash_inc=1; //va avanti
			if (--m_cntflash<=0) m_iflash_mode=3; //fine flash

		}
		else if (m_iflash_item>m_inumitems-2)
		{
			m_iflash_item=m_inumitems-2;
			m_iflash_inc=-1; //torna indietro
			if (--m_cntflash<=0) m_iflash_mode=3; //fine flash
		}	
		
		if (m_iflash_inc==1)
		{
			//avanti

			HilightItem(m_iflash_item,1);
			HilightItem(m_iflash_item+1,1);
			HilightItem(m_iflash_item-1,-1);
			HilightItem(m_iflash_item-2,-1);
		}
		else
		{
			//indietro
			HilightItem(m_iflash_item,1);
			HilightItem(m_iflash_item+1,-1);
			HilightItem(m_iflash_item+2,-1);
			HilightItem(m_iflash_item-1,1);			
		}
			

		break;

	case 2:

		//illumina tutti gli elementi

		if (++m_iflash_item>=3)
		{
			m_iflash_item=0;
			m_iflash_inc=-m_iflash_inc;

			if (--m_cntflash<=0) m_iflash_mode=3; //fine flash			

		}

		for (count=0;count<m_inumitems-1;count++)
		{
			HilightItem(count,m_iflash_inc);
		}		

		break;

	case 3:

		//elimina gradualmente il flash
		//quando non c'� piu' flash resetta la modalit�
		for (count=0;count<m_inumitems-1;count++)
		{
			//fino a che ci sono elementi illuminati non resetta la modalit�
			HilightItem(count,-1);
			bFlash |= (m_Items[count]->GetCurFrame()>0); 
		}

		if (!bFlash) m_iflash_mode=0; //nessun flash attivo

		break;

	default:

		break;
	}
}

HRESULT CJlTentacle::Draw(void)
{
	LONG xp,yp;

	if (m_pParent)
	{
		m_pParent->GetJointPosition(&xp,&yp,m_iparent_hot_spot);	
	}

	else xp=yp=0;

	for (int count=0;count<m_inumitems;count++)
	{
		if (m_piEnergy[count]<=0) break;
		m_Temp=m_Items[count];
		m_Temp->SetDisplay(m_pimgOut);
		m_Temp->RenderToDisplay(xp+m_Temp->x,yp+m_Temp->y);			
	}

	return S_OK;
}


//-------------------------------------------- CJellyShip ---------------------------------------------


CJellyShip::CJellyShip(void)
{
	m_iNumTent=0;

	dwClass=CL_ENEMY;

	lpfnExplMulti=NULL;

	for (int count=0;count<JELLY_TENT_NUM;count++)
	{
		m_ptent[count]=NULL;
	}
}

/* 

  Collega il tentacolo tentacle alla navicella
  parenthotspot=hotspot della frame genitore

*/

BOOL CJellyShip::AddJellyTentacle(CJlTentacle *tentacle,int parenthotspot)
{
	if (!tentacle || parenthotspot<0 || m_iNumTent==JELLY_TENT_NUM) return FALSE;

	if (tentacle->LinkTo(this,parenthotspot))
	{
		m_ptent[m_iNumTent++]=tentacle;

		return TRUE;
	}
	
	return FALSE;
}

BOOL CJellyShip::DoCollisionWith(CSBObject *pobj)
{
	BOOL bres=FALSE;

	bres |= CEnemy::DoCollisionWith(pobj);

	for (int count=0;count<m_iNumTent;count++)
	{
		bres |= m_ptent[count]->DoCollisionWith(pobj);
	}	

	return bres;

}

HRESULT CJellyShip::Draw(void)
{
	CEnemy::Draw();

	for (int count=0;count<m_iNumTent;count++)
	{
		m_pTemp=m_ptent[count];
		if (m_pTemp)
		{
			m_pTemp->SetDisplay(m_pimgOut);
			m_pTemp->Draw();
		}
	}

	return S_OK;
}

void CJellyShip::Reset(void)
{
	CEnemy::Reset();
	SetVelocity(180,2);

	for (int count=0;count<m_iNumTent;count++) 
	{
		if (m_ptent[count]) 
		{
			m_pTemp=m_ptent[count];

			m_pTemp->Reset();
			//usa un programma personalizzato per i tentacoli
			m_pTemp->ResetProgram();			
			
			m_pTemp->PushInstruction(jlStop,5,0,0);	
			m_pTemp->PushInstruction(jlSaw,350,0,0);
			m_pTemp->PushInstruction(jlSwing,3,30,30,2);
			m_pTemp->PushInstruction(jlFlash,10,8,0);

			switch (count)
			{
			case 0:
				m_pTemp->PushInstruction(jlTarget,-180,-50,0);
				break;
			case 1:
				m_pTemp->PushInstruction(jlTarget,-110,20,0);
				break;
			case 2:
				m_pTemp->PushInstruction(jlTarget,110,20,0);
				break;
			case 3:
				m_pTemp->PushInstruction(jlTarget,180,-50,0);
				break;
			}

			m_pTemp->PushInstruction(jlStop,4,0,0);
			m_pTemp->PushInstruction(jlGotoDef,0,0,0);
			m_pTemp->PushInstruction(jlRndSwing,0,0,0,2);
			m_pTemp->PushInstruction(jlGotoDef,0,0,0);

			m_pTemp->PushInstruction(jlFlash,10,8,0);

			if (count<2) 
			{
				m_pTemp->PushInstruction(jlRotate,2,1,0);
			}
			else m_pTemp->PushInstruction(jlRotate,2,-1,0);

			m_pTemp->PushInstruction(jlSaw,300,0,0);
			m_pTemp->PushInstruction(jlRndOp,0,0,0);
			m_pTemp->PushInstruction(jlFlashAll,20,8,0);
			m_pTemp->PushInstruction(jlFollowPl,50,3,0);
			m_pTemp->PushInstruction(jlFlashAll,20,8,0);
			m_pTemp->PushInstruction(jlStop,3,0,0);
			m_pTemp->PushInstruction(jlGotoDef,0,0,0);
			m_pTemp->PushInstruction(jlStop,3,0,0);
			m_pTemp->PushInstruction(jlRndSwing,0,0,0,2);
			m_pTemp->PushInstruction(jlSaw,550,0,0);
			m_pTemp->PushInstruction(jlTarget,-180,80,0);
			m_pTemp->PushInstruction(jlStop,1,0,0);
			m_pTemp->PushInstruction(jlRotate,1,0,0);
			m_pTemp->PushInstruction(jlTarget,80,-80,0);
			m_pTemp->PushInstruction(jlGotoDef,0,0,0);
			m_pTemp->PushInstruction(jlFollowPl,20,10,0);
			m_pTemp->PushInstruction(jlFlash,10,8,0);
			m_pTemp->PushInstruction(jlSwing,6,45,45);
			m_pTemp->PushInstruction(jlSaw,350,0,0);
			m_pTemp->PushInstruction(jlRotate,2,0,0,3);

		}

	}

	iUpdateFreq=120;
	m_ttl=50;
	m_repeat=1;
	m_ai=0; //algoritmo corrente
}

BOOL CJellyShip::Update(void)
{
	if (++iStepUpdate>iUpdateFreq)
	{
		iStepUpdate=0;

		if (--m_repeat<=0)
		{
			m_ai=g_fmath.RndFast(8);
		}

		int ang;	

		switch(m_ai)
		{
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:

			ang=g_fmath.GetAngle(x,y,lTargetX,lTargetY);

			iUpdateFreq=50;		
			if (m_repeat<=0) m_repeat=8;


			switch(m_ai)
			{
			case 2:
			case 3:

				SetVelocity(ang,2);
				break;

			case 4:

				SetVelocity(ang,3);
				break;

			default:

				SetVelocity(ang,1);
				break;

			}

			break;

		default:

			m_repeat=1;
			SetVelocity(0,0);
			iUpdateFreq=g_fmath.RndFast(180)+80;
			break;

		}

		if (m_ttl-- <=0)
		{
			//esce di scena
			SetVelocity(180,3);
			iUpdateFreq=300;
		}

	}

	UpdatePosition();

	for (int count=0;count<m_iNumTent;count++)
	{
		m_pTemp=m_ptent[count];
		if (m_pTemp) m_pTemp->Update();
		//informa il tentacolo sulla posizione del player
		m_pTemp->lTargetX=this->lTargetX;
		m_pTemp->lTargetY=this->lTargetY;


	}

	if (dwEnergy<=0)
	{
		if (lpfnExplMulti) lpfnExplMulti(this,8);
		CreateBonus();
		return FALSE;

	}

	return (x>-400 && x<CSBObject::g_iScreenWidth+200 && y>-300 && y<CSBObject::g_iScreenHeight); 
}