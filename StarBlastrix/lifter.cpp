//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  lifter.cpp -- nemico che decolla da terra con scudi laterali estraibili
///////////////////////////////////////////////////////////////////////*/

#include "lifter.h"
#include "sbengine.h"
#include "a32util.h"

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
//stack oggetti attivi
extern CObjectStack g_objStack;
extern CADXSound g_cs;
//p. laser orientabile
extern void PushExplMulti(CADXSprite*,int);
extern CObjectStack g_sNozzleFlamesV;
extern void PushExpl9(LONG x,LONG y);
//p. inseguitori bianchi
extern CObjectStack g_sFwBullet;
//p. piccolo orizzontale
extern CObjectStack g_sEShell5;
extern CPlayer Player1;
extern LONG g_snFire8;
extern LONG g_snNozzle;
extern LONG g_snMech1;

#ifdef _TRACE_
//flag per interruzione/debug
extern BOOL g_bTraceOn;
#endif

LONG m_snMech4=-1,m_snMech5=-1;

IMAGE_FRAME g_imgLifterBody,g_imgLifterLeftShield,g_imgLifterRightShield,g_imgLifterPiston;
BOOL g_bLifterResLoaded=FALSE;

HRESULT LoadLifterRes(CResourceManager* prm)
{
	if (g_bLifterResLoaded) return S_OK;
		
	if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

	CADXFrameManager* fm=prm->GetFrameManager();

	if (NULL==fm) return E_FAIL;

	//immagine corrente
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (FAILED(fm->GrabFrame(&g_imgLifterBody,665,240,761,383,pimg))) return E_FAIL;
	//i due portelli non hanno le stesse dimensioni
	if (FAILED(fm->GrabFrame(&g_imgLifterLeftShield,555,239,600,328,pimg))) return E_FAIL;
	if (FAILED(fm->GrabFrame(&g_imgLifterRightShield,607,239,657,325,pimg))) return E_FAIL;
	//il "pistone" � composto da due pistoni sovrapposti
	if (FAILED(fm->GrabFrame(&g_imgLifterPiston,780,279,844,331,pimg))) return E_FAIL;

	pimg=NULL;

	m_snMech4=g_cs.CreateSound("WAVS\\mech4.wav",3); //chiusura
	m_snMech5=g_cs.CreateSound("WAVS\\mech5.wav",3); //scorrimento porte

	return S_OK;


}

HRESULT CreateLifterStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	if (!pstk) return E_FAIL;

	if (!prm) return E_FAIL;

	CLifter* ps=NULL;
	CMechComponent* pc=NULL;

	if (!pstk) return E_FAIL;
    //carica le risorse per il serpente di tipo 2
	if (FAILED(LoadLifterRes(prm))) return E_FAIL;
	
	if (FAILED(pstk->Create(inumitems))) return E_FAIL;	

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CLifter;
		ps=(CLifter*)pstk->m_objStack[count];
		ps->bActive=FALSE;
		ps->SetGraphicManager(prm->m_lpGm);
		ps->SetFrameManager(prm->m_lpFm);
		ps->SetUpdateFreq(40);
		ps->SetEnergy(25);
		ps->dwPower=10;
		ps->dwScore=8000;
		ps->dwResId=EN_LIFTER;
		ps->lpfnExplode=PushExpl9;
		ps->pfnCollide=PushCollideMetal;
		ps->SetActiveStack(&g_objStack);		
		ps->SetCartridgeStack(NULL); //proiettili rossi	
		ps->SetFireSound(-1);
		
		
		pc=&ps->Frame;

		pc->SetFrameManager(&g_fm);
		pc->CreateComponent(0,0,&g_imgLifterBody,1);
		pc->AddFixCoupling(49,42);//bocca di fuoco frontale sup.
		pc->AddFixCoupling(49,89);//bocca di fuoco frontale inf.
		pc->AddFixCoupling(52,142);//ugello (rivolto in basso)
		int cleft=pc->AddFixCoupling(3,44);//barriera sinistra
		int cright=pc->AddFixCoupling(93,44);; //barriera destra
		pc->AddFixCoupling(0,64); //cannone laterale sinistro
		pc->AddFixCoupling(96,64); //cannone laterale destro
		

		pc=&ps->m_LeftPiston.Frame;
		pc->SetFrameManager(&g_fm);
		ps->m_LeftPiston.SetFrameManager(&g_fm);

		pc->CreateComponent(0,0,&g_imgLifterPiston,1);
		//sx
		pc->AddFixCoupling(60,5); 
		pc->AddFixCoupling(0,5); //qui va attaccato il portello		
		
		pc=&ps->m_RightPiston.Frame;
		pc->SetFrameManager(&g_fm);
		ps->m_RightPiston.SetFrameManager(&g_fm);


		pc->CreateComponent(0,0,&g_imgLifterPiston,1);
		//dx
		pc->AddFixCoupling(0,5);		
		pc->AddFixCoupling(64,5);
	
		pc=&ps->m_LeftShield.Frame;
		pc->SetFrameManager(&g_fm);
		ps->m_LeftShield.SetFrameManager(&g_fm);

		pc->CreateComponent(0,0,&g_imgLifterLeftShield,1);
		pc->AddFixCoupling(9,19);
	
		pc=&ps->m_RightShield .Frame;
		pc->SetFrameManager(&g_fm);
		ps->m_RightShield.SetFrameManager(&g_fm);

		pc->CreateComponent(0,0,&g_imgLifterRightShield,1);
		pc->AddFixCoupling(34,19);

		ps->AttachEnemy(cleft,0,&ps->m_LeftPiston);
		ps->AttachEnemy(cright,0,&ps->m_RightPiston);
		ps->m_LeftPiston.lpfnExplode=PushExpl9;
		ps->m_RightPiston.lpfnExplode=PushExpl9;
		ps->m_LeftPiston.AttachEnemy(1,0,&ps->m_LeftShield);
		ps->m_RightPiston.AttachEnemy(1,0,&ps->m_RightShield);
		ps->m_LeftShield.lpfnExplode=PushExpl9;
		ps->m_RightShield.lpfnExplode=PushExpl9;
		ps->m_LeftShield.dwPower=10;
		ps->m_RightShield.dwPower=10;
		ps->m_LeftPiston.dwPower=10;
		ps->m_RightPiston.dwPower=10;
		ps->SetClosedPos(-16,80);
		ps->SetOpenPos(60,0);//corrisponde con le posizioni iniziali dei giunti (vedi sopra)
		//fumo fermo rispetto al livello
		ps->pfnSmoke=PushSmoke;
		//arma primaria (p. inseguitori)
		ps->SetCartridgeStack(&g_sFwBullet); 
		ps->SetWeapon1(&g_sEShell5);
		ps->SetFireSound(g_snFire8);
		ps->SetNozzleFlameStack(&g_sNozzleFlamesV);
		

	
	}

	return S_OK;

}

void FreeLifterRes(void)
{	
	if (g_bLifterResLoaded)
	{
		g_fm.FreeImgFrame(&g_imgLifterBody);
		g_fm.FreeImgFrame(&g_imgLifterLeftShield);
		g_fm.FreeImgFrame(&g_imgLifterRightShield);
		g_fm.FreeImgFrame(&g_imgLifterPiston);
		g_bLifterResLoaded=FALSE;
		g_cs.FreeSound(m_snMech4);
		g_cs.FreeSound(m_snMech5);
	}
}


//-------------------------------------- CLifter -----------------------------------------------

CLifter::CLifter()
{
	dwClass=CL_ENEMY;
	dwType=38;	
	m_closed_left=m_closed_right=m_open_left=m_open_right;
	pLeftShield=&m_LeftPiston.Frame;
	pRightShield=&m_RightPiston.Frame;	
	m_pNozzleFlame=NULL;	
}

void CLifter::Reset(void)
{	
	CModularEnemy::Reset();
	Frame.SetPosition(x,y);
	Frame.UpdatePosition();
	stLeftShield=stRightShield=SH_CLOSED;
	bGround=TRUE;
	Frame.SetVelocity(180,CSbLevel::g_scroll_speed);
	iStepConfig=0;
	//posizione solo bocche aperte
//	m_LeftPiston.Frame.SetFrameHotSpot(0,0,0,5); 
//	m_RightPiston.Frame.SetFrameHotSpot(0,0,64,5);
	m_LeftPiston.Frame.SetFrameHotSpot(0,0,m_closed_left,5); //chiuso //-16
	m_RightPiston.Frame.SetFrameHotSpot(0,0,m_closed_right,5); //chiuso //80
	stLeftShield=stRightShield=SH_CLOSED;//parte chiuso	
	bBoxActive=FALSE;
	m_prefLeft=m_prefRight=SH_CLOSED;
	ttl=40;
	step_shield=0; //passo aggiornamento barriere
	iUpdateFreq=40;
	step_smoke=0;
	m_bDoFlames=FALSE;
	m_pFlames=NULL;

	if (m_pNozzleFlame)
	{
		m_pFlames=(CAnimatedObject *)m_pNozzleFlame->Pop();
		if (m_pFlames)
		{
			//attacca le fiamme a questo oggetto
			m_pFlames->pParent=this;			
			//acquisisce la posizione relativa dell'ugello
			Frame.GetFrameHotSpot(0,2,&tmpx,&tmpy);
			//imposta la posizione relativa delle fiamme
			m_pFlames->SetPosition(tmpx-18,tmpy);		
		}
	}
	
}

void CLifter::SetNozzleFlameStack(CObjectStack *pstack)
{
	m_pNozzleFlame=pstack;
}

void CLifter::Explode(void)
{
	CModularEnemy::Explode();
};

//Valore della posizione di chiusura delle barriere laterali
void CLifter::SetClosedPos(int left,int right)
{
	m_closed_left=left;
	m_closed_right=right;
}

void CLifter::SetOpenPos(int left,int right)
{
	m_open_left=left;
	m_open_right=right;
}


//Imposta lo stato delle barriere: sh=1 sinistra altrimenti destra
void CLifter::SetShieldState(int sh,SHIELD_STATE state)
{
	SHIELD_STATE* ptr= sh==1 ? &stLeftShield : &stRightShield;

	switch (state)
	{
	case SH_CLOSED:
	case SH_OPEN:

		//stato non impostabile
		break;

	case SH_CLOSING:

		if (*ptr==SH_STEADY || *ptr==SH_OPEN) *ptr=state;
		break;

	case SH_OPENING:

		if (*ptr==SH_STEADY || *ptr==SH_CLOSED) *ptr=state;
		break;

	case SH_STEADY:

		if (*ptr==SH_OPENING || *ptr==SH_CLOSING) *ptr=state;
		break;
	}

}

//shield: 1=sinistra altrimenti destra
void CLifter::UpdateShieldPosition(CADXSprite *pShield,int inc,int openlimit,int closedlimit,SHIELD_STATE &state)
{	

	LONG displx,disply;

	pShield->GetFrameHotSpot(0,0,&displx,&disply);
	
	switch (state)
	{
		case SH_OPENING:
			
			//sx positivo dx negativo
			displx+=inc;
		
			if ((displx<openlimit && inc<0) || (displx>openlimit && inc>0)) 
			{
				displx=openlimit;
				state=SH_OPEN;
			}

			pShield->SetFrameHotSpot(0,0,displx,disply);

			break;


		case SH_CLOSING:

			//sx negativo dx positivo
			displx-=inc;

			if ((displx<closedlimit && inc>0) || (displx>closedlimit && inc<0))
			{
				displx=closedlimit;
				state=SH_CLOSED;
				g_cs.PlaySound(g_snMech1,0,0);
				g_cs.PlaySound(m_snMech4,0,0);
			}

			pShield->SetFrameHotSpot(0,0,displx,disply);

			break;
	}

};


void CLifter::ResetBBox()
{
	rcLimitBox.left=10;
	rcLimitBox.right=CSBObject::g_iScreenWidth-80;
	rcLimitBox.top=-10;
	rcLimitBox.bottom=400;
}


BOOL CLifter::Update(void)
{
	if (++iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;

		iStepConfig++;		
		
		if (--ttl<=0)
		{
			bBoxActive=FALSE;
			Frame.SetVelocity(180,4);
		}
		else
		{

			if (bGround)
			{
				if (iStepConfig>1)
				{
					if (g_fmath.RndFast(2)==1 || Frame.x<80)
					{
						//decollo...
						bGround=FALSE;
						iStepConfig=0;
						Frame.SetVelocity(262,2);
					}
				}				

			}
			else
			{	
				int an=0;
				bBoxActive=TRUE;
				ResetBBox();

				switch (iStepConfig)
				{
				case 1:

					bBoxActive=FALSE;
					Frame.SetVelocity(270,3);
					SetShieldState(1,SH_OPENING); //sx
					SetShieldState(0,SH_OPENING); //dx
					break;

				case 2:
				
					iUpdateFreq=90;
					Frame.SetVelocity(0,0);
					break;

				case 3:							
					
					iUpdateFreq=50;
					if (y>300) Frame.SetVelocity(270,3);
					else Frame.SetVelocity(0,1);
					break;

				case 4:
					
					iUpdateFreq=30;				
					if (y>300) Frame.SetVelocity(270,3);
					else Frame.SetVelocity(0,0);
					break;

				case 5:					
				
					iUpdateFreq=140;
					an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);

					Frame.SetVelocity(an,2);
					break;
				
				case 6:

					iUpdateFreq=100;
					an=g_fmath.RndFast(360);
					Frame.SetVelocity(an,1);
					break;

				case 7:

					iUpdateFreq=50;
					Frame.SetVelocity(0,0);
					break;

				case 8:

					iUpdateFreq=50;
					Frame.SetVelocity(270,3);
					break;			
					
				case 9:

					iUpdateFreq=120;
					iStepConfig=g_fmath.RndFast(6)+2;
					Frame.SetVelocity(90,1);
					break;				

				default:

					iStepConfig=1;
					bBoxActive=TRUE;
					Frame.SetVelocity(0,3);
					break;

				}

				//cambia lo stato desiderato delle barriere se sono entrambe in stato modificabile
				if ((stLeftShield==SH_OPEN || stLeftShield==SH_CLOSED) && (stRightShield==SH_OPEN || stRightShield==SH_CLOSED))
				{		
					
					if (stLeftShield==SH_OPEN && stRightShield==SH_OPEN)
					{
						//if (++iStepFire>=iFireFreq)
						//{
							
							//acquisisce la posizione della bocca di fuoco superiore
							Frame.GetJointPosition(&tmpx,&tmpy,0);
							
							
							CFwBullet *pw=(CFwBullet *)m_Cartridges->Pop();
						
							an=lTargetX<tmpx ? 180 : 0;

							if (pw)
							{
								pw->Reset();
								pw->SetPosition(tmpx,tmpy);
								pw->SetVelocity(an,3);
								pw->SetTarget(&Player1);	
								pw->SetTTL(45); //time to live
								pw->dwClass=CL_ENEMY;
								pw->SetEnergy(1);
								pw->dwPower=10;
								m_ActiveStack->Push(pw);
							}

							Frame.GetJointPosition(&tmpx,&tmpy,1);
						
							pw=(CFwBullet *)m_Cartridges->Pop();

							if (pw)
							{
								pw->Reset();
								pw->SetPosition(tmpx,tmpy);
								pw->SetVelocity(an,3);
								pw->SetTarget(&Player1);	
								pw->SetTTL(45); //time to live
								pw->dwClass=CL_ENEMY;
								pw->SetEnergy(1);
								pw->dwPower=10;
								m_ActiveStack->Push(pw);
							}
						
						//}
					}


					step_shield++;
					int iprob=g_fmath.RndFast(3);

					switch (step_shield)
					{
					case 1:
						
						m_prefLeft=m_prefRight=SH_OPEN;
						break;

					case 2:

						g_cs.PlaySound(m_snMech5,0,0);

						switch (iprob)
						{
						case 0:
							m_prefLeft=SH_CLOSING;
							m_prefRight=SH_CLOSING;
							break;						
						case 1:
							m_prefLeft=SH_OPENING;
							m_prefRight=SH_OPENING;
							break;
						default:
							m_prefLeft=SH_OPENING;
							m_prefRight=SH_CLOSING;
							break;

						}
						break;

					case 3:
						
						g_cs.PlaySound(m_snMech5,0,0);

						m_prefLeft=SH_CLOSING;
						m_prefRight=SH_OPENING;
						break;


					case 4:
						
						g_cs.PlaySound(m_snMech5,0,0);


						if (iprob==1)
						{
							m_prefLeft=SH_OPENING;
							m_prefRight=SH_CLOSING;
						}
						else
						{
							m_prefLeft=SH_CLOSING;
							m_prefRight=SH_OPENING;
						}
						break;

					case 5:

						if (iprob==1)
						{
							m_prefLeft=SH_CLOSING;
							m_prefRight=SH_OPENING;
						}
						else
						{
							m_prefLeft=SH_OPENING;
							m_prefRight=SH_OPENING;
						}
						break;

					case 6:

						g_cs.PlaySound(m_snMech5,0,0);

						m_prefLeft=SH_OPENING;
						m_prefRight=SH_OPENING;
						break;

					case 7:

						g_cs.PlaySound(m_snMech5,0,0);


						m_prefLeft=SH_CLOSING;
						m_prefRight=SH_CLOSING;
						break;	

					default:

						g_cs.PlaySound(m_snMech5,0,0);

						step_shield=1;
						m_prefLeft=SH_CLOSING;
						m_prefRight=SH_CLOSING;
						break;

					}
					
					SetShieldState(1,m_prefLeft);
					SetShieldState(0,m_prefRight);			


				}		
				
				
			}
		
		}
	}


	//sinistra
	UpdateShieldPosition((CADXSprite *)pLeftShield,1,m_open_left,m_closed_left,stLeftShield);	
	//destra
	UpdateShieldPosition((CADXSprite *)pRightShield,-1,m_open_right,m_closed_right,stRightShield);	

	Frame.UpdatePosition();	
	//this->SetPosition(Frame.x,Frame.y);	
	this->x=Frame.x;
	this->y=Frame.y;

	if (++step_smoke>=8)
	{
		step_smoke=0;
		int a=Frame.GetAngle();
		//se sta decollando genera il fumo 
		if (a>=250 && a<=280 && pfnSmoke)
		{
			if (m_pFlames) 
			{
		
				m_pFlames->Update();
				if (!m_bDoFlames) g_cs.PlaySound(g_snNozzle,0,0);

			}

			//acquisisce la posizione dell'ugello
			Frame.GetJointPosition(&tmpx,&tmpy,2);
			
			pfnSmoke(tmpx-5,tmpy-5);
			pfnSmoke(tmpx-5,tmpy+5);

			m_bDoFlames=TRUE;

		}
		else m_bDoFlames=FALSE;
	}
	

	if (bBoxActive)
	{		
		if (y<rcLimitBox.top)
		{
			iStepUpdate=0;
			iUpdateFreq=40;
			Frame.SetVelocity(90,2);
		}
		else if (y>rcLimitBox.bottom)
		{
			iStepUpdate=0;
			iUpdateFreq=40;
			Frame.SetVelocity(270,2);
		}
		else if (x<rcLimitBox.left)
		{
			iStepUpdate=0;
			iUpdateFreq=40;
			Frame.SetVelocity(0,2);
		}
		else if(x>rcLimitBox.right)
		{
			iStepUpdate=0;
			iUpdateFreq=40;
			Frame.SetVelocity(0,2);
		}		

	};	


	if (++iStepFire>=iFireFreq && m_pWeapon1)
	{
		iStepFire=0;


		CSBObject *p=(CSBObject*) m_pWeapon1->Pop();

		if (p)
		{		
			p->SetCurFrame(0);
			p->SetEnergy(1);
			p->dwPower=8;
			p->dwClass=CL_ENFIRE;			
			p->Reset();
			
			if (lTargetX<Frame.x) 
			{
				p->SetVelocity(180,10);
				Frame.GetJointPosition(&tmpx,&tmpy,5); //spara a sinistra
			}
			else 
			{
				p->SetVelocity(0,10);
				Frame.GetJointPosition(&tmpx,&tmpy,6); //spara a destra
			}

			if (abs(lTargetY-tmpy)<40)
			{
				p->SetPosition(tmpx,tmpy);
				m_ActiveStack->Push(p);
				if (m_lFireSound>0) g_cs.PlaySound(m_lFireSound,0,0);
			}
			else p->bActive=FALSE;//lo rimette nello stack

		}

	}

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return (Frame.x>=-150 && Frame.x<=CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<600);
}

void CLifter::Collide(void)
{
	if (lpfnCollide) lpfnCollide(x,y);
}


HRESULT CLifter::Draw(void)
{	

	//i pistoni sono dietro quindi vanno disegnati per primi
	if (m_LeftShield.GetEnergy()>0)
	{
		m_LeftPiston.Frame.SetDisplay(m_pimgOut);
		m_LeftPiston.Frame.RenderToDisplay();
	}

	if (m_RightShield.GetEnergy()>0)
	{
		m_RightPiston.Frame.SetDisplay(m_pimgOut);
		m_RightPiston.Frame.RenderToDisplay();
	}

	//disegna il corpo centrale
	Frame.SetDisplay(m_pimgOut);
	HRESULT hr=Frame.RenderToDisplay();


	//alla fine disegna le due barriere laterali
	if (m_LeftShield.GetEnergy()>0)
	{
		m_LeftShield.Frame.SetDisplay(m_pimgOut);
		m_LeftShield.Frame.RenderToDisplay();
	}

	if (m_RightShield.GetEnergy()>0)
	{
		m_RightShield.Frame.SetDisplay(m_pimgOut);
		m_RightShield.Frame.RenderToDisplay();
	}

	//fiamme ugello
	if (m_pFlames && m_bDoFlames)
	{
		m_pFlames->SetDisplay(m_pimgOut);
		m_pFlames->Draw();
	}
	
	return hr;


}


