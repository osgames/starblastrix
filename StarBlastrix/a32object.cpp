/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  21-07-2004
  --------------------------------------------------------------
  A32OBJECT.CPP
  oggetti e stack di oggetti derivati dalla classe sprite

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/

#include "a32graph.h" //GraphicManager e FrameManager
#include "a32sprit.h" //sprites
#include "a32object.h" //header di questo file

///////////////////////////////////// classe CSBObject //////////////////////////////////////

DWORD CSBObject::dwInstance=0;
int CSBObject::g_iScreenWidth=0;
int CSBObject::g_iScreenHeight=0;
int CSBObject::g_iScreenBottomHeight=0;

CSBObject::CSBObject(void)
{
	bActive=FALSE;
	dwScore=0;
	dwClass=0;
	dwStatus=0;
	dwEnergy=0;
	dwResId=0;
	m_lInitialEnergy=0;
	dwPower=0;
	rcBoundBox.left=0;
	rcBoundBox.top=0;
	rcBoundBox.right=0;
	rcBoundBox.bottom=0;
	lpfnExplode=NULL; 
	lpfnCollide=NULL;	
	dwInstance++;
	lTargetX=0;
	lTargetY=0;
	lGroundX=0;
	lGroundY=0;
	pParent=NULL;
	dwType=0;
	m_zlayer=0;
	xabs=0;
	yabs=0;
}


CSBObject::~CSBObject(void)
{
	dwInstance--;	
}

void CSBObject::Reset(void)
{
	dwEnergy=m_lInitialEnergy;
	pParent=NULL;
}


//----------------------------------- CSBObject::SetEnergy ----------------------------------------
//imposta l'energia corrente

void CSBObject::SetEnergy(LONG energy) 
{
	dwEnergy=energy;
}

//---------------------------------- CSBObject::GetEnergy ------------------------------------------
//energia corrente dell'oggetto

LONG CSBObject::GetEnergy(void)
{
	return dwEnergy;
}

//----------------------------------- CSBObject::IncrEnergy ---------------------------
//Incrementa l'energia
void CSBObject::IncrEnergy(LONG delta) 
{
	dwEnergy+=delta;
}


//------------------------------------ CSBObject::GetIndex ---------------------------

DWORD CSBObject::GetIndex(void)
{
    return dwIndex;
}

//---------------------------------- CSBObject::SetInitialEnergy -------------------------

//imposta l'energia iniziale dell'oggetto	
void CSBObject::SetInitialEnergy(LONG energy) 
{
	if (energy>=0) m_lInitialEnergy=energy;
	else m_lInitialEnergy=0;
}

//----------------------------------- CSBObject::GetType -------------------------------------------
//rende il tipo di classe
DWORD CSBObject::GetType(void)
{
	return dwType;
}

//---------------------------------- CSBObject::SetType -------------------------------------------
//imposta l'identificativo del tipo di oggetto
BOOL CSBObject::SetType(DWORD type)
{
	dwType=type;
	return TRUE;
}

//------------------------------------ CSBObject::GetStackIndex --------------------------------
//restituisce l'indice che l'oggetto ha nello stack attivo
DWORD CSBObject::GetStackIndex(void)
{
	return dwIndex;
}


//----------------------------- UpdateAbsXY--------------------------------
//aggiorna le coordinate assolute dell'oggetto
void CSBObject::UpdateAbsXY(void)
{
	static CSBObject *pobj=NULL;
	
	//oggetto genitore
	pobj=pParent;
	
	//azzera le coordinate assolute
	xabs = x;
	yabs = y; 

	while(pobj)
	{
		//l'oggetto genitore potrebbe a sua volta essere posizionato 
		//relativamente ad un altro
		xabs += pobj->x;
        yabs += pobj->y;
		pobj=pobj->pParent;
	}

}

//----------------------------- GetAbsXY ----------------------------------
//restituisce le coordinate assolute dell'oggetto 

void CSBObject::GetAbsXY(int *ax,int *ay)
{
	
	//aggiorna xabs,yabs
	UpdateAbsXY();

	*ax=xabs;
	*ay=yabs;

}

//------------------------------------- CSBObject::Draw --------------------------------------------

//disegna l'oggetto tenendo conto del sistema di riferimento
HRESULT CSBObject::Draw(void)
{
	static IMAGE_FRAME_PTR pimg;
	static LPDIRECTDRAWSURFACE7 lpDDS;
    
	UpdateAbsXY();

	pimg=frames[cur_frame];

	if (pimg)
	{	
		rcBoundBox.left=xabs;
		rcBoundBox.top=yabs;
		rcBoundBox.right=xabs+pimg->width;
		rcBoundBox.bottom=yabs+pimg->height;
		lpDDS=m_pimgOut->surface;			
		return lpDDS->Blt(&rcBoundBox,pimg->surface,NULL,DDBLT_KEYSRC,NULL);
	}	

	else return E_FAIL;		
}

//---------------------------------- CSBObject::Update ------------------------------------------------

BOOL CSBObject::Update(void)
{	

	this->UpdatePosition();
	if (x<-120 || x>CSBObject::g_iScreenWidth  || y<-120) return FALSE;
	if (y>CSBObject::g_iScreenHeight-CSBObject::g_iScreenBottomHeight) 
	{	
		//quando impatta col terreno esplode
		if (lpfnExplode && CSBObject::g_iScreenBottomHeight) lpfnExplode(x,y);
		return FALSE;
	}
	else if (dwEnergy<=0) return FALSE; //quando l'oggetto non ha piu' energia rende false 
	else return TRUE;

}

//------------------------------- CSBObject::DoCollisionWith ----------------------------------------

//Controlla se l'oggetto collide con l'oggetto pobj
//se avviene la collisione allora provvede a diminuire l'energia dei due oggetti
//rende true se la collisione � avvenuta

BOOL CSBObject::DoCollisionWith(CSBObject *pobj)
{	

	if (pParent)
	{		
		//l'oggetto � vincolato ad un altro

		LPRECT lp2,lp1;	
		IMAGE_FRAME_PTR pimg=frames[cur_frame];		

		UpdateAbsXY();

		rcBoundBox.left=xabs;
		rcBoundBox.top=yabs;
		rcBoundBox.right=xabs+pimg->width;
		rcBoundBox.bottom=yabs+pimg->height;
		
		lp1=&rcBoundBox;			 
		
		//l'oggetto � inscritto in un rettangolo (bounding rectangle)
		lp2=&pobj->rcBoundBox;	

		if (lp1 && lp2)
		{
			if       ((lp1->left<= lp2->right) &&
					 (lp1->right>= lp2->left) &&
					 (lp1->top<= lp2->bottom) &&
					 (lp1->bottom >= lp2->top))
			{
				Damage(pobj);
				return TRUE;
			}
		}

	}

	else
	{	

		//nota: alcuni oggetti non hanno uno sprite base vedi CSnake2 ecc...
		if (pobj->GetMaxFramesIndex()>=0 && !pobj->pParent)
		{
			if (pobj->pParent)
			{
				int ax,ay,ox,oy;

				ox=pobj->x;
				
				oy=pobj->y;

				pobj->UpdateAbsXY();
				
				pobj->GetAbsXY(&ax,&ay);

				pobj->x=ax;pobj->y=ay;
				
				if (this->DetectCollision(pobj))
				{
					pobj->x=ox;
					pobj->y=oy;
					Damage(pobj);
					return TRUE;

				}

				pobj->x=ox;
				pobj->y=oy;
				
			}
			//usa la funzione di collisione della classe sprite
			else if (this->DetectCollision(pobj))
			{	
				Damage(pobj);
				return TRUE;
			}
		}

	}

	return FALSE; //bCollide; 

}

//--------------------------------------- CSBObject::Damage --------------------------------

void CSBObject::Damage(CSBObject *pobj,float perc)
{
	//diminusce l'energia dei due oggetti che hanno fatto la collisione
	//Quando due oggetti si scontrano, all'energia dell'uno viene sottratto il potere
	//distruttivo dell'altro
		
	dwEnergy -= (LONG)(pobj->dwPower*perc);
	
	pobj->dwEnergy -= dwPower;
										
	if ((pobj->dwEnergy) <= 0)
	{		
		pobj->Explode(); //fa esplodere l'oggetto con cui si � confrontato
	}

	if ((dwEnergy) <=0)
	{	
	    UpdateAbsXY();
		Explode(); //fa esplodere questo oggetto
	}

	pobj->Collide();
	this->Collide();
}

//--------------------------------------- CSBObject::Explode ---------------------------------------------

//provoca l'esplosione dell'oggetto

void CSBObject::Explode(void)
{
	if (lpfnExplode) lpfnExplode(xabs,yabs);
}

//collisione
void CSBObject::Collide(void)
{
	if (lpfnCollide) lpfnCollide(xabs,yabs);
}


///////////////////////////////// classe CObjectStack ///////////////////////////////////////////

//inizializza lo stack
CObjectStack::CObjectStack(void)
{

	m_dwStackTop=0; //elemento libero corrente
    m_dwStatus=0;
	m_dwNumElems=0;

	//for (dwCount=0;dwCount<MAX_OBJECTS;dwCount++) m_objStack[dwCount]=NULL;
	m_objStack=NULL;

}

//inizializza lo stack
CObjectStack::~CObjectStack(void)
{
	//attenzione: prima di rilasciare lo stack si devono anche distruggere i sigoli oggetti nella lista
	Clear(FALSE); //rilascia gli oggetti	
}

//-------------------------- CObjectStack::Clear --------------------------------------------------

//distrugge la lista 
//se bDestroyObjects � true allora distrugge anche i singoli oggetti presenti nella lista
void CObjectStack::Clear(BOOL bDestroyObjects)
{
	DWORD dwCount;

	if (!this->m_dwStatus || this->m_objStack==NULL) return;
    
	if (bDestroyObjects)
	{
		for (dwCount=0;dwCount<m_dwNumElems;dwCount++)
		{
			SAFE_DELETE(m_objStack[dwCount]); //ditrugge il singolo oggetto
		}	
	}

	m_dwStackTop=0; //elemento libero corrente	
	SAFE_DELETE_ARRAY(this->m_objStack); //ditrugge il vettore di puntatori
	m_dwNumElems=0;
}


//----------------------------------- CObjectStack::Delete --------------------------------------------------

//Elima un elemento dalla lista 
//(Attenzione! imposta solo il puntatore su null ma non distrugge l'oggetto)

BOOL CObjectStack::Delete(DWORD Index)
{	
	if (Index<m_dwNumElems && m_objStack[Index])
	{
		//rende l'oggetto di nuovo disponibile
		m_objStack[Index]->bActive=FALSE;
		m_objStack[Index]->dwStatus=0;
		m_objStack[Index]=NULL;	
		
		if (Index==m_dwStackTop && m_dwStackTop>0) 
		{
			m_dwStackTop--;
			//sposta il top indietro
			while(!m_objStack[m_dwStackTop] && m_dwStackTop>0)
			{
				m_dwStackTop--;
			}

		}
	}
	
	else return FALSE;
	
	return TRUE;
}

//------------------------------------ CObjectStack::DeleteAll------------------------------------

void CObjectStack::DeleteAll(void)
{
	for (DWORD count=0;count<m_dwNumElems;count++) this->Delete(count);
}

//------------------------- CObjectStack::Push ---------------------------------------------------

LONG CObjectStack::Push(CSBObject *pobj)
{
	if(!pobj) return -1;

	//cerca il primo elemento libero
	for (DWORD dwCount=0;dwCount<m_dwNumElems;dwCount++)
	{
		//cerca il primo posto vuoto
		if (!m_objStack[dwCount])
		{
			//inserisce l'oggetto nella lista copiandolo dal riferimento			
			pobj->dwStatus=1;
			m_objStack[dwCount]=pobj;			
			pobj->dwIndex=dwCount; //memorizza l'indice anche nell'oggetto (utile per debug)
			if (m_dwStackTop<dwCount) m_dwStackTop=dwCount;
		
			return dwCount; //restituisce l'indice in cui ha inserito l'oggetto
		}
	}
	
	pobj->bActive=FALSE; 
	return -1; //non ha inserito l'oggetto	
}


//------------------------------------ CObjectStack::Pop -----------------------------------------
//Restituisce il primo elemento non attivo
CSBObject *CObjectStack::Pop(void)
{
	register DWORD dwCount;

	for (dwCount=0;dwCount<m_dwNumElems;dwCount++)
	{
		if (m_objStack[dwCount])
		{
			if (!m_objStack[dwCount]->bActive)
			{
				m_objStack[dwCount]->bActive=TRUE; //l'oggetto � stato estratto 
				return m_objStack[dwCount];					
			}
		}
	}

	return NULL;
}

//------------------------------------- CObjectStack::GetTop ----------------------------------------

DWORD CObjectStack::GetTop(void)
{
	return m_dwStackTop;
}

//------------------------------------- CObjectStack::Create ----------------------------------------
//crea lo stack di puntatori
HRESULT CObjectStack::Create(DWORD dwNumElems)
{
	DWORD dwCount;

	this->m_objStack = new OBJECT_PTR[dwNumElems];

	if (this->m_objStack) 
	{
		m_dwStatus=1;
		m_dwNumElems=dwNumElems;

		for (dwCount=0;dwCount<dwNumElems;dwCount++)
		{
			m_objStack[dwCount]=NULL;		

		}

	    m_dwStackTop=0;		
		return S_OK;
	}

	else 
	{
		m_dwStackTop=0;
		m_dwNumElems=0;	
		return E_FAIL; //errore di allocazione di memoria
	}
}

//-------------------------------------- CObjectStack::ItemCount -------------------------------

DWORD CObjectStack::ItemCount(void)
{
	return m_dwNumElems;
}


