//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  antia3.h -- torretta contraerea doppia
///////////////////////////////////////////////////////////////////////*/

#ifndef _SB_ANTI_AIRCRAFT3_
#define _SB_ANTI_AIRCRAFT3_

#include "a32graph.h"
#include "a32sprit.h"
#include "a32object.h"
#include "mech.h"
#include "resman.h"
#include "enemy.h"

//contraerea con doppio cannone
class CAntiAircraft3: public CMechAssembly
{
private:

	CObjectStack *m_pSmoke;

protected:

	int lCurX;
	CMechComponent m_Cannon;	
	CMechComponent m_Base;
	int m_iLeftCannon,m_iRightCannon;
	int m_jBase; //id giunto alla base usato per vincolarla ad un veicolo

public:

	CAntiAircraft3(void);
	~CAntiAircraft3(void);
	BOOL DoCollisionWith(CSBObject *pobj);
	void Reset(void);
	BOOL Update(void);
	HRESULT Draw(void);
	HRESULT Create(IMAGE_FRAME_PTR pimgTurret,IMAGE_FRAME_PTR pimgTurretDamaged,IMAGE_FRAME_PTR pimgCannon,int iCannonFrames,int ifrom_angle,int jointx,int jointy,int joint_basex,int joint_basey,int joint_basex1,int joint_basey1);
	HRESULT AddCannonsHotSpots(int xleft,int ylefy,int xright,int yright);
	HRESULT SetSmokeStack(CObjectStack *pStack);
	HRESULT LinkToVehicle(CMechComponent *pParent,int iParentJoint);	
	void SetEnergy(LONG energy);

};


//carica le risorse del ragno
HRESULT LoadAA3Resources(CResourceManager *prm);
HRESULT FreeAA3Resources(CADXFrameManager *pfm);
HRESULT CreateAA3(CAntiAircraft3 *pAA3,CResourceManager *prm);
HRESULT CreateAA3Stack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags);
void FreeAA3Stack(CObjectStack *pstk);


#endif