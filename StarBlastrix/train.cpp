//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  train.h -- treno
///////////////////////////////////////////////////////////////////////*/

#include "sbengine.h"
#include "grndauxen.h"
#include "resman.h"
#include "modenemy.h"
#include "train.h"

#ifdef _TRACE_

extern BOOL g_bTraceOn;

#endif

extern CADXFrameManager g_fm;
extern CADXSound g_cs;
extern CADXFastMath g_fmath;
extern CObjectStack g_objStack;
extern CObjectStack g_sEShell11;
extern LONG g_snPlLaser;
extern LONG g_snMissile;
extern CObjectStack g_sSMissile;

#define MAX_WAGON 5

CObjectStack m_stkWagon[MAX_WAGON];

IMAGE_FRAME m_imgWagon[MAX_WAGON];

static BOOL m_bTrainResLoaded=FALSE;

LONG m_snTrain=-1;

HRESULT CreateTrainStack(int numitems,CObjectStack *pstack,CResourceManager* prm,int flags)
{	
	if (FAILED(LoadTrainRes(prm))) return E_FAIL;
	if (!pstack) return E_FAIL;

	CTrain* p=NULL;
	pstack->Create(numitems);

	for (int i=0;i<numitems;i++)
	{

		pstack->m_objStack[i]=new CTrain();
		p=(CTrain *)pstack->m_objStack[i];
		p->bActive=FALSE;
		p->SetFrameManager(&g_fm);
		p->SetActiveStack(&g_objStack);
		p->SetInitialEnergy(200);
		p->AddFrame(&m_imgWagon[3]);
		p->Frame.SetFrameManager(&g_fm);
		p->Frame.CreateComponent(0,0,&m_imgWagon[3],1);
		p->SetCurFrame(0);
		p->SetInitialEnergy(80);
		p->Frame.AddFixCoupling(0,46);
		p->Frame.AddFixCoupling(107,46);
		p->lpfnMultiExplosion=PushExplMulti;

	}

	return S_OK;
}

//------------------------------------- CTrain --------------------------------------------

CTrain::CTrain(void)
{
	dwType=36;
	dwResId=EN_TRAIN;
	dwClass=CL_ENEMY;
	m_pCurWagon=this;
	dwScore=10000;
}

//atacca un vagone al treno
HRESULT CTrain::AttachWagon(enum wagon_enum wagonid)
{
	static CWagon* p=NULL;

	p=(CWagon*) m_stkWagon[wagonid].Pop();

	if (p)
	{

		p->Unlink();

		p->Frame.Unlink();
		p->Reset();
		p->bActive=TRUE;
		//attacca all'ultimo vagone corrente (gancio di destra) il nuovo vagone (gncio di sinistra)
		if (0!=(m_pCurWagon->AttachEnemy(1,0,p)))
		{
			//l'utimo vagone � ora p
			m_pCurWagon=p;
			return S_OK;
		}				

	}

	return E_FAIL;
}

void CTrain::Reset(void)
{
	CModularEnemy::Reset();
	Frame.SetPosition(x,y); //la posizione iniziale della frame coincide con quella del'oggetto che la incapsula
	Frame.UpdatePosition();
	Frame.SetVelocity(180,CSbLevel::g_scroll_speed); //inizialmente fermo
	m_iStartDelay=8;
	iUpdateFreq=90;
	iStepConfig=0;
	iStepConfig=0;
	dwScore=10000;
	m_AI=0; //algoritmo AI
	m_pCurWagon=this;
	m_lgrndx=0;
	 //punto iniziale (serve a non farlo andare troppo avanti)
	//stacca eventuali elementi collegati
	this->Unlink();

	switch(m_dwFlags)
	{
	case 0:
		
		AttachWagon(base_wagon);		
		AttachWagon(base_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(base_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(cannon_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(locom_wagon);
		AttachWagon(locom_wagon);
		AttachWagon(missile_wagon);

		break;

	case 1:

		//treno corto
		AttachWagon(missile_wagon);	
		m_iStartDelay=4;
		m_AI=1; //segue il player
		break;

	case 2:
		
		AttachWagon(base_wagon);
		AttachWagon(cannon_wagon);
		AttachWagon(cannon_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(base_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(tank_wagon);
		AttachWagon(base_wagon);
		AttachWagon(cannon_wagon);
		AttachWagon(cannon_wagon);
		AttachWagon(missile_wagon);
		AttachWagon(base_wagon);
		AttachWagon(locom_wagon);

		break;


	default:


		break;

	}
}

HRESULT CTrain::Draw(void)
{
	return CModularEnemy::Draw();
}

BOOL CTrain::UpdateAI(void)
{
	BOOL bres=FALSE;

#ifdef _TRACE_

	if (g_bTraceOn)
	{
		g_bTraceOn=g_bTraceOn;
	}

#endif

	//esegue l'aggiornamento di tutti gli oggetti collegati
	bres |= CModularEnemy::UpdateAI();
	
	iStepUpdate++;

	if (m_lgrndx==0) m_lgrndx=lGroundX;

	if (iStepUpdate>iUpdateFreq)
	{		
		iStepUpdate=0;

		switch(m_AI)
		{
		case 1:

			//segue il player
			if (iStepConfig==0)
			{
				//inizialmente fermo
				m_iStartDelay--;
				if (m_iStartDelay<=0)
				{
					iStepConfig=1;
					iUpdateFreq=26;
					Frame.SetVelocity(0,1); //avvia il treno in avanti	
					g_cs.PlaySound(m_snTrain,0,0);
				}

			}
			else
			{

				if (lTargetX>x+100)
				{
					Frame.SetVelocity(0,1);
				}
				else if (lTargetX<x-50)
				{
					Frame.SetVelocity(180,1);
				}
				else 
				{
					SetVelocity(0,0);
					g_cs.StopSound(m_snTrain);
				}

				if (x>CSBObject::g_iScreenWidth) 
				{
					Frame.SetVelocity(180,1);
				}
				else if (x<-30) Frame.SetVelocity(0,1);
				
			}

			break;

			default:
		
			switch(iStepConfig)
			{
				case 0:
				
					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{
						iStepConfig=1;
						Frame.SetVelocity(0,1); //avvia il treno in avanti
						m_iStartDelay=10;
						g_cs.PlaySound(m_snTrain,0,0);
					}
					break;

				case 1:

					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{
						iStepConfig=2;
						Frame.SetVelocity(180,CSbLevel::g_scroll_speed); //fermo
						m_iStartDelay=5;
						g_cs.StopSound(m_snTrain);
					}
					break;

				case 2:

					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{
						iStepConfig=3;
						Frame.SetVelocity(0,0);	
						g_cs.PlaySound(m_snTrain,0,0);
						m_iStartDelay=6;

					}

				case 3:

					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{					
						iStepConfig=4;
						Frame.SetVelocity(180,CSbLevel::g_scroll_speed); 
						g_cs.StopSound(m_snTrain);
						m_iStartDelay=2;

					}

				case 4:

					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{
						iStepConfig=5;
						Frame.SetVelocity(180,CSbLevel::g_scroll_speed+1); 
						g_cs.PlaySound(m_snTrain,0,0);
						m_iStartDelay=3;

					}

				case 5:

					m_iStartDelay--;
					if (m_iStartDelay<=0)
					{
						iStepConfig=5;
						Frame.SetVelocity(180,CSbLevel::g_scroll_speed); 
						m_iStartDelay=4;
						g_cs.StopSound(m_snTrain);

					}

					break;

			}

			break;

		}//fine switch m_AI	
		
		if (abs(lGroundX-m_lgrndx)>3600)
		{
			//limita la posizione del treno(dal punto in cui viene resettato al 
			//punto di uscita non ci deve essere piu' dello spazio consentito (va fuori dai binari!)
			Frame.SetVelocity(180,CSbLevel::g_scroll_speed); 
			g_cs.StopSound(m_snTrain);
			
		}

	}

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	return ((x>-300 && x<CSBObject::g_iScreenWidth+300 && y>-200 && y<CSBObject::g_iScreenHeight+100) || bres);
	
}

BOOL CTrain::Update(void)
{
	//nota bene: per evitare l'effetto "sfarfallio" dei moduli collegati
	//si deve prima aggiornare i moduli collegati e poi aggiornare la posizione della root
	BOOL bres=UpdateAI();		
	//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
	Frame.UpdatePosition();
	this->x=Frame.x;
	this->y=Frame.y;		
	return bres;
			
}

//cpx1,cpy1 e cpx2 e cpy2 sono le coordinate dei ganci anteriore e posteriore

void CreateWagon(CWagon* p,IMAGE_FRAME_PTR pimg,int cpx1,int cpy1,int cpx2,int cpy2)
{
	p->Release(); //rimuove le frame
	p->Frame.Release(); //dealloca tutti i link
	p->SetFrameManager(&g_fm);
	p->Frame.SetFrameManager(&g_fm);
	p->Frame.CreateComponent(0,0,pimg,1);
	p->SetCurFrame(0);
	p->SetInitialEnergy(80);
	p->Frame.AddFixCoupling(cpx1,cpy1);
	p->Frame.AddFixCoupling(cpx2,cpy2);
	p->dwScore=1000;
	p->bActive=FALSE;
	p->SetActiveStack(&g_objStack);
	p->lpfnMultiExplosion=PushExplMulti;
}

HRESULT LoadTrainRes(CResourceManager *prm)
{
	if(m_bTrainResLoaded) return S_OK;

	if (!(4==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data6.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data6.vpx"),4))) return E_FAIL;
	}

	//immagine corrente
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (FAILED(g_fm.GrabFrame(&m_imgWagon[0],3,11,111,51,pimg))) return E_FAIL; //base
	if (FAILED(g_fm.GrabFrame(&m_imgWagon[1],151,7,257,97,pimg))) return E_FAIL; //missile
	if (FAILED(g_fm.GrabFrame(&m_imgWagon[2],271,25,379,96,pimg))) return E_FAIL; //cannone verticale
	if (FAILED(g_fm.GrabFrame(&m_imgWagon[3],6,129,114,194,pimg))) return E_FAIL; //locomotiva
	if (FAILED(g_fm.GrabFrame(&m_imgWagon[4],138,125,247,197,pimg))) return E_FAIL; //silos
	

	int i;
	CWagon* p=NULL;

	m_stkWagon[base_wagon].Create(10);

	for (i=0;i<(int)m_stkWagon[base_wagon].ItemCount();i++)
	{
		m_stkWagon[base_wagon].m_objStack[i]=new CWagon();
		p=(CWagon *)m_stkWagon[base_wagon].m_objStack[i];
		CreateWagon(p,&m_imgWagon[0],0,21,107,21);	
	}

	m_stkWagon[missile_wagon].Create(6);

	for (i=0;i<(int)m_stkWagon[missile_wagon].ItemCount();i++)
	{
		m_stkWagon[missile_wagon].m_objStack[i]=new CWagon();
		p=(CWagon *)m_stkWagon[missile_wagon].m_objStack[i];
		CreateWagon(p,&m_imgWagon[1],0,71,107,71);	
		p->SetCartridgeStack(&g_sSMissile);
		p->Frame.AddFixCoupling(40,6);
		p->SetWeapon(15,225,2,g_snMissile,2);
		p->iStartFireFreq=200;
	}
	
	m_stkWagon[cannon_wagon].Create(6);

	for (i=0;i<(int)m_stkWagon[cannon_wagon].ItemCount();i++)
	{
		m_stkWagon[cannon_wagon].m_objStack[i]=new CWagon();
		p=(CWagon *)m_stkWagon[cannon_wagon].m_objStack[i];		
		CreateWagon(p,&m_imgWagon[2],0,52,107,52);		
		p->SetCartridgeStack(&g_sEShell11);
		p->Frame.AddFixCoupling(48,0);//bocca del cannone
		p->iStartFireFreq=120;
		p->SetWeapon(6,270,2,g_snPlLaser,5);
	}

	m_stkWagon[locom_wagon].Create(6);

    for (i=0;i<(int)m_stkWagon[locom_wagon].ItemCount();i++)
	{
		m_stkWagon[locom_wagon].m_objStack[i]=new CWagon();
		p=(CWagon *)m_stkWagon[locom_wagon].m_objStack[i];
		CreateWagon(p,&m_imgWagon[3],0,46,107,46);
	}

	m_stkWagon[tank_wagon].Create(10);

	for (i=0;i<(int)m_stkWagon[tank_wagon].ItemCount();i++)
	{
		m_stkWagon[tank_wagon].m_objStack[i]=new CWagon();
		p=(CWagon *)m_stkWagon[tank_wagon].m_objStack[i];
		CreateWagon(p,&m_imgWagon[4],0,52,107,52);
	}
	
	m_snTrain=g_cs.CreateSound("WAVS\\train.wav",1);

	m_bTrainResLoaded=TRUE;

	return S_OK;

}


void FreeTrainRes(void)
{
	if (m_bTrainResLoaded)
	{		

		for (int i=0;i<MAX_WAGON;i++)
		{

			g_fm.FreeImgFrame(&m_imgWagon[i]);
			m_stkWagon[i].Clear(TRUE);

		}

		g_cs.FreeSound(m_snTrain);

		m_bTrainResLoaded=FALSE;
	}

}

//-------------------------------------------- CWagon ----------------------------------------------

CWagon::CWagon(void)
{
	dwClass=CL_ENEMY;
	SetWeapon(0,0,0,0,0);
	iStartFireFreq=120;
}


void CWagon::Reset(void)
{
	CModularEnemy::Reset();
	dwPower=10;
	SetVelocity(0,0);
	iFireFreq=iStartFireFreq;
	iUpdateFreq=40;
	m_burst_cnt=0;
}

BOOL CWagon::UpdateAI(void)
{
	BOOL bres=FALSE;

    //esegue l'aggiornamento di tutti gli oggetti collegati
	bres |= CModularEnemy::UpdateAI();

	if (dwEnergy<=0) return FALSE;

	if (iStepFire++>=iFireFreq) 
	{
		iStepFire=0;		

		if (m_burst_cnt<=0)
		{
			m_burst_cnt=m_burst;
			iFireFreq=g_fmath.RndFast(80)+iStartFireFreq;
		}
		else iFireFreq=10;

		m_burst_cnt--;		
	
		if (m_Cartridges && m_ActiveStack)
		{	

			CSBObject *shell=m_Cartridges->Pop();

			if (shell && m_shell_angle!=180)
			{
				int xf=0,yf=0;
				shell->dwClass=CL_ENFIRE;
				Frame.GetCouplingPos(m_shell_hot_spot,&xf,&yf);
				
				xf+=x;
				yf+=y;

				shell->SetCurFrame(m_shell_frame);
				shell->SetPosition(xf,yf);
				shell->SetEnergy(1);
				shell->dwPower=8;
				shell->SetVelocity(m_shell_angle,10);
				m_ActiveStack->Push(shell);
				g_cs.PlaySound(m_weapon_sound,0,0);

			}
		}

		

	}

	return ((Frame.x>-300 && Frame.x<CSBObject::g_iScreenWidth+200 && Frame.y>-200 && Frame.y<CSBObject::g_iScreenHeight) || bres);

}


BOOL CWagon::Update(void)
{

	//aggiorna la posizione della frame e di tutta la catena di oggetti collegata
	Frame.UpdatePosition();
	this->x=Frame.x;
	this->y=Frame.y;

	//algoritmo AI	
	return UpdateAI();

}


void CWagon::SetWeapon(int shell_frame,int shell_angle,int hot_spot,LONG sound,int burst)
{
	m_shell_frame=shell_frame;
	m_shell_angle=shell_angle;
	m_shell_hot_spot=hot_spot;
	m_burst=burst;
	m_weapon_sound=sound;
}