/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
*/

#define WIN32_LEAN_AND_MEAN   //se si inserisce questa definizione il file windows.h

#include "ccharset.h"   

#ifdef _DEBUG_
#include <crtdbg.h>
#endif

//--------------------- CADXCharSet ------------------------------------------------

//Costruttore principale
CADXCharSet::CADXCharSet(void)
{
	//resetta i membri privati
	m_lCharWidth=0;   //larghezza ed altezza in pixel di ogni carattere
	m_lCharHeight=0;
	m_lNumCharX=0;
	m_lNumCharY=0;
	m_lVertexGridX=0;
	m_lVertexGridY=0;
	m_iStatus=0; //diventa 1 quando si imposta un framemanager corretto e 2 quando ci sono dei caratteri caricati
    m_iScale=100;
    m_lpFm=NULL;
	m_lpGm=NULL;
	m_iFirstAscii=48; //lo 0 � il primo carattere della tabella per default
	m_lCharSpacing=0;

}

//secondo costruttore
CADXCharSet::CADXCharSet(CADXFrameManager *fm)
{
	if (fm)
	{
		if (fm->GetStatus() >0)
		{
			m_lpFm=fm;
			m_iStatus=1; //frame manager impostato
		}
	}

}


//---------------------------- ~CADXCharSet ------------------------

CADXCharSet::~CADXCharSet(void)
{
	m_iStatus=0;
	FreeCharTable();
}


//----------------------- SetScale ------------------------------------------------------------

//Imposta la scala dei caratteri.
//Se questa viene impostata prima di caricarli, i caratteri vengono ridimensionati
//100 indica che l'immagine non viene scalata
//al momento del caricamento dal bitmap

HRESULT CADXCharSet::SetScale(int iScale)
{
	if (iScale > 0) 
	{
		m_iScale=iScale;
		return S_OK;
	}

	return E_FAIL;
}

//-------------------- GetScale -----------------------------------------------------------------

int CADXCharSet::GetScale(void)
{
	return m_iScale;
}

//----------------------- SetCharSpacing ------------------------------
//Imposta la distanza fra la frame di un carattere e la successiva
HRESULT CADXCharSet::SetCharSpacing(LONG lcs)
{	
	m_lCharSpacing=lcs;	

	return S_OK;
}

//------------------------GetCharSpacing ---------------------------------------------

//Restituisce la distanza fra un carattere e l'altro
LONG CADXCharSet::GetCharSpacing(void)
{
	return m_lCharSpacing;
}

//-------------------------- GetCharWidth --------------------------

//Restituisce la larghezza dei caratteri caricati
LONG CADXCharSet::GetCharWidth(void)
{
	if (this->m_iStatus == 2) return m_lCharWidth;
	else return -1;

}

//------------------------------- GetTextWidth --------------------------
//restituisce la larghezza di una stringa
LONG CADXCharSet::GetTextWidth(TCHAR *szText)
{
	DWORD dwCnt=0;
	TCHAR *pch;
	if (!szText) return 0L;

	else
	{
		pch=szText;
		//calcola la lunghezza della stringa
		
		while (*pch != 0x0)
		{		
			pch ++;
			dwCnt++;
		}

		pch=NULL;

		//restituisce la lunghezza
		return dwCnt * (m_lCharWidth + m_lCharSpacing); 
	}
}
//---------------------------- GetcharHeight ----------------------

LONG CADXCharSet::GetCharHeight(void)
{
	if (this->m_iStatus == 2) return m_lCharHeight;
	else return -1;
}

//--------------------- LoadCharSet --------------------------------------------------------------

HRESULT CADXCharSet::LoadCharSet(TCHAR *szBmp,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY) //carica i caratteri grabbandoli da un bitmap
{
	IMAGE_FRAME imgChar;
	HRESULT hr;

	hr=m_lpFm->CreateImgFrame(&imgChar,szBmp);
	
	if (FAILED(hr))
	{
		WriteErrorFile(TEXT("LoadCharSet:impossibile caricare il bitmap %s"),szBmp);

	}
    else
	{
		hr=LoadCharSet(&imgChar,lLeft,lTop,lCharWidth,lCharHeight,lCharX,lCharY);

	}	

	m_lpFm->FreeImgFrame(&imgChar);

	return hr;

}

//Carica il set di caratteri da un file VPX
//dwImg � l'indice della frame da caircare dal file VPX
HRESULT CADXCharSet::LoadCharSet(TCHAR *szVPX,DWORD dwImg,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY) //carica i caratteri grabbandoli da un file compresso VPX
{
	IMAGE_FRAME imgChar;
	HRESULT hr;
	
	hr=m_lpFm->CreateImgFrameFromVPX(&imgChar,szVPX,dwImg);

	if (FAILED(hr))
	{
		WriteErrorFile(TEXT("LoadCharSet:impossibile caricare la frame n� %d dal file %s"),dwImg,szVPX);


	}

	else

	{
		//carica il set dall'immagine creata
		hr=LoadCharSet(&imgChar,lLeft,lTop,lCharWidth,lCharHeight,lCharX,lCharY);
	}

	m_lpFm->FreeImgFrame(&imgChar);

	return hr;	
}

//Carica i caratteri grabbandoli da un'immagine gi� in memoria
HRESULT CADXCharSet::LoadCharSet(IMAGE_FRAME_PTR img,LONG lLeft,LONG lTop,LONG lCharWidth,LONG lCharHeight,LONG lCharX,LONG lCharY)
{

	RECT rcGrab;
    int iCurIndex;
	HRESULT hr=S_OK;
	HRESULT hrErr=S_OK;
	IMAGE_FRAME img1;

	if (img == NULL) 
	{
		WriteErrorFile(TEXT("LoadCharSet : l'immagine non � valida !"));
		return E_FAIL;
	}

	if (lLeft<0 || lTop<0 || lLeft>(LONG)img->width || lTop>(LONG)img->height)
	{
		WriteErrorFile(TEXT("LoadCharSet : le coordinate del vertice in alto a sx della tabella caratteri non sono valide."));
		return E_FAIL;
	}

	if (lCharWidth<=0 || lCharHeight <=0)
	{
		WriteErrorFile(TEXT("LoadCharSet : dimensioni del carattere non valide"));
		return E_FAIL;
	}

	if (lCharX <=0 || lCharY <=0)
	{
		WriteErrorFile(TEXT("Numero di caratteri in orizzontale e verticale non validi!"));
		return E_FAIL;
	}

	//se ci sono dei caratteri gi� caricati li cancella
	if (this->GetStatus() > 1) FreeCharTable();

	
	//controlla se l'immagine � stata caricata prima di procedere
	if (img->status)
	{

		iCurIndex=m_iFirstAscii; //codice ascii del primo carattere

		
		for (LONG cy=0;cy<lCharY;cy++)
		{
			rcGrab.top=cy*lCharHeight+lTop+1;
			rcGrab.bottom=rcGrab.top+lCharHeight; //altezza del rettangolo di grabbing

			for (LONG cx=0;cx<lCharX;cx++)
			{
				//definisce il rettangolo di grabbing per il carattere
				rcGrab.left=cx*lCharWidth+lLeft+1;
				rcGrab.right=rcGrab.left+lCharWidth;

				img1.status=0;
				img1.surface=NULL;

				hr=m_lpFm->GrabFrame(&img1,&rcGrab,img);

                if (FAILED(hr))
				{
					WriteErrorFile("LoadCharSet: errore acquisendo la frame, immagine sorgente=%X grab rect(l,t,r,b)=%d,%d,%d,%d",img,rcGrab.left,rcGrab.top,rcGrab.right,rcGrab.bottom);
					hrErr=hr; //ultimo risultato 

				}

				else if (iCurIndex<256)
				{
					if (m_iScale==100)
					{
						hr=m_lpFm->DuplicateImgFrame(&m_imgChars[iCurIndex],&img1);

						if (FAILED(hr)) 
						{
							WriteErrorFile("LoadCharSet: errore duplicando la frame per il carattere %d",iCurIndex);
							hrErr=hr;
						}
					}
					else
					{
						hr=m_lpFm->CreateScaledFrame(m_iScale,&m_imgChars[iCurIndex],&img1);

						if (FAILED(hr))
						{
							WriteErrorFile("LoadCharSet: errore scalando la frame del carattere %d con scala %d",iCurIndex,m_iScale);
							hrErr=hr;							
						}

					}

				
				}
				
				//incrementa il codice ASCII del carattere corrente
				iCurIndex++;

			}
		}

		m_lNumCharY=lCharY;
		m_lNumCharX=lCharX;
		m_lCharWidth=(LONG)(((double) m_iScale / 100) * lCharWidth);
		m_lCharHeight=(LONG)(((double) m_iScale / 100) * lCharHeight);
		
		m_iStatus=2;

		return hrErr;	
	}

	else return E_FAIL;

}


//---------------------- FreeCharTable -----------------------------------

void CADXCharSet::FreeCharTable(void)
{
	//rilascia i caratteri

	for (int iCount=0;iCount<256;iCount++)
	{
		if (m_imgChars[iCount].status) m_lpFm->FreeImgFrame(&m_imgChars[iCount]);
	}
    
	m_iStatus=1;

}

//---------------------------- TextOut ----------------------------------
//Visualizza sullo schermo corrente una stringa formattata usando i caratteri caricati

void CDECL CADXCharSet::TextOut(int xout,int yout,TCHAR *szFormat, ...) //stampa una stringa formattata
{
	TCHAR szBuffer[2048]; //2k per il messaggio
	va_list pArgList;
	TCHAR *szChar;
	int iChar;
	LONG lCurX;


	memset (szBuffer,0,(size_t)2048);

	if (m_iStatus > 1)
	{

		//Questa macro fa si che pArgList punti all'inizio del buffer
		//dei valori opzionali
		va_start(pArgList,szFormat);
		
		//formatta e mette la stringa formattata nel buffer szBuffer
		_vsnprintf(szBuffer,sizeof(szBuffer) / sizeof(TCHAR) ,szFormat,pArgList);
    
		va_end(pArgList);

		szChar=szBuffer;
        iChar = (int) szChar[0]; 

		lCurX=(LONG)xout;

		//Esegue un ciclo su tutti i caratteri della stringa

		while (iChar)
		{
			iChar = (int) szChar[0];

			if (iChar>0 && iChar<256)
			{
				m_lpFm->PutImgFrame(lCurX,(LONG)yout,&m_imgChars[iChar]);
				
			}
			szChar ++;
			lCurX += m_lCharWidth+m_lCharSpacing;
		}

	}

}

//-------------------------- TextOut (ovr) ------------------------------------------
//Output di testo su una immagine qualsiasi

void CDECL CADXCharSet::TextOut(int xout,int yout,IMAGE_FRAME_PTR img,TCHAR *szFormat, ...) //stampa una stringa formattata
{
	TCHAR szBuffer[2048]; //2k per il messaggio
	va_list pArgList;
	TCHAR *szChar;
	int iChar;
	LONG lCurX;
    
	//se il testo � nullo esce
	if (!szFormat) return;

	memset (szBuffer,0,(size_t)2048);

	//Questa macro fa si che pArgList punti all'inizio del buffer
	//dei valori opzionali
	va_start(pArgList,szFormat);
	
	//formatta e mette la stringa formattata nel buffer szBuffer
	_vsnprintf(szBuffer,sizeof(szBuffer) / sizeof(TCHAR) ,szFormat,pArgList);

	va_end(pArgList);

	szChar=szBuffer;
    iChar = (int) szChar[0]; 

	lCurX=(LONG)xout;

	//Esegue un ciclo su tutti i caratteri della stringa

	while (iChar)
	{
		iChar = (int) szChar[0];

		if (iChar>0 && iChar<256 && m_imgChars[iChar].status>0)
		{
			m_lpFm->PutImgFrameClip(lCurX,(LONG)yout,img,&m_imgChars[iChar]);
			
		}
		szChar ++;
		lCurX += m_lCharWidth+m_lCharSpacing;
	}
	
}

//--------------------------------------------------- TextOutFast ------------------------------------------------------
//Stampa una stringa non formattata sulla superficie imgDest
//NB: non esegue controlli sulla validit� dei parametri
void CADXCharSet::TextOutFast(int xout,int yout,IMAGE_FRAME_PTR imgDest,TCHAR *szText)
{
	int iChar;
	TCHAR *szChar;
	LONG lCurX;
	IMAGE_FRAME_PTR pimg;

	szChar=szText;

	lCurX=(LONG)xout;

	iChar=szChar[0];

	while (iChar)
	{
		iChar = (int) szChar[0];

		if (iChar>0 && iChar<256 && m_imgChars[iChar].status>0)
		{
			pimg=&m_imgChars[iChar];

			m_lpFm->PutImgFrameRect(lCurX,(LONG)yout,imgDest,pimg,&pimg->rcClip);
			
		}

		lCurX += m_lCharWidth+m_lCharSpacing;

		szChar ++;
	}

}