/*
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
    modular.h
	Nemico linkabile 

*/


#ifndef _MODULAR_
#define _MODULAR_

#define MAX_MOD_LINKS 8   //numero massimo di nemici linkabili


#include "mech.h"
#include "mcasmbl.h"

class CModularEnemy: public CMechAssembly
{
private:

	int m_iLinkCount; //contatore numero elementi linkati
	CModularEnemy *m_LinkedEnemies[MAX_MOD_LINKS]; //vettore che contiene i nemici linkati a questo

public:	
		
	CModularEnemy(void);
	CMechComponent Frame;
	virtual void Reset(void);
	int AttachEnemy(int iCoupling,int iParentCoupling,CModularEnemy *pModEnemy);
	CModularEnemy* GetAttachedEnemy(int index);
	//scollega tutti gli elementi
	void Unlink(void);	
	//queste sono virtuale perch� si deve sempre invocare la funzione ridefinita se presente
	virtual BOOL DoCollisionWith(CSBObject *pobj);
	virtual void SetPosition(const LONG x,const LONG y);
	virtual void SetEnergy(LONG energy);
	virtual BOOL UpdateAI(void);
	virtual void Explode(void);
	virtual HRESULT Draw(void);

};

#endif