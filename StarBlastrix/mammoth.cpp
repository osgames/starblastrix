//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  mammoth.cpp -- mammut
///////////////////////////////////////////////////////////////////////*/

#include "mammoth.h"
#include "sbengine.h"
#include "a32util.h"

#define NMAMMOTHFRAMES 3

extern CADXFrameManager g_fm;
extern CADXFastMath g_fmath;
extern CObjectStack g_sEShell11; //arma primaria p. laser rosso
extern CObjectStack g_sEShell2; //arma secondaria p. piccolo bianco
//stack oggetti attivi
extern CObjectStack g_objStack;
extern CADXSound g_cs;
//p. laser orientabile
extern LONG g_snFire9;
extern void PushExplMulti(CADXSprite*,int);
extern CObjectStack g_sNozzleFlames;
extern LONG g_snNozzle;


static BOOL g_bMammothResLoaded=FALSE;

IMAGE_FRAME g_imgMammoth[NMAMMOTHFRAMES];

BOUND_POLY g_bpMammoth[NMAMMOTHFRAMES];
BOUND_POLY_PTR pb=NULL;

HRESULT LoadMammothRes(CResourceManager* prm)
{
	if (g_bMammothResLoaded) return S_OK;

	if (!(2==prm->GetCurrentImageIndex() && 0==_tcscmp(prm->GetCurrentFileName(),TEXT("data\\data3.vpx"))))
	{
		//deve caricare il file
		if (FAILED(prm->LoadAuxResFile(TEXT("data\\data3.vpx"),2))) return E_FAIL;
	}

	CADXFrameManager* fm=prm->GetFrameManager();

	if (NULL==fm) return E_FAIL;

	//immagine corrente
	IMAGE_FRAME_PTR pimg=prm->GetCurrentImage();

	if (FAILED(fm->GrabFrame(&g_imgMammoth[0],0,313,169,422,pimg))) return E_FAIL;
	if (FAILED(fm->GrabFrame(&g_imgMammoth[1],180,314,341,444,pimg))) return E_FAIL;
	if (FAILED(fm->GrabFrame(&g_imgMammoth[2],378,314,522,465,pimg))) return E_FAIL;


	g_bpMammoth[0].poly=new VERTEX2D[19];
	pb=&g_bpMammoth[0];
	pb->inum=0;
	AddVertex(pb,0,43);AddVertex(pb,14,44);AddVertex(pb,19,37);AddVertex(pb,19,20);
	AddVertex(pb,31,7);AddVertex(pb,51,0);AddVertex(pb,167,0);AddVertex(pb,167,46);
    AddVertex(pb,117,40);AddVertex(pb,87,57);AddVertex(pb,70,44);AddVertex(pb,56,44);
    AddVertex(pb,45,63);AddVertex(pb,52,74);AddVertex(pb,54,104);AddVertex(pb,20,96);
    AddVertex(pb,20,80);AddVertex(pb,5,81);AddVertex(pb,0,59);

	g_bpMammoth[1].poly=new VERTEX2D[22];
	pb=&g_bpMammoth[1];
	pb->inum=0;
	AddVertex(pb,0,79);AddVertex(pb,17,69);AddVertex(pb,14,54);AddVertex(pb,22,35);AddVertex(pb,42,23);
	AddVertex(pb,129,0);AddVertex(pb,153,0);AddVertex(pb,164,37);AddVertex(pb,112,45);AddVertex(pb,88,61);
    AddVertex(pb,69,56);AddVertex(pb,54,59);AddVertex(pb,51,84);AddVertex(pb,66,122);AddVertex(pb,50,132);
    AddVertex(pb,32,127);AddVertex(pb,29,110);AddVertex(pb,14,111);AddVertex(pb,19,103);AddVertex(pb,26,100);
    AddVertex(pb,23,89);AddVertex(pb,3,93);

	g_bpMammoth[2].poly=new VERTEX2D[24];
	pb=&g_bpMammoth[2];
	pb->inum=0;
	AddVertex(pb,0,115);AddVertex(pb,14,106);AddVertex(pb,15,100);AddVertex(pb,7,86);AddVertex(pb,10,67);
	AddVertex(pb,21,53);AddVertex(pb,105,5);AddVertex(pb,125,0);AddVertex(pb,146,30);AddVertex(pb,126,41);
	AddVertex(pb,99,49);AddVertex(pb,84,77);AddVertex(pb,66,79);AddVertex(pb,52,86);AddVertex(pb,49,104);
	AddVertex(pb,65,123);AddVertex(pb,74,139);AddVertex(pb,69,150);AddVertex(pb,44,151);AddVertex(pb,36,135);
	AddVertex(pb,23,141);AddVertex(pb,21,134);AddVertex(pb,29,124);AddVertex(pb,6,126);

	g_bMammothResLoaded=TRUE;

	return S_OK;

}

void FreeMammothRes(void)
{
	if (!g_bMammothResLoaded) return;

	g_fm.FreeFrameArray(g_imgMammoth,3);

	SAFE_DELETE_ARRAY(g_bpMammoth[2].poly);
	SAFE_DELETE_ARRAY(g_bpMammoth[1].poly);
    SAFE_DELETE_ARRAY(g_bpMammoth[0].poly);

	g_bMammothResLoaded=FALSE;

}


HRESULT CreateMammothStack(CResourceManager *prm,CObjectStack *pstk,DWORD inumitems,int iflags)
{
	if (!prm) return E_FAIL;

	CMammoth* pm=NULL;

	if (!pstk) return E_FAIL;
    //carica le risorse per il serpente di tipo 2
	if (FAILED(LoadMammothRes(prm))) return E_FAIL;
	
	if (FAILED(pstk->Create(inumitems))) return E_FAIL;	

	for (DWORD count=0;count<inumitems;count++)
	{
		pstk->m_objStack[count]=new CMammoth;
		pm=(CMammoth*)pstk->m_objStack[count];
		pm->bActive=FALSE;
		pm->SetGraphicManager(prm->m_lpGm);
		pm->SetFrameManager(prm->m_lpFm);
		pm->SetUpdateFreq(40);
		pm->SetEnergy(25);
		pm->dwPower=10;
		pm->dwScore=1000;
		pm->dwResId=EN_MAMMOTH;
		pm->lpfnExplode=PushExpl9;
		pm->pfnCollide=PushCollideMetal;
		pm->SetActiveStack(&g_objStack);		
		pm->SetCartridgeStack(&g_sEShell11); //proiettili rossi	
		pm->SetWeapon1(&g_sEShell2); //proiettili binchi piccoli
		pm->SetFireSound(g_snFire9);
		//serpente rosso di default
		for (DWORD j=0;j<NMAMMOTHFRAMES;j++)
		{
			pm->AddFrame(&g_imgMammoth[j]);
			pm->SetBoundPoly(j,&g_bpMammoth[j]);
		};

		//imposta la posizione dell'ugello
		//frame,hostspot,dx,dy
		pm->SetFrameHotSpot(0,0,169,25);
		pm->SetFrameHotSpot(1,0,157,16);
		pm->SetFrameHotSpot(2,0,138,15);
		//hotspot 1=cannone grande
		pm->SetFrameHotSpot(0,1,0,52);
		pm->SetFrameHotSpot(1,1,1,86);
		pm->SetFrameHotSpot(2,1,4,121);
		//hotspot 2=cannone picolo
		pm->SetFrameHotSpot(0,2,9,74);
		pm->SetFrameHotSpot(1,2,16,106);
		pm->SetFrameHotSpot(2,2,22,137);
		pm->SetHNozzleFlamesStack(&g_sNozzleFlames,0);
		pm->SetNozzleNoise(g_snNozzle);
	}

	return S_OK;
}

//----------------------------------------------- CMammoth ---------------------------------------

CMammoth::CMammoth(void)
{
	dwClass=CL_ENEMY;
	dwType=37;
	m_trim=0;
	m_pHNozzleFlames=NULL;
	m_pHFlames=NULL;
	m_iHFlamesJoint=0;
	m_lNozzleNoise=0;
}

void CMammoth::Reset(void)
{
	CEnemy::Reset();	
	SetVelocity(180,2);
	SetCurFrame(0);
	m_trim=0;
	dwClass=CL_ENEMY;
	bBoxActive=FALSE;
	iConfigFreq=10;
	iStepConfig=0;
	m_bDoHFlames=FALSE;
	burst_cnt=0;
	iFireFreq=100;
	iUpdateFreq=60;
	m_ttl=g_fmath.RndFast(20)+28; //numero di cambiamenti di stato prima di uscire di scena

	if (m_pHNozzleFlames)
	{
		m_pHFlames=(CAnimatedObject *)m_pHNozzleFlames->Pop();
		if (m_pHFlames)
		{
			//attacca le fiamme a questo oggetto
			m_pHFlames->pParent=this;
			LONG xj,yj;	
			//acquisisce la posizione relativa dell'ugello
			GetFrameHotSpot(0,m_iHFlamesJoint,&xj,&yj);
			//imposta la posizione relativa delle fiamme
			m_pHFlames->SetPosition(xj,yj-12);		
		}
	}


}

void CMammoth::SetNozzleNoise(LONG sound)
{
	m_lNozzleNoise=sound;
};

BOOL CMammoth::Update(void)
{
	if (++iStepConfig>=iConfigFreq)
	{
		iStepConfig=0;
		
		if (m_trim<cur_frame) DecFrame();
		else if (m_trim>cur_frame) IncFrame();

	}

	if (++iStepUpdate>=iUpdateFreq && m_ttl>0)
	{
		iStepUpdate=0;

		m_ttl--;

		if (m_ttl<=0)
		{
			//quando questo contatore raggiunge zero imposta la traiettoria di uscita di scena
			m_trim=0;
			bBoxActive=FALSE;
			SetVelocity(180,3);
			m_bDoHFlames=TRUE;
			g_cs.PlaySound(m_lNozzleNoise,0,0);

		}
		else
		{

			int iprob=g_fmath.RndFast(8);
		
			switch(iprob)
			{
			case 0:
				
				m_trim=2; //target frame
				bBoxActive=TRUE;
				rcLimitBox.left=20;
				rcLimitBox.bottom=400;
				rcLimitBox.top=-120;
				rcLimitBox.right=610;
				SetVelocity(150,2);

				break;

			case 1:
			
				m_trim=0;
				SetVelocity(0,0);
				if (burst_cnt<=0)
				{
					iFireFreq=12;
					burst_cnt=8;
				}

				break;

			case 2:

				bBoxActive=TRUE;


				if (lTargetY>y+80 && lTargetX<x-80)
				{
					m_trim=2;
					SetVelocity(0,0);
				}
				else if (lTargetY<y)
				{
					rcLimitBox.left=20;
					rcLimitBox.bottom=400;
					rcLimitBox.top=-10;
					rcLimitBox.right=610;
					m_trim=0;
					SetVelocity(280,3);			

				}
				else if (lTargetX>x)
				{
					SetVelocity(0,4);
					m_trim=0;
				}
				else 
				{
					m_trim=2;
					SetVelocity(200,4);
				}

				if (burst_cnt<=0)
				{
					iFireFreq=12;
					burst_cnt1=8;
					burst_cnt=0;
				}
				

				break;

			case 3:

				burst_cnt1=g_fmath.RndFast(14);
				iFireFreq=12;
				m_trim=2;
				SetVelocity(120,2);
				bBoxActive=TRUE;	
				break;

			default:

				int an=g_fmath.GetAngle(x,y,lTargetX,lTargetY);

				SetVelocity(an,3);

				if (an<=170 && an>=130) m_trim=1;
				else if (an<130 && an>=100) m_trim=2;
				else m_trim=0;
				if (burst_cnt<=0)
				{
					iFireFreq=12;
					burst_cnt=g_fmath.RndFast(8)+3;					
				}

				if (burst_cnt1<=0)
				{
					iFireFreq=12;
					burst_cnt1=g_fmath.RndFast(10)+5;

				}

				bBoxActive=TRUE;

				break;

			}

		}

		int cur_an=this->GetAngle();

		if (!m_bDoHFlames && cur_frame==0 && cur_an>=110 && cur_an<270)
		{
			m_bDoHFlames=TRUE;
			g_cs.PlaySound(m_lNozzleNoise,0,0);
		}
		else m_bDoHFlames=FALSE;
	}

	UpdatePosition();

	if (dwEnergy<=0)
	{
		CreateBonus();
		return FALSE;
	}

	if (bBoxActive)
	{
		//controlla se va fuori dal box di contenimento
		//in questo caso deve ricalcolare la traiettoria
		if (y<rcLimitBox.top || y>rcLimitBox.bottom || x<rcLimitBox.left || x>rcLimitBox.right)
		{
			//lo ferma
			SetVelocity(0,0);
			//ricalcola la traiettoria al prossimo passaggio
			iStepUpdate=iUpdateFreq;
		}

	};
	
	if (m_bDoHFlames && m_pHFlames)
	{
		if (cur_frame!=0) m_bDoHFlames=FALSE;
		else m_pHFlames->Update();
	}

	if (++iStepFire>=iFireFreq)
	{

		iStepFire=0;


		if (lTargetX<x-80)
		{

			if (m_Cartridges && burst_cnt>0)
			{
				CShell* p=(CShell*)m_Cartridges->Pop(); 

				if (p)
				{

					p->Reset();
					//orienta il proiettile in base alla frame corrente
					switch(cur_frame)
					{
					case 0:

						p->SetCurFrame(0);
						p->SetVelocity(180,16);
						break;

					case 1:
						
						p->SetCurFrame(11);
						p->SetVelocity(165,16);
						break;
					case 2:

						p->SetCurFrame(10);
						p->SetVelocity(150,16);
						break;
					}

				
					p->SetEnergy(2);
					p->dwPower=10;
					p->dwClass=CL_ENFIRE;
					LONG xf,yf;
					GetJointPosition(&xf,&yf,1);
					p->SetPosition(xf,yf);

					if (m_ActiveStack)
					{
						m_ActiveStack->Push(p);
						g_cs.PlaySound(m_lFireSound,0,0);

					}

					burst_cnt--;
					

				}
			}

			if (m_pWeapon1 && burst_cnt1>0)
			{
				CShell* p=(CShell*)m_pWeapon1->Pop(); 

				if (p)
				{

					p->Reset();
					//orienta il proiettile in base alla frame corrente
					switch(cur_frame)
					{
					case 0:

						p->SetCurFrame(0);
						p->SetVelocity(180,12);
						break;

					case 1:
						
						p->SetCurFrame(11);
						p->SetVelocity(165,12);
						break;
					case 2:

						p->SetCurFrame(10);
						p->SetVelocity(150,12);
						break;
					}

				
					p->SetEnergy(2);
					p->dwPower=5;
					p->dwClass=CL_ENFIRE;
					LONG xf,yf;
					GetJointPosition(&xf,&yf,2); //cannoncino piccolo
					p->SetPosition(xf,yf);

					if (m_ActiveStack)
					{
						m_ActiveStack->Push(p);
						g_cs.PlaySound(m_lFireSound,0,0);

					}

					burst_cnt1--;
					
				}

			}

			if (burst_cnt<=0 && burst_cnt1<=0) iFireFreq=100;
		}
	}

	return (x>-200 && x<1000 && y>-200 && y<500);

};

void CMammoth::Explode(void)
{
	PushExplMulti(this,5);
}


	//imposta lo stck delle fiamme orizzontali e l'hotspot al quale vengono collegate
void CMammoth::SetHNozzleFlamesStack(CObjectStack *pflames,int hotspot)
{
	m_iHFlamesJoint=hotspot;
	m_pHNozzleFlames=pflames;
}

HRESULT CMammoth::Draw(void)
{
	CSBObject::Draw();
	if (m_bDoHFlames && m_pHFlames)
	{
		//disegna le fiamme dell'ugello
		m_pHFlames->SetDisplay(m_pimgOut);
		m_pHFlames->Draw();
	}

	return S_OK;
}