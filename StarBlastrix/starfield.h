//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  starfield.h -- header file per la classe CStarField
///////////////////////////////////////////////////////////////////////*/


#ifndef _STARFIELD_INCLUDE_
#define _STARFIELD_INCLUDE_

#include "a32graph.h"
#include "a32fastm.h"

//le particelle si muovono in senso orizzontale
typedef struct PARTICLE_TAG
{
	int color;
	float x,y;
	float velocity;	
	int size;

} PARTICLE_TYP, *PARTICLE_TYP_PTR;


class CStarField
{
private:

	const int m_view_dist;
	//costante di tempo nella funzione NormVal (1-e^(-t/Tau))
	const float m_tau; 
	const float m_range;
	const float m_max_dist; //massima distanza dal piano del video
	IMAGE_FRAME_PTR m_pscreen;
	PARTICLE_TYP_PTR m_pParticles; //vettore particelle
	PARTICLE_TYP_PTR m_pTemplateParticles; //posizioni iniziali
	PARTICLE_TYP_PTR m_p; //general purpose
	LPDIRECTDRAWSURFACE7 m_pDDS;
	PARTICLE_TYP_PTR m_last;
	DDBLTFX ddbfx;
	RECT    rcDest;
	float m_temp; //general purpose
	int m_width;
	int m_height;
	int m_nparticles; //numero totale di particelle
	int m_refvel;
	int m_white;
	void AddParticle(float distance);
	float NormVal(float x); //NormVal=1-e^(-x/m_tau))
	float RndNormVal(void);
	void CreateParticle(PARTICLE_TYP_PTR p);
	void InitStarField(void);


public:

	CStarField(int scr_width,int scr_height,int num_particles,int max_velocity=6);
	void SetDestImage(IMAGE_FRAME_PTR pimg);
	virtual ~CStarField(void);
	//esegue il rendering
	void Render(void);
	//aggiorna la posizione delle particelle
	void Update(void);

};


#endif