////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
///////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------                       - All Rights Reserved
/* Modulo di funzioni ausiliarie per Star Blastrix
/*--------------------------------------------------------------------*/


#ifndef _SBUTIL_INCLUDE_
#define _SBUTIL_INCLUDE_


#include "a32graph.h" //libreria grafica 2D arcade 32
#include "ccharset.h"
#include "a32input.h"
#include "a32sprit.h"
#ifdef _DEBUG_
#include <crtdbg.h>
#endif

//lista dei tipi impostati nell'argomento iType
#define SB_CTRL_STATIC  0x1
#define SB_CTRL_BUTTON  0x2
#define SB_CTRL_OPTION  0x3
#define SB_CTRL_TEXT    0x4
#define SB_CTRL_FORM    0x5
#define SB_CTRL_GROUP   0x6
#define SB_CTRL_KEYCODE 0x7

//posizione del selettore rispetto al controllo
#define SB_CTRLLEFT 0x0
#define SB_CTRLRIGHT 0x1
#define SB_CTRLCENTER 0x2

#define MAX_CTRLS 50 //numero massimo di controlli all'interno di ogni gruppo e di ogni form

#ifndef SPRITE_PTR 
#define SPRITE_PTR CADXSprite*
#endif

#ifndef SPRITE
#define SPRITE CADXSprite
#endif

#define JU_CENTER 0x0
#define JU_LEFT 0x1
#define JU_RIGHT 0x2

//Classe che definisce un controllo utente generico, deriva dalla classe base di Arcade32
class CSbControl:public virtual CADXBase
{
protected:
	
	LONG lLeft,lTop,lWidth,lHeight;;         //posizione 
	CADXCharSet *pCChar;	 //carattere
	TCHAR *szText;
	int iType; //tipo
	int iStyle; //stile
	int (*m_lpfnAction)(void *ctrl); //punta alla funzione da chiamare quando viene attivato il controllo
	BOOL m_bActive; //indica se il controllo � attivo
	BOOL m_bHasFocus;
	void *m_objParent; //oggetto genitore (es. form a cui appartiene)

public: 

	CSbControl(void);
	~CSbControl(void);

	virtual HRESULT Create(LONG lLeft,LONG lTop,TCHAR *szText,int iStyle);
	virtual void MoveTo(LONG x,LONG y); //posiziona il controllo
	virtual HRESULT SetCharSet(CADXCharSet *cs); //imposta il carattere
	virtual TCHAR *Text(void); //acquisisce il testo
	virtual void SetText(TCHAR *txt); //imposta il testo
	virtual void Render(IMAGE_FRAME_PTR img); //disegna il controllo
	virtual HRESULT SetAction(int (*lpfnActionFn)(void *ctrl));
	virtual LONG GetWidth(void); //acquisisce le dimensioni del controllo
	virtual LONG GetHeight(void);
	virtual LONG GetLeft(void);
	virtual LONG GetTop(void);	
	virtual void SetLeft(LONG newval);
	virtual void SetTop(LONG newval);
	virtual HRESULT Justify(int imode,LONG lref);
	virtual void Execute(void);
	virtual void SetFocus(BOOL bFocus); //imposta il fuoco o lo disattiva
	virtual BOOL HasFocus(void); //rende lo stato del controllo
	BOOL Active(void); //restituisce TRUE se il controllo � attivo
	void Activate(BOOL bVal); //attiva o disattiva il controllo
	int GetType(void); //restituisce il tipo di controllo

};

//testo statico 
class CSbStaticText:public virtual CSbControl
{
private:


public:

	CSbStaticText(void);
	virtual void Render(IMAGE_FRAME_PTR img);
};

//input text

class CSbInputText:public CSbControl
{
private:

	LONG m_lSelStart; //posizione corrente
	LONG m_lLeftText; //left del testo di input
	LONG m_lCaptWidth; //larghezza del testo di caption
	BOOL bSetText;
	IMAGE_FRAME m_imgCaret; //caret
	CADXCharSet *m_pTextCharSet; //set di caratteri usato per il testo digitato

protected:

	DWORD dwMaxLen; //numero max. di caratteri digitabili
	TCHAR *szInputText; //testo digitato

public:
	
	CSbInputText(void); //costruttore
	~CSbInputText(void); //distruttore
	
    HRESULT Create(LONG lLeft,LONG lTop,TCHAR *szCaption,int iStyle);
	virtual void Render(IMAGE_FRAME_PTR img);
	virtual HRESULT SetMaxLen(DWORD dwValue);
	DWORD GetMaxLen(void);
	virtual LONG GetWidth(void); //acquisisce le dimensioni del controllo
	virtual LONG GetHeight(void);
	virtual LONG GetLeft(void);
	virtual LONG GetTop(void);	
	virtual void SetText(TCHAR *szText);
	virtual BOOL SendKey(int iKey);
	virtual HRESULT SetInputText(TCHAR *szTxt); //imposta il testo all'interno del controllo
	virtual TCHAR *GetInputText(void); //acquisisce il testo
	LONG GetSelStart(void);
	HRESULT SetSelStart(LONG new_val);
	HRESULT SetTextCharSet(CADXCharSet *pCS);

};


//controllo che contiene il codice di un tasto
class CSbKeyCode:public CSbInputText
{
private:

	int m_iKeyCode; //codice del tasto
	CADXInput m_ci;

public:
	
	CSbKeyCode(void);
	~CSbKeyCode(void);
	int GetKeyCode(void);
	HRESULT SetKeyCode(int iKey);
	HRESULT CSbKeyCode::SetMaxLen(DWORD dwValue);
	virtual BOOL SendKey(int iKey);	
};

//Implementa un bottone
class CSbButton:public virtual CSbControl
{
private:

	LONG m_lTextX;         //distanza fra il testo e il bordo sx del pulsante
	LONG m_lTextY;         //distanza fra il testo e il bordo in alto
	IMAGE_FRAME_PTR m_pimgBtu; //immagine del pulsante

public:
	
	CSbButton(void);	
	virtual void Render(IMAGE_FRAME_PTR img);
    virtual HRESULT Create(LONG lLeft,LONG lTop,TCHAR *szText,IMAGE_FRAME_PTR pimgBtu,int iStyle);
	LONG GetWidth(void); //acquisisce le dimensioni del controllo
	LONG GetHeight(void);
	LONG GetLeft(void);
	LONG GetTop(void);	
};


//Implementa il controllo option
class CSbOption:public virtual CSbStaticText
{
private:

	BOOL m_bValue; //valore attuale, puo' essere vero o falso
    IMAGE_FRAME_PTR m_pimgOn;  //immagine quando � on
	IMAGE_FRAME_PTR m_pimgOff; //immagine quando � off

public:

	CSbOption(void); //costruttore	
	virtual HRESULT Create(LONG lLeft,LONG lTop,TCHAR *szText,BOOL bInitialState,IMAGE_FRAME_PTR pimgOn,IMAGE_FRAME_PTR pimgOff);
	virtual void Render(IMAGE_FRAME_PTR img);
	BOOL Value(void); //restituisce lo stato
	HRESULT SetValue(BOOL bNewVal); //imposta lo stato
	virtual void Execute(void);
};


//Crea un gruppo di controlli
class CSbGroup:public virtual CSbControl
{
private:


protected:

	CSbControl *m_lpctrlList[MAX_CTRLS]; //vettore che contiene tutti i controlli
	DWORD m_dwCount; //numero di controlli all'interno del gruppo

public:

	CSbGroup(void);
	~CSbGroup(void);
	virtual HRESULT AddControl(CSbControl *ctrl,BOOL bActive);

};


//Implementa un contenitore controlli
//che attende l'input dell'utente
class CSbForm:public virtual CSbGroup
{

private:


protected:


	SPRITE_PTR m_psprPointer; //sprite del puntatore del mouse
	SPRITE_PTR m_psprSel; //sprite dell selettore	
    RECT m_rcClientRect; //rettangolo che definisce l'area attiva del form
    BOOL m_bMouseOn; //eventi del mouse attivi
	BOOL m_bBreakListen;
	BOOL m_bActive;
	CADXInput *m_pci; //input manager 
	int m_iSelectPosition; //posizione del selettore rispetto al controllo
	DWORD m_dwCurrentControl; //indice del controllo correntemente selezionato
	int m_iSelWidth,m_iSelHeight; //larghezza e altezza del selettore
	IMAGE_FRAME m_imgOut; //immagine su cui vengono disegnati i controlli    
	DWORD GetNextActiveControl(DWORD dwStart);
	DWORD GetPrevActiveControl(DWORD dwStart);
	DWORD GetControlXY(LONG x,LONG y); //acquisisce il controllo alla posizione X,Y
	void SelectControl(DWORD dwIndex); //imposta il fuoco sul controllo con indice dwIndex
	void (*m_lpfnBackGround)(IMAGE_FRAME_PTR img);    //punta alla funzione che disegna il background mentre � in attesa dell'input
	void CSbForm::MouseUpdate(void);
	BOOL bMouseLeft; //serve per gestire l'evento click del mouse
	BOOL m_bJoyCentered; //serve per gestire lo postamento nei menu tramite joystick
	BOOL m_bJoyButtonReady; //switch on off per controllare lo stato del tasto 1 del joy

public:

	CSbForm(void);
	~CSbForm(void);
	void ShowMouse(void);
	void HideMouse(void);
	HRESULT SetMousePointer(SPRITE_PTR psprPoint);
	HRESULT SetSelector(SPRITE_PTR psprSel,int iPosition); //selettore che evidenzia il controllo corrente
	HRESULT SetActiveRect(LPRECT pRect);
	HRESULT SetInputManager(CADXInput *ci); //dopo aver impostato l'input manager, lo stato viene sattato su 2			
	HRESULT SetActiveControl(CSbControl *pctrl); //imposta il fuoco sul controllo pctrl
	CSbControl *GetActiveControl(void); //restituisce il controllo attivo corrente
	void SetBackGroundProc(void (*lpfnBack)(IMAGE_FRAME_PTR img)); //imposta la funzione che disegna il background
	BOOL IsActive(void); //indica se � attiva
	void Close(void); //termina il ciclo di controllo input e chiude la form
	void Listen(void); //attende l'input dell'utente
	void Open(void); //carica e visualizza la form
	void Render(IMAGE_FRAME_PTR img);

};




#endif


