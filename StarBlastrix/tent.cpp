//////////////////////////////////////////////////////////////////////////
/*
/*    ****   *****    ***    ****      ***   *      ***     **** *****   
/*   *         *     *   *   *   *     *  *  *     *   *   *       *
/*   ****      *     *****   *****     ***   *     *****   ****    *
/*     *       *     *   *   **        *  *  *     *   *     *     *
/* ****        *     *   *   *  **     ***   ****  *   * ****      *
/*
//////////////////////////////////////////////////////////////////////////
    Starblastrix - side scrolling shoot'em up
	
	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------
/*  tent.cpp -- tentacolo meccanico
///////////////////////////////////////////////////////////////////////*/


#include "tent.h"
#include "a32sprit.h"
#include "a32graph.h"
#include "a32audio.h"
#include "thunder.h"

extern LONG g_snBfire; //fuoco primario
extern LONG g_snFire10; //tuono
extern CADXSound g_cs;


CTentacle::CTentacle(void)
{
	CEnemy::CEnemy();
	m_iNumItems=0;
	m_pBody=NULL;
	m_pHead.m_iTag1=0;
	m_pLeftJaw.m_iTag1=m_pRightJaw.m_iTag1=0;
	dwStatus=0;	
	m_ialpha=NULL;
	m_itarget_config=NULL;
	m_inumalpha=0;
	dwClass=CL_ENEMY;
	dwScore=8000;
	m_pcurseq=m_seq1;
	m_iseqindex=0;
	m_pmath=NULL;

	m_seq1[0]=1;m_seq1[1]=1;m_seq1[2]=1;m_seq1[3]=2;m_seq1[4]=-1;m_seq1[5]=-1;m_seq1[6]=-1;m_seq1[7]=2;m_seq1[8]=-1;m_seq1[9]=1;
	m_seq1[10]=1;m_seq1[11]=1;m_seq1[12]=1;m_seq1[13]=0;m_seq1[14]=0;m_seq1[15]=-1;m_seq1[16]=2;m_seq1[17]=-1;m_seq1[18]=-1;m_seq1[19]=0;
	m_seq1[20]=0;m_seq1[21]=-1;m_seq1[22]=-1;m_seq1[23]=0;m_seq1[24]=2;m_seq1[25]=0;m_seq1[26]=-1;m_seq1[27]=0;m_seq1[28]=2;
    m_seq1[29]=1;

	m_vBodyJoints[0].x=m_vBodyJoints[0].y=m_vBodyJoints[1].x=m_vBodyJoints[1].y=0;
    m_vLeftJawJoints[0].x=m_vLeftJawJoints[0].y=m_vLeftJawJoints[1].x=m_vLeftJawJoints[1].y=0;
    m_vRightJawJoints[0].x=m_vRightJawJoints[0].y=m_vRightJawJoints[1].x=m_vRightJawJoints[1].y=0;
	m_bBodyJoints=false;
	m_bLeftJawJoints=false;
	m_bRightJawJoints=false;
	m_bHeadJoints=false;
	
}

CTentacle::CTentacle(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,IMAGE_FRAME_PTR pimgJawLeft,IMAGE_FRAME_PTR pimgJawRight)
{
	CTentacle();
	Create(numitems,pimgbody,pimghead,pimgJawLeft,pimgJawRight);
}


CTentacle::~CTentacle(void)
{
	if (m_pBody)
	{
		for (int count=0;count<m_iNumItems;count++)
		{
			m_pBody[count].Release();		
		}
	}

	m_pHead.Release();
	m_pLeftJaw.Release();
	m_pRightJaw.Release();

	SAFE_DELETE_ARRAY(m_pBody);
	SAFE_DELETE_ARRAY(m_ialpha);
	SAFE_DELETE_ARRAY(m_itarget_config);
}


HRESULT CTentacle::Create(int numitems,IMAGE_FRAME_PTR pimgbody,IMAGE_FRAME_PTR pimghead,IMAGE_FRAME_PTR pimgJawLeft,IMAGE_FRAME_PTR pimgJawRight)
{
	if (dwStatus>=1) return E_FAIL; //prima di creare il tentacolo si devono scpecificare i punti di collegamento fra i vari elementi
	if (numitems<=0) return E_FAIL;
	if (!m_lpFm) return E_FAIL;
	if (!m_bBodyJoints) return E_FAIL;
	if (pimghead && !m_bHeadJoints) return E_FAIL; //si vuol attaccare la testa ma non si sono creati i giunti
	if (pimgJawLeft && !m_bLeftJawJoints) return E_FAIL;
	if (pimgJawRight && !m_bRightJawJoints) return E_FAIL;

	m_pBody=new CMechComponent[numitems];

	m_iNumItems=numitems;

	//ciclo su ogni elemento del corpo
	//l'elemento radice da attaccare alla frame dell'oggetto genitore, � quello con indice 0
	for (int count=0;count<numitems;count++)
	{
		m_pBody[count].SetFrameManager(m_lpFm);

		if (!(m_pBody[count].CreateComponent(0,360,pimgbody,24))) return E_FAIL;
		//aggiunge le coppie rotoidali al corpo
		//giunto posteriore
		m_pBody[count].AddRotCoupling(m_vBodyJoints[0].x,m_vBodyJoints[0].y,0,360,1);
		//giunto anteriore
		m_pBody[count].AddRotCoupling(m_vBodyJoints[1].x,m_vBodyJoints[1].y,0,360,1);

		if (count<=3)
		{
			//gli elementi vicini alla radice sono piu' robusti
			m_pBody[count].m_iTag1=dwEnergy*2; //energia di ogni elemento
		}
		else
		{
			m_pBody[count].m_iTag1=dwEnergy; //energia di ogni elemento
		}

		if (count>0)
		{
			//collega ogni elemento al precedente
			m_pBody[count].LinkToComponent(0,1,&m_pBody[count-1]);
		}
	}

	//crea la testa del tentacolo
	if (pimghead)
	{
		m_pHead.SetFrameManager(m_lpFm);
		if (!(m_pHead.CreateComponent(0,360,pimghead,24))) return E_FAIL;
		m_pHead.AddRotCoupling(m_vHeadJoints.x,m_vHeadJoints.y,0,360,1);
		//collega la testa al resto del tentacolo
		m_pHead.LinkToComponent(0,1,&m_pBody[numitems-1]);

		m_pHead.m_iTag1=dwEnergy*2;

		m_inumalpha=numitems+1;
	
	}

	else m_inumalpha=numitems;

	m_ialpha=new int[m_inumalpha];
	memset(m_ialpha,0,sizeof(int)*m_inumalpha);

	//pattern configurazione target
	m_itarget_config=new int[m_iNumItems];
	memset(m_itarget_config,0,sizeof(int)*m_iNumItems);

	//sinistra
	if (pimgJawLeft)
	{
		m_pLeftJaw.SetFrameManager(m_lpFm);
		if (!(m_pLeftJaw.CreateComponent(0,360,pimgJawLeft,24))) return E_FAIL;
		m_pLeftJaw.AddRotCoupling(m_vLeftJawJoints[1].x,m_vLeftJawJoints[1].y,0,360,1);
		if (pimghead)
		{
			//viene collegata alla testa
			m_pHead.AddRotCoupling(m_vLeftJawJoints[0].x,m_vLeftJawJoints[0].y,0,360,1);
			//atacca la tenaglia sinistra alla testa
			m_pLeftJaw.LinkToComponent(0,2,&m_pHead);		

		}
		else
		{
			//la testa non � presente e quindi viene collegata al corpo
			m_pBody[numitems-1].AddRotCoupling(m_vLeftJawJoints[0].x,m_vLeftJawJoints[0].y,0,360,1);
			m_pLeftJaw.LinkToComponent(0,2,&m_pBody[numitems-1]);
		}

		m_pLeftJaw.m_iTag1=dwEnergy*2;

	}

	//destra
	if (pimgJawRight)
	{
		m_pRightJaw.SetFrameManager(m_lpFm);
		if (!(m_pRightJaw.CreateComponent(0,360,pimgJawRight,24))) return E_FAIL;
		m_pRightJaw.AddRotCoupling(m_vRightJawJoints[1].x,m_vRightJawJoints[1].y,0,360,1);
		if (pimghead)
		{
			//viene collegata alla testa
			m_pHead.AddRotCoupling(m_vRightJawJoints[0].x,m_vRightJawJoints[0].y,0,360,1);
			//atacca la tenaglia sinistra alla testa
			m_pRightJaw.LinkToComponent(0,3,&m_pHead);		

		}
		else
		{
			//la testa non � presente e quindi viene collegata al corpo
			m_pBody[numitems-1].AddRotCoupling(m_vRightJawJoints[0].x,m_vRightJawJoints[0].y,0,360,1);
			m_pRightJaw.LinkToComponent(0,3,&m_pBody[numitems-1]);
		}

		m_pRightJaw.m_iTag1=dwEnergy*2;
	}

	dwStatus=1;

	return S_OK;
}


//x1,y1=coordinate giunto posteriore
//x2,y2=coordinate giunto anteriore
void CTentacle::SetBodyJoints(int x1,int y1,int x2,int y2)
{
	m_bBodyJoints=true;
	m_vBodyJoints[0].x=x1;
	m_vBodyJoints[0].y=y1;
	m_vBodyJoints[1].x=x2;
	m_vBodyJoints[1].y=y2;
}


//xj,yj=giunto sulla tenaglia
//xb,yb=giunto sul corpo
void CTentacle::SetLeftJawJoints(int xb,int yb,int xj,int yj) //coordinate delle coppie rotoidali sulla corpo e sulla tenaglia
{
	m_bLeftJawJoints=true;
	m_vLeftJawJoints[0].x=xb;
	m_vLeftJawJoints[0].y=yb;
	m_vLeftJawJoints[1].x=xj;
	m_vLeftJawJoints[1].y=yj;
}


void CTentacle::SetRightJawJoints(int xb,int yb,int xj,int yj)
{
	m_bRightJawJoints=true;
	m_vRightJawJoints[0].x=xb;
	m_vRightJawJoints[0].y=yb;
	m_vRightJawJoints[1].x=xj;
	m_vRightJawJoints[1].y=yj;
}

void CTentacle::SetHeadJoint(int xh,int yh) //giunto sulla testa
{
	m_bHeadJoints=true;
	m_vHeadJoints.x=xh;
	m_vHeadJoints.y=yh;
}


HRESULT CTentacle::Draw(void)
{
	//disegna il corpo
	for (int count=0;count<m_iNumItems;count++)
	{
		if (m_pBody[count].m_iTag1<=0) return S_OK;
		
		m_pBody[count].SetDisplay(m_pimgOut);
		m_pBody[count].RenderToDisplay();
	}

	if (m_pHead.m_iTag1>0)
	{
		m_pHead.SetDisplay(m_pimgOut);
		m_pHead.RenderToDisplay();
	}

	if (m_pLeftJaw.m_iTag1>0)
	{
		m_pLeftJaw.SetDisplay(m_pimgOut);
		m_pLeftJaw.RenderToDisplay();
	}

	if (m_pRightJaw.m_iTag1>0)
	{
		m_pRightJaw.SetDisplay(m_pimgOut);
		m_pRightJaw.RenderToDisplay();
	}

	/*

	CMechComponent *p=m_pBody[0].GetParentComponent();

	if (p) p->RenderKinematicChain(m_pimgOut);
	*/

	return S_OK;
}


//imprime una rotazione che si propaga dalla testa fino alla coda
void CTentacle::PushAlpha(int alpha)
{
	float decay=1.0f;
	int ialpha=m_inumalpha-1;
	CMechComponent *p;

	for(int count=1;count<m_inumalpha;count++)
	{
		m_ialpha[count-1]=m_ialpha[count];		
	}

	m_ialpha[m_inumalpha-1]=alpha;

	if (m_pHead.GetStatus())
	{
		m_pHead.SetLambda(m_pHead.GetLambda()+m_ialpha[ialpha--]);
	}

	for (count=ialpha;count>=0;count--)
	{	
		p=&m_pBody[count];
		p->SetLambda(p->GetLambda()+m_ialpha[count]);
	}

	p=p->GetParentComponent();

	p->UpdatePosition();//aggiorna la posizione
}


void CTentacle::DecayAlpha(float decay)
{
	for (int count=1;count<m_inumalpha;count++)
	{
		m_ialpha[count] = 0;
	//	m_Snake[count].SetLambda(m_Snake[count].GetLambda()*decay);
	}

	//aggiorna la configurazione
	m_pBody[0].GetParentComponent()->UpdatePosition();

}

void CTentacle::ResetAlpha(void)
{
//	int lambda;

	for (int count=1;count<m_inumalpha;count++)
	{
	//	lambda=m_pBodySnake[count].GetLambda();

		m_ialpha[count] = 0;
	//	if (lambda<0) m_Snake[count].SetLambda(lambda+15);
	//	else if (lambda>0) m_Snake[count].SetLambda(lambda-15);		
	}
}

//restituisce l'elemento radice da attaccare ad eventuali oggetti genitori
CMechComponent *CTentacle::GetRootComponent(void)
{
	return &m_pBody[0];
}


BOOL CTentacle::Update(void)
{
//	CMechComponent *pm;	

	//da fare

	static int alpha=15;
	int xp,yp;
	int count;
	int ang;				
	int theta,lambda;	
	int rn;
	int ist;

	iStepUpdate++;
	   
	if (iStepUpdate>=iUpdateFreq)
	{
		iStepUpdate=0;

		iStepConfig++;

		if (iStepConfig>=iConfigFreq && m_pmath) 
		{
			iStepConfig=0;	

			rn=m_pmath->RndFast(10);

			if (rn<5) m_tentacle_state=TN_FOLLOW_TARGET; //si muove in modo intelligente inseguendo il player

			else m_tentacle_state=TN_FOLLOW_PATTERN; //segue il pattern di movimento prefissato
		
		}		
		
		if (!m_isteady) //m_isteady indica che in questo momento � fermo perch� vicino la player
		{

			if (m_tentacle_state==TN_FOLLOW_TARGET)
			{
				//insegue il player

				for (count=0;count<m_iNumItems;count++)
				{
					m_pmc=&m_pBody[count];

					lambda=m_pmc->GetLambda(); //angolo relativo
					
					if (count>=m_iper1 && count<m_iper1+m_iperwidth)
					{
						//introduce una perturbazione
						m_pmc->SetLambda(lambda-15);
					}
					else
					{

						//coordinate del primo elemento
						xp=m_pmc->x;
						yp=m_pmc->y;

						ang=m_pmath->GetAngle(xp,yp,lTargetX,lTargetY);
						theta=m_pmc->GetTheta(); //angolo di rotazione assoluto del componente			

						if (m_pmath->GetAngleDiff(theta,ang)>0) m_pmc->SetLambda(lambda+15);
						else m_pmc->SetLambda(lambda-15);
					}

				}
				
				m_iper1++;
				if (m_iper1>m_iNumItems-3) m_iper1=0;

			}

			else
			{
				//segue un pattern di configurazione
				int icmd=m_pcurseq[m_iseqindex];
				if (icmd==-1) PushAlpha(-15);
				else if (icmd==0) PushAlpha(0);
				else if (icmd==1) PushAlpha(15);
				else if (icmd==2) ResetAlpha();

				m_iseqindex++;

				if (m_iseqindex>29) m_iseqindex=0;
			}

		}

		m_pmc=&m_pBody[m_iNumItems-1];

		m_isteady=0;

		//se il player si avvicina alla testa apre e chiude la bocca
		if (abs(m_pmc->x-lTargetX)<100)
		{
			if (abs(m_pmc->y-lTargetY)<100)
			{
				m_isteady=1;

				if (!m_ijaws_state)	
				{
					//avvia la funzione di aprtura della bocca
					m_ijaws_state=1;
					m_ijaws_step=0;
				}
			}
		}

		if (m_ijaws_state!=0)
		{
			if (m_pLeftJaw.GetStatus())
			{
				lambda=m_pLeftJaw.GetLambda();
				
				m_pLeftJaw.SetLambda(-m_ijaws_state*15+lambda);				
			}			

			if (m_pRightJaw.GetStatus())
			{
				lambda=m_pRightJaw.GetLambda();

				m_pRightJaw.SetLambda(m_ijaws_state*15+lambda);
			}
			
			m_ijaws_step++;

			if (m_ijaws_step>=4)
			{
				if (m_ijaws_state==1) m_ijaws_state=-1; //inizia a richiudersi
				else 
				{
					if (m_pLeftJaw.GetStatus()) m_pLeftJaw.SetLambda(0);
					if (m_pRightJaw.GetStatus()) m_pRightJaw.SetLambda(0);

					m_ijaws_state=0; //fine prcedura di apertura/chiusura bocca
				}
				m_ijaws_step=0; //resetta il passo
			}
		}



	}	// fine if iStepUpdate>=iUpdateFreq

	//ciclo armi

	iStepFire++;

	if (iStepFire==iFireFreq && m_pWeapon1)
	{
		CThunderBolt *pTh;
		CSBObject *pobj=NULL;

		iStepFire=0;

		rn=m_pmath->RndFast(10);

		if (rn<6)
		{
			ist=m_pHead.GetStatus();

			m_pmc=&m_pBody[m_iNumItems-1];

			if ((ist && m_pHead.m_iTag1>0) || (!ist && m_pmc->m_iTag1>0))
			{				
				//emette il fulmine solo se il player � davanti
			    ang=m_pmath->GetAngle(m_pmc->x,m_pmc->y,lTargetX,lTargetY);
				
				theta=m_pmc->GetTheta(); //angolo di rotazione assoluto del componente			

				if (abs(ang-theta)<70)
				{			
					//fulmine
					
					pobj=m_pWeapon1->Pop();

				
					if (pobj)
					{
						pTh=(CThunderBolt*)pobj;
						pTh->Reset();
						pTh->AddTargetXY(lTargetX+15,lTargetY+15);
						pTh->SetSource((CADXSprite *)m_pmc);
						pTh->dwClass=CL_ENFIRE;
						pTh->SetDuration(22);			
						g_cs.PlaySound(g_snFire10,0,0);
						m_ActiveStack->Push(pTh);	
					}

				}

			}
		}
	}
	
	return (dwEnergy>0);	
}

void CTentacle::Reset(void)
{
	CEnemy::Reset();
	dwEnergy=100;
	dwPower=10;
	m_tentacle_state=TN_FOLLOW_PATTERN;
	m_iper1=0;
	m_iperwidth=3;
	m_ijaws_state=m_ijaws_step=0;

	
}

void CTentacle::SetEnergy(LONG energy)
{
	dwEnergy=energy;

	for (int count=0;count<m_iNumItems;count++)
	{
		if (count<=3)
		{
			//gli elementi vicini alla radice sono piu' robusti
			m_pBody[count].m_iTag1=dwEnergy*2; //energia di ogni elemento
		}
		else
		{
			m_pBody[count].m_iTag1=dwEnergy; //energia di ogni elemento
		}

	}
}

//controlla la collisione con un altro oggetto
BOOL CTentacle::DoCollisionWith(CSBObject *pobj)
{
	for (int count=0;count<m_iNumItems;count++)
	{
		m_pmc=&m_pBody[count];
		if (m_pmc->m_iTag1>0)
			{
			if (m_pmc->DetectCollision(pobj))
			{
				DamageComponent(m_pmc,pobj);
				return TRUE;
			}
		}
	}

	if (m_pHead.GetStatus())
	{
		if (m_pHead.m_iTag1>0)
		{
			if (m_pHead.DetectCollision(pobj))
			{
				DamageComponent(&m_pHead,pobj);
				return TRUE;
			}
		}
	}

	if (m_pLeftJaw.GetStatus())
	{
		if (m_pLeftJaw.m_iTag1>0)
		{

			if (m_pLeftJaw.DetectCollision(pobj))
			{
				DamageComponent(&m_pLeftJaw,pobj);
				return TRUE;
			}
		}
	}

	if (m_pRightJaw.GetStatus())
	{
		if (m_pRightJaw.m_iTag1>0)
		{
			if (m_pRightJaw.DetectCollision(pobj))
			{
				DamageComponent(&m_pRightJaw,pobj);
				return TRUE;
			}
		}
	}

	return FALSE;
}

//imposta il modulo matematico
void CTentacle::SetMathModule(CADXFastMath *pmath)
{
	m_pmath=pmath;
}

void CTentacle::Explode(void)
{
	ExplodeChain(NULL);
}