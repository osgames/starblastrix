STARBLASTRIX v. 1.0 - readme file

Starblastrix (c) 2006 by Leonardo Berti 

------------------------------------------------------------------------------------

Contents

1. License
2. System requirements
3. Installation
4. Playing the game
5. Credits
6. Contacts

------------------------------------------------------------------------------------
1. License
------------------------------------------------------------------------------------
This game is released under the GNU General Public License,
for more info see license.txt or visit www.gnu.org

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option)
any later version. 

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details. 

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc., 59
Temple Place - Suite 330, Boston, MA 02111-1307, USA.  It can also be
found on the Internet at http://www.fsf.org/copyleft/gpl.html.

------------------------------------------------------------------------------------
2. System requirements
------------------------------------------------------------------------------------

CPU: Intel Pentium or AMD 2.50 MHz
Operating system: Win98/Me/XP (note: the game may not run on some Win98/Me installation)
                  Win XP is recommended 
Memory: 256 MB RAM
Video Card:DirectX compatible (DirectX8 or later is required) 
VideoMode: the game runs @ 640x480 16 bit per pixel (65k colors)
A DirectX sound card is also required.

------------------------------------------------------------------------------------
3. Installation
------------------------------------------------------------------------------------
Run the sb_setup.exe program to install.  
Follow the on screen instructions to install the game to your hard drive.

------------------------------------------------------------------------------------
4. Playing the game
------------------------------------------------------------------------------------
Starblastrix is a side scrolling shoot'em up game in which your task is to complete
the 8 missions destroying as many enemies as you can.Some enemies release powerups and 
additional weapons that you can fire with the Z key on the keyboard or the button 3 
of the joystick.When you get an extra weapon, an icon and a weapon level bar appear
on the top right corner of the screen.
To start the game, run Starblastrix.exe in the installation directory.
Using a joystick/joypad is recommended for this game.
To choose the input device (keyboard or joystick), click
the "options" menu in the game main page, and switch on the keyboard menu
or joystick menu.Use arrow keys and return or use the mouse and left button to navigate the
menu system.To change the state of a switch option, click on the switch by the menu name
or press RETURN on the keyboard.
You can save the control settings and language settings from the input setup page 
by clicking on "save".

KEYBOARD default control settings:

MOVE RIGHT: RIGHT ARROW KEY
MOVE LEFT: LEFT ARROW KEY
MOVE UP: UP ARROW KEY
MOVE DOWN: DOWN ARROW KEY
PRIMARY WEAPON: LEFT CTRL
BOMBS: SPACE BAR
EXTRA WEAPON: Z KEY

JOYSTICK settings:

PRIMARY WEAPON: BUTTON 1
BOMBS: BUTTON 2
EXTRA WEAPON: BUTTON 3

------------------------------------------------------------------------------------
5. Credits
------------------------------------------------------------------------------------
Programming:
Leonardo Berti
Graphics:
Leonardo Berti
Some graphics is based on royalty-free images available from the Internet
Music:
free royalty-free MIDI files in the music subdir are form www.partnerinrhymes.com

------------------------------------------------------------------------------------
6. Contacts
------------------------------------------------------------------------------------

For more info contact leonardo.berti@tin.it

------------------------------------------------------------------------------------
The end
------------------------------------------------------------------------------------












                  

