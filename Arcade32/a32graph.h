/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  18-11-2001
  --------------------------------------------------------------
  A32GRAPH.H 
  Grafica 2D

  ***************************************************************/

//Impostazioni VCC
//Da Project/Settings  aggiungere (per tutte le configurazioni) alle librerie della sezione Link  ddraw.lib dxguid.lib winmn.lib e tutte le altre che servono
//In Tool/Options impostare le directory della sezione Include su C:\dxsdk7/include facendo in modo che appaia per prima nella lista altrimenti prende quella del Vcc che � piu' vecchia
//In Tool/Options impostare le der. della sezionone Library su C:\dxsdk7/lib
//In Tool/Options impostare le der. della sezionone Sources sulla dir. in cui si trovano i file comuni arcade32.cpp ecc..



#ifndef _A32GRAPH_
#define _A32GRAPH_

//#include <ddraw.h>
//#include <windows.h>          //contiene definizioni base per windows
//#include <windowsx.h>         //contiene la definizione di alcune macro importanti in windows
//#include <winbase.h> 
//#include <wingdi.h>         
//#include <mmsystem.h>
//#include <malloc.h>
#include <stdio.h>
#include <math.h>
#include "a32debug.h"		  //libreria per debug direct draw

//-------------------------- DEFINIZIONI ----------------------------------

#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }

//Se � definito USEFASTBLT, le funzioni di blitting vengono realizzate
//usando BltFast in vece di Blt, inoltre la funzione SetGraphMode
//non attacca i clipper alle superfici primaria e backbuffer.
//USEFASTBLT non deve essere definito se si crea un'applicazione DirectDraw in modalit� Windowed (cio� in finestra e non a tutto schermo)
//Se Direct Draw usa l'hardware, non c'� differenza di velocit� fra Blt e BltFast
      

//--------------------------- STRUTTURE --------------------------------

#ifndef _DDGRAPHICS_INCLUDE_
//le seguenti definizioni vengono elaborate solo se non si include ddgraphics

#define BITMAP_ID 0x4D42  //identificatore dei bitmap

#define DEG_TO_RADIANS (float)0.01745329252     //fattore di conversione gradi->radianti
#ifndef MAX_ANGLE
	#define MAX_ANGLE 361                     //dimensioni delle tabelle trigonometriche
#endif
#define MAX_FILL_BUFFER 150               //buffer funzione FillArea 

// inizializza una struttura direct X
#define DD_INIT_STRUCT(ddstruct) { memset(&ddstruct,0,sizeof(ddstruct)); ddstruct.dwSize=sizeof(ddstruct); }
//macro per la gestione della tastiera
#define ABS(val) ((val<0) ? -val : val)

//macro per la creazione di valori rgb 
// colore a 16 bit formato 555
#define RGB16BIT555(r,g,b) ((b & 31) + ((g & 31) << 5) + ((r & 31) << 10))

// colore a 16 bit formato 565
#define RGB16BIT565(r,g,b) ((b & 31) + ((g & 63) << 5) + ((r & 31) << 11))

// colore a 24 formato 888 
#define RGB24BIT(a,r,g,b) ((b) + ((g) << 8) + ((r) << 16) )

// colore a 32 bit (8 bit per canale alpha)
#define RGB32BIT(a,r,g,b) ((b) + ((g) << 8) + ((r) << 16) + ((a) << 24))


//struttura che contiene una immagine 
typedef struct IMAGE_FRAME_TAG
        {

        DWORD width;                         //larghezza in pixel
        DWORD height;                        //altezza in pixel
		DWORD width_fill;                    //� la larghezza da aggiungere a width per ottenere una larghezza multipla di 8
        int effect;
        DWORD transp_color;                    //usato in modalit� con palette, � l�indice nella pal. del colore trasparente
        int status;                          //settato su 1 sel buffer � stato caricato		
		RECT rcClip;                         //rettangolo sogente durante il clipping
		LPDIRECTDRAWSURFACE7 surface;         //superficie direct draw che contiene l'immagine
		BYTE *pbuffer;                       //punttore al buffer immagine 
       
        } IMAGE_FRAME, *IMAGE_FRAME_PTR;

// struttura che contiene i dati di un file bitmap
typedef struct BITMAP_FILE_TAG
        {
        BITMAPFILEHEADER bitmapfileheader;  // contiene l'header del file bitmap
        BITMAPINFOHEADER bitmapinfoheader;  // contiene le informazioni inclusa la palette       
		PALETTEENTRY     palette[256];      // palette
        UCHAR            *buffer;           // puntatore ai dati      
        int              status;            // 1=bitmap caricato 
        } BITMAP_FILE, *BITMAP_FILE_PTR;

#endif

#define VPX_ID 0x5856;     //VX identificatore file VPX

//header di un file VPX
typedef struct VPX_HEADER_TAG
        {

			 WORD  id;            //id del file � impostato su "VX" 
			 CHAR *szAuthor[30];  //autore
			 DWORD dwBitsPerPixel; //bit per pixel                 		 			 
			 DWORD dwOffset;      //offset dell'intestazione della prima immagine dall'inizio del file
			 DWORD dwCompression;  //compressione : puo' essere BI_RGB (non compresso usato per bitmap 16,24 bit o BI_RLE8)
								  //il formato compresso supportato � lo stesso dei bitmap compressi		             
								  //con RLE 8 bit(la stessa dei bitmap).
								  //wCompression=1L => BI_RLE8 , 0L= BI_RGB
			 WORD  wVal2;         //non usato
        } VPX_HEADER,*VPX_HEADER_PTR;

//header di un'immagine in un file VPX
typedef struct VPX_IMAGE_HEADER_TAG
        {

	     DWORD dwWidth;         //larghezza in pixel
		 DWORD dwHeight;        //altezza in pixel	
		 DWORD dwExpandedSize;  //numero totale di bytes espansi escluso il riempimento = dwWidth*dwHeight*bitsperpixel
		 DWORD dwSize;          //dimensione in bytes del buffer che segue l'intestazione
         DWORD dwReserved;      //non usato

        } VPX_IMAGE_HEADER,*VPX_IMAGE_HEADER_PTR;


//i file VPX contengono immagini compresse o vettori di immagini compresse
//un file VPX � costituito da un'intestazione VPX_HEADER
//seguita dalla palette costituita da un array di 256 strutture RBGQUAD se wBitsPerPixel=8
//dopo la palette seguono le immagini, ogni immagine
//� costituita da un'intestazione VPX_IMAGE_HEADER
//e dal buffer dati compresso
//le immagini sono salvate capovolte nello stesso modo dei bitmap compressi

typedef struct VPX_FILE_TAG
        {

		 VPX_HEADER vpxfileheader;
		 int status ;  //settato a 1 se � carico
		 PALETTEENTRY palette[256];		   
		 CHAR *lpszFileName;
		 DWORD dwNumImages;  //numero di immagini nel file

        } VPX_FILE, *VPX_FILE_PTR;

#ifndef _CIMGBUFFER16_ 

//struttura usata per contenere informazioni riguardo alla modalit� 16 bit (High Color)
typedef struct
{

	DWORD dwRBitMask,
		  dwGBitMask,
		  dwBBitMask;

    RGBQUAD Position;   

	DWORD dwRBits; //numero di bits riservati per il rosso
	DWORD dwGBits; //  "      verde   
	DWORD dwBBits; //  "      blu questi valori cambiano a seconda della modalita' 565 o 555
	DWORD dwShrR;  //numero di bit da shiftare a dx per convertire il pixel nel formato 565 o 555
	DWORD dwShrG;
	DWORD dwShrB;

} RGB16;

#endif

//tipo tile (usato da CADXMap)

typedef struct MAP_TILE_TAG
{
	DWORD dwFrame;     //frame da mostrare in questo tile (se � <0 il tile � vuoto)  
	int status;     //stato (di uso generico)
	int tag;        //tag (generico)

} MAP_TILE,*MAP_TILE_PTR;

//----------------------------- CLASSI -----------------------------------

//////////////////////////// CADXGraphicManager //////////////////////
//Implementa le funzioni generali per la grafica Direct Draw
class CADXGraphicManager: public virtual CADXDebug
{

	friend class CADXFrameManager;            //la classe frame manager deve poter accedere ai membri privati di questa classe
    friend class CADXMap;

private: 
     
	LPDIRECTDRAW7 lpDD;                         //oggetto DD principale	
	LPDIRECTDRAWSURFACE7 *lpDDS;                //flipping chain compreso la sup. primaria  
	IMAGE_FRAME_PTR m_imgScreen;     //image frame che incapsula le superfici della flipping chain
	LPDIRECTDRAWSURFACE7 lpDDSCurrent;          //punta la sup. corrente
	LPDIRECTDRAWCLIPPER *lpClipper; //clipper da attaccare alle varie superfici	
	LPDIRECTDRAWPALETTE lpDDpalette;	
	DDSCAPS2 ddscaps;
	UCHAR*              g_primary_buffer;       //buffers della superficie primaria e secondaria
	UCHAR*              g_secondary_buffer;
	RGB16 g_rgb16;                                //informazioni sulle maschere di bit dei pixel in modalit� 16 bit
	int g_screen_width;							//larghezza in pixel settata con DD_Init 
	int g_screen_height;						//altezza
	int g_screen_bpp;							//profondit� colore bit per pixel
	int g_primary_lpitch;
	int g_secondary_lpitch;
	int *m_lpitch;                  //pitch delle superfici
	int m_current_visible_surface;               //superficie visibile corrente
	int m_current_drawing_surface;               //superficie in uso corrente
	int m_iBackBuffers;                          //numero di back buffers 
	DDBLTFX m_Clsddbltfx;                        //struttura usata per cancellare una superficie

	
	PALETTEENTRY m_current_text_color;           //colore di primo piano del testo
	PALETTEENTRY m_current_text_bkgcolor;        //colore di background predefinito
	PALETTEENTRY m_pal[256];                     //palette 8 bit corrente
	int m_itextbkmode;                           //testo opaco o trasparente  
	HWND g_hwnd;		
	//e la posizione dei caratteri nel file di caratteri *.VPX
	
	BOOL g_bFullScreen;                         //modalit� full screen
    int m_status;                               //settato su uno se l'inizializzazione � avvenuta    
    static int iInstanceCounter;            //contatore istanze
	RECT m_rcScreen;                        //rettangolo di clipping dello schermo  
	float SinTable[MAX_ANGLE];					//tabella della funzione seno
	float CosTable[MAX_ANGLE];					//tabella della funzione coseno


protected:
    
	//void (*fnptrSetPixel) (int,int,COLORREF,LPDIRECTDRAWSURFACE7); //punta alla funzione putpixel; questo puntatore cambia in base alle impostazioni windowed o full screen	
    
	BOOL DD_Fill_Surface(LPDIRECTDRAWSURFACE7 lpdds,DWORD color);    
    void SetPixel(int x,int y,COLORREF color,LPDIRECTDRAWSURFACE7 pDDS); 
	void SetPixelWin(int x,int y,COLORREF color,LPDIRECTDRAWSURFACE7 pDDS); 
    void SetPixel(int x,int y,int color,UCHAR *buffer,int lpitch);    
    UCHAR *LockSurface(LPDIRECTDRAWSURFACE7 lpdds,int *lpitch);
    BOOL UnlockSurface(LPDIRECTDRAWSURFACE7 lpdds, UCHAR *surface_buffer);
    LPDIRECTDRAWCLIPPER AttachClipper(LPDIRECTDRAWSURFACE7 lpdds, int num_rects, LPRECT clip_list);
    DWORD GetPixel(int x, int y,UCHAR *buffer,int lpitch);
    int DDTextOut(int xout,int yout,LPDIRECTDRAWSURFACE7 lpDDS,char *szText,PALETTEENTRY *color); 
   	
public:	
   
    //interfaccia da utilizzare per la gestione della grafica	
	CADXGraphicManager(void);
	~CADXGraphicManager(void);
	//Imposta la modalit� grafica ed inizializza Direct Draw
	//height,width sono altezza e larghezza in pixel dello schermo nella modalit� grafica che si vuol impostare
	//bpp � la profondit� colore (8,16,24 bit),bFullscreen indica se si imposta la modalit� a schermo intero,iBackBuffers � il numero di back buffer da creare.Il numero minimo � 1 (default)
    HRESULT SetGraphMode(int width,int height,int bpp,HWND hwnd,BOOL bFullScreen,int iBackBuffers=1);
    HRESULT UseScreen(int screen_num);  
	HRESULT UseBackBuffer(void); //imposta come superficie di disegno corrente la prima superficie non visibile a disposizione
	HRESULT ShowBackBuffer(void);
	HRESULT FlipScreen(int iScreen);
	HRESULT FlipScreenFast(int iScreen);
	HRESULT GetFlipStatus(int iScreen);
	int GetScreenWidth(void);
    int GetScreenHeight(void);
	int GetScreenBitsPerPixel(void);
	int GetScreenNum(void);
    int GetStatus(void);  
	LPDIRECTDRAW7 GetDDObject(void);
	IMAGE_FRAME_PTR GetScreenImage(int screen_num); //restituisce il puntatore alla superficie
    UINT GetVisibleScreen(void); //restituisce l'indice della superficie visibile
	UINT GetDrawingScreen(void); //restituisce l'indice della superficie attiva per il disegno

	//funzioni grafiche
    void SetPixel(int x,int y,COLORREF color);
	COLORREF GetPixel(int x,int y);
	//queste funzioni hanno gli stessi paramatri in modo da poter usare dei
	//puntatori a funzione, a seconda della modalit� impostata punto la funzione che mi interessa
	COLORREF GetPixel16(DWORD x,DWORD y,BYTE *buff,DWORD dwPitch);
	COLORREF GetPixel24(DWORD x,DWORD y,BYTE *buff,DWORD dwPitch);

    void DrawLine(int x1,int y1,int x2,int y2,COLORREF color);
    void DrawRectangle(LONG x,LONG y,LONG x1,LONG y1,COLORREF fill_color);
	void DrawPolygon(LONG xc,LONG yc,DWORD radius,DWORD sides,COLORREF color,DWORD start_angle);
	void DrawCircle(int cx,int cy,int radius,COLORREF color);
    void Cls(COLORREF color);
	void Cls(COLORREF color,IMAGE_FRAME_PTR img);
    int FillArea(int x,int y,DWORD dwColor,DWORD dwBound,int mode);
	void ShutDown(void);
    void SetTextColor(BYTE red,BYTE green,BYTE blue);
	void SetTextBackColor(BYTE red,BYTE green,BYTE blue);
    void SetTextBkgMode(DWORD mode);
    void CDECL TextOut(int xout,int yout,char *szFormat, ...);
	void SetPaletteEntry(BYTE num_entry,BYTE r,BYTE g,BYTE b);
    void SetAllPalette(PALETTEENTRY *pal);
	COLORREF CreateRGB( int r, int g, int b );
	void SplitRGB(PALETTEENTRY *plDest,COLORREF dwSrc,DWORD bpp);
	DWORD GetNearestPaletteIndex(int r,int g,int b,PALETTEENTRY *cpal);
	void GetPaletteEntries(PALETTEENTRY *cpal);
	RGB16 GetPixelFormat(void);
    void CreatePaletteEntry(PALETTEENTRY *pe,BYTE red,BYTE green,BYTE blue);
	void CreateCoolPalette(PALETTEENTRY *pe,BYTE iStart,BYTE iEnd,PALETTEENTRY *pStart,PALETTEENTRY *pEnd);
	void DisplayPalette(void);
	BOOL FadePalette(PALETTEENTRY dest_color,LPDIRECTDRAWPALETTE lpDDpal,int *iExclude,int iSize,int iSteps,BOOL initcycle);
	BOOL FadeColors(const COLORREF clDestColor,const int iSteps,IMAGE_FRAME_PTR pFrm,BOOL bInitCycle);
	BOOL FadeScreen(const COLORREF clDestColor,const int iSteps,BOOL bInitCycle);
	BOOL IsWindowed(void);
	
};

/////////////////////////////////// CADXFrameManager ////////////////////////////////
//Implementa le funzioni necessarie alla manipolazione delle frame

class CADXFrameManager:public virtual CADXDebug
{
		
private:

	int  m_bpp;                             //bit x pixel   	
    CADXGraphicManager *m_lpGm;             //punta all'oggetto graphic manager
    COLORREF transp_color;                  //colore trasparente corrente    
	COLORREF dest_transp_color;             //colore trasparente sulla sup. destinazione
	int m_status;                           //settato su 1 se il sistema grafico � inizializzato
    static int iInstanceCounter;            //contatore istanze   
    DWORD m_dwScreenWidth;                    //dimensioni schermo acquisite tramite l'oggetto graphic manager
	DWORD m_dwScreenHeight;        
	DDSURFACEDESC2 m_ddsd;
	LONG m_lTotmem;                         //memoria totale allocata per le superfici
	
protected:
   
	DWORD   m_dwDefCaps; //paramatro di default per la creazione delle superfici
	HRESULT CopyPixels(BYTE *dest_buffer,BYTE *src_buffer,DWORD dwDestPitch,DWORD dwSrcPitch,DWORD dwSrcWidth,DWORD dwHeight,DWORD dwDestBitCount,DWORD dwSrcBitCount,PALETTEENTRY *plSrc);
    HRESULT CreateImageHeader(IMAGE_FRAME_PTR frm,VPX_IMAGE_HEADER_PTR vpxHead);	
	DWORD   EncodeRle8(BYTE *dest,BYTE *src,DWORD dwSize);
    HRESULT DecodeRle8(BYTE *dest,BYTE *src,DWORD dwSize);
    DWORD   EncodeRle16(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight);
	HRESULT DecodeRle16(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight);
	DWORD   EncodeRle24(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight);
	HRESULT DecodeRle24(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight);
	HRESULT ConvBuffer565(BYTE *pBuffer,DWORD dwBuffSize); //converte un buffer a 16 bit nel formato 565
	HRESULT ConvBuffer555(BYTE *pBuffer,DWORD dwBuffSize); //formato 555
	HRESULT GetFileHeader(VPX_HEADER *pvpxHead,LPCSTR lpszFileName);
    DWORD GetPitchLength(DWORD dwWidth,DWORD dwBpp);
    DWORD GetSurfacePitch(LPDIRECTDRAWSURFACE7 lpDDS);

public:

	RECT rcClip;//rettangolo di clipping video     
 	
	CADXFrameManager(void);
	~CADXFrameManager(void);
    HRESULT SetGraphicManager(CADXGraphicManager *pgm);
	CADXGraphicManager *GetGraphicManager(void);
	int GetStatus(void);
	//bitmap
	int LoadBitmap(BITMAP_FILE_PTR bitmap,char *filename);
	HRESULT SaveImgToBitmap(TCHAR *szFileName,const IMAGE_FRAME_PTR pimgSrc); 
	HRESULT CreateImgFrame(IMAGE_FRAME_PTR frm,BITMAP_FILE_PTR bitmap);
	HRESULT CreateImgFrame(IMAGE_FRAME_PTR frm,char *filename);
    //buffers
    void FreeBitmap(BITMAP_FILE_PTR bitmap);   
	BYTE *GetBuffer(IMAGE_FRAME_PTR frm,LONG *lPitch);
	HRESULT ReleaseBuffer(IMAGE_FRAME_PTR frm,BYTE *buffer=NULL);	 
    //frames
    HRESULT CreateFrameSurface(IMAGE_FRAME_PTR frm,DWORD width,DWORD height);
	HRESULT CreateRotFrame(int angle,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,BOOL bFitSize=TRUE); 
    HRESULT CreateScaledFrame(int ScaleFactor,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc);
    HRESULT CreateScaledFrame(DWORD dwNewWidth,DWORD dwNewHeight,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc);
	HRESULT CreateMirrorFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc);
	HRESULT CreateTopDownFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc);
	HRESULT DuplicateImgFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc); 
	HRESULT GrabFrame(IMAGE_FRAME_PTR frmDest,RECT *rcS,BITMAP_FILE_PTR bitmap);
    HRESULT GrabFrame(IMAGE_FRAME_PTR frmDest,LONG left,LONG top,LONG right,LONG bottom,IMAGE_FRAME_PTR frmSrc);
	HRESULT GrabFrame(IMAGE_FRAME_PTR frmDest,RECT *rcS,IMAGE_FRAME_PTR frmSrc);
    HRESULT CaptureScreen(IMAGE_FRAME_PTR pimgDest);
	//funzioni di blitting
	HRESULT PutImgFrame(LONG xout,LONG yout,IMAGE_FRAME_PTR frm);  
	HRESULT PutImgFrame(LONG xout,LONG yout,IMAGE_FRAME_PTR frm,int iBltEfcts);	
	HRESULT PutImgFrameClip(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,int iBltEffcts=DDBLT_WAIT | DDBLT_KEYSRC);
    HRESULT PutImgFrameResize(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,LONG lNewWidth=-1,LONG lNewHeight=-1,int iBltEffcts=DDBLT_WAIT | DDBLT_KEYSRC);
	HRESULT PutImgFrameRect(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,LPRECT prcSrc,int iBltEffcts=DDBLT_WAIT | DDBLT_KEYSRC);    
	HRESULT PutImgRotFrame(LONG xc,LONG yc,int angle,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,int iBltEffcts=DDBLTFAST_SRCCOLORKEY);
	void FreeImgFrame(IMAGE_FRAME_PTR frm);	
	void Cls(IMAGE_FRAME_PTR pFrm,COLORREF color);
	//------- vpx files ----------//
	HRESULT CreateFileVPX(LPCSTR lpszFileName,WORD bpp,LPCSTR lpszAuthor,PALETTEENTRY *pal);	    
	HRESULT SaveToFileVPX(LPCSTR lpszFileName,IMAGE_FRAME_PTR pfSrc);	
	BOOL    IsValidVPX(LPCSTR lpszFileName);
	HRESULT LoadVPXFile(VPX_FILE_PTR pVPXFile,LPCSTR lpszFileName);
	HRESULT CreateImgFrameFromVPX(IMAGE_FRAME_PTR pfDest,VPX_FILE_PTR pVPXFile,DWORD dwFrame);
	HRESULT CreateImgFrameFromVPX(IMAGE_FRAME_PTR pfDest,LPCSTR lpszFileName,DWORD dwFrame);	
	HRESULT FreeVPXFile(VPX_FILE_PTR pVPXfile);
	HRESULT SetTranspColor(COLORREF color);
	DWORD GetDefaultCaps();
	LONG GetTotmem(void);
	void SetDefaultCaps(DWORD dwCaps);
	//Estrae da una immagine una striscia orizzontale di frames
	HRESULT GrabFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
	//Carica un vettore di frame disposte in senso vertcale
	//i parametri hanno lo stesso significato della funzione precedente
	HRESULT GrabVFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
	//rilascia un vettore di frames allocate precedentemente
	void FreeFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames);
	
};


//////////////////////////// CADXBase /////////////////////////////////
//Classe astratta CADXBase

class CADXBase
{

private:

protected:

	int m_iStatus;
	int m_iBltEffcts;	

public:

	CADXFrameManager *m_lpFm;
	CADXGraphicManager *m_lpGm;
	CADXBase(void);
	~CADXBase(void);
	virtual HRESULT SetFrameManager(CADXFrameManager *pfm);
	virtual HRESULT SetGraphicManager(CADXGraphicManager *pgm);
	virtual int GetStatus();
	virtual CADXGraphicManager *GetGraphicManager(void);
	virtual CADXFrameManager *GetFrameManager(void);

};



//------------------------------ CADXRender ---------------------------------------
//classe astratta CADXRender
//� usata per fornire metodi di rendering su immagini diverse dal video
class CADXRender:public virtual CADXBase
{
protected:

	int m_iBltEffcts;
	RECT m_rcClip; //rettangolo di clipping del display

public:

	IMAGE_FRAME_PTR m_pimgOut; //questo membro � bubblico per aggiornarlo piu' velocemnte
	CADXRender(void);
	virtual HRESULT SetDisplay(IMAGE_FRAME_PTR imgDisplay);
	virtual void SetBlitEffects(int iBltEffcts);
	virtual HRESULT RenderToDisplay(void);
	virtual HRESULT RenderToDisplay(LONG x,LONG y);	
};



#endif
