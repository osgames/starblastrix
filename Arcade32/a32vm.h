/*
   ARCADE 32 VIRTUAL MACHINE
   Code by Leonardo Berti 2004 (c)
   Macchina virtuale usata per l'esecuzione di script
   
   N.B. questo codice deve essere mantenuto cross-platform : usare solo la libreria standard del C

 */
/*
ESEMPIO;

int prova=10;
int pippo;
pippo=190;
int v[]={11,33,44,32,11,33,22,77,88,99,10};
string a="wiwi";
string b[]={"ksks","sks","lslsk","ksks"};
b[0]=190;
b[1]=191;
b=miafunz(1920,prova);

 */

#ifndef _A32VM_
#define _A32VM_

//tipi base
#ifndef BYTE
#define BYTE unsigned char   //8 bit
#endif
#ifndef WORD
#define WORD unsigned short  //16 bit
#endif
#ifndef DWORD
#define DWORD unsigned int //32 bit
#endif
#ifndef LONG
#define LONG int //32 bit con segno
#endif

//------------------------------------------------------------------------------------
/*nota: le parentesi [] indicano  un indirizzo es, [symbol] indicano un indirizzo di una variabile
                                                 [int]=indirizzo di un intero ecc... */

/* tabella dei codici delle istruzioni
   legenda:

   int=valore intero immediato (32 bit)
   float=valore float immediato
   [addr]=indirizzo (numero intero 32 bit)

   
   ogni istruzione � costituita da un byte che indica l'operazione
   i byte successivi indicano il tipo degli operandi; a seconda del tipo di istruzione 
   ci possono essere 1 2 3 o nessun operando

   il primo byte dopo l'opcode indica la destinazione e il secondo il sorgente quando si tratta di un trasferimento di memoria
   

*/

//A32VM instruction set

#define OP_NOP   0x00  //no operation
#define OP_MOV   0x01  //mov dest,src: sposta dati da un indirizzo ad un altro , da un registro all'altro ecc...
#define OP_MOVSB 0x02  //sposta m_CX byte da SI a DI
#define OP_PUSH  0x03  //immette dati nello stack
#define OP_ADD   0x04  //add dest,src  addizione aritmetica
#define OP_SUB   0x05  //SUB dest,src sottrazione fra interi 
#define OP_DIV   0x06  //DIV dest src :divisione fra interi
#define OP_MUL   0x07  //MUL dest,src :moltiplicazione
#define OP_POP   0x08  //POP dest :estrae dati dallo stack  
#define OP_CALL  0x09  //CALL n :chiamata a una funzione 
#define OP_INC   0x0A  //INC dest :incremento
#define OP_DEC   0x0B  //DEC dest :decrementa
#define OP_END   0x0C  //END :termina l'esecuzione 

/*  defnizione del tipo di operandi:

*/

#define R_AX 0x01
#define R_BX 0x02
#define R_CX 0x03
#define R_DX 0x04
#define R_SI 0x05   //l'operando � indirizzo puntato da SI
#define R_DI 0x06   //l'operando � l'indirizzo puntato da DI
#define R_SP 0x07
#define R_FN 0x08
#define R_ST0 0x09  //registri floating point
#define R_ST1 0x0A
#define R_ST2 0x0B
#define R_ST3 0x0C
#define R_IMMINT 0x0D  //valore costante intero
#define R_IMMFLOAT 0x0E //valore costante float
#define R_IMMADDR 0x0F //indirizzo di memoria

//--- error codes -------------------------------------------------------------------

#define VMERR_INVALIDADDRESS 0x01
#define VMERR_COPYTOCONST 0x02
#define VMERR_INVALIDOPERANDS 0x03
#define VMERR_OUTOFMEMORY 0x04
#define VMERR_OPERANDTYPEDONOTMATCH 0x05

//------------------------------------------------------------------------------------

//valori di default
#define DATA_MEM_SIZE 1024 //memoria usata per i dati in bytes
#define STACK_MEM 256 //memoria dello stack in bytes
#define CODE_MEM 1024 //memoria usata per il codice
#define MAX_SYMBOLS 200 //numero massimo di simboli nella sym table

//--- tipi
#define TP_INT 0x0
#define TP_STRING 0x1
#define TP_FLOAT 0x2

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#endif
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif

//stato della virtual machine
#define VM_NONE 0x0
#define VM_INITIALIZED 0x1
#define VM_CODE_LOADED 0x2
#define VM_ERROR 0x3
#define VM_RUNNING 0x4

#define MAX_EXT_FN 20


typedef struct SYM_TABLE_ITEM
{
	char *name;   //nome del simbolo
	DWORD address; //indirizzo nella memoria
	size_t size;     //dimensione allocata in byte
	int type; //tipo es, TP_INT ecc...
	SYM_TABLE_ITEM *next; //linked list

} SYM_TABLE_ITEM,*SYM_TABLE_ITEM_PTR;

//semplice macchina virtuale che esegue lo script
class CA32VirtualMachine
{
private:

	int m_state; //stato della mcchina virtuale


	SYM_TABLE_ITEM *sym_table;
	SYM_TABLE_ITEM *sym_table_start; //primo elemento della sym table
	void (*m_fntable[MAX_EXT_FN])(void); //tabella delle funzioni esterne
	size_t m_data_mem; //memoria globale
	size_t m_stack_mem; //memoria riservata allo stack
	size_t m_code_mem;
	int m_imax_instructions; //numero massimo di istruzioni inseribili
	int m_iloaded_instructions; //istruzioni caricate
	BYTE *m_code; //codice eseguibile
	BYTE *m_codetop; //punta il top della memoria m_code
	BYTE  *m_data; //dati
	BYTE  *m_stack; //stack
	int m_iExitCode; //codice di uscita del programma , 0 tutto ok 1 c'� un errore

	//registri
	int m_RINTA,m_RINTB,m_RINTC;
	float m_RFLTA,m_RFLTB,m_RFLTC;
	char *m_RSTRA,*m_RSTRB,m_RSTRC;
	char *m_szError; //errore che si � verificato
	int m_iErrorCode;

	//registri della memoria
	DWORD m_SP; //stack pointer
	DWORD m_DP; //data pointer;
	DWORD m_FN; //contatore tabella delle funzioni esterne (indice della tabella m_fntable)
	DWORD m_IP; //instruction pointer
	DWORD m_DI; //destination address
	DWORD m_SI; //source address
	//registri integer general porpuse
	LONG m_AX;
	LONG m_BX;
	LONG m_CX;
	LONG m_DX;
	//registri float
	float m_ST0,m_ST1,m_ST2,m_ST3;	
	//flags
	int m_f1,m_f2,m_f3;	

protected:

	void DeleteSymTable(SYM_TABLE_ITEM_PTR ptable); //cancella e dealloca la sym table
	void DeleteFnTable(void); //dealloca la tabella delle funzioni
	int AddValue(int value); //aggiunge un valore in memoria al DP corrente
	int AddValue(float value);
	int AddValue(BYTE *value);
	int AddSymbol(char *name); //aggiunge un simbolo alla sym table
	int GetSymbol(SYM_TABLE_ITEM *symbol,char *name); //restituisce la definizione di un simbolo nella sym table
	int SetSymbol(char *symbol,int value,DWORD offset=0); //imposta il valore di un simbolo
	int SetSymbol(char *symbol,float value,DWORD offset=0);
	int SetSymbol(char *symbol,BYTE *buff,DWORD offset=0);
	int ExecuteInstr(void); //esegue l'istruzione corrente
	void SetError(const char *msg,const int icode);
	void ResetVirtualCPU(void);
	BYTE *GetOpAddress(BYTE op,size_t *nsize);

public:

	CA32VirtualMachine(void);
	~CA32VirtualMachine(void);
	void Reset(void);
	int Reset(unsigned long memdata=DATA_MEM_SIZE,unsigned long memcode=CODE_MEM,unsigned long memstack=STACK_MEM);
	//fa ripartire il programma dall'inizio
    void Restart(void); 
	int AddSymbol(int value,char *name); //aggiunge un symbolo di tipo int alla sym table
	int AddSymbol(float value,char *name);
	int AddSymbol(char *buff,char *name);
	int AddInstruction(BYTE opcode,BYTE op1,BYTE op2);
	int AddInstruction(BYTE opcode,BYTE op1);
	int AddInstruction(BYTE opcode);
	int AddInstruction(BYTE opcode,BYTE op1,BYTE op2,int v1);
	int AddInstruction(BYTE opcode,BYTE op1,BYTE op2,int v1,int v2);	
	int GetSymbolInt(int *dest,char *name);
	int GetSymbolString(char **dest,char *name); 
	int GetSymbolFloat(float *dest,char *name);
	int AddExtFnCall(void (*fn)(void)); //aggiunge un puntatore ad una funzione esterna che viene chiamata dalla macchina virtuale
	//restituisce l'indirizzo virtuale di un certo simbolo
	int GetAddressBySymbol(char *name);
	int Execute(int *opcode,int *op1,int *op2); //esegue una istruzione e rende il codice dell'operazione e gli operandi
	int Execute(void); //esegue l'istruzione corrente
	int GetInstrAddr(void); //indirizzo dell'istruzione corrente
	int GexMaxInstructions(void); //rende il numero massimo di istruzioni caricabili
	int GetLoadedInstructions(void); //rende il numero di istruzioni del programma caricato
	int LoadObjCode(char *fname); //carica codice copilato in formato binario da un file
	int SaveObjCode(char *fdest); //salva il codice compilato su file binario
	int GetExitCode(void); //restituisce il codice di uscita del programma
	int GetLastError(char **err,int *icode=NULL); //restituisce l'ultimo errore

};

#endif