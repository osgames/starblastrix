/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32FASTM.H
  funzioni matematiche,trogonometriche 2D e fixedpoint

  ***************************************************************/


#ifndef _A32FASTMATH_
#define _A32FASTMATH_

#include <math.h>
#include <stdlib.h>
#include <time.h>

#ifndef MAX_RND
	#define MAX_RND 10000             //tabella Rnd
#endif

#ifndef MAX_ANGLE
	#define MAX_ANGLE 361
#endif

#ifndef DEG_TO_RADIANS 
	#define DEG_TO_RADIANS  (float) 0.0174533     //fattore di conversione gradi->radianti
#endif

#define M_PI (float) 0.3145927


//matematica fixed point
//tipo fixedpoint a 10 bit
#ifndef FIXED_T

	typedef long FIXED_T; //numeri fixed point

	#define FIXEDTOLONG(val) (val >> 10) //converte da fixed type a long (vengono usati 10 bit per i decimali)
	#define LONGTOFIXED(val) (val << 10) //converte da long a fixedtype

#endif


class CADXFastMath
{
private:


protected:

	int m_status; //impostato su 1 quando le tabelle sono inizializzate
	unsigned long m_dwMaxRnd;
	unsigned long RndTable[MAX_RND];     

public:

	CADXFastMath(void);
	~CADXFastMath(void);
	//inizializza le tabelle delle varie funzioni
	void InitTables(void);
	int IsInitialized(void);//rende 1 se la classe � gi� stata inizializzata
	float SinTable[MAX_ANGLE];					//tabella della funzione seno
	float CosTable[MAX_ANGLE];					//tabella della funzione coseno
	float TanTable[MAX_ANGLE];					//tabella della funzione tangente
    FIXED_T CosFast[MAX_ANGLE];					//tabelle del seno e coseno intere (valore reale x 1000)
	FIXED_T SinFast[MAX_ANGLE];    
	FIXED_T AcosFast[1024];                //tabella arcocoseno
	unsigned int FastSqrt(long r);                   //radice quadrata veloce
	int GetAngle(long x1,long y1,long x2,long y2);    //restituisce l'angolo del vettore che ha come punto di applicazione x1,x2 ed estremo x2,y2 
	int GetAngleDiff(int angle,int angle_target);
	int GetAngleDiff(int angle,int angle_target,int tolerance); //in questo caso usa la tolleranza
	int Randomize(unsigned long dwMaxRndValue); //ricrea la tabella dei numeri casuali        
	int Sgn(int x); //rende 1 se positivo -1 se nagatico 0 altrimenti
	unsigned long RndFast(unsigned long seed);               //genera un numero casuale in [0,seed] 
 
};


#endif
