/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  18-11-2001
  --------------------------------------------------------------
  A32GRAPH.CPP
  Grafica 2D

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/


#define WIN32_LEAN_AND_MEAN   
 
#include "a32debug.h"
#include "a32graph.h"

#ifdef _DEBUG_

#include <crtdbg.h>

#endif

//-------------inizializzazione dei membri statici ----------------------

int CADXFrameManager::iInstanceCounter=0;
int CADXDebug::iInstanceCounter=0;
int CADXGraphicManager::iInstanceCounter=0;
//CADXFrameManager *CADXBase::m_lpFm=NULL;
//CADXGraphicManager *CADXBase::m_lpGm=NULL;


////////////////////// CADXGraphicManager ////////////////////////////////////////
/* Questa classe implementa le funzioni grafiche di base con DirectDraw*/

//costruttore
//inizializza le tabelle del seno e coseno e setta gli oggetti interni
//quando la classe � inizializzata m_satus � settato su 1
CADXGraphicManager::CADXGraphicManager(void)
{	
	iInstanceCounter++;

	lpDD=NULL;
	lpDDS=NULL;
	m_imgScreen=NULL;
	lpDDSCurrent=NULL;
    g_primary_buffer=NULL;
	g_secondary_buffer=NULL;
	lpDDpalette=NULL;
    g_hwnd=NULL;
    m_status=0;           //inizializza
    g_screen_height=0;
	g_screen_width=0;
	g_screen_bpp=0;
	m_iBackBuffers=0;
    m_current_visible_surface=-1;
	m_current_drawing_surface=-1;
    m_current_text_color.peRed=0xFF;        //Imposta il bianco come colore del testo predefinito
	m_current_text_color.peGreen=0xFF;
	m_current_text_color.peBlue=0xFF;
    m_current_text_bkgcolor.peRed=0x0;      //sfondo nero per il testo
	m_current_text_bkgcolor.peBlue=0x0;
	m_current_text_bkgcolor.peGreen=0x0;
    m_itextbkmode=TRANSPARENT;              //background del testo trasparente
    
	// alloca la memoria per lo struct e setta a zero tutti i bytes 
	//Questa struc � usato dalle funzioni Cls e DD_Fill_Surface
	DD_INIT_STRUCT(m_Clsddbltfx);
	

	//crea le due tabelle trigonometriche usate per il disegno di poligoni,cerchi ecc...
    for (int count=0;count<MAX_ANGLE;count++)  SinTable[count]=(float)sin(count*DEG_TO_RADIANS);
    for (count=0;count<MAX_ANGLE;count++)  CosTable[count]=(float)cos(count*DEG_TO_RADIANS);  
    
    
	m_lpitch=NULL;


    //setta a zero tutta la palette
    memset(m_pal,0,sizeof(PALETTEENTRY)*256L);

    //imposta la palette di default
	CreatePaletteEntry(&m_pal[0],0,0,0);
	CreatePaletteEntry(&m_pal[1],0,0,128);
	CreatePaletteEntry(&m_pal[2],0,128,0);
    CreatePaletteEntry(&m_pal[3],0,128,128);
    CreatePaletteEntry(&m_pal[4],128,0,0);
    CreatePaletteEntry(&m_pal[5],128,0,128);
	CreatePaletteEntry(&m_pal[6],128,128,0);
	CreatePaletteEntry(&m_pal[7],128,128,128);
    CreatePaletteEntry(&m_pal[8],64,64,64);
    CreatePaletteEntry(&m_pal[9],0,0,255);
    CreatePaletteEntry(&m_pal[10],0,255,0);
	CreatePaletteEntry(&m_pal[11],0,255,255);
	CreatePaletteEntry(&m_pal[12],255,0,0);
    CreatePaletteEntry(&m_pal[13],255,0,255);
    CreatePaletteEntry(&m_pal[14],255,255,0);
    CreatePaletteEntry(&m_pal[15],255,255,255);
	CreatePaletteEntry(&m_pal[16],155,96,70);	
	CreatePaletteEntry(&m_pal[17],8,8,8);
	CreatePaletteEntry(&m_pal[27],248,248,248);
	//crea le tonalit� del grigio
	CreateCoolPalette(m_pal,17,27,&m_pal[17],&m_pal[27]);
	//tonalit� blu
	CreatePaletteEntry(&m_pal[28],0,0,8);
	CreatePaletteEntry(&m_pal[36],0,0,255);
    CreateCoolPalette(m_pal,28,36,&m_pal[28],&m_pal[36]);
	//verde acqua
	CreatePaletteEntry(&m_pal[37],0,40,40);
	CreatePaletteEntry(&m_pal[41],0,255,208);
    CreateCoolPalette(m_pal,37,41,&m_pal[37],&m_pal[41]);
	//verde
	CreatePaletteEntry(&m_pal[42],0,20,0);
	CreatePaletteEntry(&m_pal[50],0,255,0);
	CreateCoolPalette(m_pal,42,50,&m_pal[42],&m_pal[50]);
	//celeste chiaro
    CreatePaletteEntry(&m_pal[51],12,253,115);
	CreatePaletteEntry(&m_pal[62],32,219,219);
	CreateCoolPalette(m_pal,51,62,&m_pal[51],&m_pal[62]);    
	
	CreatePaletteEntry(&m_pal[70],253,187,253);
	CreateCoolPalette(m_pal,62,70,&m_pal[62],&m_pal[70]);

    CreatePaletteEntry(&m_pal[75],214,12,214);
	CreateCoolPalette(m_pal,70,75,&m_pal[70],&m_pal[75]);
    //viola (al�)
    CreatePaletteEntry(&m_pal[82],82,3,82);
	CreateCoolPalette(m_pal,75,82,&m_pal[75],&m_pal[82]);

    CreatePaletteEntry(&m_pal[90],216,105,138);
	CreateCoolPalette(m_pal,82,90,&m_pal[82],&m_pal[90]);
    
    CreatePaletteEntry(&m_pal[96],251,47,78);
	CreateCoolPalette(m_pal,90,96,&m_pal[90],&m_pal[96]);
    //arancione
	CreatePaletteEntry(&m_pal[103],240,101,81);
	CreateCoolPalette(m_pal,96,103,&m_pal[96],&m_pal[103]);    
	//marrone
	CreatePaletteEntry(&m_pal[110],151,30,13);
	CreateCoolPalette(m_pal,103,110,&m_pal[103],&m_pal[110]);    
	//marrone
	CreatePaletteEntry(&m_pal[118],111,77,53);
	CreateCoolPalette(m_pal,110,118,&m_pal[110],&m_pal[118]);	
	CreatePaletteEntry(&m_pal[123],67,47,33);
	CreateCoolPalette(m_pal,118,123,&m_pal[118],&m_pal[123]);
    CreatePaletteEntry(&m_pal[128],228,254,150);
	CreateCoolPalette(m_pal,123,128,&m_pal[123],&m_pal[128]);    
	CreatePaletteEntry(&m_pal[135],183,228,24);
	CreateCoolPalette(m_pal,128,135,&m_pal[128],&m_pal[135]);
	CreatePaletteEntry(&m_pal[138],79,79,0);
	CreateCoolPalette(m_pal,135,138,&m_pal[135],&m_pal[138]);
	CreatePaletteEntry(&m_pal[142],255,255,10);
	CreateCoolPalette(m_pal,138,142,&m_pal[138],&m_pal[142]);
   	CreatePaletteEntry(&m_pal[152],51,85,60);
	CreateCoolPalette(m_pal,142,152,&m_pal[142],&m_pal[152]);
	CreatePaletteEntry(&m_pal[158],2,55,70);
	CreateCoolPalette(m_pal,152,158,&m_pal[152],&m_pal[158]);
	CreatePaletteEntry(&m_pal[168],143,227,252);
	CreateCoolPalette(m_pal,158,168,&m_pal[158],&m_pal[168]);
	CreatePaletteEntry(&m_pal[173],10,27,239);
	CreateCoolPalette(m_pal,168,173,&m_pal[168],&m_pal[173]);
   	CreatePaletteEntry(&m_pal[185],253,200,100);
	CreateCoolPalette(m_pal,173,185,&m_pal[173],&m_pal[185]);
	CreatePaletteEntry(&m_pal[190],245,139,22);
	CreateCoolPalette(m_pal,185,190,&m_pal[185],&m_pal[190]);
    CreatePaletteEntry(&m_pal[196],69,38,3);
	CreateCoolPalette(m_pal,190,196,&m_pal[190],&m_pal[196]);
	CreatePaletteEntry(&m_pal[200],81,70,51);
	CreateCoolPalette(m_pal,196,200,&m_pal[196],&m_pal[200]);
    CreatePaletteEntry(&m_pal[212],255,21,220);
	CreateCoolPalette(m_pal,200,212,&m_pal[200],&m_pal[212]);
	CreatePaletteEntry(&m_pal[216],32,32,0);
	CreateCoolPalette(m_pal,212,216,&m_pal[212],&m_pal[216]);
	CreatePaletteEntry(&m_pal[222],255,249,0);
	CreateCoolPalette(m_pal,216,222,&m_pal[216],&m_pal[222]);
	CreatePaletteEntry(&m_pal[229],1,63,53);
	CreateCoolPalette(m_pal,222,229,&m_pal[222],&m_pal[229]);
	CreatePaletteEntry(&m_pal[234],142,253,236);
	CreateCoolPalette(m_pal,229,234,&m_pal[229],&m_pal[234]);
    CreatePaletteEntry(&m_pal[241],241,145,131);
	CreateCoolPalette(m_pal,234,241,&m_pal[234],&m_pal[241]);
    CreatePaletteEntry(&m_pal[250],16,30,16);
	CreateCoolPalette(m_pal,241,250,&m_pal[241],&m_pal[250]); 

}   

//-----------------------------------------------------------------------

//distruttore: rilascia gli oggetti
CADXGraphicManager::~CADXGraphicManager(void)
{   
	iInstanceCounter++;
	ShutDown();
}

//------------------- ShutDown ----------------------------------------
//rilascia gli oggetti creati
void CADXGraphicManager::ShutDown(void)
{
	if (this->GetStatus()==1)
	{
    
		//Siccome lpDDS[0] � una superficie complessa, rilasciando questa si rilasciano 
		//anche tutti i back buffers
		if (lpDDS[0] != NULL) lpDDS[0]->Release();	

		//invoca la funzione Release degli oggetti DirectDraw
		if (lpDD != NULL) 
		{
			if (g_bFullScreen) lpDD->RestoreDisplayMode();  //ripristina la modalit� originale
			lpDD->Release();
		}

//		SAFE_DELETE_ARRAY(lpDDS);
	//	SAFE_DELETE_ARRAY(lpClipper);
		SAFE_DELETE_ARRAY(m_imgScreen);
	//	SAFE_DELETE_ARRAY(m_lpitch);

	}

	m_status=0;

}

//----------------------------- DD_Fill_Surface -----------------------
//pulisce una superficie usando un colore
BOOL CADXGraphicManager::DD_Fill_Surface(LPDIRECTDRAWSURFACE7 lpdds,DWORD color)
{
	
	// colore di riempimento
	m_Clsddbltfx.dwFillColor = color; 

	// usa blt per riempire la superficie
	lpdds->Blt(NULL,       // rettangolo di destinazione
			   NULL,       // punta alla superficie sorgente            
			   NULL,       // punta al rettangolo sorgente
		       DDBLT_COLORFILL | DDBLT_WAIT,   // riempie e aspetta                  			   &m_Clsddbltfx);  // punta alla struttura DDBLTFX 
			   &m_Clsddbltfx);

	return TRUE;
}


//---------------------------- SetGraphMode ----------------------------
//inizializza DirectDraw e imposta la modalit� grafica,
//se fallisce restituisce E_FAIL
//bFullScreen indica se l'applicazione � a schermo intero
//Se si inizializza in modalit� windowed (bFullScreen=FALSE) altezza,larghezza e profondit� colore vengono ignorate e vengono
//prese quelle correntemente impostate in Windows

HRESULT CADXGraphicManager::SetGraphMode(int width,int height,int bpp,HWND hwnd,BOOL bFullScreen,int iBackBuffers)
{
	HRESULT ddrval;
    DDSURFACEDESC2 ddsdInfo;
	DDSURFACEDESC2 ddsd;
    char r=0,g=0,b=0;
	int count;
		
    if (m_status !=0 ) return E_FAIL;  //modalit� gi� impostata     
	
	//crea l'oggetto DD direct X 7
	ddrval=DirectDrawCreateEx(NULL,(VOID **)&lpDD, IID_IDirectDraw7,NULL);

	if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode : impossibile impostare la modalit� grafica %d x %d %d bitxpixel",width,height,bpp); return ddrval;}   
 
     //inizializza la struttura che verr� usata per creare la superficie primaria
	memset(&ddsd,0,sizeof(ddsd));

	//riempie la struttura prima di creare la superficie
	ddsd.dwSize = sizeof(ddsd); 

	//numero di back buffers da creare
	iBackBuffers = iBackBuffers < 1 ? 1 : iBackBuffers;
    
    m_iBackBuffers=iBackBuffers;	

	lpDDS=new LPDIRECTDRAWSURFACE7[iBackBuffers+1];   

      
	if (!lpDDS) {WriteErrorFile("SetGraphMode,errore dimensionando il vettore delle superfici della flipping-chain");return E_FAIL;}

    lpClipper=new LPDIRECTDRAWCLIPPER[iBackBuffers+1];
	

	if (!lpClipper) {WriteErrorFile("SetGraphMode,errore dimensionando il vettore dei clipper");return E_FAIL;}

    m_lpitch=new int[iBackBuffers+1];

	if (!m_lpitch) {WriteErrorFile("SetGraphMode,errore dimensionado il vettore dei pitch");return E_FAIL;}	

	for (count=0;count<iBackBuffers+1;count++) m_lpitch[count]=0;	
 
	m_imgScreen=new IMAGE_FRAME[iBackBuffers+1];

	memset(m_imgScreen,0,sizeof(IMAGE_FRAME)*(iBackBuffers+1));

	if (!m_imgScreen) {WriteErrorFile("SetGraphMode,errore dimensionado il vettore delle frame del video");return E_FAIL;}

    if (bFullScreen)
	{//modalit� a tutto schermo

		//setta il controllo esclusivo a tutto schermo
		ddrval = lpDD->SetCooperativeLevel(hwnd, DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWREBOOT); 
		if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode,SetCooperativeLevel : impossibile impostare la modalit� a tutto schermo ");return ddrval;}


		ddrval=lpDD->SetDisplayMode(width,height,bpp,0,DDSDM_STANDARDVGAMODE );

		if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode,SetDisplayMode"); return ddrval;}

		
		//dwFlags specifica quali membri saranno riempiti con valori validi
		ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT; 
		//superficie primaria,flipping e complessa
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP | DDSCAPS_COMPLEX | DDSCAPS_VIDEOMEMORY | DDSCAPS_LOCALVIDMEM ; 
		//numero di back buffer
		ddsd.dwBackBufferCount = iBackBuffers;
 
		//crea la sup. primaria
		ddrval = lpDD->CreateSurface(&ddsd, &lpDDS[0], NULL); 
		if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode,CreateSurface : errore creando la superficie primaria");return E_FAIL;}       
	

        //acquisisce le superfici secondarie della flipping chain
		//NOTA: solo la superficie immediatamente attaccata al FRONT_BUFFER
        //ha la propriet� DDSCAPS_BACKBUFFER le altre vanno ricercate  
		//tramite la propriet� DDSCAPS_FLIP            
	

		ddscaps.dwCaps = DDSCAPS_BACKBUFFER; 		
		
		for (count=1;count<=iBackBuffers;count++)
		{	  		   
		    
	    	
			 ddrval = lpDDS[count-1]->GetAttachedSurface(&ddscaps, &lpDDS[count]); 			 
			 if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode:Errore impostando la superficie secondaria n�%d",count);
			                      WriteErrorFile(GetErrorDescription(ddrval));                        
								  return ddrval;}

			 ddscaps.dwCaps=DDSCAPS_FLIP | DDSCAPS_VIDEOMEMORY; //imposta le caratteristiche da ricercare nelle superfici successive della flipping chain		 	
	  	  
		}
		      
		 //a questo punto acquisisce le informazioni sulla modalit� video
	    //impostata
	    DD_INIT_STRUCT(ddsdInfo);
    
	    ddrval=lpDD->GetDisplayMode(&ddsdInfo);
    
        if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode:Errore durante l'acquisizione della modalit� grafica impostata.");return ddrval;}
     
	    g_screen_width=ddsdInfo.dwWidth;
	    g_screen_height=ddsdInfo.dwHeight;
	    g_screen_bpp=ddsdInfo.ddpfPixelFormat.dwRGBBitCount;

    }
	else
	{//mod. windowed
			//setta il controllo cooperativo con le GDI (windowed mode)
		ddrval = lpDD->SetCooperativeLevel(hwnd, DDSCL_NORMAL); 
		if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode,SetCooperativeLevel : impossibile impostare la modalit� windowed ");return ddrval;}

		//crea le superfici
		ddsd.dwFlags=DDSD_CAPS;
		//in modalit� windowed non crea una flipping chain
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE; 
       	//crea la sup. primaria
		ddrval = lpDD->CreateSurface(&ddsd, &lpDDS[0], NULL); 
		if (FAILED(ddrval)) { WriteErrorFile("SetGraphMode,CreateSurface : errore creando la superficie primaria in modalit� windowed");return ddrval;}		
		//crea le superfici secondarie
		ddsd.dwFlags = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
		//crea una superficie in memoria non visibile
		ddsd.ddsCaps.dwCaps=DDSCAPS_OFFSCREENPLAIN;
		
	    //a questo punto acquisisce le informazioni sulla modalit� video
	    //impostata
	    DD_INIT_STRUCT(ddsdInfo);
    
	    ddrval=lpDD->GetDisplayMode(&ddsdInfo);
    
        if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode:Errore durante l'acquisizione della modalit� grafica impostata.");return ddrval;}
     
	    g_screen_width=ddsdInfo.dwWidth;
	    g_screen_height=ddsdInfo.dwHeight;
	    g_screen_bpp=ddsdInfo.ddpfPixelFormat.dwRGBBitCount; 
	
		//crea le image frame che contengono le superfici della flipping chain
	
		
		//dimensioni della superficie		
        ddsd.dwWidth=g_screen_width;
		ddsd.dwHeight=g_screen_height;
		//crea i back buffers
		for (int count=1;count<iBackBuffers+1;count++)
		{
			 ddrval = lpDD->CreateSurface(&ddsd,&lpDDS[count],NULL); 			 
			 if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode:Errore impostando la superficie secondaria n�%d in windowed mode.",count);return ddrval;}
		}

	}

	g_hwnd=hwnd;  
	
	//definisce il rettangolo di clipping del video
	m_rcScreen.left=0;
	m_rcScreen.top=0;
	m_rcScreen.right=g_screen_width;
	m_rcScreen.bottom=g_screen_height;


	//definisce le image frame che incapsulano le superfici della flipping chain
	for (count=0;count<iBackBuffers+1;count++)
	{
		m_imgScreen[count].rcClip=m_rcScreen;
	    m_imgScreen[count].effect=0;
		m_imgScreen[count].status=1;
		m_imgScreen[count].transp_color=0;
		m_imgScreen[count].width=g_screen_width;
		m_imgScreen[count].height=g_screen_height;
		m_imgScreen[count].width_fill=0;
		m_imgScreen[count].surface=lpDDS[count];


    }

    //acquisisce le maschere di bit per i pixel in modalit� 16 bit
    if (g_screen_bpp == 16)
	{
		for(int n = 1; n<65536; n<<=1) //esamina tutti i 16 bit
		{    //conta i bit a partire dall'inizio usati per il rosso verde e blu
			 if(ddsdInfo.ddpfPixelFormat.dwRBitMask & n) ++r;

			 if(ddsdInfo.ddpfPixelFormat.dwGBitMask & n) ++g;

			 if(ddsdInfo.ddpfPixelFormat.dwBBitMask & n) ++b;
		}

		 //pos. del rosso
		 g_rgb16.dwRBitMask = ddsdInfo.ddpfPixelFormat.dwRBitMask;
         g_rgb16.Position.rgbRed = g + b;
		 g_rgb16.dwRBits=(DWORD) r; //numero di bit per il rosso
		 g_rgb16.dwShrR=8 - r; //calcola il numero di bit per lo shift left

		 if (!(r == 5 || r == 6)) {WriteErrorFile("SetGraphMode: errore impostando la modalit� a 16 bpp, il numero di bit per il rosso � %d",r);return E_FAIL;}

         //pos. del verde
		 g_rgb16.dwGBitMask = ddsdInfo.ddpfPixelFormat.dwGBitMask;
         g_rgb16.Position.rgbGreen = b;
		 g_rgb16.dwGBits=(DWORD) g; //num. di bit per il verde
		 g_rgb16.dwShrG=8 - g;

		 if (!(g == 5 || g == 6)) {WriteErrorFile("SetGraphMode: errore impostando la modalit� a 16 bpp, il numero di bit per il verde � %d",r);return E_FAIL;}

		 //pos. del blu
         g_rgb16.dwBBitMask = ddsdInfo.ddpfPixelFormat.dwBBitMask;
         g_rgb16.Position.rgbBlue = 0;
		 g_rgb16.dwBBits=(DWORD) b; // num di bit per il blu
		 g_rgb16.dwShrB=8 - b;

		 if (!(r == 5 || r == 6)) {WriteErrorFile("SetGraphMode: errore impostando la modalit� a 16 bpp, il numero di bit per il blu � %d",r);return E_FAIL;}

         
	}


    //imposta la variabile privata che indica se siamo in mod. full screen
    g_bFullScreen=bFullScreen; 

    if (bFullScreen)
	{//full screen
    

#ifndef USEFASTBLT
		
		//attenzione! attaccare il clipper anche a lpDDS2
		lpClipper[0] = AttachClipper(lpDDS[0],1,&m_rcScreen);
    
		for (int count=1;count<iBackBuffers+1;count++)
		{
			lpClipper[count]=AttachClipper(lpDDS[count],1,&m_rcScreen);
		}

#endif

    }

	else
	{//windowed
		//attacca i clipper alle superfici
		for (int count=0;count<iBackBuffers+1;count++)
		{
		
			//crea il clipper
			ddrval=lpDD->CreateClipper(0,&lpClipper[count],NULL);
			if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode : errore durante la creazione del clipper in windowed mode.");return E_FAIL;}
			//attacca il clipper alla finestra  	
			ddrval = lpClipper[count]->SetHWnd(0, hwnd);
			if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode : errore assegnando l'handle della finestra al clipper.");return E_FAIL;}

#ifndef USEFASTBLT
			
       		ddrval = lpDDS[count]->SetClipper(lpClipper[count]);
			if (FAILED(ddrval)) {WriteErrorFile("SetGraphMode : errore attaccando il clipper alla superficie primaria.");return E_FAIL;}					
       			   
#endif
		}

	}

    m_current_visible_surface=0;   //il buffer primario � impostato
	m_current_drawing_surface=0;
    lpDDSCurrent=lpDDS[m_current_drawing_surface];
    m_status=1;  //modalit� impostata
    
	//nella modalit� 8 bit acquisisce la palette
    if (g_screen_bpp==8) 
	{

		ddrval=lpDD->CreatePalette(DDPCAPS_8BIT,m_pal,&lpDDpalette,NULL);
		
		if (FAILED(ddrval))
		{
			WriteErrorFile("SetGraphMode:Errore durante la creazione della palette");			
            lpDDpalette=NULL;

		}	
		else 
		{   
            ddrval=lpDDS[0]->SetPalette(lpDDpalette); //attacca la palette alla sup. primaria
            
			if (!FAILED(ddrval))
			{   //se � ok procede acquisendo la palette
				ddrval=lpDDS[0]->GetPalette(&lpDDpalette);						    
				if (FAILED(ddrval)) {lpDDpalette=NULL;WriteErrorFile("SetGraphMode: Errore durante l'acquisizione della palette");lpDDpalette=NULL;}			
                SetAllPalette(m_pal);			
			}  
			else WriteErrorFile("SetGraphMode:Errore collegando la palette alla superficie primaria.");		
		}

	}	

	return S_OK;
}

//-------------------------LockSurface---------------------------------------- 
   
UCHAR *CADXGraphicManager::LockSurface(LPDIRECTDRAWSURFACE7 lpdds,int *lpitch)
{
// blocca la superficie e restituisc eun puntatore ad essa
DDSURFACEDESC2 ddsd;
// se la superficie � non � valida...
if (!lpdds)
   return(NULL);

// blocca la superficie
DD_INIT_STRUCT(ddsd);
lpdds->Lock(NULL,&ddsd,DDLOCK_WAIT | DDLOCK_SURFACEMEMORYPTR,NULL); 

// setta il pitch della sup.
if (lpitch)
   *lpitch = ddsd.lPitch;

// restituisce un puntatore al buffer della superficie
return((UCHAR *)ddsd.lpSurface);

} // end LockSurface

//---------------------------- UnlockSurface--------------------
// sblocca una superficie generica
  
BOOL CADXGraphicManager::UnlockSurface(LPDIRECTDRAWSURFACE7 lpdds, UCHAR *surface_buffer)
{

// controlla se la sup. � valida
if (!lpdds) return(FALSE);

lpdds->Unlock(NULL);

// return success
return(TRUE);
} 


//---------------------------- AttachClipper ---------------------------------------------------------
//Attacca un clipper per permettere alle DD di essere usate anche in modo windowed
//clip_list � una serie di rettangoli che identificano le aree su cui si puo' scrivere
//num_rects � il numero di retangoli nella clip list

LPDIRECTDRAWCLIPPER CADXGraphicManager::AttachClipper(LPDIRECTDRAWSURFACE7 lpdds, int num_rects, LPRECT clip_list) {

int index;                         
LPDIRECTDRAWCLIPPER lpddclipper;   // punta ad un nuovo clipper
LPRGNDATA region_data;             


// per prima cosa si crea il clipper
if (FAILED(lpDD->CreateClipper(0,&lpddclipper,NULL))) {WriteErrorFile("Errore durante la creazione del clipper.",0); return FALSE;}
  
// crea la lista dei clipper

// alloca la memoria per la region data
//region_data =  (LPRGNDATA) malloc(sizeof(RGNDATAHEADER)+num_rects*sizeof(RECT));

region_data =  new RGNDATA[(sizeof(RGNDATAHEADER)+num_rects*sizeof(RECT))];

// copia tutti i rettangoli che definoscono i clipper nel membro Buffer della struttura region data
memcpy(region_data->Buffer, clip_list, sizeof(RECT)*num_rects);

// setta i campi per l'intestazione della struttura
region_data->rdh.dwSize          = sizeof(RGNDATAHEADER);  //dimensione dell'header
region_data->rdh.iType           = RDH_RECTANGLES;         //tipo della region data 
region_data->rdh.nCount          = num_rects;              //num. di rettangoli 
region_data->rdh.nRgnSize        = num_rects*sizeof(RECT); //memoria occupata dai rettangoli

region_data->rdh.rcBound.left    =  64000;                //rcBound � un rettangolo di contenimento per la regione di rettangoli
region_data->rdh.rcBound.top     =  64000;
region_data->rdh.rcBound.right   = -64000;
region_data->rdh.rcBound.bottom  = -64000;

// cerca i confini di tutti i rettangoli
for (index=0; index<num_rects; index++)
    {
    // allarga il rettangolo di bounding in modo da contenerli tutti
    if (clip_list[index].left < region_data->rdh.rcBound.left)
       region_data->rdh.rcBound.left = clip_list[index].left;

    if (clip_list[index].right > region_data->rdh.rcBound.right)
       region_data->rdh.rcBound.right = clip_list[index].right;

    if (clip_list[index].top < region_data->rdh.rcBound.top)
       region_data->rdh.rcBound.top = clip_list[index].top;

    if (clip_list[index].bottom > region_data->rdh.rcBound.bottom)
       region_data->rdh.rcBound.bottom = clip_list[index].bottom;

    } // end for index

// setta la clipping list

if (FAILED(lpddclipper->SetClipList(region_data, 0)))    
   {
		
		WriteErrorFile("Errore nel settaggio della clipping list");
		// rilascia la memoria
		SAFE_DELETE_ARRAY(region_data);
		return (NULL);
   } // end if

// attacca il clipper alla superficie
if (FAILED(lpdds->SetClipper(lpddclipper))) 
   {
	   WriteErrorFile("Errore durante il collegamento del clipper alla superficie",0);
	   // rilascia la memoria
	   SAFE_DELETE_ARRAY(region_data);
	   return (NULL);
   } // end if

// l'operazione � andata a buon fine restituisce il puntatore al clipper
SAFE_DELETE_ARRAY (region_data);
return(lpddclipper);

} // fine AttachClipper

//--------------------------- SetTextColor -----------------------------
//imposta il colore del testo
void CADXGraphicManager::SetTextColor(BYTE red,BYTE green,BYTE blue)
{
	m_current_text_color.peRed=red;
	m_current_text_color.peBlue=blue;
	m_current_text_color.peGreen=green;
}
//---------------------------- SetTextBackColor -------------------------
//imposta il colore di background del testo
void CADXGraphicManager::SetTextBackColor(BYTE red,BYTE green,BYTE blue)
{
	m_current_text_bkgcolor.peRed=red;
	m_current_text_bkgcolor.peBlue=blue;
	m_current_text_bkgcolor.peGreen=green;
    m_itextbkmode=OPAQUE;

}
//----------------------------- SetTextBkgMode -------------------------
//se mode=0 il background del testo � trasparente altrimenti � opaco
void CADXGraphicManager::SetTextBkgMode(DWORD mode)
{
	m_itextbkmode = (mode>0) ? OPAQUE : TRANSPARENT;

}
//------------------------------ Cls ---------------------------------
//riempie con un colore lo schermo corrente
void CADXGraphicManager::Cls(COLORREF color)
{
	
	// colore di riempimento
	m_Clsddbltfx.dwFillColor = color; 
    	

	// usa blt per riempire la superficie
	lpDDSCurrent->Blt(NULL,       // rettangolo di destinazione
			   NULL,       // punta alla superficie sorgente            
			   NULL,       // punta al rettangolo sorgente
			   DDBLT_COLORFILL,   // riempie e aspetta                  			   &m_Clsddbltfx);  // punta alla struttura DDBLTFX 
			   &m_Clsddbltfx);

}

//-------------------------- Cls (ovr 1)----------------------------
//riempie una immagine con il colore specificato in color
void CADXGraphicManager::Cls(COLORREF color,IMAGE_FRAME_PTR img)
{	
	// colore di riempimento
	m_Clsddbltfx.dwFillColor = color; 

	// usa blt per riempire la superficie
	img->surface->Blt(NULL,       // rettangolo di destinazione
			   NULL,       // punta alla superficie sorgente            
			   NULL,       // punta al rettangolo sorgente
			   DDBLT_COLORFILL,   // riempie e aspetta                  			   &m_Clsddbltfx);  // punta alla struttura DDBLTFX 
			   &m_Clsddbltfx);
}

//---------------------------- UseBackBuffer -------------------------
//imposta come superficie di disegno corrente la prima superficie non visibile a disposizione
HRESULT CADXGraphicManager::UseBackBuffer(void)
{
	return UseScreen((m_current_visible_surface + 1) % (m_iBackBuffers+1));	
}

//----------------------------- ShowBackBuffer -------------------------------
//Mostra la superficie di disegno corrente
HRESULT CADXGraphicManager::ShowBackBuffer(void)
{
	return FlipScreen(m_current_drawing_surface);	
}
//---------------------------- UseScreen ------------------------
//imposta la superficie di disegno attiva
HRESULT CADXGraphicManager::UseScreen(int screen_num)
{
	HRESULT hr=S_OK;
	
	lpDDSCurrent=lpDDS[screen_num];        
    //controlla se la suprficie � stata persa, la ripristina in quel caso  
	if (lpDDSCurrent->IsLost()==DDERR_SURFACELOST)
	{
			//ripristina
			hr=lpDDSCurrent->Restore();
	}
        
	if (!FAILED(hr)) m_current_drawing_surface=screen_num;
	return hr;
	
}

//------------------------------- GetDrawingScreen -------------------------

UINT CADXGraphicManager::GetDrawingScreen(void)
{
	return m_current_drawing_surface;
}


//---------------------------------- GetVisibleScreen-------------------------

UINT CADXGraphicManager::GetVisibleScreen(void)
{
	return m_current_visible_surface;
}
//------------------------------- FlipScreenFast ----------------------------------
//Esegue il flipping come FlipScreen ma funziona solo in modalit� a tutto schermo
//inoltre non esegue nessun controllo aggiuntivo DDERR_SURFACELOST
HRESULT CADXGraphicManager::FlipScreenFast(int iScreen)
{
	HRESULT ddrval;

	ddrval=lpDDS[0]->Flip(lpDDS[iScreen],DDFLIP_DONOTWAIT ); 

	if (!FAILED(ddrval)) m_current_visible_surface=iScreen;

	return ddrval;
}

//------------------------------- GetFlipStatus ------------------------------
//restituisce lo stato del page flipping della superficie
HRESULT CADXGraphicManager::GetFlipStatus(int iScreen)
{
	return lpDDS[iScreen]->GetFlipStatus(DDGFS_ISFLIPDONE);
}
//------------------------------- FlipScreen -----------------------------
//Esegue il flipping rendendo visibile la superficie indicata da iScreen
//dopo aver usato FlipScreen, la superficie di disegno corrente non viene cambiata

HRESULT CADXGraphicManager::FlipScreen(int iScreen)
{
   HRESULT ddrval;
   RECT rcSrc,rcDest;  //rettangoli di blitting     		
   
   
   if (g_bFullScreen)
   {

	   while(1) 
	   {	   		   
			//flipping
			ddrval = lpDDS[0]->Flip(lpDDS[iScreen],DDFLIP_WAIT ); 
			
			if(ddrval == DD_OK) 
			{ 
				m_current_visible_surface=iScreen; //superficie visibile corrente            
				return S_OK;
				break; 
			} 
			else if(ddrval == DDERR_SURFACELOST) 
			{   //ripristina la superficie se la perde
				ddrval = lpDDS[0]->Restore(); 
				if(ddrval == DD_OK) 
				{ 
					m_current_visible_surface=iScreen; 					
	  				return S_OK;
					break; 
				} 
			} 
			else if(ddrval != DDERR_WASSTILLDRAWING) 
			{ 
				m_current_visible_surface=iScreen;				
				return S_OK;
				break; 
			} 


		}
   }
   else
	  //modalit� windowed   
   {	  
		  POINT p;
		  p.x=0;
		  p.y=0;

		  ::ClientToScreen(g_hwnd,&p);		  
		  //acquisisce il rettangolo dell'area client della finestra		  
		  ::GetClientRect(g_hwnd,&rcDest);		  
		  ::OffsetRect(&rcDest,p.x,p.y);
		  ::IntersectRect(&rcSrc,&m_rcScreen,&rcDest);

		  //esegue il blitting sulla superficie corrente
		  ddrval = lpDDS[0]->Blt(&rcDest, lpDDS[iScreen], &rcSrc, DDBLT_WAIT, NULL);		  
		  if (FAILED(ddrval))
		  {
			  WriteErrorFile(GetErrorDescription(ddrval));
			  WriteErrorFile("rect=%d %d %d %d",rcDest.left,rcDest.top,rcDest.right,rcDest.bottom) ;
		  }	

		  else m_current_visible_surface=iScreen; 
		  
		  return ddrval;
   }

}
int CADXGraphicManager::GetStatus(void)
{
	return m_status;
}
//------------------------- GetScreenWidth --------------------------
int CADXGraphicManager::GetScreenWidth(void)
{
	if (m_status==1) return g_screen_width;
	else return -1;

}
//------------------------ GetScreenHeight ----------------------------
int CADXGraphicManager::GetScreenHeight(void)
{
	if (m_status==1) return g_screen_height;
	else return -1;
}
//-------------------------- GetScrennBpp --------------------------
//restituisce la profondit� colore
//8 bit = 256 colori, 16 bit = 16K colori 32 bit = 16.7M colori
int CADXGraphicManager::GetScreenBitsPerPixel(void)
{
	if (m_status==1) return g_screen_bpp;
	else return -1;
}

//------------------------- BackBuffersCount ----------------------------
//Restituisce il numero di buffer grafici a disposizione compreso il front buffer
int CADXGraphicManager::GetScreenNum(void)
{
	return m_iBackBuffers+1;	
}
//---------------------- GetDDObject ---------------------

LPDIRECTDRAW7 CADXGraphicManager::GetDDObject(void)
{
	return lpDD;

}
//--------------------------- GetScreen -----------------------------
//restituisce il puntatore alla superficie specificata nella flip chain
IMAGE_FRAME_PTR CADXGraphicManager::GetScreenImage(int screen_num)
{
	if (screen_num>=0 && screen_num<m_iBackBuffers+1) return &m_imgScreen[screen_num];																																																																																																																																																																																										
    else return NULL;
}

//--------------- SetPixel (1)--------------------------------------------------
//[Solo in modalit� a 256 colori]
void CADXGraphicManager::SetPixel(int x,int y,COLORREF color)
{
	SetPixel(x,y,color,lpDDSCurrent);
}
//--------------- SetPixel (2)--------------------------------------------------
//usata in caso di modalit� full screen
//Disegna un pixel (in modalit� 8 bpp color � l'indice della palette
void CADXGraphicManager::SetPixel(int x,int y,COLORREF color,LPDIRECTDRAWSURFACE7 pDDS) 
{
	HRESULT hr;
	DDBLTFX ddbfx;
	RECT    rcDest;
		
	// Inizializza la struttura ddbfx con il colore del pixel
	ddbfx.dwSize = sizeof( ddbfx );
	ddbfx.dwFillColor = color;
	
	// prepara il rettangolo di destinazione delle dimensioni 1x1
	rcDest.left=x;
	rcDest.top=y;
	rcDest.right=x+1;
	rcDest.bottom=y+1;
	
	// Blitting del rettangolo
	hr = pDDS->Blt( &rcDest, NULL, NULL, DDBLT_WAIT | DDBLT_COLORFILL, &ddbfx );

}

//---------------------------------- SetPixel (4) ------------------------
//[Solo in modalit� a 256 colori]
void CADXGraphicManager::SetPixel(int x,int y,int color,UCHAR *buffer,int lpitch) {

buffer[x+y*lpitch]=color;

}
//------------------------------- GetPixel -------------------------------------
//acquisisce un pixel dalle coordinate x,y
//prima di chiamare questa funz. occorre bloccare la superficie
//per ora funziona solo per grafica 8 bit
//[Solo in modalit� a 256 colori]
DWORD CADXGraphicManager::GetPixel(int x, int y,UCHAR *buffer,int lpitch)
{

	return (DWORD) buffer[x+y*lpitch];
}

//----------------------------------- GetPixel ------------------------------
//Funzione GetPixel generale (tutte le modalit�)
//restituisce il colore del pixel alle coordinate x,y della superficie corrente
COLORREF CADXGraphicManager::GetPixel(int x,int y)
{
	int iPitch=0;
	BYTE *pBuffer=NULL;           
	COLORREF cl;
	HDC hdc;
	
	hdc=GetDC(g_hwnd);
	//nota: questa api rende un valore rgb con i bit del rosso nella posizione meno significativa
	//mentre la libreria arcade 32 individua le terne RGB come R-G-B con il rosso nella posizione piu' significativa
	cl=::GetPixel(hdc,x,y);
    //scambia il byte del rosso e quello del blu perch� questa libreria usa i valori BGR
    cl=(cl>>16) & 0xFF | (cl & 0xFF00) | (cl & 0xFF) << 16;


	return cl;
	
}


//--------------------------------- GetPixelFormat -----------------------------
//Restituisce la struttura che contiene il formato pixel corrente
RGB16 CADXGraphicManager::GetPixelFormat(void)
{
	return g_rgb16;
}
//---------------------- GetPixel16 ------------------------------------
//Restituisce un pixel in modalit� 16 bit da un buffer immagine
COLORREF CADXGraphicManager::GetPixel16(DWORD x,DWORD y,BYTE *buff,DWORD dwPitch)
{
	return (COLORREF) buff[x*2+y*dwPitch];
}

//--------------------- GetPixel24 ---------------------------------------
//Restituisce un pixel in modalit� 24 bit
COLORREF CADXGraphicManager::GetPixel24(DWORD x,DWORD y,BYTE *buff,DWORD dwPitch)
{
	COLORREF cl=0;
	memcpy(&cl,buff+x*3+y*dwPitch,3L);	
	return cl;

}

//------------------------------ DDTextOut ---------------------------------------------------

//scrive del testo sulla superficie DirectDraw
//xout,yout coordinate di output, lpDDs una sup. DD valida szText stringa da stampare
//color un elemento di palette. se � nullo prende un colore di default
int CADXGraphicManager::DDTextOut(int xout,int yout,LPDIRECTDRAWSURFACE7 lpDDS,char *szText,PALETTEENTRY *color) 
{   

    HDC hdc; //device context della superficie
    HRESULT hr;
    PALETTEENTRY peText;
    
    if (!lpDDS) return (0);

	xout=(xout<0) ? 0:xout;
	yout=(yout<0) ? 0:yout;      

	//acquisisce il device context dalla superficie
    
	if (g_bFullScreen)
	{
       //Modalit� a tutto schermo
	   hr=lpDDS->GetDC(&hdc);
       if (FAILED(hr)) return 0;
	}

	else
	{
        //In modalit� windowed
		//acquisisce l'handle della finestra
		hdc=GetDC(g_hwnd);
		
		if (hdc==NULL) return (0);
    }


	SetBkMode(hdc,m_itextbkmode);

    if (color) memcpy(&peText,color,sizeof(PALETTEENTRY));

    else 
	{
		peText.peRed=90;
		peText.peGreen=240;
		peText.peBlue=220;

	}

    //l'op. :: in questo caso significa che le funzioni chiamate sono quelle esterne alla classe
    ::SetTextColor(hdc,RGB(peText.peRed,peText.peGreen,peText.peBlue));
    ::SetBkColor(hdc,RGB(m_current_text_bkgcolor.peRed,m_current_text_bkgcolor.peGreen,m_current_text_bkgcolor.peBlue));
	::TextOut(hdc,xout,yout,szText,lstrlen(szText));
	lpDDS->ReleaseDC(hdc);	

	return (1);
    
}

//------------------------ TextOut (2) --------------------------------------
//scrive del testo formattato sulla superficie direct draw corrente
//il funzionamento � identico alla funzione C printf

void CDECL CADXGraphicManager::TextOut(int xout,int yout,char *szFormat, ...)
{
	char szBuffer[1024];
    va_list pArgList;


    //punta gli argomenti opzionali
	va_start (pArgList,szFormat);
	//esegue un output formattato sul buffer szBuffer
	_vsnprintf(szBuffer,sizeof(szBuffer) / sizeof(char),szFormat,pArgList);
    
	va_end(pArgList);  
   
	//chiama la funzione protetta DDTextOut
	DDTextOut(xout,yout,lpDDSCurrent,szBuffer,&m_current_text_color);

}

//--------------------------------- FillArea -----------------------------
//algoritmo di riempimento di un qualsiasi poligono
//la superficie deve essere sbloccata
//dwColor = colore di riempimento
//dwBound = colore del bordo
//lpDDSCurrent = superficie
//mode = se � 0 significa che deve riempire fino a che non trova il colore di riempimento
//altrimenti riempie solo l'area di colore dwBound 
//[Solo in modalit� a 256 colori]
int CADXGraphicManager::FillArea(int x,int y,DWORD dwColor,DWORD dwBound,int mode)
{
	
    int ixstart,ixfinish;			      
    int curx;   
    int xPixl,yPixl;
    int count;
    BOOL bChangeU,bChangeD;
    BOOL bFill,bCycle;
    DWORD pixel;
    int maxbuff;
	int xb[MAX_FILL_BUFFER];
	int yb[MAX_FILL_BUFFER];
    UCHAR *buffer;
	int lpitch;
    
	if (lpDDSCurrent == NULL) return 0;
        

	if (!(buffer=LockSurface(lpDDSCurrent,&lpitch))) return 0;           
   
	bFill=TRUE;curx=x;
    bCycle=TRUE;
    	   
	maxbuff=0;

	xb[maxbuff]=x;
	yb[maxbuff]=y;        

    while (maxbuff>=0)
	{
		curx=xb[maxbuff];
        yPixl=yb[maxbuff];
        xPixl=curx;bFill=TRUE;                     
                    
        maxbuff--;
        //riempie il poligono per linee orizzontali
		while (bFill)
		{   //segmento di sinistra
			pixel=GetPixel(curx,yPixl,buffer,lpitch);            
            //controlla se il pixel va riempito
			if (mode == 0) bFill=(pixel != dwBound);
			else bFill=(pixel == dwBound);                   
		
			if (bFill) SetPixel(curx,yPixl,dwColor,buffer,lpitch);			
            else break;

			curx--; 
			if (curx<0) bFill=FALSE;

		}   
   
		curx++;
		ixstart=curx;      
       
       
		bFill=TRUE;
		curx=xPixl;        
      
		while (bFill)
		{   //segmento di destra
			pixel=GetPixel(curx,yPixl,buffer,lpitch);
        
			if (mode == 0) bFill=(pixel != dwBound);
			else bFill=(pixel == dwBound);       
        
			if (bFill) SetPixel(curx,yPixl,dwColor,buffer,lpitch);
			else break;

			curx++;
			if (curx>g_screen_width-1) bFill=FALSE;	
		}
		curx--;
		ixfinish=curx;

		//-----	
        
		bChangeU=TRUE;bChangeD=TRUE;       
	    //controlla i punti adiacenti in alto alla linea riempita
		for (count=ixstart;count<=ixfinish;count++)
		{
           
			if (yPixl>0)
			{
				pixel=GetPixel(count,yPixl-1,buffer,lpitch);

				if (mode == 0) bFill=(pixel != dwBound && pixel != dwColor);
			    else bFill=(pixel == dwBound);       
                				
				if (bFill && bChangeU)
				{
				
					bChangeU=FALSE;
					if (maxbuff<MAX_FILL_BUFFER-1)
					{
						maxbuff++;
						xb[maxbuff]=count;
						yb[maxbuff]=yPixl-1;

					}
				}

				else if (!bFill ) bChangeU=TRUE;			

			}
            //controlla i punti adiacenti alla linea riempita adiacenti in basso
			if (yPixl<g_screen_height-1)
			{
				pixel=GetPixel(count,yPixl+1,buffer,lpitch);

				if (mode == 0) bFill=(pixel != dwBound && pixel != dwColor);
			    else bFill=(pixel == dwBound);       

				if (bFill && bChangeD)
				{
					bChangeD=FALSE;
					if (maxbuff<MAX_FILL_BUFFER-1)
					{
						maxbuff++;
						xb[maxbuff]=count;
						yb[maxbuff]=yPixl+1;
					}
				}

				else if (!bFill) bChangeD=TRUE;
			}


		}//fine for	
		
	}

	UnlockSurface(lpDDSCurrent,buffer);

	return 1;
}

//---------------------------------- DrawLine ------------------------------------
//disegna una linea in modo veloce usando l'algoritmo di Jack Bresenham (1965)
//nella video-ram o nel doppio buffer.
void CADXGraphicManager::DrawLine(int x1,int y1,int x2,int y2,COLORREF color){

int dx,dy;
int d,inca,incb;
int incax,incbx,incay,incby;
int x,y;
int temp;
HRESULT hr;
DDBLTFX ddbfx;
RECT    rcDest;

// Inizializza la struttura ddbfx con il colore del pixel
ddbfx.dwSize = sizeof( ddbfx );
ddbfx.dwFillColor = color;


dy=y2-y1;
dx=x2-x1;

if (dx<0) {

	 dx=-dx;
	 incbx=-1;

	  }

else incbx=1;

if (dy<0) {

	 dy=-dy;
	 incby=-1;
}

else incby=1;

if (dx<dy) { temp=dx;  //scambia dx e dy
	     dx=dy;
	     dy=temp;
	     incax=0;
	     incay=incby;
	   }

else { incax=incbx;
       incay=0; }

d=dy*2-dx;
inca=dy*2;
incb=2*(dy-dx);
x=x1;
y=y1;


  if ((y>=0) && (y<=g_screen_height-1)) {
     if ((x>=0) && (x<=g_screen_width-1))
     // prepara il rettangolo di destinazione delle dimensioni 1x1
     rcDest.left=x;
     rcDest.top=y;
     rcDest.right=x+1;
     rcDest.bottom=y+1;
     hr = lpDDSCurrent->Blt( &rcDest, NULL, NULL, DDBLT_WAIT | DDBLT_COLORFILL, &ddbfx );}//fine if
     
for (temp=0;temp<dx;temp++) {

     if (d<0) {x=x+incax;
	       y=y+incay;
	       d=d+inca;}

     else     {x=x+incbx;
	       y=y+incby;
	       d=d+incb;}

    if ((y>=0) && (y<=g_screen_height-1)) {
     if ((x>=0) && (x<=g_screen_width-1))
      	 // prepara il rettangolo di destinazione delle dimensioni 1x1
         rcDest.left=x;
         rcDest.top=y;
         rcDest.right=x+1;
         rcDest.bottom=y+1;
		 hr = lpDDSCurrent->Blt(&rcDest, NULL, NULL, DDBLT_WAIT | DDBLT_COLORFILL, &ddbfx );}//fine if

  }//fine for
}

//----------------------- DrawRectangle ----------------------------------
//disegna un rettangolo riempito
//in modalit� 8 bit colorref � l'indice della palette
void CADXGraphicManager::DrawRectangle(LONG x,LONG y,LONG x1,LONG y1,COLORREF fill_color)
{

	static DDBLTFX ddbltfx; 
	static RECT    rcDest;

	// alloca la memoria per lo struct e setta a zero tutti i bytes 
	DD_INIT_STRUCT(ddbltfx);

	// colore di riempimento
	ddbltfx.dwFillColor = fill_color; 
    
	rcDest.left=x;
	rcDest.top=y;
	rcDest.bottom=y1;
	rcDest.right=x1;

	lpDDSCurrent->Blt(&rcDest,
		              NULL,
					  NULL,
   				      DDBLT_COLORFILL | DDBLT_WAIT,   // riempie e aspetta                  
			          &ddbltfx);  // punta alla struttura DDBLTFX                  
}

//----------------------- DrawCircle --------------------------------------------------
//N.B. Prima di chiamare questa funzione devono essere state inizializzate
//le tabelle del seno e coseno.
void CADXGraphicManager::DrawCircle(int cx,int cy,int radius,COLORREF color){

int count; 
int rx,ry;
int cposx,cposy; //posizione corrente
int newx,newy;
int stx,sty;

cposx=cx+radius;
cposy=cy;
stx=cposx;
sty=cposy;

#pragma warning( disable : 4244 ) 

for(count=0;count<=359;count++) { rx=radius*CosTable[count];
				  ry=radius*SinTable[count];
				  newx=cx+rx;
				  newy=cy-ry;
				  DrawLine(cposx,cposy,newx,newy,color);
				  cposx=newx;//aggiorna la posizione corrente
				  cposy=newy; }

#pragma warning (default : 4244 )


DrawLine(newx,newy,stx,sty,color);//chiude il cerchio.

}

//------------------------- DrawPolygon -------------------------------
//disegna un poligono con un numero di lati qualsiasi
void CADXGraphicManager::DrawPolygon(LONG xc,LONG yc,DWORD radius,DWORD sides,COLORREF color,DWORD start_angle)
{
	LONG x,y,x1,y1,x0,y0;    
	int angle;
    int sangle=0;
	int cangle;
    DWORD count;
    
    #pragma warning( disable : 4244 ) 

	if ((lpDD!=NULL) && (sides>=3))
	{		
		angle=360/sides;		                    //calcola l'angolo               

		if (start_angle) sangle=start_angle % 360; //angolo di partenza
        
       	x1=xc+(LONG)radius*CosTable[sangle];
		y1=yc+(LONG)radius*SinTable[sangle]; 
        
        x0=x1,y0=y1;

		for (count=0;count<sides;count++)
		{
			cangle=count*angle+sangle;  //angolo corrente
			cangle %= 360;

			x=xc+(LONG)radius*CosTable[cangle];
			y=yc+(LONG)radius*SinTable[cangle];
            
			DrawLine(x1,y1,x,y,color);

			x1=x;y1=y;

		}

		DrawLine(x,y,x0,y0,color);  //chiude il poligono

	}

	#pragma warning( default : 4244 ) 

}

//-------------------------- CreateRGB ----------------------------
//crea un valore di colore a partire dalle componenti R G B

COLORREF CADXGraphicManager::CreateRGB( int r, int g, int b )
{
   COLORREF pixel;   
   
   switch (g_screen_bpp)
   {
   case 8:
      //restituisce l'indice della palette i cui valori r,g,b
      //sono piu' simili a quelli richiesti
      return GetNearestPaletteIndex(r,g,b,m_pal);

      break;
   case 16:
	  //usa la struttura g_rbg16 creata in SetGraphMode
	  //per determinare lo shift dei bit
      pixel = ((r >> g_rgb16.dwShrR) << g_rgb16.Position.rgbRed) |

              ((g >> g_rgb16.dwShrG) << g_rgb16.Position.rgbGreen) |

              (b >> g_rgb16.dwShrB);

      break;
   case 24:
   case 32:
      pixel = (r<<16) | (g<<8) | (b);
      break;
   default:
      pixel =0;
   }
   return pixel;
}

//---------------------------- SplitRGB ------------------------------
//In base alla profondit� colore impostata,crea una PALETTEENTRY da un valore di colore
//� la funzione inversa a CreateRGB
void CADXGraphicManager::SplitRGB(PALETTEENTRY *plDest,COLORREF dwSrc,DWORD bpp)
{
  
	 
   switch (bpp)
   {
   case 8:
      //dwSrc in questo caso � l'indice della palette
      plDest->peRed=m_pal[dwSrc%256].peRed;
      plDest->peGreen=m_pal[dwSrc%256].peGreen;
	  plDest->peBlue=m_pal[dwSrc&256].peBlue;
      break;

   case 16:

   	  plDest->peRed= (BYTE) ((dwSrc & g_rgb16.dwRBitMask) >> g_rgb16.Position.rgbRed);
	  plDest->peGreen=(BYTE) ((dwSrc & g_rgb16.dwGBitMask) >> g_rgb16.Position.rgbGreen);
	  plDest->peBlue=(BYTE) ((dwSrc & g_rgb16.dwBBitMask) >> g_rgb16.Position.rgbBlue);
      break;

   default:
      
      //true color
      plDest->peBlue=(BYTE)(dwSrc & 0xFF);               //primo byte
	  plDest->peGreen=(BYTE)((dwSrc & 0xFF00)>>8);       //secondo byte
	  plDest->peRed=(BYTE)((dwSrc & 0xFF0000)>>16);      //terzo byte
      break;   
   }
   
}

//-------------------------- GetNearestPaletteIndex -------------------
//Restituisce l'indice della palette passata
//del colore che piu' si avvicina alla terna RGB passata
//[Solo in modalit� a 256 colori]
DWORD CADXGraphicManager::GetNearestPaletteIndex(int r,int g,int b,PALETTEENTRY *cpal)
{
	DWORD dwCur=0;
	DWORD dwCount;
    int idcur;  //scostamenti
	int idtot;
    PALETTEENTRY *pl;
    
    idcur=256*3;

	for (dwCount=0;dwCount<256;dwCount++)
	{
		pl=&cpal[dwCount];
		//calcola lo scostamento
        idtot=abs(pl->peRed-r)+abs(pl->peBlue-b)+abs(pl->peGreen-g);
        
		if (idtot<idcur) {idcur=idtot;dwCur=dwCount;}        	

	}
	
	return dwCur;
}
//------------------------ CreatePaletteEntry -------------------------
//imposta i valori rgb in una paletteentry
void CADXGraphicManager::CreatePaletteEntry(PALETTEENTRY *pe,BYTE red,BYTE green,BYTE blue)
{
	pe->peRed=red;
    pe->peGreen=green;
    pe->peBlue=blue;
    pe->peFlags = PC_NOCOLLAPSE;  //inserisce il registro in una locazione non usata se possibile

}

//---------------------- CreateCoolPalette -----------------------------------
//modifica una serie di paletteentry partendo da un colore rgb
//arrivando ad un altro rgb con passaggi graduali
//la palette va poi applicata alla palette DD corrente con SetAllPalette
//*pe= punta ad un vettore di 256 paletteentry pStart=colore di partenza pEnd=colore di arrivo
void CADXGraphicManager::CreateCoolPalette(PALETTEENTRY *pe,BYTE iStart,BYTE iEnd,PALETTEENTRY *pStart,PALETTEENTRY *pEnd)
{ 
	float fsRed;
	float fsGreen;
	float fsBlue;
	float fStep;
    int iSteps;
	int count;
	size_t stPal;
	PALETTEENTRY peCur;

	if (pe!=NULL && pStart !=NULL && pEnd !=NULL && (iEnd>iStart))
	{

		 stPal=sizeof(PALETTEENTRY);

         iSteps=iEnd-iStart;         
         
		 fsRed  =(float)(pEnd->peRed-pStart->peRed)/(float)iSteps;
         fsGreen=(float)(pEnd->peGreen-pStart->peGreen)/(float)iSteps;
         fsBlue =(float)(pEnd->peBlue-pStart->peBlue)/(float)iSteps;                   
		 
         memcpy(&peCur,pStart,stPal);

		 for (count=iStart;count<=iEnd;count++)
		 {
			 memcpy(pe+count,&peCur,stPal);
			 
             fStep=(float)(count-iStart);

			 #pragma warning( disable : 4244 )
			  //modifica il colore avvicinandolo a quello di destinazione
		      peCur.peRed=(float)pStart->peRed+fsRed*fStep;
              peCur.peGreen=(float)pStart->peGreen+fsGreen*fStep;
              peCur.peBlue=(float)pStart->peBlue+fsBlue*fStep;                                      		                 
             #pragma warning( default : 4244 )
		 }
	}
}

//----------------------- SetPaletteEntry ----------------------------------
//imposta i valori r g b per un registro di palette
void CADXGraphicManager::SetPaletteEntry(BYTE num_entry,BYTE r,BYTE g,BYTE b) {

PALETTEENTRY pe;
HRESULT hr;

if (lpDDpalette) 
{
	pe.peRed=r;
    pe.peGreen=g;
    pe.peBlue=b;
    pe.peFlags = PC_NOCOLLAPSE;  //inserisce il registro in una locazione non usata se possibile

	hr=lpDDpalette->SetEntries(0,num_entry,1,&pe);
	//aggiorna il vettore della palette
	memcpy((BYTE *)&m_pal[num_entry],(BYTE *)&pe,sizeof(PALETTEENTRY));

}

}

//---------------DD_Set_Palette--------------------------------
//applica la palette alla sup. primaria 
//[Solo in modalit� a 256 colori]
void CADXGraphicManager::SetAllPalette(PALETTEENTRY *pal) {

HRESULT hr;

if (lpDDpalette!=NULL && pal!=NULL) 
{       
   
    hr=lpDDpalette->SetEntries(0,0,256,pal);
    
	if (FAILED(hr)) WriteErrorFile("SetAllPalette:Errore durante l'impostazione della palette : %X",lpDDpalette); 
	//aggiorna il vettore palette entries corrente
    else lpDDpalette->GetEntries(0,0,256,m_pal);     
}

}

//------------------ GetPaletteEntries-----------------------------------------
//[Solo in modalit� a 256 colori]
//recupera la palette dall'oggetto principale DirectDraw e la salva in cpal
void CADXGraphicManager::GetPaletteEntries(PALETTEENTRY *cpal) {

if (lpDDpalette != NULL ) {
    
   lpDDpalette->GetEntries(0,0,256,cpal);     

}

}

//--------------------- DD_Fade_Palette -------------------------------------
//fa tendere i color della palette ad un colore predefinito
//quando termina (ha raggiunto il colore destinazione restituisce FALSE)
//iExclude=indirizzo del vettore di indici da escludere nel processo
//iSize dimensione di iExclude;
BOOL CADXGraphicManager::FadePalette(PALETTEENTRY dest_color,LPDIRECTDRAWPALETTE lpDDpal,int *iExclude,int iSize,int iSteps,BOOL initcycle)
{
	
    static PALETTEENTRY palStart[256];      //palette di partenza
	static float fSRed[256];
	static float fSGreen[256];
	static float fSBlue[256];
	static int iCStep=0;
	static int iSize1;
	static int iTotalSteps;	
	PALETTEENTRY palC[256];                  //palette corrente
    int count,icount;
    BOOL bFade;     
    
    if (lpDDpal) 
	{
	   //inizializza il ciclo
	   if (initcycle==TRUE) 
	   {
		   if (!iSteps) 
		   {
#ifdef _DEBUG_
			   WriteErrorFile(g_szErrorFileName,"DD_Fade_Palette : non � stato specificato il numero di passi.");
			   return (FALSE);	   
#endif
		   }
		   //� stato passato un vettore di indici da escludere ma non � stata specificata la dimensione del vettore
		   else if (iExclude && iSize<=0) 
           {
#ifdef _DEBUG_
			   WriteErrorFile(g_szErrorFileName,"DD_Fade_Palette : la dimensione del vettore degli indici da escludere non � valida.");
			   return (FALSE);
#endif
		   } else
		   {
			   //inizializza   
			   //acquisisce la palette corrente
			   lpDDpal->GetEntries(0,0,256,palStart);			 
               iSize1=iSize;
			   iTotalSteps=iSteps;
		       //calcola gli incrementi
			  
		       for (count=0;count<256;count++) 
		       {
			       fSRed[count]=(float)(dest_color.peRed-palStart[count].peRed)/(float)iSteps;
                   fSGreen[count]=(float)(dest_color.peGreen-palStart[count].peGreen)/(float)iSteps;
                   fSBlue[count]=(float)(dest_color.peBlue-palStart[count].peBlue)/(float)iSteps;                   
		       }
               //inizialmente la la palette corrente � uguale a quella iniziale
			   memcpy(palC,palStart,256*sizeof(PALETTEENTRY));
			  
		   }
	   }
       
	   //incrementa il passo corrente
       iCStep++;            
	  
       //esegue un ciclo di fading
       for (count=0;count<256;count++) 
	   {   
		   bFade=TRUE;
		   
		   for (icount=0;icount<iSize1;icount++)
		   {   
			   //se l'indice appartiene a quelli da non processare lo salta
		   	   bFade &= (!(iExclude[icount]==count));

		   }
		   
		   if (bFade)
		   {

//disabilita il warning che indica la possibile perdita di dati
//nella conversione float -> intero
#pragma warning( disable : 4244 )
			   //modifica il colore avvicinandolo a quello di destinazione
		       palC[count].peRed=(float)palStart[count].peRed+fSRed[count]*iCStep;
               palC[count].peGreen=(float)palStart[count].peGreen+fSGreen[count]*iCStep;
               palC[count].peBlue=(float)palStart[count].peBlue+fSBlue[count]*iCStep;                                      		   
              
#pragma warning( default : 4244 )
		   
		   }
		          
	   }
	   
       //applica la palette trasformata
	   lpDDpal->SetEntries(0,0,256,palC);       
            
       //quando ha finito restituisce FALSE
       return (iCStep<iTotalSteps);
        
	}
	else
	//se lpDDpal � nullo reinizializza
	{
	
		return (FALSE);
	}

	return (FALSE);
}

/////////////////////////////////////// FadeColors /////////////////////////////
//Fa tendere tutti i pixel della superficie corrente al colore destinazione
//iSteps � il numero di passi che impiega.
//Quando termina il ciclo (cio� ha raggiunto il colore destinazione) rende FALSE
//bInitCycle va impostato su TRUE quando si inizializza il ciclo

BOOL CADXGraphicManager::FadeColors(const COLORREF clDestColor,const int iSteps,IMAGE_FRAME_PTR pFrm,BOOL bInitCycle)
{
	static BOOL bInitialized=FALSE;
	static BYTE *pBytes=NULL;
	static BYTE *pOrigBytes=NULL;
    static int iPitch=0;
    static DWORD dwBitsPerPixel=0;
    static int iStep=0;
    static LONG *pfSRed=NULL;
	static LONG *pfSBlue=NULL;
	static LONG *pfSGreen=NULL;
	static WORD *wColors=NULL;
	static LONG *lRed=NULL;
	static LONG *lGreen=NULL;
	static LONG *lBlue=NULL;
	static LONG fEBlue=0;//valori finali del rosso verde e blu
	static LONG fERed=0;
	static LONG fEGreen=0;
	static int iTotalSteps=0;
	static DWORD dwBuffSize=0;
	DWORD dwCount;
	DWORD dwHeight;  
	WORD wColor16;
	static LPDIRECTDRAWSURFACE7 lpDDS=NULL;
	BYTE *pbyCol;
	BYTE byRed,byBlue,byGreen; 
	int iCount;
	DWORD dwGMask=0,dwRMask=0,dwBMask=0;
	DWORD dwRShift,dwGShift,dwBShift; //spostamenti per estrarre i bit R,G,B,
	const LONG lMulti=1000L;
	PALETTEENTRY pl;

	//se il ciclo non � inizializzato, e bInitCycle � false, allora forza l'inizializzazione
	if (bInitCycle==FALSE && bInitialized==FALSE) return FALSE;
	
	//blocca la superficie
	if (lpDDS) pBytes=LockSurface(lpDDS,&iPitch);

	//prima di inizializzare, controlla lo stato dell'oggetto G.M. e della superficie corrente
	if (bInitCycle && GetStatus()==1 &&lpDDSCurrent)
	{
		//sblocca la superficie precedentemente bloccata
		if (pBytes && iPitch) UnlockSurface(lpDDSCurrent,pBytes);		
		
		if (pFrm) {lpDDS=pFrm->surface;dwHeight=pFrm->height;}
		else {lpDDS=lpDDSCurrent;dwHeight=GetScreenHeight();}
				
	    //blocca la superficie
		pBytes=LockSurface(lpDDS,&iPitch);
		
		if (pBytes)
		{			
			dwBitsPerPixel=GetScreenBitsPerPixel();		
			iStep=0; //numero di cicli da eseguire
			pfSBlue = new LONG[256];  //dimensiona i vettori con gli incrementi
			pfSGreen = new LONG[256];
			pfSRed = new LONG[256];
			dwBuffSize=iPitch*dwHeight;
			pOrigBytes = new BYTE[dwBuffSize];
			//fa una copia della superficie prima di modificarla
			memcpy (pOrigBytes,pBytes,(size_t)dwBuffSize);
            iTotalSteps=iSteps;
			
			if (pOrigBytes != NULL && pfSBlue != NULL && pfSRed != NULL && pfSGreen != NULL)
			{
			
				memset (pfSBlue,0,sizeof(LONG)*256);
				memset (pfSGreen,0,sizeof(LONG)*256);
				memset (pfSRed,0,sizeof(LONG)*256);
				//estrae le componenti RGB del colore finale
				SplitRGB(&pl,clDestColor,dwBitsPerPixel);
                
                if (dwBitsPerPixel != 16)
				{

					fEBlue=(LONG) pl.peBlue * lMulti;
					fEGreen=(LONG) pl.peGreen *lMulti; 
					fERed=(LONG) pl.peRed * lMulti;
				
				}
				else
				{   //16 bit
					
					//maschere di estrazione dei bit
					dwGMask=g_rgb16.dwGBitMask;
					dwRMask=g_rgb16.dwRBitMask;
					dwBMask=g_rgb16.dwBBitMask;
					dwRShift=g_rgb16.Position.rgbRed;   //spostamento a dx dei bit del rosso
					dwGShift=g_rgb16.Position.rgbGreen; //spostamento a dx bit verdi
					dwBShift=g_rgb16.Position.rgbBlue;  //spostamento a dx bit blue                
					
					fEBlue=(LONG) (pl.peBlue * lMulti)/ 8;
					fEGreen=(LONG) (pl.peGreen *lMulti)/ 8;
					fERed=(LONG) (pl.peRed *lMulti)/ 8;
					lRed=new LONG[65536];
					lGreen=new LONG[65536];
					lBlue=new LONG[65536];
					wColors=new WORD[65536];
                    //estrae le componenti RGB da ogni word
					for (dwCount=0;dwCount<65536;dwCount++)
					{
						lRed[dwCount]= ((dwCount & dwRMask) >> dwRShift);
						lGreen[dwCount]= ((dwCount & dwGMask) >> dwGShift);
						lBlue[dwCount]= ((dwCount & dwBMask) >> dwBShift);						

					}
				}

				//calcola gli incrementi per ogni componente per raggiungere il colore finale
				//moltiplica per lMulti per non perdere informazioni e per usare l'aritmetica intera invece che in virgola mobile
				for (iCount=0;iCount<256;iCount++)
				{
					pfSBlue[iCount]=(fEBlue-iCount * lMulti)/iTotalSteps;
					pfSGreen[iCount]=(fEGreen-iCount * lMulti)/iTotalSteps;
					pfSRed[iCount]=(fERed-iCount * lMulti)/iTotalSteps;
				}

				//inizializzazione del ciclo
				bInitialized=TRUE;

			}
			else 
			{
				//Inizializzazione fallita: rilascia la memoria allocata
				SAFE_DELETE_ARRAY(pOrigBytes);
				SAFE_DELETE_ARRAY(pfSBlue);
				SAFE_DELETE_ARRAY(pfSGreen);
				SAFE_DELETE_ARRAY(pfSRed);
				bInitialized=FALSE;
			}

		}
	}

	if (bInitialized)
	{
		
		iStep++;
		
		if (iStep<=iTotalSteps) 
		{
			switch(dwBitsPerPixel)
			{
			case 8:break; //non implementato (usare FadePalette)

			case 16:
			
				//esegue un pre ciclo di scansione e crea una tabella 
				//con i valori che ogni word deve assumere in questo passo
                for (dwCount=0;dwCount<65536;dwCount++)
				{
			
					byRed = (BYTE)((lRed[dwCount]*lMulti+pfSRed[lRed[dwCount]]*iStep)/lMulti);
					byGreen = (BYTE)((lGreen[dwCount]*lMulti+pfSGreen[lGreen[dwCount]]*iStep)/lMulti);
					byBlue = (BYTE)((lBlue[dwCount]*lMulti+pfSBlue[lBlue[dwCount]]*iStep)/lMulti);
				
					wColors[dwCount]=(byRed << g_rgb16.Position.rgbRed) | (byGreen << g_rgb16.Position.rgbGreen) | byBlue;				

				}

				//esegue un ciclo sul buffer dell'immagine
				for (dwCount=0;dwCount<dwBuffSize;dwCount += 2)
				{
					//azzera tutti i bit della word prima di copiare
					wColor16=0;
					memcpy(&wColor16,pOrigBytes+dwCount,2);					
		
					wColor16=wColors[wColor16];
					memcpy (pBytes+dwCount,&wColor16,2);

				}

				//sblocca la superfcie
				UnlockSurface(lpDDS,pBytes);
				break;
				
            case 24:
                
				if (iStep == iTotalSteps)
				{   //passo finale a 24 bit
					for (dwCount=0;dwCount<dwBuffSize;dwCount += 3)
					{				

						pBytes[dwCount]=(BYTE)((BYTE)fEBlue/lMulti);
						pBytes[dwCount+1]=(BYTE)((BYTE)fEGreen/lMulti);
						pBytes[dwCount+2]=(BYTE)((BYTE)fERed/lMulti);						
						
					}
				

				}

				
				else
				{


					for (dwCount=0;dwCount<dwBuffSize;dwCount += 3)
					{				

						//Blu
						pbyCol=&pOrigBytes[dwCount];
						pBytes[dwCount]=(BYTE)(pfSBlue[*pbyCol]/lMulti*iStep+ (*pbyCol));

						//Verde
						pbyCol=&pOrigBytes[dwCount+1];
						pBytes[dwCount+1]=(BYTE)(pfSGreen[*pbyCol]/lMulti*iStep+(*pbyCol));

						//Rosso
						pbyCol=&pOrigBytes[dwCount+2];
						pBytes[dwCount+2]=(BYTE)(pfSRed[*pbyCol]/lMulti*iStep+(*pbyCol));								
						
					}
				
				}
				
				UnlockSurface(lpDDS,pBytes);
				pbyCol=NULL;
				break;
		
			}

			return TRUE;
		}
		else 
		{

			UnlockSurface(lpDDS,pBytes);
			pBytes=NULL;
			bInitialized=FALSE;
            //rilascia la memoria
			SAFE_DELETE_ARRAY(pfSRed);
			SAFE_DELETE_ARRAY(pfSBlue);
			SAFE_DELETE_ARRAY(pfSGreen);
			SAFE_DELETE_ARRAY(pOrigBytes);
			SAFE_DELETE_ARRAY(wColors);
			SAFE_DELETE_ARRAY(lRed);
			SAFE_DELETE_ARRAY(lGreen);
			SAFE_DELETE_ARRAY(lBlue);

			return FALSE; //fine del ciclo			
		}
	}

	else return FALSE;
	
}

//////////////////////////////////// FadeScreen ////////////////////////////////

//Esegue il fading della superficie primaria usando il
//gamma control della scheda grafica
//a differenza di FadeColors puo' essere applicato solo alla superficie primaria

BOOL CADXGraphicManager::FadeScreen(const COLORREF clDestColor,const int iSteps,BOOL bInitCycle)
{
	LPDIRECTDRAWGAMMACONTROL lpDDGammaControl = NULL; //gamma control
	DDGAMMARAMP DDGammaRamp; //valori gamma attuali
	DDGAMMARAMP DDGammaOld;  //valori gamma originali 
    HRESULT hr;
	DWORD dwCount;

	hr=lpDDS[0]->QueryInterface(IID_IDirectDrawGammaControl,(LPVOID *)&lpDDGammaControl); 
   
	if (!FAILED(hr))
	{
		
		//salva i valori gamma attuali
		hr=lpDDGammaControl->GetGammaRamp(0, &DDGammaOld); 

		if (FAILED(hr)) MessageBox(g_hwnd,GetErrorDescription(hr),"err",16);

		hr=lpDDGammaControl->GetGammaRamp(0, &DDGammaRamp);

		for (dwCount=0;dwCount<256;dwCount++)
		{
			if (DDGammaRamp.red[dwCount] > 0)
			{
				DDGammaRamp.red[dwCount]=0;

				hr=lpDDGammaControl->SetGammaRamp(0, &DDGammaRamp); 

				
			}

			//we wouldn't want to decrement a value unless it's greater than 0
            if(DDGammaRamp.green[dwCount] > 0)
			{
				//set the current value of DDGammaRamp.yellow to 0.
				DDGammaRamp.green[dwCount]=0; 
				hr=lpDDGammaControl->SetGammaRamp(DDSGR_CALIBRATE, &DDGammaRamp);
			} //end if
	

			//we wouldn't want to decrement a value unless it's greater than 0
			if(DDGammaRamp.blue[dwCount] > 0) 
			{
				//set the current value of DDGammaRamp.Blue to 0.
				DDGammaRamp. blue [dwCount]=0; 
				lpDDGammaControl->SetGammaRamp(DDSGR_CALIBRATE, &DDGammaRamp);

			} //end if

		}//end for
	}

	return TRUE;
}

////////////////////////////////// DisplayPalette /////////////////////////////
//mostra sulla superficie corrente la palette
//(solo modalit� 8bit)
void CADXGraphicManager::DisplayPalette(void)
{

	int x,y,r,c;
	int count;
    int dx;
	int dy;
    
	if (lpDDpalette)
		{

		dx=g_screen_width/18;
		dy=g_screen_height/18;
 
		if (g_screen_bpp == 8)
		{
			for (count=0;count<256;count++)
			{

				r=int(count/16);
				c=count % 16;
				x=10+c*dx;			
				y=10+r*dy;
				DrawRectangle(x,y,x+dx-2,y+dy-2,count);	
				TextOut(x,y,"%d",count);
			}

		}
	}
}

////////////////////////////// CADXBase /////////////////////////////////////////

//costruttore
CADXBase::CADXBase(void)
{

	//inizializza i membri della classe
	m_lpFm=NULL;
	m_lpGm=NULL;
	m_iStatus=0;
}


//----------------------- SetFrameManager ----------------------------------

//Imposta l'oggetto frame manager corrente
//se l'�oggetto pfm � valido, imposta anche graphic manager acquisendolo
//da pfm

HRESULT CADXBase::SetFrameManager(CADXFrameManager *pfm)
{
	if (pfm)
	{
		if (pfm->GetStatus() > 0)
		{
			if (m_lpGm == NULL)
			{
				//l'oggetto gm viene acquisito dal frame manager impostato
				m_iStatus=1;
				m_lpFm=pfm;
				m_lpGm=pfm->GetGraphicManager();

				return S_OK;

			}
			else 				
			{
				if (m_lpGm != pfm->GetGraphicManager()) 
				{ 
					return E_FAIL; 
				}
				else 
				{
					m_iStatus=1;
					m_lpFm=pfm;
					return S_OK;
				}
			}
		}

		else return E_FAIL;
	}

	else return E_FAIL;

}

//------------------------- SetGraphicManager --------------------

HRESULT CADXBase::SetGraphicManager(CADXGraphicManager *pgm)
{
	if (pgm)
	{
		if (pgm->GetStatus() > 0) 
		{
			m_lpGm=pgm;
			return S_OK;
		}
	}

	return E_FAIL;
}


//-------------------------- GetStatus ----------------------------------------

int CADXBase::GetStatus(void)
{
	return m_iStatus;
}

//----------------------------- GetGraphicManager -----------------------------

CADXGraphicManager *CADXBase::GetGraphicManager(void)
{
	return m_lpGm;
}

//------------------------------ GetFrameManager ---------------------------

CADXFrameManager *CADXBase::GetFrameManager(void)
{
	return m_lpFm;
}


//-------------------------------- ~CADXBase --------------------------------

CADXBase::~CADXBase(void)
{
	m_iStatus=0;
	m_lpFm=NULL;
	m_lpGm=NULL;

}

//////////////////////////////// CADXRender ///////////////////////////////////////
//classe astratta CADXRender
//� usata per fornire metodi di rendering su immagini diverse dal video

CADXRender::CADXRender(void)
{
	m_iBltEffcts=DDBLTFAST_SRCCOLORKEY; //inizializza
}

//----------------------- SetDisplay ---------------------------------

//Imposta l'immagine su cui effettuare il rendering
HRESULT CADXRender::SetDisplay(IMAGE_FRAME_PTR imgDisplay)
{
	if (imgDisplay->status > 0)
	{
		//imposta il rettangolo di clipping
		m_rcClip.left=0;
		m_rcClip.top=0;
		m_rcClip.bottom=imgDisplay->height;
		m_rcClip.right=imgDisplay->width;

		m_pimgOut=imgDisplay;
		return S_OK;
	}

	else return E_FAIL;
}


HRESULT CADXRender::RenderToDisplay()
{
	return S_OK;
}

HRESULT CADXRender::RenderToDisplay(LONG x,LONG y)
{
	return S_OK;
}

//---------------------- SetBlitEffects ------------------------------
//Imposta gli effetti da usare con la funzione di blitting
void CADXRender::SetBlitEffects(int iBltEffcts)
{
	m_iBltEffcts=iBltEffcts;

}

///////////////////// CADXFrameManager ///////////////////////////////////////////
/* Questa classe implementa tutte le funzioni necessarie alla manipolazione delle frames
*/

//costruttore------------------------------------------------------
//si deve passare l'oggetto graphic manager precedentemente inizializzato

CADXFrameManager::CADXFrameManager(void)
{	

	m_lTotmem=0;
	m_status=0;
    m_bpp=8;
	m_lpGm=NULL;
	transp_color=0;
	dest_transp_color=0;
	iInstanceCounter++;
	DD_INIT_STRUCT(m_ddsd);
	//impostazione di default delle prop. delle superfici create
	m_dwDefCaps=DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY;		

}

//distruttore------------------------------------------------------
CADXFrameManager::~CADXFrameManager(void)
{
	m_lpGm=NULL;
	m_bpp=0;
	m_status=0;
	iInstanceCounter--;
}

//------------------------------------------------------------------

int CADXFrameManager::GetStatus(void)
{
	return m_status;
}

//------------------------------------------------------------------
//questo metodo va chiamato dopo aver impostato la modalit� grafica
HRESULT CADXFrameManager::SetGraphicManager(CADXGraphicManager *pgm)
{
	if ((pgm) && m_status==0)  //controlla che l'oggetto sia valido ed inizializzato
	{
		if(pgm->GetDDObject()) 
		{
			m_status=1;       //sistema grafico inizializzato
			//acquisisce la profondit� colore impostata
			m_bpp=pgm->GetScreenBitsPerPixel();			
			m_lpGm=pgm;
            //crea il rettangolo che definisce lo schermo usato 
			//dalle funzioni PutImgFrame per calcolare il rettangolo di clipping
			//Le dimensioni grafiche impostate sono gi� note dall'oggetto GraphicManager precedentemente creato
			m_dwScreenWidth=pgm->GetScreenWidth();
			m_dwScreenHeight=pgm->GetScreenHeight();		
            

			return S_OK;

		}
		else WriteErrorFile("Frame Manager : modalit� grafica non impostata");
		return E_FAIL;
	}

 else WriteErrorFile("Frame Manager : errore di inizializzazione");
 return E_FAIL;

}

//---------------------- GetGraphicManager ------------------------------
//restituisce l'oggetto graphic manager impostato
CADXGraphicManager *CADXFrameManager::GetGraphicManager(void)
{
	return m_lpGm;	
}


HRESULT CADXFrameManager::SetTranspColor(COLORREF color)
{
	transp_color=color;	
	return S_OK;
}
/*------------------------ LoadBitmap -------------------------------
// Carica un bitmap - restituisce 1 se riesce a caricare il BMP
*/
int CADXFrameManager::LoadBitmap(BITMAP_FILE_PTR bitmap,char *filename)
{
	
	HANDLE file_handle;
	int intType;
	int index;
	int temp_color;
	LONG count;
	LONG bytes_per_line; 
	LONG height;
	DWORD dwFileSize;
	DWORD dwBufferSize;
	DWORD dwImageSize;
	DWORD dwBfOffsBits;
	DWORD dwBytesWidth;
	LPDWORD dwHighSize;	
	DWORD dwBytesRead;    
	BYTE *pFile;
	BOOL bSuccess;
	BITMAPFILEHEADER *pbmfh;
	//BITMAPCOREHEADER *pbmch;
    BITMAPV4HEADER   *pbmih;    //header versione 4  		    
    
	if (m_status == 0) {WriteErrorFile("LoadBitmap: Graphic Manager non impostato");return 0;}
	if (bitmap->status == 1) FreeBitmap(bitmap);
    //resetta il bitmap
	memset((BYTE *)&bitmap->bitmapfileheader,0,sizeof(bitmap->bitmapfileheader));
	memset((BYTE *)&bitmap->bitmapfileheader,0,sizeof(bitmap->bitmapfileheader));
	bitmap->buffer=NULL;
	bitmap->status=0;
   
	//apre il file che contiene il bitmap in lettura
	if ((file_handle=CreateFile(filename,GENERIC_READ,
		                            FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL)) == INVALID_HANDLE_VALUE) 
	{ 
		WriteErrorFile("CADXFrameManager::LoadBitmap : Impossibile aprire il file %s",filename);
		return (0);
	}
    
    dwHighSize=NULL; 
   
	//acquisisce la dimensione del file aperto 
	//la word di ordine alto delle dimensioni deve essere zero altrimenti si ha a che fare con un file troppo grande
	dwFileSize=GetFileSize(file_handle,NULL);
	
	if (dwHighSize!=NULL) {CloseHandle(file_handle);return (0);} //file maggiore di 4GB: non viene trattato
	
	//alloca un buffer nel quale viene caricato tutto il file
	pFile=new BYTE[dwFileSize];

	if (pFile) {
    
		 bSuccess=ReadFile(file_handle,pFile,dwFileSize,&dwBytesRead,NULL);
         
		 if ((!bSuccess) || (dwBytesRead != dwFileSize)) 
		 {
			CloseHandle(file_handle);SAFE_DELETE_ARRAY(pFile);
			WriteErrorFile("CADXFrameManager::LoadBitmap : Impossibile leggere %lu bytes dal file %s.",dwFileSize,filename);
		    return 0;

		 }

		 else {
         //ha letto il numero esatto di bytes e quindi procede
              CloseHandle(file_handle); //chiude l'handle tanto non serve piu'
			  	  
			  pbmfh=(BITMAPFILEHEADER *)pFile;
              //controlla che sia un bitmap
			  if (pbmfh->bfType != BITMAP_ID) 
			  {
               SAFE_DELETE_ARRAY(pFile);return(0);                
			  } 	
			  //1] legge il file header 
			  //e copia nella struttura 
			  memcpy(&bitmap->bitmapfileheader,pFile,sizeof(bitmap->bitmapfileheader));
              
			  //2]legge l'infoheader e determina il tipo di bitmap	  
			  //dalle dimensioni si capisce che tipo �
			  pbmih=(BITMAPV4HEADER *) (pFile+sizeof(BITMAPFILEHEADER));
			  
			  switch (pbmih->bV4Size) 
			  {
			  case sizeof(BITMAPCOREHEADER): intType=0;break;
              case sizeof(BITMAPINFOHEADER): intType=1;break;
              case sizeof(BITMAPV4HEADER):intType=2;   break;
              default: intType=-1;
                       SAFE_DELETE_ARRAY(pFile);
					   return (2);
               }// fine switch
            
              //Implementa i tipi supportati
			  switch (intType) 
			  {

			  case 1:  //Windows o OS/2 Bitmap 
				    
                    dwBfOffsBits=bitmap->bitmapfileheader.bfOffBits;

				    memcpy(&bitmap->bitmapinfoheader,pFile+sizeof(BITMAPFILEHEADER),sizeof(BITMAPINFOHEADER));
				  
				    //3]  Carica la palette se � presente
                    if (bitmap->bitmapinfoheader.biBitCount == 8) 
					{
						//palette 8 bit
						memcpy(&bitmap->palette,pFile+sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER),256*sizeof(PALETTEENTRY));
                        
						for (index=0;index<256;index++) 
						{
							//scambia il rosso e il blu
							temp_color=bitmap->palette[index].peRed;  
							bitmap->palette[index].peRed=bitmap->palette[index].peBlue;
							bitmap->palette[index].peBlue=temp_color;
							//indica di salvare il colore in una pos. libera
							bitmap->palette[index].peFlags=PC_NOCOLLAPSE;
						}//fine for   

					 }//fine if
					 //4]Carica il buffer (le dimensioni sono biImageSize e NON heigh*width*bitperpixel               
					 //if (!(bitmap->buffer = (BYTE *) malloc(bitmap->bitmapinfoheader.biSizeImage)))
		            if (!(bitmap->buffer = new BYTE[bitmap->bitmapinfoheader.biSizeImage]))		             
					{
						CloseHandle(file_handle);
		                return (3);
        
                      }//fine if
                      bitmap->status=1;
					  //altezza (numero di righe)
					  height=bitmap->bitmapinfoheader.biHeight;
                      //in alcuni casi l'altezza � negativa (indica bitmap non rovesciato)
					  //questo caso non � supportato
					  if (height<=0)
					  {
						  WriteErrorFile("LoadBitmap : il file % s ha altezza non valida.",filename);                        	
						  CloseHandle(file_handle);		                  
						  return (4);     
					  }					  
					  
					  //dimensione immagine
					  dwImageSize=bitmap->bitmapinfoheader.biSizeImage;				  
					  //alcune volte biSizeImage � zero
					  //questo caso viene trascurato (per ora)
					  if (dwImageSize == 0)
					  {
						  WriteErrorFile("LoadBitmap : il file % s non ha dimensioni valide (biSizeImage=0).",filename);                        	
						  CloseHandle(file_handle);		                  
						  return (4); 
					  }			  					  
					  //byte per ogni linea (attenzione le DIB hanno larghezze multiple di 4)
                      dwBytesWidth=dwImageSize/height;
                      //larghezza in bytes del buffer della struttura bitmap
					  bytes_per_line=bitmap->bitmapinfoheader.biWidth*(bitmap->bitmapinfoheader.biBitCount/8);					  
					  //dimensioni buffer
					  dwBufferSize=height*bytes_per_line;
					  //il bitmap va rovesciato
					  for (count=0;count<height;count++) {
							 //copia i bytes nel buffer
					         memcpy(bitmap->buffer+((height-count-1)*dwBytesWidth),pFile+dwBfOffsBits+count*dwBytesWidth,(size_t)bytes_per_line);						  

					  }			
					  
					  SAFE_DELETE_ARRAY(pFile); 
              break;                   
              
			  case 2: 

				  break;

			  case 0:
				  //formato DIB OS/2 non implementato
				  break;

                      

			  }//fine switch intType
			  return(1);

         }//fine if bSuccess
    }//fine if pFile
	
    else return 0; //allocazione fallita 
}

/*------------------- FreeBitmap ---------------------*/
// Rilascia un bitmap caricato precedentemente
//

void CADXFrameManager::FreeBitmap(BITMAP_FILE_PTR bitmap) {
     
     if (bitmap)
	 {
		 bitmap->status=0;

		 if (bitmap->buffer && bitmap->status==1) 
		 {     
     
			 SAFE_DELETE_ARRAY(bitmap->buffer);
			 
			 bitmap->buffer=NULL;

	     }
	 
	 }

}

//----------------------------- CreateFrameSurface -----------------------
//Crea la superfice DD della frame settando il colore della trasparenza su transp_color
//viene anche settato il width_fill
//viene usato l'oggetto DirectDraw principale 
//width e height sono le dimensioni in pixel della superficie da creare
HRESULT CADXFrameManager::CreateFrameSurface(IMAGE_FRAME_PTR frm,DWORD width,DWORD height)
{
        
       HRESULT hr; 
       DWORD dwSize,dwPitch;
	   BYTE *pSurface=NULL;
       DDSURFACEDESC2 ddsd;

	   if (frm->status==1 && frm->surface) FreeImgFrame(frm); 
	   if (m_lpGm == NULL) {WriteErrorFile(TEXT("CreateFrameSurface: l'oggetto FrameManager non � impostato."));return E_FAIL;}
       if (m_lpGm->GetStatus() != 1) {WriteErrorFile(TEXT("CreateFrameSurface : l'oggetto graphic manager non � impostato."));return E_FAIL;}
 
	   frm->width_fill = ((width%8!=0) ? (8 - width % 8) : 0);
	   frm->width=width;
	   frm->height=height;   
	   frm->transp_color=this->transp_color; //imposta il colore trasparente
       frm->status=0;
	   frm->rcClip.left=0;
	   frm->rcClip.top=0;
	   //imposta il rettangolo di clipping per non doverlo 
	   //ridefinire ogni volta
	   frm->rcClip.bottom=(LONG)height;
	   frm->rcClip.right=(LONG)width;
	   frm->pbuffer=NULL;

		//Imposta le caratteristiche delle superfici delle frame create con CreateFrameSurface
		//resetta la struttura
		DD_INIT_STRUCT(ddsd);	  
		//idica che solo i membri dwWidth dwHeigh e ddsCaps sono validi 
		//in questa struttura ddsd (setto solo quelli)
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT | DDSD_CKSRCBLT;

		// DDSCAPS_OFFSCREENPLAIN indica che la superficie � semplicemente
		//un'area di memoria flat usata per il rendering nel back buffer	
		ddsd.ddsCaps.dwCaps = m_dwDefCaps;        
        //setta il colore chiave sorgente (colore dei pixel che non vengono copiati nei blit trasparenti)
		ddsd.ddckCKSrcBlt.dwColorSpaceHighValue=transp_color;
		ddsd.ddckCKSrcBlt.dwColorSpaceLowValue=transp_color;
        //setta il colore chiave destinazione (colore dei pixel sui quali verr� copiata l'immagine)
	    //per ora non usato
		//	ddsd.ddckCKDestBlt.dwColorSpaceHighValue=dest_transp_color;
	    //	ddsd.ddckCKDestBlt.dwColorSpaceLowValue=dest_transp_color;

	    //le dimensioni in pixel della superficie da creare 
	    ddsd.dwWidth=width+frm->width_fill;
	    ddsd.dwHeight=height;
	   // DDSCAPS_OFFSCREENPLAIN indica che la superficie � semplicemente
	   //un'area di memoria flat usata per il rendering nel back buffer	
	   //crea la superficie	    
	   hr=m_lpGm->lpDD->CreateSurface(&ddsd,&frm->surface,NULL);	   
	   
	   if (FAILED(hr))
	   {
		   //se fallisce tente di creare la superficie nella memoria di sisteme
		   //per default crea la sup. nella memoria di sistema, ma il prog. chiamante 
		   //potrebbe aver cambiato le impostazioni di dwCaps in modo da creare la sup. nella memoria video
		   	ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY;
			hr=m_lpGm->lpDD->CreateSurface(&ddsd,&frm->surface,NULL);	   	   	   

	   }

	   //controlla che la creazione della sup. sia ok
	   if (FAILED(hr)) {WriteErrorFile("CreateFrameSurface:errore creando la superfice direct draw, width=%lu, height=%lu",width,height);return hr;}
       
	   //imposta il colore trasparente uguale a quello passato
	   //se i due membri seguneti sono uguali significa che color key
	   //� un colore e non un color space
	 //  color_key.dwColorSpaceLowValue  = transp_color;
	 //  color_key.dwColorSpaceHighValue = transp_color;              
	   // setta la chiave-colore come colore sorgente (DDCKEY_SRCBLT)
	   //significa che i pixel di quel colore non vengono copiati
	 //  frm->surface->SetColorKey(DDCKEY_SRCBLT, &color_key);       	   
	   //le frame create hanno sempre una larghezza in pixel multipla di 8

	   frm->width=ddsd.dwWidth;
	   frm->width_fill=0;
       
	   //blocca la sup. per poter mettere a zero tutti i bytes
	   pSurface=m_lpGm->LockSurface(frm->surface,(int *)&dwPitch);
       
		   if (pSurface==NULL) {
		                          WriteErrorFile("CreateFrameSurface :  impossibile bloccare la superficie");
                                  return E_FAIL;
		   }	  
     
	   dwSize=dwPitch*height;
	   //azzera tutti i byte della superficie
	   memset(pSurface,0x0,dwSize);
	   //incrementa il contatore della memoria allocata
	   m_lTotmem += dwSize;

       frm->status=1; //immagine creata

	   m_lpGm->UnlockSurface(frm->surface,pSurface);


	   return S_OK;

}

//----------------------- CreateImgFrame ---------------------------------------
//(overloaded)
//Crea una frame inserendo i bytes nel buffer immagine e settando le dimensioni
//L'immagine puo' essere creata a partire da bitmap a 8 16 o 24 bit
//La funzione tiene conto della profondit� colore corrente e converte il 
//buffer dell'immagine se necessario

HRESULT CADXFrameManager::CreateImgFrame(IMAGE_FRAME_PTR frm,BITMAP_FILE_PTR bitmap)
{
   BOOL retval=TRUE;   
   DWORD height,width; //altezza e larghezza dell'immagine in pixel       
   BYTE *src_ptr,*dest_ptr;
   HRESULT hr;
   DWORD dwPixelSize;      //dimensione in byte di un pixel sullo schermo corrente 
   DWORD dwBiBitCount;
   DWORD dwBuffSize;  
   DWORD dwDestPitch;
   DWORD dwSrcPitch;
   
   if (frm->status==1 && frm->surface) FreeImgFrame(frm);  
   if (bitmap->status != 1) return E_FAIL;
   if (m_status <1) {WriteErrorFile("CreateImgFrame: il sistema grafico non � inizializzato.");return E_FAIL;}

   //resetta la frame
   frm->surface=NULL;
   frm->transp_color=transp_color;
   frm->width=0;
   frm->height=0;
   frm->effect=0;
   
   if (bitmap) {
       
       //dimensioni in byte di un pixel sullo schermo corrente 
       dwPixelSize=m_bpp/8; 
	   //biWidth e biHeight sono dimensioni in pixel dell'immagine
	   width=bitmap->bitmapinfoheader.biWidth;
	   height=bitmap->bitmapinfoheader.biHeight;
	   //profondit� colore del bitmap
	   dwBiBitCount=bitmap->bitmapinfoheader.biBitCount;
	   
       //calcola le dimensioni del buffer
	   dwBuffSize=height*width*dwPixelSize;
      
       hr=CreateFrameSurface(frm,width,height);

	   if (FAILED(hr)) {WriteErrorFile("CreateImgFrame:errore creando la superficie della frame.");return hr;}
       
       //il puntatore sorgente punta all'inizio del buffer
       src_ptr=bitmap->buffer;
       //il puntatore destinazione punta sulla superficie creata
	   dest_ptr=(BYTE *)m_lpGm->LockSurface(frm->surface,NULL);	   
       
	   //copia i pixel dal buffer sorgente a quello destinazione, questa funzione si
	   //occupa anche delle trasformazioni fra le varie profondit� colore
       
	   dwDestPitch=GetSurfacePitch(frm->surface);	         

	   dwSrcPitch=GetPitchLength(width,dwBiBitCount);

	   CopyPixels(dest_ptr,src_ptr,dwDestPitch,dwSrcPitch,width,height,m_bpp,dwBiBitCount,bitmap->palette);

	   frm->surface->Unlock(NULL); //sblocca la superfice
       
	   frm->status=1;                  //frame caricata
       retval=TRUE;

   } else retval=E_FAIL;

   return S_OK;

}

//------------------------------------ SaveImgToBitmap -------------------------------------------------------------
//Salva una immagine in un file bmp a 24 bit

HRESULT CADXFrameManager::SaveImgToBitmap(TCHAR *szFileName,const IMAGE_FRAME_PTR pimgSrc )
{
	HANDLE fh;
	const TCHAR *szFnName=TEXT("SaveImgToBitmap");
	BITMAP_FILE bitmap;
	LPDWORD dwHighSize=NULL;	
	LONG lPitch;
	LPBYTE pbBuffer=NULL;
	DWORD dwImageSize;
	DWORD dwDestPitch;
	DWORD count;
	DWORD dwBytes;
	DWORD dwHeight;
	PALETTEENTRY *pal=NULL;
	BOOL bFreeBuff=FALSE;

	if (!pimgSrc)
	{
		WriteErrorFile("%s: l'immagine � nulla.",szFnName);
		return E_FAIL;
	}

	if (pimgSrc->status <1)
	{
		WriteErrorFile("%s: l'immagine non � caricata.",szFnName);
		return E_FAIL;
	}

	//blocca la sup. ed acquisisce il buffer dell'immagine
	pbBuffer=GetBuffer(pimgSrc,&lPitch);

	if (!pbBuffer)
	{
		WriteErrorFile("%s: impossibile acquisire il buffer dell'immagine, errore bloccando la superficie.",szFnName);
		return E_FAIL;	
	}

	if (!szFileName)
	{
		WriteErrorFile("%s: il nome del file su cui salvare non � valido.",szFnName);
		return E_FAIL;		
	}

	fh=CreateFile(szFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);

	if (fh==INVALID_HANDLE_VALUE)	
	{
		WriteErrorFile("%s: Errore aprendo il file %s in scrittura.",szFnName,szFileName);
		return E_FAIL;		
	}

	bitmap.status=1;

	//resetta il bitmap
	memset((BYTE *)&bitmap.bitmapfileheader,0,sizeof(bitmap.bitmapfileheader));
	memset((BYTE *)&bitmap.bitmapfileheader,0,sizeof(bitmap.bitmapfileheader));
	bitmap.buffer=NULL;


	//imposta il file header
	bitmap.bitmapfileheader.bfType=BITMAP_ID; //word che identifica il bitmap
	bitmap.bitmapfileheader.bfReserved1=0;
	bitmap.bitmapfileheader.bfReserved2=0;
	

	bitmap.bitmapinfoheader.biBitCount=24; //salva sempre un bitmap a 24 bit
	bitmap.bitmapinfoheader.biXPelsPerMeter=0;
	bitmap.bitmapinfoheader.biYPelsPerMeter=0;
	bitmap.bitmapinfoheader.biWidth=pimgSrc->width;
	bitmap.bitmapinfoheader.biHeight=pimgSrc->height;
	bitmap.bitmapinfoheader.biClrImportant=0;
	bitmap.bitmapinfoheader.biSize=sizeof(BITMAPINFOHEADER); //dimensioni della struttura 
	bitmap.bitmapinfoheader.biPlanes=1; //il numero di piani � sempre 1
	bitmap.bitmapinfoheader.biClrUsed=0;//la palette non � usata perch� un bmp a 24 bit
	bitmap.bitmapinfoheader.biClrImportant=0;//non usato per bitmap a 16,24 e 32 bit
	bitmap.bitmapinfoheader.biCompression=BI_RGB;//bitmap non compressa
	//la larghezza di una riga in  bytes deve sempre essere multipla di 4;
	//bitmap.bitmapinfoheader.biSizeImage=(1+(pimgSrc->width+pimgSrc->width_fill)*3/4)*4;
	
	switch (m_lpGm->GetScreenBitsPerPixel())
	{

	case 16: //l'immagine da salvare � a 16 bit (HighColor)

		//alloca la memoria per il buffer del bitmap	    
		dwDestPitch=(lPitch/2)*3;
		dwImageSize=dwDestPitch*pimgSrc->height;
	
		//dimensione dell'intero file
	    bitmap.bitmapfileheader.bfSize=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+dwImageSize;
	    //dimensione del buffer immagine 
		bitmap.bitmapinfoheader.biSizeImage = dwDestPitch*pimgSrc->height; //dimensione dell'immagine in bytes			
		//offset del buffer
		bitmap.bitmapfileheader.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
	
		bitmap.buffer=new BYTE[dwImageSize];

		if (!bitmap.buffer)
		{
			WriteErrorFile("%s: Errore allocando il buffer di %d bytes.",szFnName,dwImageSize);
			CloseHandle(fh);
		    return E_FAIL;	

		}

		//converte il buffer da 16 a 24 bit
		CopyPixels(bitmap.buffer,pbBuffer,dwDestPitch,(DWORD)lPitch,pimgSrc->width,pimgSrc->height,24,16,NULL);        

		bFreeBuff=TRUE; //dopo deve rilasciare il buffer

		break;

	case 24: //l'immagine da salvare � a 24 bit

		//alloca la memoria per il buffer del bitmap	    
		dwDestPitch=lPitch;
		dwImageSize=dwDestPitch*pimgSrc->height;
	
		//dimensione dell'intero file
	    bitmap.bitmapfileheader.bfSize=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+dwImageSize;
	    //dimensione del buffer immagine 
		bitmap.bitmapinfoheader.biSizeImage = dwImageSize; //dimensione dell'immagine in bytes			
		//offset del buffer
		bitmap.bitmapfileheader.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);			
	
		//il buffer viene copiato cosi' com'�
		CopyPixels(bitmap.buffer,pbBuffer,dwDestPitch,(DWORD)lPitch,pimgSrc->width,pimgSrc->height,24,24,NULL);        
	
		bitmap.buffer=pbBuffer;

		bFreeBuff=FALSE; //dopo non deve rilasciare il buffer

		break;
		
	case 8://bitmap a 8 bit

		//alloca la memoria per il buffer del bitmap	    
		dwDestPitch=lPitch;
		dwImageSize=dwDestPitch*pimgSrc->height;
		bitmap.bitmapinfoheader.biBitCount=8; 
		bitmap.bitmapinfoheader.biClrUsed=256;
	
		//dimensione dell'intero file
	    bitmap.bitmapfileheader.bfSize=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+dwImageSize+256*sizeof(RGBQUAD);
	    //dimensione del buffer immagine 
		bitmap.bitmapinfoheader.biSizeImage = dwImageSize; //dimensione dell'immagine in bytes			
		//offset del buffer
		bitmap.bitmapfileheader.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+256*sizeof(RGBQUAD);
			
		bitmap.buffer=pbBuffer;

		/*if (!bitmap.buffer)
		{
			WriteErrorFile("%s: Errore allocando il buffer di %d bytes.",szFnName,dwImageSize);
			CloseHandle(fh);
		    return E_FAIL;	

		}*/

		bFreeBuff=FALSE; //dopo non deve rilasciare il buffer
		
		break;

	default:

		WriteErrorFile("%s: Profondit� colore di %d bit non ancora gestita.",szFnName,m_lpGm->GetScreenBitsPerPixel());
		CloseHandle(fh);
		return E_FAIL;				

	}//fine switch


	//scrive sul file
	//fileheader
	WriteFile(fh,&bitmap.bitmapfileheader,sizeof(BITMAPFILEHEADER),&dwBytes,NULL);
	
	if (!dwBytes)
	{
		CloseHandle(fh);
		//rilascia il buffer immagine
	    ReleaseBuffer(pimgSrc);
		WriteErrorFile("%s: Errore scrivendo il bitmap fileheader.",szFnName);
		return E_FAIL;
	}

	//bitmapinfo
	WriteFile(fh,&bitmap.bitmapinfoheader,sizeof(BITMAPINFOHEADER),&dwBytes,NULL);
	
	if (!dwBytes)
	{
		CloseHandle(fh);
		//rilascia il buffer immagine
	    ReleaseBuffer(pimgSrc);
		WriteErrorFile("%s: Errore scrivendo il bitmap infoheader.",szFnName);
		return E_FAIL;
	}

	//se il bitmap � a 8 bit scrive la palette
	if (bitmap.bitmapinfoheader.biBitCount==8)
	{
		pal=new PALETTEENTRY[256];
		m_lpGm->GetPaletteEntries(bitmap.palette);

		if (!pal)
		{
			CloseHandle(fh);
			//rilascia il buffer immagine
	        ReleaseBuffer(pimgSrc);
			WriteErrorFile("%s: Errore scrivendo la palette.",szFnName);
			return E_FAIL;
		}
	
		WriteFile(fh,bitmap.palette,sizeof(RGBQUAD)*256,&dwBytes,NULL);

		if (!dwBytes)
		{
			WriteErrorFile("%s: Errore scrivendo la palette.",szFnName);
			return E_FAIL;
		}
	}

	dwHeight=pimgSrc->height;
	//scrive il buffer dell'immagine bitmap
	//il buffer va rovesciato perch� l'immagine nei file bitmap � salvata capovolta
	//bitmapinfo
	for (count=0;count<dwHeight;count++)
	{
		//scrive una linea del buffer
		WriteFile(fh,bitmap.buffer+(dwHeight-count-1)*dwDestPitch,dwDestPitch,&dwBytes,NULL);
	}	
	
	WriteFile(fh,bitmap.buffer,dwImageSize,&dwBytes,NULL);
	
	if (!dwBytes)
	{
		WriteErrorFile("%s: Errore scrivendo il buffer dell'immagine.",szFnName);
		return E_FAIL;
	}	
	
	//rilascia il buffer bitmap
	if (bFreeBuff) SAFE_DELETE_ARRAY(bitmap.buffer);

	//rilascia il buffer immagine
	ReleaseBuffer(pimgSrc);
	//chiude il file
	CloseHandle(fh);	

	return S_OK;

}
//----------------------------- CopyPixels -----------------------------------------------------------------------------------------------------------------------
//Copia un buffer grafico sorgente in uno destinazione
//dest_buffer=punta l'indirizzo nel quale si inizia a copiare
//src_buffer=indirizzo iniziale del buffer sorgente
//dwDestPitch = pitch in bytes del buffer destinazione
//dwSrcPitch = pitch in bytes del buffer sorgente
//dwSrcWidth = larghezza immagine sorgente in pixel
//dwHeigth=altezza immagine (o numero di righe del buffer)
//dwDestBitCount,dwDestSrcCount = profondit� colore dell'immagine sorgente e di quella destinazione
//plSrc = punta alla palette dell'immagine sorgente se � a 8 bit altrimenti puo' essere nullo
//N.B. Quando si converte un'immagine da 24 o 16 bit in una a 8 bit viene usato come riferimento la palette 
//corrente e quindi in questo caso l'oggetto GraphicManager deve essere inizializzato

HRESULT CADXFrameManager::CopyPixels(BYTE *dest_buffer,BYTE *src_buffer,DWORD dwDestPitch,DWORD dwSrcPitch,DWORD dwSrcWidth,DWORD dwHeight,DWORD dwDestBitCount,DWORD dwSrcBitCount,PALETTEENTRY *plSrc)
{
	BYTE byCur;
    DWORD count,count1,dwPixCount;
    DWORD dwDestPixelSize,dwSrcPixelSize;
	DWORD offs=0,offs1=0;
    PALETTEENTRY *pl,pe;
	COLORREF cRGB;
	DWORD dwHighPixel; 
	DWORD dwBytesToCopy;  //byte da copiare
	BYTE *pOldDest,*pOldSrc;
    BYTE r,g,b;
	RGB16 rgb16;
    unsigned wPix;

	//le profondit� colore prese in considerazione sono 8,16,24
    if (!((dwSrcBitCount == 8) || (dwSrcBitCount == 16) || (dwSrcBitCount == 24))) return E_FAIL;
    if (!((dwDestBitCount ==8) || (dwDestBitCount ==16) || (dwDestBitCount== 24))) return E_FAIL;
    
    dwDestPixelSize = dwDestBitCount/8;
	dwSrcPixelSize = dwSrcBitCount/8;
        
    //memorizza gli indirizzi per ripristinarli alla fine
    pOldDest=dest_buffer;
	pOldSrc=src_buffer;
    //byte da copiare (esclude la parte usata come riempimento)
    dwBytesToCopy=dwSrcPixelSize*dwSrcWidth;

    //profondit� colore del bitmap e dello schermo coincidono
	if (dwSrcBitCount == dwDestBitCount)
	   {
		   
		   for (count=0;count<dwHeight;count++) 
		   {
			   //esegue la copia di una linea dal bitmap al buffer della superficie 
			   memcpy(dest_buffer,src_buffer,dwBytesToCopy);
      
			   src_buffer += dwSrcPitch;     //il ptr sorgente viene incrementato della larghezza del bitmap in bytes
			   dest_buffer += dwDestPitch;   //il ptr destinazione viene incrementato della larghezza della superficie in bytes
       
		   }

		   return S_OK;

	   }

	   else if (dwSrcBitCount==8 && dwDestBitCount>8)
	   {

		//bitmap a 8 bit su schermo  high color o true color
        //in questo caso le triple RGB vanno prese dalla palette del bitmap		
		    for (count=0;count<dwHeight;count++)
			{
				for (count1=0;count1<dwSrcWidth;count1++)
				{
					//acquisisce il pixel
					byCur=src_buffer[count1];
					//punta alla palette
					pl=&plSrc[byCur];
					//crea il pixel 			                 
                    cRGB=m_lpGm->CreateRGB(pl->peRed,pl->peGreen,pl->peBlue);
                    memcpy(dest_buffer+count1*dwDestPixelSize,(BYTE *)&cRGB,dwDestPixelSize);

				}

				src_buffer += dwSrcPitch;
				dest_buffer += dwDestPitch;
			}

			return S_OK;

	   }

	   //bitmap high color o true color su schermo a 8 bit
	   else if (dwSrcBitCount > dwDestBitCount && dwDestBitCount==8)
	   {	  

         for (count=0;count<dwHeight;count++) 
		   {
			 for (count1=0;count1<dwSrcWidth;count1++)
			 { 
               //copia il pixel nella dword
			   memcpy((BYTE *)&dwHighPixel,src_buffer+count1*dwSrcPixelSize,(size_t)dwSrcPixelSize);
               //splitta il pixel in modo da ottenere le componenti RGB
			    m_lpGm->SplitRGB(&pe,dwHighPixel,dwSrcBitCount);			               			   	
			       								  
			   //cerca il colore piu' vicino nella palette corrente
			   byCur=(BYTE)m_lpGm->GetNearestPaletteIndex(pe.peRed,pe.peGreen,pe.peBlue,m_lpGm->m_pal);			   
			   //copia il byte che rappresenta l'inidce della palette nel buffer immagine
               memcpy(dest_buffer+count1,(BYTE *)&byCur,1L);		                
			 }

			 src_buffer += dwSrcPitch;
			 dest_buffer += dwDestPitch;

		   }
         
         //ripristina i buffers
		 src_buffer=pOldSrc;
		 dest_buffer=pOldDest;

		 return S_OK;
	   }

	   //High Color (16 bit)
	   //da 24 bit a 16 bit
       else if (dwSrcBitCount == 24 && dwDestBitCount==16)
	   {
		    
		   for (count=0;count<dwHeight;count++)
		   {
			   offs1=count*dwSrcPitch;
			   
			   dwPixCount=0;
              
			   for(count1=0;count1<dwBytesToCopy;count1 += 3)
			   {
				   offs=offs1+count1;
			       	   
				   b=src_buffer[offs];
				   g=src_buffer[offs+1];
				   r=src_buffer[offs+2];                                                         
				   //crea il colore a 16 bit				   
				   wPix=(WORD)m_lpGm->CreateRGB((int)r,(int)g,(int)b);
       
                   dest_buffer[dwPixCount*2]=LOBYTE(wPix);
				   dest_buffer[dwPixCount*2+1]=HIBYTE(wPix);                   				   
                   
				   dwPixCount++;
			   }
			   
			   dest_buffer += dwDestPitch;
		   }
		   return S_OK;
	   }

       //Da 16 bit a 24 bit
	   else if (dwSrcBitCount == 16 && dwDestBitCount == 24)
	   {		   
		   
		   rgb16=m_lpGm->GetPixelFormat();		   
		   
		   for (count=0;count<dwHeight;count++) //legge le righe dal buffer sorgente
		   {
			   offs1=count*dwSrcPitch; //offset della riga corrente nel buffer sorgente

			   dwPixCount=0;

			   for (count1=0;count1<dwBytesToCopy;count1 += 2) //prende due byte alla volta dalla riga da copiare 
			   {
				   offs=offs1+count1;//calcola l'offset del pixel corrente

                   wPix=0; //azzera tutti i bit del pixel
				   memcpy(&wPix,src_buffer+offs,2); //copia i 16 bite che sostituiscono il pixel dal buffer sorgente
                   
				   r = g = b = 0;				 
                  
				   //estrae i valori rgb dal pixel a 16 bit
				   r=(wPix & rgb16.dwRBitMask) >> rgb16.Position.rgbRed;
				   g=(wPix & rgb16.dwGBitMask) >> rgb16.Position.rgbGreen;
				   b=(wPix & rgb16.dwBBitMask) >> rgb16.Position.rgbBlue;				 

				   //converte i valori per il pixel a 24 bit
				   r=(r*0xFF)/((1<<rgb16.dwRBits)-1); //maschere 11111 e 
				   g=(g*0xFF)/((1<<rgb16.dwGBits)-1); //111111 per 565
				   b=(b*0xFF)/((1<<rgb16.dwBBits)-1);		

				   //copia il pixel nel buffer destinazione
				   dest_buffer[dwPixCount*3]=b;
				   dest_buffer[dwPixCount*3+1]=g;
				   dest_buffer[dwPixCount*3+2]=r;

				   dwPixCount++;
			   }

			    dest_buffer += dwDestPitch; //punta la riga successiv anel buffer destinazione
		   }
	   }

	   //da fare: modalit� a 32 bit

	   return E_FAIL;  //caso non trovato
}

//----------------------------------- ConvBuffer565 --------------------------

//Converte un buffer immagine nel formato 565 dal formato 555 (mod. 16 bit)
HRESULT CADXFrameManager::ConvBuffer565(BYTE *pBuffer,DWORD dwBuffSize)
{

	COLORREF pix;
	DWORD dwCount;
	DWORD dwGMask=0x3E0; //maschere mod. 555
	WORD dwRMask=0xFC00;
	DWORD dwBMask=0x1F;
	DWORD dwG,dwR,dwB;
    

//	WriteErrorFile("converte in 565");


	if (!pBuffer) return E_FAIL;

	//esegue la conversione di tutto il buffer prendendo 2 bytes alla volta
	for (dwCount=0;dwCount<dwBuffSize;dwCount += 2)
	{
		pix=0;
		//acquisisce il pixel dal buffer
		memcpy (&pix,pBuffer+dwCount,2);			

		if (pix>0)
		{
			//estrae il canale verde e moltiplica per due
			dwG = (pix & dwGMask) >> 5; //VERDE		
			dwG = (DWORD)((float)(63/31) *dwG);  //moltiplica x 2 (il verde ha 6 bit)			
			dwG <<= 5;
 			dwR = pix & dwRMask;  //ROSSO
			dwR <<=1;
			dwB = pix & dwBMask; //BLU
			
			//ricrea il pixel nel formato 565		
			pix= dwR  | dwG | dwB;

		}

		
		//copia il pixel trasformato nel buffer
		memcpy (pBuffer+dwCount,&pix,2);
	}

	return S_OK;
}

//------------------------------- ConvPixel555 ----------------------------------

//Converte un buffer immagine nel formato 555 dal formato 565 (mod. 16 bit)
HRESULT CADXFrameManager::ConvBuffer555(BYTE *pBuffer,DWORD dwBuffSize)
{

	COLORREF pix;
	DWORD dwCount;
	DWORD dwGMask=0x7E0; //maschere mod. 565
	DWORD dwRMask=0xF800;
	DWORD dwBMask=0x1F;
	DWORD dwG,dwR,dwB;

	if (!pBuffer) return E_FAIL;

	//esegue la conversione di tutto il buffer prendendo 2 bytes alla volta
 	for (dwCount=0;dwCount<dwBuffSize;dwCount += 2)
	{
		//azzera il pixel
		pix=0;
		//acquisisce il pixel dal buffer
		memcpy (&pix,pBuffer+dwCount,2);
		//estrae il canale verde e moltiplica per due
		dwG = (pix & dwGMask) >>5; 		
		dwG >>= 1;
		dwG <<= 5;
		dwR = (pix & dwRMask) >> 1; //Attenzione alla precedenza degli operatori >> e & !!!
		dwB = pix & dwBMask;
		
		//ricrea il pixel nel formato 555		
		pix = dwR  | dwG  | dwB;

		//copia il pixel trasformato nel buffer
		memcpy (pBuffer+dwCount,&pix,2);
	}

	return S_OK;
}

//----------------------------------------------------------------------------

DWORD CADXFrameManager::GetSurfacePitch(LPDIRECTDRAWSURFACE7 lpDDS)
{
	DDSURFACEDESC2 ddsd;
	if (lpDDS == NULL) return 0;
    
	//inizializza la struttura
	DD_INIT_STRUCT(ddsd);
    
	ddsd.dwFlags=DDSD_PITCH;

    lpDDS->GetSurfaceDesc(&ddsd);
    //restituisce la larghezza pitch della superficie
	return (DWORD) ddsd.lPitch;
}
//----------------------------------------------------------------

//Restituisce la larghezza totale in bytes (effettiva+riempimento)
//di un buffer di dati di un'immagine
//dwWidth=larghezza in pixels dell'immagine
//dwBpp = bit per pixel dell'immagine
DWORD CADXFrameManager::GetPitchLength(DWORD dwWidth,DWORD dwBpp)
{
	DWORD dwRes;
	DWORD dwLineBytes;

    dwLineBytes=dwWidth*(dwBpp/8);
	dwRes=(dwLineBytes % 4 != 0) ? (4 - dwLineBytes % 4) : 0;
	dwRes += dwLineBytes;

	return dwRes;

}

//-------------------------- GetBuffer ----------------------------------

BYTE *CADXFrameManager::GetBuffer(IMAGE_FRAME_PTR frm,LONG *lPitch)
{
	frm->surface->Lock(NULL,&m_ddsd,DDLOCK_SURFACEMEMORYPTR,NULL);
	if (lPitch) *lPitch=m_ddsd.lPitch;    

	frm->pbuffer=(BYTE *)m_ddsd.lpSurface;//imposta il buffer
	
	return ((BYTE *)m_ddsd.lpSurface);
}

//---------------------------- ReleaseBuffer --------------------------------

HRESULT CADXFrameManager::ReleaseBuffer(IMAGE_FRAME_PTR frm,BYTE *buffer)
{

	frm->surface->Unlock(NULL);	

    frm->pbuffer=NULL; //resetta il punttore al buffer 

	return S_OK;	
}

LONG CADXFrameManager::GetTotmem(void)
{
	return m_lTotmem;
}
//-------------------------------------------------------------------------

//crea la frame da un file BMP
HRESULT CADXFrameManager::CreateImgFrame(IMAGE_FRAME_PTR frm,char *filename) 
{

	HRESULT retval=S_OK;
	int intRetVal;   
	BITMAP_FILE bmpF;  //bitmap temporaneo
    
    if (frm != NULL)
	{
		//rilascia la frame se � stata caricata
		if (frm->status==1 && frm->surface) FreeImgFrame(frm);  
	}

    //resetta la frame
    frm->surface=NULL;
    frm->transp_color=0;
    frm->width=0;
    frm->height=0;
    frm->effect=0;
	frm->status=0;

    //**** inserire qui il codice per caricare da altri tipi di file **** 

    //carica il bitmap dal file
	intRetVal=LoadBitmap(&bmpF,filename);

    if (intRetVal != 1) retval=E_FAIL;
	else
	{   //crea la frame dal bitmap creato
		retval=CreateImgFrame(frm,&bmpF);
	  	FreeBitmap(&bmpF);
		
	}	
    
	return retval;
}

//--------------------------------- GrabFrame ------------------------------


//Estrae una porzione di immagine da un file bmp
HRESULT CADXFrameManager::GrabFrame(IMAGE_FRAME_PTR frmDest,RECT *rcS,BITMAP_FILE_PTR bitmap)
{
	IMAGE_FRAME imgBmp;  
	HRESULT hr;

	if (!bitmap) 
	{
		WriteErrorFile("CADXFrameManager::GrabFrame: il bitmap non � valido (NULL)");
		return E_FAIL;
	}	 

	 else if (bitmap->status != 1)
	{
		WriteErrorFile("CADXFrameManager::GrabFrame: il bitmap non � stato caricato correttamente. (status=0)");
		return E_FAIL;
	}
    //carica il bmp in memoria
	hr=CreateImgFrame(&imgBmp,bitmap);

	if (FAILED(hr)) {WriteErrorFile("GrabFrame : impossibile creare la frame a partire dal bitmap.");return E_FAIL;}

	//chiama l'altra funzione
	hr=GrabFrame(frmDest,rcS,&imgBmp);
    //rilascia il bitmap
	FreeImgFrame(&imgBmp);

	return hr;

}

//Estrae una frame da un'altra attraverso le coordinate del rettangolo che si vuol estrarre
HRESULT CADXFrameManager::GrabFrame(IMAGE_FRAME_PTR frmDest,LONG left,LONG top,LONG right,LONG bottom,IMAGE_FRAME_PTR frmSrc)
{
	RECT rc;

	rc.left=left;
	rc.top=top;
	rc.bottom=bottom;
	rc.right=right;

	return GrabFrame(frmDest,&rc,frmSrc);
}


//Estrae una frame da un'altra frame
//rcS � il rettangolo da estrarre 

HRESULT CADXFrameManager::GrabFrame(IMAGE_FRAME_PTR frmDest,RECT *rcS,IMAGE_FRAME_PTR frmSrc)
{
	DWORD dwWidth;             //larghezza area da copiare
	DWORD dwLines;             //altezza area da copiare
	DWORD width,height;        //dimensioni del bmp    
      
    HRESULT hr;
	RECT rcDest;
       
	if (frmDest->status != 0 || frmDest->surface) FreeImgFrame(frmDest);

	
	//procedura di grabbing
	//controllo del rettangolo
	if ((rcS->left>=rcS->right) || (rcS->top >= rcS->bottom))
	{
		WriteErrorFile("GrabFrame: il rettangolo passato non � valido: left=%d right=%d top=%d bottom=%d",rcS->left,rcS->right,rcS->top,rcS->bottom);
 		return E_FAIL;		
	}
    
    width=frmSrc->width;
	height=frmSrc->height;                
   
    //se il rettangolo � fuori bitmap lo ridimensiona
	rcS->left=(rcS->left<0) ? 0 : rcS->left;
	rcS->top=(rcS->top<0) ? 0 : rcS->top;
	rcS->right=(rcS->right<0) ? 0 : rcS->right;
	rcS->bottom=(rcS->bottom<0) ? 0 : rcS->bottom;
	rcS->right=(((unsigned)rcS->right>width) ? rcS->right=width : rcS->right);
	rcS->bottom=(((unsigned)rcS->bottom>height) ? rcS->bottom=height : rcS->bottom);
   

	dwLines=rcS->bottom-rcS->top;  //altezza dell'immagine estratta
	dwWidth=rcS->right-rcS->left;  //larghezza dell'immagine estratta

    //crea l'immagine
    hr=CreateFrameSurface(frmDest,dwWidth,dwLines);
    //rettangolo dest. per il blitting
    rcDest.top=0;
	rcDest.left=0;
	rcDest.right=dwWidth;
	rcDest.bottom=dwLines;

    if (FAILED(hr)) {WriteErrorFile("GrabFrame:errore creando la superficie di destinazione.");return E_FAIL;}        	

	//esegue il blitting dalla sup. sogente alla sup. destinazione
	//usando rcS come rettangolo per l'estrazione di una parte del bitmap
	frmDest->surface->Blt(&rcDest, frmSrc->surface,rcS,(DDBLT_WAIT | DDBLT_KEYSRC),NULL);       
	frmDest->status=1;  
	frmDest->transp_color=this->transp_color;
	return S_OK;

}

//------------------------- FreeImgFrame ------------------------------
//rilascia una frame
void CADXFrameManager::FreeImgFrame(IMAGE_FRAME_PTR frm)
{
	
	if (frm != NULL)
	{
		if (frm->status==1)
		{

	    //incrementa il contatore della memoria allocata
	    m_lTotmem -= frm->height*(frm->width+frm->width_fill);
		//rilascia il buffer se � stato creato    
		if ((frm->surface) && (frm->height !=0 )) frm->surface->Release();

		frm->surface=NULL;	
		frm->width=0;
		frm->height=0;
		frm->effect=0;
		frm->transp_color=0;
		frm->status=0;
		frm->pbuffer=NULL;
		
		} else

		{
	#ifdef _DEBUG_
			WriteErrorFile(g_szErrorFileName,"FreeImgFrame : impossibile rilasciare una frame non valida status=%d",frm->status);
	#endif
		}
	}
}

//--------------------------- PutImgFrame ---------------------------------------------
//Esegue il blitting dell'immagine frm sulla superficie video corrente 
HRESULT CADXFrameManager::PutImgFrame(LONG xout,LONG yout,IMAGE_FRAME_PTR frm)
{


	static RECT dest,*src;
	
	dest.left=xout;
	dest.top=yout;
	dest.right=xout+frm->width;
	dest.bottom=yout+frm->height;    

	src=&frm->rcClip;     

	//il rettangolo sorgente parte dalle coor. 0,0 della frame  
	//lpDDScurrent ha un clipper collegato
	return m_lpGm->lpDDSCurrent->Blt(&dest, frm->surface,src,(DDBLT_WAIT | DDBLT_KEYSRC),NULL);    

  
}

//--------------------------- PutImgFrame (ovr 1) -------------------------
//Questa � un'estensione della funzione precedente (blit sulla sup. video corrente)
//dwFalgs : flags passati a Blt 
//iBltEfcts : opzioni da usare con BltFast:
//DDBLTFAST_SRCCOLORKEY
//blit trasparente che non copia i pixel con colore uguale a srcColorKey
//DDBLTFAST_NOCOLORKEY 
//Copia tutti i pixel senza considerare la trasparenza  
//DDBLTFAST_DESTCOLORKEY 
//Copia solo sui pixel della sup. di destinazione con colore uguale a destColorKey 
//DDBLTFAST_WAIT 
//Restisuisce DDERR_WASSTILLDRAWING se il blitter is busy,ed esce dalla chiamata appena si libere il blitter

HRESULT CADXFrameManager::PutImgFrame(LONG xout,LONG yout,IMAGE_FRAME_PTR frm,int iBltEfcts)						
{

	static RECT dest,*src;
	
	dest.left=xout;
	dest.top=yout;
	dest.right=xout+frm->width;
	dest.bottom=yout+frm->height;    
    

	src=&frm->rcClip;
 
	//il rettangolo sorgente parte dalle coor. 0,0 della frame  
	//se non � definito USEFASTBLT questa funzione si comporta come le altre
	return m_lpGm->lpDDSCurrent->Blt(&dest, frm->surface,src,iBltEfcts,NULL);    


}

//-------------------------- PutImgFrame (ovr 2) --------------------------
//Esegue il blitting di una frame su un'altra frame
//esegue il clipping internamente
HRESULT CADXFrameManager::PutImgFrameClip(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,int iBltEffcts)
{
	RECT rcSrc;
	RECT rcDest;
	LONG lxl,lxr,lyt,lyb;

	//esegue il clipping
	if (xout>=0) lxl=0;
	else lxl=-xout;

	lxr=xout+frmSrc->width-frmDest->width;
	if (lxr<=0) lxr=0;

	if (yout>=0) lyt=0;
	else lyt=-yout;

	lyb=yout+frmSrc->height-frmDest->height;
	if (lyb<=0) lyb=0;

	//rettangolo sorgente
	rcSrc.left=lxl;
	rcSrc.right=frmSrc->width-lxr;
	rcSrc.top=lyt;
	rcSrc.bottom=frmSrc->height-lyb;

	//rettangolo destinazione
	rcDest.left=xout+lxl;
	rcDest.top=yout+lyt;
	rcDest.right=rcDest.left+rcSrc.right-rcSrc.left;
	rcDest.bottom=rcDest.top+rcSrc.bottom-rcSrc.top;      
    
	return frmDest->surface->Blt(&rcDest,frmSrc->surface,&rcSrc,iBltEffcts,NULL);
	
}

//---------------------------- PutImgFrameRect -----------------------------------------------------------------

//Esegue il blitting di una porzione rettangolare da una sup. sorgente a una destinazione
//NOTA: questa funzione non esegue il clipping a meno che non ci sia un clipper sulla sup. di destinazione
//Il rettangolo sorgente non puo' essere NULL

HRESULT CADXFrameManager::PutImgFrameRect(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,LPRECT prcSrc,int iBltEffcts)
{

	RECT rcDest;
	HRESULT hr;

	rcDest.left=xout;
	rcDest.top=yout;
	rcDest.bottom=yout+prcSrc->bottom-prcSrc->top;
	rcDest.right=xout+prcSrc->right-prcSrc->left;
	
	hr=frmDest->surface->IsLost();

	if (hr==DDERR_SURFACELOST) frmDest->surface->Restore();
	
	return frmDest->surface->Blt(&rcDest,frmSrc->surface,prcSrc,iBltEffcts,NULL);

}


//------------------------- PutImgFrameResize ----------------------------------------------------------------------------------------------------------
//Esegue il blitting ridimensionando l'immagine in altezza e larghezza
//se lNewWidth o lNewHeight sono minori di zero lascia la dimensione inalterata
//NON esegue il clipping

HRESULT CADXFrameManager::PutImgFrameResize(LONG xout,LONG yout,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,LONG lNewWidth,LONG lNewHeight,int iBltEffcts)
{
	LONG lw,lh;
	RECT rcSrc;
	RECT rcDest;

	if (lNewWidth<0) lw=frmSrc->width;
	else lw=lNewWidth;

	if (lNewHeight<0) lh=frmSrc->height;
	else lh=lNewHeight;

	rcSrc.left=0;
	rcSrc.top=0;
	rcSrc.right=frmSrc->width;
	rcSrc.bottom=frmSrc->height;

	rcDest.left=xout;
	rcDest.top=yout;
	rcDest.right=xout+lw;
	rcDest.bottom=yout+lh;

	return frmDest->surface->Blt(&rcDest,frmSrc->surface,&rcSrc,iBltEffcts,NULL);


}


//---------------------------- PutImgRotFrame ----------------------------------------------------------------------------------------------------------



//------------------------------------------------ Cls ---------------------------------
//Pulisce una superficie usando il colore specificato

void CADXFrameManager::Cls(IMAGE_FRAME_PTR pFrm,COLORREF color)
{
	m_lpGm->DD_Fill_Surface(pFrm->surface,color);
}

//---------------------------- DuplicateImgFrame----------------------------
//duplica una frame esistente
HRESULT CADXFrameManager::DuplicateImgFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc) 
{

	DDSURFACEDESC2 ddsdSrc;
    DDCOLORKEY color_key;
	HRESULT hr;	
	RECT rcDest;
    
	if (frmDest->status == 1 && frmDest->surface != NULL) FreeImgFrame(frmDest); 
	
	else if (frmSrc->surface==frmDest->surface)
	{
		WriteErrorFile("DuplicateImgFrame : le due frame sono coincidenti lpSrc=%lX lpDest=%lX",frmSrc->surface,frmDest->surface);
		return E_FAIL;
	}
    //copia i membri width,height ecc...    
        
    hr=CreateFrameSurface(frmDest,frmSrc->width+frmSrc->width_fill,frmSrc->height);

	if (FAILED(hr)) return E_FAIL;
	
	//crea il colore trasparente per la frame
	color_key.dwColorSpaceLowValue = frmSrc->transp_color;
	color_key.dwColorSpaceHighValue = frmSrc->transp_color;    
	//il colore chiave � uguale a quello della frame sorgente
	hr=frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key);    
    
    if (FAILED(hr)) 
	{
        frmDest->status=0;
        return E_FAIL;
	}

	//acquisisce la descrizione della sup. sorgente
	memset(&ddsdSrc,0,sizeof(ddsdSrc));
	ddsdSrc.dwSize=sizeof(ddsdSrc);
	hr=frmSrc->surface->GetSurfaceDesc(&ddsdSrc);
	rcDest.left=0;
	rcDest.top=0;
	rcDest.right=frmSrc->width-1;
	rcDest.bottom=frmSrc->height-1;
    //esegue il blitting dalla sup. sorgente a quella destinazione
	hr=frmDest->surface->Blt(&rcDest,frmSrc->surface,&rcDest,DDBLT_WAIT,NULL);
	  
	frmDest->status=1;
	frmDest->transp_color=frmSrc->transp_color;
	frmDest->width=frmSrc->width;
	frmDest->height=frmSrc->height;
    frmDest->rcClip=frmSrc->rcClip; //rettangolo di clipping

	//controlla se � andato a buon fine
	if (FAILED(hr)) 
	{
		WriteErrorFile("Errore durante il blitting sulla sup. destinazione");
        WriteErrorFile("Frame destinazione : width=%d height=%d status=%d",frmDest->width,frmDest->height,frmDest->status);
		WriteErrorFile("Frame sorgente : width=%d height=%d status=%d",frmSrc->width,frmSrc->height,frmSrc->status);
        return E_FAIL; 
	}

	return S_OK;   
}

//---------------------------------- CreateTopDownFrame ----------------------------------

//Crea una frame capovolta in senso verticale
HRESULT CADXFrameManager::CreateTopDownFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc)
{
	LONG lW=0,lH=0;
	DDCOLORKEY color_key; //color key (0 per default)
	LONG lImgSrcPitch; //pitch sup. sorgente
    LONG lImgDestPitch; //pitch sup. destinazione
	LONG srcoffs,destoffs;
	DWORD dwPixSize;
	BYTE *src_ptr=NULL;
	BYTE *dest_ptr=NULL;

	if (!frmDest) return E_FAIL;
	if (!frmSrc) return E_FAIL;

    if (frmDest->status == 1 && frmDest->surface) FreeImgFrame(frmDest);

	if (frmSrc->status != 1) return E_FAIL;

	//acquisisce le dimensioni dell'immagine
	lW=frmSrc->width;
	lH=frmSrc->height;

	if (FAILED(CreateFrameSurface(frmDest,lW,lH))) return E_FAIL;

	color_key.dwColorSpaceLowValue  = frmSrc->transp_color;
	color_key.dwColorSpaceHighValue = frmSrc->transp_color; 

	frmDest->transp_color=frmSrc->transp_color;

	//il colore chiave � uguale a quello della frame sorgente
	if (FAILED(frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key))) return E_FAIL;

	//acquisisce il puntatore alle superfici bloccandole
	if (!(src_ptr=GetBuffer(frmSrc,&lImgSrcPitch))) return E_FAIL;
	if (!(dest_ptr=GetBuffer(frmDest,&lImgDestPitch))) return E_FAIL;

	//dimensioni in byte di un pixel delle superfici (dipende dalla profondit� colore attuale)
	dwPixSize=m_bpp/8;

	for (LONG y=0;y<(LONG)frmDest->height;y++) 
	{
		for (LONG x=0;x<(LONG)frmDest->width;x++)
		{
			srcoffs=y*lImgSrcPitch+x*dwPixSize; //indirizzo del pixel della sup sorgente
			destoffs=(lH-y)*lImgDestPitch+x*dwPixSize; //pixel speculare nella superficie di destinazione
			//copia il pixel
			memcpy(dest_ptr+destoffs,src_ptr+srcoffs,dwPixSize);			
		}
	}

	//sblocca le superfici 
	ReleaseBuffer(frmDest);
	ReleaseBuffer(frmSrc);

	return S_OK;
}

//------------------------------ CreateMirrorFrame ---------------------------------------
//Crea una immagine speculare (rivoltata in senso orizzontale)
HRESULT CADXFrameManager::CreateMirrorFrame(IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc)
{

	LONG lW=0,lH=0;
	DDCOLORKEY color_key; //color key (0 per default)
	LONG lImgSrcPitch; //pitch sup. sorgente
    LONG lImgDestPitch; //pitch sup. destinazione
	LONG srcoffs,destoffs;
	DWORD dwPixSize;
	BYTE *src_ptr=NULL;
	BYTE *dest_ptr=NULL;

	if (!frmDest) return E_FAIL;
	if (!frmSrc) return E_FAIL;

    if (frmDest->status == 1 && frmDest->surface) FreeImgFrame(frmDest);

	if (frmSrc->status != 1) return E_FAIL;

	//acquisisce le dimensioni dell'immagine
	lW=frmSrc->width;
	lH=frmSrc->height;

	if (FAILED(CreateFrameSurface(frmDest,lW,lH))) return E_FAIL;

	color_key.dwColorSpaceLowValue  = frmSrc->transp_color;
	color_key.dwColorSpaceHighValue = frmSrc->transp_color; 

	frmDest->transp_color=frmSrc->transp_color;

	//il colore chiave � uguale a quello della frame sorgente
	if (FAILED(frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key))) return E_FAIL;

	//acquisisce il puntatore alle superfici bloccandole
	if (!(src_ptr=GetBuffer(frmSrc,&lImgSrcPitch))) return E_FAIL;
	if (!(dest_ptr=GetBuffer(frmDest,&lImgDestPitch))) return E_FAIL;

	//dimensioni in byte di un pixel delle superfici (dipende dalla profondit� colore attuale)
	dwPixSize=m_bpp/8;

	for (LONG y=0;y<(LONG)frmDest->height;y++) 
	{
		for (LONG x=0;x<(LONG)frmDest->width;x++)
		{
			srcoffs=y*lImgSrcPitch+x*dwPixSize; //indirizzo del pixel della sup sorgente
			destoffs=y*lImgDestPitch+(lW-1-x)*dwPixSize; //pixel speculare nella superficie di destinazione
			//copia il pixel
			memcpy(dest_ptr+destoffs,src_ptr+srcoffs,dwPixSize);			
		}
	}

	//sblocca le superfici 
	ReleaseBuffer(frmDest);
	ReleaseBuffer(frmSrc);

	return S_OK;

}

//----------------------------- CreateScaledFrame-----------------------------------------
//crea una frame scalata a partire da una frame sorgente
//ScaleFactor indica il fattore di moltiplicazione : 100 indica che l'immagine rimane cosi' com'�

HRESULT CADXFrameManager::CreateScaledFrame(int ScaleFactor,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc) {

LONG lW,lH;             //larghezza e altezza scalate
DDCOLORKEY color_key;
HRESULT hr;
RECT rcSrc,rcDest;
float fScaleFactor;

if (frmDest->status == 1 && frmDest->surface) FreeImgFrame(frmDest);

if (ScaleFactor<=0)
{
	WriteErrorFile("CreateScaledFrame : il fattore di scala non � valido ScaleFactor=%f",ScaleFactor);
	return E_FAIL;
}
else if (frmSrc->status != 1)
{
	WriteErrorFile("CreateScaledFrame : la frame sorgente non � valida status=%d",frmSrc->status);
	return E_FAIL;
}
else if (frmSrc->surface==frmDest->surface)
{
	WriteErrorFile("CreateScaledFrame : le due frame sono coincidenti lpSrc=%lX lpDest=%lX",frmSrc->surface,frmDest->surface);
	return E_FAIL;
}
else
{

	fScaleFactor=(float)ScaleFactor/100;

//disabilita il warning che indica la possibile perdita di dati
//nella conversione float -> LONG
#pragma warning( disable : 4244 )
	//calcola le nuove dimensioni
	lW=fScaleFactor*frmSrc->width;
	lH=fScaleFactor*frmSrc->height;

#pragma warning (default : 4244 )
   
	//crea la nuova superficie con le dimensioni scalate
	hr=CreateFrameSurface(frmDest,lW,lH);


	if (FAILED(hr)) 
	{
		WriteErrorFile("CreateScaledFrame : errore durante la creazione della superficie di dest.");		
		return (hr);
	}

	
	
    color_key.dwColorSpaceLowValue  = frmSrc->transp_color;
	color_key.dwColorSpaceHighValue = frmSrc->transp_color; 

	frmDest->transp_color=frmSrc->transp_color;

	//il colore chiave � uguale a quello della frame sorgente
	hr=frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key);    

    if (FAILED(hr))
	{
		WriteErrorFile("CreateScaledFrame : errore durante l'impostazione del colore chiave.");
		frmDest->status=0;
		return hr;
	}
   
    //rettangolo sorgente
    rcSrc.left=0;
	rcSrc.top=0;
	rcSrc.right=frmSrc->width;  //-1;
	rcSrc.bottom=frmSrc->height; //-1;
    //rettangolo destinazione		
    rcDest.top=0;
	rcDest.left=0;
	rcDest.right=frmDest->width; //-1;
	rcDest.bottom=frmDest->height; //-1;
    hr=frmDest->surface->Blt(&rcDest,frmSrc->surface,&rcSrc,DDBLT_WAIT,NULL);
    //controlla se � andato a buon fine
	if (FAILED(hr)) 
	{
#ifdef _DEBUG_
		WriteErrorFile("Errore durante il blitting sulla sup. destinazione");
        WriteErrorFile("Frame destinazione : width=%d height=%d status=%d",frmDest->width,frmDest->height,frmDest->status);
		WriteErrorFile("Frame sorgente : width=%d height=%d status=%d",frmSrc->width,frmSrc->height,frmSrc->status);
#endif
     return hr; 
	}
 }
	return S_OK;   
}

//----------------------------- CreateScaledFrame (ovr. 1) ----------------------------------------

//Crea una immagine ridimensionata a partire dalle nuove dimensioni

HRESULT CADXFrameManager::CreateScaledFrame(DWORD dwNewWidth,DWORD dwNewHeight,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc) {

LONG lW,lH;             //larghezza e altezza scalate
DDCOLORKEY color_key;
HRESULT hr;
RECT rcSrc,rcDest;

if (frmDest->status == 1 && frmDest->surface) FreeImgFrame(frmDest);

if (dwNewWidth<1 || dwNewHeight<1)
{
	WriteErrorFile("CreateScaledFrame : le nuove dimensioni non sono valide.");
	return E_FAIL;
}
else if (frmSrc->status != 1)
{
	WriteErrorFile("CreateScaledFrame : la frame sorgente non � valida status=%d",frmSrc->status);
	return E_FAIL;
}
else if (frmSrc->surface==frmDest->surface)
{
	WriteErrorFile("CreateScaledFrame : le due frame sono coincidenti lpSrc=%lX lpDest=%lX",frmSrc->surface,frmDest->surface);
	return E_FAIL;
}
else
{
	//Imposta le nuove dimensioni
	lW=dwNewWidth;
	lH=dwNewHeight;
   
	//crea la nuova superficie con le dimensioni scalate
	hr=CreateFrameSurface(frmDest,lW,lH);

	if (FAILED(hr)) 
	{
		WriteErrorFile("CreateScaledFrame : errore durante la creazione della superficie di dest.");		
		return (hr);
	}

    color_key.dwColorSpaceLowValue  = frmSrc->transp_color;
	color_key.dwColorSpaceHighValue = frmSrc->transp_color; 
	//il colore chiave � uguale a quello della frame sorgente
	hr=frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key);    

    if (FAILED(hr))
	{
		WriteErrorFile("CreateScaledFrame : errore durante l'impostazione del colore chiave.");
		frmDest->status=0;
		return hr;
	}
   
    //rettangolo sorgente
    rcSrc.left=0;
	rcSrc.top=0;
	rcSrc.right=frmSrc->width;  //-1;
	rcSrc.bottom=frmSrc->height; //-1;
    //rettangolo destinazione		
    rcDest.top=0;
	rcDest.left=0;
	rcDest.right=frmDest->width; //-1;
	rcDest.bottom=frmDest->height; //-1;
    hr=frmDest->surface->Blt(&rcDest,frmSrc->surface,&rcSrc,DDBLT_WAIT,NULL);
    //controlla se � andato a buon fine
	if (FAILED(hr)) 
	{
#ifdef _DEBUG_
		WriteErrorFile("Errore durante il blitting sulla sup. destinazione");
        WriteErrorFile("Frame destinazione : width=%d height=%d status=%d",frmDest->width,frmDest->height,frmDest->status);
		WriteErrorFile("Frame sorgente : width=%d height=%d status=%d",frmSrc->width,frmSrc->height,frmSrc->status);
#endif
     return hr; 
	}
 }
	return S_OK;   
}

//------------------------- crea una frame per rotazione di un'altra frame -------
//angle � espresso in gradi
//bFitSize=se impostato su TRUE ridimensiona la frame di destinazione in modo da farci entrare perfettamente la freme originale ruotata
HRESULT CADXFrameManager::CreateRotFrame(int angle,IMAGE_FRAME_PTR frmDest,IMAGE_FRAME_PTR frmSrc,BOOL bFitSize) 
{ 
	DDSURFACEDESC2 ddsd;
    DDSURFACEDESC2 ddsd1;
    DDCOLORKEY color_key;	
	BYTE *src_ptr;
	BYTE *dest_ptr;
    HRESULT hr;
	LONG x,y,X,Y;
	double SinAlpha;         //seno e coseno degli angoli di rotazione
	double CosAlpha;     
    LONG lHW;                //met� larghezza
	LONG lHH;                //met� altezza del rettangolo non ruotato
    LONG lMH,lMW;
	DWORD srcoffs,destoffs;    
    DWORD dwImgSrcPitch;
    DWORD dwImgDestPitch;
    DWORD dwPixSize;

	angle = angle % 360;   
   
	if (frmDest->status == 1 && frmDest->surface) FreeImgFrame(frmDest);
    
	if (frmSrc->status != 1)
    {
		WriteErrorFile("CreateRotFrame : la frame sorgente non � valida status=%d",frmSrc->status);
		return E_FAIL;
    }
	else if (frmSrc->surface==frmDest->surface)
	{
		WriteErrorFile("CreateRotFrame : le due frame sono coincidenti lpSrc=%lX lpDest=%lX",frmSrc->surface,frmDest->surface);
		return E_FAIL;
	}
    else
    {             

		lHW=frmSrc->width/2;
		lHH=frmSrc->height/2;
        
        SinAlpha=sin(DEG_TO_RADIANS*angle);
		CosAlpha=cos(DEG_TO_RADIANS*angle);	

        //casi particolari, viene forzata la definizione del seno e coseno
		//per evitare effetti di disturbo dovuti alla propagazione degli errori 
		if (angle==0) {SinAlpha=0;CosAlpha=1;}
		else if (angle==90) {SinAlpha=1;CosAlpha=0;}
		else if (angle==180) {SinAlpha=0;CosAlpha=-1;}
        else if (angle==270) {SinAlpha=-1;CosAlpha=0;}
		else if (angle==360) {SinAlpha=0;CosAlpha=1;}

		if (bFitSize)
		{
			//larghezza e altezza della superficie dopo la rotazione
			lMW=(LONG)((double)frmSrc->width*ABS(CosAlpha)+(double)frmSrc->height*ABS(SinAlpha));
			lMH=(LONG)((double)frmSrc->height*ABS(CosAlpha)+(double)frmSrc->width*ABS(SinAlpha));
		}
		else
		{
			lMW=frmSrc->width;
			lMH=frmSrc->height;
		}
        
       
        DD_INIT_STRUCT(ddsd1);
		//acquisisce la descrizione della superficie sorgente
		hr=frmSrc->surface->GetSurfaceDesc(&ddsd1);
        if (FAILED(hr))
		{
			WriteErrorFile("CreateRotFrame,GetSurfaceDesc");
			return hr;   	        
		}

        hr=CreateFrameSurface(frmDest,lMW,lMH);        			
	
	    if (FAILED(hr))
		{
			WriteErrorFile("CreateRotFrame : CreateFrameSurface errore creando la superficie.");
            return hr;		
        }
		DD_INIT_STRUCT(ddsd);
		hr=frmDest->surface->GetSurfaceDesc(&ddsd);        
	
		if (FAILED(hr))
		{
			WriteErrorFile("CreateRotFrame : errore durante l'acquisizione della descrizione della superficie.");
            return hr;		
        }

        color_key.dwColorSpaceLowValue  = frmSrc->transp_color;
	    color_key.dwColorSpaceHighValue = frmSrc->transp_color; 
	    //il colore chiave � uguale a quello della frame sorgente
	    hr=frmDest->surface->SetColorKey(DDCKEY_SRCBLT,&color_key);    
        if (FAILED(hr))
	    {
			WriteErrorFile("CreateRotFrame : errore durante l'impostazione del colore chiave");				
			frmDest->status=0;
			return hr;
	    }       	    
		
        //esegue la rotazione pixel per pixel
		//blocca le superfici per accedere direttamente al buffer dati
        hr=frmDest->surface->Lock(NULL,&ddsd,DDLOCK_WAIT | DDLOCK_SURFACEMEMORYPTR,NULL);
		if (FAILED(hr)) 
		{
			WriteErrorFile("CreateRotFrame,Locking frmDest"); 
		    frmDest->status=0;
			return hr;
		}        

		DD_INIT_STRUCT(ddsd1);
		//acquisisce la descrizione della superficie sorgente
		hr=frmSrc->surface->GetSurfaceDesc(&ddsd1);
        if (FAILED(hr))
		{
			WriteErrorFile("CreateRotFrame,GetSurfaceDesc");
			return hr;   	        
		}
        
		hr=frmSrc->surface->Lock(NULL,&ddsd1,DDLOCK_WAIT | DDLOCK_SURFACEMEMORYPTR,NULL);
        if (FAILED(hr))
		{
			WriteErrorFile("CreateRotFrame,Locking frmSrc"); 
			frmDest->status=0;return hr;
		}              
	
        lMW /= 2;
		lMH /= 2;        
        
		//i puntatori sorgente e destinazione
		//puntano all'inizio del buffer delle superfici
        src_ptr=(BYTE *)ddsd1.lpSurface;
        dest_ptr=(BYTE *)ddsd.lpSurface;  
        
        dwImgDestPitch=GetSurfacePitch(frmDest->surface); 
        dwImgSrcPitch=GetSurfacePitch(frmSrc->surface);
        dwPixSize=m_bpp/8; 

        //la funzione di trasformazione ha come dominio frmDest
		//e come codominio frmSrc , se si fa alla rovescia si ottiene un effetto tearing
		for (y=0;y<(LONG)frmDest->height;y++) 
		{
			for (x=0;x<(LONG)frmDest->width;x++)
			{
									
				//esegue una rototraslazione di ogni punto
				X=(LONG)((double)(x-lMW)*CosAlpha+(double)(y-lMH)*SinAlpha);
				Y=-(LONG)((double)(x-lMW)*SinAlpha-(double)(y-lMH)*CosAlpha);                     

				if (( X > -lHW) && (X < lHW) && (Y > -lHH) && (Y < lHH)) 
				{	
					//trasla l'origine nell'angolo in alto a sx
					X += lHW;
					Y += lHH;          
                    
					srcoffs=(DWORD)Y*dwImgSrcPitch+(DWORD)X*dwPixSize; //indirizzo del pixel della sup sorgente
					destoffs=(DWORD)y*dwImgDestPitch+(DWORD)x*dwPixSize; // .. sup. dest.
					//copia il pixel dal buffer sorgente al buffer ruotato (destinazione)
					memcpy((BYTE *)dest_ptr+destoffs,(BYTE *)src_ptr+srcoffs,dwPixSize);				
				}

			}

		}	

   
		frmDest->surface->Unlock(NULL);
        frmSrc->surface->Unlock(NULL);

        frmDest->status=1;


		return S_OK;
	}
}

// algoritmo di compressione per una riga di bytes secondo la codifica RLE 8 bit
// dest=buffer destinazione
// src=buffer sorgente
// i buffers devono essere allocati prima di chiamare la funzione
// riepilogo compressione RLE 8 bit
//- Il primo byte indica quanti bytes che hanno valore pari al secondo byte, ci sono nella riga
//  (encoded mode) es. 04 05 07 1E  indica 4 bytes di valore 05 e 7 bytes di valore 1E
//- sequenze di escape : 00 indica una sequenza di escape
//  se 00 � seguito da: 00=fine riga 01=significa fine bitmap,02 significa che i due bytes successivi indicano
//  lo spostamento in senso orizzontale e verticale , se 00 � seguito da un valore
//  maggiore o uguale di 03h significa che i bytes seguenti devono essere presi cos� come sono
//  esempio: 00 04 07 89 1D 56 indica la sequenza di 4 bytes con valori 07 89 1D 56
//  se la sequenza � dipari si deve aggiungere 00 per ottenere un allineamento a word
//  esempio:  linea copressa : 04 56 03 1E 00 04 77 78 79 00 03 00 00 00
//  linea espansa : 56 56 56 56 1E 1E 1E 77 78 79 00 00 00 <fine riga>     
//  restitutisce le dimensioni del buffer compresso (compreso escape di fine riga)

DWORD CADXFrameManager::EncodeRle8(BYTE *dest,BYTE *src,DWORD dwSize)
{
	BYTE *ptrSrc=NULL;
	BYTE *ptrDest=NULL;
	BYTE *ptrDest1=NULL;
	BYTE *ptrSrc1=NULL;
    DWORD dwCount;
	LONG dwCount1;
	BOOL bLoop;
    BYTE cVal;
    DWORD dwRleBytes=0;   

	if (dest==NULL || src==NULL || dwSize==0) return 0;
	    

    ptrSrc=src;
	ptrDest=dest;
	dwCount=0;
	cVal=0;
    //esegue un ciclo su tutta la lunghezza della linea di bytes da comprimere
	while (ptrSrc<src+dwSize)
	{			
		dwCount1=dwCount;
        bLoop=TRUE;
		
		ptrSrc1=ptrSrc;
        ptrDest1=ptrDest;
		*ptrDest=0;
		ptrDest1++;
		*ptrDest1=0;
	
		while ((*ptrSrc1 != *(ptrSrc1+1)) && (ptrSrc1<src+dwSize))
		{   //absolute mode
				
			ptrDest1++;
            *ptrDest1=*ptrSrc1; //copia il byte dalla sorgente alla destinazione
			ptrSrc1++;	
			ptrDest[1]++;  //incrementa il numero dei bytes 	
			
		}
		
		//se la sequenza di escape contiene meno di 4 bytes allora usa l'encoded mode
		if (ptrDest1-ptrDest<4) {ptrDest1=ptrDest+1;ptrSrc1=ptrSrc;}

		if (((ptrSrc1 - ptrSrc) % 2) == 1) {ptrDest[1]++;*(++ptrDest1)=0;}
        
		if (ptrSrc1==ptrSrc) 
		{
			ptrSrc1=ptrSrc;
			ptrDest1=ptrDest;
            
			*ptrDest1=0;
			*(ptrDest1+1)=*ptrSrc;
            
            ptrDest[0]++;
            
			while ((*ptrSrc1 == *(ptrSrc1+1)) && ptrSrc1<src+dwSize && ptrDest[0]<0xFE)
			{ 
				//encoded mode (al massimo codifica 255 bytes uguali)
				ptrDest[0]++;
				ptrSrc1++;				
			}

		    ptrDest+=2;
			ptrSrc=ptrSrc1+1;
		}

		else 
		{
			//absolute mode : aggiorna i puntatori			   
			ptrDest=ptrDest1+1;      
			ptrSrc=ptrSrc1;

		}
	}

	*ptrDest=0;	
	ptrDest[1]=0;  //fine riga
	
    dwRleBytes=ptrDest-dest+2;  //aggiunge anche i due caratteri di fine riga

    ptrDest=NULL;ptrSrc=NULL;ptrDest1=NULL;ptrSrc=NULL;    

	return dwRleBytes;
}

//-------------------------- DecodeRle8 ---------------------------------------
//algoritmo di decompressione di una linea compressa con codifica Rle 8 bit
//il buffer di destinazione deve essere allocato prima di passarlo alla funzione.

HRESULT CADXFrameManager::DecodeRle8(BYTE *dest,BYTE *src,DWORD dwSize)
{

	BYTE *ptrSrc;
	BYTE *ptrDest;
	BYTE cVal=0x0;
    DWORD dwCount=0;
	DWORD dwNum=0;
       
	if (src==NULL) return E_FAIL;  	
 
/*	memset(dest,0x3D,dwSize);                        //azzera l'array
	
    return S_OK;  */

    ptrDest=dest;  //imposta i puntatori al buffer sorgente e destinazione
	ptrSrc=src;

	while (ptrDest<dest+dwSize)
	{
		if (*ptrSrc == 0) 
		{   //absolute mode
			ptrSrc++;
			//� una sequenza di escape
			cVal=*(ptrSrc++);
			
			if (cVal ==1) break; //fine bitmap
			else if (cVal == 0) {} //fine riga
		    else if (cVal == 2) { /* spostamento relativo : non implementato */}
		    else
			{			
				//sequenza di bytes in absolute mode
				dwNum=(WORD) cVal;

				for (dwCount=0;dwCount<dwNum;dwCount++)
				{
					if (!(*ptrSrc==0 && dwCount==dwNum-1))  //salta il carattere 00 usato per allineamento a word
					{
						*(ptrDest++)=*ptrSrc++;
					}
					else ptrSrc++;
					
				}
			}

		}

		else

		{	//modalit� encoded
			dwNum=(DWORD) *(ptrSrc++);  //numero di bytes da ripetere
			cVal=*(ptrSrc++);           //byte da ripetere

			for (dwCount=0;dwCount<dwNum;dwCount++)
			{	
				*(ptrDest++)=cVal;	
				if (ptrDest>=dest+dwSize) break;
			}
		}

	}
    

	return S_OK;
}
//------------------------------------ EncodeRle24 --------------------------------------
//Comprime un buffer immagine a 24 bit usando un algoritmo RLE simile a quello a 8 bit
//restituisce le dimensioni in byte del buffer compresso
//src e dest devono essere gi� allocati prima di chiamare la funzione
//dest : punta al buffer immagine di destinazione (buffer compresso)
//src : punta al buffer immagine sorgente (buffer espanso)
//dwImgPitch : larghezza in bytes di ogni linea del buffer espanso
//dwHeight : numero di righe nell'immagine

DWORD CADXFrameManager::EncodeRle24(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight)
{
	const DWORD dwPixSize=3;
	BYTE *ptrSrc=NULL;
	BYTE *ptrDest=NULL;
	BYTE *ptrDestOld=NULL;
	BYTE *ptrSrcOld=NULL;  
	COLORREF pix,pix1;
    DWORD dwY;
	DWORD dwBuffSize;
    DWORD dwCount;
	BYTE *pLimit;
	DWORD dwRes;

	//dimensione in bytes del buffer
    dwBuffSize=dwImgPitch*dwHeight;
    
    ptrDest=dest;

	for (dwY=0;dwY<dwHeight;dwY++)
	{		

		//punta alla linea corrente
		ptrSrc=src+dwY*dwImgPitch;

        pLimit=ptrSrc+dwImgPitch;
               
        while (ptrSrc<pLimit)
		{
		    ptrDestOld=ptrDest;
     		ptrSrcOld=ptrSrc;           
			
            *ptrDest=0x0;
            ptrDest +=2;

			memcpy((BYTE *)&pix,ptrSrc,dwPixSize);
			memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);
			ptrSrc += dwPixSize;
            ptrDest += dwPixSize;

			dwCount=1;              

			memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
            				
			while((pix1 != pix) && ptrSrc<pLimit && dwCount<0xFE)
			{//absolute mode

					if (dwCount==255) break;    

					dwCount++;                                               
                    
					memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);

					ptrDest += dwPixSize;               
					ptrSrc += dwPixSize;
					
					if (ptrSrc<pLimit-dwPixSize)
					{					
						memcpy((BYTE *)&pix,ptrSrc,dwPixSize);				
					}
				
			 }//fine while

			 if (dwCount<4) {ptrSrc=ptrSrcOld;ptrDest=ptrDestOld;}
             else {ptrDestOld[1] = (BYTE) dwCount;}

			 if (ptrSrc==ptrSrcOld)
			 {   //encoded mode
				 *ptrDest=1;
				 ptrDest++;
				 memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);

				 memcpy((BYTE *)&pix,ptrSrc,dwPixSize);				 
				 ptrSrc += dwPixSize;
				 ptrDest += dwPixSize;

				 memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
				// ptrSrc += dwPixSize;

				 while((pix1==pix) && ptrSrc<pLimit && ptrDestOld[0]<0xFE)
				 {
					 ptrDestOld[0]++;
					 ptrSrc += dwPixSize;
					 memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
					 
				 }

			 }
			 

		}//fine while

		 //fine riga
	
		 *ptrDest=0x0;
		 ptrDest++;
		 *ptrDest=0x0;	
		 ptrDest++;


	}//fine for
    
	//fine immagine
    *ptrDest=0x0;
	ptrDest++;
	*ptrDest=0x1;
    
    dwRes=(DWORD)(ptrDest - dest);

	return dwRes;

}

//------------------------------- DecodeRle24 -----------------------------------------------------------------------------
//Decodifica un buffer precedentemente compresso con EncodeRle24
//dest=puntatore al buffer sorgente decompresso
//src=buffer compresso
//dwImgPitch = larghezza in bytes di una linea
HRESULT CADXFrameManager::DecodeRle24(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight)
{
	DWORD dwLine=0;
    BYTE *ptrSrc,*ptrDest;
	BYTE bVal,bVal1;
	BOOL bCycle;
    DWORD dwRLineBytes;
	DWORD dwCount;

	if (src == NULL) return E_FAIL;
	
    ptrSrc=src;
	ptrDest=dest;

	while (dwLine < dwHeight)
	{
		
		bCycle=TRUE;
        dwRLineBytes=0;

		do
		{
			bVal=*ptrSrc;
            //absolute mode
			if (bVal == 0)			
			{
				
				ptrSrc++;
				
				bVal1=*ptrSrc;               

				if (bVal1 == 1) 
				
				//fine bitmap
				{					  				 
					if (dwLine<dwHeight-1) {WriteErrorFile("DecodeRle24 : � stata incontrato il tag di fine bitmap prima di aver riempito tutta l'area.");return E_FAIL;}
					break;
				}
                else if (bVal1 == 0)
				//fine riga
				{
					dwLine++; //incrementa il numero di righe lette
					if (dwRLineBytes != dwImgPitch) {WriteErrorFile("DecodeRle24 : errore durante la lettura della riga %lu, sono stati letti %lu bytes su %lu.",dwLine,dwRLineBytes,dwImgPitch);return E_FAIL;}
					ptrSrc++;
					break;
				}
				else if (bVal1==2) {/*spostamento in coordinato dx e dy non implementato*/}
                else
				{//bVal in questo caso indica i bytes della sequenza di escape
                    
                    ptrSrc++;

					for (dwCount=0;dwCount<(DWORD)bVal1;dwCount++)
					{
						//ogni pixel � costituito da 3 byte
						*ptrDest++ = *ptrSrc++;
				        *ptrDest++=*ptrSrc++;
                        *ptrDest++=*ptrSrc++;

						dwRLineBytes += 3; //incrementa il numero di bytes letti per ogni riga
					}		

                }
			}
			else
			{//encoded mode

				ptrSrc++;

				//copia tanti pixel quanti indicato in bVal
				for (dwCount=0;dwCount<(DWORD)bVal;dwCount++)
				{
					
					*ptrDest++ = *(ptrSrc);
					*ptrDest++ = *(ptrSrc+1);
					*ptrDest++ = *(ptrSrc+2);	
					
					dwRLineBytes += 3;
				}

                //si sposta su successivo byte da leggere
				ptrSrc += 3;

			}		

		} while (TRUE);

	}

	return S_OK;

}

//------------------------------------ EncodeRle16 --------------------------------------
//Comprime un buffer immagine a 16 bit usando un algoritmo RLE simile a quello a 8 bit
//restituisce le dimensioni in byte del buffer compresso
//src e dest devono essere gi� allocati prima di chiamare la funzione
//dest : punta al buffer immagine di destinazione (buffer compresso)
//src : punta al buffer immagine sorgente (buffer espanso)
//dwImgPitch : larghezza in bytes di ogni linea del buffer espanso
//dwHeight : numero di righe nell'immagine

DWORD CADXFrameManager::EncodeRle16(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight)
{
	const DWORD dwPixSize=2;
	BYTE *ptrSrc=NULL;
	BYTE *ptrDest=NULL;
	BYTE *ptrDestOld=NULL;
	BYTE *ptrSrcOld=NULL;  
	COLORREF pix,pix1;
    DWORD dwY;
	DWORD dwBuffSize;
    DWORD dwCount;
	BYTE *pLimit;
	DWORD dwRes;

	//dimensione in bytes del buffer
    dwBuffSize=dwImgPitch*dwHeight;
    
    ptrDest=dest;

	for (dwY=0;dwY<dwHeight;dwY++)
	{		

		//punta alla linea corrente
		ptrSrc=src+dwY*dwImgPitch;

        pLimit=ptrSrc+dwImgPitch;
               
        while (ptrSrc<pLimit)
		{
		    ptrDestOld=ptrDest;
     		ptrSrcOld=ptrSrc;           
			
            *ptrDest=0x0;
            ptrDest +=2;

			memcpy((BYTE *)&pix,ptrSrc,dwPixSize);
			memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);
			ptrSrc += dwPixSize;
            ptrDest += dwPixSize;

			dwCount=1;              

			memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
            				
			while((pix1 != pix) && ptrSrc<pLimit && dwCount<0xFE)
			{//absolute mode

					if (dwCount==255) break;    

					dwCount++;                                               
                    
					memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);

					ptrDest += dwPixSize;               
					ptrSrc += dwPixSize;
					
					if (ptrSrc<pLimit-dwPixSize)
					{
						pix1=pix;
						memcpy((BYTE *)&pix,ptrSrc,dwPixSize);					
					}
				
			 }//fine while

			 if (dwCount<4) {ptrSrc=ptrSrcOld;ptrDest=ptrDestOld;}
             else {ptrDestOld[1] = (BYTE) dwCount;}

			 if (ptrSrc==ptrSrcOld)
			 {   //encoded mode
				 *ptrDest=1;
				 ptrDest++;
				 memcpy((BYTE *)ptrDest,ptrSrc,dwPixSize);

				 memcpy((BYTE *)&pix,ptrSrc,dwPixSize);				 
				 ptrSrc += dwPixSize;
				 ptrDest += dwPixSize;

				 memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
				// ptrSrc += dwPixSize;

				 while((pix1==pix) && ptrSrc<pLimit && ptrDestOld[0]<0xFE)
				 {
					 ptrDestOld[0]++;
					 ptrSrc += dwPixSize;
					 memcpy((BYTE *)&pix1,ptrSrc,dwPixSize);
					 
				 }

			 }
			 

		}//fine while

		 //fine riga
	
		 *ptrDest=0x0;
		 ptrDest++;
		 *ptrDest=0x0;	
		 ptrDest++;


	}//fine for
    
	//fine immagine
    *ptrDest=0x0;
	ptrDest++;
	*ptrDest=0x1;
    
    dwRes=(DWORD)(ptrDest - dest);

	return dwRes;

}

//------------------------------- DecodeRle16 -----------------------------------------------------------------------------
//Decodifica un buffer precedentemente compresso con EncodeRle16
//dest=puntatore al buffer sorgente decompresso
//src=buffer compresso
//dwImgPitch = larghezza in bytes di una linea
HRESULT CADXFrameManager::DecodeRle16(BYTE *dest,BYTE *src,DWORD dwImgPitch,DWORD dwHeight)
{
	DWORD dwLine=0;
    BYTE *ptrSrc,*ptrDest;
	BYTE bVal,bVal1;
	BOOL bCycle;
    DWORD dwRLineBytes;
	DWORD dwCount;
    const DWORD dwPixSize=2;

	if (src == NULL) return E_FAIL;
	
    ptrSrc=src;
	ptrDest=dest;

	while (dwLine < dwHeight)
	{
		
		bCycle=TRUE;
        dwRLineBytes=0;

		do
		{
			bVal=*ptrSrc;
            //absolute mode
			if (bVal == 0)			
			{
				
				ptrSrc++;
				
				bVal1=*ptrSrc;               

				if (bVal1 == 1) 
				
				//fine bitmap
				{					  				 
					if (dwLine<dwHeight-1) {WriteErrorFile("DecodeRle16 : � stata incontrato il tag di fine bitmap prima di aver riempito tutta l'area.");return E_FAIL;}
					break;
				}
                else if (bVal1 == 0)
				//fine riga
				{
					dwLine++; //incrementa il numero di righe lette
					if (dwRLineBytes != dwImgPitch) {WriteErrorFile("DecodeRle16 : errore durante la lettura della riga %lu, sono stati letti %lu bytes su %lu.",dwLine,dwRLineBytes,dwImgPitch);return E_FAIL;}
					ptrSrc++;
					break;
				}
				else if (bVal1==2) {/*spostamento in coordinato dx e dy non implementato*/}
                else
				{//bVal in questo caso indica i bytes della sequenza di escape
                    
                    ptrSrc++;

					for (dwCount=0;dwCount<(DWORD)bVal1;dwCount++)
					{
						//ogni pixel � costituito da 2 byte
						*ptrDest++=*ptrSrc++;
				        *ptrDest++=*ptrSrc++;                       

						dwRLineBytes += dwPixSize; //incrementa il numero di bytes letti per ogni riga
					}		

                }
			}
			else
			{//encoded mode

				ptrSrc++;

				//copia tanti pixel quanti indicato in bVal
				for (dwCount=0;dwCount<(DWORD)bVal;dwCount++)
				{
					
					*ptrDest++ = *(ptrSrc);
					*ptrDest++ = *(ptrSrc+1);				
					
					dwRLineBytes += dwPixSize;
				}

                //si sposta su successivo byte da leggere
				ptrSrc += dwPixSize;

			}		

		} while (TRUE);

	}

	return S_OK;

}
//----------------- CreateFileVPX -----------------------------------------------------------------------------------------
//crea un file VPX : se il file esiste gi� lo sovrascrive
//lpszFileName=nome del file
//bpp=profondit� colore 8,16,24
//wCompression 0=non compresso 1=compresso
//lpszAuthor=autore (puo' essere anche NULL)
//pal=in caso di immagini 8 bit punta alla palette da usare nel file

HRESULT CADXFrameManager::CreateFileVPX(LPCSTR lpszFileName,WORD bpp,LPCSTR lpszAuthor,PALETTEENTRY *pal)
{
	HANDLE hFile;
	VPX_HEADER vpxHeader;
	RGBQUAD rgbPal;
	DWORD dwBytes,dwCount;
    DWORD dwSize1,dwSize2;
    
	if (bpp==8 && pal==NULL) {WriteErrorFile("CreateFileVPX:occorre specificare una palette se il file contiene immagini a 8 bit");return E_FAIL;}  //nessuna palette   
	
	hFile=CreateFile(lpszFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);          
	//controllo handle del file
	if (hFile==INVALID_HANDLE_VALUE) {WriteErrorFile("CreateFileVPX:Impossibile creare il file %s ",lpszFileName);return E_FAIL;}
    //controllo bits per pixel
	if (!(bpp==8 || bpp==24 || bpp==16)) {WriteErrorFile("CreateFileVPX:la profondit� colore %d bit non � consentita.",bpp);return E_FAIL;}
	
	memset(vpxHeader.szAuthor,0,20L);
	if (lpszAuthor != NULL) memcpy(vpxHeader.szAuthor,lpszAuthor,20);
    //id di controllo del file
    vpxHeader.id=VPX_ID;
	//compressione 8 bit = 1 24 bit = 2
	vpxHeader.dwCompression = 1; //il file si considera sempre compresso 	
	//bit per pixel
	vpxHeader.dwBitsPerPixel=bpp;
	
	
	//offset dell'intestazione della prima immagine dall'inizio del file    
	dwSize1=sizeof(RGBQUAD);
	dwSize2=sizeof(VPX_HEADER);
	if (bpp==8) 
	{
		vpxHeader.dwOffset=256*dwSize1+dwSize2;
		//scrive nel file l'header del file
	    WriteFile(hFile,&vpxHeader,sizeof(VPX_HEADER),&dwBytes,NULL);
		//salva la palette
		for (dwCount=0;dwCount<256;dwCount++)
		{
			 rgbPal.rgbBlue=pal[dwCount].peBlue;
			 rgbPal.rgbGreen=pal[dwCount].peGreen;
			 rgbPal.rgbRed=pal[dwCount].peRed;
			 rgbPal.rgbReserved=0;
			 //salva l'elemento della palette
			 WriteFile(hFile,&rgbPal,sizeof(RGBQUAD),&dwBytes,NULL);
		}
			
	}
		
	else
	{
		vpxHeader.dwOffset=dwSize2;
		//scrive nel file l'header del file
	    WriteFile(hFile,&vpxHeader,sizeof(VPX_HEADER),&dwBytes,NULL);
	} 
 

	CloseHandle(hFile);

	return S_OK;
}

//------------------------------- SaveToFileVPX ------------------------------------------------------------------------------------------
//Salva un'immagine in un file di immagini VPX esistente
//se il file VPX non esiste restituisce un errore
//vengono salvati anche i bytes di riempimento (width_fill)
//lpszFileName = nome del file VPX, se il file non esiste o non � valido esce.
//pwCompression = se � 1 l'immagine � compressa altrimenti � espansa
//lpszAuthor = autore del file se viene creato 
//N.B. l'immagine deve avere la stessa profondit� colore impostata nell'header del file 

HRESULT CADXFrameManager::SaveToFileVPX(LPCSTR lpszFileName,IMAGE_FRAME_PTR pfSrc)
{
	HANDLE hFile;   //handler del file
    WIN32_FIND_DATA fnd;    
	VPX_IMAGE_HEADER vpxImageHeader;
    DWORD dwCompression=0;
	DWORD dwCount=0;
	DWORD dwBytes=0;
	DWORD dwLineBytes=0;
	DWORD dwImgWidth=0;
	DWORD dwLineRleBytes=0;
	DWORD dwFilePos=0;
	DWORD dwFilePosh=0;
	DWORD dwImgBitCount;
	DDPIXELFORMAT ddPixfmt;            //formato pixel della superficie
	BYTE *pLine=NULL;
	BYTE *pSrc=NULL;                    //buffer sorgente	
	DDSURFACEDESC2 ddsd;  
	VPX_HEADER vpxH;
	BOOL b555=FALSE;
    
	if (lpszFileName == NULL) return E_FAIL;       //parametri non validi    
	
	hFile=FindFirstFile(lpszFileName,&fnd);

	//il file VPX non esiste, prima di salvare occorre crearlo
	if (hFile == INVALID_HANDLE_VALUE) return E_FAIL;

    //se la frame non contiene immagini
	if (pfSrc->status != 1 || pfSrc->surface==NULL) return E_FAIL; //immagine non valida
       
    DD_INIT_STRUCT(ddsd);
    //acquisisce il formato del pixel della superficie da salvare nel file
	//per controllare se � compatibile con il pixel format del file
	//il file e la frame devono avere la stessa profondit� colore
	DD_INIT_STRUCT(ddPixfmt);	

	if (FAILED(pfSrc->surface->GetPixelFormat(&ddPixfmt))) {WriteErrorFile("SaveToFileVPX : errore acquisendo il formato del pixel della frame.");return E_FAIL;}
	//il membro dwRGBCount contiene la profondit� colore della supercie
	dwImgBitCount=ddPixfmt.dwRGBBitCount;
	//acquisisce l'header del file per vedere se l'immagine puo' essere salvata nel file
	if (FAILED(GetFileHeader(&vpxH,lpszFileName))) {WriteErrorFile("Errore di acquisizione del file header del file %s",lpszFileName);return E_FAIL;}
    //controlla la copatibilit�
	if (dwImgBitCount != vpxH.dwBitsPerPixel) {WriteErrorFile("SaveToFileVPX: il formato del pixel dell'immagine e del file non sono compatibili : bpp immagine=%d bpp file=%d",dwImgBitCount,vpxH.dwBitsPerPixel); return E_FAIL;}
	
    dwCompression=vpxH.dwCompression;

	//blocca la superficie DD della frame
	if (FAILED(pfSrc->surface->Lock(NULL,&ddsd,DDLOCK_WAIT | DDLOCK_SURFACEMEMORYPTR,NULL))) return E_FAIL;
    
	hFile=CreateFile(lpszFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);          
	if (hFile==INVALID_HANDLE_VALUE) return E_FAIL;
	dwFilePos=SetFilePointer(hFile,0,(LONG *)&dwFilePosh,FILE_END);
	
    //crea l'header dell'immagine
	if (FAILED(CreateImageHeader(pfSrc,&vpxImageHeader))) return E_FAIL;    
	//scrive sul file l'intestazione dell'immagine
	WriteFile(hFile,&vpxImageHeader,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL);
    
	//----------------salva nel file l'immagine compressa -------------------------       
    //punta la superfcie dell'immagine
    pSrc=(BYTE *)ddsd.lpSurface;
	
	if (dwImgBitCount == 8) //compressione immagini a 8 bit
	{
		//alloca l'array di dati compressi con un margine di sicurezza
        pLine = new BYTE[vpxImageHeader.dwWidth*2];        

		dwLineBytes=(DWORD)ddsd.lPitch;   //pitch della superficie
		dwImgWidth=pfSrc->width;   //larghezza in pixel
		
		for (dwCount=0;dwCount<pfSrc->height;dwCount++)
		{   
			//crea una linea compressa
			dwLineRleBytes=EncodeRle8(pLine,pSrc+dwCount*dwLineBytes,dwLineBytes);
			//incrementa le dimensioni nell'header
			vpxImageHeader.dwSize += dwLineRleBytes;
			if (!WriteFile(hFile,pLine,dwLineRleBytes,&dwBytes,NULL)) {WriteErrorFile("SaveToFileVPX : errore durante la scrittura sul file del buffer compresso dell'immagine a 8 bit");CloseHandle(hFile);return E_FAIL;}			

		}

        //si porta all'inizio dell'intestazione immagine
		SetFilePointer(hFile,dwFilePos,(LONG *)&dwFilePosh,FILE_BEGIN);
        if (!WriteFile(hFile,&vpxImageHeader,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL)) {WriteErrorFile("SaveToFileVPX : errore durante la scrittura dell'header del file");CloseHandle(hFile);return E_FAIL;}

		SetFilePointer(hFile,-2,NULL,FILE_END);
    
		pLine[0]=0x0;         //escape di fine immagine
		pLine[1]=0x1;         //scrive l'escape di fine immagine        
		WriteFile(hFile,pLine,2L,&dwBytes,NULL);	

	}

	else if (dwImgBitCount == 24 || dwImgBitCount == 16)  //comprime immagini a 24 bit
	//RLE 24 bit
	{
		dwLineBytes=ddsd.dwLinearSize;   //pitch della superficie
        
        dwBytes=dwLineBytes*pfSrc->height;

        pLine=new BYTE[dwBytes];        
		
		if (pLine == NULL) {WriteErrorFile("SaveToFileVPX : errore durante l'allocazione di memoria di %lu bytes per il buffer temporaneo dell'immagine a 24 bit",dwBytes);CloseHandle(hFile);return E_FAIL;}
        
		if (dwImgBitCount == 24) vpxImageHeader.dwSize=EncodeRle24(pLine,pSrc,dwLineBytes,pfSrc->height);
        else 
		{
			//inmodalit� a 16 bit deve convertire il buffer in formato 565
			//il buffer salvato � sempre in formato 565	
			
			if (m_lpGm->g_rgb16.dwGBits == 5 && m_lpGm->g_rgb16.dwRBits == 5 && m_lpGm->g_rgb16.dwBBits == 5)
			{
				//la modalit� grafica corrente � 555 quindi deve convertire il buff. in 565
				ConvBuffer565(pSrc,dwBytes);
					b555=TRUE; 
			}

			vpxImageHeader.dwSize=EncodeRle16(pLine,pSrc,dwLineBytes,pfSrc->height);

			//se siamo in modalit� 555 deve riconvertire il buffer
			if (b555)
			{
				ConvBuffer555(pSrc,dwBytes);
			}
		}

        if (!WriteFile(hFile,pLine,vpxImageHeader.dwSize,&dwBytes,NULL)) {WriteErrorFile("SaveToFileVPX : errore durante la scrittura sul file del buffer compresso dell'immagine a 24 bit");CloseHandle(hFile);return E_FAIL;}

		SetFilePointer(hFile,dwFilePos,(LONG *)&dwFilePosh,FILE_BEGIN);
        
		if (!WriteFile(hFile,&vpxImageHeader,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL)) {WriteErrorFile("SaveToFileVPX : errore durante la scrittura dell'header del file");CloseHandle(hFile);return E_FAIL;}

	}
	else
	{

		WriteErrorFile("SaveToFileVPX: la profondit� (%d bit) colore dell'immagine non � supportata",dwImgBitCount);
        //sblocca la superficie della frame

        pfSrc->surface->Unlock(NULL);

		CloseHandle(hFile);
		return E_FAIL;
    
	}

    pfSrc->surface->Unlock(NULL);

    SAFE_DELETE_ARRAY(pLine);
    CloseHandle(hFile);

return S_OK;

}
//---------------------------- LoadVPXFile ------------------------
//Carica un file VPX e riempie pVPXFile
HRESULT CADXFrameManager::LoadVPXFile(VPX_FILE_PTR pVPXFile,LPCSTR lpszFileName)
{
	VPX_HEADER vpxH;
    HANDLE hFile;
	DWORD dwBitFileCount;
	DWORD dwBytes,dwCount;
	DWORD dwBytesSkip;
    RGBQUAD rgbSrc[256];
	VPX_IMAGE_HEADER vpxImg;
	DWORD dwLen;
	HRESULT hr;

    //se il file VPX � gi� carico lo rilascia
    if (pVPXFile->status==1) 
	{
		hr=FreeVPXFile(pVPXFile);
		if (FAILED(hr)) {WriteErrorFile("LoadVPXFile: impossibile rilasciare il file VPX %s passato alla funzione.",lpszFileName);return hr;}

	}

    pVPXFile->status=0;  //inizializza 
    pVPXFile->dwNumImages=0;

	if (!IsValidVPX(lpszFileName)) {WriteErrorFile("LoadVPXFile : il file %s non � valido.",lpszFileName);return E_FAIL;}
    
    
    //apre il file
    hFile=CreateFile(lpszFileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);    

	if (hFile == INVALID_HANDLE_VALUE) {WriteErrorFile("LoadVPXFile : impossibile aprire il file %s in lettura.",lpszFileName);return E_FAIL;}
    //legge l'intestazione
    ReadFile(hFile,(BYTE *)&vpxH,sizeof(VPX_HEADER),&dwBytes,NULL);
    //copia l'intestazione nella struttura
    memcpy((BYTE *)&pVPXFile->vpxfileheader,(BYTE *)&vpxH,sizeof(VPX_HEADER)); 
    //acquisisce i bits per pixel dal file
	dwBitFileCount=vpxH.dwBitsPerPixel;
	
    if (dwBitFileCount==8) 
	{
		//carica la palette se � un file a 8 bit
		ReadFile(hFile,&rgbSrc,sizeof(RGBQUAD)*256L,&dwBytes,NULL);
		//converte da quadruble RGB in paletteentry
        for (dwCount=0;dwCount<256;dwCount++)
		{
			pVPXFile->palette[dwCount].peRed=rgbSrc[dwCount].rgbRed;
			pVPXFile->palette[dwCount].peGreen=rgbSrc[dwCount].rgbGreen;
			pVPXFile->palette[dwCount].peBlue=rgbSrc[dwCount].rgbBlue;
		}
	}
    
    ReadFile(hFile,&vpxImg,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL);
	
    //conta le immagini presenti
    while (dwBytes !=0 ) //legge fino alla fine del file
	{
		pVPXFile->dwNumImages++;
        dwBytesSkip=vpxImg.dwSize;
		SetFilePointer(hFile,dwBytesSkip,NULL,FILE_CURRENT);
        ReadFile(hFile,&vpxImg,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL);

	}  
		
    //setta il nome file
    dwLen=lstrlen(lpszFileName);
	pVPXFile->lpszFileName = new CHAR[dwLen+1];
	memset(pVPXFile->lpszFileName,0,sizeof(CHAR)*(dwLen+1));
    lstrcpy(pVPXFile->lpszFileName,lpszFileName); //setta il nome del file

    CloseHandle(hFile);
    pVPXFile->status=1; //file caricato
	
	return S_OK;

}

//-------------------------------- FreeVPXFile-------------------------------------------------

//rilascia un file VPX caricato
HRESULT CADXFrameManager::FreeVPXFile(VPX_FILE_PTR pVPXFile)
{
	if (pVPXFile->status != 1) return E_FAIL;

    //SAFE_DELETE(pVPXFile->lpszFileName);
    pVPXFile->status=0;
	pVPXFile->dwNumImages=0;

	return S_OK;

}

//--------------------------- CreateImgFrameFromFile (ovr. 1)-----------------------------------
//Crea un'immagine a partire da una struttura VPXFile gi� caricata
//dwFrame � l'immagine da caricare , 0 indica la prima immagine

HRESULT CADXFrameManager::CreateImgFrameFromVPX(IMAGE_FRAME_PTR pfDest,VPX_FILE_PTR pVPXFile,DWORD dwFrame)
{
	
	HANDLE hFile;
    VPX_IMAGE_HEADER vpxImg;  
    DWORD dwOffs;
    BOOL bLoop=FALSE;
	DWORD dwCount=0;
    DWORD dwBytes;
	DWORD dwBitCount;
    BYTE *pImgBuffer=NULL;
	BYTE *pOutBuffer=NULL;
	BYTE *pDestBuffer=NULL;    
    DWORD dwWidth,dwHeight; //larghezza e altezza immagine in pixel
    HRESULT hr;
    DWORD dwCompression;
    DWORD dwLineBytes,dwExpandedSize;
    DWORD dwSuPitch;

	if (pVPXFile->status == 0)
	{
		WriteErrorFile("CreateImgFrameFile : Il file VPX non � caricato");
		return E_FAIL;

	}

	if (dwFrame > pVPXFile->dwNumImages-1)
	{
		WriteErrorFile("CreateImgFromFile : la frame %lu non � presente nel file VPX.",dwFrame);
        return E_FAIL;

	}
    //se la frame � gi� riempita la sovrascrive
	if (pfDest->status != 0 || pfDest->surface != NULL) FreeImgFrame(pfDest);

	//apre il file
    hFile=CreateFile(pVPXFile->lpszFileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL); 
    
	if (hFile == INVALID_HANDLE_VALUE) {WriteErrorFile("CreateImgFromFile : impossibile aprire il file %s in lettura.",pVPXFile->lpszFileName);return E_FAIL;}
    
	//acquisisce l'offset dell'intestazione della prima immagine
    dwOffs=pVPXFile->vpxfileheader.dwOffset;
    //si sposta sull'header della prima immagine
    SetFilePointer(hFile,dwOffs,NULL,FILE_BEGIN);

    do
	{
        bLoop=ReadFile(hFile,&vpxImg,sizeof(VPX_IMAGE_HEADER),&dwBytes,NULL);
		bLoop=(bLoop & ((DWORD)dwCount < dwFrame));
		
		if (bLoop) 
		{ 
			dwOffs=vpxImg.dwSize;
			//va avanti saltando l'immagine corrente
            SetFilePointer(hFile,dwOffs,NULL,FILE_CURRENT);
		    dwCount++;
		}
		

	} while (bLoop);
    
    if (dwFrame != (DWORD)dwCount) 
		//non � risucito a leggere fino alla frame specificata
	{
		CloseHandle(hFile);
		WriteErrorFile("CrateImgFrameFromFile:Impossibile leggere la frame %lu nel file VPX %s ",dwFrame,pVPXFile->lpszFileName);
		return E_FAIL;
	}
    //buffer che contiene l'immagine compressa
	pImgBuffer = new BYTE[vpxImg.dwSize];
	
	if (pImgBuffer == NULL) {WriteErrorFile("CreateImgFrameFromFile:impossibile allocare %lu bytes per il buffer immagine della frame n� %lu nel file VPX %s",vpxImg.dwSize,dwFrame,pVPXFile->lpszFileName);return E_FAIL;}
    
	//legge il buffer immagine compresso
	ReadFile(hFile,(BYTE *)pImgBuffer,vpxImg.dwSize,&dwBytes,NULL);
	
    if (dwBytes != vpxImg.dwSize) {WriteErrorFile("CreateImgFrameFromFile:impossibile leggere %lu bytes dal buffer della frame n�%d del file VPX %s",vpxImg.dwSize,dwFrame,pVPXFile->lpszFileName);return E_FAIL;}

	//bpp del file
	dwBitCount=pVPXFile->vpxfileheader.dwBitsPerPixel;
    //acquisisce le dimensioni dell'immagine in pixel
    dwWidth=vpxImg.dwWidth;
	dwHeight=vpxImg.dwHeight;
        
    //calcola la larghezza di riempimento
	pfDest->width_fill = ((dwWidth % 4 != 0) ? (4 - dwWidth% 8) : 0);
    
	hr=CreateFrameSurface(pfDest,dwWidth,dwHeight);
    
    if (FAILED(hr)) {CloseHandle(hFile);WriteErrorFile("CreateImgFrameFromVPX:errore durante la creazione dalla superficie della frame");return hr;}
    //blocca la superficie per ottenere il buffer

    pDestBuffer=m_lpGm->LockSurface(pfDest->surface,NULL);
    
    if (pDestBuffer == NULL) {WriteErrorFile("CreateImgFrameFromVPX:impossibile bloccare la superficie della frame");return E_FAIL;}
	
    dwCompression=pVPXFile->vpxfileheader.dwCompression;
    //bytes per ogni linea dell'immagine
	dwLineBytes=dwWidth*(dwBitCount/8);
	//dimensioni dell buffer immagine dopo l'espansione
    //senza tener conto della bpp dello schemro attuale
    dwExpandedSize=dwHeight*dwLineBytes;
    //alloca il buffer che conterr� l'immagine espansa ma non convertita con il bpp corrente
    pOutBuffer=new BYTE[dwExpandedSize];
    
    if (pOutBuffer == NULL) {CloseHandle(hFile);WriteErrorFile("CreateImgFrameFromVPX:impossibile allocare %lu bytes per il buffer provvisorio.",dwExpandedSize);return E_FAIL;}
    	
	if (dwBitCount == 8)
	{//immagine compressa a 8 bit
         //decodifica il bitmap 
		 DecodeRle8(pOutBuffer,pImgBuffer,dwExpandedSize);
         
         SAFE_DELETE_ARRAY(pImgBuffer);
 
		 pImgBuffer = new BYTE[dwExpandedSize];                        
	
		 memcpy((BYTE *)pImgBuffer,(BYTE *)pOutBuffer,dwExpandedSize);
    }
    else if (dwBitCount == 16)
	{
		 DecodeRle16(pOutBuffer,pImgBuffer,dwLineBytes,dwHeight);

         SAFE_DELETE_ARRAY(pImgBuffer); 

		 pImgBuffer= new BYTE[dwExpandedSize];

		 memcpy((BYTE *)pImgBuffer,(BYTE *)pOutBuffer,dwExpandedSize);	

		 //nel file VPX il buffer � salvato in modalit� 565
		 //quindi se la modalit� corrente � 555 deve convertirlo
		 if (m_lpGm->g_rgb16.dwRBits == 5 && m_lpGm->g_rgb16.dwGBits == 5 && m_lpGm->g_rgb16.dwBBits == 5)
		 {
			 ConvBuffer555(pImgBuffer,dwExpandedSize);
		 }
	}
	else if (dwBitCount == 24)
	{//immagine compressa 24 bit
		
		 DecodeRle24(pOutBuffer,pImgBuffer,dwLineBytes,dwHeight);

         SAFE_DELETE_ARRAY(pImgBuffer); 

		 pImgBuffer= new BYTE[dwExpandedSize];

		 memcpy((BYTE *)pImgBuffer,(BYTE *)pOutBuffer,dwExpandedSize);
	}
	else if (dwBitCount == 32)
	{
		//mod. 32 bit non implementata
	}
	else
	{//immagine espansa

          /* non ancora implementato */
	}
	
    dwSuPitch=GetSurfacePitch(pfDest->surface);

	//a questo punto converte il buffer in base alla modalit� grafica corrente
    hr=CopyPixels(pDestBuffer,pImgBuffer,dwSuPitch,dwLineBytes,dwWidth,dwHeight,m_bpp,dwBitCount,pVPXFile->palette);
    
    m_lpGm->UnlockSurface(pfDest->surface,pDestBuffer);

    //rilascia il buffer
    SAFE_DELETE_ARRAY(pImgBuffer);
    SAFE_DELETE_ARRAY(pOutBuffer);
	
	CloseHandle(hFile);

    pfDest->status=1; //frame caricata!

	return S_OK;

}

//------------------------CreateImgFrameFromVPX  --------------------------------------

//Crea un'immagine a partire da un file VPX
HRESULT CADXFrameManager::CreateImgFrameFromVPX(IMAGE_FRAME_PTR pfDest,LPCSTR lpszFileName,DWORD dwFrame)
{
	HRESULT hr;
	VPX_FILE vpxFile;
    
	//carica il file contenente le immagini
    hr=LoadVPXFile(&vpxFile,lpszFileName);

	if (FAILED(hr)) {WriteErrorFile("CreateImgFrameFromVPX : errore durante il caricamento del file %s",lpszFileName);return hr;}
    
	hr=CreateImgFrameFromVPX(pfDest,&vpxFile,dwFrame);

    FreeVPXFile(&vpxFile);
	
	return hr;
}

//------------------------ IsValidVPX --------------------------------
//restituisce TRUE se il file VPX � valido e non corrotto
BOOL CADXFrameManager::IsValidVPX(LPCSTR lpszFileName)
{
	HANDLE hFile;   //handler del file    
	VPX_HEADER vpxHeader;
    char buff[2];
	char buff2[2];
	DWORD dwBytes;
    

    //apre il file in lettura
	hFile=CreateFile(lpszFileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL); 
   
	if (hFile == INVALID_HANDLE_VALUE) return FALSE;

	ReadFile(hFile,buff,2L,&dwBytes,NULL);

    lstrcpyn(buff2,buff,3); //copia i primi due caratteri nella stringa di confronto

    //controlla l'id VX
	if (lstrcmp((char *)buff2,"VX")==0) 
	{ 
		//legge l'header
		SetFilePointer(hFile,0,NULL,FILE_BEGIN);
        ReadFile(hFile,&vpxHeader,sizeof(VPX_HEADER),&dwBytes,NULL);
        CloseHandle(hFile);
		return TRUE;
	}
	else return FALSE;
}

//----------------------- CreateImageHeader ------------------------------------

HRESULT CADXFrameManager::CreateImageHeader(IMAGE_FRAME_PTR frm,VPX_IMAGE_HEADER_PTR vpxHead)
{
	if ((frm->status != 1) || vpxHead == NULL) return E_FAIL; //param. non validi
	
	vpxHead->dwWidth=frm->width;
	vpxHead->dwHeight=frm->height;
	vpxHead->dwExpandedSize=vpxHead->dwHeight*vpxHead->dwWidth;
	vpxHead->dwReserved=0;
	vpxHead->dwSize=0;     //dimensioni del buffer compresso: vengono impostate dopo la compressione

	return S_OK;

}
//----------------------------- GetFileHeader -----------------------------------
//Recupera l'header da un file VPX, se il file non esiste o non � valido esce
HRESULT CADXFrameManager::GetFileHeader(VPX_HEADER *pvpxHead,LPCSTR lpszFileName)
{
	HANDLE hFile;
	DWORD dwBytes;

	if (IsValidVPX(lpszFileName))
	{
		//apre il file in sola lettura
		hFile=CreateFile(lpszFileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);

		if (hFile == INVALID_HANDLE_VALUE) {
			                                 WriteErrorFile("GetFileHeader : impossibile aprire in lettura il file %s ",lpszFileName);
											 return E_FAIL;
		                                   }
        //legge l'intestazione
		ReadFile(hFile,pvpxHead,sizeof(VPX_HEADER),&dwBytes,NULL);
        
        CloseHandle(hFile);

	}
	else
	{
		WriteErrorFile("GetFileHeader : il file %s non � valido.",lpszFileName);
		return E_FAIL;
	}

	return S_OK;

}

//-------------------------- CaptureScreen --------------------------------
//Dimensiona la frame pimgDest e la riempie con il contenuto dello schermo corrente
HRESULT CADXFrameManager::CaptureScreen(IMAGE_FRAME_PTR pimgDest)
{
	HRESULT hr;
	if (pimgDest)
	{	
		if (m_lpGm->g_bFullScreen)
		{
			//crea un'immagine vuota
			hr=CreateFrameSurface(pimgDest,m_dwScreenWidth,m_dwScreenHeight);		

			if (!FAILED(hr))
			{
				hr=pimgDest->surface->BltFast(0,0,m_lpGm->lpDDSCurrent,NULL,0);

				return hr;

			}
			else return hr;
		}

		else
		{
			//controllare il seguente codice
			//windowed
			POINT p;
			RECT rcDest,rcSrc,rcDest1;

			p.x=0;
			p.y=0;
			::ClientToScreen(m_lpGm->g_hwnd,&p);
			::GetClientRect(m_lpGm->g_hwnd,&rcSrc);
			::OffsetRect(&rcSrc,p.x,p.y);
			::IntersectRect(&rcDest1,&m_lpGm->m_rcScreen,&rcSrc);

            //crea la superficie
			hr=CreateFrameSurface(pimgDest,rcSrc.right-rcSrc.left,rcSrc.bottom-rcSrc.top);
			
			if (FAILED(hr))
			{
				WriteErrorFile(TEXT("CaptureScreen: errore durante la creazione della frame di destinazione in modalit� windowed err=%d"),hr);
				return hr;
			}

			rcDest.left=0;
			rcDest.top=0;
			rcDest.bottom=pimgDest->height;
			rcDest.right=pimgDest->width;

			hr=pimgDest->surface->Blt(&rcDest,m_lpGm->lpDDSCurrent,&rcDest1,0,NULL);

			if (FAILED(hr))
			{
				WriteErrorFile(TEXT("CaptureScreen: errore durante il blitting in modalit� windowed err=%d %s"),hr,GetErrorDescription(hr));
				return hr;
			}

			return hr;
		}
	}

    else return E_FAIL;

}


//----------------------------------- GrabFrameArray ------------------------------------------------

//Carica nel vettore di frames pArray un striscia orizzontale di dwFrames frames
//a partire dal punto px,py e con dimensioni di w,h
HRESULT CADXFrameManager::GrabFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h)
{
	DWORD dwCount;
	RECT rc;
	HRESULT hr;

	if (pimgSrc && pArray)
	{
	
		for (dwCount=0;dwCount<dwFrames;dwCount++)
		{
			rc.left=px+(dwCount*w);
			rc.top=py;
			rc.bottom=rc.top+h;
			rc.right=rc.left+w;

			hr=GrabFrame(&pArray[dwCount],&rc,pimgSrc);

			if (FAILED(hr)) return hr;
			
		}
	}

	else return E_FAIL;
	return S_OK;

}


//--------------------------------------- GrabVFrameArray ----------------------------------------------

//Carica un vettore di frame disposte in senso vertcale
//i parametri hanno lo stesso significato della funzione precedente
HRESULT CADXFrameManager::GrabVFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h)
{
	DWORD dwCount;
	RECT rc;
	HRESULT hr;

	if (pimgSrc && pArray)
	{
	
		for (dwCount=0;dwCount<dwFrames;dwCount++)
		{
			rc.left=px;
			rc.top=py+(dwCount*h);
			rc.bottom=rc.top+h;
			rc.right=rc.left+w;

			hr=GrabFrame(&pArray[dwCount],&rc,pimgSrc);

			if (FAILED(hr)) return hr;
			
		}
	}

	else return E_FAIL;
	return S_OK;

}

//-------------------- FreeFrameArray ------------------------------------------------

void CADXFrameManager::FreeFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames)
{
	for (DWORD dwCount=0;dwCount<dwFrames;dwCount++) 
	{
		FreeImgFrame(&pArray[dwCount]);
	}
}

//------------------------------- GetDefaultCaps ---------------------------------
DWORD CADXFrameManager::GetDefaultCaps()
{
	return m_dwDefCaps;
}

//------------------------------ SetDefaultCaps --------------------------------
//imposta le propriet� delle superfici create con CreateFrameSurface

void CADXFrameManager::SetDefaultCaps(DWORD dwCaps)
{
	m_dwDefCaps=dwCaps;
}
	
//-------------------------------- IsWindowed -----------------------------------

BOOL CADXGraphicManager::IsWindowed(void)
{
   return !g_bFullScreen;
}


