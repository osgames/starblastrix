/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32DEBUG.CPP
  funzioni di debug directdraw

  ***************************************************************/

#ifndef _A32DEBUG_
#define _A32DEBUG_

#define WIN32_LEAN_AND_MEAN 

#include <windows.h>          //contiene definizioni base per windows
#include <windowsx.h>         //contiene la definizione di alcune macro importanti in windows
#include <winbase.h> 
#include <wingdi.h>         
#include <mmsystem.h>
//#include <malloc.h>
#include <stdio.h>
#include <ddraw.h>
#include <math.h>

//usato per il debug dello heap 
#define _CRTDBG_

#define MAX_ERRDBG 180 //numero di caratteri per messaggio di errore

///////////////////////////////// CADXDebug ////////////////////////////////
//classe usata per eseguire il debug scrivendo le informazioni su un file
class CADXDebug
{
private:

	char m_szErrorFileName[80];
	HRESULT CreateErrorFile(BOOL bOverwrite); 
	int m_status;
	static int iInstanceCounter; //contatore istanze 
	TCHAR m_szLastError[MAX_ERRDBG];

public:
    
	CADXDebug(void);
	~CADXDebug(void);
	TCHAR *GetErrorDescription(HRESULT hr);
	//imposta il nome del file di output 
	HRESULT SetErrorFile(char *szFileName,BOOL bOverwrite);   
	//scrive sul file di output una stringa formattata (� simile a printf)
    void CDECL WriteErrorFile(char *szFormat, ...);  
	TCHAR *GetLastError(void); //restituisce l'ultimo errore che si � verificato
    //imposta l'errore
	void CDECL SetLastError(char *szFormat,...);
    
};


#endif
