/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32DEBUG.CPP
  funzioni di debug directdraw

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/


#define WIN32_LEAN_AND_MEAN //win api pura!!!


#include "a32debug.h"
#include <crtdbg.h>

#ifdef _CRTDBG_
#define new new(_NORMAL_BLOCK,__FILE__,__LINE__)
#endif

/*---------------------- CADXDebug -------------------------------
 Classe usata per inviare messaggi di debug su file
*/
//costruttore
CADXDebug::CADXDebug(void)
{	

#ifdef _CRTDBG_

	int tmp=_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	_CrtCheckMemory();

#endif

	iInstanceCounter++;
	m_status=0;
	memset(m_szErrorFileName,0,80);
	memset(m_szLastError,0,sizeof(TCHAR)*MAX_ERRDBG);

#ifdef _CRTDBG_

	_CrtCheckMemory();

#endif

	
}

//distruttore
CADXDebug::~CADXDebug(void)
{
	iInstanceCounter--;
	m_status=0;
}
//------------------------ SetErrorFile -------------------------------
//costruttore : imposta il file creandolo se non esiste
HRESULT CADXDebug::SetErrorFile(char *szFileName,BOOL bOverwrite)   	
{
	HRESULT hr=E_FAIL;
     
	if (szFileName)
	{
		memcpy((char *)m_szErrorFileName,(char *)szFileName,lstrlen(szFileName));
        //crea il file 
		hr=CreateErrorFile(bOverwrite);	

        if (!FAILED(hr)) m_status=1;  //file creato ed utilizzabile

	}

	return hr;
}

//------------------ CreateErrorFile ---------------------------------

HRESULT CADXDebug::CreateErrorFile(BOOL bOverwrite)
{
	HANDLE hFile;   
	int iMode;
    char crlf[2];
    
	crlf[0]=13;
	crlf[1]=10;
    
    if (bOverwrite) iMode=CREATE_ALWAYS;   //crea sovrascrivendo anche se esiste gi�
    else iMode=OPEN_EXISTING;              //il file deve essere esistente

    hFile=CreateFile(m_szErrorFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,iMode,FILE_FLAG_SEQUENTIAL_SCAN,NULL);    

    if ((hFile==INVALID_HANDLE_VALUE) && !bOverwrite)
	{		
		//il file non esiste e quindi forza la creazione
		hFile=CreateFile(m_szErrorFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);    		
	}

	//a questo punto tutto deve essere ok altrimenti esce
	if (hFile==INVALID_HANDLE_VALUE) return E_FAIL;   
    
	CloseHandle(hFile);

	return S_OK;
}

//------------------------ WriteErrorFile ----------------------------
//scrive sul file di output una stringa con formattazione tipo printf

void CDECL CADXDebug::WriteErrorFile(char *szFormat, ...)
{
	char szBuffer[1024];	
    va_list pArgList;	
    DWORD dwBytesWr;
	HANDLE hFile;
    char crlf[2];
    
	crlf[0]=13;   //cr+lf
	crlf[1]=10;
    
    if (m_status == 1)
	{
		hFile=CreateFile(m_szErrorFileName,GENERIC_WRITE,FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
    
        if (hFile != INVALID_HANDLE_VALUE) 
		{		
	 
			//va_start � una macro che consente di puntare pArgList all'inizio dell'elenco dei parametri opzionali
 			va_start (pArgList,szFormat);
			//la funzione _vsnprintf � basata sulla lunghezza del buffer che viene passato e non sullo zero finale come wsprintf ecc...
 			_vsnprintf(szBuffer,sizeof(szBuffer) / sizeof(char),szFormat,pArgList);
     
			va_end(pArgList);

			SetFilePointer(hFile,0,NULL,FILE_END);
		
			WriteFile(hFile,szBuffer,lstrlen(szBuffer)*sizeof(char),&dwBytesWr,NULL);
		
			WriteFile(hFile,crlf,sizeof(crlf),&dwBytesWr,NULL);
    
		}   
		CloseHandle(hFile);
	}
}

//--------------------- DDErrorString -----------------------------------------

//Traduce un codice di errore in una stringa

TCHAR *CADXDebug::GetErrorDescription(HRESULT hr)
{
	switch (hr)
	{
	case DDERR_ALREADYINITIALIZED:           return TEXT("DDERR_ALREADYINITIALIZED");
	case DDERR_CANNOTATTACHSURFACE:          return TEXT("DDERR_CANNOTATTACHSURFACE");
	case DDERR_CANNOTDETACHSURFACE:          return TEXT("DDERR_CANNOTDETACHSURFACE");
	case DDERR_CURRENTLYNOTAVAIL:            return TEXT("DDERR_CURRENTLYNOTAVAIL");
	case DDERR_EXCEPTION:                    return TEXT("DDERR_EXCEPTION");
	case DDERR_GENERIC:                      return TEXT("DDERR_GENERIC");
	case DDERR_HEIGHTALIGN:                  return TEXT("DDERR_HEIGHTALIGN");
	case DDERR_INCOMPATIBLEPRIMARY:          return TEXT("DDERR_INCOMPATIBLEPRIMARY");
	case DDERR_INVALIDCAPS:                  return TEXT("DDERR_INVALIDCAPS");
	case DDERR_INVALIDCLIPLIST:              return TEXT("DDERR_INVALIDCLIPLIST");
	case DDERR_INVALIDMODE:                  return TEXT("DDERR_INVALIDMODE");
	case DDERR_INVALIDOBJECT:                return TEXT("DDERR_INVALIDOBJECT");
	case DDERR_INVALIDPARAMS:                return TEXT("DDERR_INVALIDPARAMS");
	case DDERR_INVALIDPIXELFORMAT:           return TEXT("DDERR_INVALIDPIXELFORMAT");
	case DDERR_INVALIDRECT:                  return TEXT("DDERR_INVALIDRECT");
	case DDERR_LOCKEDSURFACES:               return TEXT("DDERR_LOCKEDSURFACES");
	case DDERR_NO3D:                         return TEXT("DDERR_NO3D");
	case DDERR_NOALPHAHW:                    return TEXT("DDERR_NOALPHAHW");
	case DDERR_NOCLIPLIST:                   return TEXT("DDERR_NOCLIPLIST");
	case DDERR_NOCOLORCONVHW:                return TEXT("DDERR_NOCOLORCONVHW");
	case DDERR_NOCOOPERATIVELEVELSET:        return TEXT("DDERR_NOCOOPERATIVELEVELSET");
	case DDERR_NOCOLORKEY:                   return TEXT("DDERR_NOCOLORKEY");
	case DDERR_NOCOLORKEYHW:                 return TEXT("DDERR_NOCOLORKEYHW");
	case DDERR_NODIRECTDRAWSUPPORT:          return TEXT("DDERR_NODIRECTDRAWSUPPORT");
	case DDERR_NOEXCLUSIVEMODE:              return TEXT("DDERR_NOEXCLUSIVEMODE");
	case DDERR_NOFLIPHW:                     return TEXT("DDERR_NOFLIPHW");
	case DDERR_NOGDI:                        return TEXT("DDERR_NOGDI");
	case DDERR_NOMIRRORHW:                   return TEXT("DDERR_NOMIRRORHW");
	case DDERR_NOTFOUND:                     return TEXT("DDERR_NOTFOUND");
	case DDERR_NOOVERLAYHW:                  return TEXT("DDERR_NOOVERLAYHW");
	case DDERR_NORASTEROPHW:                 return TEXT("DDERR_NORASTEROPHW");
	case DDERR_NOROTATIONHW:                 return TEXT("DDERR_NOROTATIONHW");
	case DDERR_NOSTRETCHHW:                  return TEXT("DDERR_NOSTRETCHHW");
	case DDERR_NOT4BITCOLOR:                 return TEXT("DDERR_NOT4BITCOLOR");
	case DDERR_NOT4BITCOLORINDEX:            return TEXT("DDERR_NOT4BITCOLORI)NDEX";
	case DDERR_NOT8BITCOLOR:                 return TEXT("DDERR_NOT8BITCOLOR");
	case DDERR_NOTEXTUREHW:                  return TEXT("DDERR_NOTEXTUREHW");
	case DDERR_NOVSYNCHW:                    return TEXT("DDERR_NOVSYNCHW");
	case DDERR_NOZBUFFERHW:                  return TEXT("DDERR_NOZBUFFERHW");
	case DDERR_NOZOVERLAYHW:                 return TEXT("DDERR_NOZOVERLAYHW");
	case DDERR_OUTOFCAPS:                    return TEXT("DDERR_OUTOFCAPS");
	case DDERR_OUTOFMEMORY:                  return TEXT("DDERR_OUTOFMEMORY");
	case DDERR_OUTOFVIDEOMEMORY:             return TEXT("DDERR_OUTOFVIDEOMEMORY");
	case DDERR_OVERLAYCANTCLIP:              return TEXT("DDERR_OVERLAYCANTCLIP");
	case DDERR_OVERLAYCOLORKEYONLYONEACTIVE: return TEXT("DDERR_OVERLAYCOLORKEYONLYONEACTIVE");
	case DDERR_PALETTEBUSY:                  return TEXT("DDERR_PALETTEBUSY");
	case DDERR_COLORKEYNOTSET:               return TEXT("DDERR_COLORKEYNOTSET");
	case DDERR_SURFACEALREADYATTACHED:       return TEXT("DDERR_SURFACEALREADYATTACHED");
	case DDERR_SURFACEALREADYDEPENDENT:      return TEXT("DDERR_SURFACEALREADYDEPENDENT");
	case DDERR_SURFACEBUSY:                  return TEXT("DDERR_SURFACEBUSY");
	case DDERR_CANTLOCKSURFACE:              return TEXT("DDERR_CANTLOCKSURFACE");
	case DDERR_SURFACEISOBSCURED:            return TEXT("DDERR_SURFACEISOBSCURED");
	case DDERR_SURFACELOST:                  return TEXT("DDERR_SURFACELOST");
	case DDERR_SURFACENOTATTACHED:           return TEXT("DDERR_SURFACENOTATTACHED");
	case DDERR_TOOBIGHEIGHT:                 return TEXT("DDERR_TOOBIGHEIGHT");
	case DDERR_TOOBIGSIZE:                   return TEXT("DDERR_TOOBIGSIZE"));
	case DDERR_TOOBIGWIDTH:                  return TEXT("DDERR_TOOBIGWIDTH");
	case DDERR_UNSUPPORTED:                  return TEXT("DDERR_UNSUPPORTED");
	case DDERR_UNSUPPORTEDFORMAT:            return TEXT("DDERR_UNSUPPORTEDFORMAT");
	case DDERR_UNSUPPORTEDMASK:              return TEXT("DDERR_UNSUPPORTEDMASK");
	case DDERR_VERTICALBLANKINPROGRESS:      return TEXT("DDERR_VERTICALBLANKINPROGRESS");
	case DDERR_WASSTILLDRAWING:              return TEXT("DDERR_WASSTILLDRAWING");
	case DDERR_XALIGN:                       return TEXT("DDERR_XALIGN");
	case DDERR_INVALIDDIRECTDRAWGUID:        return TEXT("DDERR_INVALIDDIRECTDRAWGUID");
	case DDERR_DIRECTDRAWALREADYCREATED:     return TEXT("DDERR_DIRECTDRAWALREADYCREATED");
	case DDERR_NODIRECTDRAWHW:               return TEXT("DDERR_NODIRECTDRAWHW");
	case DDERR_PRIMARYSURFACEALREADYEXISTS:  return TEXT("DDERR_PRIMARYSURFACEALREADYEXISTS");
	case DDERR_NOEMULATION:                  return TEXT("DDERR_NOEMULATION");
	case DDERR_REGIONTOOSMALL:               return TEXT("DDERR_REGIONTOOSMALL");
	case DDERR_CLIPPERISUSINGHWND:           return TEXT("DDERR_CLIPPERISUSINGHWND");
	case DDERR_NOCLIPPERATTACHED:            return TEXT("DDERR_NOCLIPPERATTACHED");
	case DDERR_NOHWND:                       return TEXT("DDERR_NOHWND");
	case DDERR_HWNDSUBCLASSED:               return TEXT("DDERR_HWNDSUBCLASSED");
	case DDERR_HWNDALREADYSET:               return TEXT("DDERR_HWNDALREADYSET");
	case DDERR_NOPALETTEATTACHED:            return TEXT("DDERR_NOPALETTEATTACHED");
	case DDERR_NOPALETTEHW:                  return TEXT("DDERR_NOPALETTEHW");
	case DDERR_BLTFASTCANTCLIP:              return TEXT("DDERR_BLTFASTCANTCLIP");
	case DDERR_NOBLTHW:                      return TEXT("DDERR_NOBLTHW");
	case DDERR_NODDROPSHW:                   return TEXT("DDERR_NODDROPSHW");
	case DDERR_OVERLAYNOTVISIBLE:            return TEXT("DDERR_OVERLAYNOTV)ISIBLE");
	case DDERR_NOOVERLAYDEST:                return TEXT("DDERR_NOOVERLAYDEST");
	case DDERR_INVALIDPOSITION:              return TEXT("DDERR_INVALIDPOSITION");
	case DDERR_NOTAOVERLAYSURFACE:           return TEXT("DDERR_NOTAOVERLAYSURFACE");
	case DDERR_EXCLUSIVEMODEALREADYSET:      return TEXT("DDERR_EXCLUSIVEMODEALREADYSET");
	case DDERR_NOTFLIPPABLE:                 return TEXT("DDERR_NOTFLIPPABLE");
	case DDERR_CANTDUPLICATE:                return TEXT("DDERR_CANTDUPLICATE");
	case DDERR_NOTLOCKED:                    return TEXT("DDERR_NOTLOCKED");
	case DDERR_CANTCREATEDC:                 return TEXT("DDERR_CANTCREATEDC");
	case DDERR_NODC:                         return TEXT("DDERR_NODC");
	case DDERR_WRONGMODE:                    return TEXT("DDERR_WRONGMODE");
	case DDERR_IMPLICITLYCREATED:            return TEXT("DDERR_IMPLICITLYCREATED");
	case DDERR_NOTPALETTIZED:                return TEXT("DDERR_NOTPALETTIZED");
	case DDERR_UNSUPPORTEDMODE:              return TEXT("DDERR_UNSUPPORTEDMODE");
	case DDERR_NOMIPMAPHW:                   return TEXT("DDERR_NOMIPMAPHW");
	case DDERR_INVALIDSURFACETYPE:           return TEXT("DDERR_INVALIDSURFACETYPE");
	case DDERR_DCALREADYCREATED:             return TEXT("DDERR_DCALREADYCREATED");
	case DDERR_CANTPAGELOCK:                 return TEXT("DDERR_CANTPAGELOCK");
	case DDERR_CANTPAGEUNLOCK:               return TEXT("DDERR_CANTPAGEUNLOCK");
	case DDERR_NOTPAGELOCKED:                return TEXT("DDERR_NOTPAGELOCKED");
	case DDERR_NOTINITIALIZED:               return TEXT("DDERR_NOTINITIALIZED");
	}
	return "Unknown Error";
}

//---------------------------------------- GetLastError ----------------------------

TCHAR *CADXDebug::GetLastError(void) //restituisce l'ultimo errore che si � verificato
{
	return m_szLastError;
}

//------------------------------------------ SetSbLastError ---------------------------

void CADXDebug::SetLastError(char *szFormat,...)
{
	va_list pArgList;

	memset(m_szLastError,0,MAX_ERRDBG);

	if (!szFormat) return;
	
	va_start(pArgList,szFormat);
	//va_start � una macro che consente di puntare pArgList all'inizio dell'elenco dei parametri opzionali
	//la funzione _vsnprintf � basata sulla lunghezza del buffer che viene passato e non sullo zero finale come wsprintf ecc...
	_vsnprintf(m_szLastError,sizeof(m_szLastError) / sizeof(char),szFormat,pArgList);
     
	va_end(pArgList);

}