/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  26-11-2003
  --------------------------------------------------------------
  A32SPRIT.H
  classe sprite 

  ***************************************************************/

#ifndef _A32SPRIT_
#define _A32SPRIT_

#include "a32graph.h"                       //libreria grafica 2D 
#include "a32fastm.h"  
#ifdef _DEBUG_                              //libreria matemtica fixedpoint
#include <crtdbg.h>
#endif

#define MAX_SPRITE_FRAME  60                //numero max. di frame in uno sprite
#define MAX_FRAME_HOT_SPOT 8                //numero massimo di hot spot per ogni frame


#define ANIM_LOOP 0
#define ANIM_TO_AND_FRO 1

//vertice 2D
typedef struct VERTEX2D_TAG
{
	int x,y;
} VERTEX2D,*VERTEX2D_PTR;


//poligono di contenimento degli sprite, serve per determinare la collisione
typedef struct BOUND_POLY_TAG
{
	VERTEX2D_PTR poly;
	size_t inum; //numero di vertici		

} BOUND_POLY,*BOUND_POLY_PTR;


//-------------------------------------- CADXSprite --------------------------------------------------//

//Sprite generico
class CADXSprite:public virtual CADXRender
{
	friend class CADXFrameManager;         //la classe CADXFrameManager puo' accedere a tutti i membri di questa classe

private: 
    
   	CADXFastMath m_Fn;                       //funzioni matematiche
    static int iInstanceCounter;
	LONG pivot_x,pivot_y;  
	FIXED_T cosk,sink;						 //coseni direttori della velocit�
	int s;                                   //spazio percorso dall'ultimo cambio di direzione
	int m_w,m_h;                                 //var. temporanee
	BOUND_POLY_PTR m_bp;

protected:
    
	//LPSTR lpszName;                          //nome dello sprite
	int cur_frame;                           //numero di frame corrente
	int max_frame_index;                     //indice massimo nel vettore delle frames
	int status;                              //stato (indica lo stato attuale dello sprite)	
	int angle_dir;                           //angolo della direzione di movimento
	int speed;                               //velocit� in pixel/frame
	int iVelx,iVely;                         //componenti del vettore velocit�
    float anim_clock;                        //tempo animazione (numero di cicli)  
	float anim_speed;                        //velocit� animazione (frame/ciclo)
	int m_iAnimStartFrame;
	int m_iAnimEndFrame;
	int m_iAnimType;                         //tipo di animazione 0=loop 1=avanti e indietro
	
	//coordinate di riferimento per il calcolo della posizione
	//coordinate degli "hot spot" di ogni frame, un hot spot � un punto della singola frame 
    int delta_x[MAX_SPRITE_FRAME][MAX_FRAME_HOT_SPOT];    
	int delta_y[MAX_SPRITE_FRAME][MAX_FRAME_HOT_SPOT];	
	BOUND_POLY_PTR m_bpoly[MAX_SPRITE_FRAME]; //poligoni di bounding per ogni frame
	IMAGE_FRAME_PTR frames[MAX_SPRITE_FRAME];//vettore che contiene i riferimenti alle frames dello sprite

public:	       	

	LONG x,y; //posizione dello sprite
	LONG old_x,old_y; //posizione precedente

	CADXSprite(void);
	CADXSprite(LPSTR name);   
	virtual ~CADXSprite(void);	
	HRESULT SetFrameManager(CADXFrameManager *fm);             //imposta il frame manager per questo sprite  
    HRESULT SetCurFrame(const LONG dwFrame);                   //imposta la frame corrente  
    HRESULT IncFrame(void); //incrementa la frame corrente
	HRESULT DecFrame(void); //decrementa la frame corrente
	int GetCurFrame(void);  //acquisisce il numero di frame corrente  
	IMAGE_FRAME_PTR GetCurFramePtr(void);
	int GetMaxFramesIndex(void); //restituisce il numero di frame impostate
	HRESULT AddFrame(const LONG dwFrame,IMAGE_FRAME_PTR pfrm); //imposta una frame del vettore
    HRESULT AddFrame(IMAGE_FRAME_PTR pfrm);	
	HRESULT SetFrameHotSpot(const LONG dwFrame,const UINT iHotSpot,const LONG lDeltax,const LONG lDeltay);
	HRESULT RotateFrameHotSpot(const LONG dwFrame,const UINT iDestHotSpot,const UINT iSrcHotSpot,const float angle);
	HRESULT SetBoundPoly(int frame_index,BOUND_POLY_PTR ppoly); //imposta il bounding polygon della frame
	BOUND_POLY_PTR GetBoundPoly(void); //rende il poligono di contenimento corrente
	virtual BOOL DetectCollision(int xc,int yc); //rende true se il punto xc,yc collide con lo sprite
	virtual BOOL DetectCollision(CADXSprite *pspr); //rende true se lo sprite collide con lo sprite pspr
	void GetFrameHotSpot(const LONG dwFrame,unsigned int iHotSpot,LONG *plx,LONG *ply);
	void SetJointPosition(LONG xnew,LONG ynew,int iHotSpot);   //imposta la posizione dell'oggetto in modo che l'ho spot si trovi in xnew ynew
    void GetJointPosition(LONG *xj,LONG *yj,int iHotSpot);       //acquisisce le coordinate asolute dell'hotspot iHotSpot 
	IMAGE_FRAME_PTR GetFramePtr(const LONG dwFrame);           //acquisisce una frame dal vettore
    HRESULT SetVelocity(const int angle,const int speed);      //imposta il vettore velocit� (angolo e modulo)     
	int GetAngle(void);  //acquisisce l'angolo di direzione
	int GetSpeed(void);  //acquisisce la velocit�
	HRESULT SetAnimSpeed(const float speed);
	float GetAnimSpeed(void);	
	HRESULT SetAnimRange(int iframe_start,int iframe_end);
	void SetAnimLoopType(int type);
	int GetAnimLoopType(void);
	void GetAnimRange(int *iframe_start,int *iframe_end);
	virtual HRESULT UpdatePosition(void);                                      //aggiorna la posizione e la frame dello sprite
	virtual void SetPosition(const LONG x,const LONG y);
	virtual void GetPosition(LONG *x,LONG *y);
	virtual void Release(void);                     //ditrugge lo sprite
    virtual void Put(void);
	virtual void Put(LONG x,LONG y);                //visualizza lo sprite sulla superficie di disegno corrente
	HRESULT RenderToDisplay(LONG x,LONG y);
    HRESULT RenderToDisplay(void);
//	HRESULT CADXSprite::RenderToDisplayJoint(LONG x,LONG y,int iHotSpot);
    virtual void UpdateFrame(void);                 //aggiorna la frame 

};

#endif
