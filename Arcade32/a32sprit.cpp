/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  26-11-2003
  --------------------------------------------------------------
  A32SPRIT.CPP
  classe sprite 

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/


#define WIN32_LEAN_AND_MEAN   

#include "a32sprit.h"

int CADXSprite::iInstanceCounter=0;

//----------------------------- CADXSprite ------------------------------------------

//Costruttore 2 (overloaded)
CADXSprite::CADXSprite(void)
{   	
	m_iBltEffcts=DDBLT_WAIT | DDBLT_KEYSRC;
	Release();

}

/* distruttore
   rilascia le frames caricate
*/

CADXSprite::~CADXSprite(void)
{
	int count;
	iInstanceCounter++;
	cur_frame=0;           //nessuna frame corrente
	max_frame_index=-1;     //nessuna frame
	status=0;               //non caricato
    
	for(count=0;count<MAX_SPRITE_FRAME;count++) frames[count]=NULL;      
}

/*----------------------------- SetFrameManager ----------------------*/

HRESULT CADXSprite::SetFrameManager(CADXFrameManager *fm)
{
	if (fm==NULL) return E_FAIL;
	
	m_lpFm=fm;

	return S_OK;

}

/*---------------------------- SetCurFrame--------------------------
 Imposta la frame corrente fra quelle caricate nel vettore.
 dwFrame � l'indice della frame da impostare come frame corrente
 dFrame deve essere >=0 e <=max_frame_index
*/

HRESULT CADXSprite::SetCurFrame(const LONG dwFrame)
{
	if (dwFrame>max_frame_index || dwFrame<0) return E_FAIL;    
	cur_frame=dwFrame;
	anim_clock=(float)cur_frame;
	return S_OK;
}

/*----------------------------- GetCurFrame ---------------------------
 Acquisisce l'indice della frame corrente
*/
int CADXSprite::GetCurFrame(void)
{
	return cur_frame;
}
//----------------------- SetFrameHotSpot ---------------------------------
//Imposta il punto di riferimento usato per disengare la frame
//lDeltaX e lDeltaY sono le coordinate del punto rispetto all'angolo in alto a sx

HRESULT CADXSprite::SetFrameHotSpot(const LONG dwFrame,const UINT iHotSpot,const LONG lDeltax,const LONG lDeltay)
{
	if (this->GetStatus()<=0) return E_FAIL;

	if (dwFrame>max_frame_index || dwFrame<0) return E_FAIL;

	if (iHotSpot>=MAX_FRAME_HOT_SPOT) return E_FAIL;

    delta_x[dwFrame][iHotSpot]=lDeltax;
	delta_y[dwFrame][iHotSpot]=lDeltay;

	return S_OK;

}

//---------------------------- RotateHotSpot ---------------------------------------
/*
Crea un hot spot ruotato nella frame dwFrame prendendo come sistema di riferimento quello della frame con indice 0
iDestHotspot=hotspot di destinazione
iSrcHotspot=hot spot da ruotare
angle=angolo in gradi
*/
HRESULT CADXSprite::RotateFrameHotSpot(const LONG dwFrame,const UINT iDestHotSpot,const UINT iSrcHotSpot,const float angle)
{
	int xt,yt,xt1,yt1;
	int w2,h2;
	float an;
	
	if (this->GetStatus()<=0) return E_FAIL;

	if (dwFrame>max_frame_index || dwFrame<0) return E_FAIL;

	if (iSrcHotSpot>=MAX_FRAME_HOT_SPOT || iDestHotSpot>=MAX_FRAME_HOT_SPOT) return E_FAIL;

	//semi larghezza e semi altezza della frame 0
	w2=(int)(frames[0]->width*0.5);
	h2=(int)(frames[0]->height*0.5);
	//trasla l'hot spot sorgente al centro della frame 0
	xt=delta_x[0][iSrcHotSpot]-w2;
	yt=delta_y[0][iSrcHotSpot]-h2; 
	//converta da gradi a radianti
	an=angle*DEG_TO_RADIANS;
	//coordinate nel sistema ruotato con centro nell'origine della frame
	xt1=(int)(xt*cos(an)-yt*sin(an));
	yt1=(int)(xt*sin(an)+yt*cos(an));
	//traslazione (le coordinate relative sono riferite all'angolo sup. sinistro della frame)
	xt1+=(int)(frames[dwFrame]->width*0.5);
	yt1+=(int)(frames[dwFrame]->height*0.5);
	//imposta il nuovo hot spot otenuto ruotando l'hot spot sorgente
	this->SetFrameHotSpot(dwFrame,iDestHotSpot,xt1,yt1);

	return S_OK;

}

//-------------------------- GetFrameHotSpot ---------------------------------------
//Acquisice l'hotspot iHotSpot di una frame (coordinate relative alla frame , origne nell'angolo sup. sinistro)
//NOTA: non controlla la validit� dei parametri
//restituisce le coordinate dell'hotspot in plx e ply
void CADXSprite::GetFrameHotSpot(const LONG dwFrame,unsigned int iHotSpot,LONG *plx,LONG *ply)
{
	*plx=delta_x[dwFrame][iHotSpot];
	*ply=delta_y[dwFrame][iHotSpot];
}

// ------------------------------ AddFrame ------------------------------
// Imposta una frame del vettore facendola puntare ad una frame caricata
// se la frame � valida (cio� gi� caricata) allora restituisce S_OK altrimenti E_FAIL
// se la frame destinazione � gi� occupata, viene rimpiazzata (in realt� cambia solo il puntatore)
// in quanto le frames non sono memorizzate dentro allo sprite

HRESULT CADXSprite::AddFrame(const LONG dwFrame,IMAGE_FRAME_PTR pfrm)
{
	if (pfrm == NULL || dwFrame>MAX_SPRITE_FRAME) return E_FAIL;
	if (m_lpFm == NULL) return E_FAIL; //in questo caso il frame manager non � inizializzato
	//se la frame � valida procede
    if ((pfrm->status==1) && (pfrm->surface)) 
	{   
		//aggiorna l'indice massimo delle frame se necessario
		if (dwFrame>max_frame_index) max_frame_index=dwFrame;
        //aggiorna il vettore
		frames[dwFrame]=pfrm;	        
        m_iStatus=1;  //frames presenti nel vettore 
		//nota: quando si aggiunge una frame aumenta anche il range di animazione
		m_iAnimEndFrame=max_frame_index;

		return S_OK;

	}
	else return E_FAIL;
}

//-------------------------- AddFrame (ovr1) ----------------------------------

HRESULT CADXSprite::AddFrame(IMAGE_FRAME_PTR pfrm)
{
	int iMax=0;
    
    iMax=GetMaxFramesIndex();
    //aggiunge un elemento al vettore delle frames
	iMax++;
	
	return AddFrame((DWORD)iMax,pfrm);
}

/*------------------------- GetFramePtr -------------------------
 Restituisce il puntatore alla frame del vettore
*/
IMAGE_FRAME_PTR CADXSprite::GetFramePtr(const LONG dwFrame) 
{
	if ((dwFrame>max_frame_index) || (max_frame_index<0)) return NULL;
    else return frames[dwFrame];
}


//-------------------------- GetCurFramePtr ----------------------

IMAGE_FRAME_PTR CADXSprite::GetCurFramePtr(void)
{
	return frames[cur_frame];
}
/*------------------- Release -----------------------------------*/
void CADXSprite::Release(void)
{
	int count,count1;

	for (count=0;count<MAX_SPRITE_FRAME;count++)
	{
		//azzera il vettore delle frames
		if (frames[count]) frames[count]=NULL;
		//resetta i poligoni di contenimento
		if (m_bpoly[count]) m_bpoly[count]=NULL;

		for (count1=0;count1<MAX_FRAME_HOT_SPOT;count1++)
		{
			delta_x[count][count1]=0;
			delta_y[count][count1]=0;

		}


	}
	
	cur_frame=0;            //frame corrente
	max_frame_index=-1;     //nessuna frame
	status=0;               //non caricato
    angle_dir=0;
	speed=0;
	x=0;y=0;old_x=0;old_y=0;
	pivot_x=0;
	pivot_y=0;
	m_iAnimStartFrame=0;
	m_iAnimEndFrame=0;
	m_iAnimType=0;
	anim_clock=0.0f;
	anim_speed=0.0f;
	s=0;
}

/*---------------------- GetMaxFramesIndex -----------------------------*/
//Restituisce l'inidce massimo del vettore delle frame
//se non ci sono frames, rende -1
int CADXSprite::GetMaxFramesIndex(void)
{   

	//restituisce il numero di frame impostate
	return max_frame_index;
	
}



//---------------------------------- GetAngle ------------------------
int CADXSprite::GetAngle(void)
{
	return angle_dir;
}

//----------------------------------- GetSpeed -------------------------
int CADXSprite::GetSpeed(void)
{
	return speed;
}

//------------------------------------ SetAnimDelay ---------------------
//imposta il tempo di ritardo delle frame per l'animazione
//NOTA: se animdelay � zero significa che non c'� animazione
HRESULT CADXSprite::SetAnimSpeed(const float speed)
{
	anim_speed=speed;
	return S_OK;
}	

//------------------------------------- GetAnimDelay --------------------

float CADXSprite::GetAnimSpeed(void)
{
	return anim_speed;
}

//----------------------------- UpdateFrame ---------------------------------
//Aggiorna la frame corrente (iCurTime � il tempo corrente
void CADXSprite::UpdateFrame(void)
{	
	
	if (anim_speed==0.0f) return;

	anim_clock += anim_speed;

	if (anim_clock<m_iAnimStartFrame) 
	{
		
		if (m_iAnimType==ANIM_TO_AND_FRO) 
		{
			//animazione avanti e indietro
			anim_clock=(float)m_iAnimStartFrame;
			anim_speed=-anim_speed; //inverte la velocit�
			cur_frame=m_iAnimStartFrame;
        }
		else
		{
			//animazione tipo loop
			cur_frame=m_iAnimEndFrame;
			anim_clock=(float)m_iAnimEndFrame;
		}
	}
	else if(anim_clock>m_iAnimEndFrame) 
	{
		if (m_iAnimType==ANIM_TO_AND_FRO)
		{
			cur_frame=m_iAnimEndFrame;
			anim_clock=(float)m_iAnimEndFrame;
			anim_speed=-anim_speed;

		}
		else
		{
			cur_frame=m_iAnimStartFrame;
			anim_clock=(float)m_iAnimStartFrame;
		}
	}

	else cur_frame=(int)anim_clock;
	
}

//------------------------------- SetAnimRange ------------------------------
//imposta l'intervallo delle frame fra cui avviene l'animazione.
//per default questo intervallo copre tutte le frame dalla 0 a max_frame_index
HRESULT CADXSprite::SetAnimRange(int iframe_start,int iframe_end)
{
	if (iframe_start>iframe_end) return E_FAIL;

	m_iAnimStartFrame=iframe_start;
	m_iAnimEndFrame=iframe_end;
	return S_OK;
}

//------------------------------- SetAnimLoopType ---------------------------
//Imposta il tipo di loop di animazione 0=loop 1=avanti e indietro
void CADXSprite::SetAnimLoopType(int type)
{
	if (type>=0 && type<=1) m_iAnimType=type;
}

//---------------------------------- GetAnimType ----------------------------

int CADXSprite::GetAnimLoopType(void)
{
	return m_iAnimType;
}

//--------------------------------- GetAnimRange ----------------------------
//restituisce l'intervallo di animazione frame
void CADXSprite::GetAnimRange(int *iframe_start,int *iframe_end)
{	
	if (*iframe_start) *iframe_start=m_iAnimStartFrame;
	if (*iframe_end) *iframe_end=m_iAnimEndFrame;
}

//---------------------------- IncFrame -------------------------------------
//incrementa la frame corrente
HRESULT CADXSprite::IncFrame(void)
{
	if (cur_frame<max_frame_index)
	{
		cur_frame++;
		return S_OK;
	}
	else return E_FAIL;

}

//---------------------------- DecFrame ---------------------------------------
HRESULT CADXSprite::DecFrame(void)
{
	if (cur_frame>0) 
	{
		cur_frame--;
		return S_OK;
	}
	else return E_FAIL;
}

//------------------------- SetPosition ------------------------------------
// Imposta la posizione dello sprite
void CADXSprite::SetPosition(const LONG x,const LONG y)
{
	this->old_x=x;
	this->old_y=y;
	this->pivot_x=x;
	this->pivot_y=y;
	this->x=x;
	this->y=y;
}

//-------------------------- GetPosition ----------------------------------
// Acquisisce la posizione corrente
void CADXSprite::GetPosition(LONG *x,LONG *y)
{
	*x=this->x;
	*y=this->y;
}

//----------------------------- Put --------------------------------------
//Disegna lo sprite sulla superficie corrente usando come
//coordinate di output i membri x e y

void CADXSprite::Put(void)
{	
	this->Put(x,y);
}

//----------------------------- Put (x,y) -----------------------------
//Disegna lo sprite mettendo il centro della frame corrente nel punto x,y
//NB la posizione dello sprite non viene aggiornata
void CADXSprite::Put(LONG x,LONG y)
{    
   
	if (frames[cur_frame] != NULL) 
	{   //centra la frame corrente in modo che il suo centro coincida con x e y		
        //disegna la frame corrente
		m_lpFm->PutImgFrame(x,y,frames[cur_frame]);
	}
}

//----------------------------- RenderToDisplay ------------------
//Esegue il rendering su una superficie 
HRESULT CADXSprite::RenderToDisplay(void)
{	
    //disegna la frame corrente
	return m_lpFm->PutImgFrameClip(x,y,m_pimgOut,frames[cur_frame],m_iBltEffcts);	
}


HRESULT CADXSprite::RenderToDisplay(LONG x,LONG y)
{	
    //disegna la frame corrente
	return m_lpFm->PutImgFrameClip(x,y,m_pimgOut,frames[cur_frame],m_iBltEffcts);	
}


//-------------------------------------------- SetJointPosition --------------------------------

//imposta le coordinate dello sprite in modo che l'hot spot iHotSpot della frame corrente si trovi in x,y
void CADXSprite::SetJointPosition(LONG xnew,LONG ynew,int iHotSpot)
{
	x=xnew-delta_x[cur_frame][iHotSpot];
	y=ynew-delta_y[cur_frame][iHotSpot];
}

//-------------------------------------------- GetJointPosition --------------------------------

//restituisce le coordinate assolute dell'hot spot iHotSpot della frame corrente
void CADXSprite::GetJointPosition(LONG *xj,LONG *yj,int iHotSpot)
{
	*xj=x+delta_x[cur_frame][iHotSpot];
	*yj=y+delta_y[cur_frame][iHotSpot];
}

/*----------------------- SetVelocity------------------------------------*/
//Imposta la velocit� e l'angolo di direzione
HRESULT CADXSprite::SetVelocity(const int angle,const int speed)
{
	if (angle<0)
	{
		angle_dir = (-angle) % 360;
		angle_dir =360-angle_dir;

	}

	else angle_dir=angle % 360;
	
	//aggiorna i coseni direttori
	cosk=m_Fn.CosFast[angle_dir];
	sink=m_Fn.SinFast[angle_dir];
	
	this->speed=speed;
	//imposta le componenti della velocit�
	iVelx=(speed*cosk)>>10;
	iVely=(speed*sink)>>10;
	
	pivot_x=x;  //imposta le coordinate di riferimento per il calcolo della velocit�
	pivot_y=y;
	s=0;        //resetta lo spazio percorso
    
	return S_OK;
}

//-------------------- UpdatePosition ----------------------------
//Aggiorna la posizione in base alla velocit� impostata
//senza ridisegnare lo sprite
HRESULT CADXSprite::UpdatePosition(void)
{	
	old_x=x;
	old_y=y; 

	s += speed;
    //aggiorna la posizione dello sprite
	//cosk e sink sono fixed point con 10 bit per i decimali
	x=(int) ((long)pivot_x+(((long)s*cosk)>>10)); //calcola le nuove coord.
    y=(int) ((long)pivot_y+(((long)s*sink)>>10)); //in base a pivot_x e pivot_y che sono il punto di cambio direzione       

    return S_OK; 

}

//-------------------------- SetBoundPoly -----------------------------------------
//Imposta il poligono di contenimento della frame.Viene usato questo poligono per
//determinare le collisioni

HRESULT CADXSprite::SetBoundPoly(int frame_index,BOUND_POLY_PTR ppoly) //imposta 
{
	if (frame_index<0 || frame_index>max_frame_index) return E_FAIL;
	
	m_bpoly[frame_index]=ppoly;

	return S_OK;
}

//---------------------- DetectCollision -----------------------------------------

BOOL CADXSprite::DetectCollision(int xc,int yc) //rende true se il punto xc,yc collide con lo sprite
{
	//traslazione
	xc=xc-x;
	yc=yc-y;

	if (m_bpoly[cur_frame])
	{
		m_bp=m_bpoly[cur_frame];
		//rende true se xc,yc � all'interno del poligono di definizione della frame corrente
		BOOL inside=FALSE;
		size_t npoints=m_bp->inum;			
		VERTEX2D_PTR ppoly=m_bp->poly;
		VERTEX2D_PTR newp,oldp=&ppoly[npoints-1];
		VERTEX2D_PTR pleft,pright;
		
		//algoritmo che determina se un punto � all'interno di un poligono 
		for (unsigned int count=0;count < npoints;count++)
		{
			newp=&ppoly[count];
			
			if (newp->x>oldp->x)
			{
				pleft=oldp;
				pright=newp;
			}

			else
			{
				pleft=newp;
				pright=oldp;
			}

			if ((newp->x<xc) == (xc<=oldp->x) && (yc-pleft->y)*(pright->x-pleft->x) <(pright->y - pleft->y)*(xc-pleft->x))
			{
				inside=!inside;
			}

			oldp=newp;
		}

		return (inside);

	}

	else
	{
		//nessun poligono � definito: considera le frames rettangolari
		m_w=frames[cur_frame]->width;
		m_h=frames[cur_frame]->height;

		//non � definito un poligono e quindi usa il rettangolo della frame
		if (xc<x) return FALSE;
		if (xc>x+m_w) return FALSE;
		if (yc<y) return FALSE;
		if (yc>y+m_h) return FALSE;
		return TRUE;
	}

}


//-------------------------------- GetBoundPoly -------------------------------
//restituisce il poligono di contenimento corrente
BOUND_POLY_PTR CADXSprite::GetBoundPoly(void)
{
	return m_bpoly[cur_frame];
}

//-------------------------------- DetectCollision ------------------------------
//collisione fra due sprite
BOOL CADXSprite::DetectCollision(CADXSprite *pspr) //rende true se lo sprite collide con lo sprite pspr
{
	if (!pspr) return FALSE;

	BOUND_POLY_PTR pb=pspr->GetBoundPoly();

	if (!pb)
	{
		m_bp=m_bpoly[cur_frame];

		if (!m_bp)
		{
			//semplice collisione fra rettangoli
			IMAGE_FRAME_PTR pimg=frames[cur_frame];
			IMAGE_FRAME_PTR pimg1=pspr->GetCurFramePtr();
			
			if (x>pspr->x+(int)pimg1->width) return FALSE;
			if (pspr->x>x+(int)pimg->width) return FALSE;
			if (y>pspr->y+(int)pimg1->height) return FALSE;
			if (pspr->y>y+(int)pimg->height) return FALSE;

			return TRUE;					
		}

		else
		{
			IMAGE_FRAME_PTR pimg1=pspr->GetCurFramePtr();
			int x1=pspr->x+(int)pimg1->width;
			int y1=pspr->y+(int)pimg1->height;

			//lo sprite secondario non ha poligono mentre questo si
			//controlla i quattro vertici con il poligono corrente
			if (DetectCollision(pspr->x,pspr->y)) return TRUE;
			if (DetectCollision(x1,pspr->y)) return TRUE;
			if (DetectCollision(x1,y1)) return TRUE;
			if (DetectCollision(pspr->x,y1)) return TRUE;

			return FALSE;

		}
	}
	else
	{
		//lo sprite di confronto ha un poligono di contorno
		m_bp=m_bpoly[cur_frame];

		if (!m_bp)
		{
			//lo sprite attuale non ha poligono
			IMAGE_FRAME_PTR pimg=frames[cur_frame];
			int x1=x+(int)pimg->width;
			int y1=y+(int)pimg->height;

			if (pspr->DetectCollision(x,y)) return TRUE;
			if (pspr->DetectCollision(x1,y)) return TRUE;
			if (pspr->DetectCollision(x1,y1)) return TRUE;
			if (pspr->DetectCollision(x,y1)) return TRUE;

			return FALSE;

		}
		else
		{
			//caso piu' complesso: entrambi gli sprite hanno un poligono di bounding
			int num=pb->inum;
		//int x1=pspr->x;
		//int y1=pspr->y;
			VERTEX2D_PTR pv=pb->poly;
			for (int count=0;count<num;count++)
			{
				if (DetectCollision(pv->x,pv->y)) return TRUE;
				pv++;
			}

		}
	}
	//da fare
	return FALSE;
}




	