/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  26-11-2003
  --------------------------------------------------------------
  A32AUDIO.H
  funzioni x effetti sonori e musica (wrapper direct sound e direct music)

  ***************************************************************/


#ifndef _A32AUDIO_
#define _A32AUDIO_

#include <dmusicc.h>
#include <dmusici.h>
#include <direct.h>
#include "a32debug.h"

//struttura che contiene le informazioni di un file sonoro
typedef struct SOUND_STRUCTTag
{
	char *strFileName;
	LPDIRECTSOUNDBUFFER *lpDSBuffers;  //buffer sonori		
	int iNumBuffers;                   //numero di buffer creati


} SOUND_STRUCT,*SOUND_STRUCT_PTR;

#define DD_INIT_STRUCT(ddstruct) { memset(&ddstruct,0,sizeof(ddstruct)); ddstruct.dwSize=sizeof(ddstruct); }

#define MAXSOUNDS 80    //numero massimo di file sonori manipolati
#define MAXBUFFERS 160

//converte da formato multibyte a unicode
//CP_ACP indica che la stringa multibyte usa il code page ANSI
#define MULTI_TO_WIDE( x,y )  MultiByteToWideChar( CP_ACP, \
        MB_PRECOMPOSED, y, -1, x, _MAX_PATH );

#define NUMEVENTS 2

////////////////////////////// CADXSound //////////////////////////////

class CADXSound : private virtual CADXDebug
{

private:
	

	//file handle per il file wav
    HMMIO m_hmmio;
    //chunk information
    MMCKINFO m_mmckinfoData, m_mmckinfoParent;
    WAVEFORMATEX *m_pwfx;        
	HANDLE m_rghEvent[NUMEVENTS];
	LPDIRECTSOUND m_lpds; 	//oggetto direct sound
	LPDIRECTSOUNDBUFFER m_lpdsbPrimary;
	SOUND_STRUCT m_sndArray[MAXSOUNDS]; 
    DSBUFFERDESC m_dsbdescPrimary;  	
	LPDIRECTSOUNDNOTIFY m_lpdsNotify;
	DSBUFFERDESC m_dsbdesc;	//struttura che descrive il buffer sonoro	
    int m_iStatus;

	//carica un suono nel direcsound buffer
	HRESULT DD_Load_Sound(LPSTR strFileName,LPDIRECTSOUNDBUFFER *lppdsb);
	BOOL RestoreBuffer( LPDIRECTSOUNDBUFFER pDSB,BOOL *pbWasRestored);
    
public :

	CADXSound();
	~CADXSound();
	
	HRESULT Init(HWND hwnd,GUID *pguid); //inizializza direct sound
	LONG CreateSound(TCHAR *strFileName,DWORD dwNBuff); //carica un wav in memoria
    BOOL FreeSound(LONG dwNBuff);//libera un suono caricato
	void FreeAllSounds(void);//libera tutti i suoni caricati
	BOOL PlaySound(LONG dwBuffIndex,DWORD dwPriority, DWORD dwFlags); //suona un file wav
	BOOL StopSound(DWORD dwNBuff);//interrompe un file wav
	BOOL IsPlaying(LONG dwBuffIndex);//restituisce true se il suono � in esecuzione
	int GetStatus(void);
	void ShutDown(void);

};


///////////////////////////////// CADXMusic ///////////////////////////////////////

//Classe Music : implementa la gestione dei file MIDI

class CADXMusic : private virtual CADXDebug
{

private :
	
	//oggetto performance principale
	IDirectMusicPerformance* m_pPerf;
	//oggetto loader usato per caricare i file musicali da disco
	IDirectMusicLoader* m_pLoader;
	//segmento MIDI
	IDirectMusicSegment* m_pMIDISeg;
	//stato del segmento midi
	IDirectMusicSegmentState* m_pSegState;

	IDirectMusicPerformance* CreatePerformance(void);
	IDirectMusicLoader* CreateLoader(void);
	IDirectMusicSegment* LoadMIDISegment(IDirectMusicLoader* pLoader,WCHAR *wszMidiFileName,char *szSearchDir);
    int m_iStatus;

public :

	CADXMusic(); //costruttore
	~CADXMusic();//distruttore
	HRESULT Init(HWND hwnd);//Inizializza la classe 
	BOOL LoadMIDI(char *szMidiFileName,char *szSearchDir);
	BOOL PlayMIDI(DWORD dwRepeats);
	void StopMIDI(void);
	void FreeMIDI(void);
	void ShutDown(void);
	int GetStatus(void);
};

#endif