/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  5-11-2004
  --------------------------------------------------------------
  A32UTIL.CPP
  funzioni utili varie da usare con la libreria arcade32

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  *************************************************************/


#include "a32util.h"
#include "a32fastm.h"
#include "a32graph.h"
#include "a32sprit.h"
#include "a32debug.h"

//------------------------- AddVertex -----------------------------------------------------
//Aggiunge un vertice ad un poligono di contenimento gi� allocato

void AddVertex(BOUND_POLY_PTR pdest,int x,int y)
{
	pdest->poly[pdest->inum].x=x;
	pdest->poly[pdest->inum].y=y;
	pdest->inum++;
}

//-------------------------- RotatePoly ---------------------------------------------------
/*
Ruota un poligono di contenimento
pdest=poligono di destinazione (poligono ruotato)
psrc=poligono sorgente da ruotare
pimgsrc=immagine di riferimento (viene usata per acquisire larghezza e altezza)
theta=angolo di rotazione
bFitSize=indica se dopo la rotazione le dimensioni del rettangolo che circoscrive il poligono devono essere adattate (vedere funzione CreateRotFrame)
*/

void RotatePoly(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc,int theta,BOOL bFitSize)
{
	float tx,ty,tx1,ty1;
	float fx,fy,fx1,fy1;
	float ca,sa;
	if (!psrc || !pdest || !pimgsrc) return;

	if (pdest->poly)
	{
		if (pdest->inum<=0) return; //situazione ambigua
		//resetta il poligono di destinazione
		SAFE_DELETE_ARRAY(pdest->poly);
	}

	pdest->inum =psrc->inum;
	pdest->poly=new VERTEX2D[pdest->inum];

	tx=(float)(pimgsrc->width * 0.5);
	ty=(float)(pimgsrc->height *0.5);

	ca=(float)cos(theta*DEG_TO_RADIANS);
	sa=(float)sin(theta*DEG_TO_RADIANS);

	//calcola la semilarghezza e semialtezza della frame dopo la rotazione
	if (!bFitSize)
	{
		tx1=tx;
		ty1=ty;

	}
	else
	{		
		//la frame viene ridimensionata
		tx1=(float)(tx*fabs(ca)+ty*fabs(sa));
		ty1=(float)(ty*fabs(ca)+tx*fabs(sa));			
	}

	//trasla tutti i punti
    for (unsigned int count=0;count<pdest->inum;count++)
	{
		fx=(float)psrc->poly[count].x;
		fy=(float)psrc->poly[count].y;

		//trasla nell'origine
		fx-=tx;
		fy-=ty;

		//esegue la rotazione
		fx1=fx*ca+fy*sa;
		fy1=fx*sa-fy*ca;		

		//riporta l'origine nell'angolo sup. sinistro
		fx1+=tx1;
		fy1+=ty1;

		//salva il punto ruotato
		pdest->poly[count].x=(int)fx1;
		pdest->poly[count].y=(int)fy1;
		
	}
}


//crea un poligono speculare rispetto all'asse verticale 
void MirrorPolyH(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc)
{
	if (!pdest || !psrc || !pimgsrc) return;

	if (pdest->poly)
	{
		if (pdest->inum<=0) return; //situazione ambigua
		//resetta il poligono di destinazione
		SAFE_DELETE_ARRAY(pdest->poly);
	}

	pdest->inum =psrc->inum;
	pdest->poly=new VERTEX2D[pdest->inum];

	unsigned int w=pimgsrc->width;

	//trasla tutti i punti
    for (unsigned int count=0;count<pdest->inum;count++)
	{
		pdest->poly[count].y=psrc->poly[count].y; //l'ordinata resta invariata perch� il flipping � orizzontale
		pdest->poly[count].x=w-psrc->poly[count].x; //punto speculare rispetto all'asse verticale
	}
}

//crea un poligono speculare rispetto all'asse orizzontale
void MirrorPolyV(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc)
{
	if (!pdest || !psrc || !pimgsrc) return;

	if (pdest->poly)
	{
		if (pdest->inum<=0) return; //situazione ambigua
		//resetta il poligono di destinazione
		SAFE_DELETE_ARRAY(pdest->poly);
	}

	pdest->inum =psrc->inum;
	pdest->poly=new VERTEX2D[pdest->inum];

	unsigned int h=pimgsrc->height;

	//trasla tutti i punti (l'origine � nell'angolo sup. sinistro dell'immagine)
    for (unsigned int count=0;count<pdest->inum;count++)
	{
		pdest->poly[count].y=h-psrc->poly[count].y; 
		pdest->poly[count].x=psrc->poly[count].x; 
	}
}


