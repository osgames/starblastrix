/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32INPUT.H
  funzioni di acquisizione input (wrapper x DIRECT INPUT)

  ***************************************************************/

#include "a32debug.h" //libreri x debug
#include <dinput.h>

#ifndef _DDINPUT_INCLUDE_
#define _DDINPUT_INCLUDE_

//macro che copntrolla lo stato di un tasto 
#define KEYDOWN(buff,key) (buff[key] & 0x80)   
#define KEYUP(buff,key) (buff[key] == 0)

#define MAX_JOY_RANGE 1000

//linked list con i device di input presenti sul pc
typedef struct DEVICE_INFO
{
	DIDEVICEINSTANCE di;
	DEVICE_INFO *next;

} DEVICE_INFO,*DEVICE_INFO_PTR;

//funzione callback per l'enumerazione dei dispositivi di input
BOOL CALLBACK DIEnumDevicesCallback(LPCDIDEVICEINSTANCE lpddi,LPVOID pvRef);
//funzioni di enumerzione dei device di input
void FreeDeviceList(void);
LONG EnumDevices(LPDIRECTINPUT7 lpdi);
//funzione callback per l'enumerazione dei devie di input
BOOL CALLBACK DIEnumDevicesCallback(LPCDIDEVICEINSTANCE lpddi,LPVOID pvRef);
//enumera gli oggetti del joystick e imposta i range max e min per gli assi
BOOL CALLBACK EnumJoyObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi,
                                   VOID* pContext );

#define MAX_VKEY 256

class CADXInput : public virtual CADXDebug
{
private:


	LPDIRECTINPUT7        m_lpdi;           //istanza di direct input (versione 7)
	LPDIRECTINPUTDEVICE7  m_lpKyb;          //keyboard device
	LPDIRECTINPUTDEVICE7  m_lpMouse;        //Mouse device
	LPDIRECTINPUTDEVICE   m_lpJoystick;     //joystick
	unsigned char         m_Buff[MAX_VKEY];      //buffer dei tasti della tastiera
	HANDLE                m_hevtMouse;      //evento usato per gestire il mouse
	BOOL                  m_bKeyStatus[MAX_VKEY];
	int                   m_iASCII[MAX_VKEY]; //vettore di corrispondenze fra codici dei tasti DirectInput e codici ASCII  
	int m_iStatus;
	DIMOUSESTATE          m_mouse_state;    //struttura che contiene lo stato del mouse (posizione stato dei tasti ecc...)
	DIMOUSESTATE          m_buffered_mouse_state; //struttura che contiene lo stato bufferizzato del mouse (coordinate assolute)
	LONG                  EnumAllDevices(void); //esegue l'enumerazione di tutti i device di input e memorizza le informazioni nella lista m_pDevices
	BOOL                  m_bJoystickAttached;    //flag che indica se un joystick � attaccato al pc
	DIJOYSTATE2           m_js;                   //stato del joystick

public:
	
	CADXInput();
	~CADXInput();
	void ResetKyb(void);
	void ResetMouse(void);
	HRESULT Init(HINSTANCE hinst,HWND hwnd); //Inizializza la classe
	void ShutDown(void); //rilascia direct input
	HRESULT GetJoystickStatus(void);
	HRESULT GetKybStatus(void); 
	HRESULT GetMouseState(void);
    HRESULT GetBufferedMouseState(void);
	void SwitchWinLogoKey(BOOL enabled);//abilita / disabilita il tasto win logo 
	BOOL KeyDown(int key);
	BOOL KeyUp(int key);
	BOOL KeyPress(int key);
	int GetKeyASCII(int iKeyCode);
	int GetCurrentKeyDown(int iScan1,int iScan2);
	int GetCurrentKeyDown(void);
	int GetStatus(void);
	int GetDeviceCount(void); //restituisce il numero di device
	TCHAR *GetDeviceName(LONG Index); //nome del device con indice Index
	TCHAR *GetDeviceProductName(LONG Index); //nome di produzione del device con indice Index
	DWORD GetDeviceType(LONG Index); //tipo DirectInput del device con indice Index
	BOOL JoystickAttached(void); //rende true se un joystick � attaccato al pc
	HANDLE GetMouseHandle(void); //ritorna l'handle dell'evento che gestisce il mouse
    HRESULT SetMousePosition(LONG lX,LONG lY,LONG lZ);//imposta la posizione assoluta del mouse 
	
	//funzioni che leggono lo stato del mouse:
	//input non bufferizzato
	LONG MouseDeltaX(void);
	LONG MouseDeltaY(void);
	LONG MouseDeltaWheel(void);
	//input bufferizzatto
	LONG MouseX(void);
	LONG MouseY(void);
	LONG MouseWheel(void);
	BOOL MouseLeftButton(void);
	BOOL MouseRightButton(void);
	BOOL MouseMiddleButton(void);  
	LONG JoystickX(void); //assex joystick
	LONG JoystickY(void); //assey joystick	
	BOOL JoystickButtons(int button); //stato del tasto del joystick
	
};

#endif //fine include





