/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  26-11-2003
  --------------------------------------------------------------
  A32AUDIO.CPP
  funzioni x effetti sonori e musica (wrapper direct sound e direct music)
------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/


#define WIN32_LEAN_AND_MEAN   //se si inserisce questa definizione il file windows.h


#include "a32audio.h"          //header per questo file
#include <crtdbg.h>

//---------------------------------------------------------------------
//                     CADXSound  
//---------------------------------------------------------------------

CADXSound::CADXSound(void)
{
	m_iStatus=0;
	m_lpds=NULL;

}

CADXSound::~CADXSound(void)
{	
	this->ShutDown();
}

//---------------------- Init -------------------------------------
//Inizializza direct sound se va tutto bene rende S_OK

HRESULT CADXSound::Init(HWND hwnd,GUID *pguid)
{
	
	WAVEFORMATEX wfx;      //struttura direct sound usata per impostare il formato
    HRESULT hr;    
	int count;
    
    m_lpds=NULL;

    
	//azzera il vettore dei suoni
	for (count=0;count<MAXSOUNDS;count++)
	{
		m_sndArray[count].iNumBuffers = 0;
		m_sndArray[count].lpDSBuffers = NULL;
		m_sndArray[count].strFileName = NULL;
	}  

	hr=DirectSoundCreate(pguid, &m_lpds, NULL);

    // crea l'oggetto direct sound 
    if FAILED(hr) 
	{
		this->WriteErrorFile("DD_Init_Sound : errore durante la creazione dell'oggetto principale.");		
		return hr;
	} 
	
	//imposta il cooperative level
	hr=IDirectSound_SetCooperativeLevel(m_lpds,hwnd,DSSCL_PRIORITY);
    //imposta il cooperative level 
	//occorre impostare DSSCL_PRIORITY per poter poi impostare il formato
    if FAILED(hr)
	{
		this->WriteErrorFile("InitSound : errore durante l'impostazione del cooperative level.");

        return hr;
	}
    
    //ottiene il buffer sonoro primario
    ZeroMemory(&m_dsbdescPrimary, sizeof(DSBUFFERDESC));
    m_dsbdescPrimary.dwSize = sizeof(DSBUFFERDESC);
    m_dsbdescPrimary.dwFlags = DSBCAPS_PRIMARYBUFFER;
    

	hr=m_lpds->CreateSoundBuffer(&m_dsbdescPrimary, &m_lpdsbPrimary, NULL);

	if FAILED(hr)
    {

		WriteErrorFile("InitSound : errore durante la creazione del buffer sonoro primario.");
	
        return hr;
	}    
    
	//imposta il formato
    memset(&wfx, 0, sizeof(WAVEFORMATEX)); 
	//l'unico formato valido � WAVE_FORMAT_PCM
    wfx.wFormatTag = WAVE_FORMAT_PCM; 
	//2 canali stereo
    wfx.nChannels = 2; 
	//frequenza di campionamento
    wfx.nSamplesPerSec = 44100; 
	//16 bit per campione
    wfx.wBitsPerSample = 16; 	
    wfx.nBlockAlign = wfx.wBitsPerSample / 8 * wfx.nChannels;
    wfx.nAvgBytesPerSec = wfx.nSamplesPerSec * wfx.nBlockAlign;
    //imposta il formato
    hr = m_lpdsbPrimary->SetFormat(&wfx); 
    
    if (FAILED(hr))
	{
		
		this->WriteErrorFile("InitSound : errore durante l'impostazione del formato: canali=%d freq. di campionamento=%lu",wfx.nChannels,wfx.nSamplesPerSec);
        	
        return hr;
	}

	m_iStatus=1; //oggetto inizializzato

    return TRUE;

}

//------------------------- CreateSound -------------------------------
//Carica un file wav in memoria
//strfileName � il file da caricare
//dwNBuff � il numero di buffer da riservare
//Se il caricamento va a buon fine restituisce un indice
//che identifica il suono caricato che rappresenta la posizione del suono
//all'interno del vettore (� anche il numero da passare a PlaySound per suonare il file)
//se fallisce rende -1

LONG CADXSound::CreateSound(TCHAR *strFileName,DWORD dwNBuff)
{
	DWORD   dwCount;	
	DWORD   dwBuffs;
	DWORD   index;
	size_t  len; 
	BOOL    bFound = FALSE;
	HRESULT hr;
    LPDIRECTSOUNDBUFFER lpDS;
    

	if (m_iStatus==0)
	{
		this->WriteErrorFile(TEXT("CreateSound: l'oggetto CADXSound non � stato inizializzato."));

		return -1;
	}

    //cerca la prima posizione libera
	for (dwCount=0; dwCount<MAXSOUNDS; dwCount++)
	{
		if (m_sndArray[dwCount].iNumBuffers == 0)
		{
			bFound=TRUE;
			
			index=dwCount;

			//crea la struttura			
			break;
		}

	}

	if (!bFound) return  -1;  //nessuno spazio libero
    //imposta il numero di buffer da caricare per questo suono
	if ((dwNBuff >0) && (dwNBuff<MAXBUFFERS)) dwBuffs=dwNBuff;
	else return -1; //non crea il suono

    //crea il vettore dei buffers per questo suono
    m_sndArray[index].lpDSBuffers = new LPDIRECTSOUNDBUFFER[dwBuffs];
    

	hr=S_OK;

	for (dwCount=0; dwCount<dwBuffs; dwCount++)
	{   

        lpDS= (LPDIRECTSOUNDBUFFER) m_sndArray[index].lpDSBuffers[dwCount];
        
		
		//il suono viene copiato in ogni buffer
		hr=DD_Load_Sound(strFileName, &m_sndArray[index].lpDSBuffers[dwCount]);
       // hr=DD_Load_Sound(strFileName, lpTry);
        
		if (FAILED(hr))
		{
#ifdef _DEBUG_
			WriteErrorFile(g_szErrorFileName,"CreateSound : errore nella creazione del suono n�%d dal file %s",index,strFileName);
#endif
						
			//if (lpDS) lpDS->Release();							
			
			//appena fallisce esce dal ciclo
			break;
		}
		else
		{   

		
			//incrementa il numero di buffer aggiunti
			m_sndArray[index].iNumBuffers ++;
            
		}
        
	}

	if (!FAILED(hr))
	{

	if (m_sndArray[index].iNumBuffers < 0) return -1;	
   
	//copia il nome del file nel membro della struttura
	len=strlen(strFileName);
	
	m_sndArray[index].strFileName = new char[len+1];
    memcpy((char *) m_sndArray[index].strFileName,(char *)strFileName,len);
    
    } 
	
	else index=-1;

    return (LONG) index;

}

//----------------------- DD_Load_Sound ---------------------------------

//Carica un file Wav nel buffer direct sound

HRESULT CADXSound::DD_Load_Sound(LPSTR strFileName,LPDIRECTSOUNDBUFFER *lppdsb)
{   

	MMRESULT         mmr;
	HRESULT          hr;
	MMCKINFO         ckIn;
	MMIOINFO         mmioinfoIn;				//struttura usata per acquisire le informazioni su file IO prima della lettura dal file al buffer
    PCMWAVEFORMAT    pcmWaveFormat;             //struttura temp. 
    DWORD            dwBufferSize;	            //dimensione del buffer 
    VOID*            pDSLockedBuffer= NULL;		//punta alla memoria del buffer bloccata
    DWORD            dwDSLockedBufferSize;
    LPDIRECTSOUNDBUFFER lpdsb;

    //apre il file per I/O bufferizzato
    m_hmmio=mmioOpen( strFileName, NULL, MMIO_ALLOCBUF | MMIO_READ );
    
    if (m_hmmio == NULL)
	{

		this->WriteErrorFile("DD_Load_Sound: errore durante l'apertura del file %s",strFileName); 
        
        mmioClose(m_hmmio,0);		
		return E_FAIL;
	}

    mmr=mmioDescend( m_hmmio, &m_mmckinfoData, NULL , 0); 

    //ricerca le informazioni sul file
	if( ( mmr != MMSYSERR_NOERROR) )
	{

        this->WriteErrorFile("DD_Load_Sound: errore durante la ricerca delle informazioni nel file multimediale %s errore=%d",strFileName,mmr);

		mmioClose(m_hmmio,0);	

		return E_FAIL;
	}
    //controlla che sia un file WAVE valido
    if( (m_mmckinfoData.ckid != FOURCC_RIFF) ||
        (m_mmckinfoData.fccType != mmioFOURCC('W', 'A', 'V', 'E') ) )
    {

        this->WriteErrorFile("DD_Load_Sound: il file %s non � un file WAV valido!",strFileName);

        mmioClose(m_hmmio,0);	

	    return E_FAIL;

	}
        
    // ricerca il chunk f m t nel file WAV.
    ckIn.ckid = mmioFOURCC('f', 'm', 't', ' ');
	
    if ( 0 != mmioDescend( m_hmmio, &ckIn, &m_mmckinfoData, MMIO_FINDCHUNK ) )
    {

		this->WriteErrorFile("DD_Load_Sound: errore durante la ricerca del chunk fmt nel file %s ",strFileName);

		mmioClose(m_hmmio, 0);
		return E_FAIL;

	}   		
	
    // Ci si aspetta che il 'chunk' fmt sia grande almeno quanto <PCMWAVEFORMAT>;
    // se ci sono parametri extra vengono ignorati
    if( ckIn.cksize < (LONG) sizeof(PCMWAVEFORMAT) )
	{

		this->WriteErrorFile("DD_Load_Sound: dimensioni del chunk fmt inferiori a PCWAVEFORMAT nel file %s",strFileName);
		mmioClose(m_hmmio,0);		 
		return E_FAIL;        
	}
    
    // Copia i dati dal chunk fmt nella struttura PCMWAVEFORMAT
    if( mmioRead( m_hmmio, (HPSTR) &pcmWaveFormat,sizeof(pcmWaveFormat)) != sizeof(pcmWaveFormat) )
    {

        this->WriteErrorFile("DD_Load_Sound: errore durante la lettura del chunk fmt dal file %s",strFileName);

		mmioClose(m_hmmio,0);	
		
		return E_FAIL;        

	}	

    // Alloca la struttura waveformatex, ma se non � formato pcm
    // legge la word successiva alloca i bytes extra
    if( pcmWaveFormat.wf.wFormatTag == WAVE_FORMAT_PCM )
    {
        m_pwfx = (WAVEFORMATEX*) new CHAR[ sizeof(WAVEFORMATEX) ];
        if( NULL == m_pwfx )
		{

             this->WriteErrorFile("DD_Load_Sound: errore durante l'allocazione della struttura WAVEFORMATEX per il file %s",strFileName);

		     mmioClose(m_hmmio,0);			   

   	         return E_FAIL; 
		
		}
        // Copia dalla struttura PCM in m_pwfx
        memcpy( m_pwfx, &pcmWaveFormat, sizeof(pcmWaveFormat) );        
		m_pwfx->cbSize = 0;
		
    }
    else
    {
        // Legge gli extra bytes
        WORD cbExtraBytes = 0L;
        if( mmioRead( m_hmmio, (CHAR*)&cbExtraBytes, sizeof(WORD)) != sizeof(WORD) )
	    {

             this->WriteErrorFile("DD_Load_Sound: errore durante la lettura del numero di extra bytes nel file %s",strFileName);

   	         mmioClose(m_hmmio,0);			 

			 return E_FAIL; 		
	    }
         
        m_pwfx = (WAVEFORMATEX*)new CHAR[ sizeof(WAVEFORMATEX) + cbExtraBytes ];
        
		if( NULL == m_pwfx )
		{

             this->WriteErrorFile("DD_Load_Sound: errore durante la lettura della struttura WAVEFORMATEX e di %lu extrabytes nel file %s",strFileName,cbExtraBytes);

   	         mmioClose(m_hmmio,0);	

			 return E_FAIL; 		
	    }           

        // Copia i bytes nella struttura e setta il numero di extra-bytes
        memcpy( m_pwfx, &pcmWaveFormat, sizeof(pcmWaveFormat) );
        //setta gli extrabytes
		m_pwfx->cbSize = cbExtraBytes;

        // Now, read those extra bytes into the structure, if cbExtraAlloc != 0.
        if( mmioRead( m_hmmio, (CHAR*)(((BYTE*)&(m_pwfx->cbSize))+sizeof(WORD)),
                      cbExtraBytes ) != cbExtraBytes )
        {

               this->WriteErrorFile("DD_Load_Sound: errore durante la lettura di %lu extrabytes nel file %s",strFileName,cbExtraBytes);
       

   	           mmioClose(m_hmmio,0);
			   
			   delete m_pwfx;

			   return E_FAIL;            
        }
    }//fine 

    // Risale il file dal chunk fmt
    if( 0 != mmioAscend( m_hmmio, &ckIn, 0 ) )
    {


               this->WriteErrorFile("DD_Load_Sound: durante l'ascesa del file %s a partire dal chunk fmt",strFileName);

   	         
			   mmioClose(m_hmmio,0);	

			   if (m_pwfx) delete m_pwfx;

			   return E_FAIL;       
    }
    

    //crea un buffer statico per contenere l'effetto sonoro
    memset(&m_dsbdesc, 0, sizeof(DSBUFFERDESC)); 
    m_dsbdesc.dwSize = sizeof(DSBUFFERDESC); 
    m_dsbdesc.dwFlags =
		    DSBCAPS_GETCURRENTPOSITION2   
            | DSBCAPS_GLOBALFOCUS         // background playing
            | DSBCAPS_CTRLPOSITIONNOTIFY
			| DSBCAPS_CTRLVOLUME 
			| DSBCAPS_LOCDEFER;
			
			 //permette di specificare se il buffer verr� suonato tramite hw o sw
    
    //imposta le dimensioni del buffer tramite la struttura m_mmckinfoData
    
	dwBufferSize=m_mmckinfoData.cksize;
    
    //le dimensioni del buffer sono quelle ottenute in m_mmckinfoData durante
	//la lettura del file, infatti si carica tutto il file nel buffer
	//se il buffer secondario fosse stato uno streaming buffer si deve caricare solo
	//una porzione del file e aggiornarlo di volta in volta
    m_dsbdesc.dwBufferBytes = dwBufferSize;
     
	m_dsbdesc.lpwfxFormat = m_pwfx; 
        
    //crea il buffer secondario
    if FAILED(IDirectSound_CreateSoundBuffer(
		m_lpds, &m_dsbdesc, lppdsb, NULL))
    {
        

               this->WriteErrorFile("DD_Load_Sound: impossibile creare il buffer secondario relativo al file %s.",strFileName);

   	         
			   mmioClose(m_hmmio,0);

			   if (m_pwfx) delete m_pwfx;

			   return E_FAIL;     
	}
    
    lpdsb=(LPDIRECTSOUNDBUFFER) *lppdsb;

	
    //riempie il buffer con i dati dal file WAV
	
    hr=lpdsb->Lock(0,dwBufferSize,&pDSLockedBuffer,&dwDSLockedBufferSize,NULL,NULL,0L);
    //se il buffer � stato perso lo ripristina
    if (DSERR_BUFFERLOST == hr) 
    {   //usa la virtual table per chiamare Restore
        lpdsb->Restore(); 
        //blocca il buffer di nuovo...
		hr=lpdsb->Lock(0,dwBufferSize,&pDSLockedBuffer,&dwDSLockedBufferSize,NULL,NULL,0L);
    } 
    	
	//a questo punto non ci devono essere errori altrimenti esce
    if (FAILED(hr))
	{

		this->WriteErrorFile("DD_Load_Sound : si � verificato un errore bloccando il buffer "); 

		mmioClose(m_hmmio,0);

		if (m_pwfx) delete m_pwfx;

		return E_FAIL;   
	}

    //riporta il puntatore all'inizio del file
	if (-1 == mmioSeek(m_hmmio,m_mmckinfoData.dwDataOffset + sizeof(FOURCC),SEEK_SET))
	{
		

		this->WriteErrorFile("DD_Load_Sound: si � verificato un errore riportando il puntatore all'inizio del file WAV : %s",strFileName);


		mmioClose(m_hmmio,0);

		if (m_pwfx) delete m_pwfx;

		return E_FAIL;
	}

	//cerca i dati nel file WAV
	ckIn.ckid=mmioFOURCC('d','a','t','a');
		
	mmr=mmioDescend(m_hmmio, &ckIn, &m_mmckinfoData, MMIO_FINDCHUNK);
        
	if( ( mmr != MMSYSERR_NOERROR) )
	{

		this->WriteErrorFile("DD_Load_Sound: errore durante la ricerca del chunk dei dati nel file multimediale %s errore=%d",strFileName,mmr);

		mmioClose(m_hmmio,0);	

		if (m_pwfx) delete m_pwfx;

		return E_FAIL;
	}

	if (0 != mmioGetInfo(m_hmmio,&mmioinfoIn,0))
	{

		this->WriteErrorFile("DD_Load_Sound : errore durante l'acquisizione delle informazioni sul chunk dati del file %s",strFileName);

		mmioClose(m_hmmio,0);

		if (m_pwfx) delete m_pwfx;

		return E_FAIL;
	}

	UINT cbDataIn = dwDSLockedBufferSize; 
    
    //se i byte da leggere sono maggiori delle dimensioni del chunk allora li rende uguali alle dim. del chunk
	if (cbDataIn > ckIn.cksize) cbDataIn=ckIn.cksize;
        
	ckIn.cksize -= cbDataIn;
    
  //copia i dati dall file io al buffer sonoro
	
	for( DWORD cT = 0; cT < cbDataIn; cT++ )
    {
           
            if( mmioinfoIn.pchNext == mmioinfoIn.pchEndRead )
            {
                if( 0 != mmioAdvance( m_hmmio, &mmioinfoIn, MMIO_READ ) )
				{

					this->WriteErrorFile("DD_Load_Sound : errore durante l'avanzamento nel file I/O %s",strFileName);

					mmioClose(m_hmmio,0);

					if (m_pwfx) delete m_pwfx;

					return E_FAIL;
				}

				if( mmioinfoIn.pchNext == mmioinfoIn.pchEndRead )
				{

					this->WriteErrorFile("DD_Load_Sound : errore durante il ciclo di lettura dei dati dal file %s nel buffer sonoro.",strFileName);

					mmioClose(m_hmmio,0);

					if (m_pwfx) delete m_pwfx;

					return E_FAIL;
				}
			}

			//copia
			*((BYTE *)pDSLockedBuffer+cT) = *((BYTE*) mmioinfoIn.pchNext);
			//avanza la posizione da leggere
            mmioinfoIn.pchNext++; 

	}//fine for


	if( 0 != mmioSetInfo( m_hmmio, &mmioinfoIn, 0 ) )

		{

			this->WriteErrorFile("DD_Load_Sound : errore durante l'aggiornamento delle informazioni nella struttura MMIOINFO del file %s",strFileName);


			mmioClose(m_hmmio,0);

			if (m_pwfx) delete m_pwfx;

			return E_FAIL;
		}

        //se il file WAV � vuoto riempie il buffer di zeri (silenzio)
	if (cbDataIn==0) 
	{
			FillMemory( (BYTE*) pDSLockedBuffer,dwDSLockedBufferSize,
			(BYTE )(m_pwfx->wBitsPerSample == 8 ? 128 : 0) );

	}
	else if ((DWORD)cbDataIn < dwDSLockedBufferSize)
	{
			//se i dati letti sono minori delle dimensioni del buffer riempie lo spazio che avanza con silenzio
			FillMemory( (BYTE*) pDSLockedBuffer + cbDataIn,
				        dwDSLockedBufferSize - (DWORD) cbDataIn,
						(BYTE)(m_pwfx->wBitsPerSample == 8 ? 128 : 0) );
	}

	lpdsb->Unlock( pDSLockedBuffer, dwDSLockedBufferSize, NULL, 0);    
	        
    
    mmioClose(m_hmmio,0);
    if (m_pwfx) delete m_pwfx;

    //tutto OK!
    return S_OK;

}

//--------------------- FreeSound ---------------------------------

//Rimuove un suono dal vettore dei suoni
//numBuff � l'indice che identifica il suono all'interno del vettore

BOOL CADXSound::FreeSound(LONG numBuff)
{
    
	int count;
	LPDIRECTSOUNDBUFFER lpDS;

    if ((LONG)numBuff >= MAXSOUNDS || numBuff<0) return FALSE;

	if (m_sndArray[numBuff].iNumBuffers > 0)
	{
		for (count=0;count<m_sndArray[numBuff].iNumBuffers;count++)
		{

			lpDS=m_sndArray[numBuff].lpDSBuffers[count];

			if (lpDS) lpDS->Release();

			lpDS=NULL;

		}

		m_sndArray[numBuff].iNumBuffers=0;

		delete[] m_sndArray[numBuff].lpDSBuffers;

		return TRUE;
	
	}

	else return FALSE;
	
}


//---------------------------- FreeAllSounds ---------------------------
//Rilascia tutti i file wav caricati
void CADXSound::FreeAllSounds(void)
{
	DWORD dwCnt;	

	for (dwCnt=0;dwCnt<MAXSOUNDS;dwCnt++)
	{
		this->FreeSound(dwCnt);
	}

}
//------------------------ RestoreBuffer -------------------------------

//Ripristina il buffer se � stato perso 

BOOL CADXSound::RestoreBuffer( LPDIRECTSOUNDBUFFER pDSB,BOOL *pbWasRestored)
{

    
    HRESULT hr;
    DWORD dwStatus;
    
	if( pDSB == NULL )
        return FALSE;   
    
    if( pbWasRestored )
        *pbWasRestored = FALSE; 

    if( FAILED( hr = pDSB->GetStatus( &dwStatus ) ) )
	{

		this->WriteErrorFile("RestoreBuffer : errore durante l'acquisizione dello stato del buffer sonoro.");

		return FALSE;    
	}    

    if( dwStatus & DSBSTATUS_BUFFERLOST )
    {
        // Since the app could have just been activated, then
        // DirectSound may not be giving us control yet, so 
        // the restoring the buffer may fail.  
        // If it does, sleep until DirectSound gives us control.
        do 
        {
            hr = pDSB->Restore();
            if( hr == DSERR_BUFFERLOST )
                Sleep( 10 );
        }
        while( hr = pDSB->Restore() );

        if( pbWasRestored != NULL )
            *pbWasRestored = TRUE;

        return TRUE;
    }
    else
    {
        return FALSE;
    }



}

//---------------------- IsPlaying --------------------------------
//Restituisce true se il suono � in esecuzione

BOOL CADXSound::IsPlaying(LONG dwBuffIndex)
{

	DWORD dwStatus;
	SOUND_STRUCT_PTR sndPtr=NULL;

	if (dwBuffIndex > MAXSOUNDS || dwBuffIndex == -1) return FALSE;

	sndPtr=&m_sndArray[dwBuffIndex];

	if (sndPtr && sndPtr->iNumBuffers == 0) return FALSE;	
	
	//cerca il primo buffer non in esecuzione
	for (int count=0;(LONG)count<sndPtr->iNumBuffers;count++)
	{

		dwStatus=0;
		
		sndPtr->lpDSBuffers[count]->GetStatus( &dwStatus );
        if ( ( dwStatus & DSBSTATUS_PLAYING ) == 0 )
		{
		    return TRUE;			
		}

	}

	return FALSE;
}


//---------------------- PlaySound ---------------------------------
//suona un file wav precedentemente caricato
//iBuffIndex indica l'indice del buffer sonoro caricato precedentemente
//che verr� suonato
//dwPriority � la priorit� dell'effetto sonoro se � stato creato con il flag DSBCAPS_LOCDEFER 
//questo flag permette di specificare se il buffer � un buffer software o hardware
//dwFalgs:
//una combinazione di:
//DSBPLAY_LOOPING fa ripartire il suono dall'inizio (loop)
//DSBPLAY_LOCHARDWARE forza il suono ad essere suonato su un buffer hardaware, se nessun buffer � disponibile fallisce
//DSBPLAY_LOCSOFTWARE forza il suono su un buffer software
//vedere la guida per le altre opzioni



BOOL CADXSound::PlaySound(LONG dwBuffIndex,DWORD dwPriority, DWORD dwFlags)
{
	SOUND_STRUCT_PTR sndPtr;
    DWORD dwStatus=0;
    DWORD count;
	BOOL bFound=FALSE;
    BOOL bRestored;
    HRESULT hr;
    LPDIRECTSOUNDBUFFER lpCBuff;

	if (dwBuffIndex > MAXSOUNDS || dwBuffIndex == -1)
	{

		this->WriteErrorFile("PlaySound: il buffer %d non � contenuto nel vettore dei buffer sonori.",dwBuffIndex);				

		return FALSE;
	} 	

   
	sndPtr=&m_sndArray[dwBuffIndex];

	if (sndPtr->iNumBuffers == 0)
	{
	
		this->WriteErrorFile("PlaySound : il buffer  %d non � caricato.",dwBuffIndex);		
		
		return (FALSE);
	}

	//cerca il primo buffer non in esecuzione
	for (count=0;(LONG)count<sndPtr->iNumBuffers;count++)
	{

		dwStatus=0;
		
		sndPtr->lpDSBuffers[count]->GetStatus( &dwStatus );
        if ( ( dwStatus & DSBSTATUS_PLAYING ) == 0 )
		{
		    bFound=TRUE;
			break;
		}

	}
        
	if (!bFound) 
	{

		this->WriteErrorFile("PlaySound : Nessun buffer libero nella struttura n� %d",dwBuffIndex);

		return FALSE;

	}
    
    lpCBuff=sndPtr->lpDSBuffers[count];
    bRestored=FALSE;
	//RestoreBuffer(lpCBuff,&bRestored);

	if (bRestored)
	{
		//il buffer � stato ripristinato e quindi va di nuovo riempito con i dati del file WAV
		if (FAILED(DD_Load_Sound(sndPtr->strFileName,&lpCBuff)))
		{
			return FALSE;
		}

	}
    
   	
	hr=m_sndArray[dwBuffIndex].lpDSBuffers[count]->Play(0,dwPriority,dwFlags);
	
	return (!FAILED(hr));

}


//-------------------------- StopSound -------------------------------
//interrompe l'esecuzione di un buffer sonoro

BOOL CADXSound::StopSound(DWORD numBuffer)
{

	SOUND_STRUCT_PTR sndPtr;
	DWORD            count;

	if ((numBuffer<0) || (numBuffer>MAXSOUNDS)) return FALSE;

    sndPtr=&m_sndArray[numBuffer];
    //ferma tutti i buffers
	for (count=0;(LONG) count<sndPtr->iNumBuffers;count++)
	{
		if (NULL != sndPtr->lpDSBuffers[count]) sndPtr->lpDSBuffers[count]->Stop();
	
	}

	return TRUE;

}

//--------------------- ShutDown ----------------------------------

void CADXSound::ShutDown(void)
{
	if (!m_iStatus) return;

	m_iStatus=0;
	FreeAllSounds();
    //rilascia l'oggetto pricipale
	if (m_lpds) m_lpds->Release();

}

//-------------------- GetStatus ---------------------------------

int CADXSound::GetStatus(void)
{
	return m_iStatus;
}

//---------------------------------------------------------------------
//                     CADXMusic  
//---------------------------------------------------------------------

//costruttore

CADXMusic::CADXMusic()
{
	//oggetto performance principale
	
	m_pPerf=NULL;
	//oggetto loader usato per caricare i file musicali da disco
	
	m_pLoader=NULL;
    //segmento MIDI
    
	m_pMIDISeg=NULL;
    //stato del segmento midi

    m_pSegState=NULL;

	m_iStatus=0;


}

//distruttore

CADXMusic::~CADXMusic()
{
	m_iStatus=0;


}

//--------------------------- Init ---------------------------------

HRESULT CADXMusic::Init(HWND hwnd)
{

	HRESULT hr;
	//inizializza le interfacce COM
	hr=CoInitialize(NULL);
    
	if (FAILED(hr))
	{


		this->WriteErrorFile("Init_Music: impossibile inizializzare le inerfacce COM.");        
        
		return hr;

    }

    //crea l'oggetto performance principale
	m_pPerf = CreatePerformance();

    if (m_pPerf == NULL)
    {

	    this->WriteErrorFile("DD_Init_Music: impossibile creare l'oggetto performance principale.");        

		return E_FAIL;

    }

    //inizializza l'oggetto performance principale
	hr = m_pPerf->Init(NULL, NULL, NULL);

	if (FAILED(hr))

    {

	    this->WriteErrorFile("Init_Music: errore durante l'inizializzazione dell'oggetto performance principale.");        

		return hr; 	
	}
	
	//aggiunge una porta all'oggetto performance
	//passando NULL aggiunge la porta di default (Microsoft Software Synthesizer)
	hr=m_pPerf->AddPort(NULL);

	if (FAILED(hr))
    {

	    this->WriteErrorFile("DD_Init_Music: errore durante l'aggiunta della porta all'oggetto performance principale principale.");        

		return (FALSE);
	}    

	//crea l'oggetto loader principale
	m_pLoader = CreateLoader();
    
	if (m_pLoader == NULL)
    {

	    this->WriteErrorFile("DD_Init_Music: errore durante la creazione dell'oggetto loader.");        

		return (FALSE);
        
    }

	m_iStatus=1;

    return (TRUE);

}

//------------------------- CreatePerformance ------------------------------

//funzione usata per creare l'oggetto performance principale

IDirectMusicPerformance* CADXMusic::CreatePerformance(void)
{
    IDirectMusicPerformance* pPerf;
 
    if (FAILED(CoCreateInstance(
            CLSID_DirectMusicPerformance,
            NULL,
            CLSCTX_INPROC, 
            IID_IDirectMusicPerformance2,
            (void**)&pPerf
        )))
    {
        pPerf = NULL;
    }
    return pPerf;
}

//---------------------------- CreateLoader -----------------------------------------
//crea l'oggetto loader necessario per caricare da disco i file musicali
IDirectMusicLoader* CADXMusic::CreateLoader(void)
{
    IDirectMusicLoader* pLoader;
 
    if (FAILED(CoCreateInstance(
            CLSID_DirectMusicLoader,
            NULL,
            CLSCTX_INPROC, 
            IID_IDirectMusicLoader,
            (void**)&pLoader
        )))
    {
        pLoader = NULL;
    }
    return pLoader;
}

//----------------------------------------------- LoadMIDISegment ------------------------------------ 
//carica un segmento con i dati musicali di un file MIDI
//wszSearchDir � la directori che contiene il file MIDI se � nullo lo cerca nella dir.
//corrente

IDirectMusicSegment* CADXMusic::LoadMIDISegment(IDirectMusicLoader* pLoader,WCHAR *wszMidiFileName,char *szSearchDir)
{
    
	HRESULT hr;
	DMUS_OBJECTDESC ObjDesc; 
    //segmento 
	IDirectMusicSegment* pSegment = NULL;
	//_MAX_PATH � definito nella lib. standard stdlib.h
    char szDir[_MAX_PATH];
    WCHAR wszDir[_MAX_PATH];   
    
	if  (szSearchDir == NULL) 
	{
		//dir. corrente
		if(_getcwd( szDir, _MAX_PATH ) == NULL)
        {
           return NULL;
        }
    }   
    else
	{   //copia in szDir la directory di ricerca passata
		strcpy(szDir,szSearchDir);
	}
   
   
    //converte da multibyte a Unicode
    MULTI_TO_WIDE(wszDir, szDir);
    
    //imposta nel loader la directory per la ricerca
    hr = pLoader->SetSearchDirectory(GUID_DirectMusicAllTypes,
        wszDir, FALSE);
    if (FAILED(hr)) 
    {   

		this->WriteErrorFile("LoadMidiSegment : errore impostando la directory di ricerca del file: dir=%ls",wszDir);

        return NULL;
    }
    //occorre ora descrivere l'oggetto da caricare nella struttura DMUS_OBJECTDESC
	    
    ObjDesc.guidClass = CLSID_DirectMusicSegment;
    ObjDesc.dwSize = sizeof(DMUS_OBJECTDESC);
    //copia la stringa univode dentro la stringa nome file della struttura
	wcscpy( ObjDesc.wszFileName, wszMidiFileName );
	//imposta i membri validi
    ObjDesc.dwValidData = DMUS_OBJ_CLASS | DMUS_OBJ_FILENAME;
    //Now load the object and query it for the IDirectMusicSegment interface. This is done in a single call to IDirectMusicLoader::GetObject. Note that loading the object also initializes the tracks and does everything else necessary to get the MIDI data ready for playback.
   
    hr=pLoader->GetObject(&ObjDesc,IID_IDirectMusicSegment2, (void**) &pSegment);
    
	if (FAILED(hr)) 
	{

		this->WriteErrorFile("LoadMidiSegment: errore durante l'acquisizione dell'interfaccia da parte dell'oggetto loader");

		return NULL;
	}	
	//per assicurarsi che il segmento sia un file MIDI standard occorre settare un parametro sul "band track".
    
    //The next step is to download the instruments. This is necessary even for playing a simple MIDI file, because the default software synthesizer needs the DLS data for the General MIDI instrument set. If you skip this step, the MIDI file will play silently. Again, you call SetParam on the segment, this time specifying the GUID_Download parameter:
    //passando -1 (0xFFFFFFFF) si settano tutti i gruppi
    hr=pSegment->SetParam(GUID_Download, 0xFFFFFFFF , 0, 0, (void*)m_pPerf);    

	//Note that there's no harm in requesting the download even though this might already have been done in a previous call to the LoadMIDISegment function. A redundant request is simply ignored. Eventually you have to unload the instruments, but that can wait until you're ready to shut down DirectMusic.
    if (FAILED(hr)) 
	{

		this->WriteErrorFile("LoadMidiSegment: m_pMIDISeg->SetParam");

		return NULL;
	} 

    //restituisce il segmento pronto per essere suonato
    return pSegment;

} // End of LoadSegment()

//------------------------------------------- LoadMIDI ---------------------------------------------
//carica il file MIDI
//wszMidiFileName=nome file midi
//szSearchDir=directory in cui cercare il file MIDI

BOOL CADXMusic::LoadMIDI(char *szMidiFileName,char *szSearchDir)
{ 

	WCHAR wszName[_MAX_PATH];

	if (m_iStatus==1)

	{
		
		if (!(m_pLoader && m_pPerf))
		{

			this->WriteErrorFile(TEXT("LoadMIDI : i parametri passati non sono validi."));        

			return (FALSE);
		}

		else if (m_iStatus==0)
		{
			this->WriteErrorFile(TEXT("LoadMIDI : l'oggetto CADXMusic non � stato inizializzato."));

			return (FALSE);

		}
    
		MULTI_TO_WIDE(wszName,szMidiFileName);    
    
		//parametri validi : carica il segmento
		m_pMIDISeg=LoadMIDISegment(m_pLoader,wszName,szSearchDir);

		if (!m_pMIDISeg) return E_FAIL;
		
		//imposta il parametro indicando che � un MIDI standard
		if (FAILED(m_pMIDISeg->SetParam(GUID_StandardMIDIFile,-1,0,0,(void *)m_pPerf))) return E_FAIL;

		//download degli strumenti
		if (FAILED(m_pMIDISeg->SetParam(GUID_Download,-1,0,0,(void *)m_pPerf))) return E_FAIL;
    
		return (m_pMIDISeg != NULL);

	}

	else return NULL;
}

//---------------------- PlayMIDI ---------------------------------------------

//suona il segmento midi caricato
//dwRepeats:imposta il numero di volte che deve essere ripetuto il segmento MIDI
//se dwRepeats � zero viene suonato solo una volta
//se invece si setta su DMUS_SEG_REPEAT_INFINITE  il segmento viene ripetuto
//all'infinito fino a che non viene bloccato volutamente.


BOOL CADXMusic::PlayMIDI(DWORD dwRepeats)
{

	if (m_pMIDISeg)
	{
		if (FAILED(m_pMIDISeg->SetRepeats(dwRepeats))) return FALSE;
		m_pPerf->PlaySegment(m_pMIDISeg, 0, 0, &m_pSegState);
		return (TRUE);
	}
    else
	{

		this->WriteErrorFile("PlayMIDI : il segmento midi non � stato caricato o non � valido.");

		return (FALSE); 
	}

}

//----------------------------- ShutDown -----------------------------

void CADXMusic::ShutDown(void)
{
	if (this->GetStatus() == 1)
	{
		 //interrompe eventuali esecuzioni
		m_pPerf->Stop( NULL, NULL, 0, 0 );
 
		//scarica gli strumenti 
		if (m_pMIDISeg) 
		{
			m_pMIDISeg->SetParam(GUID_Unload, 0xFFFFFFFF, 0, 0, (void*)m_pPerf);
 
			//rilascia il segmento
			m_pMIDISeg->Release();
		}
 
		// chiude e rilascia la performance
		m_pPerf->CloseDown();
		m_pPerf->Release();
 
		//rilascia l'oggetto loader 
		//nb in alcuni casi si ottiene un'eccezione (esempio in modalit� windowed)
	//	m_pLoader->Release();
 
		//rilascia le interfacce COM
		CoUninitialize();

		m_iStatus=0;
	}
}

//-------------------- FreeMIDI ---------------------------------
//Libera il segmento midi se � stato caricato
void CADXMusic::FreeMIDI(void)
{
	if (m_pMIDISeg && m_iStatus==1)
	{
		//interrompe eventuali esecuzioni
    m_pPerf->Stop( NULL, NULL, 0, 0 );
 
    //scarica gli strumenti 
    m_pMIDISeg->SetParam(GUID_Unload, 0xFFFFFFFF, 0, 0, (void*)m_pPerf);
 
    //rilascia il segmento
    m_pMIDISeg->Release();
	}

}


//------------------------ StopMIDI --------------------------------
void CADXMusic::StopMIDI(void)
{
	if (m_pMIDISeg && m_iStatus==1)
	{
		//interrompe eventuali esecuzioni
		m_pPerf->Stop( NULL, NULL, 0, 0 );	
	}

}
//-------------------------- GetStatus --------------------------

int CADXMusic::GetStatus(void)
{
	return m_iStatus;
}

