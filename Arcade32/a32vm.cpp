/*

   ARCADE 32 VIRTUAL MACHINE
   Code by Leonardo Berti 2004 (c)
   Macchina virtuale usata per l'esecuzione di script

    N.B. questo codice deve essere mantenuto cross-platform : usare solo la libreria standard del C

   Lo scopo � quello di fornire la libreria arcade32 con un semplice script 
   con sintassi simile a quella del C da usare nei giochi.
   Il linguaggio deve avere le seguenti caratteristiche (per il momento):
   1]Sintassi simile a quella del C
   2]deve consetire di definire delle variabili e potergli assegnare un valore sia al momento della definizione che in seguito
   3]deve avere come tipi base int,string e float
   4]deve consetire di definire agevolmente dei buffer dinamici (vettori) che siano autoridimensionanti
   5]deve consentire la chiamata a funzioni che poi vengono interpretate dal programma chiamante
    
    Per ora non � previsto un expression parser e neanche la possbilit� di definire strutture decisionali if/else ecc...

    Esempio di script:

    int valore1=190;
	int valore2=199;
	valore1=90;
	int prova[]={1,2,3,4,5
	             6,7,8,9,0};
    prova[4]=12;
	prova[5]=120;
	string testo="prova stringa";
	string a[]={"uno","due","tre"};

    Alla fine della compilazione viene creata una lista di "istruzioni":

*/   

/*
class CA32VirtualMachine
{
private:

	int m_state; //stato della mcchina virtuale
	size_t m_data_mem; //memoria globale
	size_t m_stackmem; //memoria riservata allo stack
	int m_ip; //instruction pointer
	int m_sp; //stack pointer

	SYM_TABLE_ITEM *sym_table;
	SYM_TABLE_ITEM *sym_table_local;
	A32INSTRUCTION *m_code; //codice
	unsigned char  *m_data; //dati
	unsigned char  *m_stack; //stack

	//registri
	int m_RINTA,m_RINTB,m_RINTC;
	float m_RFLTA,m_RFLTB,m_RFLTC;
	char *m_RSTRA,*m_RSTRB,m_RSTRC;

protected:

	void DeleteSymTable(SYM_TABLE_ITEM_PTR ptable);

public:

	CA32VirtualMachine(void);
	~CA32VirtualMachine(void);
	void Reset(void);
	void Reset(unsigned long memdata,unsigned long memcode,unsigned long memstack);
	//fa ripartire il programma dall'inizio
    void Restart(void); 
	int GetSymbolInt(int *dest,char *name);
	int GetSymbolString(char *dest,char *name); 
	int GetSymbolFloat(float *dest,char *name);
	//restituisce l'indirizzo di un certo simbolo
	int GetAddressBySymbol(char *sym);
	int Execute(int *opcode,int *op1,int *op2); //esegue una istruzione e rende il codice dell'operazione e gli operandi
	int Execute(void); //esegue l'istruzione corrente
	int GetInstrAddr(void); //indirizzo dell'istruzione corrente
};
*/

#include <stdio.h>
#include <string.h>
#include "a32vm.h"


CA32VirtualMachine::CA32VirtualMachine(void)
{
	sym_table=NULL;
	sym_table_start=NULL;	
	m_code=NULL;
	m_stack=NULL;
	m_data=NULL;	
	m_IP=0;
    m_data_mem=DATA_MEM_SIZE;
	m_stack_mem=STACK_MEM;
	m_code_mem=CODE_MEM;
	m_codetop=NULL;
	m_imax_instructions=0;
	m_iloaded_instructions=0;
	m_state=VM_NONE; //stato
	m_iExitCode=0;
	ResetVirtualCPU();
}

void CA32VirtualMachine::DeleteFnTable(void)
{

	for (DWORD i=0;i<MAX_EXT_FN;i++) m_fntable[i]=NULL;
	m_FN=0;
}


//rilascia la tabella dei simboli
void  CA32VirtualMachine::DeleteSymTable(SYM_TABLE_ITEM_PTR p)
{
	SYM_TABLE_ITEM_PTR p1;

	p1=p;

	if (p)
	{
		while(p && p->next)
		{
			p1=p->next;
			SAFE_DELETE(p->name); //cancella il nome del simbolo
			SAFE_DELETE(p);
			p=p1;
		}		

		SAFE_DELETE(p);
		sym_table_start=NULL;
	}
}

CA32VirtualMachine::~CA32VirtualMachine(void)
{
	SAFE_DELETE_ARRAY(m_code);
	SAFE_DELETE_ARRAY(m_data);
	SAFE_DELETE_ARRAY(m_stack);
	DeleteSymTable(sym_table_start);
	DeleteFnTable();
	m_state=VM_NONE;
}

//resetta con il valore corrente di memdata,memcode e memstack
void CA32VirtualMachine::Reset(void)
{
	Reset(m_data_mem,m_code_mem,m_stack_mem);
}

//Resetta la memoria rende 1 se va tutto bene 0 altrimenti
int CA32VirtualMachine::Reset(unsigned long memdata,unsigned long memcode,unsigned long memstack)
{
	DWORD count;

	if (memdata>0 && memcode>0 && memstack>0)
	{
		DeleteSymTable(sym_table); //cancella la sym table
		DeleteFnTable();
		ResetVirtualCPU();
		SAFE_DELETE_ARRAY(m_data);
		SAFE_DELETE_ARRAY(m_stack);
		SAFE_DELETE_ARRAY(m_code);
 
		//azzera la tabella delle funzioni esterne
		for (count=0;count<MAX_EXT_FN;count++) m_fntable[count]=NULL;

		m_data_mem=memdata;
		m_code_mem=memcode;
		m_stack_mem=memstack;

		//azzera i vari tipi di memoria
		m_data=new BYTE[memdata];
		memset(m_data,0,memdata);
		m_codetop=m_data;
		m_stack=new BYTE[memstack];
		memset(m_stack,0,memstack);		
		m_DP=0;
		m_SP=0;	
		m_FN=0;
		m_state=VM_INITIALIZED;
		return 1;

	}

	return 0;
}

//resetta la cpu virtuale
void CA32VirtualMachine::ResetVirtualCPU(void)
{
	m_SP=0;m_DI=0;m_IP=0;m_AX=0;m_BX=0;m_CX=0;m_DX=0;m_SI=0;
	m_ST0=m_ST1=m_ST2=m_ST3=0;
	m_f1=m_f2=m_f3=0;
}

//rende il numero massimo di istruzioni caricabili
int CA32VirtualMachine::GexMaxInstructions(void) 
{
	return m_imax_instructions;
}

int CA32VirtualMachine::GetLoadedInstructions(void) //rende il numero di istruzioni del programma caricato
{
	return m_iloaded_instructions;
}

/*	int AddValue(int value);
	int AddValue(float value);
	int AddValue(BYTE *value);
*/

//aggiunge un simbolo  nella sym table
int CA32VirtualMachine::AddSymbol(char *name)
{
	int llen;
	if (!name) return 0;
	if (m_state<VM_INITIALIZED) return 0;

	if (sym_table) 
	{
		//aggiunge un elemento alla linked list
		sym_table->next=new SYM_TABLE_ITEM;
		sym_table=sym_table->next;
	}
	else 
	{
		sym_table=new SYM_TABLE_ITEM; //inizializza la sym table
		sym_table_start=sym_table; //imposta il puntatore di inizio lista
	}

	llen=strlen(name)+1;
	sym_table->name=new char[llen];
	memset(sym_table->name,0,llen);
	memcpy(sym_table->name,name,llen);
	sym_table->size=0;
	sym_table->address=0; //indirizzo in cui � stato inserito il valore		
	sym_table->next=NULL; //resetta il puntatore all'elemento successivo
	return 1;
	
}

int CA32VirtualMachine::AddSymbol(int value,char *name)
{
	if (AddSymbol(name))
	{
		if (AddValue(value)) //aggiunge il valore e incrementa DP
		{
			sym_table->address=m_DP-sizeof(int);				
			sym_table->size=sizeof(int);
			sym_table->type=TP_INT;
			return 1;
		}
		else return 0;
	}

	else return 0;
}

int CA32VirtualMachine::AddSymbol(float value,char *name)
{
	if (AddSymbol(name))
	{
		if (AddValue(value)) //aggiunge il valore e incrementa DP
		{
			sym_table->address=m_DP-sizeof(float);				
			sym_table->size=sizeof(float);
			sym_table->type=TP_FLOAT;
			return 1;
		}
		else return 0;
	}

	else return 0;
	return 1;
}

int CA32VirtualMachine::AddSymbol(char *buff,char *name)
{
	if (AddSymbol(name))
	{
		if (AddValue((BYTE *)buff)) //aggiunge il valore e incrementa DP
		{
			sym_table->address=m_DP-strlen(buff)*sizeof(char);				
			sym_table->size=strlen(buff)*sizeof(char);
			sym_table->type=TP_STRING;
			return 1;
		}
		else return 0;
	}

	else return 0;
}

//aggiunge un valore nella memoria
int CA32VirtualMachine::AddValue(int value)
{
	if ((size_t)(m_DP+sizeof(int))<m_data_mem)
	{
		memcpy(m_data+m_DP,&value,sizeof(int));
		m_DP += sizeof(int);
		return 1;
	}

	return 0;
}


int CA32VirtualMachine::AddValue(float value)
{
	if ((size_t)(m_DP+sizeof(float))<m_data_mem)
	{
		memcpy(m_data+m_DP,&value,sizeof(float));
		m_DP += sizeof(float);
		return 1;
	}

	return 0;
}

int CA32VirtualMachine::AddValue(BYTE *value)
{
	size_t len;

	len=strlen((char *)value)*sizeof(BYTE);

	if ((size_t)(m_DP+len)<m_data_mem)
	{
		memcpy(m_data+m_DP,value,len);
		m_DP += len;
		return 1;
	}

	return 0;
}

//restituisce in addr l'indirizzo di memoria di un simbolo
int CA32VirtualMachine::GetSymbol(SYM_TABLE_ITEM *symbol,char *name) //restituisce l'indirizzo di un simbolo
{
	SYM_TABLE_ITEM_PTR psym=sym_table_start;
	if (!name) return NULL;

	//percorre a ritroso la sym table alla ricerca del simbolo
	while (psym)
	{
		if (!strcmp(psym->name,name))
		{
			//ha trovato il simbolo
			memcpy(symbol,psym,sizeof(SYM_TABLE_ITEM));
			return 1;
		}
		//si sposta sull'elemento successivo
		psym=psym->next;
	}	

	return 0;
}

int CA32VirtualMachine::SetSymbol(char *symbol,int value,DWORD offset)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,symbol))
	{
		if (itm.type==TP_INT)
		{
			//copia il nuovo valore in memoria
			memcpy(m_data+itm.address+offset*sizeof(int),&value,itm.size);
			return 1;
		}
		else return 0; //type mismatch
	}
	else return 0; //non trovato
}

int CA32VirtualMachine::SetSymbol(char *symbol,float value,DWORD offset)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,symbol))
	{
		if (itm.type==TP_FLOAT)
		{
			//copia il nuovo valore in memoria
			memcpy(m_data+itm.address+offset*sizeof(float),&value,itm.size);
			return 1;
		}
		else return 0; //type mismatch
	}
	else return 0; //non trovato

}

int CA32VirtualMachine::SetSymbol(char *symbol,BYTE *buff,DWORD offset)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,symbol))
	{
		if (itm.type==TP_STRING)
		{
			//copia il nuovo valore in memoria
			memcpy(m_data+itm.address+offset*sizeof(BYTE),buff,itm.size);
			return 1;
		}
		else return 0; //type mismatch
	}
	else return 0; //non trovato
}


int CA32VirtualMachine::GetAddressBySymbol(char *name)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,name))
	{
		return itm.address;
	}
	else return -1; //non trovato

}

int CA32VirtualMachine::GetSymbolInt(int *dest,char *name)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,name))
	{
		if (itm.type==TP_INT)
		{
			memcpy(dest,m_data+itm.address,itm.size);
			return 1;
		}
		else return 0; //tipo non corrsipondente

	}
	else return 0; //non trovato
}

int CA32VirtualMachine::GetSymbolString(char **dest,char *name)
{
	SYM_TABLE_ITEM itm;	

	if (GetSymbol(&itm,name))
	{
		if (itm.type==TP_STRING)
		{
			SAFE_DELETE_ARRAY(*dest); //attenzione: dest deve essere allocato dinamicamente
			*dest=new char[itm.size+1];
			memset(*dest,0,itm.size+1);
			memcpy(*dest,m_data+itm.address,itm.size);
			return 1;
		}
		else return 0; //tipo non corrsipondente

	}
	else return 0; //non trovato
}

int CA32VirtualMachine::GetSymbolFloat(float *dest,char *name)
{
	SYM_TABLE_ITEM itm;
	if (GetSymbol(&itm,name))
	{
		if (itm.type==TP_FLOAT)
		{
			memcpy(dest,m_data+itm.address,itm.size);
			return 1;
		}
		else return 0; //tipo non corrsipondente

	}
	else return 0; //non trovato
}

//aggiunge un puntatore ad una funzione esterna che viene chiamata dalla macchina virtuale
int CA32VirtualMachine::AddExtFnCall(void (*fn)(void))
{
	if (m_FN>=MAX_EXT_FN-1) return 0; //non c'� piu' spazio per aggiungere funzioni
    
	m_fntable[m_FN++]=fn; //aggiunge il riferimento

	return 1;

}

//fa ripartire il codice dalla prima istruzione
void CA32VirtualMachine::Restart(void)
{
	if (m_state==VM_ERROR) m_state=VM_CODE_LOADED;
	ResetVirtualCPU();
	m_iExitCode=0;
}


int CA32VirtualMachine::GetExitCode(void)
{
	return m_iExitCode;
}

//esegue il programma sulla CPU virtuale
//rende 0 se il programma viene arrestato o finisce
//rende 1 altrimenti
int CA32VirtualMachine::Execute(void)
{
	BYTE opcode;
	BYTE *pcode;
	BYTE op1,op2; //operandi
	BYTE *addr1,*addr2;
	LONG offs1;
	size_t nsize1,nsize2; 
	size_t bysize=sizeof(BYTE); 

	if (m_state<VM_CODE_LOADED) return 0;

	if (m_state==VM_ERROR) return 0;	
	
	m_state=VM_RUNNING;

	pcode=m_code; //punta il codice eseguibile

	//azzera il flag di errore
	SetError(NULL,0);

	while(1)
	{

		//prende l'istruzione corrente
		opcode=(BYTE)*(pcode);

		switch(opcode)
		{

		case OP_NOP:  //nessuna operazione

			pcode+=bysize;
			break;

		case OP_MOV: //sposta dati

			//determina il tipo di operandi
			op1=(BYTE)*(pcode+bysize); //destinazione 
			op2=(BYTE)*(pcode+2*bysize); //sorgente

			if (op2==R_IMMINT)
			{
				switch(op1)
				{

				case R_IMMADDR:
				
					//mov [addr],[immint] sposta un valore intero immediato in un indirizzo di memoria
					
					offs1=(LONG)*(pcode+3*bysize);
					addr2=pcode+3*bysize+sizeof(LONG);

					memcpy(m_data+offs1,addr2,sizeof(int));
					pcode += (3*bysize+sizeof(LONG)+sizeof(int));
				break;

				case R_IMMINT:
				case R_IMMFLOAT:
				case R_ST0:
				case R_ST1:					
			    case R_ST2:
                case R_ST3:

					//invalid operand
					return 0;
				break;

				default:

					//mov [register],[immint] //sposta un valore immediato intero in un registro

					addr1=GetOpAddress(op1,&nsize1);
					if (!addr1)
					{
						return 0;
					}

					addr2=pcode+3*bysize;
					memcpy(addr1,addr2,sizeof(int));
					pcode+=3*bysize+sizeof(int);
					break;
					
				}

			}

			else if(op2==R_IMMFLOAT)
			{
				switch(op1)
				{

				case R_IMMADDR:
				
					//mov [addr],[immfloat] sposta un valore intero immediato in un indirizzo di memoria
					
					offs1=(LONG)*(pcode+3*bysize);
					addr2=pcode+3*bysize+sizeof(LONG);

					memcpy(m_data+offs1,addr2,sizeof(float));
					pcode += (3*bysize+sizeof(LONG)+sizeof(int));
					break;

				case R_IMMINT:
				case R_IMMFLOAT:
				case R_AX:
				case R_BX:
				case R_CX:
				case R_DX:

					//invalid operand
					return 0;
					break;

				default:

					//mov [register],[immint] //sposta un valore immediato intero in un registro

					addr1=GetOpAddress(op1,&nsize1);
					if (!addr1)
					{
						return 0;
					}

					addr2=pcode+3*bysize;
					memcpy(addr1,addr2,sizeof(float));
					pcode+=3*bysize+sizeof(float);
					break;
					
				}
			}

			else
			{
		
				//mov [register][register]

				//acquisisce gli indirizzi per lo spostamento della memoria
				addr1=GetOpAddress(op1,&nsize1);
				addr2=GetOpAddress(op2,&nsize2);

				if (!addr1 || !addr2)
				{
						SetError("Impossibile copiare la memoria",1);
						return 0;
				}

				memcpy(addr1,addr2,nsize2);
			

				pcode+=(bysize*3); //incrementa l'instruction pointer
				break;
			}


		case OP_MOVSB: //sposta m_CX bytes da m_SI a m_DI

			if (m_CX<0)
			{
				return 0; //definire qui l'errore
			}

			addr1=m_data+m_DI;
			addr2=m_data+m_SI;
			memcpy(addr1,addr2,(size_t)m_CX);
			pcode+=(bysize*3); //incrementa l'instruction pointer
			break;

		case OP_PUSH:

			//determina il tipo di operandi
			op1=(BYTE)*(pcode+bysize); //destinazione 

			if (op1==R_IMMINT)
			{
				//push [immint]  immette un intero immediato nello stack
				memcpy(m_stack+m_SP,pcode+bysize*2,sizeof(int));
				//incrementa lo stack pointer
				m_SP += sizeof(int);

				if (m_SP>=m_stack_mem)
				{
					//out of stack
					return 0;
				}
				//incrementa l'instruction pointer
				pcode += bysize*2+sizeof(int);
				break;

			}
			else if (op1==R_IMMFLOAT)
			{
				//push [immint]  immette un intero immediato nello stack
				memcpy(m_stack+m_SP,pcode+bysize*2,sizeof(float));
				//incrementa lo stack pointer
				m_SP += sizeof(float);

				if (m_SP>=m_stack_mem)
				{
					//out of stack
					return 0;
				}

				//incrementa l'instruction pointer
				pcode += bysize*2+sizeof(float);
				break;
			}
			else if (op1==R_IMMADDR)
			{

				//invalid operand
				return 0;
			}
			else
			{
				//push [register] 
				addr1=GetOpAddress(op1,&nsize1);
				if (addr1)
				{
					memcpy(m_stack+m_SP,addr1,nsize1);
					m_SP+=nsize1;
					if (m_SP>=m_stack_mem)
					{
						//out of stack
						return 0;
					}
					pcode += bysize*2;

				}

				else
				{
					//invalid operand
					return 0;
				}

			}

			break;

		case OP_POP:

			//determina il tipo di operandi
			op1=(BYTE)*(pcode+bysize); //destinazione 

			switch(op1)
			{
			case R_IMMINT:
			case R_IMMFLOAT:
			case R_IMMADDR:

				//invalid instruction
				return 0;
				break;

			default:

				addr1=GetOpAddress(op1,&nsize1);
				if (!addr1)
				{
					//invalid operand
					return 0;
				}

				memcpy(addr1,m_stack+m_SP-nsize1,nsize1);
				m_SP-=nsize1; //bilancia lo stack
				pcode+=bysize*2;
				break;
			}

			break;
        
        //operzioni aritmetiche intere
		case OP_ADD:   //ADD op1,op2  op1=op1+op2
		case OP_MUL:   //MUL op1,op2  op1=op1*op2
		case OP_DIV:   //DIV op1,op2  op1=op1/op2
		case OP_SUB:   //SUB op1,op2  op1=op1-op2 

			int v1,v2;

			op1=(BYTE)(pcode+bysize);
			op2=(BYTE)(pcode+bysize*2);

			switch(op1)
			{
				case R_IMMINT:
				case R_IMMFLOAT:
				case R_IMMADDR:
				case R_ST0:
				case R_ST1:
				case R_ST2:
				case R_ST3:

					//invalid operand
					return 0;
				default:

					addr1=GetOpAddress(op1,&nsize1);

					if (!addr1)
					{
						return 0;
					}
					
					if (op2==R_IMMINT)
					{
						v1=(int)*addr1;
						v2=(int)*(pcode+bysize*2);

						switch(op1)
						{
						case OP_ADD:
							v1+=v2;
							break;
						case OP_SUB:
							v1-=v2;
							break;
						case OP_DIV:
							v1/=v2;
							break;
						case OP_MUL:
							v1*=v2;
							break;
						}

						memcpy(addr1,&v1,sizeof(int));
						pcode+=3*bysize+sizeof(int);
					}

					else if (op2==R_IMMFLOAT)
					{

						return 0;
					}
					else
					{

						addr2=GetOpAddress(op2,&nsize2);
						v2=(int)*addr2;

						switch(op1)
						{
						case OP_ADD:
							v1+=v2;
							break;
						case OP_SUB:
							v1-=v2;
							break;
						case OP_DIV:
							v1/=v2;
							break;
						case OP_MUL:
							v1*=v2;
							break;
						}

						memcpy(addr1,&v1,sizeof(int));
						pcode+=3*bysize;
					}

					break;
			}

			break;

			case OP_CALL:
				//chiama una funzione esterna alla VM (interfaccia fra VM e programma)
				op1=(BYTE)(pcode+bysize);
				int ifn=0;
				
				switch(op1)
				{
				case R_IMMFLOAT:

					return 0;
                case R_ST0:
				case R_ST1:
				case R_ST2:
				case R_ST3:
				case R_DI:
				case R_SI:				
				case R_SP:

					return 0;
					break;
			

				case R_IMMINT: //CALL [int]

					ifn=(int)*(pcode+bysize*2);
					pcode += (bysize*2+sizeof(int));
					break;
				default:

					addr1=GetOpAddress(op1,&nsize1);

					if (!addr1) return 0;

					ifn=(int)*addr1; //indice funzione //CALL [register]
					
				}

				if (ifn>=0 && ifn<(int)m_FN)
				{
					//chiama la funzione  della tabella funzioni
					if (m_fntable[ifn]) m_fntable[ifn]();
				}

				break;

		}//fine switch

	}//fine while

	return 1;
}

//restituisce l'indirizzo di memoria effettivo di un registro o l'indirizzo puntato da DI o SI
BYTE *CA32VirtualMachine::GetOpAddress(BYTE op,size_t *nsize)
{
	switch(op)
	{
	case R_AX:

		*nsize=sizeof(LONG);
		return (BYTE *)&m_AX;

	case R_BX:

		*nsize=sizeof(LONG);
		return (BYTE *)&m_BX;

	case R_CX:

		*nsize=sizeof(LONG);
		return (BYTE *)&m_CX;

	case R_DX:

		*nsize=sizeof(LONG);
		return (BYTE *)&m_DX;

	case R_SI:

		*nsize=sizeof(BYTE);
		return (m_data+m_SI);

	case R_DI:

		*nsize=sizeof(BYTE);
		return (m_data+m_DI);

	case R_SP:

		*nsize=sizeof(BYTE);
		return (m_stack+m_SP);

	case R_ST0:

		*nsize=sizeof(float);
		return (BYTE *)&m_ST0;

	//floating point
	case R_ST1:

		*nsize=sizeof(float);
		return (BYTE *)&m_ST1;

	case R_ST2:

		*nsize=sizeof(float);
		return (BYTE *)&m_ST2;

    
	case R_ST3:

		*nsize=sizeof(float);
		return (BYTE *)&m_ST3;
	}

	return NULL;
}

//esegue l'istruzione corrente 
int CA32VirtualMachine::ExecuteInstr(void)
{
	return 1;
}


//imposta l'errore corrente
void CA32VirtualMachine::SetError(const char *msg,const int icode)
{
	if (!msg) 
	{
		m_iErrorCode=0;
		SAFE_DELETE_ARRAY(m_szError);
		return;
	}

	int llen=strlen(msg);
	SAFE_DELETE_ARRAY(m_szError);
	m_szError=new char[llen+1];
	memset(m_szError,0,llen+1);
	memcpy(m_szError,msg,llen);
	m_iErrorCode=icode;
}

//restituisce l'ultimo errore
int CA32VirtualMachine::GetLastError(char **err,int *icode)
{
	if (m_iErrorCode)
	{
		if (*err)
		{
			SAFE_DELETE_ARRAY(*err);
			int llen=strlen(m_szError);
			*err=new char[llen+1];
			memset(*err,0,llen+1);
			memcpy(*err,m_szError,llen);			
		}		

		if (icode) *icode=m_iErrorCode;

		return 1;
	}

	else
	{
		SAFE_DELETE_ARRAY(err);
		if (icode) *icode=0;
		return 0; //non ci sono errori
	}
}

//aggiunge una istruzione
int CA32VirtualMachine::AddInstruction(BYTE opcode,BYTE op1,BYTE op2)
{
	if (m_state != VM_INITIALIZED) return 0;
	if ((m_codetop+sizeof(BYTE)*3-m_data)>=(LONG)m_code_mem) return 0; //out of memory

	m_codetop[0]=opcode;
	m_codetop[1]=op1;
	m_codetop[2]=op2;
	m_codetop+=sizeof(BYTE)*3;
	return 1;

}


int CA32VirtualMachine::AddInstruction(BYTE opcode,BYTE op1)
{
	if (m_state != VM_INITIALIZED) return 0;
	if ((m_codetop+sizeof(BYTE)*2-m_data)>=(LONG)m_code_mem) return 0; //out of memory

	m_codetop[0]=opcode;
	m_codetop[1]=op1;	
	m_codetop+=sizeof(BYTE)*2;
	return 1;	
}


int CA32VirtualMachine::AddInstruction(BYTE opcode)
{
	if (m_state != VM_INITIALIZED) return 0;
	if ((m_codetop+sizeof(BYTE)-m_data)>=(LONG)m_code_mem) return 0; //out of memory

	m_codetop[0]=opcode;	
	m_codetop+=sizeof(BYTE);
	return 1;
}

int CA32VirtualMachine::AddInstruction(BYTE opcode,BYTE op1,BYTE op2,int v1,int v2)
{
	if (m_state != VM_INITIALIZED) return 0;
	if ((m_codetop+sizeof(BYTE)*3+2*sizeof(int)-m_data)>=(LONG)m_code_mem) return 0; //out of memory

	m_codetop[0]=opcode;
	m_codetop[1]=op1;
	m_codetop[2]=op2;
	memcpy(m_codetop+3*sizeof(BYTE),&v1,sizeof(int));
	memcpy(m_codetop+3*sizeof(BYTE)+sizeof(int),&v2,sizeof(int));
	m_codetop+=sizeof(BYTE)*3+2*sizeof(int);
	return 1;

}
