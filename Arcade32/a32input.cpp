/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32INPUT.CPP
  funzioni di acquisizione input (wrapper x DIRECT INPUT)

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/

#define WIN32_LEAN_AND_MEAN   //se si inserisce questa definizione il file windows.h
#define DINPUT_BUFFERSIZE  16 //numero di elementi memorizzabili nel buffer del mouse

#include <windows.h>          //contiene definizioni base per windows
#include <windowsx.h>         //contiene la definizione di alcune macro importanti in windows
#include <winbase.h>          //funzioni del kernel
#include <stdio.h>
#include <mmsystem.h>
#include <malloc.h>

//nota: la seguente definizione � stata necessaria per 
//compilare con l'sdk directx 9
//utilizza la versione 7 di direct input
#define DIRECTINPUT_VERSION 0x0700

#include <dinput.h>           //include direct input

#ifdef _DEBUG_
#include <crtdbg.h>
#endif
#include "a32input.h"


//----------------------------- externals ----------------------------------

//extern char *m_szErrorFileName;
//extern char *szErr;
//--------------------------- globals ---------------------------------------

LONG g_dwDeviceCount=0;
DEVICE_INFO_PTR g_pDevices=NULL;
DEVICE_INFO_PTR g_pTop=g_pDevices;

LPDIRECTINPUTDEVICE g_pJoy=NULL;

//definisce la struttura per acquisire l'input bufferizzato dal mouse 
DIPROPDWORD dipdw =
    {
        // the header
        {
            sizeof(DIPROPDWORD),        // diph.dwSize
            sizeof(DIPROPHEADER),       // diph.dwHeaderSize
            0,                          // diph.dwObj
            DIPH_DEVICE,                // diph.dwHow
        },
        // the data
        DINPUT_BUFFERSIZE,              // dwData
    };

//------------------------------------------------------------------------
//                              CADXInput
//-------------------------------------------------------------------------

CADXInput::CADXInput(void)
{

	
	//inizializza le strutture che contengono l'input del mouse
	memset(&m_mouse_state,0,sizeof(DIMOUSESTATE));
	memset(&m_buffered_mouse_state,0,sizeof(DIMOUSESTATE));
	memset(&m_iASCII,0,sizeof(int));
	memset(&m_js,0,sizeof(m_js));

	//inizializza gli scan codes dei tasti
	//(la corrispondenza � creata solo per i tasti piu' importanti)
	m_iASCII[DIK_A]=65;
	m_iASCII[DIK_B]=66;
	m_iASCII[DIK_C]=67;
	m_iASCII[DIK_D]=68;
	m_iASCII[DIK_E]=69;
	m_iASCII[DIK_F]=70;
	m_iASCII[DIK_G]=71;
	m_iASCII[DIK_H]=72;
	m_iASCII[DIK_I]=73;
	m_iASCII[DIK_J]=74; 
	m_iASCII[DIK_K]=75; 
	m_iASCII[DIK_L]=76; 	
	m_iASCII[DIK_M]=77;
	m_iASCII[DIK_N]=78;
	m_iASCII[DIK_O]=79;
	m_iASCII[DIK_P]=80;
	m_iASCII[DIK_Q]=81;	
	m_iASCII[DIK_R]=82;
	m_iASCII[DIK_S]=83;
	m_iASCII[DIK_T]=84;
	m_iASCII[DIK_U]=85;
	m_iASCII[DIK_V]=86;
    m_iASCII[DIK_W]=87;
	m_iASCII[DIK_X]=88;
	m_iASCII[DIK_Y]=89;
	m_iASCII[DIK_Z]=90;
	m_iASCII[DIK_ESCAPE]=27;
	m_iASCII[DIK_F1]=112;
	m_iASCII[DIK_F2]=113;
	m_iASCII[DIK_F3]=114;
	m_iASCII[DIK_F4]=115;
	m_iASCII[DIK_F5]=116;
	m_iASCII[DIK_F6]=117;
	m_iASCII[DIK_F7]=118;
	m_iASCII[DIK_F8]=119;
	m_iASCII[DIK_F9]=120;
	m_iASCII[DIK_F10]=121;	
	m_iASCII[DIK_F11]=122;
	m_iASCII[DIK_F12]=123;
	m_iASCII[DIK_0]=48;
	m_iASCII[DIK_1]=49;
	m_iASCII[DIK_2]=50;
	m_iASCII[DIK_3]=51;	
	m_iASCII[DIK_4]=52;
	m_iASCII[DIK_5]=53;
	m_iASCII[DIK_6]=54;
	m_iASCII[DIK_7]=55;
	m_iASCII[DIK_8]=56;	
	m_iASCII[DIK_9]=57;
	m_iASCII[DIK_BACK]=8; //back space
	m_iASCII[DIK_LCONTROL]=17;
	m_iASCII[DIK_RCONTROL]=17;
	m_iASCII[DIK_RETURN]=13;
	m_iASCII[DIK_MINUS]=45;
	m_iASCII[DIK_SUBTRACT]=45;
	m_iASCII[DIK_ADD]=43;
	m_iASCII[DIK_UNDERLINE]=95;
	m_iASCII[DIK_RIGHT]=39;
	m_iASCII[DIK_LEFT]=37;
	m_iASCII[DIK_UP]=38;
	m_iASCII[DIK_SPACE]=32;
	m_iASCII[DIK_RALT]=18;
	m_iASCII[DIK_LALT]=18;
	m_iASCII[DIK_CAPSLOCK]=20; //tasto maiuscole-minuscole
	m_iASCII[DIK_LSHIFT]=16;
	m_iASCII[DIK_RSHIFT]=16;


	m_lpKyb=NULL;
	m_lpMouse=NULL;
	m_lpJoystick=NULL;
	m_bJoystickAttached=FALSE;

}

CADXInput::~CADXInput(void)
{
	this->ShutDown();
	FreeDeviceList();

}

//-------------------------------- GetStatus ----------------------------

int CADXInput::GetStatus(void)
{
	return m_iStatus;				
}

//------------------------------- ResetKyb -------------------------------
//Azzera il buffer della tastiera
void CADXInput::ResetKyb(void)
{
	memset(m_Buff,0,sizeof(unsigned char)*256);
	memset(m_bKeyStatus,0,sizeof(m_bKeyStatus));
}

//------------------------------- ResetMouse -------------------------------

void CADXInput::ResetMouse(void)
{
	memset(&m_mouse_state,0,sizeof(DIMOUSESTATE));
}

//----------------------------- DD_InitInput --------------------------------

//hinst � l'istanza dell'applicazione
//hwnd � l'handle della finestra corrente
HRESULT CADXInput::Init(HINSTANCE hinst,HWND hwnd) 
{   
	//azzera il buffer della tastiera
    ResetKyb(); 

	HRESULT hr; 
    //crea l'oggetto direct input principale
	hr = DirectInputCreateEx(hinst, DIRECTINPUT_VERSION,
		                     IID_IDirectInput7, 
							 (void**)&m_lpdi, NULL);  
	
	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore durante l'inizializzazione di direct input."));
		return hr;

	}

	//enumera i device di input
	EnumAllDevices();

	DEVICE_INFO_PTR p=g_pDevices;

	DWORD dwType;
	DWORD dwSubType;

    //determina se il joystick � attaccato e ne crea una istanza
	while(p)
	{
		dwType=LOBYTE(p->di.dwDevType); //tipo del device
		dwSubType=HIBYTE(p->di.dwDevType); //sottotipo

		//il tipo � definito nel byte meno significativo
		if (dwType==DIDEVTYPE_JOYSTICK)
		{
			m_bJoystickAttached=TRUE;

			hr=m_lpdi->CreateDevice(p->di.guidInstance,&m_lpJoystick,NULL);

			if (FAILED(hr)) 
			{
				this->WriteErrorFile(TEXT("Errore creando il device di input per il joystick"));
				this->ShutDown();
				return hr;
			}

			hr=m_lpJoystick->SetDataFormat(&c_dfDIJoystick2);
			//imposta il formato dei dati di input per questo device
			if (FAILED(hr)) 
			{
				this->WriteErrorFile(TEXT("Errore impostando il tipo di dati per il joystick"));
				this->ShutDown();
				return hr;
			}

			hr=m_lpJoystick->SetCooperativeLevel(hwnd,DISCL_EXCLUSIVE | DISCL_FOREGROUND | DISCL_NOWINKEY);
			if (FAILED(hr))
			{
				this->WriteErrorFile(TEXT("Errore impostando il cooperative level per il joystick"));
				this->ShutDown();
				return hr;
			}

			hr=m_lpJoystick->Acquire();
			if (FAILED(hr))
			{
				this->WriteErrorFile(TEXT("Errore acquisendo l'accesso al joystick"));
				this->ShutDown();
				return hr;
			}
			
			g_pJoy=this->m_lpJoystick;

			//enumera gli assi del joystick e ne imposta il range
			if( FAILED( hr = m_lpJoystick->EnumObjects( EnumJoyObjectsCallback, 
                                                NULL, DIDFT_AXIS) ) )
				return hr;  		   
		

			break;

		} //fine 	if (dwType==DIDEVTYPE_JOYSTICK)
		
		p=p->next;
	}
   
	//crea il device tastiera	 
    hr = m_lpdi->CreateDeviceEx(GUID_SysKeyboard, IID_IDirectInputDevice7,
                               (void**)&m_lpKyb, NULL); 
    

	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore creando il device di input per la tastiera"));
		this->ShutDown();

		return hr;
	}
	
	//crea il device mouse 
    hr = m_lpdi->CreateDeviceEx(GUID_SysMouse, IID_IDirectInputDevice7,
                               (void**)&m_lpMouse, NULL); 
    
	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore creando il device di input per il mouse."));
		this->ShutDown();

		return hr;
	}	

	//setta il formato dei dati che il device deve ritornare
    hr = m_lpKyb->SetDataFormat(&c_dfDIKeyboard); 
    
    if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore impostando il formato dati per la tastiera (SetDataFormat)."));
		this->ShutDown();

		return hr;
	}	
	
	//setta il formato dei dati per il mouse
	//c_dfDIMouse � una variabile globale fornita da direct input
	hr = m_lpMouse->SetDataFormat(&c_dfDIMouse);
    //esegue il controllo

	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore impostando il formato dati per il mouse (SetDataFormat)."));
		this->ShutDown();

		return hr;
	}
    
	//setta il comportamento della tastiera
	//DISCL_FOREGROUND richide che la finestra dell'applicazione sia ttiva
	//DISC_NONEXCLUSIVE significa che l'app. non ha controllo esclusivo sulla tastiera
	hr = m_lpKyb->SetCooperativeLevel(hwnd,DISCL_FOREGROUND | DISCL_NONEXCLUSIVE); 
    
	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore impostando il cooperative level per la tastiera."));
		this->ShutDown();

		return hr;
	}
    
	
	//setta il comportamento del mouse
	hr = m_lpMouse->SetCooperativeLevel(hwnd,DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
    

	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore impostando il cooperative level per il mouse."));
		this->ShutDown();

		return hr;
	}

     //crea un evento che poi verr� associato al mouse
    m_hevtMouse = CreateEvent(0, 0, 0, 0);
 
    if (m_hevtMouse == NULL) 
	{
		this->WriteErrorFile(TEXT("Errore impostando il gestore di eventi per il mouse"));
		this->ShutDown();
        return E_FAIL;
    }

    //collega l'evento creato, al mouse 
    hr = m_lpMouse->SetEventNotification(m_hevtMouse);
 
	if (FAILED(hr)) 
	{
		this->WriteErrorFile(TEXT("Errore impostando il gestore di eventi per il mouse"));
		this->ShutDown();
        return E_FAIL;
    }

    
	//imposta il buffer di ricezione del mouse
    hr = m_lpMouse->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph);
 	
	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore impostando il buffer di ricezione del mouse"));
		this->ShutDown();
        return hr;

	}

	//guadagna l'accesso alla tastiera
    hr=m_lpKyb->Acquire();
    
	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore accedendo alla tastiera."));
		this->ShutDown();
        return hr;
	}

	//guadagna l'accesso al mouse
	hr=m_lpMouse->Acquire();

	if (FAILED(hr))
	{
		this->WriteErrorFile(TEXT("Errore accedendo al mouse."));
		this->ShutDown();
		return hr;
	}
    
	m_iStatus=1; //direct input inizializzato

	//inizializzazione direct input OK!
	return S_OK;
}


//Abilita/Disabilita il tasto win logo
//Sembra non funzionare

void CADXInput::SwitchWinLogoKey(BOOL enabled)
{

	LONG lKeyHandle,lRet;
	LPDWORD lpDispos=0;
	HKEY hkey;
    LPCTSTR regKey="System\\CurrentControlSet\\Control\\Keyboard Layout";
    TCHAR* RData=NULL;

	//crea un chiave "volatile" (viene resettata al riavvio del sistema
    lKeyHandle = ::RegCreateKeyEx (HKEY_LOCAL_MACHINE,regKey,0,"",REG_OPTION_VOLATILE,KEY_ALL_ACCESS,NULL,&hkey,lpDispos);

	/*
	'--------------------------------------------------------------------------------------------------
	' Rdata holds the data for the REG_BINARY entry. Each table entry consists of 4 bytes.
	' Key entries have the following format:
	' aa, bb, cc, dd
	' cc and dd hold the key scan code.
	' aa and bb hold the keyscan code to replace it with; holds zero if key needs disabling.
	' So: &hID, &h00, &h5B, &hE0 replaces Left WinLogo Startkey (5B,E0) with Left Ctrl key (ID,00)
	' &h00, &h00, &h5B, &hE0 disables Left WinLogo Startkey (5B,E0)
	'--------------------------------------------------------------------------------------------------
	*/

	if (!enabled)
	{

	BYTE RData[]={0x00,0x00,0x00,0x00, //Scan code map version sempre settato a zero
		          0x00,0x00,0x00,0x00, //Scan code map flags
				  0x03,0x00,0x00,0x00, //numero di tasti + 1
				  0x00,0x00,0x5B,0xE0, //Tasto 1 : il tasto con il logo di Windows a sinistra
				  0x00,0x00,0x5C,0xE0, //Tasto 2 : il tasto con il logo di Windows a destra
				  0x00,0x00,0x00,0x00}; //zero padding

	
	//disabilita il tasto logo di Windows
	       lRet = ::RegSetValueEx (hkey,"Scancode Map",NULL,REG_BINARY,RData, sizeof(BYTE)*24);

	}
	else
	{

		::RegDeleteValue(hkey,"Scancode Map");
	}

	::RegCloseKey(hkey);

};

//------------------------------ GetKeyASCII -----------------------------
//restituisce il codice ascii a partire dal codice DirectInput del tasto premuto
int CADXInput::GetKeyASCII(int iKeyCode)
{
	if (iKeyCode>0 && iKeyCode<256) return m_iASCII[iKeyCode];
	return 0;

}
//----------------------------- DDInputShutDown ---------------------------
//rilascia gli oggetti globali DDInput

void CADXInput::ShutDown(void)
{

	if (!m_iStatus) return;
	//rilascia il device tastiera

	if (m_lpKyb)   { m_lpKyb->Unacquire();m_lpKyb->Release();}
    //rilascia il mouse
	if (m_lpMouse) { m_lpMouse->Unacquire();m_lpMouse->Release();}

	//rilascia il joystick
	if (m_lpJoystick) {m_lpJoystick->Unacquire();m_lpJoystick->Release();}

	//rilascia l'oggetto principale
    if (m_lpdi) m_lpdi->Release();

	m_iStatus=0;

}

//---------------------------- GetJoystickStatus ---------------------------
//Acquisisce lo stato del joystick
HRESULT CADXInput::GetJoystickStatus(void)
{
	if (m_lpJoystick)
	{
		HRESULT hr;

		//nota: il metodo Poll � usato da ddinput8 non � disponibile in direct input 7
	/*	hr=m_lpJoystick->Poll();

		if (FAILED(hr))
		{
			//tenta di riacquisire il joystick
			hr = m_lpJoystick->Acquire();
			while(hr == DIERR_INPUTLOST) hr = m_lpJoystick->Acquire();
    	}*/
	
		hr = m_lpJoystick->GetDeviceState(sizeof(DIJOYSTATE2), &m_js);

		if (hr==DIERR_INPUTLOST)
		{
			while(FAILED(hr=m_lpJoystick->Acquire())){};
		}
		
        return hr; 

	}

	else return E_FAIL; //joystick non presente
}

//----------------------------  JoystickX ----------------------------------
//Restituisce lo stato dell'asse X del joystick (1=destra -1=sinistra o zero)
//questa funzione va chiamata dopo aver aggiornato lo stato del joystick
LONG CADXInput::JoystickX(void)
{
	switch(m_js.lX)
	{
	case MAX_JOY_RANGE:
		return 1;
	case -MAX_JOY_RANGE:
		return -1;
	default:
		return 0;
	}
}

//----------------------------- JoystickY ----------------------------------
//(1=su -1=giu' o zero)
LONG CADXInput::JoystickY(void)
{
	switch(m_js.lY)
	{
	case MAX_JOY_RANGE:
		return 1;
	case -MAX_JOY_RANGE:
		return -1;
	default:
		return 0;
	}
}

//---------------------------- JoystickButton ------------------------------
//rende true se il bottone � premuto, false altrimenti
BOOL CADXInput::JoystickButtons(int button)
{
	if (button>=0 && button<128)
	{
		return (m_js.rgbButtons[button] & 128);
	}
	else return FALSE;
}

//---------------------------- GetKybStatus --------------------------------

//Acquisisce lo stato della tastiera	
//riempie il vettore di stato della tastiera
HRESULT CADXInput::GetKybStatus(void)
{
	HRESULT hr;
 
    hr = m_lpKyb->GetDeviceState(sizeof(m_Buff),(LPVOID)&m_Buff);  
   	
	if (hr==DIERR_INPUTLOST)
	{
		while(FAILED(hr=m_lpKyb->Acquire())){};
	}
    
    return hr;
}

//------------------------------ GetMouseState --------------------------------

//Acquisisce lo stato del mouse in modo immediato
HRESULT CADXInput::GetMouseState(void)
{
	return m_lpMouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouse_state);    
}

//-------------------------------- GetBufferedMouseState -------------------------

//Acquisisce lo stato del mouse aggiornando la struttura m_bufferd_mouse_state
//che contiene l'input bufferizzato
//Si deve usare questa funzione per aggiornare lo stato del mouse, quando
//ci interessano le coordinate assolute e non gli spostamenti relativi
//Questa funzione aggiorna anche la struttura per l'input non bufferizzato

HRESULT CADXInput::GetBufferedMouseState(void)
{
	HRESULT hr;

	hr=GetMouseState();

	m_buffered_mouse_state.lX += m_mouse_state.lX;
	m_buffered_mouse_state.lY += m_mouse_state.lY;
	m_buffered_mouse_state.lZ += m_mouse_state.lZ;

	return hr;

}

//--------------------------------- MouseDeltaX ---------------------------------

//Restituisce la posizione X del mouse
//NOTA: le funzioni che restituiscono la posizione del mouse
//non rendono la posizione assoluta ma il movimento fatto dall'ultima chiamata
//al metdodo Acquire
LONG CADXInput::MouseDeltaX(void)
{
	return m_mouse_state.lX;
}

//----------------------------------- MouseDeltaY ----------------------------------
//Posizione Y del mouse
LONG CADXInput::MouseDeltaY(void)
{
	return m_mouse_state.lY;
}

//---------------------------------- MouseDeltaWheel ---------------------------------
//Rotella del mouse
LONG CADXInput::MouseDeltaWheel(void)
{
	return m_mouse_state.lZ; 
}

//Le seguenti funzioni rendono invece l'input del mouse bufferizzato, cio� le coordinate assolute dall'inizio dell'applicazione

//---------------------------------- MouseX,Y,Y ------------------------------------

//Queste funzioni rendono la poszione assoluta del mouse (input bufferizzato) 

LONG CADXInput::MouseX(void)
{
	return m_buffered_mouse_state.lX;
}

LONG CADXInput::MouseY(void)
{
	return m_buffered_mouse_state.lY;
}

LONG CADXInput::MouseWheel(void)
{
	return m_buffered_mouse_state.lZ;
}

//------------------------------------ MouseLeftButton --------------------------
//Rende true se il tasto sinistro � premuto
BOOL CADXInput::MouseLeftButton(void)
{
	return m_mouse_state.rgbButtons[0] & 0x80; //se il tasto � premuto, il bit piu' significativo � 1
}

//---------------------------------- MouseRightButton -----------------------

BOOL CADXInput::MouseRightButton(void)
{
	return m_mouse_state.rgbButtons[1] & 0x80;
}

//---------------------------------- MouseMiddleButton -----------------------

BOOL CADXInput::MouseMiddleButton(void)
{
	return m_mouse_state.rgbButtons[2] & 0x80;
}

//----------------------------------- SetMousePosition ------------------------

//Imposta la posizione assoluta del mouse 
HRESULT CADXInput::SetMousePosition(LONG lX,LONG lY,LONG lZ) //imposta la posizione assoluta del mouse
{
	if (this->GetStatus() == 0) return E_FAIL;

	m_buffered_mouse_state.lX=lX;
	m_buffered_mouse_state.lY=lY;
	m_buffered_mouse_state.lZ=lZ;

	return S_OK;
}

//------------------------------- DDKeyDown -----------------------------------
/*restituisce lo stato di un tasto
  va chiamata subito dopo DDGetKybStatus
  esempi di costanti DirectInput:
  
  DIK_LEFT    = freccia sinistra
  DIK_RIGHT   = freccia destra
  DIK_UP      = freccia in alto
  DIK_DOWN    = freccia in basso
  DIK_A       = tasto A
  DIK_SPACE   = spazio
  DIK_LCONTROL= control sinistro
  DIK_RCONTROL= control destro
  DIK_RETURN  = return
  DIK_LSHIFT  = shift sx
  DIK_RSHIFT  = shift destro

*/

BOOL CADXInput::KeyDown(int key)
{
	return (KEYDOWN(m_Buff,key));
}

//---------------------------- KeyUp -----------------------------------

BOOL CADXInput::KeyUp(int key)
{
	return KEYUP(m_Buff,key);
}
//----------------------- KeyPress ------------------------------------

BOOL CADXInput::KeyPress(int key)
{
	if (m_bKeyStatus[key] && KEYUP(m_Buff,key)) 
	{
		m_bKeyStatus[key]=FALSE; //resetta di nuovo

		return TRUE; //il tasto � stato premuto

	}
	else if (KEYDOWN(m_Buff,key)) m_bKeyStatus[key]=TRUE; //setta il flag

	return FALSE;
}


//---------------------------- GetCurretKeyDown --------------------------

//restituice il primo tasto che trova premuto nell'intervallo iScan1, iScan2
//(l'intervallo massimo � da 0 a 256 compresi)
int CADXInput::GetCurrentKeyDown(int iScan1,int iScan2)
{
	for (int icnt=iScan1;icnt<=iScan2;icnt++)
	{
		if (KEYDOWN(m_Buff,icnt)) return icnt;
	}

	return 0;
}

int CADXInput::GetCurrentKeyDown(void)
{
	return GetCurrentKeyDown(0,MAX_VKEY);
}

//---------------------------- GetMouseHandle ------------------------

//Restituisce,l'handle dell'evento del mouse da usare con la funzione
//WaitForMultipleObject

HANDLE CADXInput::GetMouseHandle(void)
{
	return m_hevtMouse;

}

//-----------------------------  EnumAllDevices(void) ---------------------
//esegue l'enumerazione di tutti i device di input e memorizza le informazioni nella lista m_pDevices

LONG CADXInput::EnumAllDevices(void) //esegue l'enumerazione di tutti i device di input e memorizza le informazioni nella lista m_pDevices
{
   return EnumDevices(m_lpdi);
}

//-------------------------- funzioni globali per enumerazione dei device ------------- 

//-------------------------------------- FreeDeviceList ---------------------------------

void FreeDeviceList(void)
{ 	//rilascia la linked list con i device di input

	DEVICE_INFO_PTR p=g_pDevices,p1=NULL;
    
	while (p)
	{
		p1=p->next;
		if (p) {delete p;p=NULL;}       
		p=p1;
	}

	g_pDevices=NULL;
	g_dwDeviceCount=0;
}

//-------------------------------------------- GetDeviceCount ----------------------------

int CADXInput::GetDeviceCount(void) //restituisce il numero di device
{
	return (int) g_dwDeviceCount;
}

TCHAR * CADXInput::GetDeviceName(LONG Index) //nome del device con indice Index
{
	if (Index>=0 && Index<g_dwDeviceCount)
	{
		DEVICE_INFO_PTR p=g_pDevices;

		for (int cnt=0;cnt<Index;cnt++)
		{
			if (p) 
			{
				p=p->next;			
			}
			else return NULL; //fuori range
		}

		return  (TCHAR*)p->di.tszInstanceName;
	}

	return NULL;
}

//---------------------------------------- GetDeviceProductName ------------------------------

TCHAR * CADXInput::GetDeviceProductName(LONG Index) //nome di produzione del device con indice Index
{
	if (Index>=0 && Index<g_dwDeviceCount)
	{
		DEVICE_INFO_PTR p=g_pDevices;

		for (int cnt=0;cnt<Index;cnt++)
		{
			if (p) 
			{
				p=p->next;			
			}
			else return NULL; //fuori range
		}

		return  (TCHAR*)p->di.tszProductName;
	}

	return NULL;
}


//----------------------------------------- GetDeviceType ---------------------------------------

DWORD CADXInput::GetDeviceType(LONG Index) //tipo DirectInput del device con indice Index
{
	if (Index>=0 && Index<g_dwDeviceCount)
	{
		DEVICE_INFO_PTR p=g_pDevices;

		for (int cnt=0;cnt<Index;cnt++)
		{
			if (p) 
			{
				p=p->next;			
			}
			else return 0; //fuori range
		}

		return p->di.dwDevType;
	}

	return 0;	

}

//--------------------------------------- funzioni globali ------------------------------

//--------------------------------------- JoystickAttached -----------------------------------

//rende true se il joystick (o gme pad o simile) � presente e collegato al pc
BOOL CADXInput::JoystickAttached(void) //rende true se un joystick � attaccato al pc
{
	return m_bJoystickAttached;
}


//-------------------------------------- EnumDevices ------------------------------------

//restituisce il numero di device attaccati al pc
LONG EnumDevices(LPDIRECTINPUT7 lpdi)
{
	FreeDeviceList();
	//gli oggetti enumerati vanno in g_pDevices
	HRESULT hr=lpdi->EnumDevices(DIDEVTYPE_KEYBOARD,DIEnumDevicesCallback,NULL,DIEDFL_ATTACHEDONLY);
    hr=lpdi->EnumDevices(DIDEVTYPE_MOUSE,DIEnumDevicesCallback,NULL,DIEDFL_ATTACHEDONLY);
	hr=lpdi->EnumDevices(DIDEVTYPE_JOYSTICK,DIEnumDevicesCallback,NULL,DIEDFL_ATTACHEDONLY);
	return g_dwDeviceCount;
}

//---------------------------- DIEnumDevicesCallback ----------------------------
//la funzione callback viene chiamata ripetutamente da EnumDevice per ogni tipo di device
BOOL CALLBACK DIEnumDevicesCallback(LPCDIDEVICEINSTANCE lpddi,LPVOID pvRef)
{
	if (g_pDevices==NULL)
	{
		//inizializza
		g_pDevices=new DEVICE_INFO;
		memset(g_pDevices,0,sizeof(DEVICE_INFO));
		g_pTop=g_pDevices;
	}
	else
	{
		g_pTop->next=new DEVICE_INFO;
		g_pTop=g_pTop->next;
		memset(g_pTop,0,sizeof(DEVICE_INFO));
	}

	memcpy(&g_pTop->di,lpddi,sizeof(DIDEVICEINSTANCE));	
	g_dwDeviceCount++;

	return DIENUM_CONTINUE;
}

//------------------------------- EnumJoyObjectsCallback -------------------------------
//enumera gli oggetti del joystick e imposta i range max e min per gli assi
BOOL CALLBACK EnumJoyObjectsCallback( const DIDEVICEOBJECTINSTANCE* pdidoi,
                                   VOID* pContext )
{
	if( pdidoi->dwType & DIDFT_AXIS )
    {
        DIPROPRANGE diprg; 
        diprg.diph.dwSize       = sizeof(DIPROPRANGE); 
        diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER); 
        diprg.diph.dwHow        = DIPH_BYID; 
        diprg.diph.dwObj        = pdidoi->dwType; // Specify the enumerated axis
        diprg.lMin              = -MAX_JOY_RANGE; 
        diprg.lMax              = +MAX_JOY_RANGE; 
    
        // in pContext viene passato il joystick
        if( FAILED(g_pJoy->SetProperty( DIPROP_RANGE, &diprg.diph ) ) ) 
            return DIENUM_STOP;
         
    }

	return DIENUM_CONTINUE;
}




