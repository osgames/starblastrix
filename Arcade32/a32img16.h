/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32img16.H
  Buffer grafici a 16 bit
  Questo modulo deve essere platform-independent

  ***************************************************************/


#ifndef _A32IMG16_
#define _A32IMG16_ 

#define WIN32_LEAN_AND_MEAN   


#define FILL_BUFF 100

#define ANGLE_CONV_BF  (float) 0.0174533  

#include <malloc.h>
#include <stdio.h>
#include <windows.h>          //contiene definizioni base per windows
#include <math.h>

#include "a32fastm.h"        //matematica fixed point


//definizione dei tipi base
typedef unsigned short WORD;
typedef unsigned char BYTE;
typedef long LONG;
typedef unsigned long DWORD;


#ifndef  _A32GRAPH_
//struttura usata per contenere informazioni riguardo alla modalitÓ 16 bit (High Color)
typedef struct
{

	DWORD dwRBitMask,
		  dwGBitMask,
		  dwBBitMask;

    RGBQUAD Position;   

	DWORD dwRBits; //numero di bits riservati per il rosso
	DWORD dwGBits; //  "      verde   
	DWORD dwBBits; //  "      blu questi valori cambiano a seconda della modalita' 565 o 555
	DWORD dwShrR;  //numero di bit da shiftare a dx per convertire il pixel nel formato 565 o 555
	DWORD dwShrG;
	DWORD dwShrB;

} RGB16;

//macro per la creazione di valori rgb 
// colore a 16 bit formato 555
#define RGB16BIT555(r,g,b) ((b & 31) + ((g & 31) << 5) + ((r & 31) << 10))

// colore a 16 bit formato 565
#define RGB16BIT565(r,g,b) ((b & 31) + ((g & 63) << 5) + ((r & 31) << 11))

#endif

//buffer con profonditÓ colore 16 bit
class CImgBuffer16
{

private:

	WORD *m_wBuff;	
	DWORD *m_pdwOffset; //tabella degli offset (serve per accedere al buffer ancora piu' velocemente)
	LONG m_lPitch; //pitch del buffer in WORD
	LONG m_lHeight; //altezza in pixel
	LONG m_lWidth; //larghezza in pixel
	LONG m_lDestHeight; //altezza buff. destinazione
	LONG m_lDestWidth; //larghezza in pixel buff. destinazione
	LONG m_lDestPitch; //pitch in WORD del buffer destinazione
	WORD *m_wDestBuff; //buffer destinazione
	BYTE m_byAlpha; //valore dell'alpha blending corrente
	BYTE m_AlphaTable[64][64]; //tabella di cosultzione x alpha blending
	void CreateAlphaBuff(void); //crea la matrice x alpha blending
	DWORD m_dwRedMask,m_dwGreenMask,m_dwBlueMask; //maschere per i tre canali R,G,B
	BYTE m_byRedPos,m_byGreenPos,m_byBluePos; //posizione dei bit 
    DWORD m_dwAlphaPitch;
	DWORD m_dwByteSize; //dimensione del buffer in bytes
	WORD m_wFillColor; //usata da fillarea
	WORD m_wBound; //usata da fillarea
	int m_iFillMode;
	CImgBuffer16 *m_pDestBuffer;
	CADXFastMath m_Math;
	WORD wlookup[65536]; //tabella precalcolata
	BYTE byAlphaTable[3][64];
	
	void CreateFadingTable(WORD wDestColor,BYTE alpha);
	void FillLine(const LONG x,const LONG y,LONG *xleft,LONG *xright); //funzione interna usata da FillArea
    
public:
	
	CImgBuffer16(void);
	~CImgBuffer16(void);	
	HRESULT SetBuffer(WORD *pwBuff,LONG lPitch,LONG height,LONG width); //imposta il buffer grafico
	HRESULT SetDestBuffer(CImgBuffer16 *dest); //imposta il buffer su cui eseguire il blitting
	void SetUnlockedBuffer(WORD *pwBuff);  //imposta il puntatore del buffer (va chiamata dopo aver inizializzato tutto con SetBuffer)
	void SetUnlockedDestBuffer(WORD *pwBuff); //imposta il puntatore del buffer destinazione
	LONG Width(void); //rende la larghezza
	LONG Height(void);
	LONG Pitch(void);
	DWORD ByteSize(void); //restituisce le dimensioni in bytes del buffer
	void SetPixelFormat(RGB16 *pixformat);
	WORD *GetBuffer(void); //restituisce il buffer impostato
	void Blt(LONG x,LONG y);
	void BltAlphaBlend(LONG x,LONG y);
	void BltFading(LONG x,LONG y,WORD wDestColor,BYTE alpha);
	void BltRot(LONG xc,LONG yc,int angle);
	void Fade(WORD wDestColor,BYTE alpha);
	//imposta il parametro per l'alpha blending
    void SetAlphaBlending(BYTE alpha);	
	void Cls(WORD Color);//pulisce il buffer impostando tutte le WORD su wColor
	void DrawLine(LONG x1,LONG y1,LONG x2,LONG y2,WORD wColor); //disegna una linea sul buffer
	void DrawEllipse(LONG xc,LONG yc,DWORD dwXRadius,DWORD dwYRadius,WORD wColor);
	void DrawRect(LONG left,LONG top,LONG right,LONG bottom,WORD wColor); //disengna un rettangolo
	void DrawFilledRect(LONG left,LONG top,LONG right,LONG bottom,WORD wForeColor,WORD wFillColor);
	void SetPixel(LONG x,LONG y,WORD wColor); //accende un pixel
	WORD GetPixel(LONG x,LONG y);//acquisisce un pixel
	//riempie un area con un colore
	void FillArea(const LONG x,const LONG y,WORD wColor,WORD wBound,int mode=0);


};

#endif
