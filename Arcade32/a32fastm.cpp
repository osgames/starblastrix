/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32FASTM.CPP
  funzioni matematiche,trogonometriche 2D e fixedpoint

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/


#include "a32fastm.h" 

//------------------------------- CADXFastMath ----------------------------------------

//costruttore
CADXFastMath::CADXFastMath(void)
{
	m_status=0;
	InitTables();
}

CADXFastMath::~CADXFastMath(void)
{	
	m_status=0;
}
//----------------- InitTables ------------------------------------------
//inizializza le tabelle del seno e coseno
void CADXFastMath::InitTables(void)
{
	long count;

    for (count=0;count<MAX_ANGLE;count++)  SinTable[count]=(float)sin(count*DEG_TO_RADIANS);
    for (count=0;count<MAX_ANGLE;count++)  CosTable[count]=(float)cos(count*DEG_TO_RADIANS);
    for (count=0;count<MAX_ANGLE;count++)  TanTable[count]=(float)tan(count*DEG_TO_RADIANS);
	//usa numeri fixed point per il seno e coseno
    for (count=0;count<MAX_ANGLE;count++)  	CosFast[count]=(FIXED_T)(CosTable[count]*1024L);	
    for (count=0;count<MAX_ANGLE;count++)  SinFast[count]=(FIXED_T)(SinTable[count]*1024L);
    CosFast[0]=LONGTOFIXED(1L);
	CosFast[360]=LONGTOFIXED(1L);
    SinFast[0]=0L;
	SinFast[360]=0L;
    
	Randomize(MAX_RND); //inizialmente al massimo si possono creare numeri casuali da 0 a 10000
	   
	//tabella arcoseno con argomento  numero fixed
	for (count=0;count<1024;count++) AcosFast[count]=FIXED_T(acos((double)count/1024)/DEG_TO_RADIANS);
    for (count=0;count<MAX_RND;count++) RndTable[count]=rand()%MAX_RND;

    m_status=1;

}

//---------------------------- Randomize -----------------------------------------
//Reimposta la tabella dei numeri casuali
//i numeri casuali vanno da 0 a dwMaxRndValue compresi
int CADXFastMath::Randomize(unsigned long dwMaxRndValue)
{
	unsigned long dwCount;

	//inizializza il generatore di numeri random
    srand( (unsigned)time( NULL ) );

	if (dwMaxRndValue>0)
	{
		m_dwMaxRnd=dwMaxRndValue;		
	
		if (!RndTable) return -1;

		for (dwCount=0;dwCount<MAX_RND;dwCount++)
		{
			RndTable[dwCount]=rand()%m_dwMaxRnd;
		}

		return 1;
	}
	else return -1;
}

//----------------------------------- FastSqrt ---------------------------------------------------
//Calcola la radice quadrata intera

unsigned int CADXFastMath::FastSqrt(long r)
{

	//calcola velocemente una radice quadrata

	double x,y;
	float tempf;
	unsigned long *tfptr = (unsigned long *)&tempf;
	int is;

	tempf = (float)r;
	*tfptr=(0xbe6f0000L-*tfptr)>>1;
	x=tempf;
	y=r*0.5;
	x*=1.5-x*x*y; 
	if( r>101123L ) x*=1.5-x*x*y;
	x*=r;

    //casting e arrotondamento veloce
	_asm 
	{
		fld x
    	fistp is
	}

	if( is*is>r ) is--;
	return is;
}

//--------------------------------------- Sgn ------------------------------

int CADXFastMath::Sgn(int x)
{
	if (x<0) return -1;
	else if (x>0) return 1;
	return 0;
}

//--------------------------------- GetAngle ---------------------------------
//Restituisce l'angolo [0,360] del vettore che va da x1,y1 a x2,y2
int CADXFastMath::GetAngle(long x1,long y1,long x2,long y2)
{
	long l1,l2,d1,dx,dy;
	FIXED_T f1,ang;
	int phase=0;
    

	dx=x2-x1;
	dy=y2-y1;

	d1=dx;

	//tiene conto del segno (gli angoli sono sempre positivi)
	if (d1<0) d1=-d1;
	
	l2=dy*dy;;
	l1=dx*dx;

	l1 += l2; //l1 � la distanza al quadrato
	
	l2=(long) FastSqrt(l1); //distanza	

	if (!l2) return 0; //attenzione a non dividere per zero!

	f1=1024*d1/l2; //arco coseno fixed point

	ang=AcosFast[f1]; //la tabella AcosFast contiene l'arcocoseno degli angoli del I quadrante
    
    //deve determinare il quadrante del secondo punto
	if (dx<0)
	{
		if (dy<0) ang+=180;
		else ang=180-ang;
	}

	else if (dy<0) ang=360-ang;

	return ang;

}

//-------------------------------- RndFast -----------------------------------
//restituisce un numero compreso in [0;seed]  (si puo' ottenere anche seed)
//N.B. il massimo valore di seed e' 10000!!!   (se seed e' 0 si ottiene 0)
unsigned long CADXFastMath::RndFast(unsigned long seed) {

static unsigned long pos_rnd=0;

pos_rnd++;
if (pos_rnd>=MAX_RND) pos_rnd=0;

return RndTable[pos_rnd]%(seed+1);

}

//--------------------------------- IsInitialized ------------------------------

//rende 1 se le tabelle trigonometriche e le altre tabelle sono inizializzate
int CADXFastMath::IsInitialized(void)
{
	return (m_status>0 ? 1 : 0);
}

//---------------------------------- GetAngleDiff ----------------------------

//restituisce -1 se da angle per arrivare ad angle_target si deve ruotare in senso antiorario,
//1 se si deve ruotare in senso orario e 0 se i due angoli sono coincidenti
int CADXFastMath::GetAngleDiff(int angle,int angle_target)
{
	int da;
	angle=angle % 360;
	angle_target=angle_target%360;

	if (angle_target==angle) return 0;

	da=angle_target-angle;

	if (da>0)
	{
		if (da<360-da) return 1;
		else return -1;
	}
	else
	{
		da=-da;
		if (da<360-da) return -1;
		else return 1;
	}

}


//restituisce -1 se da angle per arrivare ad angle_target si deve ruotare in senso antiorario,
//1 se si deve ruotare in senso orario e 0 se i due angoli sono coincidenti
//in questo caso � possibile specificare la tolleranza in gradi
int CADXFastMath::GetAngleDiff(int angle,int angle_target,int tolerance)
{
	int da;
	angle=angle % 360;
	angle_target=angle_target%360;

	if (angle_target==angle) return 0;

	da=angle_target-angle;

	if (abs(da)<tolerance) return 0;

	if (da>0)
	{
		if (da<360-da) return 1;
		else return -1;
	}
	else
	{
		da=-da;
		if (da<360-da) return -1;
		else return 1;
	}
}