/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  5-11-2004
  --------------------------------------------------------------
  A32UTIL.H
  miscellanea di funzioni utili da usare con la libreria arcade32

  ***************************************************************/

#ifndef _A32UTIL_
#define _A32UTIL_

#include "a32graph.h"
#include "a32sprit.h"

//Aggiunge un vertice ad un poligono di contenimento gi� allocato
void AddVertex(BOUND_POLY_PTR pdest,int x,int y);
/*
Ruota un poligono di contenimento
pdest=poligono di destinazione (poligono ruotato)
psrc=poligono sorgente da ruotare
pimgsrc=immagine di riferimento (viene usata per acquisire larghezza e altezza)
theta=angolo di rotazione
bFitSize=indica se dopo la rotazione le dimensioni del rettangolo che circoscrive il poligono devono essere adattate (vedere funzione CreateRotFrame)
*/
void RotatePoly(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc,int theta,BOOL bFitSize);
//crea un poligono speculare rispetto all'asse verticale 
void MirrorPolyH(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc);
//crea un poligono speculare rispetto all'asse orizzontale
void MirrorPolyV(BOUND_POLY_PTR pdest,const BOUND_POLY_PTR psrc,const IMAGE_FRAME_PTR pimgsrc);
//Estrae da una immagine una striscia orizzontale di frames
HRESULT GrabFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
//Estrae da un'immgine un vettore di frames disposte in senso verticale
HRESULT GrabVFrameArray(IMAGE_FRAME_PTR pArray,IMAGE_FRAME_PTR pimgSrc,DWORD dwFrames,LONG px,LONG py,LONG w,LONG h);
//rilascia un vettore di frames allocate precedentemente
void FreeFrameArray(IMAGE_FRAME_PTR pArray,DWORD dwFrames);

#endif