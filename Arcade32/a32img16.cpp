/*************************************************************

  ARCADE 32 LIBRARY
  Code by Leonardo Berti 2001 (c)
  14-08-2003
  --------------------------------------------------------------
  A32FASTM.H
  manipolazione buffer immagini a 16 bit

  ------------------------------------------------------------------------  
	
    ARCADE32 - 2D game engine

	Copyright (C) 2002-2006 Leonardo Berti
   
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: leonardo.berti@tin.it  
-------------------------------------------------------------------------

  ***************************************************************/

#include "a32img16.h"
//#include "aarcade32.h"

//---------------------- CImgBuffer16 ------------------------------------------

CImgBuffer16::CImgBuffer16(void)
{
	m_wBuff=NULL;
	m_lPitch=0;
	m_lHeight=0;
	m_lDestWidth=0;
	m_lDestHeight=0;
	m_lDestPitch=0;
	m_wDestBuff=NULL;
	m_byAlpha=0xFF; //opaco (alpha blending)
	//per default � 555
	m_dwRedMask=31744; //imposta le maschere di default
	m_dwGreenMask=992;
	m_dwBlueMask=31;
	m_byRedPos=10; //posizione R,G,B, nel pixel di default
	m_byGreenPos=5;
	m_byBluePos=0;
	m_dwAlphaPitch;
	m_dwByteSize=0;
	CreateAlphaBuff(); //crea la matrice per l'alpha blending
	m_pdwOffset=NULL;


}

CImgBuffer16::~CImgBuffer16(void)
{
	//rilascia la memoria
	if (m_pdwOffset) delete[] m_pdwOffset;

}

///////////////////////////////////////  CImgBuffer16::SetBuffer ////////////////////////////

//imposta il buffer
//NOTA BENE: lPitch � il pitch della sup. in WORD e non BYTES
//pwBuff=buffer WORD * da impostare
//lPitch pitch del buffer in word
//height e width sono la larghezza e l'altezza in WORD (Pixel) in quanto 1 pixel = 1WORD in HighColor
HRESULT CImgBuffer16::SetBuffer(WORD *pwBuff,LONG lPitch,LONG height,LONG width)
{
	DWORD dwCount;

	if (pwBuff && lPitch>=width && height>0)
	{
		m_wBuff=pwBuff;
		m_lPitch=lPitch;
		m_lHeight=height;	
		m_lWidth=width;		
		m_dwByteSize=m_lPitch*m_lHeight*sizeof(WORD);

		if (m_pdwOffset) delete[] m_pdwOffset;
		//crea la tabella degli offset

		m_pdwOffset=new DWORD[m_lHeight];

		for (dwCount=0;dwCount<(DWORD)m_lHeight;dwCount++)
		{
			m_pdwOffset[dwCount]=dwCount*m_lPitch; //offset della riga con ordinata dwCount
			
		}
	}

	else return E_FAIL;

	return S_OK;
}

////////////////////////////// CImgBuffer16::SetUnlockedBuffer///////////////////////////////

//imposta il puntatore del buffer (va chiamata dopo aver inizializzato tutto con SetBuffer)
void CImgBuffer16::SetUnlockedBuffer(WORD *pwBuff)  //imposta il puntatore del buffer (va chiamata dopo aver inizializzato tutto con SetBuffer)
{
	m_wBuff=pwBuff;
}

//////////////////////////////// CImgBuffer16::SetUnlockedDestBuffer ///////////////////////////

void CImgBuffer16::SetUnlockedDestBuffer(WORD *pwBuff) //imposta il puntatore del buffer destinazione
{
	m_wDestBuff=pwBuff;
}

///////////////////////////////// CImgBuffer16::GetBuffer //////////////////////////////////////

//acquisisce il buffer
WORD *CImgBuffer16::GetBuffer(void)
{
	return m_wBuff;
}

///////////////////////////////// CImgBuffer16::Width //////////////////////////////////////


LONG CImgBuffer16::Width(void) //rende la larghezza in pixel
{
	return m_lWidth;
}

///////////////////////////////// CImgBuffer16::Height //////////////////////////////////////


LONG CImgBuffer16::Height(void) //altezza in pixel
{
	return m_lHeight;
}

///////////////////////////////// CImgBuffer16::Pitch //////////////////////////////////////


LONG CImgBuffer16::Pitch(void) //pitch in WORD
{
	return m_lPitch;
}

/////////////////////////////////CImgBuffer16::ByteSize //////////////////////////////////////

DWORD CImgBuffer16::ByteSize(void)
{
	return m_dwByteSize;
}

/////////////////////////////////CImgBuffer16::SetDestBuffer //////////////////////////////////////

HRESULT CImgBuffer16::SetDestBuffer(CImgBuffer16 *dest) //imposta il buffer su cui eseguire il blitting
{	
	if (!dest) return E_FAIL;

	m_wDestBuff=dest->GetBuffer();

	if (!m_wDestBuff) return E_FAIL;

	m_lDestWidth=dest->Width();
	m_lDestHeight=dest->Height();
	m_lDestPitch=dest->Pitch();

	m_pDestBuffer=dest;
	
	return S_OK;
}

///////////////////////////// CImgBuffer16::CreateAlphaBuff /////////////////////////////////

//crea una matrice 32x32 in cui elementi rappresentano il risultato del blending in base al paramatro alpha
//impostato, alpha va da 0 a 0xFF, 0xFF significa blitting opaco
//0x0 significa che l'immagine sorgente � completamente trasparente
//l'elemento x,y � il valore che il canale assume quando il canale dell'immagine sorgente
//�x e quello destinazione � y
void CImgBuffer16::CreateAlphaBuff(void)
{
	float fAlpha;

	fAlpha=(float)m_byAlpha / 255;

	//canale del pixel di primo piano
	for (int i=0;i<64;i++)
	{
		//canale del pixel di sfondo
		for (int j=0;j<64;j++)
		{
			m_AlphaTable[i][j]=(BYTE)((float)i*fAlpha + (float)j*(1-fAlpha));		
		}
	}
	
}

///////////////////////////// CImgBuffer16::SetAlphaBlending /////////////////////////////////

//imposta il parametro per l'alpha blending alpha=255 opaco alpha=0 completamente trasparente
void CImgBuffer16::SetAlphaBlending(BYTE alpha)
{
	m_byAlpha=alpha;
	this->CreateAlphaBuff(); //crea il buffer per il calcolo del blending
}

/////////////////////////////  CImgBuffer16::SetPixelFormat /////////////////////////////////

//imposta il formato del pixel
void CImgBuffer16::SetPixelFormat(RGB16 *pixformat)
{
	if (!pixformat) return;

	m_dwRedMask=pixformat->dwRBitMask;
	m_dwGreenMask=pixformat->dwGBitMask;
	m_dwBlueMask=pixformat->dwBBitMask;
	m_byRedPos=pixformat->Position.rgbRed;
	m_byGreenPos=pixformat->Position.rgbGreen;
	m_byBluePos=pixformat->Position.rgbBlue;
	
}

/////////////////////////////  CImgBuffer16::Blt /////////////////////////////////////////////

//esegue un blitting con colore trasparente lo zero
void CImgBuffer16::Blt(LONG x,LONG y)
{
	LONG lx,ly,lSizex,lSizey;
	LONG lcx1=0,lcx2=0,lcy1=0,lcy2=0;
	LONG lcny,lcnx;
	LONG lMaxy;
	WORD *wDest,*wSrc;

	lcx1=x <0 ? 0 : x;
	lcx2=x + m_lWidth >= m_lDestWidth ? m_lDestWidth : x+m_lWidth;
	
	lSizex=lcx2-lcx1;

	if (lSizex<0) return;
	lx=lcx1-x;

	lcy1=y <0 ? 0 : y;
	lcy2=y + m_lHeight >= m_lDestHeight ? m_lDestHeight : y+m_lHeight;
	
	lSizey=lcy2-lcy1;
	ly=lcy1-y;

	if (lSizey <0) return;
	
	wDest=m_wDestBuff+m_lDestPitch*lcy1+lcx1;

	lMaxy=lSizey+ly;
	
	for (lcny=ly;lcny<lMaxy;lcny ++)
	{
		wSrc=m_wBuff+lcny*m_lPitch+lx;
		
		for (lcnx=0;lcnx<lSizex;lcnx++)
		{
			if (*wSrc !=0) //se il pixel � trasparente non lo copia nel buffer destinazione
			{
				*(wDest+lcnx)=*wSrc; //blit
			}

			wSrc++;
		}

		wDest += m_lDestPitch;
	}
}

////////////////////////////////////////////// BltFading ////////////////////////////////////////////
//esegue un blitting con colore trasparente zero, inoltre esegue il fading su tutti gli altri pixel
//wDestColor=colore destinazione del fading
//alpha=255 tutti i pixel=wDestcolor alpha=0 pixel non alterati

void CImgBuffer16::BltFading(LONG x,LONG y,WORD wDestColor,BYTE alpha)
{
	LONG lx,ly,lSizex,lSizey;
	LONG lcx1=0,lcx2=0,lcy1=0,lcy2=0;
	LONG lcny,lcnx;
	LONG lMaxy;
	WORD *wDest,*wSrc;

	//crea le tabelle di look-up pe ril fading
	CreateFadingTable(wDestColor,alpha);

	lcx1=x <0 ? 0 : x;
	lcx2=x + m_lWidth >= m_lDestWidth ? m_lDestWidth : x+m_lWidth;
	
	lSizex=lcx2-lcx1;

	if (lSizex<0) return;
	lx=lcx1-x;

	lcy1=y <0 ? 0 : y;
	lcy2=y + m_lHeight >= m_lDestHeight ? m_lDestHeight : y+m_lHeight;
	
	lSizey=lcy2-lcy1;
	ly=lcy1-y;

	if (lSizey <0) return;
	
	wDest=m_wDestBuff+m_lDestPitch*lcy1+lcx1;

	lMaxy=lSizey+ly;
	
	for (lcny=ly;lcny<lMaxy;lcny ++)
	{
		wSrc=m_wBuff+lcny*m_lPitch+lx;
		
		for (lcnx=0;lcnx<lSizex;lcnx++)
		{
			if (*wSrc !=0) //se il pixel � trasparente non lo copia nel buffer destinazione
			{
				*(wDest+lcnx)=wlookup[*wSrc]; //blit con fading
			}

			wSrc++;
		}

		wDest += m_lDestPitch;
	}
}

///////////////////////////// BltRot ///////////////////////////////////////////////////////////////////
//Esegue il blitting ruotando l'immagine
//nota bene: per non far apparire gli errori dovuti all'arrotondamento
//le dimensioni della immagine sorgente vengono ridotte a met�
//xc,yc=coordinate di output
//come punto di riferimento per l'output viene preso il centro dell'immagine
//zero � considerato colore trasparente
//ad angoli positivi corrisponde una rotazione oraria

void CImgBuffer16::BltRot(LONG xc,LONG yc,int angle)
{	

	FIXED_T fxca,fxsa;
	FIXED_T f1,f2;	    
	DWORD dwOff;	
	WORD wPixel;
	LONG lX,lY,lX1,lY1;
	LONG lvh,lvw;
	LONG lpx,lpy;    

	angle=angle % 360;

	if (angle<0) angle = 360+angle;
    //calcola il coseno e il seno dell'angolo in fixed point
    fxca=m_Math.CosFast[angle%360];
	fxsa=m_Math.SinFast[angle%360];    
	
	lvh=m_lHeight;
	lvw=m_lWidth;
    //coordinate di traslazione (servono a far apparire il punto centrale dell'immagine in xc e yc
	lpx=((lvw*fxca+lvh*fxsa)>>12)-xc;
	lpy=((lvw*fxsa-lvh*fxca)>>12)-yc;

	for (lX=0;lX<lvw;lX++)
	{
		//calcola la prima parte della trasformazione			
		f1=(FIXED_T) lX * fxca;
		f2=(FIXED_T) lX * fxsa;

		for (lY=0;lY<lvh;lY++)
		{		
			//rotazione
			lX1=f1+lY*fxsa;  //x'=x*cos(a)-y*sen(a)
			lY1=f2-lY*fxca;  //y'=x*sin(a)+y*cos(a)
			//converte da fixed a long e poi divide per due per non far apparire gli errori di arrotondamento
			lX1 >>= 11;
			lY1 >>= 11;
			//traslazione
			lX1 -= lpx;
			lY1 -= lpy;

			//lX1 e lY1 sono le coordinate del punto dopo la rotazione nel buffer di destinazione
			if (lX1>=0 && lX1<m_lDestWidth)
			{
				if (lY1>=0 && lY1<m_lDestHeight)
				{
					wPixel=m_wBuff[lX+m_pdwOffset[lY]];
					if (wPixel)	
					{
						dwOff=lY1*m_lDestPitch+lX1;
						m_wDestBuff[dwOff]=m_wBuff[lX+m_pdwOffset[lY]];
					}
				}
			}
		}
	}
}

/////////////////////////////// CImgBuffer16::CrateFadingTable ///////////////////////////////////////////

//crea la tabella per il fading
void CImgBuffer16::CreateFadingTable(WORD wDestColor,BYTE alpha)
{
	register DWORD count;
	BYTE red,green,blue;
	float fAlpha;

	fAlpha=(float)alpha/255;

	//estrae le componenti R,G,B per il olore destinazione
	red=(BYTE)((wDestColor &  m_dwRedMask) >> m_byRedPos);
	green=(BYTE)((wDestColor &  m_dwGreenMask) >> m_byGreenPos);
	blue=(BYTE)((wDestColor & m_dwBlueMask) >> m_byBluePos);

	//crea la tabella alpha 
	for (count=0;count<64;count++)
	{
		//rosso
		byAlphaTable[0][count]=(BYTE)((float)count*(1-fAlpha)+(float)red*fAlpha);
		//verde
		byAlphaTable[1][count]=(BYTE)((float)count*(1-fAlpha)+(float)green*fAlpha);
		//blu
		byAlphaTable[2][count]=(BYTE)((float)count*(1-fAlpha)+(float)blue*fAlpha);	

	}

	for (count=0;count<65536;count++)
	{
		//estrae i componenti del colore corrente
		red=(BYTE)((count &  m_dwRedMask) >> m_byRedPos);
		green=(BYTE)((count &  m_dwGreenMask) >> m_byGreenPos);
		blue=(BYTE)((count & m_dwBlueMask) >> m_byBluePos);

		//componenti trasformate 
		red=byAlphaTable[0][red];
		green=byAlphaTable[1][green];
		blue=byAlphaTable[2][blue];

		//costruisce il pixel trasformato
		wlookup[count]=(red << m_byRedPos)+(green << m_byGreenPos) + blue;

	}

}

////////////////////////////// CImgBuffer16::Fade //////////////////////////////////////////////////////
//Applica il fading a tutto il buffer primario
//wDestcolor=colore destinazione
//alpha=255-> tutti i pixel del colore wDestcolor
//alpha=0 i pixel restano inalterati

void CImgBuffer16::Fade(WORD wDestColor,BYTE alpha)
{
	DWORD dwPixels;

	//crea la tabella per il fading
	CreateFadingTable(wDestColor,alpha);

	dwPixels=(DWORD)m_lPitch*m_lHeight;

	for (DWORD count=0;count<dwPixels;count++)
	{
		m_wBuff[count]=wlookup[m_wBuff[count]]; //applica la trasformazione ad ogni pixel del buffer
	}


}
/////////////////////////////  CImgBuffer16::BltAlphaBlend /////////////////////////////////////////////

//esegue un blitting con colore trasparente lo zero e alpha blending
//ATTENZIONE!:Prima di chimare questa funzione bisogna aver specificato il
//formato pixel del bufffer chiamando la funzione SetPixelFormat

void CImgBuffer16::BltAlphaBlend(LONG x,LONG y)
{
	LONG lx,ly,lSizex,lSizey;
	LONG lcx1=0,lcx2=0,lcy1=0,lcy2=0;
	LONG lcny,lcnx;
	LONG lMaxy;
	WORD *wDest,*wSrc,*wPixDest;
	BYTE red,green,blue; //RGB sorgente
	BYTE red1,green1,blue1; //RGB destinazione

	lcx1=x <0 ? 0 : x;
	lcx2=x + m_lWidth >= m_lDestWidth ? m_lDestWidth : x+m_lWidth;
	
	lSizex=lcx2-lcx1;

	if (lSizex<0) return;
	lx=lcx1-x;

	lcy1=y <0 ? 0 : y;
	lcy2=y + m_lHeight >= m_lDestHeight ? m_lDestHeight : y+m_lHeight;
	
	lSizey=lcy2-lcy1;
	ly=lcy1-y;

	if (lSizey <0) return;
	
	wDest=m_wDestBuff+m_lDestPitch*lcy1+lcx1;

	lMaxy=lSizey+ly;
	
	for (lcny=ly;lcny<lMaxy;lcny ++)
	{
		wSrc=m_wBuff+lcny*m_lPitch+lx;
		
		for (lcnx=0;lcnx<lSizex;lcnx++)
		{
			if (*wSrc !=0) //se il pixel � trasparente non lo copia nel buffer destinazione
			{	
				red=(BYTE)((*wSrc &  m_dwRedMask) >> m_byRedPos);
				green=(BYTE)((*wSrc &  m_dwGreenMask) >> m_byGreenPos);
				blue=(BYTE)((*wSrc & m_dwBlueMask) >> m_byBluePos);

				//prende il pixel di destinazione
				wPixDest=wDest+lcnx;

				red1=(*wPixDest & (BYTE) m_dwRedMask) >> m_byRedPos;
    			green1=(*wPixDest & (BYTE) m_dwGreenMask) >> m_byGreenPos;
				blue1=(*wPixDest & (BYTE) m_dwBlueMask) >> m_byBluePos;						
		
				red=m_AlphaTable[red][red1];
				green=m_AlphaTable[green][green1];
				blue=m_AlphaTable[blue][blue1];

				//blit
				//ricrea il pixel
				*wPixDest = (red << m_byRedPos) + (green << m_byGreenPos) + blue;
				
			}

			wSrc++;
		}

		wDest += m_lDestPitch;
	}
}
/////////////////////////////  CImgBuffer16::Cls /////////////////////////////////////////////

//pulisce il buffer con il colore wColor
//NB non esegue nessun controllo sulla validit� del buffer
void CImgBuffer16::Cls(WORD wColor)
{
	DWORD dwCount;
	DWORD dwSize;

	dwSize=m_dwByteSize>>1;	
	
	for (dwCount=0;dwCount<dwSize;dwCount++) m_wBuff[dwCount]=wColor;
}

/////////////////////////////  CImgBuffer16::DrawLine /////////////////////////////////////////////
//Disegna una linea sul buffer
void CImgBuffer16::DrawLine(LONG x1,LONG y1,LONG x2,LONG y2,WORD wColor) //disegna una linea sul buffer
{

	int dx,dy;
	int d,inca,incb;
	int incax,incbx,incay,incby;
	int x,y;
	int temp;
	DWORD dwOff;
		

	dy=y2-y1;
	dx=x2-x1;

	if (dx<0) 
	{
		 dx=-dx;
		 incbx=-1;
	}

	else incbx=1;

	if (dy<0) 
	{
		 dy=-dy;
		 incby=-1;
	}

	else incby=1;

	if (dx<dy) 
	{ 
		temp=dx;  //scambia dx e dy
		dx=dy;
		dy=temp;
		incax=0;
		incay=incby;
	}

	else 
	{ 
		incax=incbx;
		incay=0; 
	}

	d=dy*2-dx;
	inca=dy*2;
	incb=2*(dy-dx);
	x=x1;
	y=y1;


	if ((y>=0) && (y<=m_lHeight-1)) 
	{
		if ((x>=0) && (x<=m_lWidth-1))
		{
		
		 //imposta il pixel
		 dwOff=m_pdwOffset[y];
		 m_wBuff[dwOff+x]=wColor;
		}	 
		 
	}//fine if
     
	for (temp=0;temp<dx;temp++) 
	{

		 if (d<0) 
		 {
			 x=x+incax;
			 y=y+incay;
			 d=d+inca;
		 }

		 else  
		 {
			 x=x+incbx;
			 y=y+incby;
			 d=d+incb;
		 }

		if ((y>=0) && (y<=m_lHeight-1)) 
		{
			if ((x>=0) && (x<=m_lWidth-1))
			{
      			 // prepara il rettangolo di destinazione delle dimensioni 1x1
				 //imposta il pixel
				dwOff=m_pdwOffset[y];
				m_wBuff[dwOff+x]=wColor;
			}
		}
	}//fine for temp
}

//////////////////////////////////// CimgBuffer16::DrawEllipse ////////////////////////////////
//Disegna un'ellisse in coordinate polari 
//cx,cy=coordinate del centro
//dwMaxRadius=raggio massimo
//dwMinRadius=raggio minimo
//wColor=colore

void CImgBuffer16::DrawEllipse(LONG xc,LONG yc,DWORD dwXRadius,DWORD dwYRadius,WORD wColor)
{
	LONG x,y,xo,yo;
	const float fPi=3.1415927f;
	float fCount;	
	float fStep;
	float fLimit;
    
    fStep=2*fPi/1000;

	fLimit=2*fPi;
	
    //punto iniziale
	xo=xc+dwXRadius;
	yo=yc;

	
	for(fCount=0;fCount<=fLimit;fCount+=fStep)
	{
		x=(LONG)((float)dwXRadius*cos(fCount)+xc);
		y=(LONG)((float)dwYRadius*sin(fCount)+yc);

		DrawLine(xo,yo,x,y,wColor);
		xo=x;
		yo=y;
	} 

	DrawLine(x,y,xc+dwXRadius,yc,wColor); //chiude l'ellisse
}

//////////////////////////////////////////// CImgBuffer16::DrawRect ////////////////////////////////////

void CImgBuffer16::DrawRect(LONG left,LONG top,LONG right,LONG bottom,WORD wColor) //disengna un rettangolo
{
	DWORD dwCount;
	DWORD dwX1,dwX2;
	DWORD dwY1,dwY2;	
	DWORD dwOff1;
	BOOL bTop,bLeft,bRight,bBottom;

	if (right <0) return; //rettangolo fuori dal buffer
	if (top>=m_lHeight) return; //rettangolo fuori dal buffer
	if (bottom <0) return; //rettangolo fuori dal buffer
	if (left>=m_lWidth) return; //rettangolo fuori dal buffer

	if (left<0) {dwX1=0;bLeft=FALSE;} else {dwX1=(DWORD) left;bLeft=TRUE;}

	if (top<0) {dwY1=0;bTop=FALSE;}	else {dwY1=(DWORD) top;bTop=TRUE;}

	if (right>=m_lWidth) {dwX2=(DWORD) m_lWidth-1;bRight=FALSE;} else {dwX2=(DWORD) right;bRight=TRUE;}

	if (bottom>= m_lHeight) {dwY2=(DWORD) m_lHeight-1;bBottom=FALSE;} else {dwY2=(DWORD) bottom;bBottom=TRUE;}

	if (bTop) 
	{
		//linea orizzontale in alto
		dwOff1=m_pdwOffset[dwY1];
		for (dwCount=dwX1;dwCount<=dwX2;dwCount++)
		{	
			m_wBuff[dwOff1+dwCount]=wColor;			
		}		
	}

	if (bBottom) 
	{
		//linea orizzontale in basso
		dwOff1=m_pdwOffset[dwY2];
		for (dwCount=dwX1;dwCount<=dwX2;dwCount++)
		{	
			m_wBuff[dwOff1+dwCount]=wColor;			
		}		
	}

	if (bLeft) 
	{
		//lato sinistro		
		for (dwCount=dwY1;dwCount<=dwY2;dwCount++)
		{	
			dwOff1=m_pdwOffset[dwCount];
			m_wBuff[dwOff1+dwX1]=wColor;			
		}		
	}

	if (bRight) 
	{
		//lato sinistro		
		for (dwCount=dwY1;dwCount<=dwY2;dwCount++)
		{	
			dwOff1=m_pdwOffset[dwCount];
			m_wBuff[dwOff1+dwX2]=wColor;			
		}		
	}	    
}

///////////////////////////////////////// CImgBuffer16::DrawFilledRect ////////////////////////////////////////

void CImgBuffer16::DrawFilledRect(LONG left,LONG top,LONG right,LONG bottom,WORD wForeColor,WORD wFillColor)
{
	DWORD dwCount;
	DWORD dwX1,dwX2;
	DWORD dwY1,dwY2;	
	DWORD dwOff1;
	BOOL bTop,bLeft,bRight,bBottom;
	DWORD dwCount1;

	if (right <0) return; //rettangolo fuori dal buffer
	if (top>=m_lHeight) return; //rettangolo fuori dal buffer
	if (bottom <0) return; //rettangolo fuori dal buffer
	if (left>=m_lWidth) return; //rettangolo fuori dal buffer

	if (left<0) {dwX1=0;bLeft=FALSE;} else {dwX1=(DWORD) left;bLeft=TRUE;}

	if (top<0) {dwY1=0;bTop=FALSE;}	else {dwY1=(DWORD) top;bTop=TRUE;}

	if (right>=m_lWidth) {dwX2=(DWORD) m_lWidth-1;bRight=FALSE;} else {dwX2=(DWORD) right;bRight=TRUE;}

	if (bottom>= m_lHeight) {dwY2=(DWORD) m_lHeight-1;bBottom=FALSE;} else {dwY2=(DWORD) bottom;bBottom=TRUE;}

	if (bTop) 
	{
		//linea orizzontale in alto
		dwOff1=m_pdwOffset[dwY1];
		for (dwCount=dwX1;dwCount<=dwX2;dwCount++)
		{	
			m_wBuff[dwOff1+dwCount]=wForeColor;			
		}		
	}

	if (bBottom) 
	{
		//linea orizzontale in basso
		dwOff1=m_pdwOffset[dwY2];
		for (dwCount=dwX1;dwCount<=dwX2;dwCount++)
		{	
			m_wBuff[dwOff1+dwCount]=wForeColor;			
		}		
	}

	if (bLeft) 
	{
		//lato sinistro		
		for (dwCount=dwY1;dwCount<=dwY2;dwCount++)
		{	
			dwOff1=m_pdwOffset[dwCount];
			m_wBuff[dwOff1+dwX1]=wForeColor;			
		}		
	}

	if (bRight) 
	{
		//lato sinistro		
		for (dwCount=dwY1;dwCount<=dwY2;dwCount++)
		{	
			dwOff1=m_pdwOffset[dwCount];
			m_wBuff[dwOff1+dwX2]=wForeColor;			
		}		
	}	
	

	//non ridisegna il bordo
	dwY1++;
	dwY2--;
	dwX1++;
	dwX2--;

	//riempimento
	for (dwCount=dwY1;dwCount<=dwY2;dwCount++)
	{
		dwOff1=m_pdwOffset[dwCount];
		for (dwCount1=dwX1;dwCount1<=dwX2;dwCount1++)
		{
			m_wBuff[dwOff1+dwCount1]=wFillColor;
		}			
	}

}

///////////////////////////////////// CImgBuffer16::SetPixel ////////////////////////////////

void CImgBuffer16::SetPixel(LONG x,LONG y,WORD wColor) //accende un pixel
{
	m_wBuff[m_pdwOffset[y]+x]=wColor;
}

///////////////////////////////////// CImgBuffer16::GetPixel ////////////////////////////////

WORD CImgBuffer16::GetPixel(LONG x,LONG y)
{
	return m_wBuff[m_pdwOffset[y]+x];
}
//////////////////////////////////// CImgBuffer16::FillArea //////////////////////////////////

void CImgBuffer16::FillArea(const LONG x,const LONG y,WORD wColor,WORD wBound,int mode)
{	
	LONG xp;
	LONG yup;
	WORD wPix;
	BOOL bUp=FALSE,bDown=FALSE;
	DWORD dwCount=0;
	BOOL bSw1=FALSE,bSw2=FALSE;
	LONG lxl[FILL_BUFF],lxr[FILL_BUFF],lys[FILL_BUFF];
	LONG lxs1[100],lys1[100];	
	LONG ly;
	DWORD dwIndex=0,dwIndex1=0;
   	
	if (!(x>=0 && x<m_lWidth && y>=0 && y<m_lHeight)) return; //il punto � fuori dal buffer

	m_wFillColor=wColor;
	m_iFillMode=mode;
	m_wBound=wBound;

	if (m_wBuff[m_pdwOffset[y]+x]==wBound) return;  


	dwIndex=1;
	//riempie la prima riga
	FillLine(x,y,&lxl[dwIndex],&lxr[dwIndex]);
	lys[dwIndex]=y; //ordinata della prima linea
	
	yup=0;

	while (dwIndex>0)
	{	

		dwIndex1=0;

		//I fase ricerca le linee da riempire adiacenti a quelle gi� riempite (dwIndex = numero di linee riempite in precedenza)
		for (dwCount=1;dwCount<=dwIndex;dwCount++)
		{
		
            //ricerca le linee sul lato superiore
			if (lys[dwCount]>1)
			{
				bUp=FALSE;

				ly=lys[dwCount]-1;//controlla i pixel sul lato superiore

			//	cdb.WriteErrorFile("esamina linea sup. alla linea (%d,%d,y=%d) ycur=%d",lxl[dwCount],lxr[dwCount],lys[dwCount],ly); 
				
				for (xp=lxl[dwCount];xp<=lxr[dwCount];xp++)
				{	
					wPix=m_wBuff[m_pdwOffset[ly]+xp];
					if (wPix != wBound && wPix!= wColor)
					{
						if (!bUp && dwIndex1<FILL_BUFF-1)
						{
							bUp=TRUE;
							dwIndex1++;
							//inserisce un punto
							lxs1[dwIndex1]=xp;
							lys1[dwIndex1]=ly;

						//	cdb.WriteErrorFile("aggiunge punto superiore %d %d index=%d",lxs1[dwIndex1],lys1[dwIndex1],dwIndex1);

						}
					}
					else 
					{
						if (bUp) bUp=!bUp;
					}
				}//fine for xp
			}//fine if

			//ricerca le linee sul lato inferiore
			if (lys[dwCount]<m_lHeight-1)
			{
				bDown=FALSE;

				ly=lys[dwCount]+1;//controlla i pixel sul lato inferiore

			//	cdb.WriteErrorFile("esamina linea inferiore. alla linea (%d,%d,y=%d) ycur=%d",lxl[dwCount],lxr[dwCount],lys[dwCount],ly); 
				
				
				for (xp=lxl[dwCount];xp<=lxr[dwCount];xp++)
				{
					wPix=m_wBuff[m_pdwOffset[ly]+xp];
					if (wPix != wBound && wPix != wColor)
					{
						if (!bDown && dwIndex1<FILL_BUFF-1)
						{
						
							bDown=TRUE;
							dwIndex1++;
							//inserisce un punto
							lxs1[dwIndex1]=xp;
							lys1[dwIndex1]=ly;

						//	cdb.WriteErrorFile("aggiunge punto inferiore %d %d index=%d",lxs1[dwIndex1],lys1[dwIndex1],dwIndex1);

						}
					}
					else 
					{
						if (bDown) bDown=!bDown;
					}
				}

			//	cdb.WriteErrorFile("fine ciclo dwCount=%d dwIndex=%d",dwCount,dwIndex);
			}

		}//fine for dwCount

        //II fase riempie le linee a partire dai punti trovati
		if (dwIndex1>0)
		{			

			for (dwCount=1;dwCount<=dwIndex1;dwCount++)
			{
				FillLine(lxs1[dwCount],lys1[dwCount],&lxl[dwCount],&lxr[dwCount]);
				lys[dwCount]=lys1[dwCount];

			//	cdb.WriteErrorFile("add line x=%d y=%d left=%d right=%d",lxs1[dwCount],lys1[dwCount],lxl[dwCount],lxr[dwCount]);
			}				
		}

		dwIndex=dwIndex1;

	//	cdb.WriteErrorFile("dwIndex=%d",dwIndex);

	} //fine while

	
}

///////////////////////////////// CImgBuffer16::FindLine ///////////////////////////////////////
//funzione privata usata da FillArea
void CImgBuffer16::FillLine(const LONG x,const LONG y,LONG *xleft,LONG *xright)
{
	WORD wPix;
	LONG xp;
	LONG xleft1,xright1;
	BOOL bFound1=FALSE,bFound2=FALSE;
	static int iIndex=0;

	xright1=xleft1=xp=x;

	wPix=m_wBuff[m_pdwOffset[y]+x];

	*xleft=xleft1;
	*xright=xright1;

	if (wPix==m_wBound) return;

	while (wPix != m_wBound)
	{
		xp--;

		if (xp<0) {wPix=m_wBound;}
		else wPix=m_wBuff[m_pdwOffset[y]+xp];	
		
	}

	xp++; //non copre il bordo

	xleft1=xp; //estremo sinistro

	xp=x;

	wPix=m_wBuff[m_pdwOffset[y]+x];

	while (wPix != m_wBound)
	{
		xp++;

		if (xp>=m_lWidth) {wPix=m_wBound;}
		else wPix=m_wBuff[m_pdwOffset[y]+xp];

	}

	xp--;

	xright1=xp;

	//riempie la lienea
	for (xp=xleft1;xp<=xright1;xp++) m_wBuff[m_pdwOffset[y]+xp]=m_wFillColor;

	*xleft=xleft1;
	*xright=xright1;    

}
